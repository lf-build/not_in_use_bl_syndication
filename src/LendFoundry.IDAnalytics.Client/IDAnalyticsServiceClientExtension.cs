﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.IDAnalytics.Client
{
    public static  class IDAnalyticsServiceClientExtension
    {
        public static IServiceCollection AddIDAnalyticsService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IIDAnalyticsServiceClientFactory>(p => new IDAnalyticsServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IIDAnalyticsServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
