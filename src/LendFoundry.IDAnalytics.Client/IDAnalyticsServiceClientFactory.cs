﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.IDAnalytics;
using System;

namespace LendFoundry.IDAnalytics.Client
{
    public class IDAnalyticsServiceClientFactory :IIDAnalyticsServiceClientFactory
    {
        public IDAnalyticsServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;

        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IIDAnalyticsService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new IDAnalyticsService(client);
        }
    }
}
