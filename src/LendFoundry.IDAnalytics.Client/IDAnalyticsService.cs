﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.IDAnalytics;
using LendFoundry.Syndication.IDAnalytics.Proxy.Response;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.IDAnalytics.Client
{
    public class IDAnalyticsService :IIDAnalyticsService
    {
        public IDAnalyticsService(IServiceClient client)
        {
            Client = client;

        }
      
        private IServiceClient Client { get; }

        public async Task<Envelope> GetReport(string entityType, string entityId, IDAnalyticsRequest request)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/report", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<Envelope>(restrequest);
        }
    }
}
