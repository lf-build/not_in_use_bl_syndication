﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.IDAnalytics;

namespace LendFoundry.IDAnalytics.Client
{
    public interface IIDAnalyticsServiceClientFactory
    {
        IIDAnalyticsService Create(ITokenReader reader);
    }
}
