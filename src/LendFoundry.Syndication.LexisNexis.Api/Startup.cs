﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.LexisNexis.Api.Configuration;
using LendFoundry.Syndication.LexisNexis.FlexId;
using LendFoundry.Syndication.LexisNexis.FraudPoint;
using LendFoundry.Syndication.LexisNexis.InstantId;
using LendFoundry.Syndication.LexisNexis.InstantIdKba;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Lookup.Client;

using Newtonsoft.Json;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using System.Threading.Tasks;
using System;
using Microsoft.AspNet.Http;

namespace LendFoundry.Syndication.LexisNexis.Api
{
    public class Startup
    {
        private ILexisNexisConfiguration LexisNexisConfigurations { get; set; }

        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                // FlexIdRequest
                options.AddInterfaceConverter<IFlexIdRequest, FlexIdRequest>()
                .AddInterfaceConverter<FlexId.IName, FlexId.Name>()
                .AddInterfaceConverter<FlexId.IAddress, FlexId.Address>()
                .AddInterfaceConverter<FlexId.IDate, FlexId.Date>()
                .AddInterfaceConverter<FlexId.IPassport, FlexId.Passport>();

                // FraudPointRequest
                options.AddInterfaceConverter<IFraudPointRequest, FraudPointRequest>()
                .AddInterfaceConverter<FraudPoint.IName, FraudPoint.Name>()
                .AddInterfaceConverter<FraudPoint.IAddress, FraudPoint.Address>()
                .AddInterfaceConverter<FraudPoint.IDate, FraudPoint.Date>()
                .AddInterfaceConverter<FraudPoint.ITimeStamp, FraudPoint.TimeStamp>()
                .AddInterfaceConverter<IFraudPointOption, FraudPoint.FraudPointOption>()
                .AddInterfaceConverter<IFraudPointModels, FraudPoint.FraudPointModels>();

                // InstantIdRequest
                options.AddInterfaceConverter<IInstantIdRequest, InstantIdRequest>()
                .AddInterfaceConverter<InstantId.IName, InstantId.Name>()
                .AddInterfaceConverter<InstantId.IAddress, InstantId.Address>()
                .AddInterfaceConverter<InstantId.IDate, InstantId.Date>()
                .AddInterfaceConverter<InstantId.IPassport, InstantId.Passport>()
                .AddInterfaceConverter<InstantId.ITimeStamp, InstantId.TimeStamp>();

                // InstantIdIdentityRequest
                options.AddInterfaceConverter<IInstantIdIdentityRequest, InstantIdIdentityRequest>()
                .AddInterfaceConverter<IBirthDateType, BirthDateType>()
                .AddInterfaceConverter<IAddressType, AddressType>()
                .AddInterfaceConverter<IUkAddressType, UkAddressType>()
                .AddInterfaceConverter<IPhoneNumberType, PhoneNumberType>()
                .AddInterfaceConverter<IBusinessType, BusinessType>()
                .AddInterfaceConverter<IRiskAssessmentType, RiskAssessmentType>()
                .AddInterfaceConverter<IRiskComponentType, RiskComponentType>()
                .AddInterfaceConverter<IRequireExactMatchFlexId, RequireExactMatchFlexId>();

                // InstantIdContinuationRequest
                options.AddInterfaceConverter<IInstantIdContinuationRequest, InstantIdContinuationRequest>()
                .AddInterfaceConverter<IAnswerType, AnswerType>();
            });
            services.AddCors();

            // interface implements
            services.AddConfigurationService<LexisNexisConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);

            // configuration factory
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<LexisNexisConfiguration>>().Get());

            services.AddTransient<IFlexIdClient, FlexIdClient>();
            services.AddTransient<IFraudPointClient, FraudPointClient>();
            services.AddTransient<IInstantIdClient, InstantIdClient>();
            services.AddTransient<IInstantIdKbaClient, InstantIdKbaClient>();
            services.AddTransient<ILexisNexisService, LexisNexisService>();
            services.AddTransient<IBankruptcyService, BankruptcyService>();
            services.AddTransient<IBankruptcyProxy, BankruptcyProxy>();
            services.AddTransient<ICriminalRecordService, CriminalRecordService>();
            services.AddTransient<ICriminalRecordProxy, CriminalRecordProxy>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMvc();
            app.UseHealthCheck();
        }
    }

   
}
