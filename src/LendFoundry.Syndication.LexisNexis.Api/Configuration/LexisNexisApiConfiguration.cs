﻿using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Api.Configuration
{
    public class LexisNexisApiConfiguration 
    {
        //public FlexIdApiConfiguration FlexId { get; set; }

        //public FraudPointApiConfiguration FraudPoint { get; set; }

        //public InstantIdApiConfiguration InstantId { get; set; }

        //public InstantIdKbaApiConfiguration InstantIdKba { get; set; }

        //public ILexisNexisConfiguration Parse()
        //{
        //    var lexisNexisConfiguration = new LexisNexisConfiguration();

        //    if (FlexId == null)
        //        throw new InvalidArgumentException("The 'LexisNexisConfiguration' related to {FlexId} not found.", nameof(FlexId));

        //    if (FraudPoint == null)
        //        throw new InvalidArgumentException("The 'LexisNexisConfiguration' related to {FraudPoint} not found.", nameof(FraudPoint));

        //    if (InstantId == null)
        //        throw new InvalidArgumentException("The 'LexisNexisConfiguration' related to {InstantId} not found.", nameof(InstantId));

        //    if (InstantIdKba == null)
        //        throw new InvalidArgumentException("The 'LexisNexisConfiguration' related to {InstantIdKba} not found.", nameof(InstantIdKba));

        //    lexisNexisConfiguration.FlexId = ParseFlexIdConfiguration(FlexId);
        //    lexisNexisConfiguration.FraudPoint = ParseFraudPointConfiguration(FraudPoint);
        //    lexisNexisConfiguration.InstantId = ParseInstantIdConfiguration(InstantId);
        //    lexisNexisConfiguration.InstantIdKba = ParseInstantIdKbaConfiguration(InstantIdKba);

        //    return lexisNexisConfiguration;
        //}

        //private FlexIdServiceConfiguration ParseFlexIdConfiguration(FlexIdApiConfiguration flexId)
        //{
        //    var flexIdConfig = new FlexIdServiceConfiguration
        //    {
        //        FlexIdUrl = flexId.FlexIdUrl,
        //        UserName = flexId.UserName,
        //        Password = flexId.Password
        //    };

        //    if (flexId.EndUser != null)
        //    {
        //        flexIdConfig.EndUser = new FlexId.User
        //        {
        //            ReferenceCode = flexId.EndUser.ReferenceCode,
        //            BillingCode = flexId.EndUser.BillingCode,
        //            QueryId = flexId.EndUser.QueryId,
        //            GrammLeachBlileyPurpose = flexId.EndUser.GrammLeachBlileyPurpose,
        //            DriverLicensePurpose = flexId.EndUser.DriverLicensePurpose,
        //            AccountNumber = flexId.EndUser.AccountNumber,
        //            MaxWaitSeconds = flexId.EndUser.MaxWaitSeconds
        //        };

        //        if (flexId.EndUser.EndUser != null && !string.IsNullOrWhiteSpace(flexId.EndUser.EndUser.ToString()))
        //        {
        //            flexIdConfig.EndUser.EndUser = new FlexId.EndUserInfo
        //            {
        //                CompanyName = flexId.EndUser.EndUser.CompanyName,
        //                StreetAddress1 = flexId.EndUser.EndUser.StreetAddress1,
        //                City = flexId.EndUser.EndUser.City,
        //                State = flexId.EndUser.EndUser.State,
        //                Zip5 = flexId.EndUser.EndUser.Zip5,
        //            };
        //        }
        //    }

        //    if (flexId.Options != null)
        //    {
        //        flexIdConfig.Options = new FlexId.FlexIdOption
        //        {
        //            UseDateOfBirthFilter = flexId.Options.UseDateOfBirthFilter,
        //            DateOfBirthRadius = flexId.Options.DateOfBirthRadius,
        //            IncludeMsOverride = flexId.Options.IncludeMsOverride,
        //            PoBoxCompliance = flexId.Options.PoBoxCompliance,
        //            IncludeAllRiskIndicators = flexId.Options.IncludeAllRiskIndicators,
        //            IncludeVerifiedElementSummary = flexId.Options.IncludeVerifiedElementSummary,
        //            IncludeDrivingLicenseVerification = flexId.Options.IncludeDrivingLicenseVerification,
        //            CustomCviModelName = flexId.Options.CustomCviModelName,
        //            LastSeenThreshold = flexId.Options.LastSeenThreshold,
        //            IncludeMiOverride = flexId.Options.IncludeMiOverride,
        //            IncludeSsnVerification = flexId.Options.IncludeSsnVerification,
        //            InstantIdVersion = flexId.Options.InstantIdVersion
        //        };

        //        if (flexId.Options.DateOfBirthMatch != null)
        //        {
        //            flexIdConfig.Options.DateOfBirthMatch = new FlexId.DateOfBirthMatchOptions
        //            {
        //                MatchType = (FlexId.DateOfBirthMatchType)Enum.Parse(typeof(FlexId.DateOfBirthMatchType), flexId.Options.DateOfBirthMatch.MatchType.ToString(), true),
        //                MatchYearRadius = flexId.Options.DateOfBirthMatch.MatchYearRadius
        //            };
        //        }

        //        if (flexId.Options.RequireExactMatch != null)
        //        {
        //            flexIdConfig.Options.RequireExactMatch = new FlexId.RequireExactMatchFlexId
        //            {
        //                Address = flexId.Options.RequireExactMatch.Address,
        //                DriverLicense = flexId.Options.RequireExactMatch.DriverLicense,
        //                FirstName = flexId.Options.RequireExactMatch.FirstName,
        //                FirstNameAllowNickname = flexId.Options.RequireExactMatch.FirstNameAllowNickname,
        //                HomePhone = flexId.Options.RequireExactMatch.HomePhone,
        //                LastName = flexId.Options.RequireExactMatch.LastName,
        //                Ssn = flexId.Options.RequireExactMatch.Ssn
        //            };
        //        }

        //        if (flexId.Options.IncludeModels != null)
        //        {
        //            flexIdConfig.Options.IncludeModels = new FlexId.FlexIdModels();
        //            if (flexId.Options.IncludeModels.FraudPointModel != null)
        //            {
        //                flexIdConfig.Options.IncludeModels.FraudPointModel = new FlexId.FraudPointModelWithOptions
        //                {
        //                    ModelName = flexId.Options.IncludeModels.FraudPointModel.ModelName,
        //                    IncludeRiskIndices = flexId.Options.IncludeModels.FraudPointModel.IncludeRiskIndices
        //                };
        //            }

        //            if (flexId.Options.IncludeModels.ModelRequests != null && flexId.Options.IncludeModels.ModelRequests.Length > 0)
        //            {
        //                flexIdConfig.Options.IncludeModels.ModelRequests = flexId.Options.IncludeModels
        //                    .ModelRequests.Select(m =>
        //                    {
        //                        var modelRequest = new FlexId.ModelRequest { ModelName = m.ModelName };
        //                        if (m.ModelOptions != null && m.ModelOptions.Length > 0)
        //                        {
        //                            modelRequest.ModelOptions = m.ModelOptions
        //                            .Select(o =>
        //                            {
        //                                return new FlexId.ModelOption
        //                                {
        //                                    OptionName = o.OptionName,
        //                                    OptionValue = o.OptionValue
        //                                };
        //                            }).ToArray();
        //                        }

        //                        return modelRequest;
        //                    }).ToArray();
        //            }
        //        }

        //        if (flexId.Options.WatchLists != null && flexId.Options.WatchLists.Length > 0)
        //            flexIdConfig.Options.WatchLists = flexId.Options.WatchLists;

        //        if (flexId.Options.CviCalculationOptions != null)
        //        {
        //            flexIdConfig.Options.CviCalculationOptions = new FlexId.OptionsForCviCalculation
        //            {
        //                DisableCustomerNetworkOption = flexId.Options.CviCalculationOptions.DisableCustomerNetworkOption,
        //                IncludeDateOfBirth = flexId.Options.CviCalculationOptions.IncludeDateOfBirth,
        //                IncludeDriverLicense = flexId.Options.CviCalculationOptions.IncludeDriverLicense
        //            };
        //        }
        //    }

        //    return flexIdConfig;
        //}

        //private FraudPointServiceConfiguration ParseFraudPointConfiguration(FraudPointApiConfiguration fraudPoint)
        //{
        //    var fraudPointConfig = new FraudPointServiceConfiguration
        //    {
        //        FraudPointUrl = fraudPoint.FraudPointUrl,
        //        UserName = fraudPoint.UserName,
        //        Password = fraudPoint.Password
        //    };

        //    if (fraudPoint.EndUser != null)
        //    {
        //        fraudPointConfig.EndUser = new FraudPoint.User
        //        {
        //            ReferenceCode = fraudPoint.EndUser.ReferenceCode,
        //            BillingCode = fraudPoint.EndUser.BillingCode,
        //            QueryId = fraudPoint.EndUser.QueryId,
        //            GrammLeachBlileyPurpose = fraudPoint.EndUser.GrammLeachBlileyPurpose,
        //            DriverLicensePurpose = fraudPoint.EndUser.DriverLicensePurpose,
        //            AccountNumber = fraudPoint.EndUser.AccountNumber,
        //            MaxWaitSeconds = fraudPoint.EndUser.MaxWaitSeconds
        //        };

        //        if (fraudPoint.EndUser.EndUser != null)
        //        {
        //            fraudPointConfig.EndUser.EndUser = new FraudPoint.EndUserInfo
        //            {
        //                CompanyName = fraudPoint.EndUser.EndUser.CompanyName,
        //                StreetAddress1 = fraudPoint.EndUser.EndUser.StreetAddress1,
        //                City = fraudPoint.EndUser.EndUser.City,
        //                State = fraudPoint.EndUser.EndUser.State,
        //                Zip5 = fraudPoint.EndUser.EndUser.Zip5,
        //            };
        //        }
        //    }

        //    if (fraudPoint.Options != null)
        //    {
        //        fraudPointConfig.Options = new FraudPoint.FraudPointOption
        //        {
        //            AttributesVersionRequest = fraudPoint.Options.AttributesVersionRequest,
        //            RedFlagsReport = fraudPoint.Options.RedFlagsReport,
        //            IncludeRiskIndices = fraudPoint.Options.IncludeRiskIndices
        //        };

        //        if (fraudPoint.Options.IncludeModels != null)
        //        {
        //            fraudPointConfig.Options.IncludeModels = new FraudPoint.FraudPointModels
        //            {
        //                FraudPointModel = fraudPoint.Options.IncludeModels.FraudPointModel
        //            };

        //            if (fraudPoint.Options.IncludeModels.ModelRequests != null && fraudPoint.Options.IncludeModels.ModelRequests.Length > 0)
        //            {
        //                fraudPointConfig.Options.IncludeModels.ModelRequests = fraudPoint.Options.IncludeModels
        //                    .ModelRequests.Select(m =>
        //                    {
        //                        var modelRequest = new FraudPoint.ModelRequest { ModelName = m.ModelName };
        //                        if (m.ModelOptions != null && m.ModelOptions.Length > 0)
        //                        {
        //                            modelRequest.ModelOptions = m.ModelOptions
        //                            .Select(o =>
        //                            {
        //                                return new FraudPoint.ModelOption
        //                                {
        //                                    OptionName = o.OptionName,
        //                                    OptionValue = o.OptionValue
        //                                };
        //                            }).ToArray();
        //                        }
        //                        return modelRequest;
        //                    }).ToArray();
        //            }
        //        }
        //    }

        //    return fraudPointConfig;
        //}

        //private InstantIdServiceConfiguration ParseInstantIdConfiguration(InstantIdApiConfiguration instantId)
        //{
        //    var instantIdConfig = new InstantIdServiceConfiguration
        //    {
        //        InstantIdUrl = instantId.InstantIdUrl,
        //        UserName = instantId.UserName,
        //        Password = instantId.Password
        //    };
            
        //    if (instantId.EndUser != null)
        //    {
        //        instantIdConfig.EndUser = new InstantId.User
        //        {
        //            ReferenceCode = instantId.EndUser.ReferenceCode,
        //            BillingCode = instantId.EndUser.BillingCode,
        //            QueryId = instantId.EndUser.QueryId,
        //            GrammLeachBlileyPurpose = instantId.EndUser.GrammLeachBlileyPurpose,
        //            DriverLicensePurpose = instantId.EndUser.DriverLicensePurpose,
        //            AccountNumber = instantId.EndUser.AccountNumber,
        //            MaxWaitSeconds = instantId.EndUser.MaxWaitSeconds
        //        };

        //        if (instantId.EndUser.EndUser != null)
        //        {
        //            instantIdConfig.EndUser.EndUser = new InstantId.EndUserInfo
        //            {
        //                CompanyName = instantId.EndUser.EndUser.CompanyName,
        //                StreetAddress1 = instantId.EndUser.EndUser.StreetAddress1,
        //                City = instantId.EndUser.EndUser.City,
        //                State = instantId.EndUser.EndUser.State,
        //                Zip5 = instantId.EndUser.EndUser.Zip5,
        //            };
        //        }
        //    }

        //    if (instantId.Options != null)
        //    {
        //        instantIdConfig.Options = new InstantId.InstantIdOption2
        //        {
        //            IncludeClOverride = instantId.Options.IncludeClOverride,
        //            IncludeMsOverride = instantId.Options.IncludeMsOverride,
        //            IncludeDrivingLicenseVerification = instantId.Options.IncludeDrivingLicenseVerification,
        //            PoBoxCompliance = instantId.Options.PoBoxCompliance,
        //            RedFlagsReport = instantId.Options.RedFlagsReport,
        //            GlobalWatchlistThreshold = instantId.Options.GlobalWatchlistThreshold,
        //            IncludeAllRiskIndicators = instantId.Options.IncludeAllRiskIndicators,
        //            CustomCviModelName = instantId.Options.CustomCviModelName,
        //            LastSeenThreshold = instantId.Options.LastSeenThreshold,
        //            IncludeMiOverride = instantId.Options.IncludeMiOverride,
        //            InstantIdVersion = instantId.Options.InstantIdVersion,
        //            IncludeDeliveryPointBarcode = instantId.Options.IncludeDeliveryPointBarcode
        //        };

        //        if (instantId.Options.WatchLists != null && instantId.Options.WatchLists.Length > 0)
        //            instantIdConfig.Options.WatchLists = instantId.Options.WatchLists;

        //        if (instantId.Options.IncludeModels != null)
        //        {
        //            instantIdConfig.Options.IncludeModels = new InstantId.InstantIdModelsFp2
        //            {
        //                Thindex = instantId.Options.IncludeModels.Thindex
        //            };

        //            if (instantId.Options.IncludeModels.StudentDefenderModel != null)
        //            {
        //                instantIdConfig.Options.IncludeModels.StudentDefenderModel = new InstantId.StudentDefenderModel
        //                {
        //                    IsStudentApplicant = instantId.Options.IncludeModels.StudentDefenderModel.IsStudentApplicant,
        //                    StudentDefender = instantId.Options.IncludeModels.StudentDefenderModel.StudentDefender
        //                };
        //            }

        //            if (instantId.Options.IncludeModels.FraudPointModel != null)
        //            {
        //                instantIdConfig.Options.IncludeModels.FraudPointModel = new InstantId.FraudPointModelWithOptions
        //                {
        //                    IncludeRiskIndices = instantId.Options.IncludeModels.FraudPointModel.IncludeRiskIndices,
        //                    ModelName = instantId.Options.IncludeModels.FraudPointModel.ModelName
        //                };
        //            }

        //            if (instantId.Options.IncludeModels.ModelRequests != null && instantId.Options.IncludeModels.ModelRequests.Length > 0)
        //            {
        //                instantIdConfig.Options.IncludeModels.ModelRequests = instantId.Options.IncludeModels
        //                    .ModelRequests.Select(m => 
        //                    {
        //                        var modelRequest = new InstantId.ModelRequest { ModelName = m.ModelName };
        //                        if (m.ModelOptions != null && m.ModelOptions.Length > 0)
        //                        {
        //                            modelRequest.ModelOptions = m.ModelOptions
        //                            .Select(o =>
        //                            {
        //                                return new InstantId.ModelOption
        //                                {
        //                                    OptionName = o.OptionName,
        //                                    OptionValue = o.OptionValue
        //                                };
        //                            }).ToArray();
        //                        }
        //                        return modelRequest;
        //                    }).ToArray();
        //            }
        //        }

        //        if (instantId.Options.DateOfBirthMatch != null)
        //        {
        //            instantIdConfig.Options.DateOfBirthMatch = new InstantId.DateOfBirthMatchOptions
        //            {
        //                MatchType = (InstantId.DateOfBirthMatchType)Enum.Parse(typeof(InstantId.DateOfBirthMatchType), instantId.Options.DateOfBirthMatch.MatchType.ToString(), true),
        //                MatchYearRadius = instantId.Options.DateOfBirthMatch.MatchYearRadius
        //            };
        //        }

        //        if(instantId.Options.RequireExactMatch != null)
        //        {
        //            instantIdConfig.Options.RequireExactMatch = new InstantId.RequireExactMatchInstId
        //            {
        //                FirstName = instantId.Options.RequireExactMatch.FirstName,
        //                FirstNameAllowNickname = instantId.Options.RequireExactMatch.FirstNameAllowNickname,
        //                HomePhone = instantId.Options.RequireExactMatch.HomePhone,
        //                LastName = instantId.Options.RequireExactMatch.LastName,
        //                Ssn = instantId.Options.RequireExactMatch.Ssn
        //            };
        //        }

        //        if (instantId.Options.CviCalculationOptions != null)
        //        {
        //            instantIdConfig.Options.CviCalculationOptions = new InstantId.OptionsForCviCalculation
        //            {
        //                DisableCustomerNetworkOption = instantId.Options.CviCalculationOptions.DisableCustomerNetworkOption,
        //                IncludeDateOfBirth = instantId.Options.CviCalculationOptions.IncludeDateOfBirth,
        //                IncludeDriverLicense = instantId.Options.CviCalculationOptions.IncludeDriverLicense
        //            };
        //        }
        //    }

        //    return instantIdConfig;
        //}

        //private KbaServiceConfiguration ParseInstantIdKbaConfiguration(InstantIdKbaApiConfiguration instantIdKba)
        //{
        //    var instantIdKbaConfig = new KbaServiceConfiguration
        //    {
        //        InstantIdUrl = instantIdKba.InstantIdUrl,
        //        UserName = instantIdKba.UserName,
        //        Password = instantIdKba.Password
        //    };

        //    if (instantIdKba.EndUser != null)
        //    {
        //        instantIdKbaConfig.EndUser = new InstantId.User
        //        {
        //            ReferenceCode = instantIdKba.EndUser.ReferenceCode,
        //            BillingCode = instantIdKba.EndUser.BillingCode,
        //            QueryId = instantIdKba.EndUser.QueryId,
        //            GrammLeachBlileyPurpose = instantIdKba.EndUser.GrammLeachBlileyPurpose,
        //            DriverLicensePurpose = instantIdKba.EndUser.DriverLicensePurpose,
        //            AccountNumber = instantIdKba.EndUser.AccountNumber,
        //            MaxWaitSeconds = instantIdKba.EndUser.MaxWaitSeconds
        //        };

        //        if (instantIdKba.EndUser.EndUser != null)
        //        {
        //            instantIdKbaConfig.EndUser.EndUser = new InstantId.EndUserInfo
        //            {
        //                CompanyName = instantIdKba.EndUser.EndUser.CompanyName,
        //                StreetAddress1 = instantIdKba.EndUser.EndUser.StreetAddress1,
        //                City = instantIdKba.EndUser.EndUser.City,
        //                State = instantIdKba.EndUser.EndUser.State,
        //                Zip5 = instantIdKba.EndUser.EndUser.Zip5,
        //            };
        //        }
        //    }

        //    if (instantIdKba.Options != null)
        //    {
        //        instantIdKbaConfig.Options = new InstantId.InstantIdOption2
        //        {
        //            IncludeClOverride = instantIdKba.Options.IncludeClOverride,
        //            IncludeMsOverride = instantIdKba.Options.IncludeMsOverride,
        //            IncludeDrivingLicenseVerification = instantIdKba.Options.IncludeDrivingLicenseVerification,
        //            PoBoxCompliance = instantIdKba.Options.PoBoxCompliance,
        //            RedFlagsReport = instantIdKba.Options.RedFlagsReport,
        //            GlobalWatchlistThreshold = instantIdKba.Options.GlobalWatchlistThreshold,
        //            IncludeAllRiskIndicators = instantIdKba.Options.IncludeAllRiskIndicators,
        //            CustomCviModelName = instantIdKba.Options.CustomCviModelName,
        //            LastSeenThreshold = instantIdKba.Options.LastSeenThreshold,
        //            IncludeMiOverride = instantIdKba.Options.IncludeMiOverride,
        //            InstantIdVersion = instantIdKba.Options.InstantIdVersion,
        //            IncludeDeliveryPointBarcode = instantIdKba.Options.IncludeDeliveryPointBarcode
        //        };

        //        if (instantIdKba.Options.WatchLists != null && instantIdKba.Options.WatchLists.Length > 0)
        //            instantIdKbaConfig.Options.WatchLists = instantIdKba.Options.WatchLists;

        //        if (instantIdKba.Options.IncludeModels != null)
        //        {
        //            instantIdKbaConfig.Options.IncludeModels = new InstantId.InstantIdModelsFp2
        //            {
        //                Thindex = instantIdKba.Options.IncludeModels.Thindex,
        //                FraudPointModel = new InstantId.FraudPointModelWithOptions(),
        //                StudentDefenderModel = new InstantId.StudentDefenderModel()
        //            };

        //            if (instantIdKba.Options.IncludeModels.StudentDefenderModel != null)
        //            {
        //                instantIdKbaConfig.Options.IncludeModels.StudentDefenderModel = new InstantId.StudentDefenderModel
        //                {
        //                    IsStudentApplicant = instantIdKba.Options.IncludeModels.StudentDefenderModel.IsStudentApplicant,
        //                    StudentDefender = instantIdKba.Options.IncludeModels.StudentDefenderModel.StudentDefender
        //                };
        //            }

        //            if (instantIdKba.Options.IncludeModels.FraudPointModel != null)
        //            {
        //                instantIdKbaConfig.Options.IncludeModels.FraudPointModel = new InstantId.FraudPointModelWithOptions
        //                {
        //                    IncludeRiskIndices = instantIdKba.Options.IncludeModels.FraudPointModel.IncludeRiskIndices,
        //                    ModelName = instantIdKba.Options.IncludeModels.FraudPointModel.ModelName
        //                };
        //            }

        //            if (instantIdKba.Options.IncludeModels.ModelRequests != null && instantIdKba.Options.IncludeModels.ModelRequests.Length > 0)
        //            {
        //                instantIdKbaConfig.Options.IncludeModels.ModelRequests = instantIdKba.Options.IncludeModels
        //                    .ModelRequests.Select(m =>
        //                    {
        //                        var modelRequest = new InstantId.ModelRequest { ModelName = m.ModelName };
        //                        if (m.ModelOptions != null && m.ModelOptions.Length > 0)
        //                        {
        //                            modelRequest.ModelOptions = m.ModelOptions
        //                            .Select(o =>
        //                            {
        //                                return new InstantId.ModelOption
        //                                {
        //                                    OptionName = o.OptionName,
        //                                    OptionValue = o.OptionValue
        //                                };
        //                            }).ToArray();
        //                        }
        //                        return modelRequest;
        //                    }).ToArray();
        //            }
        //        }

        //        instantIdKbaConfig.Options.DateOfBirthMatch = new InstantId.DateOfBirthMatchOptions();
        //        if (instantIdKba.Options.DateOfBirthMatch != null)
        //        {
        //            instantIdKbaConfig.Options.DateOfBirthMatch = new InstantId.DateOfBirthMatchOptions
        //            {
        //                MatchType = (InstantId.DateOfBirthMatchType)Enum.Parse(typeof(InstantId.DateOfBirthMatchType), instantIdKba.Options.DateOfBirthMatch.MatchType.ToString(), true),
        //                MatchYearRadius = instantIdKba.Options.DateOfBirthMatch.MatchYearRadius
        //            };
        //        }

        //        instantIdKbaConfig.Options.RequireExactMatch = new InstantId.RequireExactMatchInstId();
        //        if (instantIdKba.Options.RequireExactMatch != null)
        //        {
        //            instantIdKbaConfig.Options.RequireExactMatch = new InstantId.RequireExactMatchInstId
        //            {
        //                FirstName = instantIdKba.Options.RequireExactMatch.FirstName,
        //                FirstNameAllowNickname = instantIdKba.Options.RequireExactMatch.FirstNameAllowNickname,
        //                HomePhone = instantIdKba.Options.RequireExactMatch.HomePhone,
        //                LastName = instantIdKba.Options.RequireExactMatch.LastName,
        //                Ssn = instantIdKba.Options.RequireExactMatch.Ssn
        //            };
        //        }

        //        instantIdKbaConfig.Options.CviCalculationOptions = new InstantId.OptionsForCviCalculation();
        //        if (instantIdKba.Options.CviCalculationOptions != null)
        //        {
        //            instantIdKbaConfig.Options.CviCalculationOptions = new InstantId.OptionsForCviCalculation
        //            {
        //                DisableCustomerNetworkOption = instantIdKba.Options.CviCalculationOptions.DisableCustomerNetworkOption,
        //                IncludeDateOfBirth = instantIdKba.Options.CviCalculationOptions.IncludeDateOfBirth,
        //                IncludeDriverLicense = instantIdKba.Options.CviCalculationOptions.IncludeDriverLicense
        //            };
        //        }
        //    }

        //    instantIdKbaConfig.IdentityVerificationSettings = new InstantIdKba.IdentityVerificationSettingsType();
        //    if (instantIdKba.IdentityVerificationSettings != null)
        //    {
        //        instantIdKbaConfig.IdentityVerificationSettings = new InstantIdKba.IdentityVerificationSettingsType
        //        {
        //            Task = instantIdKba.IdentityVerificationSettings.Task,
        //            SequenceId = instantIdKba.IdentityVerificationSettings.SequenceId,
        //            Agent = instantIdKba.IdentityVerificationSettings.Agent,
        //            AccountName = instantIdKba.IdentityVerificationSettings.AccountName,
        //            Mode = instantIdKba.IdentityVerificationSettings.Mode,
        //            ModeSpecified = instantIdKba.IdentityVerificationSettings.ModeSpecified,
        //            RuleSet = instantIdKba.IdentityVerificationSettings.RuleSet
        //        };
        //    }

        //    instantIdKbaConfig.ContinueSettings = new InstantIdKba.ContinueSettingsType();
        //    if (instantIdKba.ContinueSettings != null)
        //    {
        //        instantIdKbaConfig.ContinueSettings = new InstantIdKba.ContinueSettingsType
        //        {
        //            AccountName = instantIdKba.ContinueSettings.AccountName,
        //            Mode = instantIdKba.ContinueSettings.Mode,
        //            ModeSpecified = instantIdKba.ContinueSettings.ModeSpecified,
        //            RuleSet = instantIdKba.ContinueSettings.RuleSet
        //        };
        //    }

        //    return instantIdKbaConfig;
        //}
    }

    public class FlexIdApiConfiguration
    {
        public string FlexIdUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.85";
        public string UserName { get; set; }
        public string Password { get; set; }
        public User EndUser { get; set; }
        public FlexIdOption Options { get; set; }
    }

    public class FraudPointApiConfiguration
    {
        public string FraudPointUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.88";
        public string UserName { get; set; }
        public string Password { get; set; }
        public User EndUser { get; set; }
        public FraudPointOption Options { get; set; }
    }

    public class InstantIdApiConfiguration
    {
        public string InstantIdUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.85";
        public string UserName { get; set; }
        public string Password { get; set; }
        public User EndUser { get; set; }
        public InstantIdOption Options { get; set; }
    }

    public class InstantIdKbaApiConfiguration
    {
        public string InstantIdUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.85";
        public string UserName { get; set; }
        public string Password { get; set; }
        public User EndUser { get; set; }
        public InstantIdOption Options { get; set; }

        public IdentityVerificationSettingsType IdentityVerificationSettings { get; set; }
        public ContinueSettingsType ContinueSettings { get; set; }
    }

    public class ContinueSettingsType
    {
        public ContinueSettingsType() { }

        public long TransactionId { get; set; }

        public string AccountName { get; set; }

        public InstantIdKba.ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public InstantIdKba.SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public InstantIdKba.AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public SpecialFeatureType[] SpecialFeature { get; set; }

        public InternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }

    public class IdentityVerificationSettingsType
    {
        public IdentityVerificationSettingsType() {}
        

        public InstantIdKba.TaskType Task { get; set; }

        public string SequenceId { get; set; }

        public string Agent { get; set; }

        public bool PatriotActCompliance { get; set; }
        public bool PatriotActComplianceSpecified { get; set; }

        public long ParentTransactionId { get; set; }
        public bool ParentTransactionIdSpecified { get; set; }

        public string AccountName { get; set; }

        public InstantIdKba.ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public InstantIdKba.SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public InstantIdKba.AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public SpecialFeatureType[] SpecialFeature { get; set; }

        public InternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }

    public class InternationalizationType
    {
        public InstantIdKba.LanguageType Language { get; set; }

        public InstantIdKba.LanguageVenueType LanguageVenue { get; set; }
    }

    public class SpecialFeatureType
    {
        public string SpecialFeatureCode { get; set; }
        public string SpecialFeatureValue { get; set; }
    }

    public class User
    {
        public User() {}

        public string ReferenceCode { get; set; }
        public string BillingCode { get; set; }
        public string QueryId { get; set; }
        public string GrammLeachBlileyPurpose { get; set; }
        public string DriverLicensePurpose { get; set; }
        public EndUserInfo EndUser { get; set; }
        public int MaxWaitSeconds { get; set; }
        public string AccountNumber { get; set; }
    }

    public class FraudPointOption
    {
        public FraudPointModels IncludeModels { get; set; }
        public string AttributesVersionRequest { get; set; }
        public string RedFlagsReport { get; set; }
        public bool IncludeRiskIndices { get; set; }
    }

    public class FraudPointModels
    {
        public string FraudPointModel { get; set; }
        public ModelRequest[] ModelRequests { get; set; }
    }

    public class ModelRequest
    {
        public string ModelName { get; set; }
        public ModelOption[] ModelOptions { get; set; }
    }

    public class ModelOption
    {
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }

    public class FlexIdOption
    {
        public string[] WatchLists { get; set; }
        public bool UseDateOfBirthFilter { get; set; } = true;
        public int DateOfBirthRadius { get; set; } = 5;
        public bool IncludeMsOverride { get; set; } = true;
        public bool PoBoxCompliance { get; set; } = true;
        public RequireExactMatch RequireExactMatch { get; set; }
        public bool IncludeAllRiskIndicators { get; set; } = true;
        public bool IncludeVerifiedElementSummary { get; set; } = true;
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        public DateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        public FlexIdModels IncludeModels { get; set; }
        public string CustomCviModelName { get; set; }
        public string LastSeenThreshold { get; set; }
        public bool IncludeMiOverride { get; set; } = true;
        public bool IncludeSsnVerification { get; set; } = true;
        public OptionsForCviCalculation CviCalculationOptions { get; set; }
        public string InstantIdVersion { get; set; }
    }

    public class DateOfBirthMatchOptions
    {
        public DateOfBirthMatchType MatchType { get; set; } = DateOfBirthMatchType.FuzzyCCYYMMDD;
        public int MatchYearRadius { get; set; } = 5;
    }

    public enum DateOfBirthMatchType
    {
        FuzzyCCYYMMDD,
        FuzzyCCYYMM,
        RadiusCCYY,
        ExactCCYYMMDD,
        ExactCCYYMM
    }

    public class FlexIdModels
    {
        public FraudPointModelWithOptions FraudPointModel { get; set; }
        public ModelRequest[] ModelRequests { get; set; }
    }

    public class OptionsForCviCalculation
    {
        public OptionsForCviCalculation() {}

        public bool IncludeDateOfBirth { get; set; } = true;
        public bool IncludeDriverLicense { get; set; } = true;
        public bool DisableCustomerNetworkOption { get; set; } = true;
    }

    public class FraudPointModelWithOptions
    {
        public FraudPointModelWithOptions() {}

        public string ModelName { get; set; }
        public bool IncludeRiskIndices { get; set; } = true;
    }

    public class RequireExactMatch
    {
        public bool LastName { get; set; } = false;
        public bool FirstName { get; set; } = false;
        public bool FirstNameAllowNickname { get; set; } = true;
        public bool Address { get; set; } = false;
        public bool HomePhone { get; set; } = false;
        public bool Ssn { get; set; } = false;
        public bool DriverLicense { get; set; } = false;
    }

    public class InstantIdOption
    {
        public InstantIdOption() { }

        public string[] WatchLists { get; set; }
        public bool IncludeClOverride { get; set; } = true;
        public bool IncludeMsOverride { get; set; } = true;
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        public bool PoBoxCompliance { get; set; } = true;
        public InstantIdModelsFp IncludeModels { get; set; }
        public string RedFlagsReport { get; set; } = "Version1";
        public string GlobalWatchlistThreshold { get; set; }
        public DateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        public bool IncludeAllRiskIndicators { get; set; } = true;
        public RequireExactMatch RequireExactMatch { get; set; }
        public string CustomCviModelName { get; set; }
        public string LastSeenThreshold { get; set; }
        public bool IncludeMiOverride { get; set; } = true;
        public OptionsForCviCalculation CviCalculationOptions { get; set; }
        public string InstantIdVersion { get; set; }
        public bool IncludeDeliveryPointBarcode { get; set; } = true;
    }

    public class InstantIdModelsFp
    {
        public bool Thindex { get; set; }
        public StudentDefenderModel StudentDefenderModel { get; set; }
        public ModelRequest[] ModelRequests { get; set; }
        public FraudPointModelWithOptions FraudPointModel { get; set; }
    }

    public class StudentDefenderModel
    {
        public bool StudentDefender { get; set; }
        public bool IsStudentApplicant { get; set; }
    }
}