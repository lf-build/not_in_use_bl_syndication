﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.LexisNexis.Api
{
    public class Settings
    {
        public static string ServiceName { get; } = "lexisnexis";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub",$"{Prefix}_EVENTHUB_PORT");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT_HOST", "tenant", $"{Prefix}_TENANT_PORT");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE_HOST", "lookupservice", $"{Prefix}_LOOKUPSERVICE_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

    }
}