﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    [Route("/criminalrecord")]
    public class CriminalRecordController : ExtendedController
    {
        public CriminalRecordController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        [HttpPost("{entityType}/{entityId}/criminalrecord-search")]
        public async Task<IActionResult> CriminalSearch(string entityType, string entityId, [FromBody]CriminalRecordSearchRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.CriminalSearch(entityType, entityId, request)));
        }

        [HttpPost("{entityType}/{entityId}/criminalrecord-report")]
        public async Task<IActionResult> CriminalReport(string entityType, string entityId, [FromBody]CriminalRecordReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.CriminalRecordReport(entityType, entityId, request)));
        }
    }
}
