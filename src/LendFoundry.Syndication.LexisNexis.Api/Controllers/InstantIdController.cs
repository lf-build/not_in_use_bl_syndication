﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.InstantId;
using LendFoundry.SyndicationStore.Events;
using Microsoft.AspNet.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Lookup;
namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    [Route("/")]
    public class InstantIdController : ExtendedController
    {
        public InstantIdController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        [HttpPost("{entityType}/{entityId}/instant-id")]
        public async Task<IActionResult> InstantIdVerification(string entityType, string entityId, [FromBody]InstantIdRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    entityType = EnsureEntityType(entityType);
                    if (string.IsNullOrEmpty(entityId))
                        throw new ArgumentNullException(nameof(entityId));
                    if (request == null)
                        throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                    var verificationResult = Service.GetInstantIdVerificationResult(request);
                    if (verificationResult != null)
                        await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                        {
                            Data = verificationResult,
                            EntityId = entityId,
                            EntityType = entityType,
                            Name = $"{ Settings.ServiceName }.{ActionContext.RouteData.Values["controller"].ToString()}"
                        });

                    return new HttpOkObjectResult(verificationResult);
                }
                catch (LexisNexisException ex)
                {
                    Logger.Error($"The method GetInstantIdVerificationResult({request}) raised an error: {ex.Message}");
                    throw new NotFoundException($"Information not found for this request");
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    throw ex;
                }
            });
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}