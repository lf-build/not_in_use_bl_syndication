﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Api.Controllers
{
    [Route("/bankruptcy")]
    public class BankruptcyController : ExtendedController
    {
        public BankruptcyController(ILexisNexisService service, ILogger logger, IEventHubClient eventHubClient, ILookupService lookup) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Lookup = lookup;
        }

        private ILexisNexisService Service { get; }

        private IEventHubClient EventHubClient { get; }

        private ILookupService Lookup { get; }

        [HttpPost("{entityType}/{entityId}/bankruptcy-search")]
        public async Task<IActionResult> BankruptcySearch(string entityType, string entityId, [FromBody]SearchBankruptcyRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.FcraBankruptcySearch(entityType, entityId, request)));
        }

        [HttpPost("{entityType}/{entityId}/bankruptcy-fcra-report")]
        public async Task<IActionResult> FcraBankruptcyReport(string entityType, string entityId, [FromBody]BankruptcyFcraReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.FcraBankruptcyReport(entityType, entityId, request)));
        }

        [HttpPost("{entityType}/{entityId}/bankruptcy-report")]
        public async Task<IActionResult> BankruptcyReport(string entityType, string entityId, [FromBody]BankruptcyReportRequest request)
        {
            return await ExecuteAsync(async () => Ok(await Service.BankruptcyReport(entityType, entityId, request)));
        }
    }
}
