﻿using LendFoundry.Syndication.LexisNexis.FraudPoint;

namespace LendFoundry.Syndication.LexisNexis.Api.ViewModels
{
    public class FraudPointRequest
    {
        public Name Name { get; set; }
        public Address Address { get; set; }
        public Address Address2 { get; set; }
        public Date DateOfBirth { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Phone10 { get; set; }
        public string WorkPhone { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string Email { get; set; }
        public ChannelIdentifier Channel { get; set; }
        public string Income { get; set; }
        public OwnRent OwnOrRent { get; set; }
        public string LocationIdentifier { get; set; }
        public string OtherApplicationIdentifier1 { get; set; }
        public string OtherApplicationIdentifier2 { get; set; }
        public string OtherApplicationIdentifier3 { get; set; }
        public TimeStamp ApplicationDateTime { get; set; }
    }
}