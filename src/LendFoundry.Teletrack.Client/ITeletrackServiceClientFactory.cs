﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Teletrack;

namespace LendFoundry.Teletrack.Client
{
    public interface ITeletrackServiceClientFactory
    {
        ITeletrackService Create(ITokenReader reader);
    }
}
