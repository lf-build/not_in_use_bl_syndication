﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Teletrack.Client
{
    public static  class ITeletrackServiceClientExtension
    {
        public static IServiceCollection AddTeletrackService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITeletrackServiceClientFactory>(p => new TeletrackServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITeletrackServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
