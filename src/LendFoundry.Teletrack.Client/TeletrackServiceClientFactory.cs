﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Teletrack;
using System;

namespace LendFoundry.Teletrack.Client
{
    public class TeletrackServiceClientFactory : ITeletrackServiceClientFactory
    {
        public TeletrackServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;

        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITeletrackService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new TeletrackService(client);
        }
    }
}
