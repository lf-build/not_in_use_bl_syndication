﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Teletrack;
using LendFoundry.Syndication.Teletrack.Inquiry;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response;

using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.Teletrack.Client
{
    public class TeletrackService : ITeletrackService
    {
        public TeletrackService(IServiceClient client)
        {
            Client = client;

        }
      
        private IServiceClient Client { get; }

        public async Task<GetDataResponse> GetData(string entityType, string entityId, IInquiryRequest request)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/report", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<GetDataResponse>(restrequest);
        }
    }
}
