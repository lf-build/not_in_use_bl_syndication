﻿
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Api
{
   
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors
       
        public ApiController(ICriskcoService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

      
        private ICriskcoService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// End point call the method to Search Account details
        /// </summary>
        /// <param name="entityType">type of Entity</param>
        /// <param name="entityId">entity Id</param>
        /// <param name="businessId">businessId</param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/{businessId}/Criskco")]
        public async Task<IActionResult> GetApproveBusiness(string entityType, string entityId, string businessId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetApproveBusiness(entityType, entityId, businessId)));
            });
        }

        #endregion Public Methods
    }
}