﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

using LendFoundry.Syndication.Criskco.Proxy;
using LendFoundry.Clients.DecisionEngine;

namespace LendFoundry.Syndication.Criskco.Api
{
    public class Startup    {
      
        public Startup(IHostingEnvironment env)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
          
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddCors();
            services.AddMvc().AddLendFoundryJsonOptions();
            // interface implements
            services.AddConfigurationService<CriskcoConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddTransient<ICriskcoCredentialConfiguration, CriskcoCredentialConfiguration>();
            services.AddTransient<ICriskcoConfiguration>(provider => provider.GetRequiredService<IConfigurationService<CriskcoConfiguration>>().Get());           
            services.AddTransient<ICriskcoService, CriskcoService>();
            services.AddTransient<ICriskcoProxy, CriskcoProxy>();
            services.AddTransient<ICriskcoListener, CriskcoListener>();
            services.AddTransient<ICriskcoServiceFactory, CriskcoServiceFactory>();
            services.AddTransient<ICriskcoProxyFactory, CriskcoProxyFactory>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseCriskcoListener();
            app.UseHealthCheck();
        }
    }
}
