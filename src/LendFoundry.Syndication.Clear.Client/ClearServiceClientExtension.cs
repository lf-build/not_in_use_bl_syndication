﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Syndication.Clear.Client
{
    public static  class ClearServiceClientExtension
    {
        public static IServiceCollection AddClearService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IClearServiceClientFactory>(p => new ClearServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IClearServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
