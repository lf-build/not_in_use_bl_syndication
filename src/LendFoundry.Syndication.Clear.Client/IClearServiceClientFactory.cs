﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Clear;

namespace LendFoundry.Syndication.Clear.Client
{
    public interface IClearServiceClientFactory
    {
        IClearService Create(ITokenReader reader);
    }
}
