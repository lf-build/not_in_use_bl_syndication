﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Clear.Request;
using RestSharp;
using System.Threading.Tasks;
using LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult;
using LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse;
using LendFoundry.Syndication.Clear.Response.Models.PersonSearch;
using LendFoundry.Syndication.Clear.Response.Models.BusinessSearch;

namespace LendFoundry.Syndication.Clear.Client
{
    public class ClearService : IClearService
    {
        public ClearService(IServiceClient client)
        {
            Client = client;

        }
      
        private IServiceClient Client { get; }
        public async Task<IPersonSearchResponse> SearchPerson(string entityType, string entityId, IPersonSearchRequest request)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/person/search", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<PersonSearchResponse>(restrequest);
        }
        public async Task<IPersonReportDetails> GetPersonReport(string entityType, string entityId, string groupId)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/person/report/{groupId}", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddUrlSegment("groupId", groupId);
            return await Client.ExecuteAsync<PersonReportDetails>(restrequest);
        }
      
        public async Task<IBusinessSearchResponse> SearchBusiness(string entityType, string entityId, IBusinessSearchRequest request)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/business/search", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddJsonBody(request);
            return await Client.ExecuteAsync<BusinessSearchResponse>(restrequest);
        }
        public async Task<IBusinessReportDetails> GetBusinessReport(string entityType, string entityId, string groupId)
        {

            var restrequest = new RestRequest("/{entitytype}/{entityid}/business/report/{groupId}", Method.POST);
            restrequest.AddUrlSegment("entitytype", entityType);
            restrequest.AddUrlSegment("entityid", entityId);
            restrequest.AddUrlSegment( "groupId",groupId);
            return await Client.ExecuteAsync<BusinessReportDetails>(restrequest);
        }
    }
}
