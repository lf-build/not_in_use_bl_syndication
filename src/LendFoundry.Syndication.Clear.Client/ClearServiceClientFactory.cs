﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Clear.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Syndication.Clear;

namespace LendFoundry.Syndication.Clear.Client
{
    public class ClearServiceClientFactory : IClearServiceClientFactory
    {
        public ClearServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;

        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IClearService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ClearService(client);
        }
    }
}
