﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using LendFoundry.Foundation.Logging;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.IDAnalytics.Proxy;
using LendFoundry.Syndication.IDAnalytics;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Tenant.Client;

namespace LendFoundry.IDAnalytics.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // interface implements
            services.AddConfigurationService<IDAnalyticsConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddTransient<IIDAnalyticsConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<IDAnalyticsConfiguration>>().Get();
                return configuration;
            });
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IIDAnalyticsProxy, IDAnalyticsProxy>();
            services.AddTransient<IIDAnalyticsService, IDAnalyticsService>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseHealthCheck();

        }
    }
}
