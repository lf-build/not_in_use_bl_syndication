﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.IDAnalytics;
using LendFoundry.Syndication.IDAnalytics.Proxy;
using Microsoft.AspNet.Mvc;
using System;

using System.Threading.Tasks;

namespace LendFoundry.IDAnalytics.Api
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IIDAnalyticsService iDAnalyticsService)
        {
            IDAnalyticsService = iDAnalyticsService;
        }

        private IIDAnalyticsService IDAnalyticsService { get; }

        [HttpPost("{entityType}/{entityId}/report")]
        public async Task<IActionResult> GetReport(string entityType,string entityId,[FromBody] IDAnalyticsRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                  
                    return Ok(await Task.Run(() => IDAnalyticsService.GetReport(entityType, entityId, request)));
                    
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}