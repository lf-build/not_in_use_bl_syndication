﻿
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Criskco;
using LendFoundry.Syndication.Criskco.Response;
using System.Threading.Tasks;
using RestSharp;
using System;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Criskco.Client
{
    public class CriskcoService : ICriskcoService
    {

        #region Public Constructors

        public CriskcoService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<ICriskcoResponse> GetApproveBusiness(string entityType, string entityId, string bussinessId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/{businessId}/Criskco", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddUrlSegment("bussinessId", bussinessId);
            return await Client.ExecuteAsync<CriskcoResponse>(request);
        }
        public Task ProcessEvent(EventInfo eventInfo)
        {
            throw new NotImplementedException();
        }
        #endregion Public Methods
    }
}
