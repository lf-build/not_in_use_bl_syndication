﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Criskco;

namespace LendFoundry.Criskco.Client
{
    public interface ICriskcoServiceClientFactory
    {
        ICriskcoService Create(ITokenReader reader);
    }
}
