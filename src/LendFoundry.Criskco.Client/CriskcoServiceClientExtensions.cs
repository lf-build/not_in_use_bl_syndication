﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Criskco.Client
{
    public static class CriskcoServiceClientExtensions
    {

        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        public static IServiceCollection AddCriskcoService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICriskcoServiceClientFactory>(p => new CriskcoServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICriskcoServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        #endregion Public Methods
    }
}
