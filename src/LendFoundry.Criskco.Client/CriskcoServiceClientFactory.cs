﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Criskco;
using System;

namespace LendFoundry.Criskco.Client
{
    public class CriskcoServiceClientFactory : ICriskcoServiceClientFactory
    {

        #region Public Constructors

        public CriskcoServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }

        #endregion Private Properties

        #region Public Methods

        public ICriskcoService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new CriskcoService(client);
        }

        #endregion Public Methods
    }
}
