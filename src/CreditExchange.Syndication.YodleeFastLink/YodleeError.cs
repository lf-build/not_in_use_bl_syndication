﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink
{
    public class YodleeError: IYodleeError
    {
        [JsonProperty(PropertyName = "errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty(PropertyName = "referenceCode")]
        public string ReferenceCode { get; set; }
    }
}
