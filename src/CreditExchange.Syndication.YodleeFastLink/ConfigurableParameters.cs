﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public class ConfigurableParameters: IConfigurableParameters
    {
        public string UserCredentialsInstanceType { get; set; } = "com.yodlee.ext.login.PasswordCredentials";
        public string UserProfileInstanceType { get; set; } = "com.yodlee.core.usermanagement.UserProfile";
        public string PreferredCurrency { get; set; } = "PREFERRED_CURRENCY~USD";
        public string PreferredDateFormate { get; set; } = "PREFERRED_DATE_FORMAT~MM/dd/yyyy";
        public string FastLinkAppId { get; set; } = "10003600";
        public string FastLinkLaunchForm { get; set; }
    }
}
