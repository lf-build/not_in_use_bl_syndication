﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IConfigurableParameters
    {
        string UserCredentialsInstanceType { get; set; }
        string UserProfileInstanceType { get; set; }
        string PreferredCurrency { get; set; }
        string PreferredDateFormate { get; set; }
        string FastLinkAppId { get; set; }
        string FastLinkLaunchForm { get; set; }
    }
}
