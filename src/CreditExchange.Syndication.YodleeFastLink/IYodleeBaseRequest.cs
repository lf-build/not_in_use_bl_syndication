﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IYodleeBaseRequest
    {
        string CobrandToken { get; set; }
        string UserSessionToken { get; set; }
    }
}
