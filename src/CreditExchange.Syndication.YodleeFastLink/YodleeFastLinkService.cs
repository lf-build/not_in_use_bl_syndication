﻿using System;
using CreditExchange.Syndication.YodleeFastLink.Proxy;
using LendFoundry.EventHub.Client;
using System.Threading.Tasks;
using CreditExchange.Syndication.YodleeFastLink.Response;
using CreditExchange.Syndication.YodleeFastLink.Request;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System.Linq;
using CreditExchange.Syndication.YodleeFastLink.Events;
using RestSharp;

namespace CreditExchange.Syndication.YodleeFastLink
{
    public class YodleeFastLinkService : IYodleeFastLinkService
    {
        public YodleeFastLinkService(IYodleeFastLinkProxy proxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            Proxy = proxy;
            Lookup = lookup;
            EventHub = eventHub;
        }
        private IYodleeFastLinkProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }

        public async Task<ICobrandResponse> GetCobrandToken(string entityType, string entityId)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var response = await Task.Run(() => Proxy.GetCobrandToken());
                var result = new CobrandResponse(response);
                await EventHub.Publish(new YodleeFastLinkGetCobrandTokenRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = null,
                    ReferenceNumber = referencenumber
                });
                return result;
            }
          
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetCobrandTokenRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = null,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public async Task<IUserAuthenticateResponse> RegisterUser(string entityType, string entityId, UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (userAuthenticateRequest == null)
                throw new ArgumentNullException(nameof(userAuthenticateRequest));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.CobrandToken))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Username))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Username));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Password))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Password));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Email))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Email));
            try
            {
                var request = new Proxy.Request.UserRegisterNAuthenticateRequest(userAuthenticateRequest);
                var response = await Task.Run(() => Proxy.RegisterUser(request));
                var result = new UserAuthenticateResponse(response);
                await EventHub.Publish(new YodleeFastLinkRegisterUserRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = userAuthenticateRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return result;
            }
            
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkRegisterUserRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = userAuthenticateRequest,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public async Task<IUserAuthenticateResponse> AuthenticateUser(string entityType, string entityId, UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (userAuthenticateRequest == null)
                throw new ArgumentNullException(nameof(userAuthenticateRequest));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.CobrandToken))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Username))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Username));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Password))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Password));
            try
            {
                var request = new Proxy.Request.UserRegisterNAuthenticateRequest(userAuthenticateRequest);
                var response = await Task.Run(() => Proxy.AuthenticateUser(request));
                var result = new UserAuthenticateResponse(response);
                await EventHub.Publish(new YodleeFastLinkAuthenticateUserRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = userAuthenticateRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkAuthenticateUserRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = userAuthenticateRequest,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public async Task<IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId, FastlinkAccessTokenRequest fastlinkAccessTokenRequest)
        {
            entityType = EnsureEntityType(entityType);


            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (fastlinkAccessTokenRequest == null)
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.CobrandToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.UserSessionToken));
            try
            {


                var request = new Proxy.Request.FastlinkAccessTokenRequest(fastlinkAccessTokenRequest);
                var response = await Task.Run(() => Proxy.GetFastlinkAccessToken(request));
                var referencenumber = Guid.NewGuid().ToString("N");
                var result = new FastlinkAccessTokenResponse(response);
                await EventHub.Publish(new YodleeFastLinkGetFastlinkAccessTokenRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = fastlinkAccessTokenRequest,
                    ReferenceNumber = referencenumber
                });
                return result;
            }
           
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetFastlinkAccessTokenRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = fastlinkAccessTokenRequest,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public async Task<IRestResponse> GetFalstLinkUrl(string entityType, string entityId, FastLinkUrlRequest fastlinkReuest)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (fastlinkReuest == null)
                throw new ArgumentNullException(nameof(fastlinkReuest));
            try
            {
                var request = new Proxy.Request.FastLinkUrlRequest(fastlinkReuest);
                var response = await Task.Run(() => Proxy.GetFalstLinkUrl(request));
                await EventHub.Publish(new YodleeFastLinkGetFalstLinkUrlRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = fastlinkReuest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });

                IRestResponse objResponse = new RestResponse();
                objResponse.Content = response;
                objResponse.ResponseStatus = ResponseStatus.Completed;

                return objResponse;
            }
           
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetFalstLinkUrlRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = fastlinkReuest,
                    ReferenceNumber = null
                });
                throw new Exception(exception.Message);
            }
        }
        public async Task<IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId, GetBankAccountsRequest getBankAccountsRequest)
        {
            entityType = EnsureEntityType(entityType);


            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (getBankAccountsRequest == null)
                throw new ArgumentNullException(nameof(getBankAccountsRequest));
            if (string.IsNullOrWhiteSpace(getBankAccountsRequest.CobrandToken))
                throw new ArgumentNullException(nameof(getBankAccountsRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(getBankAccountsRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(getBankAccountsRequest.UserSessionToken));
            try
            {
                var referencenumber = Guid.NewGuid().ToString("N");
                var request = new Proxy.Request.GetBankAccountsRequest(getBankAccountsRequest);
                var response = await Task.Run(() => Proxy.GetUserBankAccounts(request));
                var result = new GetBankAccountResponse(response);
                await EventHub.Publish(new YodleeFastLinkGetUserBankAccountRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = getBankAccountsRequest,
                    ReferenceNumber = referencenumber
                });
                return result;
            }
            catch (YodleeException exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetUserBankAccountRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = getBankAccountsRequest,
                    ReferenceNumber = null
                });
                throw new YodleeException(exception.Message);
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetUserBankAccountRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = getBankAccountsRequest,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public async Task<ITransactionResponse> GetAccountTransactions(string entityType, string entityId, GetTransactionRequest getTransactionRequest)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (getTransactionRequest == null)
                throw new ArgumentNullException(nameof(getTransactionRequest));
            if (string.IsNullOrWhiteSpace(getTransactionRequest.CobrandToken))
                throw new ArgumentNullException(nameof(getTransactionRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(getTransactionRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(getTransactionRequest.UserSessionToken));
            try
            {

                var request = new Proxy.Request.GetTransactionRequest(getTransactionRequest);
                var response = await Task.Run(() => Proxy.GetAccountTransactions(request));
                var result = new TransactionResponse(response);
                await EventHub.Publish(new YodleeFastLinkGetAccountTransactionRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = getTransactionRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new YodleeFastLinkGetAccountTransactionRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = getTransactionRequest,
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

    }
}
