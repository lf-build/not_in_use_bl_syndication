﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.YodleeFastLink.Events
{
    public class YodleeFastLinkAuthenticateUserRequestedFail : SyndicationCalledEvent
    {
    }
}