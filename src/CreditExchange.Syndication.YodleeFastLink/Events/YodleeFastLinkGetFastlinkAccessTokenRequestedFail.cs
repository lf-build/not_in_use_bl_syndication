﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.YodleeFastLink.Events
{
    public class YodleeFastLinkGetFastlinkAccessTokenRequestedFail : SyndicationCalledEvent
    {
    }
}