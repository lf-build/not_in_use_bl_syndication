﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IYodleeFastLinkConfiguration
    {
        string CobrandUsername { get; set; }
        string CobrandPassword { get; set; }
        IConfigurableParameters ConfigurableParameters { get; set; }
        IServiceUrl ServiceUrl { get; set; }
    }
}
