﻿namespace CreditExchange.Syndication.YodleeFastLink.Request
{
    public interface IFastLinkUrlRequest:IYodleeBaseRequest
    {
        string FastlinkAccessToken { get; set; }
        FastLinkFlows FastLinkFlow { get; set; }
        string BankSearchKeyWord { get; set; }
        long SiteId { get; set; }
        long SiteAccountId { get; set; }
        string CallBackUrl { get; set; }
    }
}
