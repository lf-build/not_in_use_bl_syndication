﻿using System;

namespace CreditExchange.Syndication.YodleeFastLink.Request
{
    public interface IGetTransactionRequest:IYodleeBaseRequest
    {
        TransactionType? BaseType { get; set; }
        string AccountIds { get; set; }
        DateTime? FromDate { get; set; }
        DateTime? ToDate { get; set; }
        TransactionCategory? Category { get; set; }
        int? SkipTransactions { get; set; }
        int? NumberOfTransactions { get; set; }
    }
}

