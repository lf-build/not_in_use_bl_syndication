﻿namespace CreditExchange.Syndication.YodleeFastLink.Request
{
    public interface IUserRegisterNAuthenticateRequest
    {
        string Username { get; set; }
        string Password { get; set; }
        string Email { get; set; }
        string CobrandToken { get; set; }
    }
}
