﻿namespace CreditExchange.Syndication.YodleeFastLink.Request
{
    public class FastLinkUrlRequest : YodleeBaseRequest, IFastLinkUrlRequest
    {
        public string FastlinkAccessToken { get; set; }
        public FastLinkFlows FastLinkFlow { get; set; }
        public string BankSearchKeyWord { get; set; }
        public long SiteId { get; set; }
        public long SiteAccountId { get; set; }
        public string CallBackUrl { get; set; }
    }  
}
