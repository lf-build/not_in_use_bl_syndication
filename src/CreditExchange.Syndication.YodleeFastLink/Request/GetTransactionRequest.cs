﻿using System;

namespace CreditExchange.Syndication.YodleeFastLink.Request
{
    public class GetTransactionRequest: YodleeBaseRequest,IGetTransactionRequest
    {
        public TransactionType? BaseType { get; set; }
        public string AccountIds { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public TransactionCategory? Category { get; set; }
        public int? SkipTransactions { get; set; }
        public int? NumberOfTransactions { get; set; }
    }
}
