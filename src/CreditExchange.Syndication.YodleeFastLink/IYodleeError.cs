﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IYodleeError
    {
        string ErrorCode { get; set; }
        string ErrorMessage { get; set; }
        string ReferenceCode { get; set; }
    }
}
