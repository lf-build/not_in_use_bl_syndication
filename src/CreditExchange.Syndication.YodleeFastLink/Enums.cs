﻿namespace CreditExchange.Syndication.YodleeFastLink
{
    public enum TransactionCategory
    {
        UNCATEGORIZE,
        INCOME,
        TRANSFER,
        EXPENSE,
        DEFERRED_COMPENSATION
    }

    public enum TransactionType
    {
        DEBIT,
        CREDIT
    }
    public enum FastLinkFlows
    {
        normal,
        add,
        edit,
        refresh
    }
}
