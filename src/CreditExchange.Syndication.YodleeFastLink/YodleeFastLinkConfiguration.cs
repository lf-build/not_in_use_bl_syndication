﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink
{
    public class YodleeFastLinkConfiguration : IYodleeFastLinkConfiguration
    {
        public string CobrandUsername { get; set; } = "sbCobjsisodiya";
        public string CobrandPassword { get; set; } = "3faa4478-68ec-4d86-a6a6-786eb0853a49";
        [JsonConverter(typeof(InterfaceConverter<IConfigurableParameters, ConfigurableParameters>))]
        public IConfigurableParameters ConfigurableParameters { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IServiceUrl, ServiceUrl>))]
        public IServiceUrl ServiceUrl { get; set; }
    }
}
