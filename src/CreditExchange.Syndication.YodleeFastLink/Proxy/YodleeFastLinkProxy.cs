﻿using System;
using CreditExchange.Syndication.YodleeFastLink.Proxy.Request;
using System.Reflection;
using Newtonsoft.Json;
using RestSharp;
using CreditExchange.Syndication.YodleeFastLink.Proxy.Response;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy
{
    public class YodleeFastLinkProxy : IYodleeFastLinkProxy
    {
        public YodleeFastLinkProxy(IYodleeFastLinkConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (configuration.ConfigurableParameters == null)
                throw new ArgumentNullException(nameof(configuration.ConfigurableParameters));
            if (configuration.ServiceUrl == null)
                throw new ArgumentNullException(nameof(configuration.ServiceUrl));
            Configuration = configuration;
        }
        private IYodleeFastLinkConfiguration Configuration { get; }
        public ICobrandResponse GetCobrandToken()
        {
            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.CobrandLogin.TrimEnd('/'), Method.POST);
            request.AddParameter("cobrandLogin", Configuration.CobrandUsername);
            request.AddParameter("cobrandPassword", Configuration.CobrandPassword);
            var response = ExecuteRequest<CobrandResponse>(client, request);
            return response;
        }
        public IUserAuthenticateResponse RegisterUser(UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            if (userAuthenticateRequest == null)
                throw new ArgumentNullException(nameof(userAuthenticateRequest));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.CobrandToken))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Username))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Username));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Password))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Password));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Email))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Email));

            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.UserRegistration.TrimEnd('/'), Method.POST);
            AddHeaderWithCobrandToken(ref request, userAuthenticateRequest.CobrandToken);
            AddParamToRequest(ref request, userAuthenticateRequest);
            var response = ExecuteRequest<UserAuthenticateResponse>(client, request);
            return response;
        }
        public IUserAuthenticateResponse AuthenticateUser(UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            if (userAuthenticateRequest == null)
                throw new ArgumentNullException(nameof(userAuthenticateRequest));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.CobrandToken))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Username))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Username));
            if (string.IsNullOrWhiteSpace(userAuthenticateRequest.Password))
                throw new ArgumentNullException(nameof(userAuthenticateRequest.Password));

            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.UserLogin.TrimEnd('/'), Method.POST);
            AddHeaderWithCobrandToken(ref request, userAuthenticateRequest.CobrandToken);
            AddParamToRequest(ref request, userAuthenticateRequest);
            var response = ExecuteRequest<UserAuthenticateResponse>(client, request);
            return response;
        }
        public IFastlinkAccessTokenResponse GetFastlinkAccessToken(FastlinkAccessTokenRequest fastlinkAccessTokenRequest)
        {
            if (fastlinkAccessTokenRequest == null)
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.CobrandToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(fastlinkAccessTokenRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(fastlinkAccessTokenRequest.UserSessionToken));

            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.AccessToken.TrimEnd('/'), Method.GET);
            AddHeaderWithUserAndCobrandToken(ref request, fastlinkAccessTokenRequest.CobrandToken, fastlinkAccessTokenRequest.UserSessionToken);
            request.AddParameter("appIds", Configuration.ConfigurableParameters.FastLinkAppId);
            var response = ExecuteRequest<FastlinkAccessTokenResponse>(client, request);
            return response;
        }
        public string GetFalstLinkUrl(FastLinkUrlRequest fastlinkReuest)
        {
            if (fastlinkReuest == null)
                throw new ArgumentNullException(nameof(fastlinkReuest));
            StringBuilder additionalParam = new StringBuilder();
            FastlinkGenerationModel generateFLModel = new FastlinkGenerationModel();
            if (!string.IsNullOrWhiteSpace(fastlinkReuest.BankSearchKeyWord))
                AddAdditionalParam(ref additionalParam, "keyword", fastlinkReuest.BankSearchKeyWord);
            if (fastlinkReuest.FastLinkFlow != FastLinkFlows.normal)
            {
                if (fastlinkReuest.FastLinkFlow == FastLinkFlows.add)
                {
                    if (fastlinkReuest.SiteId == 0)
                        throw new ArgumentNullException($"{nameof(fastlinkReuest.SiteId)} is required for add flow");
                    AddAdditionalParam(ref additionalParam, "siteId", fastlinkReuest.SiteId.ToString());
                    AddAdditionalParam(ref additionalParam, "flow", fastlinkReuest.FastLinkFlow.ToString());
                }
                if (fastlinkReuest.FastLinkFlow == FastLinkFlows.edit || fastlinkReuest.FastLinkFlow == FastLinkFlows.refresh)
                {
                    if (fastlinkReuest.SiteAccountId == 0)
                        throw new ArgumentNullException($"{nameof(fastlinkReuest.SiteAccountId)} is required for edit or refresh flow");
                    AddAdditionalParam(ref additionalParam, "siteAccountId", fastlinkReuest.SiteAccountId.ToString());
                    AddAdditionalParam(ref additionalParam, "flow", fastlinkReuest.FastLinkFlow.ToString());
                }
            }
            if (!string.IsNullOrWhiteSpace(fastlinkReuest.CallBackUrl))
            {
                AddAdditionalParam(ref additionalParam, "callback", fastlinkReuest.CallBackUrl);
                generateFLModel.IsRedirect = true;
            }
            generateFLModel.FastLinkBaseUrl = Configuration.ServiceUrl.FastLinkBaseUrl;
            generateFLModel.AppId = Configuration.ConfigurableParameters.FastLinkAppId;
            generateFLModel.UserSessionToken = fastlinkReuest.UserSessionToken;
            generateFLModel.FastlinkAccessToken = fastlinkReuest.FastlinkAccessToken;
            generateFLModel.ExtraParams = additionalParam.ToString();
            return ReplaceFastLinkHtmlValue(generateFLModel);
        }
        public IGetBankAccountResponse GetUserBankAccounts(GetBankAccountsRequest getBankAccountsRequest)
        {
            if (getBankAccountsRequest == null)
                throw new ArgumentNullException(nameof(getBankAccountsRequest));
            if (string.IsNullOrWhiteSpace(getBankAccountsRequest.CobrandToken))
                throw new ArgumentNullException(nameof(getBankAccountsRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(getBankAccountsRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(getBankAccountsRequest.UserSessionToken));

            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.GetUserAccounts.TrimEnd('/'), Method.GET);
            AddHeaderWithUserAndCobrandToken(ref request, getBankAccountsRequest.CobrandToken, getBankAccountsRequest.UserSessionToken);
            return ExecuteRequest<GetBankAccountResponse>(client, request);
        }
        public ITransactionResponse GetAccountTransactions(GetTransactionRequest getTransactionRequest)
        {
            if (getTransactionRequest == null)
                throw new ArgumentNullException(nameof(getTransactionRequest));
            if (string.IsNullOrWhiteSpace(getTransactionRequest.CobrandToken))
                throw new ArgumentNullException(nameof(getTransactionRequest.CobrandToken));
            if (string.IsNullOrWhiteSpace(getTransactionRequest.UserSessionToken))
                throw new ArgumentNullException(nameof(getTransactionRequest.UserSessionToken));
            var client = new RestClient(Configuration.ServiceUrl.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.ServiceUrl.GetAccountTransactions.TrimEnd('/'), Method.GET);
            if (getTransactionRequest.FromDate.HasValue && getTransactionRequest.ToDate.HasValue)
            {
                request.AddParameter("fromDate", getTransactionRequest.FromDate.Value.ToString("yyyy-MM-dd"));
                request.AddParameter("toDate", getTransactionRequest.ToDate.Value.ToString("yyyy-MM-dd"));
            }
            AddParamToRequest(ref request, getTransactionRequest);
            AddHeaderWithUserAndCobrandToken(ref request, getTransactionRequest.CobrandToken, getTransactionRequest.UserSessionToken);
            var response = ExecuteRequest<TransactionResponse>(client, request);
            return response;
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new YodleeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new YodleeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new YodleeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new YodleeException(response.Content);
            if (response.Content.Contains("errorCode"))
            {
                var error = JsonConvert.DeserializeObject<YodleeError>(response.Content);
                throw new YodleeException(
                   $"Service call failed. Status {error.ErrorCode}. Response: {error.ErrorMessage ?? ""}");
            }
            if (response.StatusCode != HttpStatusCode.OK)
                throw new YodleeException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.Content ?? ""}");
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }
        private void AddParamToRequest<T>(ref IRestRequest request, T model, bool readJsonPropertyName = true, string dateTimeFormate = "YYYY-MM-DD") where T : class
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            PropertyInfo[] propertiesList = typeof(T).GetProperties();
            foreach (var prop in propertiesList)
            {
                if (prop.GetValue(model) != null && prop.Name != "CobrandToken" && prop.Name != "UserSessionToken")
                {
                    if (prop.PropertyType == typeof(int) && !prop.GetValue(model).Equals(0))
                    {
                        if (readJsonPropertyName)
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        else request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                    if (prop.PropertyType == typeof(string) && !string.IsNullOrWhiteSpace(prop.GetValue(model).ToString()))
                    {
                        if (readJsonPropertyName)
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        else
                            request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                    if (prop.PropertyType == typeof(bool) && prop.GetValue(model).Equals(true))
                    {
                        if (readJsonPropertyName)
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        else
                            request.AddParameter(prop.Name, prop.GetValue(model));
                    }

                    if (prop.PropertyType == typeof(TransactionCategory?) && !prop.GetValue(model).Equals(null))
                    {
                        if (readJsonPropertyName)
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        else
                            request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                    if (prop.PropertyType == typeof(TransactionType?) && !prop.GetValue(model).Equals(null))
                    {
                        if (readJsonPropertyName)
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        else
                            request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                }
            }
        }
        private void AddHeaderWithUserAndCobrandToken(ref IRestRequest request, string cobrandToken, string userSessionToken)
        {
            request.AddHeader("Authorization", "{userSession=" + userSessionToken + ",cobSession=" + cobrandToken + "}");
        }
        private void AddHeaderWithCobrandToken(ref IRestRequest request, string cobrandToken)
        {
            request.AddHeader("Authorization", "{cobSession=" + cobrandToken + "}");
        }
        private string ReplaceFastLinkHtmlValue(FastlinkGenerationModel generateFLModel)
        {
            if (generateFLModel == null)
                throw new ArgumentNullException(nameof(generateFLModel));
            string fastLinkHtml = Configuration.ConfigurableParameters.FastLinkLaunchForm;
            var regex = new Regex(@"(#{2})\S+(#{2})");
            var match = regex.Match(fastLinkHtml);
            while (match.Success)
            {
                var value = match.Value;
                var memberName = value.Trim('#');
                var propertyInfo = generateFLModel.GetType().GetProperty(memberName);
                var memberValue = propertyInfo.GetValue(generateFLModel);
                fastLinkHtml = fastLinkHtml.Replace(value, memberValue != null ? memberValue.ToString() : string.Empty);
                match = match.NextMatch();
            }
            return fastLinkHtml;
        }
        private void AddAdditionalParam(ref StringBuilder additionalParam, string parameterName, string parameterValue)
        {
            if (string.IsNullOrWhiteSpace(additionalParam.ToString()))
                additionalParam.Append($"{parameterName}={parameterValue}");
            else
                additionalParam.Append($"&{parameterName}={parameterValue}");
        }

    }
}
