﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy
{
    public class FastlinkGenerationModel
    {
        public string FastLinkBaseUrl { get; set; }
        public string AppId { get; set; }
        public string UserSessionToken { get; set; }
        public string FastlinkAccessToken { get; set; }
        public bool IsRedirect { get; set; }
        public string ExtraParams { get; set; }
    }
}
