﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy
{
    public class YodleeException: Exception
    {
        public string ErrorCode { get; set; }
        public YodleeException() { }
        public YodleeException(string message) : this(null, message, null)
        {

        }
        public YodleeException(string errorCode, string message) : this(errorCode, message, null)
        {

        }
        public YodleeException(string message, Exception innerException) : this(null, message, innerException)
        {

        }
        public YodleeException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        protected YodleeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
