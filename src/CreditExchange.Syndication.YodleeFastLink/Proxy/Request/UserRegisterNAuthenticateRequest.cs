﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class UserRegisterNAuthenticateRequest:IUserRegisterNAuthenticateRequest
    {
        public UserRegisterNAuthenticateRequest()
        {
        }

        public UserRegisterNAuthenticateRequest(YodleeFastLink.Request.IUserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            if (userAuthenticateRequest != null)
            {
                Username = userAuthenticateRequest.Username;
                Password = userAuthenticateRequest.Password;
                Email = userAuthenticateRequest.Email;
                CobrandToken = userAuthenticateRequest.CobrandToken;
            }
        }
        [JsonProperty(PropertyName = "loginName")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        public string CobrandToken { get; set; }
    }
}
