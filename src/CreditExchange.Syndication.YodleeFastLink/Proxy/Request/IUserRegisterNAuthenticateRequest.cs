﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public interface IUserRegisterNAuthenticateRequest
    {
        string Username { get; set; }
        string Password { get; set; }
        string CobrandToken { get; set; }
        string Email { get; set; }
    }
}
