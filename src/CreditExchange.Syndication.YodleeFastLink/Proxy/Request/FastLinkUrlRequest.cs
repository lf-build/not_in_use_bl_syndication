﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class FastLinkUrlRequest : YodleeBaseRequest, IFastLinkUrlRequest
    {
        public FastLinkUrlRequest()
        {
        }

        public FastLinkUrlRequest(YodleeFastLink.Request.IFastLinkUrlRequest fastLinkUrlRequest):base(fastLinkUrlRequest)
        {
            if (fastLinkUrlRequest != null)
            {
                FastlinkAccessToken = fastLinkUrlRequest.FastlinkAccessToken;
                FastLinkFlow = fastLinkUrlRequest.FastLinkFlow;
                BankSearchKeyWord = fastLinkUrlRequest.BankSearchKeyWord;
                SiteId = fastLinkUrlRequest.SiteId;
                SiteAccountId = fastLinkUrlRequest.SiteAccountId;
                CallBackUrl = fastLinkUrlRequest.CallBackUrl;
            }
        }
        public string FastlinkAccessToken { get; set; }
        public FastLinkFlows FastLinkFlow { get; set; }
        public string BankSearchKeyWord { get; set; }
        public long SiteId { get; set; }
        public long SiteAccountId { get; set; }
        public string CallBackUrl { get; set; }
    }  
}
