﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public interface IRegisterUserRequest
    {
        string Username { get; set; }
        string Password { get; set; }
        string CobrandToken { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleInitial { get; set; }
    }
}
