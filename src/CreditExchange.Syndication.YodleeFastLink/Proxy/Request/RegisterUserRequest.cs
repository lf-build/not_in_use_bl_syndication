﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class RegisterUserRequest : IRegisterUserRequest
    {
        [JsonProperty(PropertyName = "userCredentials.loginName")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "userCredentials.password")]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "cobSessionToken")]
        public string CobrandToken { get; set; }
        [JsonProperty(PropertyName = "userProfile.address1")]
        public string AddressLine1 { get; set; }
        [JsonProperty(PropertyName = "userProfile.address2")]
        public string AddressLine2 { get; set; }
        [JsonProperty(PropertyName = "userProfile.city")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "userProfile.country")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "userProfile.emailAddress")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "userProfile.firstName")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "userProfile.lastName")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "userProfile.middleInitial")]
        public string MiddleInitial { get; set; }
    }
}
