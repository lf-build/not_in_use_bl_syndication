﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class GetBankAccountsRequest : YodleeBaseRequest, IGetBankAccountsRequest
    {
        public GetBankAccountsRequest() { }

        public GetBankAccountsRequest(YodleeFastLink.Request.IGetBankAccountsRequest getBankAccountsRequest) : base(getBankAccountsRequest)
        {
        }
    }
}
