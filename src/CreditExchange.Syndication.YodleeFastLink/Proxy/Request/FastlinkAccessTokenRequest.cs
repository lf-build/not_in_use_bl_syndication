﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class FastlinkAccessTokenRequest : YodleeBaseRequest, IFastlinkAccessTokenRequest
    {
        public FastlinkAccessTokenRequest()
        {
        }

        public FastlinkAccessTokenRequest(YodleeFastLink.Request.IFastlinkAccessTokenRequest fastlinkAccessTokenRequest) : base(fastlinkAccessTokenRequest)
        {
        }
    }
}
