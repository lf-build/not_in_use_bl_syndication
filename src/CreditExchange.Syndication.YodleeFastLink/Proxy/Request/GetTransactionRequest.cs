﻿using System;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Request
{
    public class GetTransactionRequest : YodleeBaseRequest, IGetTransactionRequest
    {
        public GetTransactionRequest()
        {
        }

        public GetTransactionRequest(YodleeFastLink.Request.IGetTransactionRequest getTransactionRequest):base(getTransactionRequest)
        {
            if (getTransactionRequest != null)
            {
                BaseType = getTransactionRequest.BaseType;
                AccountIds = getTransactionRequest.AccountIds;
                FromDate = getTransactionRequest.FromDate;
                ToDate = getTransactionRequest.ToDate;
                Category = getTransactionRequest.Category;
                SkipTransactions = getTransactionRequest.SkipTransactions;
                NumberOfTransactions = getTransactionRequest.NumberOfTransactions;
            }
        }

        [JsonProperty(PropertyName = "baseType")]
        public TransactionType? BaseType { get; set; }
        [JsonProperty(PropertyName = "accountId")]
        public string AccountIds { get; set; }
        [JsonProperty(PropertyName = "fromDate")]
        public DateTime? FromDate { get; set; }
        [JsonProperty(PropertyName = "toDate")]
        public DateTime? ToDate { get; set; }
        [JsonProperty(PropertyName = "categoryType")]
        public TransactionCategory? Category { get; set; }
        [JsonProperty(PropertyName = "skip")]
        public int? SkipTransactions { get; set; }
        [JsonProperty(PropertyName = "top")]
        public int? NumberOfTransactions { get; set; }
    }
}
