﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public class TransactionResponse: ITransactionResponse
    {
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Transaction>, ITransaction>))]
        [JsonProperty("transaction")]
        public IList<ITransaction> Transactions { get; set; }
    }
}
