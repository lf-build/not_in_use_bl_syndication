﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public class GetBankAccountResponse: IGetBankAccountResponse
    {
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Account>, IAccount>))]
        [JsonProperty("account")]
        public IList<IAccount> Accounts { get; set; }
    }
}
