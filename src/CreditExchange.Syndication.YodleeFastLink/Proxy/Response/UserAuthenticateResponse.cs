﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public class UserAuthenticateResponse: IUserAuthenticateResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<User>))]
        [JsonProperty("user")]
        public IUser User { get; set; }
    }
}
