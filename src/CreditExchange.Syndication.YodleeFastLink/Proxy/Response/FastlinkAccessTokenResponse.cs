﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public class FastlinkAccessTokenResponse : IFastlinkAccessTokenResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<AccessTokenData>))]
        [JsonProperty("user")]
        public IAccessTokenData AccessTokenData { get; set; }
    }
}
