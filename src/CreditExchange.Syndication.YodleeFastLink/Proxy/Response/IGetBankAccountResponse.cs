﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public interface IGetBankAccountResponse
    {
        IList<IAccount> Accounts { get; set; }
    }
}
