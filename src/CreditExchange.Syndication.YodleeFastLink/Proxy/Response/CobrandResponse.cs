﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public class CobrandResponse : ICobrandResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<Session>))]
        [JsonProperty(PropertyName = "session")]
        public ISession Session { get; set; }
        [JsonProperty(PropertyName = "cobrandId")]
        public long CobrandId { get; set; }
        [JsonProperty(PropertyName = "applicationId")]
        public string ApplicationId { get; set; }
        [JsonProperty(PropertyName = "locale")]
        public string Locale { get; set; }
    }
}
