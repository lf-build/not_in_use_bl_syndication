﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public interface IUserAuthenticateResponse
    {
        IUser User { get; set; }
    }
}
