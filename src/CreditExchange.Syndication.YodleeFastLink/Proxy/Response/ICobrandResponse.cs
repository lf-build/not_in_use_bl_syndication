﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public interface ICobrandResponse
    {
         ISession Session { get; set; }
         long CobrandId { get; set; }
         string ApplicationId { get; set; }
         string Locale { get; set; }
    }
}
