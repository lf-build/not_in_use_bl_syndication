﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public interface IFastlinkAccessTokenResponse
    {
        IAccessTokenData AccessTokenData { get; set; }
    }
}
