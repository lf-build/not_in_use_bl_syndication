﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Models;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Response
{
    public interface ITransactionResponse
    {
        IList<ITransaction> Transactions { get; set; }
    }
}
