﻿using CreditExchange.Syndication.YodleeFastLink.Proxy.Request;
using CreditExchange.Syndication.YodleeFastLink.Proxy.Response;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy
{
    public interface IYodleeFastLinkProxy
    {
        ICobrandResponse GetCobrandToken();
        IUserAuthenticateResponse RegisterUser(UserRegisterNAuthenticateRequest userAuthenticateRequest);
        IUserAuthenticateResponse AuthenticateUser(UserRegisterNAuthenticateRequest userAuthenticateRequest);
        IFastlinkAccessTokenResponse GetFastlinkAccessToken(FastlinkAccessTokenRequest fastlinkAccessTokenRequest);
        string GetFalstLinkUrl(FastLinkUrlRequest fastlinkReuest);
        IGetBankAccountResponse GetUserBankAccounts(GetBankAccountsRequest getBankAccountsRequest);
        ITransactionResponse GetAccountTransactions(GetTransactionRequest getTransactionRequest);
    }
}
