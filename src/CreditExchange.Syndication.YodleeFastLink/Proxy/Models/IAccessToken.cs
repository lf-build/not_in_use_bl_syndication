﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IAccessToken
    {
         string AppId { get; set; }
         string Value { get; set; }
    }
}
