﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Merchant: IMerchant
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Address>))]
        [JsonProperty("address")]
        public IAddress Address { get; set; }
    }
}
