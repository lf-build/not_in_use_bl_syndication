﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IAddress
    {
        string City { get; set; }
        string State { get; set; }

        string Country { get; set; }

        string Zip { get; set; }
    }
}
