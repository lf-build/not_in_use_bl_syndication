﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Name: IName
    {
        [JsonProperty("first")]
        public string First { get; set; }

        [JsonProperty("last")]
        public string Last { get; set; }
    }
}
