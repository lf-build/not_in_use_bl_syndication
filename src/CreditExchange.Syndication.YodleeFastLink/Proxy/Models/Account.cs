﻿using Newtonsoft.Json;
using System;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Account: IAccount
    {
        [JsonProperty("CONTAINER")]
        public string Container { get; set; }

        [JsonProperty("providerAccountId")]
        public long ProviderAccountId { get; set; }

        [JsonProperty("accountName")]
        public string AccountName { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Balance>))]
        [JsonProperty("availableBalance")]
        public IBalance AvailableBalance { get; set; }

        [JsonProperty("accountType")]
        public string AccountType { get; set; }

        [JsonProperty("createdDate")]
        public DateTime? CreatedDate { get; set; }

        [JsonProperty("isAsset")]
        public bool? IsAsset { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Balance>))]
        [JsonProperty("balance")]
        public IBalance Balance { get; set; }

        [JsonProperty("providerId")]
        public long ProviderId { get; set; }

        [JsonProperty("providerName")]
        public string ProviderName { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<RefreshInfo>))]
        [JsonProperty("refreshinfo")]
        public IRefreshInfo RefreshInfo { get; set; }

        [JsonProperty("accountStatus")]
        public string AccountStatus { get; set; }
    }
}
