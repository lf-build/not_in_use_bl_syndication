﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface ISession
    {
        string CobSession { get; set; }
    }
}
