﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class User: IUser
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("loginName")]
        public string LoginName { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Name>))]
        [JsonProperty("name")]
        public IName Name { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<UserSession>))]
        [JsonProperty("session")]
        public IUserSession session { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Preferences>))]
        [JsonProperty("preferences")]
        public IPreferences preferences { get; set; }
    }
}
