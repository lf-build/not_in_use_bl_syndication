﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Description: IDescription
    {

        [JsonProperty("original")]
        public string Original { get; set; }

        [JsonProperty("consumer")]
        public string Consumer { get; set; }

        [JsonProperty("simple")]
        public string Simple { get; set; }
    }
}
