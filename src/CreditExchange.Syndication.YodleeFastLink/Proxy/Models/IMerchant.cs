﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IMerchant
    {
        string Id { get; set; }
        string Source { get; set; }
        string Name { get; set; }
        IAddress Address { get; set; }
    }
}
