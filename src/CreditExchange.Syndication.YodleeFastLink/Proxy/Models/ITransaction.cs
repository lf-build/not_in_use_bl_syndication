﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface ITransaction
    {
        string Container { get; set; }
        long Id { get; set; }
        IBalance Amount { get; set; }
        IBalance RunningBalance { get; set; }
        string BaseType { get; set; }
        string CategoryType { get; set; }
        int CategoryId { get; set; }
        string Category { get; set; }
        string CategorySource { get; set; }
        int HighLevelCategoryId { get; set; }
        string Date { get; set; }
        string transactionDate { get; set; }
        string PostDate { get; set; }
        IDescription description { get; set; }
        bool? IsManual { get; set; }
        string Status { get; set; }
        long PostingOrder { get; set; }
        long AccountId { get; set; }
        string Type { get; set; }
        string SubType { get; set; }
        IMerchant Merchant { get; set; }
    }
}
