﻿using Newtonsoft.Json;
using System;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class RefreshInfo: IRefreshInfo
    {
        [JsonProperty("statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty("statusMessage")]
        public string StatusMessage { get; set; }

        [JsonProperty("lastRefreshed")]
        public DateTime? LastRefreshed { get; set; }

        [JsonProperty("lastRefreshAttempt")]
        public DateTime? LastRefreshAttempt { get; set; }

        [JsonProperty("nextRefreshScheduled")]
        public DateTime? NextRefreshScheduled { get; set; }
    }
}
