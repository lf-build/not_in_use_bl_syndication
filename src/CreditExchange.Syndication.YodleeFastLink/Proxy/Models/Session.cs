﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Session: ISession
    {
        [JsonProperty(PropertyName = "cobSession")]
        public string CobSession { get; set; }
    }
}
