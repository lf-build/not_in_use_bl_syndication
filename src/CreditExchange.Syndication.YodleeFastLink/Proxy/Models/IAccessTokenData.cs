﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IAccessTokenData
    {
        IList<IAccessToken> AccessTokens { get; set; }
    }
}
