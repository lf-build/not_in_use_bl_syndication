﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IUserSession
    {
        string UserSessionToken { get; set; }
    }
}
