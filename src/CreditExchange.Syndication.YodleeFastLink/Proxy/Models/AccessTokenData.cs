﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class AccessTokenData: IAccessTokenData
    {
        [JsonConverter(typeof(ConcreteListJsonConverter<List<AccessToken>, IAccessToken>))]
        [JsonProperty("accessTokens")]
        public IList<IAccessToken> AccessTokens { get; set; }
    }
}
