﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IUser
    {
        long Id { get; set; }
        string LoginName { get; set; }
        IName Name { get; set; }
        IUserSession session { get; set; }
        IPreferences preferences { get; set; }
    }
}
