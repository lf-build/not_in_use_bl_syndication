﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IUserLogin
    {
        string LoginName { get; set; }
        string Password { get; set; }
    }
}
