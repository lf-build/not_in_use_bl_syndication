﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class UserLogin: IUserLogin
    {
        [JsonProperty("loginName")]
        public string LoginName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
