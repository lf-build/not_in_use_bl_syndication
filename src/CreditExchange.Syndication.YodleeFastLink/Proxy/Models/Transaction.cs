﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class Transaction: ITransaction
    {

        [JsonProperty("CONTAINER")]
        public string Container { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Balance>))]
        [JsonProperty("amount")]
        public IBalance Amount { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Balance>))]
        [JsonProperty("runningBalance")]
        public IBalance RunningBalance { get; set; }

        [JsonProperty("baseType")]
        public string BaseType { get; set; }

        [JsonProperty("categoryType")]
        public string CategoryType { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("categorySource")]
        public string CategorySource { get; set; }

        [JsonProperty("highLevelCategoryId")]
        public int HighLevelCategoryId { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("transactionDate")]
        public string transactionDate { get; set; }

        [JsonProperty("postDate")]
        public string PostDate { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Description>))]
        [JsonProperty("description")]
        public IDescription description { get; set; }

        [JsonProperty("isManual")]
        public bool? IsManual { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("postingOrder")]
        public long PostingOrder { get; set; }

        [JsonProperty("accountId")]
        public long AccountId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("subType")]
        public string SubType { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Merchant>))]
        [JsonProperty("merchant")]
        public IMerchant Merchant { get; set; }
    }
}
