﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IName
    {
         string First { get; set; }
         string Last { get; set; }
    }
}
