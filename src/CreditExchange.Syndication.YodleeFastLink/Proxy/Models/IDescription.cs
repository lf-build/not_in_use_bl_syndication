﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IDescription
    {
        string Original { get; set; }
        string Consumer { get; set; }
        string Simple { get; set; }
    }
}
