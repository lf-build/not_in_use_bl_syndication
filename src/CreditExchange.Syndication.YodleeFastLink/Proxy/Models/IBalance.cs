﻿namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public interface IBalance
    {
        decimal Amount { get; set; }
        string Currency { get; set; }
    }
}
