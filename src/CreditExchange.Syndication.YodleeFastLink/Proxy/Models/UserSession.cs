﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Proxy.Models
{
    public class UserSession: IUserSession
    {
        [JsonProperty("userSession")]
        public string UserSessionToken { get; set; }
    }
}
