﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public class CobrandResponse : ICobrandResponse
    {
        public CobrandResponse()
        {

        }
        public CobrandResponse(Proxy.Response.ICobrandResponse cobrandResponse)
        {
            if (cobrandResponse!=null)
            {
                Session = new Session(cobrandResponse.Session);
                CobrandId = cobrandResponse.CobrandId;
                ApplicationId = cobrandResponse.ApplicationId;
                Locale = cobrandResponse.Locale;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<ISession,Session>))]
        public ISession Session { get; set; }
        public long CobrandId { get; set; }
        public string ApplicationId { get; set; }
        public string Locale { get; set; }
    }
}
