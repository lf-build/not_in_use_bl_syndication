﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public interface IGetBankAccountResponse
    {
        IList<IAccount> Accounts { get; set; }
    }
}
