﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public interface ICobrandResponse
    {
         ISession Session { get; set; }
         long CobrandId { get; set; }
         string ApplicationId { get; set; }
         string Locale { get; set; }
    }
}
