﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Transaction : ITransaction
    {
        public Transaction() { }
        public Transaction(Proxy.Models.ITransaction transactions)
        {
            Container = transactions.Container;
            Id = transactions.Id;
            Amount = new Balance(transactions.Amount);
            RunningBalance = new Balance(transactions.RunningBalance);
            BaseType = transactions.BaseType;
            CategoryType = transactions.CategoryType;
            CategoryId = transactions.CategoryId;
            Category = transactions.Category;
            CategorySource = transactions.CategorySource;
            HighLevelCategoryId = transactions.HighLevelCategoryId;
            Date = transactions.Date;
            TransactionDate = transactions.transactionDate;
            PostDate = transactions.PostDate;
            Description = new Description(transactions.description);
            IsManual = transactions.IsManual;
            Status = transactions.Status;
            PostingOrder = transactions.PostingOrder;
            AccountId = transactions.AccountId;
            Type = transactions.Type;
            SubType = transactions.SubType;
            Merchant = new Merchant(transactions.Merchant);
        }
        public string Container { get; set; }
        public long Id { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBalance,Balance>))]
        public IBalance Amount { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBalance, Balance>))]
        public IBalance RunningBalance { get; set; }
        public string BaseType { get; set; }
        public string CategoryType { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public string CategorySource { get; set; }
        public int HighLevelCategoryId { get; set; }
        public string Date { get; set; }
        public string TransactionDate { get; set; }
        public string PostDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDescription, Description>))]
        public IDescription Description { get; set; }
        public bool? IsManual { get; set; }
        public string Status { get; set; }
        public long PostingOrder { get; set; }
        public long AccountId { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IMerchant, Merchant>))]
        public IMerchant Merchant { get; set; }
    }
}
