﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IPreferences
    {
         string Currency { get; set; }
         string TimeZone { get; set; }
         string DateFormat { get; set; }
         string Locale { get; set; }
    }
}
