﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IAccessTokenData
    {
        IList<IAccessToken> AccessTokens { get; set; }
    }
}
