﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Session: ISession
    {
        public Session() { }
        public Session(Proxy.Models.ISession session)
        {
            CobSession = session?.CobSession;
        }
        public string CobSession { get; set; }
    }
}
