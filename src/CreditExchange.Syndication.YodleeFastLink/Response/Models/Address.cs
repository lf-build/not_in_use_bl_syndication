﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Address : IAddress
    {
        public Address() { }
        public Address(Proxy.Models.IAddress address)
        {
            if (address != null)
            {
                City = address.City;
                State = address.State;
                Country = address.Country;
                Zip = address.Zip;
            }
        }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
    }
}
