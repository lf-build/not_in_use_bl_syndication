﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IBalance
    {
        decimal Amount { get; set; }
        string Currency { get; set; }
    }
}
