﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IName
    {
         string First { get; set; }
         string Last { get; set; }
    }
}
