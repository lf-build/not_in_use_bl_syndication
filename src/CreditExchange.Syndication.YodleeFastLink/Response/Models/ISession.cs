﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface ISession
    {
        string CobSession { get; set; }
    }
}
