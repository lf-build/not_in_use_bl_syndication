﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Balance : IBalance
    {
        public Balance() { }
        public Balance(Proxy.Models.IBalance balance)
        {
            if (balance != null)
            {
                Amount = balance.Amount;
                Currency = balance.Currency;
            }
        }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
