﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Name: IName
    {
        public Name() { }
        public Name(Proxy.Models.IName name)
        {
            if (name!=null)
            {
                First = name.First;
                Last = name.Last;
            }
        }
        public string First { get; set; }
        public string Last { get; set; }
    }
}
