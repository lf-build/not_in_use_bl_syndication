﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Merchant : IMerchant
    {
        public Merchant() { }
        public Merchant(Proxy.Models.IMerchant merchant)
        {
            if (merchant != null)
            {
                Id = merchant.Id;
                Source = merchant.Source;
                Name = merchant.Name;
                Address = new Address(merchant.Address);
            }
        }
        public string Id { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress,Address>))]
        public IAddress Address { get; set; }
    }
}
