﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class UserSession: IUserSession
    {
        public UserSession()
        {

        }
        public UserSession(Proxy.Models.IUserSession userSession)
        {
            UserSessionToken = userSession?.UserSessionToken;
        }
        public string UserSessionToken { get; set; }
    }
}
