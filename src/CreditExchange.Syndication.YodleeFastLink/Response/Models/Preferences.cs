﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Preferences : IPreferences
    {
        public Preferences() { }
        public Preferences(Proxy.Models.IPreferences preferences)
        {
            if (preferences != null)
            {
                Currency = preferences.Currency;
                TimeZone = preferences.TimeZone;
                DateFormat = preferences.DateFormat;
                Locale = preferences.Locale;
            }
        }
        public string Currency { get; set; }
        public string TimeZone { get; set; }
        public string DateFormat { get; set; }
        public string Locale { get; set; }
    }
}
