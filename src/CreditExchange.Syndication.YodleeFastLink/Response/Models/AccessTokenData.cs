﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class AccessTokenData : IAccessTokenData
    {
        public AccessTokenData() { }
        public AccessTokenData(Proxy.Models.IAccessTokenData accessTokenData)
        {
            if (accessTokenData != null)
                CopyAccessTokens(accessTokenData.AccessTokens);
        }
        [JsonConverter(typeof(InterfaceListConverter<IAccessToken,AccessToken>))]
        public IList<IAccessToken> AccessTokens { get; set; }
        private void CopyAccessTokens(IList<Proxy.Models.IAccessToken> accessTokensList)
        {
            AccessTokens = new List<IAccessToken>();
            foreach (var token in accessTokensList)
            {
                AccessTokens.Add(new AccessToken(token));
            }
        }
    }
}
