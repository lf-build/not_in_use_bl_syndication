﻿using System;
namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class RefreshInfo : IRefreshInfo
    {
        public RefreshInfo() { }
        public RefreshInfo(Proxy.Models.IRefreshInfo refreshInfo)
        {
            if (refreshInfo != null)
            {
                StatusCode = refreshInfo.StatusCode;
                StatusMessage = refreshInfo.StatusMessage;
                LastRefreshed = refreshInfo.LastRefreshed;
                LastRefreshAttempt = refreshInfo.LastRefreshAttempt;
                NextRefreshScheduled = refreshInfo.NextRefreshScheduled;
            }
        }
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public DateTime? LastRefreshed { get; set; }
        public DateTime? LastRefreshAttempt { get; set; }
        public DateTime? NextRefreshScheduled { get; set; }
    }
}
