﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class Description : IDescription
    {
        public Description() { }
        public Description(Proxy.Models.IDescription description)
        {
            if (description != null)
            {
                Original = description.Original;
                Consumer = description.Consumer;
                Simple = description.Simple;
            }
        }
        public string Original { get; set; }
        public string Consumer { get; set; }
        public string Simple { get; set; }
    }
}
