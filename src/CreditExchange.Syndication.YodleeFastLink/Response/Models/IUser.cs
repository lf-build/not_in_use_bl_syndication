﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IUser
    {
        long Id { get; set; }
        string LoginName { get; set; }
        IName Name { get; set; }
        IUserSession Session { get; set; }
        IPreferences Preferences { get; set; }
    }
}
