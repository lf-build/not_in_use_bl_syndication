﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IAccessToken
    {
         string AppId { get; set; }
         string Value { get; set; }
    }
}
