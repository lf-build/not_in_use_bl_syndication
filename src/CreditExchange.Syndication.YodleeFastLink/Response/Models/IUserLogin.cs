﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IUserLogin
    {
        string LoginName { get; set; }
        string Password { get; set; }
    }
}
