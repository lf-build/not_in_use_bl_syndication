﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class UserLogin: IUserLogin
    {
        public UserLogin()
        {

        }
        public UserLogin(Proxy.Models.IUserLogin userLogin)
        {
            if (userLogin!=null)
            {
                LoginName = userLogin.LoginName;
                Password = userLogin.Password;
            }
        }
        public string LoginName { get; set; }
        public string Password { get; set; }
    }
}
