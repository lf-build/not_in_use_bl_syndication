﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public interface IUserSession
    {
        string UserSessionToken { get; set; }
    }
}
