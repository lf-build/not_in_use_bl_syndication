﻿namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class AccessToken: IAccessToken
    {
        public AccessToken() { }
        public AccessToken(Proxy.Models.IAccessToken accessToken)
        {
            AppId = accessToken?.AppId;
            Value = accessToken?.Value;
        }
        public string AppId { get; set; }
        public string Value { get; set; }
    }
}
