﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response.Models
{
    public class User: IUser
    {
        public User()
        {

        }
        public User(Proxy.Models.IUser user)
        {
            if (user!=null)
            {
                Id = user.Id;
                LoginName = user.LoginName;
                Name = new Name(user.Name);
                Session = new UserSession(user.session);
                Preferences = new Preferences(user.preferences);
            }
        }
        public long Id { get; set; }
        public string LoginName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IName,Name>))]
        public IName Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IUserSession,UserSession>))]
        public IUserSession Session { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPreferences,Preferences>))]
        public IPreferences Preferences { get; set; }
    }
}
