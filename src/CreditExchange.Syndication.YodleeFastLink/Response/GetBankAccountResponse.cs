﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public class GetBankAccountResponse : IGetBankAccountResponse
    {
        public GetBankAccountResponse() { }
        public GetBankAccountResponse(Proxy.Response.IGetBankAccountResponse getBankAccountResponse)
        {
            if (getBankAccountResponse != null && getBankAccountResponse.Accounts.Count > 0)
            {
                CopyAccountsList(getBankAccountResponse.Accounts);
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<IAccount,Account>))]
        public IList<IAccount> Accounts { get; set; }
        private void CopyAccountsList(IList<Proxy.Models.IAccount> accounts)
        {
            Accounts = new List<IAccount>();
            foreach (var account in accounts)
            {
                Accounts.Add(new Account(account));
            }
        }
    }
}
