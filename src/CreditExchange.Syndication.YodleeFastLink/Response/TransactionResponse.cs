﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public class TransactionResponse : ITransactionResponse
    {
        public TransactionResponse() { }
        public TransactionResponse(Proxy.Response.ITransactionResponse transactionsResponse)
        {
            if (transactionsResponse != null && transactionsResponse.Transactions?.Count > 0)
            {
                CopyTransactionsList(transactionsResponse.Transactions);
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<ITransaction,Transaction>))]
        public IList<ITransaction> Transactions { get; set; }
        private void CopyTransactionsList(IList<Proxy.Models.ITransaction> transactions)
        {
            Transactions = new List<ITransaction>();
            foreach (var transaction in transactions)
            {
                Transactions.Add(new Transaction(transaction));
            }
        }
    }
}
