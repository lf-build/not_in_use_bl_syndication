﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public interface IUserAuthenticateResponse
    {
        IUser User { get; set; }
    }
}
