﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public class FastlinkAccessTokenResponse : IFastlinkAccessTokenResponse
    {
        public FastlinkAccessTokenResponse() { }
        public FastlinkAccessTokenResponse(Proxy.Response.IFastlinkAccessTokenResponse fastlinkAccessTokenResponse)
        {
            if (fastlinkAccessTokenResponse != null)
                AccessTokenData = new AccessTokenData(fastlinkAccessTokenResponse.AccessTokenData);
        }
        [JsonConverter(typeof(InterfaceConverter<IAccessTokenData,AccessTokenData>))]
        public IAccessTokenData AccessTokenData { get; set; }
    }
}
