﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public class UserAuthenticateResponse : IUserAuthenticateResponse
    {
        public UserAuthenticateResponse() { }
        public UserAuthenticateResponse(Proxy.Response.IUserAuthenticateResponse userAuthenticateResponse)
        {
            if (userAuthenticateResponse != null)
            {
                User = new User(userAuthenticateResponse.User);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IUser, User>))]
        public IUser User { get; set; }
    }
}
