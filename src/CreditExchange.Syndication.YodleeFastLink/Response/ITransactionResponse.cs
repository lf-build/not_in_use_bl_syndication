﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;
using System.Collections.Generic;
namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public interface ITransactionResponse
    {
        IList<ITransaction> Transactions { get; set; }
    }
}
