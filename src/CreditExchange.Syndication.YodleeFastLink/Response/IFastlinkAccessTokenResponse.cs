﻿using CreditExchange.Syndication.YodleeFastLink.Response.Models;

namespace CreditExchange.Syndication.YodleeFastLink.Response
{
    public interface IFastlinkAccessTokenResponse
    {
        IAccessTokenData AccessTokenData { get; set; }
    }
}
