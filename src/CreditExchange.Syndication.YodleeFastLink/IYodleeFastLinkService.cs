﻿using CreditExchange.Syndication.YodleeFastLink.Request;
using CreditExchange.Syndication.YodleeFastLink.Response;
using RestSharp;
using System.Threading.Tasks;
namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IYodleeFastLinkService
    {
        Task<ICobrandResponse> GetCobrandToken(string entityType, string entityId);
        Task<IUserAuthenticateResponse> RegisterUser(string entityType, string entityId, UserRegisterNAuthenticateRequest userAuthenticateRequest);
        Task<IUserAuthenticateResponse> AuthenticateUser(string entityType, string entityId,UserRegisterNAuthenticateRequest userAuthenticateRequest);
        Task<IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId,FastlinkAccessTokenRequest fastlinkAccessTokenRequest);
        Task<IRestResponse> GetFalstLinkUrl(string entityType, string entityId,FastLinkUrlRequest fastlinkReuest);
        Task<IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId,GetBankAccountsRequest getBankAccountsRequest);
        Task<ITransactionResponse> GetAccountTransactions(string entityType, string entityId,GetTransactionRequest getTransactionRequest);
    }
}
