namespace CreditExchange.Syndication.YodleeFastLink
{
    public interface IServiceUrl
    {
        string BaseUrl { get; set; }
        string CobrandLogin { get; set; }
        string UserLogin { get; set; }
        string UserRegistration { get; set; }
        string AccessToken { get; set; }
        string FastLinkBaseUrl { get; set; }
        string GetUserAccounts { get; set; }
        string GetAccountTransactions { get; set; }
    }
}