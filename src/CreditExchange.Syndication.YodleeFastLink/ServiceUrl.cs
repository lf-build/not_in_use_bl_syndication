using System;

namespace CreditExchange.Syndication.YodleeFastLink
{
    public class ServiceUrl : IServiceUrl
    {
        public string FastLinkBaseUrl { get; set; } = "https://node.developer.yodlee.com/authenticate/restserver/";
        public string BaseUrl { get; set; } = "https://developer.api.yodlee.com/ysl/restserver";
        public string CobrandLogin { get; set; } = "/v1/cobrand/login";

        public string UserLogin { get; set; } = "/v1/user/login";

        public string UserRegistration { get; set; } = "/v1/user/register";

        public string AccessToken { get; set; } = "/v1/user/accessTokens";

        public string GetUserAccounts { get; set; } = "/v1/accounts";
        public string GetAccountTransactions { get; set; }= "/v1/transactions";
    }
}