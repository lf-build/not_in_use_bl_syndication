﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{
    public class InstantIdVerificationCodes: CodeDescriptions
    {
        private InstantIdVerificationCodes()
        {
            Items = new Dictionary<string, string>
            {
                {"00","Nothing verified"},
                {"10","Critical ID elements not verified, are associated with different person(s) or indications such as OFAC matches, deceased/invalid SSN, or SSN issued prior to birth date provided, etc. exist"},
                {"20","Minimal verification, critical ID elements not verified or associated with different person(s)"},
                {"30","Several ID elements verified"},
                {"40","Last name, address & SSN or phone verified; first name, phone or SSN verification failures"},
                {"50","Full name, address, phone , SSN verified"}
            };
        }

        static InstantIdVerificationCodes()
        {
            Instance = new InstantIdVerificationCodes();
        }

        public static InstantIdVerificationCodes Instance { get; }

        protected override Dictionary<string, string> Items { get; }
    }
}
