﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{
    public class NameAddressPhoneSummaries : CodeDescriptions
    {
        private NameAddressPhoneSummaries()
        {
            Items = new Dictionary<string, string>
            {
                {"0","Nothing found for input criteria"},
                {"1","Input phone is associated with a different name and address."},
                {"2","First name and Last name matched"},
                {"3","First name and Address matched"},
                {"4","First name and Phone matched"},
                {"5","Last name and Address matched"},
                {"6","Address and Phone matched"},
                {"7","Last name and Phone matched"},
                {"8","First name, Last name and Address matched"},
                {"9","First name, Last name and Phone matched"},
                {"10","First name, Address and Phone matched"},
                {"11","Last name, Address and Phone matched"},
                {"12","First name, Last name, Address and Phone matched"}
            };
        }

        static NameAddressPhoneSummaries()
        {
            Instance = new NameAddressPhoneSummaries();
        }

        public static NameAddressPhoneSummaries Instance { get; }

        protected override Dictionary<string, string> Items { get; }
    }
}
