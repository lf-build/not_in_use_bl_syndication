﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{

    public abstract class CodeDescriptions
    {
        protected abstract Dictionary<string, string> Items { get; }

        public virtual CodeDescription this[string code]
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(code) && Items.ContainsKey(code))
                    return new CodeDescription { Code = code, Description = Items[code] };

                return new CodeDescription { Code = code, Description = "Unknown code." };
            }
        }
    }
}
