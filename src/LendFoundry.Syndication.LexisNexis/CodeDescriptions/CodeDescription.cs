﻿namespace LendFoundry.Syndication.LexisNexis
{
    public class CodeDescription
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
