﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{
    public class NameAddressSsnSummaries : CodeDescriptions
    {

        private NameAddressSsnSummaries()
        {
            Items = new Dictionary<string, string>
            {
                {"0","Nothing found for input criteria"},
                {"1","Input SSN is associated with a different name and address"},
                {"2","Input First name and Last Name matched"},
                {"3","Input First name and Address matched"},
                {"4","Input First name and SSN  matched"},
                {"5","Input Last name and Address matched"},
                {"6","Input Address and SSN matched"},
                {"7","Input Last name and SSN matched"},
                {"8","Input First name, Last name and Address matched"},
                {"9","Input First name, Last name and SSN matched"},
                {"10","Input First name, Address, and SSN matched"},
                {"11","Input Last name, Address, and SSN matched"},
                {"12","Input First name, Last name, Address and SSN matched"},
            };
        }

        static NameAddressSsnSummaries()
        {
            Instance = new NameAddressSsnSummaries();
        }

        public static NameAddressSsnSummaries Instance { get; }

        protected override Dictionary<string, string> Items { get; }
    }
}
