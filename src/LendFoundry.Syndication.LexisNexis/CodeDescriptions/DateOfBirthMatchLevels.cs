﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis
{
    public class DateOfBirthMatchLevels: CodeDescriptions
    {
        private DateOfBirthMatchLevels()
        {
            Items = new Dictionary<string, string>
            {
                {"0","No DOB found or no DOB submitted"},
                {"1","Nothing matches"},
                {"2","Only Day matches"},
                {"3","Only Month Matches"},
                {"4","Only Day and Month Match"},
                {"5","Only Day and Year match"},
                {"6","Only Year matches"},
                {"7","Only Month and Year match"},
                {"8","Month, Day, and year match"}
            };
        }

        static DateOfBirthMatchLevels()
        {
            Instance = new DateOfBirthMatchLevels();
        }

        public static DateOfBirthMatchLevels Instance { get; }

        protected override Dictionary<string, string> Items { get; }
    }
}
