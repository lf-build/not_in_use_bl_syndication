﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.FlexId;
using LendFoundry.Syndication.LexisNexis.FraudPoint;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis
{
    public interface ILexisNexisService
    {
        Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId, ISearchBankruptcyRequest request);
        Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId, IBankruptcyFcraReportRequest request);
        Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest request);
        Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest request);
        Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest request);
        IFlexIdResponse GetFlexIdVerificationResult(IFlexIdRequest request);
        IFraudPointResponse GetFraudPointVerificationResult(IFraudPointRequest request);
        InstantId.IInstantIdResponse GetInstantIdVerificationResult(InstantId.IInstantIdRequest request);
        InstantIdKba.ITransactionResponse GetInstantIdKbaIdentityVerificationResult(InstantIdKba.IInstantIdIdentityRequest request);
        InstantIdKba.ITransactionResponse GetInstantIdKbaContinuationVerificationResult(InstantIdKba.IInstantIdContinuationRequest request);
    }
}