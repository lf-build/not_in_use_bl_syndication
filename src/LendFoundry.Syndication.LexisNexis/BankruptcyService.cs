﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Foundation.Lookup;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.LexisNexis
{
    public class BankruptcyService : IBankruptcyService
    {
        public BankruptcyService(IBankruptcyProxy bankruptcyServiceProxy, LexisNexisConfiguration configuration, ILookupService lookupService, ILogger logger, IEventHubClient eventHubClient)
        {
            if (bankruptcyServiceProxy == null)
                throw new ArgumentNullException(nameof(bankruptcyServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            BankruptcyServiceProxy = bankruptcyServiceProxy;
            Configuration = configuration;
            Logger = logger;
            EventHubClient = eventHubClient;
            LookupService = lookupService;
        }

        private IBankruptcyProxy BankruptcyServiceProxy { get; }
        private LexisNexisConfiguration Configuration { get; }
        private ILookupService LookupService { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHubClient { get; }

      
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}
