﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.SyndicationStore.Events;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis
{
    public class CriminalRecordService : ICriminalRecordService
    {
        public CriminalRecordService(ICriminalRecordProxy criminalServiceProxy, LexisNexisConfiguration configuration, ILookupService lookupService, ILogger logger, IEventHubClient eventHubClient)
        {
            if (criminalServiceProxy == null)
                throw new ArgumentNullException(nameof(criminalServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            CriminalServiceProxy = criminalServiceProxy;
            Configuration = configuration;
            Logger = logger;
            EventHubClient = eventHubClient;
            LookupService = lookupService;
        }

        private ICriminalRecordProxy CriminalServiceProxy { get; }
        private LexisNexisConfiguration Configuration { get; }
        private ILookupService LookupService { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHubClient { get; }

        public async Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest request)
        {
            ICriminalRecordSearchResponse searchResult = null;
            try
            {
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);

                var user = new CriminalRecord.Proxy.CriminalSearch.ServiceReference.User(Configuration.CriminalRecord.EndUser);
                var options = new FcraCriminalSearchOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalSearchBy(request);
                searchResult = new CriminalRecordSearchResponse(await CriminalServiceProxy.CriminalRecordSearch(user, searchBy, options));
                if (searchResult != null)
                {
                    await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                    {
                        Data = searchResult,
                        EntityId = entityId,
                        EntityType = entityType,
                        Name = Settings.ServiceName
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
            return searchResult;
        }

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest request)
        {
            ICriminalReportResponse reportResult = null;
            try
            {
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);
                var user = new CriminalRecord.Proxy.CriminalReport.ServiceReference.User(request);
                var options = new FcraCriminalReportOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalReportBy(request);
                reportResult = new CriminalReportResponse(await CriminalServiceProxy.CriminalRecordReport(user, searchBy, options));
                if (reportResult != null)
                {
                    await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                    {
                        Data = reportResult,
                        EntityId = entityId,
                        EntityType = entityType,
                        Name = Settings.ServiceName
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
            return reportResult;
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}
