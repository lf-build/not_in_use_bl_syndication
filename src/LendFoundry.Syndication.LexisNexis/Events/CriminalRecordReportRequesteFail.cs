﻿using LendFoundry.SyndicationStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Events
{
    public class CriminalRecordReportRequesteFail : SyndicationCalledEvent
    {
    }
}
