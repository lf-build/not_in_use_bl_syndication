﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.LexisNexis.Events
{
    public class BankruptcySearchRequesteFail : SyndicationCalledEvent
    { 
    }
}
