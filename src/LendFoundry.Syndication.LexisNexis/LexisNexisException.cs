﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.LexisNexis
{
    public class LexisNexisException : Exception
    {
        public LexisNexisException()
        {
        }

        public LexisNexisException(string message) : base(message)
        {
        }

        public LexisNexisException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LexisNexisException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
