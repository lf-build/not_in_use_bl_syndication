﻿namespace LendFoundry.Syndication.LexisNexis
{
    public interface IServiceConfiguration
    {
        string UserName { get; set; }
        string Password { get; set; }
    }
}
