﻿namespace LendFoundry.Syndication.LexisNexis
{
    public class LexisNexisConfiguration
    { 
        //public FlexIdServiceConfiguration FlexId { get; set; }
        //public FraudPointServiceConfiguration FraudPoint { get; set; }
        //public InstantIdServiceConfiguration InstantId { get; set; }
        //public KbaServiceConfiguration InstantIdKba { get; set; }
        public BankruptcyConfiguration Bankruptcy { get; set; }
        public CriminalRecordConfiguration CriminalRecord { get; set; }
    }
}