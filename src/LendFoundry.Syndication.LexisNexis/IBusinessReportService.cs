﻿namespace LendFoundry.Syndication.LexisNexis
{
    public interface IBusinessReportService
    {
        BusinessSearch.ISearchResponse Search(BusinessSearch.ISearchRequest request);
        BusinessReport.IReportResponse GetReport(BusinessReport.IReportRequest request);
    }
}
