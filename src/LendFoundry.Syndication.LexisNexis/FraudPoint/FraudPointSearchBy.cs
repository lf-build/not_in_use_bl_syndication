﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointSearchBy : IFraudPointSearchBy
    {
        public FraudPointSearchBy(ServiceReference.FraudPointSearchBy searchBy)
        {
            if (searchBy == null)
                return;

            Name = new Name(searchBy.Name);
            Address = new Address(searchBy.Address);
            Address2 = new Address(searchBy.Address2);
            DateOfBirth = new Date(searchBy.DOB);
            Ssn = searchBy.SSN;
            Phone10 = searchBy.Phone10;
            WorkPhone = searchBy.WorkPhone;
            DriverLicenseNumber = searchBy.DriverLicenseNumber;
            DriverLicenseState = searchBy.DriverLicenseState;
            IpAddress = searchBy.IPAddress;
            Email = searchBy.Email;
            Channel = (ChannelIdentifier)(int)(searchBy.Channel);
            Income = searchBy.Income;
            OwnOrRent = (OwnRent)(int)searchBy.OwnOrRent;
            LocationIdentifier = searchBy.LocationIdentifier;
            OtherApplicationIdentifier1 = searchBy.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = searchBy.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = searchBy.OtherApplicationIdentifier3;
            ApplicationDateTime = new TimeStamp(searchBy.ApplicationDateTime);
        }

        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IAddress Address2 { get; set; }
        public IDate DateOfBirth { get; set; }
        public string Ssn { get; set; }
        public string Phone10 { get; set; }
        public string WorkPhone { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string Email { get; set; }
        public ChannelIdentifier Channel { get; set; }
        public string Income { get; set; }
        public OwnRent OwnOrRent { get; set; }
        public string LocationIdentifier { get; set; }
        public string OtherApplicationIdentifier1 { get; set; }
        public string OtherApplicationIdentifier2 { get; set; }
        public string OtherApplicationIdentifier3 { get; set; }
        public ITimeStamp ApplicationDateTime { get; set; }
    }
}
