﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointResult : IFraudPointResult
    {
        public FraudPointResult(ServiceReference.FraudPointResult result)
        {
            if (result == null)
                return;

            InputEcho = new FraudPointInputEcho(result.InputEcho);

            if (result.Attributes != null)
            {
                List<INameValuePair> attributes = new List<INameValuePair>();
                foreach (ServiceReference.NameValuePair attribute in result.Attributes)
                {
                    attributes.Add(new NameValuePair(attribute));
                }

                Attributes = attributes.ToArray();
            }

            RedFlagsReport = new RedFlagsReport(result.RedFlagsReport);

            if (result.Models != null)
            {
                List<IModelWithIndices> models = new List<IModelWithIndices>();
                foreach (ServiceReference.ModelWithIndices model in result.Models)
                {
                    models.Add(new ModelWithIndices(model));
                }

                Models = models.ToArray();
            }
        }


        public IFraudPointInputEcho InputEcho { get; set; }
        public INameValuePair[] Attributes { get; set; }
        public IRedFlagsReport RedFlagsReport { get; set; }
        public IModelWithIndices[] Models { get; set; }
    }
}
