﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class RedFlag : IRedFlag
    {
        public RedFlag(ServiceReference.RedFlag redFlag)
        {
            if (redFlag == null)
                return;

            Name = redFlag.Name;

            if (redFlag.HighRiskIndicators != null)
            {
                List<ISequencedRiskIndicator> highRisks = new List<ISequencedRiskIndicator>();
                foreach (ServiceReference.SequencedRiskIndicator highRisk in redFlag.HighRiskIndicators)
                {
                    highRisks.Add(new SequencedRiskIndicator(highRisk));
                }

                HighRiskIndicators = highRisks.ToArray();
            }
        }
        public string Name { get; set; }
        public ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
