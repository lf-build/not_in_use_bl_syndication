﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class ScoreWithIndices : IScoreWithIndices
    {
        public ScoreWithIndices(ServiceReference.ScoreWithIndices scoreWithIndices)
        {
            if (scoreWithIndices == null)
                return;

            Type = scoreWithIndices.Type;
            Value = scoreWithIndices.Value;

            if (scoreWithIndices.RiskIndices != null)
            {
                List<INameValuePair> risks = new List<INameValuePair>();
                foreach (ServiceReference.NameValuePair risk in scoreWithIndices.RiskIndices)
                {
                    risks.Add(new NameValuePair(risk));
                }
                RiskIndices = risks.ToArray();
            }

            if (scoreWithIndices.RiskIndicators != null)
            {
                List<IRiskIndicator> indicators = new List<IRiskIndicator>();
                foreach (ServiceReference.RiskIndicator indicator in scoreWithIndices.RiskIndicators)
                {
                    indicators.Add(new RiskIndicator(indicator));
                }

                RiskIndicators = indicators.ToArray();
            }
        }

        public string Type { get; set; }
        public int Value { get; set; }
        public INameValuePair[] RiskIndices { get; set; }
        public IRiskIndicator[] RiskIndicators { get; set; }
    }
}
