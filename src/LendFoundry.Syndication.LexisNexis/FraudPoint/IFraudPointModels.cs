﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointModels
    {
        string FraudPointModel { get; set; }
        IModelRequest[] ModelRequests { get; set; }
    }
}
