﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class RedFlagsReport : IRedFlagsReport
    {
        public RedFlagsReport(ServiceReference.RedFlagsReport redFlagsReport)
        {
            if (redFlagsReport == null)
                return;

            Version = redFlagsReport.Version;

            if (redFlagsReport.RedFlags != null)
            {
                List<IRedFlag> redFlags = new List<IRedFlag>();
                foreach (ServiceReference.RedFlag redFlag in redFlagsReport.RedFlags)
                {
                    redFlags.Add(new RedFlag(redFlag));
                }
                RedFlags = redFlags.ToArray();
            }
        }

        public int Version { get; set; }
        public IRedFlag[] RedFlags { get; set; }
    }
}
