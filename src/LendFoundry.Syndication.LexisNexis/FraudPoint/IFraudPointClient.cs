﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointClient
    {
        ServiceReference.FraudPointResponse Verify(ServiceReference.FraudPointSearchBy search);
    }
}