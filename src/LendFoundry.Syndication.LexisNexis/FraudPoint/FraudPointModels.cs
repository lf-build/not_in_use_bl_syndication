﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointModels : IFraudPointModels
    {
        public FraudPointModels() {}

        public FraudPointModels(ServiceReference.FraudPointModels models)
        {
            if (models == null)
                return;

            FraudPointModel = models.FraudPointModel;

            if (models.ModelRequests != null)
            {
                List<IModelRequest> requests = new List<IModelRequest>();
                foreach (ServiceReference.ModelRequest request in models.ModelRequests)
                {
                    requests.Add(new ModelRequest(request));
                }

                ModelRequests = requests.ToArray();
            }
        }

        public string FraudPointModel { get; set; }
        public IModelRequest[] ModelRequests { get; set; }
    }
}