﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IRedFlagsReport
    {
        int Version { get; set; }
        IRedFlag[] RedFlags { get; set; }
    }
}
