﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface INameValuePair
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}
