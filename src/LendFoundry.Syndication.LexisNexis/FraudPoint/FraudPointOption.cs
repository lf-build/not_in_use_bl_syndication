﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointOption : IFraudPointOption
    {
        public FraudPointOption() {}

        public FraudPointOption(ServiceReference.FraudPointOption option)
        {
            if (option == null)
                return;

            IncludeModels = new FraudPointModels(option.IncludeModels);
            AttributesVersionRequest = option.AttributesVersionRequest;
            RedFlagsReport = option.RedFlagsReport;
            IncludeRiskIndices = option.IncludeRiskIndices;
        }
        [JsonConverter(typeof(InterfaceConverter<IFraudPointModels, FraudPointModels>))]
        public IFraudPointModels IncludeModels { get; set; }
        public string AttributesVersionRequest { get; set; }
        public string RedFlagsReport { get; set; } = "Version1";
        public bool IncludeRiskIndices { get; set; } = true;
    }
}