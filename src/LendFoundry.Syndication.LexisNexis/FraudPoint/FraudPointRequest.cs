﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointRequest : IFraudPointRequest
    {
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IAddress Address2 { get; set; }
        public IDate DateOfBirth { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string Phone10 { get; set; }
        public string WorkPhone { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string Email { get; set; }
        public ChannelIdentifier Channel { get; set; }
        public string Income { get; set; }
        public OwnRent OwnOrRent { get; set; }
        public string LocationIdentifier { get; set; }
        public string OtherApplicationIdentifier1 { get; set; }
        public string OtherApplicationIdentifier2 { get; set; }
        public string OtherApplicationIdentifier3 { get; set; }
        public ITimeStamp ApplicationDateTime { get; set; }
    }
}