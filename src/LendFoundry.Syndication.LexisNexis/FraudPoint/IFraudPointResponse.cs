﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointResponse
    {
        IResponseHeader Header { get; set; }
        IFraudPointResult Result { get; set; }
    }
}
