﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public enum OwnRent
    {
        Item,
        Own,
        Rent
    }
}
