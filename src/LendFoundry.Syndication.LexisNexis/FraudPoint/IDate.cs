﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IDate
    {
        short Year { get; set; }
        short Month { get; set; }
        short Day { get; set; }
    }
}
