﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IModelRequest
    {
        string ModelName { get; set; }
        IModelOption[] ModelOptions { get; set; }
    }
}
