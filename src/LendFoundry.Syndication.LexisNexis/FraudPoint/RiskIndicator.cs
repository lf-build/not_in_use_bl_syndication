﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class RiskIndicator : IRiskIndicator
    {
        public RiskIndicator(ServiceReference.RiskIndicator riskIndicator)
        {
            RiskCode = riskIndicator.RiskCode;
            Description = riskIndicator.Description;
        }
           
        public string RiskCode { get; set; }
        public string Description { get; set; }
    }
}
