﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointResult
    {
        IFraudPointInputEcho InputEcho { get; set; }
        INameValuePair[] Attributes { get; set; }
        IRedFlagsReport RedFlagsReport { get; set; }
        IModelWithIndices[] Models { get; set; }
    }
}
