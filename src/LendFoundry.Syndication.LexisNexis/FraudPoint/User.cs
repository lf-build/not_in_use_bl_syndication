﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class User : IUser
    {
        public User() { }

        public User(ServiceReference.User user)
        {
            if (user == null)
                return;

            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GrammLeachBlileyPurpose = user.GLBPurpose;
            DriverLicensePurpose = user.DLPurpose;
            EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        public string ReferenceCode { get; set; }
        public string BillingCode { get; set; }
        public string QueryId { get; set; }
        public string GrammLeachBlileyPurpose { get; set; } = "1";
        public string DriverLicensePurpose { get; set; } = "3";
        public IEndUserInfo EndUser { get; set; }
        public int MaxWaitSeconds { get; set; }
        public string AccountNumber { get; set; }
    }
}