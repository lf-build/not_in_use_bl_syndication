﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class EndUserInfo : IEndUserInfo
    {
        public EndUserInfo() { }

        public EndUserInfo(ServiceReference.EndUserInfo endUserInfo)
        {
            if (endUserInfo == null)
                return;

            CompanyName = endUserInfo.CompanyName;
            StreetAddress1 = endUserInfo.StreetAddress1;
            City = endUserInfo.City;
            State = endUserInfo.State;
            Zip5 = endUserInfo.Zip5;
        }

        public string CompanyName { get; set; }
        public string StreetAddress1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip5 { get; set; }
    }
}