﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IModelOption
    {
        string OptionName { get; set; }
        string OptionValue { get; set; }
    }
}
