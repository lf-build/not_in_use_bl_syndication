﻿using System;
using System.Net;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointClient : IFraudPointClient
    {
        public FraudPointClient(ILexisNexisConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.FraudPoint == null)
                throw new ArgumentNullException(nameof(configuration.FraudPoint));

            if (configuration.FraudPoint.UserName == null)
                throw new ArgumentNullException(nameof(configuration.FraudPoint.UserName));

            if (configuration.FraudPoint.Password == null)
                throw new ArgumentNullException(nameof(configuration.FraudPoint.Password));

            Configuration = configuration.FraudPoint;
        }

        private FraudPointServiceConfiguration Configuration { get;  }

        public ServiceReference.FraudPointResponse Verify(ServiceReference.FraudPointSearchBy search)
        {
            try
            {
                ServiceReference.WsIdentity soapClient = new ServiceReference.WsIdentity();
                soapClient.Url = Configuration.FraudPointUrl;
                soapClient.Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password);

                ServiceReference.User user = new ServiceReference.User(Configuration.EndUser);
                ServiceReference.FraudPointOption options = new ServiceReference.FraudPointOption(Configuration.Options);

                return soapClient.FraudPoint(user, options, search);
            }
            catch (Exception ex)
            {
                throw new LexisNexisException($"The method Verify({search}) raised an error:{ex.Message}", ex);
            }
        }
    }
}