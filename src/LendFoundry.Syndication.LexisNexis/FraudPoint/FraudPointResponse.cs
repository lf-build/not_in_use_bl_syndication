﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointResponse : IFraudPointResponse
    {
        public FraudPointResponse(ServiceReference.FraudPointResponse response)
        {
            if (response == null)
                return;

            Header = new ResponseHeader(response.Header);
            Result = new FraudPointResult(response.Result);
        }

        public IResponseHeader Header { get; set; }
        public IFraudPointResult Result { get; set; }
    }
}
