﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface ISequencedRiskIndicator
    {
        string RiskCode { get; set; }
        string Description { get; set; }
        int Sequence { get; set; }
    }
}
