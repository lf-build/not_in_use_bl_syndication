﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class FraudPointInputEcho : IFraudPointInputEcho
    {
        public FraudPointInputEcho(ServiceReference.FraudPointInputEcho inputEcho)
        {
            if (inputEcho == null)
                return;

            Name = new Name(inputEcho.Name);
            Address = new Address(inputEcho.Address);
            Address2 = new Address(inputEcho.Address2);
            DateOfBirth = new Date(inputEcho.DOB);
            Ssn = inputEcho.SSN;
            Phone10 = inputEcho.Phone10;
            WorkPhone = inputEcho.WorkPhone;
            DriverLicenseNumber = inputEcho.DriverLicenseNumber;
            DriverLicenseState = inputEcho.DriverLicenseState;
            IpAddress = inputEcho.IPAddress;
            Email = inputEcho.Email;
            Channel = (ChannelIdentifier)(int)inputEcho.Channel;
            Income = inputEcho.Income;
            OwnOrRent = (OwnRent)(int)inputEcho.OwnOrRent;
            LocationIdentifier = inputEcho.LocationIdentifier;
            OtherApplicationIdentifier1 = inputEcho.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = inputEcho.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = inputEcho.OtherApplicationIdentifier3;
            ApplicationDateTime = new TimeStamp(inputEcho.ApplicationDateTime);
            Grade = inputEcho.Grade;
        }

        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IAddress Address2 { get; set; }
        public IDate DateOfBirth { get; set; }
        public string Ssn { get; set; }
        public string Phone10 { get; set; }
        public string WorkPhone { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string Email { get; set; }
        public ChannelIdentifier Channel { get; set; }
        public string Income { get; set; }
        public OwnRent OwnOrRent { get; set; }
        public string LocationIdentifier { get; set; }
        public string OtherApplicationIdentifier1 { get; set; }
        public string OtherApplicationIdentifier2 { get; set; }
        public string OtherApplicationIdentifier3 { get; set; }
        public ITimeStamp ApplicationDateTime { get; set; }
        public string Grade { get; set; }
    }
}
