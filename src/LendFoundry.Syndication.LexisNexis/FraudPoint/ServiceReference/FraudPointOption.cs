﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint.ServiceReference
{
    public partial class FraudPointOption
    {
        public FraudPointOption(IFraudPointOption options)
        {
            if (options == null)
                return;

            AttributesVersionRequest = options.AttributesVersionRequest;
            IncludeModels = new FraudPoint.ServiceReference.FraudPointModels();
            List<FraudPoint.ServiceReference.ModelRequest> models = new List<FraudPoint.ServiceReference.ModelRequest>();

            if (options.IncludeModels.ModelRequests != null)
            {
                foreach (FraudPoint.ModelRequest requestModel in options.IncludeModels.ModelRequests)
                {
                    FraudPoint.ServiceReference.ModelRequest modelRequest = new FraudPoint.ServiceReference.ModelRequest();
                    modelRequest.ModelName = requestModel.ModelName;

                    List<FraudPoint.ServiceReference.ModelOption> modelOptions = new List<FraudPoint.ServiceReference.ModelOption>();
                    foreach (FraudPoint.ModelOption modelOption in requestModel.ModelOptions)
                    {
                        modelOptions.Add(new FraudPoint.ServiceReference.ModelOption()
                        {
                            OptionName = modelOption.OptionName,
                            OptionValue = modelOption.OptionValue
                        });
                    }
                    modelRequest.ModelOptions = modelOptions.ToArray();
                    models.Add(modelRequest);
                }
            }
            IncludeModels.FraudPointModel = options.IncludeModels.FraudPointModel;
            IncludeModels.ModelRequests = models.ToArray();
            IncludeRiskIndices = options.IncludeRiskIndices;
            RedFlagsReport = options.RedFlagsReport;
        }
    }
}