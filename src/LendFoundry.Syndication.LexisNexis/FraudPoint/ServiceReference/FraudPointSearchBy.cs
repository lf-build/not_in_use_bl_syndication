﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint.ServiceReference
{
    public partial class FraudPointSearchBy
    {
        public FraudPointSearchBy()
        {
        }

        public FraudPointSearchBy(IFraudPointRequest searchBy)
        {
            if (searchBy == null)
                return;

            if (searchBy.Address != null)
                Address = new Address
                {
                    City = searchBy.Address.City,
                    County = searchBy.Address.County,
                    PostalCode = searchBy.Address.PostalCode,
                    State = searchBy.Address.State,
                    StateCityZip = searchBy.Address.StateCityZip,
                    StreetAddress1 = searchBy.Address.StreetAddress1,
                    StreetAddress2 = searchBy.Address.StreetAddress2,
                    StreetName = searchBy.Address.StreetName,
                    StreetNumber = searchBy.Address.StreetNumber,
                    StreetPostDirection = searchBy.Address.StreetPostDirection,
                    StreetPreDirection = searchBy.Address.StreetPreDirection,
                    StreetSuffix = searchBy.Address.StreetSuffix,
                    UnitDesignation = searchBy.Address.UnitDesignation,
                    UnitNumber = searchBy.Address.UnitNumber,
                    Zip4 = searchBy.Address.Zip4,
                    Zip5 = searchBy.Address.Zip5
                };

            if (searchBy.Address2 != null)
                Address2 = new Address
                {
                    City = searchBy.Address2.City,
                    County = searchBy.Address2.County,
                    PostalCode = searchBy.Address2.PostalCode,
                    State = searchBy.Address2.State,
                    StateCityZip = searchBy.Address2.StateCityZip,
                    StreetAddress1 = searchBy.Address2.StreetAddress1,
                    StreetAddress2 = searchBy.Address2.StreetAddress2,
                    StreetName = searchBy.Address2.StreetName,
                    StreetNumber = searchBy.Address2.StreetNumber,
                    StreetPostDirection = searchBy.Address2.StreetPostDirection,
                    StreetPreDirection = searchBy.Address2.StreetPreDirection,
                    StreetSuffix = searchBy.Address2.StreetSuffix,
                    UnitDesignation = searchBy.Address2.UnitDesignation,
                    UnitNumber = searchBy.Address2.UnitNumber,
                    Zip4 = searchBy.Address2.Zip4,
                    Zip5 = searchBy.Address2.Zip5
                };

            if (searchBy.DateOfBirth != null)
                DOB = new Date
                {
                    Day = searchBy.DateOfBirth.Day,
                    Month = searchBy.DateOfBirth.Month,
                    Year = searchBy.DateOfBirth.Year
                };

            DriverLicenseNumber = searchBy.DriverLicenseNumber;
            DriverLicenseState = searchBy.DriverLicenseState;
            IPAddress = searchBy.IpAddress;
            Channel = (ChannelIdentifier)(int)searchBy.Channel;
            Email = searchBy.Email;
            Income = searchBy.Income;
            OwnOrRent = (OwnRent)(int)searchBy.OwnOrRent;
            LocationIdentifier = searchBy.LocationIdentifier;
            OtherApplicationIdentifier1 = searchBy.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = searchBy.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = searchBy.OtherApplicationIdentifier3;

            if (searchBy.ApplicationDateTime != null)
                ApplicationDateTime = new TimeStamp
                {
                    Day = searchBy.ApplicationDateTime.Day,
                    Hour24 = searchBy.ApplicationDateTime.Hour24,
                    Minute = searchBy.ApplicationDateTime.Minute,
                    Month = searchBy.ApplicationDateTime.Month,
                    Second = searchBy.ApplicationDateTime.Second,
                    Year = searchBy.ApplicationDateTime.Year
                };

            if (searchBy.Name != null)
                Name = new Name
                {
                    First = searchBy.Name.First,
                    Full = searchBy.Name.Full,
                    Last = searchBy.Name.Last,
                    Middle = searchBy.Name.Middle,
                    Prefix = searchBy.Name.Prefix,
                    Suffix = searchBy.Name.Suffix
                };

            SSN = searchBy.SocialSecurityNumber;
            WorkPhone = searchBy.WorkPhone;
            Phone10 = searchBy.Phone10;
        }
    }
}