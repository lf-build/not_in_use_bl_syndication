﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class SequencedRiskIndicator : ISequencedRiskIndicator
    {
        public SequencedRiskIndicator(ServiceReference.SequencedRiskIndicator riskIndicator)
        {
            if (riskIndicator == null)
                return;

            RiskCode = riskIndicator.RiskCode;
            Description = riskIndicator.Description;
            Sequence = riskIndicator.Sequence;
        }

        public string RiskCode { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
    }
}
