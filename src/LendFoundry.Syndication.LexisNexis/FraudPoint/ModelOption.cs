﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class ModelOption : IModelOption
    {
        public ModelOption() { }

        public ModelOption(ServiceReference.ModelOption option)
        {
            if (option == null)
                return;

            OptionName = option.OptionName;
            OptionValue = option.OptionValue;
        }

        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }
}