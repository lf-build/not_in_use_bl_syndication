﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IRiskIndicator
    {
        string RiskCode { get; set; }
        string Description { get; set; }
    }
}
