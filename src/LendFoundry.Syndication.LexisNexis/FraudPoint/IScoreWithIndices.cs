﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IScoreWithIndices
    {
        string Type { get; set; }
        int Value { get; set; }
        INameValuePair[] RiskIndices { get; set; }
        IRiskIndicator[] RiskIndicators { get; set; }
    }
}
