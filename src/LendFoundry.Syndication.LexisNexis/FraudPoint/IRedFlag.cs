﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IRedFlag
    {
       string Name { get; set; }
       ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
