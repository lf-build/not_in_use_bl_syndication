﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointRequest
    {
        IName Name { get; set; }
        IAddress Address { get; set; }
        IAddress Address2 { get; set; }
        IDate DateOfBirth { get; set; }
        string SocialSecurityNumber { get; set; }
        string Phone10 { get; set; }
        string WorkPhone { get; set; }
        string DriverLicenseNumber { get; set; }
        string DriverLicenseState { get; set; }
        string IpAddress { get; set; }
        string Email { get; set; }
        ChannelIdentifier Channel { get; set; }
        string Income { get; set; }
        OwnRent OwnOrRent { get; set; }
        string LocationIdentifier { get; set; }
        string OtherApplicationIdentifier1 { get; set; }
        string OtherApplicationIdentifier2 { get; set; }
        string OtherApplicationIdentifier3 { get; set; }
        ITimeStamp ApplicationDateTime { get; set; }
    }
}
