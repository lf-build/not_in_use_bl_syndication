﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class TimeStamp : ITimeStamp
    {
        public TimeStamp()
        {
        }

        public TimeStamp(ServiceReference.TimeStamp timeStamp)
        {
            if (timeStamp == null)
                return;

            Year = timeStamp.Year;
            Month = timeStamp.Month;
            Day = timeStamp.Day;
            Hour24 = timeStamp.Hour24;
            Minute = timeStamp.Minute;
            Second = timeStamp.Second;
        }

        public short Year { get; set; }
        public short Month { get; set; }
        public short Day { get; set; }
        public short Hour24 { get; set; }
        public short Minute { get; set; }
        public short Second { get; set; }
    }
}