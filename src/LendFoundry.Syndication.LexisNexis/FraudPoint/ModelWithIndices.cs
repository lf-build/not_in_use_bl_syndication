﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class ModelWithIndices : IModelWithIndices
    {
        public ModelWithIndices(ServiceReference.ModelWithIndices models)
        {
            if (models == null)
                return;

            Name = models.Name;

            if (models.Scores != null)
            {
                List<IScoreWithIndices> scores = new List<IScoreWithIndices>();
                foreach (ServiceReference.ScoreWithIndices score in models.Scores)
                {
                    scores.Add(new ScoreWithIndices(score));
                }

                Scores = scores.ToArray();
            }
        }

        public string Name { get; set; }
        public IScoreWithIndices[] Scores { get; set; }
    }
}
