﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public class ModelRequest : IModelRequest
    {
        public ModelRequest() { }

        public ModelRequest(ServiceReference.ModelRequest modelRequest)
        {
            if (modelRequest == null)
                return;

            ModelName = modelRequest.ModelName;

            List<IModelOption> options = new List<IModelOption>();
            foreach (ServiceReference.ModelOption option in modelRequest.ModelOptions)
            {
                options.Add(new ModelOption(option));
            }

            ModelOptions = options.ToArray();
        }

        public string ModelName { get; set; }
        public IModelOption[] ModelOptions { get; set; }
    }
}