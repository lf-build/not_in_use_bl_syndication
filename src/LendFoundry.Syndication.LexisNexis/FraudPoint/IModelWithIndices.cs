﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IModelWithIndices
    {
        string Name { get; set; }
        IScoreWithIndices[] Scores { get; set; }
    }
}
