﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IFraudPointOption
    {
        [JsonConverter(typeof(InterfaceConverter<IFraudPointModels, FraudPointModels>))]
        IFraudPointModels IncludeModels { get; set; }
        string AttributesVersionRequest { get; set; }
        string RedFlagsReport { get; set; }
        bool IncludeRiskIndices { get; set; }
    }
}
