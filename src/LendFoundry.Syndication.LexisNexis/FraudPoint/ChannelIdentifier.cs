﻿namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public enum ChannelIdentifier
    {
        Item,
        Mail,
        PointOfSale,
        Kiosk,
        Internet,
        Branch,
        Telephonic,
        Other
    }
}
