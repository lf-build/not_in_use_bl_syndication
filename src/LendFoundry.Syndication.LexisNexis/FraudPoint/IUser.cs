﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.FraudPoint
{
    public interface IUser
    {
        string ReferenceCode { get; set; }
        string BillingCode { get; set; }
        string QueryId { get; set; }
        string GrammLeachBlileyPurpose { get; set; }
        string DriverLicensePurpose { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEndUserInfo,EndUserInfo>))]
        IEndUserInfo EndUser { get; set; }
        int MaxWaitSeconds { get; set; }
        string AccountNumber { get; set; }
    }
}
