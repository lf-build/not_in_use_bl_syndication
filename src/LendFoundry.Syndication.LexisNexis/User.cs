namespace LendFoundry.Syndication.LexisNexis
{
    public class User : IUser
    {
        public User()
        {

        }

        public User(BusinessReport.ServiceReference.User user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GrammLeachBlileyActPurpose = user.GLBPurpose;
            DriversPrivacyProtectionActPurpose = user.DLPurpose;
            EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        public User(BusinessSearch.ServiceReference.User user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GrammLeachBlileyActPurpose = user.GLBPurpose;
            DriversPrivacyProtectionActPurpose = user.DLPurpose;
            EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        public string ReferenceCode { get; set; }
        public string BillingCode { get; set; }
        public string QueryId { get; set; }
        public string GrammLeachBlileyActPurpose { get; set; }
        public string DriversPrivacyProtectionActPurpose { get; set; }
        public IEndUserInfo EndUser { get; set; }
        public int MaxWaitSeconds { get; set; }
        public string AccountNumber { get; set; }
    }
}