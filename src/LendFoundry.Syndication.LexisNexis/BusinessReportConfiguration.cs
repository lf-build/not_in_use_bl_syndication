﻿namespace LendFoundry.Syndication.LexisNexis
{
    public class BusinessReportConfiguration : ServiceConfiguration,IBusinessReportConfiguration
    {
        public BusinessReportConfiguration()
        { }
        public BusinessReportConfiguration(IBusinessReportConfiguration businessReportConfiguration)
        {
            if (businessReportConfiguration == null)
                return;
            EndUser = businessReportConfiguration.EndUser;
            SearchOption = businessReportConfiguration.SearchOption;
            ReportOption = businessReportConfiguration.ReportOption;
            UserName = businessReportConfiguration.UserName;
            Password = businessReportConfiguration.Password;
        }
        public IUser EndUser { get; set; }
        public BusinessSearch.ISearchOption SearchOption { get; set; }
        public BusinessReport.IReportOption ReportOption { get; set; }
    }
}
