﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class BankruptcyConfiguration
    {
        public string BankruptcyFCRAUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
        public string FCRAUserName { get; set; }
        public string FCRAPassword { get; set; }

        public string BankruptcyNonFCRAUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
        public string NonFCRAUserName { get; set; }
        public string NonFCRAPassword { get; set; }

        [JsonConverter(typeof(InterfaceConverter<Bankruptcy.IUser, Bankruptcy.User>))]
        public Bankruptcy.IUser EndUser { get; set; }

        [JsonConverter(typeof(InterfaceConverter<Bankruptcy.ISearchOption, Bankruptcy.SearchOption>))]
        public Bankruptcy.ISearchOption Options { get; set; }
    }
}