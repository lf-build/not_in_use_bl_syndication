﻿namespace LendFoundry.Syndication.LexisNexis
{
    public interface IBusinessReportConfiguration : IServiceConfiguration
    {
        IUser EndUser { get; set; }
        BusinessSearch.ISearchOption SearchOption { get; set; }
        BusinessReport.IReportOption ReportOption { get; set; }
    }
}
