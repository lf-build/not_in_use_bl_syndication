﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class CriminalRecordConfiguration
    {
            public string CriminalRecordUrl { get; set; }// = "https://wsonline.seisint.com/Demo/WsAccurintFCRA/?ver_=1.89";
            public string UserName { get; set; }
            public string Password { get; set; }

            [JsonConverter(typeof(InterfaceConverter<CriminalRecord.IUser, CriminalRecord.User>))]
            public CriminalRecord.IUser EndUser { get; set; }

            [JsonConverter(typeof(InterfaceConverter<ISearchOption, SearchOption>))]
            public ISearchOption Options { get; set; }
       
    }
}
