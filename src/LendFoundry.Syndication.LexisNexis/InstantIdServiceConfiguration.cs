﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class InstantIdServiceConfiguration
    {
        public string InstantIdUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.85";
        public string UserName { get; set; }
        public string Password { get; set; }

        [JsonConverter(typeof(InterfaceConverter<InstantId.IUser, InstantId.User>))]
        public InstantId.IUser EndUser { get; set; }

        [JsonConverter(typeof(InterfaceConverter<InstantId.IInstantIdOption2, InstantId.IInstantIdOption2>))]
        public InstantId.IInstantIdOption2 Options { get; set; }
    }
}