﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IException
    {
        string Source { get; set; }
        int Code { get; set; }
        string Location { get; set; }
        string Message { get; set; }
    }
}
