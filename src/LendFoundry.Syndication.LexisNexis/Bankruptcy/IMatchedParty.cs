﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IMatchedParty
    {
        string PartyType { get; set; }

        string UniqueId { get; set; }

        IName ParsedParty { get; set; }

        IAddress Address { get; set; }
    }
}
