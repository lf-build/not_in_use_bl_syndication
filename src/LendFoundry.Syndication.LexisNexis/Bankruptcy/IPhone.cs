﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IPhone
    {
        string Phone10 { get; set; }
        string Fax { get; set; }
        string TimeZone { get; set; }
    }
}
