﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class MatchedParty : IMatchedParty
    {
        public MatchedParty(BankruptcyMatchedParty2 record)
        {
            if (record == null)
                return;
            PartyType = record.PartyType;
            //ParsedParty = new Name(record.ParsedParty);
            //Address = record.Address;
            OriginName = record.OriginName;
        }
        public string PartyType { get; set; }

        public string UniqueId { get; set; }

        public IName ParsedParty { get; set; }

        public IAddress Address { get; set; }

        public string OriginName { get; set; }
    }
}
