﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IName
    {
        string CompanyName { get; set; }
        string First { get; set; }
        string Full { get; set; }
        string Last { get; set; }
        string Middle { get; set; }
        string Prefix { get; set; }
        string Suffix { get; set; }
        string Type { get; set; }
        string UniqueId { get; set; }
    }
}