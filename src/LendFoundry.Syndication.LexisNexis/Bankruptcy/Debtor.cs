﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class Debtor : IDebtor
    {
        public Debtor()
        {
        }

        public Debtor(BankruptcySearch3Debtor debtorRedord)
        {
            if (debtorRedord == null)
                return;

            TaxId = debtorRedord.TaxId;
            AppendedTaxId = debtorRedord.AppendedTaxId;
            UniqueId = debtorRedord.UniqueId;
            BusinessId = debtorRedord.BusinessId;
            SSN = debtorRedord.SSN;
            AppendedSSN = debtorRedord.AppendedSSN;
            AppendedTaxId = debtorRedord.AppendedTaxId;
            Names = debtorRedord.Names != null ? new List<IName>(debtorRedord.Names.Select(name => new Name(name))) : null;
            Addresses = debtorRedord.Addresses != null ? new List<IAddress>(debtorRedord.Addresses.Select(add => new Address(add))) : null;
        }

        public string TaxId { get; set; }

        public string AppendedTaxId { get; set; }

        public string UniqueId { get; set; }

        public string BusinessId { get; set; }

        public string SSN { get; set; }

        public string AppendedSSN { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
    }
}