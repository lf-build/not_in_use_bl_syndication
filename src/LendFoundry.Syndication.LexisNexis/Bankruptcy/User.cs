﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class User : IUser
    {
        public User() { }

        public User(Proxy.BankruptcySearch.ServiceReference.User user)
        {
            if (user == null)
                return;

            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GLBPurpose;
            DLPurpose = user.DLPurpose;
            //EndUser = new EndUserInfo(user.EndUser);
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

        public string ReferenceCode { get; set; }
        public string BillingCode { get; set; }
        public string QueryId { get; set; }
        public string GLBPurpose { get; set; } = "1";
        public string DLPurpose { get; set; } = "3";
        public IEndUserInfo EndUser { get; set; }
        public int MaxWaitSeconds { get; set; }
        public string AccountNumber { get; set; }

        
    }
}
