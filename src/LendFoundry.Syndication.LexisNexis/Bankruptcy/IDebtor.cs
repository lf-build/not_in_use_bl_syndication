﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IDebtor
    {
        string TaxId { get; set; }

        string AppendedTaxId { get; set; }

        string UniqueId { get; set; }

        string BusinessId { get; set; }

        string SSN { get; set; }

        string AppendedSSN { get; set; }

        List<IName> Names { get; set; }

        List<IAddress> Addresses { get; set; }

    }
}
