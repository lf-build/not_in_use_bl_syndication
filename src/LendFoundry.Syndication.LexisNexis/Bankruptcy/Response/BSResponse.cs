﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Response
{
    [XmlRoot(ElementName = "Debtors")]
    public class BsDbtor
    {
        [XmlElement(ElementName = "Debtors")]
        List<Debtor> Debtors { get; set; }
    }

    [XmlRoot(ElementName = "Debtor")]
    public class Debtor
    {
        [XmlElement(ElementName = "TaxId")]
        public string TaxId { get; set; }
        [XmlElement(ElementName = "AppendedTaxId")]
        public string AppendedTaxId { get; set; }
        [XmlElement(ElementName = "UniqueId")]
        public string UniqueId { get; set; }
        [XmlElement(ElementName = "BusinessId")]
        public string BusinessId { get; set; }
        [XmlElement(ElementName = "SSN")]
        public string SSN { get; set; }
        [XmlElement(ElementName = "AppendedSSN")]
        public string AppendedSSN { get; set; }
        [XmlElement(ElementName = "Names")]
        public List<Name> Names { get; set; }
        //[XmlElement(ElementName = "Addresses")]
        //public List<Address> Addresses { get; set; }
    }

    [XmlRoot(ElementName = "Name")]
    public class Name
    {
        [XmlElement(ElementName = "Full")]
        public string Full { get; set; }

        [XmlElement(ElementName = "First")]
        public string First { get; set; }

        [XmlElement(ElementName = "Middle")]
        public string Middle { get; set; }

        [XmlElement(ElementName = "Last")]
        public string Last { get; set; }

        [XmlElement(ElementName = "Suffix")]
        public string Suffix { get; set; }
        [XmlElement(ElementName = "Prefix")]
        public string Prefix { get; set; }

        [XmlElement(ElementName = "CompanyName")]
        public string CompanyName { get; set; }

        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }

        [XmlElement(ElementName = "UniqueId")]
        public string UniqueId { get; set; }
    }
    public class BankruptcySearchRecord
    {
        public List<Debtor> Debtors { get; set; }
        public bool AlsoFound { get; set; }

        public string TMSId { get; set; }

        public string AssetsForUnsecured { get; set; }

        public string CaseNumber { get; set; }

        public string CourtLocation { get; set; }

        public string CourtName { get; set; }

        public string Chapter { get; set; }

        public string CourtCode { get; set; }

        public string OriginalChapter { get; set; }

        public string FilerType { get; set; }

        public string FilingStatus { get; set; }

        public string Disposition { get; set; }

        public List<IPhone> Phones { get; set; }

        public Date FilingDate { get; set; }

        public Date OriginalFilingDate { get; set; }

        public MatchedParty MatchedParty { get; set; }
    }

    [XmlRoot(ElementName = "Exceptions")]
    public class Exception
    {
        [XmlElement(ElementName = "Item")]
        List<BSItem> Item { get; set; }
    }

    [XmlRoot(ElementName = "Item")]
    public class BSItem
    {
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "Code")]
        public int Code { get; set; }
        [XmlElement(ElementName = "Location")]
        public string Location { get; set; }
        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }

    }

    [XmlRoot(ElementName = "Header")]
    public class BSResponseHeader
    {
        [XmlElement(ElementName = "Status")]
        public int Status { get; set; }

        [XmlElement(ElementName = "Message")]
        public string Message { get; set; }

        [XmlElement(ElementName = "QueryId")]
        public string QueryId { get; set; }

        [XmlElement(ElementName = "TransactionId")]
        public string TransactionId { get; set; }

        [XmlElement(ElementName = "Exceptions")]
        public List<Exception> Exceptions { get; set; }

    }

    [XmlRoot(ElementName = "response")]
    public class BSResponse
    {
        [XmlElement(ElementName = "Header")]
        public BSResponseHeader Header { get; set; }
        [XmlElement(ElementName = "RecordCount")]
        public int RecordCount { get; set; }
        [XmlElement(ElementName = "Record")]
        public List<BankruptcySearchRecord> Records { get; set; }
    }
}
