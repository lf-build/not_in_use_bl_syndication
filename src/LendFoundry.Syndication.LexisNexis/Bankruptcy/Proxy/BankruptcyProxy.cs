﻿using System;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using System.Threading.Tasks;
using System.Net;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy
{
    public class BankruptcyProxy : IBankruptcyProxy
    {
        public BankruptcyProxy(LexisNexisConfiguration configuration)
        {
            if (configuration.Bankruptcy == null)
                throw new ArgumentNullException(nameof(configuration.Bankruptcy));

            if (string.IsNullOrWhiteSpace(configuration.Bankruptcy.BankruptcyFCRAUrl))
                throw new ArgumentNullException(nameof(configuration.Bankruptcy.BankruptcyFCRAUrl));

            if (string.IsNullOrWhiteSpace(configuration.Bankruptcy.FCRAUserName))
                throw new ArgumentNullException(nameof(configuration.Bankruptcy.FCRAUserName));

            if (string.IsNullOrWhiteSpace(configuration.Bankruptcy.FCRAPassword))
                throw new ArgumentNullException(nameof(configuration.Bankruptcy.FCRAPassword));

            if (configuration.Bankruptcy.EndUser == null)
                throw new ArgumentNullException(nameof(configuration.Bankruptcy.EndUser));

            if (configuration.Bankruptcy.Options == null)
                throw new ArgumentNullException(nameof(configuration.Bankruptcy.Options));

            Configuration = configuration;
        }

        private LexisNexisConfiguration Configuration { get; }

        public async Task<FcraBankruptcySearch3Response> BankruptcySearch(BankruptcySearch.ServiceReference.User user, FcraBankruptcySearch3By searchBy, FcraBankruptcySearch3Option options)
        {
            return await Task.Run(() =>
            {

                if (searchBy == null)
                    throw new ArgumentNullException(nameof(searchBy));

                var soapClient = new BankruptcySearch.ServiceReference.WsAccurintFCRA
                {
                    Credentials = new NetworkCredential(Configuration.Bankruptcy.FCRAUserName, Configuration.Bankruptcy.FCRAPassword),
                    Url = Configuration.Bankruptcy.BankruptcyFCRAUrl
                };

                var soapResponse = soapClient.BankruptcySearch3(user, searchBy, options);
                return soapResponse;
            });
        }

        public async Task<FcraBankruptcyReport3Response> BankruptcyReport(BankruptcyReport.ServiceReference.User user, FcraBankruptcyReport3By reportBy, FcraBankruptcyReport3Option options)
        {
            return await Task.Run(() =>
            {
                if (reportBy == null)
                    throw new ArgumentNullException(nameof(reportBy));

                var soapClient = new BankruptcyReport.ServiceReference.WsAccurintFCRA
                {
                    Credentials = new NetworkCredential(Configuration.Bankruptcy.FCRAUserName, Configuration.Bankruptcy.FCRAPassword),
                    Url = Configuration.Bankruptcy.BankruptcyFCRAUrl
                };

                var soapResponse = soapClient.BankruptcyReport3(user, options, reportBy);
                return soapResponse;
            });
        }

        public async Task<BankruptcySearch2Response> NonFCRABankruptcySearch(BankruptcySearch.NonFCRAServiceReference.User user, BankruptcySearch2By searchBy, BankruptcySearch2Option options)
        {
            return await Task.Run(() =>
            {

                if (searchBy == null)
                    throw new ArgumentNullException(nameof(searchBy));

                var soapClient = new BankruptcySearch.NonFCRAServiceReference.WsAccurint
                {
                    Credentials = new NetworkCredential(Configuration.Bankruptcy.NonFCRAUserName, Configuration.Bankruptcy.NonFCRAPassword),
                    Url = Configuration.Bankruptcy.BankruptcyNonFCRAUrl
                };

                var soapResponse = soapClient.BankruptcySearch2(user, searchBy, options);
                return soapResponse;
            });
        }

        public async Task<BankruptcyReport2Response> NonFCRABankruptcyReport(BankruptcyReport.NonFCRAServiceReference.User user, BankruptcyReport2By reportBy, BankruptcyReport2Option options)
        {
            return await Task.Run(() =>
            {
                if (reportBy == null)
                    throw new ArgumentNullException(nameof(reportBy));

                var soapClient = new BankruptcyReport.NonFCRAServiceReference.WsAccurint
                {
                    Credentials = new NetworkCredential(Configuration.Bankruptcy.NonFCRAUserName, Configuration.Bankruptcy.NonFCRAPassword),
                    Url = Configuration.Bankruptcy.BankruptcyNonFCRAUrl
                };
               
                var soapResponse = soapClient.BankruptcyReport2(user, options, reportBy);
                return soapResponse;
            });
        }
    }
}