﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IBankruptcySearchRecord
    {
        [JsonConverter(typeof(InterfaceListConverter<IDebtor,Debtor>))]
        List<IDebtor> Debtors { get; set; }
        //bool AlsoFound { get; set; }

         string TMSId { get; set; }

        // string AssetsForUnsecured { get; set; }

        // string CaseNumber { get; set; }

        // string CourtLocation { get; set; }

        // string CourtName { get; set; }

        // string Chapter { get; set; }

        // string CourtCode { get; set; }

        // string OriginalChapter { get; set; }

        // string FilerType { get; set; }

        // string FilingStatus { get; set; }

        // string Disposition { get; set; }

        //[JsonConverter(typeof(InterfaceListConverter<IPhone,Phone>))]
        //List<IPhone> Phones { get; set; }

        //[JsonConverter(typeof(InterfaceConverter<IDate, Date>))]
        //IDate FilingDate { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<IDate, Date>))]
        //IDate OriginalFilingDate { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<IMatchedParty, MatchedParty>))]
        //IMatchedParty MatchedParty { get; set; }
    }
}