﻿using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference
{
    public partial class FcraBankruptcySearch3By
    {
        public FcraBankruptcySearch3By()
        { }
        public FcraBankruptcySearch3By(ISearchBankruptcyRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            Name = new Name
            {
                First = searchBy.FirstName,
                Last = searchBy.LastName
            };
            Address = new Address
            {
                StreetAddress1 = searchBy.AddressLine1,
                StreetAddress2 = searchBy.AddressLine2,
                City = searchBy.City,
                State = searchBy.State,
                Zip4 = searchBy.Zip4,
                Zip5 = searchBy.Zip5
            };
        }
    }
}

