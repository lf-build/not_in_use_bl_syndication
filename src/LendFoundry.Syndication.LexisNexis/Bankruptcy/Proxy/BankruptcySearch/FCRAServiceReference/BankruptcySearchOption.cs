﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference
{
    public partial class FcraBankruptcySearch3Option
    {      
        public FcraBankruptcySearch3Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;           
        }
    }
}
