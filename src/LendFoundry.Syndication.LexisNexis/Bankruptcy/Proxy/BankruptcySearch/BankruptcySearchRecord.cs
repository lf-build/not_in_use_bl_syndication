﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class BankruptcySearchRecord : IBankruptcySearchRecord
    {
        public BankruptcySearchRecord() { }
        public BankruptcySearchRecord(BankruptcySearch3Record record)
        {

            if (record == null)
                return;
            Debtors = record.Debtors != null ? new List<IDebtor>(record.Debtors.Select(debtor => new Debtor(debtor))) : null;
            //AlsoFound = record.AlsoFound;
            TMSId = record.TMSId;
            //AssetsForUnsecured = record.AssetsForUnsecured;
             CaseNumber = record.CaseNumber;
            //CourtLocation = record.CourtLocation;
            //CourtName = record.CourtName;
            //Chapter = record.Chapter;
            //CourtCode = record.CourtCode;
            //AlsoFound = record.AlsoFound;
        }

        //public BankruptcySearchRecord(BankruptcySearch2Record record)
        //{

        //    if (record == null)
        //        return;
        //    Debtors = record.Debtors != null ? new List<IDebtor>(record.Debtors.Select(debtor => new Debtor(debtor))) : null;
        //    AlsoFound = record.AlsoFound;
        //    TMSId = record.TMSId;
        //    //AssetsForUnsecured = record.use;
        //    CaseNumber = record.CaseNumber;
        //    CourtLocation = record.CourtLocation;
        //    CourtName = record.CourtName;
        //    Chapter = record.Chapter;
        //    CourtCode = record.CourtCode;
        //    AlsoFound = record.AlsoFound;
        //    AlsoFoundSpecified = record.AlsoFoundSpecified;
        //    OriginalChapter = record.OriginalChapter;
        //    FilerType = record.FilerType;
        //    FilingDate = record.FilingDate != null ? new BusinessReport.Date { Day = record.FilingDate.Day, Month = record.FilingDate.Month, Year = record.FilingDate.Year } : null;
        //    FilingStatus = record.FilingStatus;
        //    Disposition = record.Disposition;
        //    OriginalFilingDate = record.OriginalFilingDate != null ? new BusinessReport.Date { Day = record.OriginalFilingDate.Day, Month = record.OriginalFilingDate.Month, Year = record.OriginalFilingDate.Year } : null;
        //    MatchedParty = record.MatchedParty;
        //}
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> Debtors { get; set; }
        public string CaseNumber { get; set; }

        // public bool AlsoFound { get; set; }

        public string TMSId { get; set; }

        //public string AssetsForUnsecured { get; set; }


        //public string CourtLocation { get; set; }

        //public string CourtName { get; set; }

        //public string Chapter { get; set; }

        //public string CourtCode { get; set; }

        //public string OriginalChapter { get; set; }

        //public string FilerType { get; set; }

        //public string FilingStatus { get; set; }

        //public string Disposition { get; set; }

        //public List<IPhone> Phones { get; set; }

        //public IDate FilingDate { get; set; }

        //public IDate OriginalFilingDate { get; set; }

        //public IMatchedParty MatchedParty { get; set; }

        //public bool AlsoFoundSpecified { get; set; }

        //public string CorpFlag { get; set; }

        //public string ExternalKey { get; set; }
    }
}
