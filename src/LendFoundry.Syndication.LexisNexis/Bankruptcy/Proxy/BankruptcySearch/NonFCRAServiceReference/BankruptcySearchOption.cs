﻿using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference
{
    public partial class BankruptcySearch2Option
    {
        public BankruptcySearch2Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            StartingRecord = searchOption.StartingRecord;
            ReturnCount = searchOption.StartingRecord;
        }
    }
}
