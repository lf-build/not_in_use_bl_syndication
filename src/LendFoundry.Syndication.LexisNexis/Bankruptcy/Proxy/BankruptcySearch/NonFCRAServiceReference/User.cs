﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference
{
   
    public partial class User
    {
        public User()
        { }
        public User(IUser user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GLBPurpose;
            DLPurpose = user.GLBPurpose;
            EndUser = new EndUserInfo
            {
                City = user.EndUser.City,
                CompanyName = user.EndUser.CompanyName,
                State = user.EndUser.State,
                StreetAddress1 = user.EndUser.StreetAddress1,
                Zip5 = user.EndUser.Zip5
            };
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }

    }
}

