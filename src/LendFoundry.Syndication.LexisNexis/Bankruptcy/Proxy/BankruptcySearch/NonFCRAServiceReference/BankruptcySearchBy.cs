﻿using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference
{
    public partial class BankruptcySearch2By
    {
        public BankruptcySearch2By()
        { }
        public BankruptcySearch2By(ISearchBankruptcyRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            Name = new Name
            {
                First = searchBy.FirstName,
                Last = searchBy.LastName
            };
        }
    }
}

