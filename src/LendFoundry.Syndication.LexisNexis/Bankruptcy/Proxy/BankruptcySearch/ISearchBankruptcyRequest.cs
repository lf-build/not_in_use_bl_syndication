﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface ISearchBankruptcyRequest
    {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip4 { get; set; }
        string Zip5 { get; set; }
    }
}
