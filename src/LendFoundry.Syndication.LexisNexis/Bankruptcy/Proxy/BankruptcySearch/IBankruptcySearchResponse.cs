﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IBankruptcySearchResponse
    {
        IResponseHeader Header { get; set; }
        int RecordCount { get; set; }
        List<IBankruptcySearchRecord> Records { get; set; }
    }
}
