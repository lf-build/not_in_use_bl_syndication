﻿using System.Collections.Generic;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using Newtonsoft.Json;
using LendFoundry.Foundation.Services;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy
{
    public class BankruptcySearchResponse : IBankruptcySearchResponse
    {
        public BankruptcySearchResponse()
        {
        }
        public BankruptcySearchResponse(FcraBankruptcySearch3Response searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if (searchResponse.Records != null)
            {
                Records = new List<IBankruptcySearchRecord>(searchResponse.Records.Select(record => new BankruptcySearchRecord(record)));
            }
        }
        public BankruptcySearchResponse(BankruptcySearch2Response searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if (searchResponse.Records != null)
            {
                //Records = new List<IBankruptcySearchRecord>(searchResponse.Records.Select(record => new BankruptcySearchRecord(record)));
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }

        public int RecordCount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcySearchRecord, BankruptcySearchRecord>))]
        public List<IBankruptcySearchRecord> Records { get; set; }
    }
}