﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy
{
    public interface IBankruptcyProxy
    {
        Task<FcraBankruptcySearch3Response> BankruptcySearch(BankruptcySearch.ServiceReference.User User, FcraBankruptcySearch3By SearchBy, FcraBankruptcySearch3Option Options);
        Task<FcraBankruptcyReport3Response> BankruptcyReport(BankruptcyReport.ServiceReference.User user, FcraBankruptcyReport3By reportBy, FcraBankruptcyReport3Option options);
        Task<BankruptcySearch2Response> NonFCRABankruptcySearch(BankruptcySearch.NonFCRAServiceReference.User user, BankruptcySearch2By searchBy, BankruptcySearch2Option options);
        Task<BankruptcyReport2Response> NonFCRABankruptcyReport(BankruptcyReport.NonFCRAServiceReference.User user, BankruptcyReport2By reportBy, BankruptcyReport2Option options);
    }
}   