﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference
{
    public partial class FcraBankruptcyReport3Option
    {
        public FcraBankruptcyReport3Option()
        { }
        public FcraBankruptcyReport3Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;

            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
