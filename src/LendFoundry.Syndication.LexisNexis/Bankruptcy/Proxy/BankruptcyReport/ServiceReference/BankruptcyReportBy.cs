﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference
{
    public partial class FcraBankruptcyReport3By
    {
        public FcraBankruptcyReport3By()
        { }
        public FcraBankruptcyReport3By(IBankruptcyFcraReportRequest reportBy)
        {
            if (reportBy == null)
                return;

            UniqueId = reportBy.UniqueId;
           
        }
    }
}
