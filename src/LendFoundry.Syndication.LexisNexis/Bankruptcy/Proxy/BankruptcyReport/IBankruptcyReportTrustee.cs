﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public interface IBankruptcyReportTrustee
    {
        string UniqueId { get; set; }
        string Title { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        List<IName> Names { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Address { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        List<IPhone> Phones { get; set; }
        BusinessIdentity BusinessIds { get; set; }
        List<string> Emails { get; set; }
        string AppendedSSN { get; set; }
        string TaxId { get; set; }
        string AppendedTaxId { get; set; }
    }
}
