using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportRecord : IBankruptcyReportRecord
    {
        public BankruptcyReportRecord()
        {
        }

        public BankruptcyReportRecord(BankruptcyReport3Record record, Boolean isFcra = true)
        {
            if (record == null)
                return;

            CaseNumber = record.CaseNumber;
            IsFcra = isFcra;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CaseType = record.CaseType;
            FilingDate = record.FilingDate != null ? new BusinessReport.Date { Day = record.FilingDate.Day, Month = record.FilingDate.Month, Year = record.FilingDate.Year } : null;
            OriginalFilingDate = record.OriginalFilingDate != null ? new BusinessReport.Date { Day = record.OriginalFilingDate.Day, Month = record.OriginalFilingDate.Month, Year = record.OriginalFilingDate.Year } : null;
            ReopenDate = record.ReopenDate != null ? new BusinessReport.Date { Day = record.ReopenDate.Day, Month = record.ReopenDate.Month, Year = record.ReopenDate.Year } : null;
            ClosedDate = record.ClosedDate != null ? new BusinessReport.Date { Day = record.ClosedDate.Day, Month = record.ClosedDate.Month, Year = record.ClosedDate.Year } : null;
            CourtName = record.CourtName;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CorpFlag = record.CorpFlag;
            FilingType = record.FilingType;
            FilerType = record.FilerType;
            FilingStatus = record.FilingStatus;
            FilingJurisdiction = record.FilingJurisdiction;
            Chapter = record.Chapter;
            OriginalChapter = record.OriginalChapter;
            JudgeName = record.JudgeName;
            Meeting = record.Meeting != null ? new BankruptcyReportMeeting(record.Meeting) : null;
            JudgeIdentification = record.JudgeIdentification;
            ClaimsDeadline = record.ClaimsDeadline != null ? new BusinessReport.Date { Day = record.ClaimsDeadline.Day, Month = record.ClaimsDeadline.Month, Year = record.ClaimsDeadline.Year } : null;
            ComplaintDeadline = record.ComplaintDeadline != null ? new BusinessReport.Date { Day = record.ComplaintDeadline.Day, Month = record.ComplaintDeadline.Month, Year = record.ComplaintDeadline.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
            Disposition = record.Disposition;
            SelfRepresented = record.SelfRepresented;
            AssetsForUnsecured = record.AssetsForUnsecured;
            Assets = record.Assets;
            Liabilities = record.Liabilities;
            AssetsForUnsecured = record.AssetsForUnsecured;
            StatusHistory = record.DischargeDate != null ? new List<IBankruptcyReportStatus>(record.StatusHistory.Select(status => new BankruptcyReportStatus(status))) : null;
            Comments = record.Comments != null ? new List<IBankruptcyReportComment>(record.Comments.Select(comment => new BankruptcyReportComment(comment))) : null;
            Trustee = record.Trustee != null ? new List<IBankruptcyReportTrustee> { new BankruptcyReportTrustee(record.Trustee) } : new List<IBankruptcyReportTrustee>();
            //record.Trustee != null ? new BankruptcyReportTrustee(record.Trustee) : null;
            Attorneys = record.Attorneys != null ? new List<IBankruptcyReportAttorney>(record.Attorneys.Select(attorneys => new BankruptcyReportAttorney(attorneys))) : null;
            Debtors = record.Debtors != null ? new List<IBankruptcyReportDebtor>(record.Debtors.Select(debtor => new BankruptcyReportDebtor(debtor))) : null;
        }
        public BankruptcyReportRecord(BankruptcyReport2Record record)
        {
            if (record == null)
                return;
            CaseNumber = record.CaseNumber;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CaseType = record.CaseType;
            FilingDate = record.FilingDate != null ? new BusinessReport.Date { Day = record.FilingDate.Day, Month = record.FilingDate.Month, Year = record.FilingDate.Year } : null;
            OriginalFilingDate = record.OriginalFilingDate != null ? new BusinessReport.Date { Day = record.OriginalFilingDate.Day, Month = record.OriginalFilingDate.Month, Year = record.OriginalFilingDate.Year } : null;
            ReopenDate = record.ReopenDate != null ? new BusinessReport.Date { Day = record.ReopenDate.Day, Month = record.ReopenDate.Month, Year = record.ReopenDate.Year } : null;
            ClosedDate = record.ClosedDate != null ? new BusinessReport.Date { Day = record.ClosedDate.Day, Month = record.ClosedDate.Month, Year = record.ClosedDate.Year } : null;
            CourtName = record.CourtName;
            CourtLocation = record.CourtLocation;
            CourtCode = record.CourtCode;
            CorpFlag = record.CorpFlag;
            FilingType = record.FilingType;
            FilerType = record.FilerType;
            FilingStatus = record.FilingStatus;
            FilingJurisdiction = record.FilingJurisdiction;
            Chapter = record.Chapter;
            OriginalChapter = record.OriginalChapter;
            JudgeName = record.JudgeName;
            Meeting = record.Meeting != null ? new BankruptcyReportMeeting(record.Meeting) : null;
            JudgeIdentification = record.JudgeIdentification;
            ClaimsDeadline = record.ClaimsDeadline != null ? new BusinessReport.Date { Day = record.ClaimsDeadline.Day, Month = record.ClaimsDeadline.Month, Year = record.ClaimsDeadline.Year } : null;
            ComplaintDeadline = record.ComplaintDeadline != null ? new BusinessReport.Date { Day = record.ComplaintDeadline.Day, Month = record.ComplaintDeadline.Month, Year = record.ComplaintDeadline.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
            Disposition = record.Disposition;
            SelfRepresented = record.SelfRepresented.ToString();
            AssetsForUnsecured = record.AssetsForUnsecured.ToString();
            Assets = record.Assets;
            Liabilities = record.Liabilities;
            AssetsForUnsecured = record.AssetsForUnsecured.ToString();
            StatusHistory = record.StatusHistory != null ? new List<IBankruptcyReportStatus>(record.StatusHistory.Select(status => new BankruptcyReportStatus(status))) : new List<IBankruptcyReportStatus>();
            Comments = record.Comments != null ? new List<IBankruptcyReportComment>(record.Comments.Select(comment => new BankruptcyReportComment(comment))) : new List<IBankruptcyReportComment>();
            Trustee = record.Trustees != null ? new List<IBankruptcyReportTrustee>(record.Trustees.Select(trustee => new BankruptcyReportTrustee(trustee))) : new List<IBankruptcyReportTrustee>();
            Attorneys = record.Attorneys != null ? new List<IBankruptcyReportAttorney>(record.Attorneys.Select(attorneys => new BankruptcyReportAttorney(attorneys))) : new List<IBankruptcyReportAttorney>();
            Debtors = record.Debtors != null ? new List<IBankruptcyReportDebtor>(record.Debtors.Select(debtor => new BankruptcyReportDebtor(debtor))) : new List<IBankruptcyReportDebtor>();

        }
        public string CaseNumber { get; set; }
        public Boolean IsFcra { get; set; }

        public string CourtLocation { get; set; }

        public string CourtCode { get; set; }

        public string CaseType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate FilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OriginalFilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ReopenDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ClosedDate { get; set; }

        public string CourtName { get; set; }

        public string FilingType { get; set; }

        public string FilerType { get; set; }

        public string CorpFlag { get; set; }

        public string FilingStatus { get; set; }

        public string FilingJurisdiction { get; set; }

        public string Chapter { get; set; }

        public string OriginalChapter { get; set; }

        public string JudgeName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBankruptcyReportMeeting, BankruptcyReportMeeting>))]
        public IBankruptcyReportMeeting Meeting { get; set; }

        public string JudgeIdentification { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ClaimsDeadline { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ComplaintDeadline { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DischargeDate { get; set; }

        public string Disposition { get; set; }

        public string SelfRepresented { get; set; }

        public string AssetsForUnsecured { get; set; }

        public string Assets { get; set; }

        public string Liabilities { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportStatus, BankruptcyReportStatus>))]
        public List<IBankruptcyReportStatus> StatusHistory { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportComment, BankruptcyReportComment>))]
        public List<IBankruptcyReportComment> Comments { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportTrustee, BankruptcyReportTrustee>))]
        public List<IBankruptcyReportTrustee> Trustee { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportAttorney, BankruptcyReportAttorney>))]
        public List<IBankruptcyReportAttorney> Attorneys { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportDebtor, BankruptcyReportDebtor>))]
        public List<IBankruptcyReportDebtor> Debtors { get; set; }

    }
}