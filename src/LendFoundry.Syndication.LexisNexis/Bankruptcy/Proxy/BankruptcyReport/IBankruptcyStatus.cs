﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public interface IBankruptcyReportStatus
    {

         string Type { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate Date { get; set; }
    }
}
