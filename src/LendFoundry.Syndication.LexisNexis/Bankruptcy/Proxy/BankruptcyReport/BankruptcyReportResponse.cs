using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportResponse : IBankruptcyReportResponse
    {
        public BankruptcyReportResponse()
        {
        }

        public BankruptcyReportResponse(FcraBankruptcyReport3Response response, Boolean isFcra = true)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.BankruptcyReportRecords != null)
            {
                Records = new List<IBankruptcyReportRecord>(response.BankruptcyReportRecords.Select(record => new BankruptcyReportRecord(record, isFcra)));
            }
        }

        public BankruptcyReportResponse(BankruptcyReport2Response response)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.BankruptcyReportRecords != null)
            {
                Records = new List<IBankruptcyReportRecord>(response.BankruptcyReportRecords.Select(record => new BankruptcyReportRecord(record)));
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }

        public int RecordCount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportRecord, BankruptcyReportRecord>))]
        public List<IBankruptcyReportRecord> Records { get; set; }
    }
}