﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class Phone : IPhone
    {
        public Phone()
        { }
        public Phone(NonFCRAServiceReference.PhoneTimeZone phone)
        {
            if (phone == null)
                return;
            Phone10 = phone.Phone10;
            Fax = phone.Fax;
            TimeZone = phone.TimeZone;

        }
        public Phone(ServiceReference.PhoneTimeZone phone)
        {
            if (phone == null)
                return;
            Phone10 = phone.Phone10;
            Fax = phone.Fax;
            TimeZone = phone.TimeZone;

        }
        public string Phone10 { get; set; }


        /// <remarks/>
        public string Fax { get; set; }

        /// <remarks/>
        public string TimeZone { get; set; }
    }
}
