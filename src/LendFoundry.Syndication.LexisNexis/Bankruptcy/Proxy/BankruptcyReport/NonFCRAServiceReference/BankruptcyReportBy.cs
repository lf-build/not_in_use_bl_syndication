﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference
{
    public partial class BankruptcyReport2By
    {
        public BankruptcyReport2By()
        { }
        public BankruptcyReport2By(IBankruptcyReportRequest reportBy)
        {
            if (reportBy == null)
                return;

            UniqueId = reportBy.UniqueId;
            BusinessId = reportBy.BusinessId;
            TMSId = reportBy.TMSId;
        }
    }
}
