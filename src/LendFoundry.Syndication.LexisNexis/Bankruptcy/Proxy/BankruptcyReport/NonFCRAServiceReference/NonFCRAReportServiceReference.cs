﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference
{
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "WsAccurintServiceSoap", Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class WsAccurint : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback BankruptcyReport2OperationCompleted;

        /// <remarks/>
        public WsAccurint()
        {
            this.Url = "https://wsonline.seisint.com/demo/WsAccurint?ver_=2.04";
        }

        /// <remarks/>
        public event BankruptcyReport2CompletedEventHandler BankruptcyReport2Completed;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("WsAccurint/BankruptcyReport2?ver_=2.04", RequestElementName = "BankruptcyReport2Request", RequestNamespace = "http://webservices.seisint.com/WsAccurint", ResponseElementName = "BankruptcyReport2ResponseEx", ResponseNamespace = "http://webservices.seisint.com/WsAccurint", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("response")]
        public BankruptcyReport2Response BankruptcyReport2(User User, BankruptcyReport2Option Options, BankruptcyReport2By ReportBy)
        {
            object[] results = this.Invoke("BankruptcyReport2", new object[] {
                    User,
                    Options,
                    ReportBy});
            return ((BankruptcyReport2Response)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginBankruptcyReport2(User User, BankruptcyReport2Option Options, BankruptcyReport2By ReportBy, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("BankruptcyReport2", new object[] {
                    User,
                    Options,
                    ReportBy}, callback, asyncState);
        }

        /// <remarks/>
        public BankruptcyReport2Response EndBankruptcyReport2(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((BankruptcyReport2Response)(results[0]));
        }

        /// <remarks/>
        public void BankruptcyReport2Async(User User, BankruptcyReport2Option Options, BankruptcyReport2By ReportBy)
        {
            this.BankruptcyReport2Async(User, Options, ReportBy, null);
        }

        /// <remarks/>
        public void BankruptcyReport2Async(User User, BankruptcyReport2Option Options, BankruptcyReport2By ReportBy, object userState)
        {
            if ((this.BankruptcyReport2OperationCompleted == null))
            {
                this.BankruptcyReport2OperationCompleted = new System.Threading.SendOrPostCallback(this.OnBankruptcyReport2OperationCompleted);
            }
            this.InvokeAsync("BankruptcyReport2", new object[] {
                    User,
                    Options,
                    ReportBy}, this.BankruptcyReport2OperationCompleted, userState);
        }

        private void OnBankruptcyReport2OperationCompleted(object arg)
        {
            if ((this.BankruptcyReport2Completed != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.BankruptcyReport2Completed(this, new BankruptcyReport2CompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class User
    {

        private string referenceCodeField;

        private string billingCodeField;

        private string queryIdField;

        private string gLBPurposeField;

        private string dLPurposeField;

        private EndUserInfo endUserField;

        private int maxWaitSecondsField;

        private bool maxWaitSecondsFieldSpecified;

        private string accountNumberField;

        /// <remarks/>
        public string ReferenceCode
        {
            get
            {
                return this.referenceCodeField;
            }
            set
            {
                this.referenceCodeField = value;
            }
        }

        /// <remarks/>
        public string BillingCode
        {
            get
            {
                return this.billingCodeField;
            }
            set
            {
                this.billingCodeField = value;
            }
        }

        /// <remarks/>
        public string QueryId
        {
            get
            {
                return this.queryIdField;
            }
            set
            {
                this.queryIdField = value;
            }
        }

        /// <remarks/>
        public string GLBPurpose
        {
            get
            {
                return this.gLBPurposeField;
            }
            set
            {
                this.gLBPurposeField = value;
            }
        }

        /// <remarks/>
        public string DLPurpose
        {
            get
            {
                return this.dLPurposeField;
            }
            set
            {
                this.dLPurposeField = value;
            }
        }

        /// <remarks/>
        public EndUserInfo EndUser
        {
            get
            {
                return this.endUserField;
            }
            set
            {
                this.endUserField = value;
            }
        }

        /// <remarks/>
        public int MaxWaitSeconds
        {
            get
            {
                return this.maxWaitSecondsField;
            }
            set
            {
                this.maxWaitSecondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxWaitSecondsSpecified
        {
            get
            {
                return this.maxWaitSecondsFieldSpecified;
            }
            set
            {
                this.maxWaitSecondsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class EndUserInfo
    {

        private string companyNameField;

        private string streetAddress1Field;

        private string cityField;

        private string stateField;

        private string zip5Field;

        private string phoneField;

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string StreetAddress1
        {
            get
            {
                return this.streetAddress1Field;
            }
            set
            {
                this.streetAddress1Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string Zip5
        {
            get
            {
                return this.zip5Field;
            }
            set
            {
                this.zip5Field = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyPerson2
    {

        private BusinessIdentity businessIdsField;

        private string idValueField;

        private BankruptcySearch2Name[] namesField;

        private AddressWithRawInfo[] addressesField;

        private PhoneTimeZone[] phonesField;

        private string[] emailsField;

        private string uniqueIdField;

        private string businessIdField;

        private string sSNField;

        private string appendedSSNField;

        private string taxIdField;

        private string appendedTaxIdField;

        /// <remarks/>
        public BusinessIdentity BusinessIds
        {
            get
            {
                return this.businessIdsField;
            }
            set
            {
                this.businessIdsField = value;
            }
        }

        /// <remarks/>
        public string IdValue
        {
            get
            {
                return this.idValueField;
            }
            set
            {
                this.idValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Name", IsNullable = false)]
        public BankruptcySearch2Name[] Names
        {
            get
            {
                return this.namesField;
            }
            set
            {
                this.namesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Address", IsNullable = false)]
        public AddressWithRawInfo[] Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Phone", IsNullable = false)]
        public PhoneTimeZone[] Phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Email", IsNullable = false)]
        public string[] Emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public string UniqueId
        {
            get
            {
                return this.uniqueIdField;
            }
            set
            {
                this.uniqueIdField = value;
            }
        }

        /// <remarks/>
        public string BusinessId
        {
            get
            {
                return this.businessIdField;
            }
            set
            {
                this.businessIdField = value;
            }
        }

        /// <remarks/>
        public string SSN
        {
            get
            {
                return this.sSNField;
            }
            set
            {
                this.sSNField = value;
            }
        }

        /// <remarks/>
        public string AppendedSSN
        {
            get
            {
                return this.appendedSSNField;
            }
            set
            {
                this.appendedSSNField = value;
            }
        }

        /// <remarks/>
        public string TaxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        /// <remarks/>
        public string AppendedTaxId
        {
            get
            {
                return this.appendedTaxIdField;
            }
            set
            {
                this.appendedTaxIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BusinessIdentity
    {

        private long dotIDField;

        private bool dotIDFieldSpecified;

        private long empIDField;

        private bool empIDFieldSpecified;

        private long pOWIDField;

        private bool pOWIDFieldSpecified;

        private long proxIDField;

        private bool proxIDFieldSpecified;

        private long seleIDField;

        private bool seleIDFieldSpecified;

        private long orgIDField;

        private bool orgIDFieldSpecified;

        private long ultIDField;

        private bool ultIDFieldSpecified;

        /// <remarks/>
        public long DotID
        {
            get
            {
                return this.dotIDField;
            }
            set
            {
                this.dotIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DotIDSpecified
        {
            get
            {
                return this.dotIDFieldSpecified;
            }
            set
            {
                this.dotIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long EmpID
        {
            get
            {
                return this.empIDField;
            }
            set
            {
                this.empIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EmpIDSpecified
        {
            get
            {
                return this.empIDFieldSpecified;
            }
            set
            {
                this.empIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long POWID
        {
            get
            {
                return this.pOWIDField;
            }
            set
            {
                this.pOWIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool POWIDSpecified
        {
            get
            {
                return this.pOWIDFieldSpecified;
            }
            set
            {
                this.pOWIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long ProxID
        {
            get
            {
                return this.proxIDField;
            }
            set
            {
                this.proxIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ProxIDSpecified
        {
            get
            {
                return this.proxIDFieldSpecified;
            }
            set
            {
                this.proxIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long SeleID
        {
            get
            {
                return this.seleIDField;
            }
            set
            {
                this.seleIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SeleIDSpecified
        {
            get
            {
                return this.seleIDFieldSpecified;
            }
            set
            {
                this.seleIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long OrgID
        {
            get
            {
                return this.orgIDField;
            }
            set
            {
                this.orgIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OrgIDSpecified
        {
            get
            {
                return this.orgIDFieldSpecified;
            }
            set
            {
                this.orgIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long UltID
        {
            get
            {
                return this.ultIDField;
            }
            set
            {
                this.ultIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UltIDSpecified
        {
            get
            {
                return this.ultIDFieldSpecified;
            }
            set
            {
                this.ultIDFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcySearch2Name
    {

        private string fullField;

        private string firstField;

        private string middleField;

        private string lastField;

        private string suffixField;

        private string prefixField;

        private string companyNameField;

        private string originalNameField;

        private string typeField;

        /// <remarks/>
        public string Full
        {
            get
            {
                return this.fullField;
            }
            set
            {
                this.fullField = value;
            }
        }

        /// <remarks/>
        public string First
        {
            get
            {
                return this.firstField;
            }
            set
            {
                this.firstField = value;
            }
        }

        /// <remarks/>
        public string Middle
        {
            get
            {
                return this.middleField;
            }
            set
            {
                this.middleField = value;
            }
        }

        /// <remarks/>
        public string Last
        {
            get
            {
                return this.lastField;
            }
            set
            {
                this.lastField = value;
            }
        }

        /// <remarks/>
        public string Suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }

        /// <remarks/>
        public string Prefix
        {
            get
            {
                return this.prefixField;
            }
            set
            {
                this.prefixField = value;
            }
        }

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string OriginalName
        {
            get
            {
                return this.originalNameField;
            }
            set
            {
                this.originalNameField = value;
            }
        }

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class AddressWithRawInfo
    {

        private string streetNumberField;

        private string streetPreDirectionField;

        private string streetNameField;

        private string streetSuffixField;

        private string streetPostDirectionField;

        private string unitDesignationField;

        private string unitNumberField;

        private string streetAddress1Field;

        private string streetAddress2Field;

        private string cityField;

        private string stateField;

        private string zip5Field;

        private string zip4Field;

        private string countyField;

        private string postalCodeField;

        private string stateCityZipField;

        private string latitudeField;

        private string longitudeField;

        private string origStreetAddress1Field;

        private string origStreetAddress2Field;

        /// <remarks/>
        public string StreetNumber
        {
            get
            {
                return this.streetNumberField;
            }
            set
            {
                this.streetNumberField = value;
            }
        }

        /// <remarks/>
        public string StreetPreDirection
        {
            get
            {
                return this.streetPreDirectionField;
            }
            set
            {
                this.streetPreDirectionField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string StreetSuffix
        {
            get
            {
                return this.streetSuffixField;
            }
            set
            {
                this.streetSuffixField = value;
            }
        }

        /// <remarks/>
        public string StreetPostDirection
        {
            get
            {
                return this.streetPostDirectionField;
            }
            set
            {
                this.streetPostDirectionField = value;
            }
        }

        /// <remarks/>
        public string UnitDesignation
        {
            get
            {
                return this.unitDesignationField;
            }
            set
            {
                this.unitDesignationField = value;
            }
        }

        /// <remarks/>
        public string UnitNumber
        {
            get
            {
                return this.unitNumberField;
            }
            set
            {
                this.unitNumberField = value;
            }
        }

        /// <remarks/>
        public string StreetAddress1
        {
            get
            {
                return this.streetAddress1Field;
            }
            set
            {
                this.streetAddress1Field = value;
            }
        }

        /// <remarks/>
        public string StreetAddress2
        {
            get
            {
                return this.streetAddress2Field;
            }
            set
            {
                this.streetAddress2Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string Zip5
        {
            get
            {
                return this.zip5Field;
            }
            set
            {
                this.zip5Field = value;
            }
        }

        /// <remarks/>
        public string Zip4
        {
            get
            {
                return this.zip4Field;
            }
            set
            {
                this.zip4Field = value;
            }
        }

        /// <remarks/>
        public string County
        {
            get
            {
                return this.countyField;
            }
            set
            {
                this.countyField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string StateCityZip
        {
            get
            {
                return this.stateCityZipField;
            }
            set
            {
                this.stateCityZipField = value;
            }
        }

        /// <remarks/>
        public string Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }

        /// <remarks/>
        public string OrigStreetAddress1
        {
            get
            {
                return this.origStreetAddress1Field;
            }
            set
            {
                this.origStreetAddress1Field = value;
            }
        }

        /// <remarks/>
        public string OrigStreetAddress2
        {
            get
            {
                return this.origStreetAddress2Field;
            }
            set
            {
                this.origStreetAddress2Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class PhoneTimeZone
    {

        private string phone10Field;

        private string faxField;

        private string timeZoneField;

        /// <remarks/>
        public string Phone10
        {
            get
            {
                return this.phone10Field;
            }
            set
            {
                this.phone10Field = value;
            }
        }

        /// <remarks/>
        public string Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        public string TimeZone
        {
            get
            {
                return this.timeZoneField;
            }
            set
            {
                this.timeZoneField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyReport2Debtor
    {

        private BusinessIdentity businessIdsField;

        private string idValueField;

        private BankruptcySearch2Name[] namesField;

        private AddressWithRawInfo[] addressesField;

        private PhoneTimeZone[] phonesField;

        private string[] emailsField;

        private string uniqueIdField;

        private string businessIdField;

        private string sSNField;

        private string appendedSSNField;

        private string taxIdField;

        private string appendedTaxIdField;

        private string caseIdField;

        private string defendantIdField;

        private string recordIdField;

        private DefendantType defendantTypeField;

        private bool defendantTypeFieldSpecified;

        private string originalCountyField;

        private string sSNSourceField;

        private string sSNSourceDescField;

        private string sSNMatchField;

        private string sSNMatchDescField;

        private string sSNMatchSourceField;

        private string screenField;

        private string screenDescField;

        private string dispositionCodeField;

        private string dispositionCodeDescField;

        private string dispositionTypeField;

        private string dispositionTypeDescField;

        private string dispositionReasonField;

        private Date statusDateField;

        private string holdCaseField;

        private Date dateVacatedField;

        private Date dateTransferredField;

        private string activityReceiptField;

        /// <remarks/>
        public BusinessIdentity BusinessIds
        {
            get
            {
                return this.businessIdsField;
            }
            set
            {
                this.businessIdsField = value;
            }
        }

        /// <remarks/>
        public string IdValue
        {
            get
            {
                return this.idValueField;
            }
            set
            {
                this.idValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Name", IsNullable = false)]
        public BankruptcySearch2Name[] Names
        {
            get
            {
                return this.namesField;
            }
            set
            {
                this.namesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Address", IsNullable = false)]
        public AddressWithRawInfo[] Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Phone", IsNullable = false)]
        public PhoneTimeZone[] Phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Email", IsNullable = false)]
        public string[] Emails
        {
            get
            {
                return this.emailsField;
            }
            set
            {
                this.emailsField = value;
            }
        }

        /// <remarks/>
        public string UniqueId
        {
            get
            {
                return this.uniqueIdField;
            }
            set
            {
                this.uniqueIdField = value;
            }
        }

        /// <remarks/>
        public string BusinessId
        {
            get
            {
                return this.businessIdField;
            }
            set
            {
                this.businessIdField = value;
            }
        }

        /// <remarks/>
        public string SSN
        {
            get
            {
                return this.sSNField;
            }
            set
            {
                this.sSNField = value;
            }
        }

        /// <remarks/>
        public string AppendedSSN
        {
            get
            {
                return this.appendedSSNField;
            }
            set
            {
                this.appendedSSNField = value;
            }
        }

        /// <remarks/>
        public string TaxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        /// <remarks/>
        public string AppendedTaxId
        {
            get
            {
                return this.appendedTaxIdField;
            }
            set
            {
                this.appendedTaxIdField = value;
            }
        }

        /// <remarks/>
        public string CaseId
        {
            get
            {
                return this.caseIdField;
            }
            set
            {
                this.caseIdField = value;
            }
        }

        /// <remarks/>
        public string DefendantId
        {
            get
            {
                return this.defendantIdField;
            }
            set
            {
                this.defendantIdField = value;
            }
        }

        /// <remarks/>
        public string RecordId
        {
            get
            {
                return this.recordIdField;
            }
            set
            {
                this.recordIdField = value;
            }
        }

        /// <remarks/>
        public DefendantType DefendantType
        {
            get
            {
                return this.defendantTypeField;
            }
            set
            {
                this.defendantTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DefendantTypeSpecified
        {
            get
            {
                return this.defendantTypeFieldSpecified;
            }
            set
            {
                this.defendantTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string OriginalCounty
        {
            get
            {
                return this.originalCountyField;
            }
            set
            {
                this.originalCountyField = value;
            }
        }

        /// <remarks/>
        public string SSNSource
        {
            get
            {
                return this.sSNSourceField;
            }
            set
            {
                this.sSNSourceField = value;
            }
        }

        /// <remarks/>
        public string SSNSourceDesc
        {
            get
            {
                return this.sSNSourceDescField;
            }
            set
            {
                this.sSNSourceDescField = value;
            }
        }

        /// <remarks/>
        public string SSNMatch
        {
            get
            {
                return this.sSNMatchField;
            }
            set
            {
                this.sSNMatchField = value;
            }
        }

        /// <remarks/>
        public string SSNMatchDesc
        {
            get
            {
                return this.sSNMatchDescField;
            }
            set
            {
                this.sSNMatchDescField = value;
            }
        }

        /// <remarks/>
        public string SSNMatchSource
        {
            get
            {
                return this.sSNMatchSourceField;
            }
            set
            {
                this.sSNMatchSourceField = value;
            }
        }

        /// <remarks/>
        public string Screen
        {
            get
            {
                return this.screenField;
            }
            set
            {
                this.screenField = value;
            }
        }

        /// <remarks/>
        public string ScreenDesc
        {
            get
            {
                return this.screenDescField;
            }
            set
            {
                this.screenDescField = value;
            }
        }

        /// <remarks/>
        public string DispositionCode
        {
            get
            {
                return this.dispositionCodeField;
            }
            set
            {
                this.dispositionCodeField = value;
            }
        }

        /// <remarks/>
        public string DispositionCodeDesc
        {
            get
            {
                return this.dispositionCodeDescField;
            }
            set
            {
                this.dispositionCodeDescField = value;
            }
        }

        /// <remarks/>
        public string DispositionType
        {
            get
            {
                return this.dispositionTypeField;
            }
            set
            {
                this.dispositionTypeField = value;
            }
        }

        /// <remarks/>
        public string DispositionTypeDesc
        {
            get
            {
                return this.dispositionTypeDescField;
            }
            set
            {
                this.dispositionTypeDescField = value;
            }
        }

        /// <remarks/>
        public string DispositionReason
        {
            get
            {
                return this.dispositionReasonField;
            }
            set
            {
                this.dispositionReasonField = value;
            }
        }

        /// <remarks/>
        public Date StatusDate
        {
            get
            {
                return this.statusDateField;
            }
            set
            {
                this.statusDateField = value;
            }
        }

        /// <remarks/>
        public string HoldCase
        {
            get
            {
                return this.holdCaseField;
            }
            set
            {
                this.holdCaseField = value;
            }
        }

        /// <remarks/>
        public Date DateVacated
        {
            get
            {
                return this.dateVacatedField;
            }
            set
            {
                this.dateVacatedField = value;
            }
        }

        /// <remarks/>
        public Date DateTransferred
        {
            get
            {
                return this.dateTransferredField;
            }
            set
            {
                this.dateTransferredField = value;
            }
        }

        /// <remarks/>
        public string ActivityReceipt
        {
            get
            {
                return this.activityReceiptField;
            }
            set
            {
                this.activityReceiptField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public enum DefendantType
    {

        /// <remarks/>
        I,

        /// <remarks/>
        B,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class Date
    {

        private short yearField;

        private bool yearFieldSpecified;

        private short monthField;

        private bool monthFieldSpecified;

        private short dayField;

        private bool dayFieldSpecified;

        /// <remarks/>
        public short Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool YearSpecified
        {
            get
            {
                return this.yearFieldSpecified;
            }
            set
            {
                this.yearFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short Month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MonthSpecified
        {
            get
            {
                return this.monthFieldSpecified;
            }
            set
            {
                this.monthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short Day
        {
            get
            {
                return this.dayField;
            }
            set
            {
                this.dayField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaySpecified
        {
            get
            {
                return this.dayFieldSpecified;
            }
            set
            {
                this.dayFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyComment
    {

        private string descriptionField;

        private Date filingDateField;

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public Date FilingDate
        {
            get
            {
                return this.filingDateField;
            }
            set
            {
                this.filingDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyStatus
    {

        private string typeField;

        private Date dateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public Date Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class Bankruptcy2Meeting
    {

        private Date dateField;

        private string timeField;

        private string addressField;

        /// <remarks/>
        public Date Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyReport2Record
    {

        private BusinessIdentity businessIdsField;

        private string idValueField;

        private string corpFlagField;

        private string filingTypeField;

        private string filerTypeField;

        private string filingJurisdictionField;

        private string caseTypeField;

        private string caseNumberField;

        private Date filingDateField;

        private Date originalFilingDateField;

        private Date closedDateField;

        private Date reopenDateField;

        private Date convertedDateField;

        private string chapterField;

        private string originalChapterField;

        private string courtNameField;

        private string courtLocationField;

        private string judgeNameField;

        private string judgeIdentificationField;

        private Date claimsDeadlineField;

        private Date complaintDeadlineField;

        private string filingStatusField;

        private string courtCodeField;

        private Date dischargeDateField;

        private string dispositionField;

        private string liabilitiesField;

        private string assetsField;

        private Bankruptcy2Meeting meetingField;

        private TriStateBoolean selfRepresentedField;

        private bool selfRepresentedFieldSpecified;

        private TriStateBoolean assetsForUnsecuredField;

        private bool assetsForUnsecuredFieldSpecified;

        private string methodDismissField;

        private string caseStatusField;

        private string splitCaseField;

        private TriStateBoolean filedInErrorField;

        private bool filedInErrorFieldSpecified;

        private Date dateReclosedField;

        private string trusteeIdField;

        private string caseIdField;

        private Date barDateField;

        private string transferInField;

        private bool deleteFlagField;

        private bool deleteFlagFieldSpecified;

        private BankruptcyStatus[] statusHistoryField;

        private BankruptcyComment[] commentsField;

        private BankruptcyReport2Debtor[] debtorsField;

        private BankruptcyPerson2[] attorneysField;

        private BankruptcyPerson2[] trusteesField;

        /// <remarks/>
        public BusinessIdentity BusinessIds
        {
            get
            {
                return this.businessIdsField;
            }
            set
            {
                this.businessIdsField = value;
            }
        }

        /// <remarks/>
        public string IdValue
        {
            get
            {
                return this.idValueField;
            }
            set
            {
                this.idValueField = value;
            }
        }

        /// <remarks/>
        public string CorpFlag
        {
            get
            {
                return this.corpFlagField;
            }
            set
            {
                this.corpFlagField = value;
            }
        }

        /// <remarks/>
        public string FilingType
        {
            get
            {
                return this.filingTypeField;
            }
            set
            {
                this.filingTypeField = value;
            }
        }

        /// <remarks/>
        public string FilerType
        {
            get
            {
                return this.filerTypeField;
            }
            set
            {
                this.filerTypeField = value;
            }
        }

        /// <remarks/>
        public string FilingJurisdiction
        {
            get
            {
                return this.filingJurisdictionField;
            }
            set
            {
                this.filingJurisdictionField = value;
            }
        }

        /// <remarks/>
        public string CaseType
        {
            get
            {
                return this.caseTypeField;
            }
            set
            {
                this.caseTypeField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public Date FilingDate
        {
            get
            {
                return this.filingDateField;
            }
            set
            {
                this.filingDateField = value;
            }
        }

        /// <remarks/>
        public Date OriginalFilingDate
        {
            get
            {
                return this.originalFilingDateField;
            }
            set
            {
                this.originalFilingDateField = value;
            }
        }

        /// <remarks/>
        public Date ClosedDate
        {
            get
            {
                return this.closedDateField;
            }
            set
            {
                this.closedDateField = value;
            }
        }

        /// <remarks/>
        public Date ReopenDate
        {
            get
            {
                return this.reopenDateField;
            }
            set
            {
                this.reopenDateField = value;
            }
        }

        /// <remarks/>
        public Date ConvertedDate
        {
            get
            {
                return this.convertedDateField;
            }
            set
            {
                this.convertedDateField = value;
            }
        }

        /// <remarks/>
        public string Chapter
        {
            get
            {
                return this.chapterField;
            }
            set
            {
                this.chapterField = value;
            }
        }

        /// <remarks/>
        public string OriginalChapter
        {
            get
            {
                return this.originalChapterField;
            }
            set
            {
                this.originalChapterField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CourtLocation
        {
            get
            {
                return this.courtLocationField;
            }
            set
            {
                this.courtLocationField = value;
            }
        }

        /// <remarks/>
        public string JudgeName
        {
            get
            {
                return this.judgeNameField;
            }
            set
            {
                this.judgeNameField = value;
            }
        }

        /// <remarks/>
        public string JudgeIdentification
        {
            get
            {
                return this.judgeIdentificationField;
            }
            set
            {
                this.judgeIdentificationField = value;
            }
        }

        /// <remarks/>
        public Date ClaimsDeadline
        {
            get
            {
                return this.claimsDeadlineField;
            }
            set
            {
                this.claimsDeadlineField = value;
            }
        }

        /// <remarks/>
        public Date ComplaintDeadline
        {
            get
            {
                return this.complaintDeadlineField;
            }
            set
            {
                this.complaintDeadlineField = value;
            }
        }

        /// <remarks/>
        public string FilingStatus
        {
            get
            {
                return this.filingStatusField;
            }
            set
            {
                this.filingStatusField = value;
            }
        }

        /// <remarks/>
        public string CourtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>
        public Date DischargeDate
        {
            get
            {
                return this.dischargeDateField;
            }
            set
            {
                this.dischargeDateField = value;
            }
        }

        /// <remarks/>
        public string Disposition
        {
            get
            {
                return this.dispositionField;
            }
            set
            {
                this.dispositionField = value;
            }
        }

        /// <remarks/>
        public string Liabilities
        {
            get
            {
                return this.liabilitiesField;
            }
            set
            {
                this.liabilitiesField = value;
            }
        }

        /// <remarks/>
        public string Assets
        {
            get
            {
                return this.assetsField;
            }
            set
            {
                this.assetsField = value;
            }
        }

        /// <remarks/>
        public Bankruptcy2Meeting Meeting
        {
            get
            {
                return this.meetingField;
            }
            set
            {
                this.meetingField = value;
            }
        }

        /// <remarks/>
        public TriStateBoolean SelfRepresented
        {
            get
            {
                return this.selfRepresentedField;
            }
            set
            {
                this.selfRepresentedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SelfRepresentedSpecified
        {
            get
            {
                return this.selfRepresentedFieldSpecified;
            }
            set
            {
                this.selfRepresentedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public TriStateBoolean AssetsForUnsecured
        {
            get
            {
                return this.assetsForUnsecuredField;
            }
            set
            {
                this.assetsForUnsecuredField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AssetsForUnsecuredSpecified
        {
            get
            {
                return this.assetsForUnsecuredFieldSpecified;
            }
            set
            {
                this.assetsForUnsecuredFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string MethodDismiss
        {
            get
            {
                return this.methodDismissField;
            }
            set
            {
                this.methodDismissField = value;
            }
        }

        /// <remarks/>
        public string CaseStatus
        {
            get
            {
                return this.caseStatusField;
            }
            set
            {
                this.caseStatusField = value;
            }
        }

        /// <remarks/>
        public string SplitCase
        {
            get
            {
                return this.splitCaseField;
            }
            set
            {
                this.splitCaseField = value;
            }
        }

        /// <remarks/>
        public TriStateBoolean FiledInError
        {
            get
            {
                return this.filedInErrorField;
            }
            set
            {
                this.filedInErrorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FiledInErrorSpecified
        {
            get
            {
                return this.filedInErrorFieldSpecified;
            }
            set
            {
                this.filedInErrorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Date DateReclosed
        {
            get
            {
                return this.dateReclosedField;
            }
            set
            {
                this.dateReclosedField = value;
            }
        }

        /// <remarks/>
        public string TrusteeId
        {
            get
            {
                return this.trusteeIdField;
            }
            set
            {
                this.trusteeIdField = value;
            }
        }

        /// <remarks/>
        public string CaseId
        {
            get
            {
                return this.caseIdField;
            }
            set
            {
                this.caseIdField = value;
            }
        }

        /// <remarks/>
        public Date BarDate
        {
            get
            {
                return this.barDateField;
            }
            set
            {
                this.barDateField = value;
            }
        }

        /// <remarks/>
        public string TransferIn
        {
            get
            {
                return this.transferInField;
            }
            set
            {
                this.transferInField = value;
            }
        }

        /// <remarks/>
        public bool DeleteFlag
        {
            get
            {
                return this.deleteFlagField;
            }
            set
            {
                this.deleteFlagField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeleteFlagSpecified
        {
            get
            {
                return this.deleteFlagFieldSpecified;
            }
            set
            {
                this.deleteFlagFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Status", IsNullable = false)]
        public BankruptcyStatus[] StatusHistory
        {
            get
            {
                return this.statusHistoryField;
            }
            set
            {
                this.statusHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Comment", IsNullable = false)]
        public BankruptcyComment[] Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Debtor", IsNullable = false)]
        public BankruptcyReport2Debtor[] Debtors
        {
            get
            {
                return this.debtorsField;
            }
            set
            {
                this.debtorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Attorney", IsNullable = false)]
        public BankruptcyPerson2[] Attorneys
        {
            get
            {
                return this.attorneysField;
            }
            set
            {
                this.attorneysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Trustee", IsNullable = false)]
        public BankruptcyPerson2[] Trustees
        {
            get
            {
                return this.trusteesField;
            }
            set
            {
                this.trusteesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public enum TriStateBoolean
    {

        /// <remarks/>
        U,

        /// <remarks/>
        Y,

        /// <remarks/>
        N,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class WsException
    {

        private string sourceField;

        private int codeField;

        private bool codeFieldSpecified;

        private string locationField;

        private string messageField;

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public int Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodeSpecified
        {
            get
            {
                return this.codeFieldSpecified;
            }
            set
            {
                this.codeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class ResponseHeader
    {

        private int statusField;

        private bool statusFieldSpecified;

        private string messageField;

        private string queryIdField;

        private string transactionIdField;

        private WsException[] exceptionsField;

        /// <remarks/>
        public int Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified
        {
            get
            {
                return this.statusFieldSpecified;
            }
            set
            {
                this.statusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string QueryId
        {
            get
            {
                return this.queryIdField;
            }
            set
            {
                this.queryIdField = value;
            }
        }

        /// <remarks/>
        public string TransactionId
        {
            get
            {
                return this.transactionIdField;
            }
            set
            {
                this.transactionIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Item", IsNullable = false)]
        public WsException[] Exceptions
        {
            get
            {
                return this.exceptionsField;
            }
            set
            {
                this.exceptionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyReport2Response
    {

        private ResponseHeader headerField;

        private BankruptcyReport2Record[] bankruptcyReportRecordsField;

        /// <remarks/>
        public ResponseHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("BankruptcyReportRecord", IsNullable = false)]
        public BankruptcyReport2Record[] BankruptcyReportRecords
        {
            get
            {
                return this.bankruptcyReportRecordsField;
            }
            set
            {
                this.bankruptcyReportRecordsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyReport2By
    {

        private string uniqueIdField;

        private string businessIdField;

        private string tMSIdField;

        /// <remarks/>
        public string UniqueId
        {
            get
            {
                return this.uniqueIdField;
            }
            set
            {
                this.uniqueIdField = value;
            }
        }

        /// <remarks/>
        public string BusinessId
        {
            get
            {
                return this.businessIdField;
            }
            set
            {
                this.businessIdField = value;
            }
        }

        /// <remarks/>
        public string TMSId
        {
            get
            {
                return this.tMSIdField;
            }
            set
            {
                this.tMSIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurint")]
    public partial class BankruptcyReport2Option
    {

        private bool includeAllBankruptciesField;

        public BankruptcyReport2Option()
        {
            this.includeAllBankruptciesField = false;
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IncludeAllBankruptcies
        {
            get
            {
                return this.includeAllBankruptciesField;
            }
            set
            {
                this.includeAllBankruptciesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    public delegate void BankruptcyReport2CompletedEventHandler(object sender, BankruptcyReport2CompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class BankruptcyReport2CompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal BankruptcyReport2CompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public BankruptcyReport2Response Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((BankruptcyReport2Response)(this.results[0]));
            }
        }
    }

}
