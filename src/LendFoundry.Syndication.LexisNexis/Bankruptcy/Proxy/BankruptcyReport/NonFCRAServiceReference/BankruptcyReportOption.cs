﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference
{
    public partial class BankruptcyReport2Option
    {
        
        public BankruptcyReport2Option(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;

            IncludeAllBankruptcies = searchOption.IncludeAllBankruptcies;
        }
    }
}
