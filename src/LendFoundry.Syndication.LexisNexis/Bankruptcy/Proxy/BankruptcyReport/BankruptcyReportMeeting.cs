using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportMeeting : IBankruptcyReportMeeting
    {
        public BankruptcyReportMeeting()
        {
        }

        public BankruptcyReportMeeting(Bankruptcy3Meeting meeting)
        {
            if (meeting == null)
                return;

            Date = meeting.Date != null ? new BusinessReport.Date { Day = meeting.Date.Day, Month = meeting.Date.Month, Year = meeting.Date.Year } : null;
            Time = meeting.Time;
            Address = meeting.Address;
        }

        public BankruptcyReportMeeting(Bankruptcy2Meeting meeting)
        {
            if (meeting == null)
                return;

            Date = meeting.Date != null ? new BusinessReport.Date { Day = meeting.Date.Day, Month = meeting.Date.Month, Year = meeting.Date.Year } : null;
            Time = meeting.Time;
            Address = meeting.Address;
        }

        [JsonConverter(typeof(InterfaceConverter<IDate,BusinessReport.Date>))]
        public IDate Date { get; set; }

        public string Time { get; set; }

        public string Address { get; set; }
    }
}