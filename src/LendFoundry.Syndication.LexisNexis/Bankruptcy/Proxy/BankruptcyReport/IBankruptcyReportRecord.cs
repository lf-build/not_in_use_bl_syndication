﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public interface IBankruptcyReportRecord
    {
        string CaseNumber { get; set; }
        Boolean IsFcra { get; set; }

        string CourtLocation { get; set; }

        string CourtCode { get; set; }

        string CaseType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate FilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate OriginalFilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ReopenDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ClosedDate { get; set; }

        string CourtName { get; set; }

        string FilingType { get; set; }

        string FilerType { get; set; }

        string CorpFlag { get; set; }

        string FilingStatus { get; set; }

        string FilingJurisdiction { get; set; }

        string Chapter { get; set; }

        string OriginalChapter { get; set; }

        string JudgeName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBankruptcyReportMeeting, BankruptcyReportMeeting>))]
        IBankruptcyReportMeeting Meeting { get; set; }

        string JudgeIdentification { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ClaimsDeadline { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate ComplaintDeadline { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate DischargeDate { get; set; }

        string Disposition { get; set; }

        string SelfRepresented { get; set; }

        string AssetsForUnsecured { get; set; }

        string Assets { get; set; }

        string Liabilities { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportStatus, BankruptcyReportStatus>))]
        List<IBankruptcyReportStatus> StatusHistory { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportComment, BankruptcyReportComment>))]
        List<IBankruptcyReportComment> Comments { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportTrustee, BankruptcyReportTrustee>))]
        List<IBankruptcyReportTrustee> Trustee { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportAttorney, BankruptcyReportAttorney>))]
        List<IBankruptcyReportAttorney> Attorneys { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankruptcyReportDebtor, BankruptcyReportDebtor>))]
        List<IBankruptcyReportDebtor> Debtors { get; set; }
    }
}
