using LendFoundry.Foundation.Services;

using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportStatus : IBankruptcyReportStatus
    {

        public BankruptcyReportStatus() { }
        public BankruptcyReportStatus(ServiceReference.BankruptcyStatus status)
        {
            if (status == null)
                return;
            Type = status.Type;
            Date = status.Date != null ? new BusinessReport.Date { Year = status.Date.Year, Day = status.Date.Day, Month = status.Date.Month } : null;
        }

        public BankruptcyReportStatus(NonFCRAServiceReference.BankruptcyStatus status)
        {
            if (status == null)
                return;
            Type = status.Type;
            Date = status.Date != null ? new BusinessReport.Date { Year = status.Date.Year, Day = status.Date.Day, Month = status.Date.Month } : null;
        }

        public string Type { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, Date>))]
        public IDate Date { get; set; }
    }
}