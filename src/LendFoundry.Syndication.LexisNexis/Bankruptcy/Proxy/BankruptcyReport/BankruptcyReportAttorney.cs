using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportAttorney : IBankruptcyReportAttorney
    {
        public BankruptcyReportAttorney()
        {
        }

        public BankruptcyReportAttorney(BankruptcyReport3Attorney attorney)
        {
            if (attorney == null)
                return;
            UniqueId = attorney.UniqueId;
            BusinessId = attorney.BusinessId;
            Names = attorney.Phones != null ? new List<IName>(attorney.Names.Select(name => new Name(name))) : null;
            Addresses = attorney.Addresses != null ? new List<IAddress>(attorney.Addresses.Select(address => new Address(address))) : null;
            Phones = attorney.Phones != null ? new List<IPhone>(attorney.Phones.Select(phone => new Phone(phone))) : null;
            Emails = attorney.Emails != null ? new List<string>(attorney.Emails.Select(email => email)) : null;
        }

        public BankruptcyReportAttorney(BankruptcyPerson2 attorney)
        {
            if (attorney == null)
                return;
            UniqueId = attorney.UniqueId;
            BusinessId = attorney.BusinessId;
            Names = attorney.Phones != null ? new List<IName>(attorney.Names.Select(name => new Name(name))) : null;
            Addresses = attorney.Addresses != null ? new List<IAddress>(attorney.Addresses.Select(address => new Address(address))) : null;
            Phones = attorney.Phones != null ? new List<IPhone>(attorney.Phones.Select(ph => new Phone(ph))) : null;
            Emails = attorney.Emails != null ? new List<string>(attorney.Emails.Select(email => email)) : null;
        }

        public string BusinessId { get; set; }

        public string UniqueId { get; set; }

        public string TaxId { get; set; }

        public string AppendedTaxId { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }

        public List<string> Emails { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Address { get; set; }

        public BusinessIdentity BusinessIds { get; set; }

        public string AppendedSSN { get; set; }
    }
}