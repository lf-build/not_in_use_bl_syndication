using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportDebtor : IBankruptcyReportDebtor
    {
        public BankruptcyReportDebtor()
        {
        }

        public BankruptcyReportDebtor(BankruptcyReport3Debtor debtor)
        {
            if (debtor == null)
                return;
            UniqueId = debtor.UniqueId;
            BusinessId = debtor.BusinessId;
            Ssn = debtor.SSN;
            AppendedSSN = debtor.AppendedSSN;
            TaxId = debtor.TaxId;
            AppendedTaxId = debtor.AppendedTaxId;
            Names = debtor.Names != null ? new List<IName>(debtor.Names.Select(name => new Name(name))) : null;
            Addresses = debtor.Addresses != null ? new List<IAddress>(debtor.Addresses.Select(address => new Address(address))) : null;
            Phones = debtor.Phones != null ? new List<IPhone>(debtor.Phones.Select(phone => new Phone(phone))) : null;
            DateTransferred = debtor.DateTransferred != null ? new BusinessReport.Date { Year = debtor.DateTransferred.Year, Day = debtor.DateTransferred.Day, Month = debtor.DateTransferred.Month } : null;
            ConvertedDate = debtor.ConvertedDate != null ? new BusinessReport.Date { Year = debtor.ConvertedDate.Year, Day = debtor.ConvertedDate.Day, Month = debtor.ConvertedDate.Month } : null;
        }


        public BankruptcyReportDebtor(BankruptcyReport2Debtor debtor)
        {
            if (debtor == null)
                return;
            UniqueId = debtor.UniqueId;
            BusinessId = debtor.BusinessId;
            Ssn = debtor.SSN;
            AppendedSSN = debtor.AppendedSSN;
            TaxId = debtor.TaxId;
            AppendedTaxId = debtor.AppendedTaxId;
            Names = debtor.Names != null ? new List<IName>(debtor.Names.Select(name => new Name(name))) : null;
            Addresses = debtor.Addresses != null ? new List<IAddress>(debtor.Addresses.Select(address => new Address(address))) : null;
            Phones = debtor.Phones != null ? new List<IPhone>(debtor.Phones.Select(phone => new Phone(phone))) : null;
            DateTransferred = debtor.DateTransferred != null ? new BusinessReport.Date { Year = debtor.DateTransferred.Year, Day = debtor.DateTransferred.Day, Month = debtor.DateTransferred.Month } : null;
           
        }

        public string BusinessId { get; set; }

        public string UniqueId { get; set; }

        public string Ssn { get; set; }

        public string AppendedSSN { get; set; }

        public string TaxId { get; set; }

        public string AppendedTaxId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate DateTransferred { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate ConvertedDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }


    }
}