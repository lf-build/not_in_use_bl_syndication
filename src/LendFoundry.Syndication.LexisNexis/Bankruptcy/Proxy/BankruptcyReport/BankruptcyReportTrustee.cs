using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportTrustee : IBankruptcyReportTrustee
    {
        public BankruptcyReportTrustee()
        {
        }

        public BankruptcyReportTrustee(Bankruptcy3Trustee trustee)
        {
            if (trustee == null)
                return;
            UniqueId = trustee.UniqueId;
            Title = trustee.Title;
            Names = trustee.Name != null ? new List<IName> { new Name(trustee.Name) } : new List<IName>();
            Address = trustee.Address != null ? new List<IAddress> { new Address(trustee.Address) } : null;
            Phones = trustee.Phone10 != null ? new List<IPhone> { new Phone { Phone10 = trustee.Phone10 } } : null;
        }
        public BankruptcyReportTrustee(BankruptcyPerson2 trustee)
        {
            if (trustee == null)
                return;
            UniqueId = trustee.UniqueId;
            Names = trustee.Names != null ? new List<IName>(trustee.Names.Select(name => new Name(name))) : null;
            Address = trustee.Addresses != null ? new List<IAddress>(trustee.Addresses.Select(add => new Address(add))) : null;
            Phones = trustee.Phones != null ? new List<IPhone>(trustee.Phones.Select(phone => new Phone(phone))) : null;
            BusinessIds = trustee.BusinessIds;
            Emails = trustee.Emails != null ?trustee.Emails.ToList():new List<string>();
            AppendedSSN = trustee.AppendedSSN;
            TaxId = trustee.TaxId;
            AppendedTaxId = trustee.AppendedTaxId;
        }

        public string UniqueId { get; set; }

        public string Title { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Names { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Address { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhone, Phone>))]
        public List<IPhone> Phones { get; set; }

        public BusinessIdentity BusinessIds { get; set; }

        public List<string> Emails { get; set; }

       
        public string AppendedSSN { get; set; }

        public string TaxId { get; set; }

        public string AppendedTaxId { get; set; }
    }
}