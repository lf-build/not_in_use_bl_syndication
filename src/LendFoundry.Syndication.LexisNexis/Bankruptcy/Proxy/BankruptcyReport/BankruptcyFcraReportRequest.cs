﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class BankruptcyFcraReportRequest : IBankruptcyFcraReportRequest
    {
        public string UniqueId { get; set; }
        public string QueryId { get; set; }

    }
}
