﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IBankruptcyReportResponse
    {
        IResponseHeader Header { get; set; }
        int RecordCount { get; set; }
        List<IBankruptcyReportRecord> Records { get; set; }
    }
}
