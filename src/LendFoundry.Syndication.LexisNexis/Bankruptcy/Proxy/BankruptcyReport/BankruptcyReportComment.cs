﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public class BankruptcyReportComment : IBankruptcyReportComment
    {
        public BankruptcyReportComment() { }
        public BankruptcyReportComment(ServiceReference.BankruptcyComment comment)
        {
            if (comment == null)
                return;
            Description = comment.Description;
            FilingDate = comment.FilingDate != null ? new BusinessReport.Date { Year = comment.FilingDate.Year, Day = comment.FilingDate.Day, Month = comment.FilingDate.Month } : null;
        }

        public BankruptcyReportComment(NonFCRAServiceReference.BankruptcyComment comment)
        {
            if (comment == null)
                return;
            Description = comment.Description;
            FilingDate = comment.FilingDate != null ? new BusinessReport.Date { Year = comment.FilingDate.Year, Day = comment.FilingDate.Day, Month = comment.FilingDate.Month } : null;
        }
        public string Description { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        public IDate FilingDate{ get; set; }
    }
}
