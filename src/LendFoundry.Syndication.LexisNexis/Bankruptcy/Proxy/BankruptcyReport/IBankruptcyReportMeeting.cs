﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport
{
    public interface IBankruptcyReportMeeting
    {
        [JsonConverter(typeof(InterfaceConverter<IDate, BusinessReport.Date>))]
        IDate Date { get; set; }

        string Time { get; set; }

        string Address { get; set; }
    }
}
