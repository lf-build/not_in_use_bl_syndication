﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IBankruptcyFcraReportRequest
    {
        string UniqueId { get; set; }
        string QueryId { get; set; }

    }
}
