﻿
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class Name :IName
    {
        private BankruptcySearch2Name name;

        public Name() { }
        public Name(Proxy.BankruptcySearch.ServiceReference.BankruptcySearch3Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;
        }
        public Name(Proxy.BankruptcyReport.ServiceReference.Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            
        }
        public Name(Proxy.BankruptcyReport.ServiceReference.BankruptcySearch3Name name)
        {
            if (name == null)
                return;
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;

        }
        public Name(BankruptcyReport3Name name)
        {
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            UniqueId = name.UniqueId;

        }

        public Name(Proxy.BankruptcyReport.NonFCRAServiceReference.BankruptcySearch2Name name)
        {
            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
            CompanyName = name.CompanyName;
            Type = name.Type;
            //UniqueId = name.UniqueId;

        }
        

        public string Full { get; set; }
        public string First { get; set; }
        public string Middle { get; set; }
        public string Last { get; set; }
        public string Suffix { get; set; }
       
        public string Prefix { get; set; }

       
        public string CompanyName { get; set; }

       
        public string Type { get; set; }

       
        public string UniqueId { get; set; }
    }
}
