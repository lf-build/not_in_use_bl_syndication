﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IUser
    {
        string ReferenceCode { get; set; }
        string BillingCode { get; set; }
        string QueryId { get; set; }
        string GLBPurpose { get; set; }
        string DLPurpose { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEndUserInfo,EndUserInfo>))]
        IEndUserInfo EndUser { get; set; }
        int MaxWaitSeconds { get; set; }
        string AccountNumber { get; set; }
    }
}
