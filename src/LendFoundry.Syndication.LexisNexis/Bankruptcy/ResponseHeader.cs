﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class ResponseHeader : IResponseHeader
    {
        public ResponseHeader()
        {
        }

        public ResponseHeader(Proxy.BankruptcySearch.ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }

        public ResponseHeader(Proxy.BankruptcyReport.ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }

        public ResponseHeader(Proxy.BankruptcySearch.NonFCRAServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }

        public ResponseHeader(Proxy.BankruptcyReport.NonFCRAServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if (responseHeader.Exceptions != null)
            {
                Exceptions = new List<IException>(responseHeader.Exceptions.Select(exception => new Exception(exception)));
            }
        }

        public int Status { get; set; }
        public string Message { get; set; }
        public string QueryId { get; set; }
        public string TransactionId { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IException, Exception>))]
        public List<IException> Exceptions { get; set; }
    }
}