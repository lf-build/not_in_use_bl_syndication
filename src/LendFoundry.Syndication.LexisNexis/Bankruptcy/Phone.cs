﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class Phone : IPhone
    {
        public Phone() { }
        public Phone(Proxy.BankruptcyReport.ServiceReference.PhoneTimeZone phone)
        {
            if (phone == null)
                return;
            Phone10 = phone.Phone10;
            Fax = phone.Fax;
            TimeZone = phone.TimeZone;
        }
        public string Phone10 { get; set; }
        public string Fax { get; set; }
        public string TimeZone { get; set; }

    }
}
