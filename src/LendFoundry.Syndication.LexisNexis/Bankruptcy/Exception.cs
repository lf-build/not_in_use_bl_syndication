﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class Exception : IException
    {
        public Exception()
        { }
        public Exception(Proxy.BankruptcySearch.ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        public Exception(Proxy.BankruptcyReport.ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }

        public Exception(Proxy.BankruptcySearch.NonFCRAServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        public Exception(Proxy.BankruptcyReport.NonFCRAServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        public string Source { get; set; }
        public int Code { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
    }
}
