﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public class SearchOption : ISearchOption
    {
        public int FCRAPurpose { get; set; }
        public int ReturnCount { get; set; }
        public int StartingRecord { get; set; }
        public bool IncludeAllBankruptcies { get; set; }
    }
}
