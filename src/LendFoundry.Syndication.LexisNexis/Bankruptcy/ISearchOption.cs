﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{

    public interface ISearchOption
    {
        int FCRAPurpose { get; set; }
        int ReturnCount { get; set; }
        int StartingRecord { get; set; }

        bool IncludeAllBankruptcies { get; set; }

    }

}
