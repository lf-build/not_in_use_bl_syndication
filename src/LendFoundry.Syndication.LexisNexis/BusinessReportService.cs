﻿using System;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using LendFoundry.Syndication.LexisNexis.BusinessSearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Proxy;

namespace LendFoundry.Syndication.LexisNexis
{
    public class BusinessReportService : IBusinessReportService
    {
        public BusinessReportService(IBusinessReportServiceProxy businessReportServiceProxy, IBusinessReportConfiguration configuration)
        {
            if (businessReportServiceProxy == null)
                throw new ArgumentNullException(nameof(businessReportServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            BusinessReportServiceProxy = businessReportServiceProxy;
            Configuration = configuration;
        }

        public IBusinessReportServiceProxy BusinessReportServiceProxy { get; }
        private IBusinessReportConfiguration Configuration { get; }

        public ISearchResponse Search(ISearchRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var user = new BusinessSearch.ServiceReference.User(Configuration.EndUser);
            var options = new TopBusinessSearchOption(Configuration.SearchOption);
            var searchBy = new TopBusinessSearchBy(request);
            return new SearchResponse(BusinessReportServiceProxy.TopBusinessSearch(user, searchBy, options));
        }

        public IReportResponse GetReport(IReportRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var user = new BusinessReport.ServiceReference.User(Configuration.EndUser);
            var options = new TopBusinessReportOption(Configuration.ReportOption);
            var reportBy = new TopBusinessReportBy(request);
            return new ReportResponse(BusinessReportServiceProxy.TopBusinessReport(user, options, reportBy));
        }

        
    }
}
