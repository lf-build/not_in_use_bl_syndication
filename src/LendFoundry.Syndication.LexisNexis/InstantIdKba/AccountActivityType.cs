﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AccountActivityType : IAccountActivityType
    {
        public AccountActivityType(ServiceReference.accountactivitytype accountActivityType)
        {
            if (accountActivityType == null)
                return;
            Account = new AccountType(accountActivityType.account);
            List<IProductType> accounttransactions = new List<IProductType>();
            if (accountActivityType.accounttransaction != null)
            {
                foreach (ServiceReference.producttype accounttransaction in accountActivityType.accounttransaction)
                {
                    accounttransactions.Add(new ProductType(accounttransaction));
                }
                AccountTransaction = accounttransactions.ToArray();
            }
            
            TotalAmount = accountActivityType.totalamount;
            BeginningBalance = accountActivityType.beginningbalance;
            EndingBalance = accountActivityType.endingbalance;
        }
        public IAccountType Account { get; set; }

        public IProductType[] AccountTransaction { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal BeginningBalance { get; set; }

        public decimal EndingBalance { get; set; }
    }
}
