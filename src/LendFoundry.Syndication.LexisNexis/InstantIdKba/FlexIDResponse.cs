﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDResponse : IFlexIDResponse
    {
        public FlexIDResponse(ServiceReference.FlexIDResponse flexIdResponse)
        {
            if (flexIdResponse == null)
                return;
            Header = new FlexIDResponseHeader(flexIdResponse.Header);
            Result = new FlexIDResult(flexIdResponse.Result);
        }
        public IFlexIDResponseHeader Header { get; set; }

        public IFlexIDResult Result { get; set; }
    }
}
