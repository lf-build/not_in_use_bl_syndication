﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDValidElementSummary
    {
        bool SSNValid { get; set; }

        bool SSNDeceased { get; set; }

        bool DLValid { get; set; }

        bool PassportValid { get; set; }

        bool AddressPOBox { get; set; }

        bool AddressCMRA { get; set; }

        bool SSNFoundForLexID { get; set; }

    }
}
