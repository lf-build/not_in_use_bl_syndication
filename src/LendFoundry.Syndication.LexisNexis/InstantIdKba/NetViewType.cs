﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class NetViewType : INetViewType
    {
        public NetViewType(ServiceReference.netviewtype netViewType)
        {
            if (netViewType == null)
                return;
            NetViewForm = netViewType.netviewform;
        }
        public string NetViewForm { get; set; }
    }
}
