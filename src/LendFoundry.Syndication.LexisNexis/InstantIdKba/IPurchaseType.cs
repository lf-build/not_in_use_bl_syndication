﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IPurchaseType
    {
        IPurchaseTypeOrder Order { get; set; }

        PaymentMethod PaymentMethod { get; set; }

        IShippingType Shipping { get; set; }

        IAccountType Account { get; set; }

        IVenueType Venue { get; set; }

        System.DateTime TransactionDate { get; set; }

        IRiskAssessmentType RiskAssessment { get; set; }
    }
}
