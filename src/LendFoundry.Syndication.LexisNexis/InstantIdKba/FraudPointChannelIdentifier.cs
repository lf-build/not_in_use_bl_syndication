﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum FraudPointChannelIdentifier
    {
        Item,
        Mail,
        PointOfSale,
        Kiosk,
        Internet,
        Branch,
        Telephonic,
        Other,
    }
}
