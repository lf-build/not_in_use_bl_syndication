﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDVerifiedElementSummary : IFlexIDVerifiedElementSummary
    {
        public FlexIDVerifiedElementSummary(ServiceReference.FlexIDVerifiedElementSummary flexIdVerifiedElementSummary)
        {
            if (flexIdVerifiedElementSummary == null)
                return;
            FirstName = flexIdVerifiedElementSummary.FirstName;
            LastName = flexIdVerifiedElementSummary.LastName;
            StreetAddress = flexIdVerifiedElementSummary.StreetAddress;
            City = flexIdVerifiedElementSummary.City;
            State = flexIdVerifiedElementSummary.State;
            Zip = flexIdVerifiedElementSummary.Zip;
            HomePhone = flexIdVerifiedElementSummary.HomePhone;
            DOB = flexIdVerifiedElementSummary.DOB;
            DOBMatchLevel = flexIdVerifiedElementSummary.DOBMatchLevel;
            SSN = flexIdVerifiedElementSummary.SSN;
            Dl = flexIdVerifiedElementSummary.DL;
        }

        public bool FirstName { get; set; }

        public bool LastName { get; set; }

        public bool StreetAddress { get; set; }

        public bool City { get; set; }

        public bool State { get; set; }

        public bool Zip { get; set; }

        public bool HomePhone { get; set; }

        public bool DOB { get; set; }

        public string DOBMatchLevel { get; set; }

        public bool SSN { get; set; }

        public bool Dl { get; set; }

    }
}
