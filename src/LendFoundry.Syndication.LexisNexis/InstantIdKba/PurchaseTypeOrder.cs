﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class PurchaseTypeOrder : IPurchaseTypeOrder
    {
        public PurchaseTypeOrder(ServiceReference.purchasetypeOrder purchaseTypeOrder)
        {
            if (purchaseTypeOrder == null)
                return;
            List<IProductType> items = new List<IProductType>();
            if (purchaseTypeOrder.item != null)
            {
                foreach (ServiceReference.producttype item in purchaseTypeOrder.item)
                {
                    items.Add(new ProductType(item));
                }
                Item = items.ToArray();
            }

            TotalAmount = purchaseTypeOrder.totalamount;
            TotalTax = purchaseTypeOrder.totaltax;
            TotalShipping = purchaseTypeOrder.totalshipping;
            Merchant = purchaseTypeOrder.merchant;
        }
        public IProductType[] Item { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal TotalTax { get; set; }

        public decimal TotalShipping { get; set; }

        public string Merchant { get; set; }
    }
}
