﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDModelSequencedHRI : IFlexIDModelSequencedHRI
    {
        public FlexIDModelSequencedHRI(ServiceReference.FlexIDModelSequencedHRI flexModelSequencedHri)
        {
            if (flexModelSequencedHri == null)
                return;
            Name = flexModelSequencedHri.Name;
            List<IFlexIDScoreSequencedHRI> scores = new List<IFlexIDScoreSequencedHRI>();
            if (flexModelSequencedHri.Scores != null)
            {
                foreach (ServiceReference.FlexIDScoreSequencedHRI score in flexModelSequencedHri.Scores)
                {
                    scores.Add(new FlexIDScoreSequencedHRI(score));
                }
                Scores = scores.ToArray();
            }
        }
        public string Name { get; set; }

        public IFlexIDScoreSequencedHRI[] Scores { get; set; }
    }
}
