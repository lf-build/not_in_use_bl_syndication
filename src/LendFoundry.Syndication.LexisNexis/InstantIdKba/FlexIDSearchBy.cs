﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDSearchBy : IFlexIDSearchBy
    {
        public FlexIDSearchBy(ServiceReference.FlexIDSearchBy flexIdSearchBy)
        {
            if (flexIdSearchBy == null)
                return;
            Name = new FlexIDName(flexIdSearchBy.Name);
            Address = new FlexIDAddress(flexIdSearchBy.Address);
            DOB = new FlexIDDate(flexIdSearchBy.DOB);
            Age = flexIdSearchBy.Age;
            SSN = flexIdSearchBy.SSN;
            SSNLast4 = flexIdSearchBy.SSNLast4;
            DriverLicenseNumber = flexIdSearchBy.DriverLicenseNumber;
            DriverLicenseState = flexIdSearchBy.DriverLicenseState;
            IPAddress = flexIdSearchBy.IPAddress;
            HomePhone = flexIdSearchBy.HomePhone;
            WorkPhone = flexIdSearchBy.WorkPhone;
            Passport = new FlexIDPassport(flexIdSearchBy.Passport);
            Gender = flexIdSearchBy.Gender;
        }
        public IFlexIDName Name { get; set; }

        public IFlexIDAddress Address { get; set; }

        public IFlexIDDate DOB { get; set; }

        public uint Age { get; set; }

        public string SSN { get; set; }

        public string SSNLast4 { get; set; }

        public string DriverLicenseNumber { get; set; }

        public string DriverLicenseState { get; set; }

        public string IPAddress { get; set; }

        public string HomePhone { get; set; }

        public string WorkPhone { get; set; }

        public IFlexIDPassport Passport { get; set; }

        public string Gender { get; set; }
    }
}
