﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AccountMaintenanceType:IAccountMaintenanceType
    {
        public AccountMaintenanceType(ServiceReference.accountmaintenancetype accountMaintenanceType)
        {
            if (accountMaintenanceType == null)
                return;
            AccountCategory = (AccountCategoryType)(int)accountMaintenanceType.accountcategory;
            AccountMaintenanceCategory = (AccountMaintenanceCategoryType)(int) accountMaintenanceType.accountmaintenancecategory;
            MaintenanceDetail = accountMaintenanceType.maintenancedetail;
            Account = new AccountType(accountMaintenanceType.account);
            Data = new ComplexDetailType(accountMaintenanceType.data);
        }
        public AccountCategoryType AccountCategory { get; set; }

        public AccountMaintenanceCategoryType AccountMaintenanceCategory { get; set; }

        public string MaintenanceDetail { get; set; }

        public IAccountType Account { get; set; }

        public IComplexDetailType Data { get; set; }
    }
}
