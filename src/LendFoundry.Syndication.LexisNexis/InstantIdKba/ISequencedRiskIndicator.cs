﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISequencedRiskIndicator
    {
        string RiskCode { get; set; }

        string Description { get; set; }

        string Sequence { get; set; }
    }
}
