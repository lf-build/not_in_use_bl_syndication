﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISettingsType
    {
        string AccountName { get; set; }

        ModeType Mode { get; set; }
        bool ModeSpecified { get; set; }

        string RuleSet { get; set; }

        SimulatorModeType SimulatorMode { get; set; }
        bool SimulatorModeSpecified { get; set; }

        AttachmentType AttachmentType { get; set; }
        bool AttachmentTypeSpecified { get; set; }

        ISpecialFeatureType[] SpecialFeature { get; set; }

        IInternationalizationType[] Internationalization { get; set; }

        string[] ReferenceId { get; set; }
    }
}
