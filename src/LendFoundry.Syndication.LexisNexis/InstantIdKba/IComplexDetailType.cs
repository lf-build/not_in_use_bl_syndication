﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IComplexDetailType
    {
        string Heading{get;set;}

        ISimpleDetailType[] SimpleDetail{get;set;}

        IComplexDetailType[] ComplexDetail{get;set;}
    }
}
