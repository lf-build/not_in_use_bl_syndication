﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum InformationCodeType
    {
        QuestionDetails,
        PassedDetails,
        FailedDetails,
        ErrorDetails,
        SpecialFeatureDetails,
        InformationDetails,
        ThreatDetails,
        VoiceidDetails,
        DispositionDetails,
        PresentationDetails,
    }
}
