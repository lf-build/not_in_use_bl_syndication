﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointName : IFraudPointName
    {
        public FraudPointName(ServiceReference.FraudPointName fraudPointName)
        {
            if (fraudPointName == null)
                return;
            Full = fraudPointName.Full;
            First = fraudPointName.First;
            Middle = fraudPointName.Middle;
            Last = fraudPointName.Last;
            Suffix = fraudPointName.Suffix;
            Prefix = fraudPointName.Prefix;
        }
        public string Full { get; set; }

        public string First { get; set; }

        public string Middle { get; set; }

        public string Last { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
