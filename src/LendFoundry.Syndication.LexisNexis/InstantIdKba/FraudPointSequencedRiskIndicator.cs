﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointSequencedRiskIndicator : IFraudPointSequencedRiskIndicator
    {
        public FraudPointSequencedRiskIndicator(ServiceReference.FraudPointSequencedRiskIndicator fraudPointSequencedRiskindicator)
        {
            if (fraudPointSequencedRiskindicator == null)
                return;
            RiskCode = fraudPointSequencedRiskindicator.RiskCode;
            Description = fraudPointSequencedRiskindicator.Description;
            Sequence = fraudPointSequencedRiskindicator.Sequence;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }

        public int Sequence { get; set; }

    }
}
