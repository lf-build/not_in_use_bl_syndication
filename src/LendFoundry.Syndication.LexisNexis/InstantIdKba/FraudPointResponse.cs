﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointResponse : IFraudPointResponse
    {
        public FraudPointResponse(ServiceReference.FraudPointResponse fraudPointResponse)
        {
            if (fraudPointResponse == null)
                return;
            Header = new ResponseHeader(fraudPointResponse.Header);
            Result = new FraudPointResult(fraudPointResponse.Result);
        }
        public IResponseHeader Header { get; set; }

        public IFraudPointResult Result { get; set; }
    }
}
