﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointResponseEx : IFraudPointResponseEx
    {
        public FraudPointResponseEx(ServiceReference.FraudPointResponseEx fraudPointResponseEx)
        {
            if (fraudPointResponseEx == null)
                return;
            Response = new FraudPointResponse(fraudPointResponseEx.response);
        }
        public IFraudPointResponse Response { get; set; }
    }
}
