﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AddressType : IAddressType
    {
        public AddressType(ServiceReference.addresstype addressType)
        {
            if (addressType == null)
                return;
            AddressStreet1 = addressType.addressstreet1;
            AddressStreet2 = addressType.addressstreet2;
            Suite = addressType.suite;
            AddressCity = addressType.addresscity;
            AddressState = addressType.addressstate;
            AddressZip = addressType.addresszip;
            AddressZipPlus4 = addressType.addresszipplus4;
            AddressCountry = addressType.addresscountry;
            AddressContext = (AddressContextType)(int)addressType.addresscontext;
            AddressYearsAt = addressType.addressyearsat;
            OccupationStartDate = addressType.occupationstartdate;
            AddressYearsAtSpecified = addressType.addressyearsatSpecified;
            OccupationStartDateSpecified = addressType.occupationstartdateSpecified;
        }
        public string AddressStreet1 { get; set; }

        public string AddressStreet2 { get; set; }

        public string Suite { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        public string AddressZip { get; set; }

        public string AddressZipPlus4 { get; set; }

        public string AddressCountry { get; set; }

        public AddressContextType AddressContext { get; set; }

        public decimal AddressYearsAt { get; set; }

        public bool AddressYearsAtSpecified { get; set; }

        public System.DateTime OccupationStartDate { get; set; }

        public bool OccupationStartDateSpecified { get; set; }

    }
}
