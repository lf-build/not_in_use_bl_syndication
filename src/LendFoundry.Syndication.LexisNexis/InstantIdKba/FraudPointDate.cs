﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointDate : IFraudPointDate
    {
        public FraudPointDate(ServiceReference.FraudPointDate fraudPointDate)
        {
            if (fraudPointDate == null)
                return;
            Year = fraudPointDate.Year;
            Month = fraudPointDate.Month;
            Day = fraudPointDate.Day;
        }
        public short Year { get; set; }

        public short Month { get; set; }

        public short Day { get; set; }

    }
}
