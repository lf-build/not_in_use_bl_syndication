﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAnswerType
    {
        long QuestionId { get; set; }

        long[] Choices { get; set; }
    }
}
