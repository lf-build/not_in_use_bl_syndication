﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class NewAreaCode : INewAreaCode
    {
        public NewAreaCode(ServiceReference.NewAreaCode newAreaCode)
        {
            if (newAreaCode == null)
                return;
            AreaCode = newAreaCode.AreaCode;
            EffectiveDate = new Date(newAreaCode.EffectiveDate);
        }
        public string AreaCode { get; set; }

        public IDate EffectiveDate { get; set; }
    }
}
