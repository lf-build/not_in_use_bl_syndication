﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class BusinessType : IBusinessType
    {
        public BusinessType(ServiceReference.businesstype businessType)
        {
            if (businessType == null)
                return;
            CompanyName = businessType.companyname;
            Fein = businessType.fein;
            Address = new AddressType(businessType.address);

            List<IPhoneNumberType> phonenumbers = new List<IPhoneNumberType>();
            if (businessType.phonenumber != null)
            {
                foreach (ServiceReference.phonenumbertype phonenumber in businessType.phonenumber)
                {
                    phonenumbers.Add(new PhoneNumberType(phonenumber));
                }
                Phonenumber = phonenumbers.ToArray();
            }
            Title = businessType.title;
            StartDate = businessType.startdate;
            EndDate = businessType.enddate;
            StartDateSpecified = businessType.startdateSpecified;
            EndDateSpecified = businessType.enddateSpecified;
        }
        public string CompanyName { get; set; }

        public string Fein { get; set; }

        public IAddressType Address { get; set; }

        public IPhoneNumberType[] Phonenumber { get; set; }

        public string Title { get; set; }

        public System.DateTime StartDate { get; set; }

        public bool StartDateSpecified { get; set; }

        public System.DateTime EndDate { get; set; }

        public bool EndDateSpecified { get; set; }
    }
}
