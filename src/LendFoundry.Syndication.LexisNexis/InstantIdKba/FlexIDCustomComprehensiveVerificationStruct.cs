﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDCustomComprehensiveVerificationStruct : IFlexIDCustomComprehensiveVerificationStruct
    {
        public FlexIDCustomComprehensiveVerificationStruct(ServiceReference.FlexIDCustomComprehensiveVerificationStruct flexIdCustomVerificationStruct)
        {
            if (flexIdCustomVerificationStruct == null)
                return;
            ComprehensiveVerificationIndex = flexIdCustomVerificationStruct.ComprehensiveVerificationIndex;
            List<IFlexIDSequencedRiskIndicator> riskindicators = new List<IFlexIDSequencedRiskIndicator>();
            if (flexIdCustomVerificationStruct.RiskIndicators != null)
            {
                foreach (ServiceReference.FlexIDSequencedRiskIndicator riskindicator in flexIdCustomVerificationStruct.RiskIndicators)
                {
                    riskindicators.Add(new FlexIDSequencedRiskIndicator(riskindicator));
                }
                RiskIndicators = riskindicators.ToArray();
            }
            List<IFlexIDRiskIndicator> potentialfollowupactions = new List<IFlexIDRiskIndicator>();
            if (flexIdCustomVerificationStruct.PotentialFollowupActions != null)
            {
                foreach (ServiceReference.FlexIDRiskIndicator potentialfollowupaction in flexIdCustomVerificationStruct.PotentialFollowupActions)
                {
                    potentialfollowupactions.Add(new FlexIDRiskIndicator(potentialfollowupaction));
                }
                PotentialFollowupActions = potentialfollowupactions.ToArray();
            }
            Name = flexIdCustomVerificationStruct.Name;
        }
        public int ComprehensiveVerificationIndex { get; set; }

        public IFlexIDSequencedRiskIndicator[] RiskIndicators { get; set; }

        public IFlexIDRiskIndicator[] PotentialFollowupActions { get; set; }

        public string Name { get; set; }
    }
}
