﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAccountVerificationType
    {
        object Item { get; set; }

        IVenueType Venue { get; set; }

        System.DateTime ActivityDate { get; set; }

        IRiskAssessmentType RiskAssessment { get; set; }
    }
}
