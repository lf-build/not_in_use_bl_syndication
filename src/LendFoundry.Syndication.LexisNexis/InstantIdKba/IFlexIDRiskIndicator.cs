﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDRiskIndicator
    {
        string RiskCode { get; set; }

        string Description { get; set; }
    }
}
