﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionType
    {
        object Item { get; set; }
    }
}
