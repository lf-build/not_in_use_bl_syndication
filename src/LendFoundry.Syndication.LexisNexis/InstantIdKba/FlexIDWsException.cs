﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDWsException :IFlexIDWsException
    {
        public FlexIDWsException(ServiceReference.FlexIDWsException flexIdWsexception)
        {
            if (flexIdWsexception == null)
                return;
            Source = flexIdWsexception.Source;
            Code = flexIdWsexception.Code;
            Location = flexIdWsexception.Location;
            Message = flexIdWsexception.Message;
        }

        public string Source { get; set; }

        public int Code { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }
    }
}
