﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SSNInfo : ISSNInfo
    {
        public SSNInfo(ServiceReference.SSNInfo ssnInfo)
        {
            if (ssnInfo == null)
                return;
            SSN = ssnInfo.SSN;
            Valid = ssnInfo.Valid;
            IssuedLocation = ssnInfo.IssuedLocation;
            IssuedStartDate = new Date(ssnInfo.IssuedStartDate);
            IssuedEndDate = new Date(ssnInfo.IssuedEndDate);
        }
        public string SSN { get; set; }

        public string Valid { get; set; }

        public string IssuedLocation { get; set; }

        public IDate IssuedStartDate { get; set; }

        public IDate IssuedEndDate { get; set; }
    }
}
