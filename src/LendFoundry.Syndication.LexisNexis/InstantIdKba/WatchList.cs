﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class WatchList : IWatchList
    {
        public WatchList(ServiceReference.WatchList watchList)
        {
            if (watchList == null)
                return;
            Table = watchList.Table;
            RecordNumber = watchList.RecordNumber;
            Name = new Name(watchList.Name);
            Address = new Address(watchList.Address);
            Country = watchList.Country;
            EntityName = watchList.EntityName;
            Sequence = watchList.Sequence;
        }
        public string Table { get; set; }

        public string RecordNumber { get; set; }

        public IName Name { get; set; }

        public IAddress Address { get; set; }

        public string Country { get; set; }

        public string EntityName { get; set; }

        public string Sequence { get; set; }
    }
}
