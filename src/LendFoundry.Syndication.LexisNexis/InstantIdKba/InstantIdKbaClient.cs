﻿using System;
using System.Net;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIdKbaClient : IInstantIdKbaClient
    {
        public InstantIdKbaClient(ILexisNexisConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.InstantIdKba == null)
                throw new ArgumentNullException(nameof(configuration.InstantIdKba));

            if (configuration.InstantIdKba.UserName == null)
                throw new ArgumentNullException(nameof(configuration.InstantIdKba.UserName));

            if (configuration.InstantIdKba.Password == null)
                throw new ArgumentNullException(nameof(configuration.InstantIdKba.Password));

            if (configuration.InstantIdKba.IdentityVerificationSettings == null)
                throw new ArgumentNullException(nameof(configuration.InstantIdKba.IdentityVerificationSettings));

            if (configuration.InstantIdKba.ContinueSettings == null)
                throw new ArgumentNullException(nameof(configuration.InstantIdKba.ContinueSettings));

            Configuration = configuration.InstantIdKba;
        }

        private KbaServiceConfiguration Configuration { get; }

        public ServiceReference.transactionresponse VerifyIdentity(ServiceReference.persontype personType)
        {
            try
            {
                ServiceReference.CarbonWebService soapClient = new ServiceReference.CarbonWebService();
                soapClient.Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password);
                var transactionIdentityVerification = new ServiceReference.TransactionIdentityVerification();
                transactionIdentityVerification.person = personType;
                transactionIdentityVerification.settings = new ServiceReference.identityverificationsettingstype(Configuration.IdentityVerificationSettings);
                transactionIdentityVerification.identityassertion = null;
                transactionIdentityVerification.transaction = new ServiceReference.transactiontype()
                {
                    Item = new ServiceReference.accountverificationtype()
                    {
                        venue = new ServiceReference.venuetype()
                        {
                            ItemElementName = ServiceReference.ItemChoiceType.online,
                            Item = new ServiceReference.onlinetype()
                            {
                                credential = new ServiceReference.credentialtype()
                                {
                                    credentialmethod = new ServiceReference.credentialmethodtype[]
                                    {
                                        ServiceReference.credentialmethodtype.basic
                                    }
                                }
                            }
                        }
                    }
                };
                return soapClient.identityVerification(transactionIdentityVerification);
            }
            catch (Exception ex)
            {
                throw new LexisNexisException($"The method VerifyIdentity({personType}) raised an error:{ex.Message}", ex);
            }
        }

        public ServiceReference.transactionresponse VerifyContinuation(ServiceReference.TransactionContinueRequest transactionContinueRequest)
        {
            try
            {
                ServiceReference.CarbonWebService soapClient = new ServiceReference.CarbonWebService();
                soapClient.Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password);
                var transactionContinue = new ServiceReference.TransactionContinue();
                transactionContinue.Item = new ServiceReference.answerstype();
                transactionContinue.Item.questionsetid = transactionContinueRequest.QuestionSetId;
                transactionContinue.Item.answer = transactionContinueRequest.Answer;
                transactionContinue.settings = new ServiceReference.continuesettingstype(Configuration.ContinueSettings);
                transactionContinue.settings.transactionid = transactionContinueRequest.TransactionId;
                return soapClient.continuation(transactionContinue);
            }
            catch (Exception ex)
            {
                throw new LexisNexisException($"The method VerifyContinuation({transactionContinueRequest}) raised an error:{ex.Message}", ex);
            }
        }
    }
}