﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IHttpHeaderTypeHeaderFields
    {
        string HeaderField { get; set; }

        string HeaderValue { get; set; }
    }
}
