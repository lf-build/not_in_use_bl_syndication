﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionContinue
    {
        IContinueSettingsType Settings { get; set; }

        IAnswersType Item { get; set; }
    }
}
