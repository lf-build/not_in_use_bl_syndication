﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionResponse
    {
        ITransactionStatusType TransactionStatus { get; set; }

        IQuestionsType Questions { get; set; }

        IInformationType[] Information { get; set; }

        IExternalResponsesType ExternalResponses { get; set; }
    }
}
