﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TextType : ITextType
    {
        public TextType(ServiceReference.texttype textType)
        {
            if (textType == null)
                return;
            Language = (LanguageType)(int)textType.language;
            Statement = textType.statement;
        }
        public LanguageType Language { get; set; }

        public string Statement { get; set; }
    }
}
