﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum AttachmentType
    {
        XML,
        GRAMMAR,
        WAV,
    }
}
