﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDResponseEx
    {
        IFlexIDResponse Response { get; set; }
    }
}
