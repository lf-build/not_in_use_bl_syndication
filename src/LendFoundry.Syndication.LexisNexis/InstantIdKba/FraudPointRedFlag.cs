﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointRedFlag : IFraudPointRedFlag
    {
        public FraudPointRedFlag(ServiceReference.FraudPointRedFlag fraudPointRedFlag)
        {
            if (fraudPointRedFlag == null)
                return;
            Name = fraudPointRedFlag.Name;
            List<IFraudPointSequencedRiskIndicator> highriskindicators = new List<IFraudPointSequencedRiskIndicator>();
            if (fraudPointRedFlag.HighRiskIndicators != null)
            {
                foreach (ServiceReference.FraudPointSequencedRiskIndicator highriskindicator in fraudPointRedFlag.HighRiskIndicators)
                {
                    highriskindicators.Add(new FraudPointSequencedRiskIndicator(highriskindicator));
                }
                HighRiskIndicators = highriskindicators.ToArray();
            }
        }
        public string Name { get; set; }

        public IFraudPointSequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
