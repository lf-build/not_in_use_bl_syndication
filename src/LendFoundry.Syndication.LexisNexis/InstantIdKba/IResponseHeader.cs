﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IResponseHeader
    {
        string Status { get; set; }

        string Message { get; set; }

        string QueryId { get; set; }

        string TransactionId { get; set; }

        IWsException[] Exceptions { get; set; }
    }
}
