﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class DecedentInfo : IDecedentInfo
    {
        public DecedentInfo(ServiceReference.DecedentInfo decedentInfo)
        {
            if (decedentInfo == null)
                return;
            Name = new Name(decedentInfo.Name);
            DOD = new Date(decedentInfo.DOD);
            DOB = new Date(decedentInfo.DOB);
        }
        public IName Name { get; set; }

        public IDate DOD { get; set; }

        public IDate DOB { get; set; }
    }
}
