﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDSequencedRiskIndicator : IFlexIDSequencedRiskIndicator
    {
        public FlexIDSequencedRiskIndicator(ServiceReference.FlexIDSequencedRiskIndicator flexIdSequencedRiskindicator)
        {
            if (flexIdSequencedRiskindicator == null)
                return;
            RiskCode = flexIdSequencedRiskindicator.RiskCode;
            Description = flexIdSequencedRiskindicator.Description;
            Sequence = flexIdSequencedRiskindicator.Sequence;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }

        public int Sequence { get; set; }
    }
}
