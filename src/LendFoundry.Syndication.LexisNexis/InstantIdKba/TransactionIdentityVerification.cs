﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionIdentityVerification : ITransactionIdentityVerification
    {
        public TransactionIdentityVerification(ServiceReference.TransactionIdentityVerification transactionIdentityVerification)
        {
            if (transactionIdentityVerification == null)
                return;

            Settings = new IdentityVerificationSettingsType(transactionIdentityVerification.settings);
            Person = new PersonType(transactionIdentityVerification.person);
            Transaction = new TransactionType(transactionIdentityVerification.transaction);
            List<IIdentityAssertionType> identityassertions = new List<IIdentityAssertionType>();
            if (transactionIdentityVerification.identityassertion != null)
            {
                foreach (ServiceReference.identityassertiontype identityassertion in transactionIdentityVerification.identityassertion)
                {
                    identityassertions.Add(new IdentityAssertionType(identityassertion));
                }
                Identityassertion = identityassertions.ToArray();
            }
        }
        public IdentityVerificationSettingsType Settings { get; set; }

        public IPersonType Person { get; set; }

        public ITransactionType Transaction { get; set; }

        public IIdentityAssertionType[] Identityassertion { get; set; }
    }
}
