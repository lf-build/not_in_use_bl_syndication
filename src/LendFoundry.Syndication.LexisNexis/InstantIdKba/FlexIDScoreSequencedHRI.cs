﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDScoreSequencedHRI : IFlexIDScoreSequencedHRI
    {
        public FlexIDScoreSequencedHRI(ServiceReference.FlexIDScoreSequencedHRI flexIdScoreSequencedHri)
        {
            if (flexIdScoreSequencedHri == null)
                return;
            Type = flexIdScoreSequencedHri.Type;
            Value = flexIdScoreSequencedHri.Value;
            List<IFlexIDNameValuePair> riskindices = new List<IFlexIDNameValuePair>();
            if (flexIdScoreSequencedHri.RiskIndices != null)
            {
                foreach (ServiceReference.FlexIDNameValuePair riskindice in flexIdScoreSequencedHri.RiskIndices)
                {
                    riskindices.Add(new FlexIDNameValuePair(riskindice));
                }
                RiskIndices = riskindices.ToArray();
            }
            List<IFlexIDSequencedRiskIndicator> highRiskIndicators = new List<IFlexIDSequencedRiskIndicator>();
            if (flexIdScoreSequencedHri.HighRiskIndicators != null)
            {
                foreach (ServiceReference.FlexIDSequencedRiskIndicator highRiskIndicator in flexIdScoreSequencedHri.HighRiskIndicators)
                {
                    highRiskIndicators.Add(new FlexIDSequencedRiskIndicator(highRiskIndicator));
                }
                HighRiskIndicators = highRiskIndicators.ToArray();
            }
        }
        public string Type { get; set; }

        public int Value { get; set; }

        public IFlexIDNameValuePair[] RiskIndices { get; set; }

        public IFlexIDSequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
