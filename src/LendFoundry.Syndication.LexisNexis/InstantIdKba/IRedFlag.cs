﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IRedFlag
    {
        string Name { get; set; }

        ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
