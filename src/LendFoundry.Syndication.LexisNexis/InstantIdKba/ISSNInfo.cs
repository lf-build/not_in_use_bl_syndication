﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISSNInfo
    {
        string SSN { get; set; }

        string Valid { get; set; }

        string IssuedLocation { get; set; }

        IDate IssuedStartDate { get; set; }

        IDate IssuedEndDate { get; set; }
    }
}
