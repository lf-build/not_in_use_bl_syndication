﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionAuthenticationCheck : ITransactionAuthenticationCheck
    {
        public TransactionAuthenticationCheck(ServiceReference.transactionauthenticationcheck transactionAuthenticationCheck)
        {
            if (transactionAuthenticationCheck == null)
                return;
            Settings = new TransactionAuthenticationCheckSettings(transactionAuthenticationCheck.settings);
            Credential = new CredentialType(transactionAuthenticationCheck.credential);
            Account = new AccountType(transactionAuthenticationCheck.account);
        }
        public ITransactionAuthenticationCheckSettings Settings { get; set; }

        public ICredentialType Credential { get; set; }

        public IAccountType Account { get; set; }
    }
}
