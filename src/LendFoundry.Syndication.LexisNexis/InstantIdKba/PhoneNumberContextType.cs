﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum PhoneNumberContextType
    {
        Home,
        Business,
        Mobile,
        Fax,
        Pager,
        Other
    }
}
