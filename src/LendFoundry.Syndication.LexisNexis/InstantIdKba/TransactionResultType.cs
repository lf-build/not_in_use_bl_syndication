﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum TransactionResultType
    {
        Passed,
        Failed,
        Error,
        Questions,
        Accepted,
    }
}
