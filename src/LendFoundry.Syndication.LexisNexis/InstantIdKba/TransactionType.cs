﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionType : ITransactionType
    {
        public TransactionType(ServiceReference.transactiontype transactionType)
        {
            if (transactionType == null)
                return;
            Item = transactionType.Item;
        }
        public object Item { get; set; }
    }
}
