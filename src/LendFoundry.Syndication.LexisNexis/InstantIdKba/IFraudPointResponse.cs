﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointResponse
    {
        IResponseHeader Header { get; set; }

        IFraudPointResult Result { get; set; }
    }
}
