﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IVenueTypePointofSale
    {
        CredentialMethodType[] CredentialMethod { get; set; }

        string MerchantZipCode { get; set; }
    }
}
