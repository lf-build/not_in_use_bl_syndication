﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISpecialFeatureType
    {
        string SpecialFeatureCode { get; set; }

        string SpecialFeatureValue { get; set; }
    }
}
