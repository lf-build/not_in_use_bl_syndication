﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ComplexDetailType : IComplexDetailType
    {
        public ComplexDetailType(ServiceReference.complexdetailtype complexDetailType)
        {
            if (complexDetailType == null)
                return;
            Heading = complexDetailType.heading;
            List<ISimpleDetailType> simpleDetails = new List<ISimpleDetailType>();
            if (complexDetailType.simpledetail != null)
            {
                foreach (ServiceReference.simpledetailtype simpleDetail in complexDetailType.simpledetail)
                {
                    simpleDetails.Add(new SimpleDetailType(simpleDetail));
                }
                SimpleDetail = simpleDetails.ToArray();
            }
            List<IComplexDetailType> complexDetails = new List<IComplexDetailType>();
            if (complexDetailType.complexdetail != null)
            {
                foreach (ServiceReference.complexdetailtype complexDetail in complexDetailType.complexdetail)
                {
                    complexDetails.Add(new ComplexDetailType(complexDetail));
                }
                ComplexDetail = complexDetails.ToArray();
            }
        }
        public string Heading { get; set; }

        public ISimpleDetailType[] SimpleDetail { get; set; }

        public IComplexDetailType[] ComplexDetail { get; set; }
    }
}
