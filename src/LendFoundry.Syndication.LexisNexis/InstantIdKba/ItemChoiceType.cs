﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum ItemChoiceType
    {
        Batch,
        CallCenter,
        CustomerService,
        Ivr,
        NetView,
        Online,
        PointOfSale,
    }
}
