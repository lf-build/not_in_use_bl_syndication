﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointRedFlagsReport
    {
        int Version { get; set; }

        IFraudPointRedFlag[] RedFlags { get; set; }
    }
}
