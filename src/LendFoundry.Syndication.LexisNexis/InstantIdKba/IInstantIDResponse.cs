﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIDResponse
    {
        IResponseHeader Header { get; set; }

        IInstantIDResult Result { get; set; }
    }
}
