﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class IdentityAssertionType : IIdentityAssertionType
    {
        public IdentityAssertionType(ServiceReference.identityassertiontype identityAssertionType)
        {
            if (identityAssertionType == null)
                return;
            Data = new ComplexDetailType(identityAssertionType.data);
            Transaction = new TransactionType(identityAssertionType.transaction);
        }
        public IComplexDetailType Data { get; set; }

        public ITransactionType Transaction { get; set; }
    }
}
