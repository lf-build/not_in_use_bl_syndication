﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class RedFlag : IRedFlag
    {
        public RedFlag(ServiceReference.RedFlag redFlag)
        {
            if (redFlag == null)
                return;
            Name = redFlag.Name;
            List<ISequencedRiskIndicator> highriskindicators = new List<ISequencedRiskIndicator>();
            if (redFlag.HighRiskIndicators != null)
            {
                foreach (ServiceReference.SequencedRiskIndicator highriskindicator in redFlag.HighRiskIndicators)
                {
                    highriskindicators.Add(new SequencedRiskIndicator(highriskindicator));
                }
                HighRiskIndicators = highriskindicators.ToArray();
            }
        }
        public string Name { get; set; }

        public ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
