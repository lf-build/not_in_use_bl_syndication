﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SecurityQuestionType : ISecurityQuestionType
    {
        public SecurityQuestionType(ServiceReference.securityquestiontype securityQuestionType)
        {
            if (securityQuestionType == null)
                return;
            SecurityQuestionQuestionCategory = (SecurityQuestionCategoryType)(int)securityQuestionType.securityquestionquestioncategory;
            SecurityQuestionQuesitonStatement = securityQuestionType.securityquestionquesitonstatement;
            SecurityQuestionAnswer = securityQuestionType.securityquestionanswer;
        }
        public SecurityQuestionCategoryType SecurityQuestionQuestionCategory { get; set; }

        public string SecurityQuestionQuesitonStatement { get; set; }

        public string SecurityQuestionAnswer { get; set; }
    }
}
