﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class Passport : IPassport
    {
        public Passport(ServiceReference.Passport passport)
        {
            if (passport == null)
                return;
            Number = passport.Number;
            ExpirationDate = new Date(passport.ExpirationDate);
            Country = passport.Country;
            MachineReadableLine1 = passport.MachineReadableLine1;
            MachineReadableLine2 = passport.MachineReadableLine2;
        }
        public string Number { get; set; }

        public IDate ExpirationDate { get; set; }

        public string Country { get; set; }

        public string MachineReadableLine1 { get; set; }

        public string MachineReadableLine2 { get; set; }
    }
}
