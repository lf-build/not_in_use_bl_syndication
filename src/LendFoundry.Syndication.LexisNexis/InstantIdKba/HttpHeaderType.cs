﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class HttpHeaderType : IHttpHeaderType
    {
        public HttpHeaderType(ServiceReference.httpheadertype httpHeaderType)
        {
            if (httpHeaderType == null)
                return;
            List<object> items = new List<object>();
            if (httpHeaderType.Items != null)
            {
                foreach (object item in httpHeaderType.Items)
                {
                    items.Add(item);
                }
                Items = items.ToArray();
            }
        }
        public object[] Items { get; set; }
    }
}
