﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDRiskIndicator : IFlexIDRiskIndicator
    {
        public FlexIDRiskIndicator(ServiceReference.FlexIDRiskIndicator flexIdRiskindicator)
        {
            if (flexIdRiskindicator == null)
                return;
            RiskCode = flexIdRiskindicator.RiskCode;
            Description = flexIdRiskindicator.Description;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }
    }
}
