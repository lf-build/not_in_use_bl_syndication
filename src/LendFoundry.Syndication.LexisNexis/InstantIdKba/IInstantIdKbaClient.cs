﻿using LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIdKbaClient
    {
        transactionresponse VerifyIdentity(ServiceReference.persontype transactionIdentityVerification);
        transactionresponse VerifyContinuation(ServiceReference.TransactionContinueRequest transactionContinue);
    }
}