﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class Assertion : IAssertion
    {
        public Assertion(ServiceReference.assertion assertion)
        {
            if (assertion == null)
                return;
            Settings = new SettingsType(assertion.settings);
            Person = new PersonType(assertion.person);
            Account = new AccountType(assertion.account);
            List<IIdentityAssertionType> identityassertions = new List<IIdentityAssertionType>();
            if (assertion.identityassertion != null)
            {
                foreach (ServiceReference.identityassertiontype identityassertion in assertion.identityassertion)
                {
                    identityassertions.Add(new IdentityAssertionType(identityassertion));
                }
                IdentityAssertion = identityassertions.ToArray();
            }
        }
        public ISettingsType Settings { get; set; }

       public IPersonType Person { get; set; }

        public IAccountType Account { get; set; }

        public IIdentityAssertionType[] IdentityAssertion { get; set; }
    }
}
