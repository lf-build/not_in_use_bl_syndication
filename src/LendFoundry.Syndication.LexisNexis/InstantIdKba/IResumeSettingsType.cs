﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IResumeSettingsType : ISettingsType
    {
        long TransactionId { get; set; }
    }
}
