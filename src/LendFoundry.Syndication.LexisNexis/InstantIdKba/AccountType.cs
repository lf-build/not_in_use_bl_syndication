﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AccountType:IAccountType
    {
        public AccountType(ServiceReference.accounttype accountType)
        {
            if (accountType == null)
                return;
            AccountNumber = accountType.accountnumber;
            AccountTransactionId = accountType.accounttransactionid;
            AccountName = accountType.accountname;
            List<string> customerIds = new List<string>();
            if (accountType.customerid != null)
            {
                foreach (string customerId in accountType.customerid)
                {
                    customerIds.Add(customerId);
                }
                CustomerId = customerIds.ToArray();
            }
        }
        public string AccountNumber { get; set; }

        public string AccountTransactionId { get; set; }

        public string AccountName { get; set; }

        public string[] CustomerId { get; set; }
    }
}
