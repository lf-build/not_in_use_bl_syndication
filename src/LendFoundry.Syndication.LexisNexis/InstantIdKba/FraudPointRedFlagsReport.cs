﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointRedFlagsReport : IFraudPointRedFlagsReport
    {
        public FraudPointRedFlagsReport(ServiceReference.FraudPointRedFlagsReport fraudPointRedFlagsReport)
        {
            if (fraudPointRedFlagsReport == null)
                return;
            Version = fraudPointRedFlagsReport.Version;
            List<IFraudPointRedFlag> redflags = new List<IFraudPointRedFlag>();
            if (fraudPointRedFlagsReport.RedFlags != null)
            {
                foreach (ServiceReference.FraudPointRedFlag redflag in fraudPointRedFlagsReport.RedFlags)
                {
                    redflags.Add(new FraudPointRedFlag(redflag));
                }
                RedFlags = redflags.ToArray();
            }
        }
        public int Version { get; set; }

        public IFraudPointRedFlag[] RedFlags { get; set; }
    }
}
