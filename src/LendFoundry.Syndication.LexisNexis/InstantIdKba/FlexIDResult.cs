﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDResult : IFlexIDResult
    {
        public FlexIDResult(ServiceReference.FlexIDResult flexIdResult)
        {
            if (flexIdResult == null)
                return;
            InputEcho = new FlexIDSearchBy(flexIdResult.InputEcho);
            UniqueId = flexIdResult.UniqueId;
            VerifiedSSN = flexIdResult.VerifiedSSN;
            NameAddressPhone = new FlexIDNameAddressPhone(flexIdResult.NameAddressPhone);
            VerifiedElementSummary = new FlexIDVerifiedElementSummary(flexIdResult.VerifiedElementSummary);
            ValidElementSummary = new FlexIDValidElementSummary(flexIdResult.ValidElementSummary);
            NameAddressSSNSummary = flexIdResult.NameAddressSSNSummary;
            ComprehensiveVerification = new FlexIDComprehensiveVerificationStruct( flexIdResult.ComprehensiveVerification);
            CustomComprehensiveVerification =  new FlexIDCustomComprehensiveVerificationStruct( flexIdResult.CustomComprehensiveVerification);
            List<IFlexIDModelSequencedHRI> models = new List<IFlexIDModelSequencedHRI>();
            if (flexIdResult.Models != null)
            {
                foreach (ServiceReference.FlexIDModelSequencedHRI model in flexIdResult.Models)
                {
                    models.Add(new FlexIDModelSequencedHRI(model));
                }
                Models = models.ToArray();
            }
            InstantIDVersion = flexIdResult.InstantIDVersion;
        }
        public IFlexIDSearchBy InputEcho { get; set; }

        public string UniqueId { get; set; }

        public string VerifiedSSN { get; set; }

        public IFlexIDNameAddressPhone NameAddressPhone { get; set; }

        public IFlexIDVerifiedElementSummary VerifiedElementSummary { get; set; }

        public IFlexIDValidElementSummary ValidElementSummary { get; set; }

        public uint NameAddressSSNSummary { get; set; }

        public IFlexIDComprehensiveVerificationStruct ComprehensiveVerification { get; set; }

        public IFlexIDCustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }

        public IFlexIDModelSequencedHRI[] Models { get; set; }

        public string InstantIDVersion { get; set; }
    }
}
