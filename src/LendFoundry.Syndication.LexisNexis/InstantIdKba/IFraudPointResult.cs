﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointResult
    {
        IFraudPointInputEcho InputEcho { get; set; }

        IFraudPointNameValuePair[] Attributes { get; set; }

        IFraudPointRedFlagsReport RedFlagsReport { get; set; }

        IFraudPointModelWithIndices[] Models { get; set; }
    }
}
