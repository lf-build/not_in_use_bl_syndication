﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IPhoneNumberType
    {
        string PhoneNumber { get; set; }

        PhoneNumberContextType PhoneNumberContext { get; set; }
    }
}
