﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum LanguageType
    {
        Spanish,
        InformalSpanish,
        English,
    }
}
