﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IDecedentInfo
    {
        IName Name{get;set;}

        IDate DOD{get;set;}

        IDate DOB{get;set;}
    }
}
