﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointModelWithIndices : IFraudPointModelWithIndices
    {
        public FraudPointModelWithIndices(ServiceReference.FraudPointModelWithIndices fraudPointModelWithIndices)
        {
            if (fraudPointModelWithIndices == null)
                return;
            Name = fraudPointModelWithIndices.Name;
            List<IFraudPointScoreWithIndices> scores = new List<IFraudPointScoreWithIndices>();
            if (fraudPointModelWithIndices.Scores != null)
            {
                foreach (ServiceReference.FraudPointScoreWithIndices score in fraudPointModelWithIndices.Scores)
                {
                    scores.Add(new FraudPointScoreWithIndices(score));
                }
                Scores = scores.ToArray();
            }
        }
        public string Name { get; set; }

        public IFraudPointScoreWithIndices[] Scores { get; set; }
    }
}
