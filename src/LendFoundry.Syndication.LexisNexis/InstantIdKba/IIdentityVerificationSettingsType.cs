﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IIdentityVerificationSettingsType : ISettingsType
    {
        TaskType Task { get; set; }

        string SequenceId { get; set; }

        string Agent { get; set; }

        bool PatriotActCompliance { get; set; }
        bool PatriotActComplianceSpecified { get; set; }

        long ParentTransactionId { get; set; }
        bool ParentTransactionIdSpecified { get; set; }

    }
}
