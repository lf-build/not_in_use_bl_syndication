﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDResponseHeader : IFlexIDResponseHeader
    {
        public FlexIDResponseHeader(ServiceReference.FlexIDResponseHeader flexIdResponseHeader)
        {
            if (flexIdResponseHeader == null)
                return;
            Status = flexIdResponseHeader.Status;
            Message = flexIdResponseHeader.Message;
            QueryId = flexIdResponseHeader.QueryId;
            TransactionId = flexIdResponseHeader.TransactionId;
            List<IFlexIDWsException> exceptions = new List<IFlexIDWsException>();
            if (flexIdResponseHeader.Exceptions != null)
            {
                foreach (ServiceReference.FlexIDWsException exception in flexIdResponseHeader.Exceptions)
                {
                    exceptions.Add(new FlexIDWsException(exception));
                }
                Exceptions = exceptions.ToArray();
            }
        }
        public int Status { get; set; }

        public string Message { get; set; }

        public string QueryId { get; set; }

        public string TransactionId { get; set; }

        public IFlexIDWsException[] Exceptions { get; set; }
    }
}
