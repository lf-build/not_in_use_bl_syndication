﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class WsException : IWsException
    {
        public WsException(ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        public string Source { get; set; }

        public string Code { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }
    }
}
