﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IIdentityAssertionType
    {
        IComplexDetailType Data { get; set; }

        ITransactionType Transaction { get; set; }
    }
}
