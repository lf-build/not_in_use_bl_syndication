﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InformationType : IInformationType
    {
        public InformationType(ServiceReference.informationtype informationType)
        {
            if (informationType == null)
                return;
            InformationCode = (InformationCodeType)(int)informationType.informationcode;
            DetailCode = informationType.detailcode;
            DetailDescription = informationType.detaildescription;
            List<IComplexDetailType> complexdetails = new List<IComplexDetailType>();
            if (informationType.complexdetail != null)
            {
                foreach (ServiceReference.complexdetailtype complexdetail in informationType.complexdetail)
                {
                    complexdetails.Add(new ComplexDetailType(complexdetail));
                }
                ComplexDetail = complexdetails.ToArray();
            }
            List<ISimpleDetailType> simpleDetails = new List<ISimpleDetailType>();
            if (informationType.simpledetail != null)
            {
                foreach (ServiceReference.simpledetailtype simpleDetail in informationType.simpledetail)
                {
                    simpleDetails.Add(new SimpleDetailType(simpleDetail));
                }
                SimpleDetail = simpleDetails.ToArray();
            }
        }
        public InformationCodeType InformationCode {get;set;}

        public string DetailCode {get;set;}

        public string DetailDescription {get;set;}

        public IComplexDetailType[] ComplexDetail{get;set;}

        public ISimpleDetailType[] SimpleDetail{get;set;}
    }
}
