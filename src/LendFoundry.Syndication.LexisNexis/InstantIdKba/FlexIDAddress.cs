﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDAddress : IFlexIDAddress
    {
        public FlexIDAddress(ServiceReference.FlexIDAddress flexIdAddress)
        {
            if (flexIdAddress == null)
                return;
            StreetNumber = flexIdAddress.StreetNumber;
            StreetPreDirection = flexIdAddress.StreetPreDirection;
            StreetName = flexIdAddress.StreetName;
            StreetSuffix = flexIdAddress.StreetSuffix;
            StreetPostDirection = flexIdAddress.StreetPostDirection;
            UnitDesignation = flexIdAddress.UnitDesignation;
            UnitNumber = flexIdAddress.UnitNumber;
            StreetAddress1 = flexIdAddress.StreetAddress1;
            StreetAddress2 = flexIdAddress.StreetAddress2;
            City = flexIdAddress.City;
            State = flexIdAddress.State;
            Zip4 = flexIdAddress.Zip4;
            Zip5 = flexIdAddress.Zip5;
            County = flexIdAddress.County;
            PostalCode = flexIdAddress.PostalCode;
            StateCityZip = flexIdAddress.StateCityZip;
        }
        public string StreetNumber { get; set; }

        public string StreetPreDirection { get; set; }

        public string StreetName { get; set; }

        public string StreetSuffix { get; set; }

        public string StreetPostDirection { get; set; }

        public string UnitDesignation { get; set; }

        public string UnitNumber { get; set; }

        public string StreetAddress1 { get; set; }

        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip5 { get; set; }

        public string Zip4 { get; set; }

        public string County { get; set; }

        public string PostalCode { get; set; }

        public string StateCityZip { get; set; }
    }
}
