﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class RiskIndicator : IRiskIndicator
    {
        public RiskIndicator(ServiceReference.RiskIndicator riskIndicator)
        {
            if (riskIndicator == null)
                return;
            RiskCode = riskIndicator.RiskCode;
            Description = riskIndicator.Description;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }
    }
}
