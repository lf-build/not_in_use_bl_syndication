﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDSequencedRiskIndicator
    {
        string RiskCode { get; set; }

        string Description { get; set; }

        int Sequence { get; set; }

    }
}
