﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum PaymentMethod
    {
        StoreCredit,
        GiftCard,
        MoneyTransfer,
        Cash,
        Check,
        CreditCard,
        DebitCard,
        Phone,
        BillGeneral,
        BillPaper,
        Margin,
        Other
    }
}
