﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInputCorrected
    {
        IName Name { get; set; }

        IAddress Address { get; set; }

        string SSN { get; set; }

        string HomePhone { get; set; }

        IDate DOB { get; set; }
    }
}
