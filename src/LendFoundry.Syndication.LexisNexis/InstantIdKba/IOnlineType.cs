﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IOnlineType
    {
        ICredentialType Credential { get; set; }
    }
}
