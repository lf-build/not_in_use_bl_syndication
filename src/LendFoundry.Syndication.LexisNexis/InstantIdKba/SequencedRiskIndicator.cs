﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SequencedRiskIndicator : ISequencedRiskIndicator
    {
        public SequencedRiskIndicator(ServiceReference.SequencedRiskIndicator sequencedRiskIndicator)
        {
            if (sequencedRiskIndicator == null)
                return;
            RiskCode = sequencedRiskIndicator.RiskCode;
            Description = sequencedRiskIndicator.Description;
            Sequence = sequencedRiskIndicator.Sequence;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }

        public string Sequence { get; set; }
    }
}
