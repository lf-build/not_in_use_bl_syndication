﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AnswersType : IAnswersType
    {
        public AnswersType(ServiceReference.answerstype answersType)
        {
            if (answersType == null)
                return;
            QuestionSetId = answersType.questionsetid;
            List<IAnswerType> answers = new List<IAnswerType>();
            if (answersType.answer != null)
            {
                foreach (ServiceReference.answertype answer in answersType.answer)
                {
                    answers.Add(new AnswerType(answer));
                }
                Answer = answers.ToArray();
            }
        }
        public long QuestionSetId { get; set; }

        public IAnswerType[] Answer { get; set; }
    }
}
