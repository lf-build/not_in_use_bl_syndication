﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum LanguageVenueType
    {
        CallCenter,
        PointOfSale,
        Online,
        Ivr,
    }
}
