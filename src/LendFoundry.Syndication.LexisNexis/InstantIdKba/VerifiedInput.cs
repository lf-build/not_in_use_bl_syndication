﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class VerifiedInput : IVerifiedInput
    {
        public VerifiedInput(ServiceReference.VerifiedInput verifiedInput)
        {
            if (verifiedInput == null)
                return;
            Name = new Name(verifiedInput.Name);
            Address = new Address(verifiedInput.Address);
            SSN = verifiedInput.SSN;
            HomePhone = verifiedInput.HomePhone;
            DOB = new MaskableDate(verifiedInput.DOB);
            DriverLicenseNumber = verifiedInput.DriverLicenseNumber;
        }
        public IName Name { get; set; }

        public IAddress Address { get; set; }

        public string SSN { get; set; }

        public string HomePhone { get; set; }

        public IMaskableDate DOB { get; set; }

        public string DriverLicenseNumber { get; set; }
    }
}
