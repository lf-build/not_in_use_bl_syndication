﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAccountMaintenanceType
    {
        AccountCategoryType AccountCategory { get; set; }

        AccountMaintenanceCategoryType AccountMaintenanceCategory { get; set; }

        string MaintenanceDetail { get; set; }

        IAccountType Account { get; set; }

        IComplexDetailType Data { get; set; }
    }
}
