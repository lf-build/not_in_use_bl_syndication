﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointModelWithIndices
    {
        string Name { get; set; }

        IFraudPointScoreWithIndices[] Scores { get; set; }
    }
}
