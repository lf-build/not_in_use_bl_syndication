﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IExternalResponsesType
    {
        IInstantIDResponseEx InstantIDResponse { get; set; }

        IFlexIDResponseEx FlexIDResponse { get; set; }

        IFraudPointResponseEx FraudPointResponse { get; set; }
    }
}
