﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ChronologyHistory : IChronologyHistory
    {
        public ChronologyHistory(ServiceReference.ChronologyHistory chronologyHistory)
        {
            if (chronologyHistory == null)
                return;
            Address = new Address(chronologyHistory.Address);
            Phone = chronologyHistory.Phone;
            DateFirstSeen = new Date(chronologyHistory.DateFirstSeen);
            DateLastSeen = new Date(chronologyHistory.DateLastSeen);
            IsBestAddress = chronologyHistory.IsBestAddress;
        }
        public IAddress Address { get; set; }

        public string Phone { get; set; }

        public IDate DateFirstSeen { get; set; }

        public IDate DateLastSeen { get; set; }

        public bool IsBestAddress { get; set; }

    }
}
