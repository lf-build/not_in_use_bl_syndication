﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class BillToType : IBillToType
    {
        public BillToType(ServiceReference.billtotype billToType)
        {
            if (billToType == null)
                return;
            Item = billToType.Item;
        }
        public object Item { get; set; }
    }
}
