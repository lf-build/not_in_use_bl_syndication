﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointNameValuePair : IFraudPointNameValuePair
    {
        public FraudPointNameValuePair(ServiceReference.FraudPointNameValuePair fraudPointNameValuePair)
        {
            if (fraudPointNameValuePair == null)
                return;
            Name = fraudPointNameValuePair.Name;
            Value = fraudPointNameValuePair.Value;
        }
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
