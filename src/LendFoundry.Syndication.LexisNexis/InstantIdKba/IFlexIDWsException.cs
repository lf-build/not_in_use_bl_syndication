﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDWsException
    {
        string Source { get; set; }

        int Code { get; set; }

        string Location { get; set; }

        string Message { get; set; }
    }
}
