﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class PhoneNumberType : IPhoneNumberType
    {
        public PhoneNumberType(ServiceReference.phonenumbertype phoneNumberType)
        {
            if (phoneNumberType == null)
                return;
            PhoneNumber = phoneNumberType.phonenumber;
            PhoneNumberContext = (PhoneNumberContextType)(int)phoneNumberType.phonenumbercontext;
        }
        public string PhoneNumber { get; set; }

        public PhoneNumberContextType PhoneNumberContext { get; set; }
    }
}
