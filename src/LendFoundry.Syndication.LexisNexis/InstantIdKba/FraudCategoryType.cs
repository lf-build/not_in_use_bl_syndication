﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum FraudCategoryType
    {
        GeneralFraud,
        FriendlyFraud,
        FalseClaimFraud,
        IdentityTheftFraud,
        Indeterminate,
        None
    }
}
