﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDPassport
    {
        string Number { get; set; }

        IFlexIDDate ExpirationDate { get; set; }

        string Country { get; set; }

        string MachineReadableLine1 { get; set; }

        string MachineReadableLine2 { get; set; }
    }
}
