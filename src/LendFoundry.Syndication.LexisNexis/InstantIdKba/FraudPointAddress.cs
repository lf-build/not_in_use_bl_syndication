﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointAddress : IFraudPointAddress
    {
        public FraudPointAddress(ServiceReference.FraudPointAddress fraudPointAddress)
        {
            if (fraudPointAddress == null)
                return;
            StreetNumber = fraudPointAddress.StreetNumber;
            StreetPreDirection = fraudPointAddress.StreetPreDirection;
            StreetName = fraudPointAddress.StreetName;
            StreetSuffix = fraudPointAddress.StreetSuffix;
            StreetPostDirection = fraudPointAddress.StreetPostDirection;
            UnitDesignation = fraudPointAddress.UnitDesignation;
            UnitNumber = fraudPointAddress.UnitNumber;
            StreetAddress1 = fraudPointAddress.StreetAddress1;
            StreetAddress2 = fraudPointAddress.StreetAddress2;
            City = fraudPointAddress.City;
            State = fraudPointAddress.State;
            Zip4 = fraudPointAddress.Zip4;
            Zip5 = fraudPointAddress.Zip5;
            County = fraudPointAddress.County;
            PostalCode = fraudPointAddress.PostalCode;
            StateCityZip = fraudPointAddress.StateCityZip;
        }
        public string StreetNumber { get; set; }

        public string StreetPreDirection { get; set; }

        public string StreetName { get; set; }

        public string StreetSuffix { get; set; }

        public string StreetPostDirection { get; set; }

        public string UnitDesignation { get; set; }

        public string UnitNumber { get; set; }

        public string StreetAddress1 { get; set; }

        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip5 { get; set; }

        public string Zip4 { get; set; }

        public string County { get; set; }

        public string PostalCode { get; set; }

        public string StateCityZip { get; set; }
    }
}
