﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum RiskCategory
    {
        Transaction,
        Identity,
        Verification,
        Biometric,
        Total,
        Credit,
    }
}
