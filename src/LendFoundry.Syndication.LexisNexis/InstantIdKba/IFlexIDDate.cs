﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDDate
    {
        short Year { get; set; }

        short Month { get; set; }

        short Day { get; set; }

    }
}
