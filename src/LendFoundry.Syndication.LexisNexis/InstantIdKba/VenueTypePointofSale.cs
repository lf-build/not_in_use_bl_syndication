﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class VenueTypePointofSale : IVenueTypePointofSale
    {
        public VenueTypePointofSale(ServiceReference.venuetypePointofsale venueTypePointOfSale)
        {
            if (venueTypePointOfSale == null)
                return;
            List<CredentialMethodType> credentialmethods = new List<CredentialMethodType>();
            if (venueTypePointOfSale.credentialmethod != null)
            {
                foreach (ServiceReference.credentialmethodtype credentialmethod in venueTypePointOfSale.credentialmethod)
                {
                    credentialmethods.Add((CredentialMethodType)(int)credentialmethod);
                }
                CredentialMethod = credentialmethods.ToArray();
            }
            MerchantZipCode = venueTypePointOfSale.merchantzipcode;
        }
        public CredentialMethodType[] CredentialMethod { get; set; }

        public string MerchantZipCode { get; set; }
    }
}
