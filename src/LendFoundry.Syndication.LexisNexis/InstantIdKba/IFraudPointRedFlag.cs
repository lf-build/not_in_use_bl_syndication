﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointRedFlag
    {
        string Name { get; set; }

        IFraudPointSequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
