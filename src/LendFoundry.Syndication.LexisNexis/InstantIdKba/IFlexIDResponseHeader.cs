﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDResponseHeader
    {
        int Status { get; set; }

        string Message { get; set; }

        string QueryId { get; set; }

        string TransactionId { get; set; }

        IFlexIDWsException[] Exceptions { get; set; }
    }
}
