﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class QuestionsType : IQuestionsType
    {
        public QuestionsType(ServiceReference.questionstype questionsType)
        {
            if (questionsType == null)
                return;
            QuestionSetId = questionsType.questionsetid;
            List<IQuestionType> questions = new List<IQuestionType>();
            if (questionsType.question != null)
            {
                foreach (ServiceReference.questiontype question in questionsType.question)
                {
                    questions.Add(new QuestionType(question));
                }
                Question = questions.ToArray();
            }
        }
        public long QuestionSetId { get; set; }

        public IQuestionType[] Question { get; set; }
    }
}
