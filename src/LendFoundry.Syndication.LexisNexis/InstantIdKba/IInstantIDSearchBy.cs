﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIDSearchBy
    {
        IName Name { get; set; }

        IAddress Address { get; set; }

        IDate DOB { get; set; }

        string Age { get; set; }

        string SSN { get; set; }

        string SSNLast4 { get; set; }

        string DriverLicenseNumber { get; set; }

        string DriverLicenseState { get; set; }

        string IPAddress { get; set; }

        string HomePhone { get; set; }

        string WorkPhone { get; set; }

        bool UseDOBFilter { get; set; }

        string DOBRadius { get; set; }

        IPassport Passport { get; set; }

        string Gender { get; set; }

        string Email { get; set; }

        string Income { get; set; }

        string LocationIdentifier { get; set; }

        string OtherApplicationIdentifier1 { get; set; }

        string OtherApplicationIdentifier2 { get; set; }

        string OtherApplicationIdentifier3 { get; set; }

        ITimeStamp ApplicationDateTime { get; set; }
    }
}
