﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface INameAddressPhone
    {
        string Summary { get; set; }

        string Type { get; set; }

        string Status { get; set; }
    }
}
