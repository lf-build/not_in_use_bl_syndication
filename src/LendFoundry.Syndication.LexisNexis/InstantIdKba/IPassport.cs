﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IPassport
    {
        string Number { get; set; }

        IDate ExpirationDate { get; set; }

        string Country { get; set; }

        string MachineReadableLine1 { get; set; }

        string MachineReadableLine2 { get; set; }
    }
}
