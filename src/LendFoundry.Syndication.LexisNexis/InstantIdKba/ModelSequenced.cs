﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ModelSequenced: IModelSequenced
    {
        public ModelSequenced(ServiceReference.ModelSequenced modelSequenced)
        {
            if (modelSequenced == null)
                return;
            Name = modelSequenced.Name;
            List<IScoreSequenced> scores = new List<IScoreSequenced>();
            if (modelSequenced.Scores != null)
            {
                foreach (ServiceReference.ScoreSequenced score in modelSequenced.Scores)
                {
                    scores.Add(new ScoreSequenced(score));
                }
                Scores = scores.ToArray();
            }
        }
        public string Name { get; set; }

        public IScoreSequenced[] Scores { get; set; }
    }
}
