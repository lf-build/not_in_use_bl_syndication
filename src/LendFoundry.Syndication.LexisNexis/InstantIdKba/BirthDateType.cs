﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class BirthDateType : IBirthDateType
    {
        public BirthDateType(ServiceReference.birthdatetype birthdatetype)
        {
            if (birthdatetype == null)
                return;
            Year = birthdatetype.year;
            Month = birthdatetype.month;
            Day = birthdatetype.day;
        }
        public string Year { get; set; }

        public string Month { get; set; }

        public string Day { get; set; }

    }
}
