﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class IdentityVerificationSettingsType : IIdentityVerificationSettingsType
    {
        public IdentityVerificationSettingsType()
        { }
        public IdentityVerificationSettingsType(ServiceReference.identityverificationsettingstype identityVerificationSettingsType)
        {
            if (identityVerificationSettingsType == null)
                return;
            Task = (TaskType)(int)identityVerificationSettingsType.task;
            SequenceId = identityVerificationSettingsType.sequenceid;
            Agent = identityVerificationSettingsType.agent;
            PatriotActCompliance = identityVerificationSettingsType.patriotactcompliance;
            ParentTransactionId = identityVerificationSettingsType.parenttransactionid;
            AccountName = identityVerificationSettingsType.accountname;
            Mode = (ModeType)(int)identityVerificationSettingsType.mode;
            RuleSet = identityVerificationSettingsType.ruleset;
            SimulatorMode = (SimulatorModeType)(int)identityVerificationSettingsType.simulatormode;
            AttachmentType = (AttachmentType)(int)identityVerificationSettingsType.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (identityVerificationSettingsType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in identityVerificationSettingsType.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (identityVerificationSettingsType.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in identityVerificationSettingsType.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = identityVerificationSettingsType.referenceid;
            AttachmentTypeSpecified = identityVerificationSettingsType.attachmenttypeSpecified;
            ModeSpecified = identityVerificationSettingsType.modeSpecified;
            ParentTransactionIdSpecified = identityVerificationSettingsType.parenttransactionidSpecified;
            PatriotActComplianceSpecified = identityVerificationSettingsType.patriotactcomplianceSpecified;
            SimulatorModeSpecified = identityVerificationSettingsType.simulatormodeSpecified;
        }

        public TaskType Task { get; set; }

        public string SequenceId { get; set; }

        public string Agent { get; set; }

        public bool PatriotActCompliance { get; set; }
        public bool PatriotActComplianceSpecified { get; set; }

        public long ParentTransactionId { get; set; }
        public bool ParentTransactionIdSpecified { get; set; }

        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
