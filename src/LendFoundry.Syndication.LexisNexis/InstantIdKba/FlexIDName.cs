﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDName : IFlexIDName
    {
        public FlexIDName(ServiceReference.FlexIDName flexIdName)
        {
            if (flexIdName == null)
                return;
            Full = flexIdName.Full;
            First = flexIdName.First;
            Middle = flexIdName.Middle;
            Last = flexIdName.Last;
            Suffix = flexIdName.Suffix;
            Prefix = flexIdName.Prefix;
        }
        public string Full { get; set; }

        public string First { get; set; }

        public string Middle { get; set; }

        public string Last { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
