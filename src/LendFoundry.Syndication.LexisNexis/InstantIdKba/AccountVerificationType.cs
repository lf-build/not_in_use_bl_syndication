﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AccountVerificationType : IAccountVerificationType
    {
        public AccountVerificationType(ServiceReference.accountverificationtype accountVerificationType)
        {
            if (accountVerificationType == null)
                return;
            Item = accountVerificationType.Item;
            Venue = new VenueType(accountVerificationType.venue);
            ActivityDate = accountVerificationType.activitydate;
            RiskAssessment = new RiskAssessmentType(accountVerificationType.riskassessment);
        }
        public object Item { get; set; }

        public IVenueType Venue { get; set; }

        public System.DateTime ActivityDate { get; set; }

        public IRiskAssessmentType RiskAssessment { get; set; }
    }
}
