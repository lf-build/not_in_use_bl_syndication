﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class CredentialType : ICredentialType
    {
        public CredentialType(ServiceReference.credentialtype credentialType)
        {
            if (credentialType == null)
                return;
            List<CredentialMethodType> credentials = new List<CredentialMethodType>();
            if (credentialType.credentialmethod != null)
            {
                foreach (var credential in credentialType.credentialmethod)
                {
                    credentials.Add((CredentialMethodType)(int)credential);
                }
                CredentialMethod = credentials.ToArray();
            }
            Username = credentialType.username;
            Domain = credentialType.domain;
            OrganizationalUnit = credentialType.organizationalunit;
            HttpHeader = new HttpHeaderType(credentialType.httpheader);
            IpAddress = credentialType.ipaddress;
            MachineId = credentialType.machineid;
        }
        public CredentialMethodType[] CredentialMethod { get; set; }

        public string Username { get; set; }

        public string Domain { get; set; }

        public string OrganizationalUnit { get; set; }

       public IHttpHeaderType HttpHeader { get; set; }

        public string IpAddress { get; set; }

        public string[] MachineId { get; set; }
    }
}
