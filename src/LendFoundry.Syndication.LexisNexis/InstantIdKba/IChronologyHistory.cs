﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IChronologyHistory
    {
        IAddress Address{get;set;}

        string Phone{get;set;}

        IDate DateFirstSeen{get;set;}

        IDate DateLastSeen{get;set;}

        bool IsBestAddress{get;set;}

    }
}
