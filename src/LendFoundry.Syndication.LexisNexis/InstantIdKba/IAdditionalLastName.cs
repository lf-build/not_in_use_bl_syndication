﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAdditionalLastName
    {
        IDate DateLastSeen{get;set;}

        string LastName{get;set;}
    }
}
