﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IBirthDateType
    {
        string Year { get; set; }

        string Month { get; set; }

        string Day { get; set; }
    }
}
