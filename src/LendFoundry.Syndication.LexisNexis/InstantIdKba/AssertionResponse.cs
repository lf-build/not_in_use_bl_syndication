﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AssertionResponse : IAssertionResponse
    {
        public AssertionResponse(ServiceReference.assertionresponse assertionResponse)
        {
            if (assertionResponse == null)
                return;
            AssertionStatus = new TransactionStatusType(assertionResponse.assertionstatus);
            Information = new InformationType(assertionResponse.information);
        }
        public ITransactionStatusType AssertionStatus { get; set; }

        public IInformationType Information { get; set; }
    }
}
