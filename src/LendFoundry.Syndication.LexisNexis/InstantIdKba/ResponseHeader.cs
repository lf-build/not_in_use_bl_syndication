﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ResponseHeader : IResponseHeader
    {
        public ResponseHeader(ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            List<IWsException> exceptions = new List<IWsException>();
            if (responseHeader.Exceptions != null)
            {
                foreach (ServiceReference.WsException exception in responseHeader.Exceptions)
                {
                    exceptions.Add(new WsException(exception));
                }
                Exceptions = exceptions.ToArray();
            }
        }
        public string Status { get; set; }

        public string Message { get; set; }

        public string QueryId { get; set; }

        public string TransactionId { get; set; }

        public IWsException[] Exceptions { get; set; }
    }
}
