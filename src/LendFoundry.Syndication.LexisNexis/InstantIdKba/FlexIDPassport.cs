﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDPassport : IFlexIDPassport
    {
        public FlexIDPassport(ServiceReference.FlexIDPassport flexIdPassport)
        {
            if (flexIdPassport == null)
                return;
            Number = flexIdPassport.Number;
            ExpirationDate = new FlexIDDate(flexIdPassport.ExpirationDate);
            Country = flexIdPassport.Country;
            MachineReadableLine1 = flexIdPassport.MachineReadableLine1;
            MachineReadableLine2 = flexIdPassport.MachineReadableLine2;
        }
        public string Number { get; set; }

        public IFlexIDDate ExpirationDate { get; set; }

        public string Country { get; set; }

        public string MachineReadableLine1 { get; set; }

        public string MachineReadableLine2 { get; set; }
    }
}
