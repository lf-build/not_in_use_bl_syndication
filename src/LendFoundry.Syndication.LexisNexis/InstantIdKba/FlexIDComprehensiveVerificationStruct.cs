﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDComprehensiveVerificationStruct : IFlexIDComprehensiveVerificationStruct
    {
        public FlexIDComprehensiveVerificationStruct(ServiceReference.FlexIDComprehensiveVerificationStruct flexIdComprehensiveVerificationStruct)
        {
            if (flexIdComprehensiveVerificationStruct == null)
                return;
            ComprehensiveVerificationIndex = flexIdComprehensiveVerificationStruct.ComprehensiveVerificationIndex;
            List<IFlexIDSequencedRiskIndicator> riskindicators = new List<IFlexIDSequencedRiskIndicator>();
            if (flexIdComprehensiveVerificationStruct.RiskIndicators != null)
            {
                foreach (ServiceReference.FlexIDSequencedRiskIndicator riskindicator in flexIdComprehensiveVerificationStruct.RiskIndicators)
                {
                    riskindicators.Add(new FlexIDSequencedRiskIndicator(riskindicator));
                }
                RiskIndicators = riskindicators.ToArray();
            }
            List<IFlexIDRiskIndicator> potentialfollowupactions = new List<IFlexIDRiskIndicator>();
            if (flexIdComprehensiveVerificationStruct.PotentialFollowupActions != null)
            {
                foreach (ServiceReference.FlexIDRiskIndicator potentialfollowupaction in flexIdComprehensiveVerificationStruct.PotentialFollowupActions)
                {
                    potentialfollowupactions.Add(new FlexIDRiskIndicator(potentialfollowupaction));
                }
                PotentialFollowupActions = potentialfollowupactions.ToArray();
            }
        }
        public int ComprehensiveVerificationIndex { get; set; }

        public IFlexIDSequencedRiskIndicator[] RiskIndicators { get; set; }

        public IFlexIDRiskIndicator[] PotentialFollowupActions { get; set; }
    }
}
