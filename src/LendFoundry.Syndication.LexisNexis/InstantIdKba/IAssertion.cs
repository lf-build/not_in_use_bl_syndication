﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAssertion
    {
        ISettingsType Settings { get; set; }

        IPersonType Person { get; set; }

        IAccountType Account { get; set; }

        IIdentityAssertionType[] IdentityAssertion { get; set; }
    }
}
