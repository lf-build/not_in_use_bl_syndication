﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDResult
    {
        IFlexIDSearchBy InputEcho { get; set; }

        string UniqueId { get; set; }

        string VerifiedSSN { get; set; }

        IFlexIDNameAddressPhone NameAddressPhone { get; set; }

        IFlexIDVerifiedElementSummary VerifiedElementSummary { get; set; }

        IFlexIDValidElementSummary ValidElementSummary { get; set; }

        uint NameAddressSSNSummary { get; set; }

        IFlexIDComprehensiveVerificationStruct ComprehensiveVerification { get; set; }

        IFlexIDCustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }

        IFlexIDModelSequencedHRI[] Models { get; set; }

        string InstantIDVersion { get; set; }
    }
}
