﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAccountActivityType
    {
        IAccountType Account { get; set; }

        IProductType[] AccountTransaction { get; set; }

        decimal TotalAmount { get; set; }

        decimal BeginningBalance { get; set; }

        decimal EndingBalance { get; set; }

    }
}
