﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IScoreSequenced
    {
        string Type { get; set; }

        string Value { get; set; }

        INameValuePair[] RiskIndices { get; set; }

        ISequencedRiskIndicator[] RiskIndicators { get; set; }
    }
}
