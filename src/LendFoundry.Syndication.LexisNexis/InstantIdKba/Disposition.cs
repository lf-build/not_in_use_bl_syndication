﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class Disposition : IDisposition
    {
        public Disposition(ServiceReference.disposition disposition)
        {
            if (disposition == null)
                return;
            Settings = new DispositionSettingsType(disposition.settings);
            Account = new AccountType(disposition.account);
            FraudCategory = (FraudCategoryType)(int)disposition.fraudcategory;
            DispositionStatus = (DispositionStatusType)(int)disposition.dispositionstatus;
            DispositionDate = disposition.dispositiondate;
        }
        public IDispositionSettingsType Settings { get; set; }

        public IAccountType Account { get; set; }

        public FraudCategoryType FraudCategory { get; set; }

        public DispositionStatusType DispositionStatus { get; set; }

        public System.DateTime DispositionDate { get; set; }

    }
}
