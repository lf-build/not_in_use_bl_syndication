﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IBatchType
    {
        string BatchId { get; set; }
    }
}