﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionStatusType
    {
        long TransactionId { get; set; }

        long TransactionRequestId { get; set; }

        string AccountsTransactionId { get; set; }

        string[] ReferenceId { get; set; }

        TransactionResultType TransactionResult { get; set; }

        IRiskAssessmentType RiskAssessment { get; set; }

        ISpecialFeatureType[] SpecialFeature { get; set; }
    }
}
