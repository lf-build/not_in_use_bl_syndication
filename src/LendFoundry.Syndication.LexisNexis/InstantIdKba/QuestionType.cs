﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class QuestionType : IQuestionType
    {
        public QuestionType(ServiceReference.questiontype questionType)
        {
            if (questionType == null)
                return;
            QuestionId = questionType.questionid;
            AnswerType = (AnswerSelectionType)(int)questionType.answertype;
            List<ITextType> texts = new List<ITextType>();
            if (questionType.text != null)
            {
                foreach (ServiceReference.texttype text in questionType.text)
                {
                    texts.Add(new TextType(text));
                }
                Text = texts.ToArray();
            }

            List<IChoiceType> choices = new List<IChoiceType>();
            if (questionType.choice != null)
            {
                foreach (ServiceReference.choicetype choice in questionType.choice)
                {
                    choices.Add(new ChoiceType(choice));
                }
                Choice = choices.ToArray();
            }

            List<ITextType> helptexts = new List<ITextType>();
            if (questionType.helptext != null)
            {
                foreach (ServiceReference.texttype helptext in questionType.helptext)
                {
                    helptexts.Add(new TextType(helptext));
                }
                HelpText = helptexts.ToArray();
            }
        }
        public long QuestionId { get; set; }

        public AnswerSelectionType AnswerType { get; set; }

        public ITextType[] Text { get; set; }

        public IChoiceType[] Choice { get; set; }

        public ITextType[] HelpText { get; set; }
    }
}
