﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointRiskIndicator
    {
        string RiskCode { get; set; }

        string Description { get; set; }
    }
}
