﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ProductType : IProductType
    {
        public ProductType(ServiceReference.producttype productType)
        {
            if (productType == null)
                return;
            ProductCategory = (ProductCategoryType)(int)productType.productcategory;
            Model = productType.model;
            Vendor = productType.vendor;
            Code = productType.code;
            Symbol = productType.symbol;
            TransactionDirection = (TransactionDirectionType)(int)productType.transactiondirection;
            ProductQuantity = productType.productquantity;
            PerItemAmount = productType.peritemamount;
            TransactionDate = productType.transactiondate;
            Data = new ComplexDetailType(productType.data);
        }
        public ProductCategoryType ProductCategory { get; set; }

        public string Model { get; set; }

        public string Vendor { get; set; }

        public string Code { get; set; }

        public string Symbol { get; set; }

        public TransactionDirectionType TransactionDirection { get; set; }

        public string ProductQuantity { get; set; }

        public decimal PerItemAmount { get; set; }

        public System.DateTime TransactionDate { get; set; }

        public IComplexDetailType Data { get; set; }
    }
}
