﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIdIdentityRequest : IInstantIdIdentityRequest
    {
        public string LexID { get; set; }
        public string NamePrefix { get; set; }
        public string NameFirst { get; set; }
        public string NameMiddle { get; set; }
        public string NameLast { get; set; }
        public string NameSuffix { get; set; }
        public string[] CustomerId { get; set; }
        public string[] Email { get; set; }
        public string Ssn { get; set; }
        public SsnType SsnType { get; set; }
        public string DriversLicenseNumber { get; set; }
        public string DriversLicenseState { get; set; }
        public IBirthDateType BirthDate { get; set; }
        public IAddressType[] Address { get; set; }
        public IUkAddressType[] UkAddress { get; set; }
        public IPhoneNumberType[] PhoneNumber { get; set; }
        public IBusinessType Business { get; set; }
        public IBusinessType[] Employer { get; set; }
        public string Occupation { get; set; }
        public IRiskAssessmentType RiskAssessment { get; set; }
    }
}