﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class NameValuePair : INameValuePair
    {
        public NameValuePair(ServiceReference.NameValuePair nameValuePair)
        {
            if (nameValuePair == null)
                return;
            Name = nameValuePair.Name;
            Value = nameValuePair.Value;
        }
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
