﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointInputEcho
    {
        IFraudPointName Name { get; set; }

        IFraudPointAddress Address { get; set; }

        IFraudPointDate DOB { get; set; }

        string SSN { get; set; }

        string Phone10 { get; set; }

        string WorkPhone { get; set; }

        string DriverLicenseNumber { get; set; }

        string DriverLicenseState { get; set; }

        string IPAddress { get; set; }

        string Email { get; set; }

        FraudPointChannelIdentifier Channel { get; set; }

        string Income { get; set; }

        FraudPointOwnRent OwnOrRent { get; set; }

        string LocationIdentifier { get; set; }

        string OtherApplicationIdentifier1 { get; set; }

        string OtherApplicationIdentifier2 { get; set; }

        string OtherApplicationIdentifier3 { get; set; }

        IFraudPointTimeStamp ApplicationDateTime { get; set; }

        string Grade { get; set; }
    }
}
