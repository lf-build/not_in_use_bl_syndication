﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface INameValuePair
    {
        string Name { get; set; }

        string Value { get; set; }
    }
}
