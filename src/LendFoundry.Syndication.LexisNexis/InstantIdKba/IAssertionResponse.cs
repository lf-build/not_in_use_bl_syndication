﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAssertionResponse
    {
        ITransactionStatusType AssertionStatus { get; set; }

        IInformationType Information { get; set; }
    }
}
