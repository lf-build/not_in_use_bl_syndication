﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IWatchList
    {
        string Table { get; set; }

        string RecordNumber { get; set; }

        IName Name { get; set; }

        IAddress Address { get; set; }

        string Country { get; set; }

        string EntityName { get; set; }

        string Sequence { get; set; }
    }
}
