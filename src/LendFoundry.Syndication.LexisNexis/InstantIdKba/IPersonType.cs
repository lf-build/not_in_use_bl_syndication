﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IPersonType
    {
        string LexID { get; set; }

        string NamePrefix { get; set; }

        string NameFirst { get; set; }

        string NameMiddle { get; set; }

        string NameLast { get; set; }

        string NameSuffix { get; set; }

        string[] CustomerId { get; set; }

        string[] Email { get; set; }

        string Ssn { get; set; }

        SsnType SsnType { get; set; }

        string DriversLicenseNumber { get; set; }

        string DriversLicenseState { get; set; }

        IBirthDateType BirthDate { get; set; }

        IAddressType[] Address { get; set; }

        IUkAddressType[] UkAddress { get; set; }

        IPhoneNumberType[] PhoneNumber { get; set; }

        IBusinessType Business { get; set; }

        IBusinessType[] Employer { get; set; }

        string Occupation { get; set; }

        IRiskAssessmentType RiskAssessment { get; set; }
    }
}
