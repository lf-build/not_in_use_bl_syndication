﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IShippingType
    {
        IBillToType BillTo { get; set; }

        IShipToType ShipTo { get; set; }

        ShippingMethod ShippingMethod { get; set; }

    }
}
