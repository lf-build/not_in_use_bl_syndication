﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointScoreWithIndices
    {
        string Type { get; set; }

        int Value { get; set; }

        IFraudPointNameValuePair[] RiskIndices { get; set; }

        IFraudPointRiskIndicator[] RiskIndicators { get; set; }
    }
}
