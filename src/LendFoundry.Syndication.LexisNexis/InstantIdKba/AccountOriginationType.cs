﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AccountOriginationType: IAccountOriginationType
    {
        public AccountOriginationType(ServiceReference.accountoriginationtype accountOriginationType)
        {
            if (accountOriginationType == null)
                return;
            AccountCategory = (AccountCategoryType)(int) accountOriginationType.accountcategory;
            List<ISecurityQuestionType> securityQuestions = new List<ISecurityQuestionType>();
            if (accountOriginationType.securityquestion != null)
            {
                foreach (ServiceReference.securityquestiontype securityquestion in accountOriginationType.securityquestion)
                {
                    securityQuestions.Add(new SecurityQuestionType(securityquestion));
                }
                SecurityQuestion = securityQuestions.ToArray();
            }
            
            VoiceEnrollment = accountOriginationType.voiceenrollment;
            Account = new AccountType(accountOriginationType.account);
        }
        public AccountCategoryType AccountCategory { get; set; }

        public ISecurityQuestionType[] SecurityQuestion { get; set; }

        public bool VoiceEnrollment { get; set; }

        public IAccountType Account { get; set; }
    }
}
