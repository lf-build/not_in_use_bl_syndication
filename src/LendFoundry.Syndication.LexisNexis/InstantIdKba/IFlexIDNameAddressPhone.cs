﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDNameAddressPhone
    {
        string Summary { get; set; }

        string Type { get; set; }

        string Status { get; set; }
    }
}
