﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum CredentialMethodType
    {
        Ssn,
        SecurityQuestion,
        Pin,
        Basic,
        Certificate,
        Token,
        Federated,
        DriversLicense,
        Passport,
        PhotoIdCard,
        AccountId,
        Voiceid,
        BirthDate,
        None
    }
}
