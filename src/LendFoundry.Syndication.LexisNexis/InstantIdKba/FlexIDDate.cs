﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDDate : IFlexIDDate
    {
        public FlexIDDate(ServiceReference.FlexIDDate flexIdDate)
        {
            if (flexIdDate == null)
                return;
            Year = flexIdDate.Year;
            Month = flexIdDate.Month;
            Day = flexIdDate.Day;
        }
        public short Year { get; set; }


        public short Month { get; set; }

        public short Day { get; set; }

    }
}
