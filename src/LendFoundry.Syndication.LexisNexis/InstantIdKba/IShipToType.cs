﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IShipToType
    {
        object Item { get; set; }
    }
}