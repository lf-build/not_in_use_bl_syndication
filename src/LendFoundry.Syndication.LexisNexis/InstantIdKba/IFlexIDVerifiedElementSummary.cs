﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDVerifiedElementSummary
    {
        bool FirstName { get; set; }

        bool LastName { get; set; }

        bool StreetAddress { get; set; }

        bool City { get; set; }

        bool State { get; set; }

        bool Zip { get; set; }

        bool HomePhone { get; set; }

        bool DOB { get; set; }

        string DOBMatchLevel { get; set; }

        bool SSN { get; set; }

        bool Dl { get; set; }

    }
}
