﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDResponse
    {
        IFlexIDResponseHeader Header { get; set; }

        IFlexIDResult Result { get; set; }
    }
}
