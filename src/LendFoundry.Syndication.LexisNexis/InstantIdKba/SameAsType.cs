﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum SameAsType
    {
        OrderedBy,
        BillTo
    }
}
