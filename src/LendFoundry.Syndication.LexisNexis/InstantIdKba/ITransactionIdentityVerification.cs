﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionIdentityVerification
    {
        IdentityVerificationSettingsType Settings {  get; set; }

        IPersonType Person { get; set; }

        ITransactionType Transaction { get; set; }

        IIdentityAssertionType[] Identityassertion { get; set; }
    }
}
