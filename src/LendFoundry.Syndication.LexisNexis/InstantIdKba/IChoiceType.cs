﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IChoiceType
    {
        long ChoiceId{get;set;}

        ITextType[] Text{get;set;}
    }
}
