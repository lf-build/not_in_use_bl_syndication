﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionContinue : ITransactionContinue
    {
        public TransactionContinue(ServiceReference.TransactionContinue transactionContinue)
        {
            if (transactionContinue == null)
                return;
            Settings = new ContinueSettingsType(transactionContinue.settings);
            Item = new AnswersType(transactionContinue.Item);
        }
        public IContinueSettingsType Settings { get; set; }

        public IAnswersType Item { get; set; }
    }
}
