﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointResult : IFraudPointResult
    {
        public FraudPointResult(ServiceReference.FraudPointResult fraudPointResult)
        {
            if (fraudPointResult == null)
                return;
            InputEcho = new FraudPointInputEcho(fraudPointResult.InputEcho);
            List<IFraudPointNameValuePair> attributes = new List<IFraudPointNameValuePair>();
            if (fraudPointResult.Attributes != null)
            {
                foreach (ServiceReference.FraudPointNameValuePair attribute in fraudPointResult.Attributes)
                {
                    attributes.Add(new FraudPointNameValuePair(attribute));
                }
                Attributes = attributes.ToArray();
            }
            RedFlagsReport = new FraudPointRedFlagsReport(fraudPointResult.RedFlagsReport);
            List<IFraudPointModelWithIndices> models = new List<IFraudPointModelWithIndices>();
            if (fraudPointResult.Models != null)
            {
                foreach (ServiceReference.FraudPointModelWithIndices model in fraudPointResult.Models)
                {
                    models.Add(new FraudPointModelWithIndices(model));
                }
                Models = models.ToArray();
            }
        }

        public IFraudPointInputEcho InputEcho { get; set; }

        public IFraudPointNameValuePair[] Attributes { get; set; }

        public IFraudPointRedFlagsReport RedFlagsReport { get; set; }

        public IFraudPointModelWithIndices[] Models { get; set; }
    }
}
