﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIDSearchBy : IInstantIDSearchBy
    {
        public InstantIDSearchBy(ServiceReference.InstantIDSearchBy instantIdSearchBy)
        {
            if (instantIdSearchBy == null)
                return;
            Name = new Name(instantIdSearchBy.Name);
            Address = new Address(instantIdSearchBy.Address);
            DOB = new Date(instantIdSearchBy.DOB);
            Age = instantIdSearchBy.Age;
            SSN = instantIdSearchBy.SSN;
            SSNLast4 = instantIdSearchBy.SSNLast4;
            DriverLicenseNumber = instantIdSearchBy.DriverLicenseNumber;
            DriverLicenseState = instantIdSearchBy.DriverLicenseState;
            IPAddress = instantIdSearchBy.IPAddress;
            HomePhone = instantIdSearchBy.HomePhone;
            WorkPhone = instantIdSearchBy.WorkPhone;
            UseDOBFilter = instantIdSearchBy.UseDOBFilter;
            DOBRadius = instantIdSearchBy.DOBRadius;
            Passport = new Passport(instantIdSearchBy.Passport);
            Gender = instantIdSearchBy.Gender;
            Email = instantIdSearchBy.Email;
            Income = instantIdSearchBy.Income;
            LocationIdentifier = instantIdSearchBy.LocationIdentifier;
            OtherApplicationIdentifier1 = instantIdSearchBy.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = instantIdSearchBy.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = instantIdSearchBy.OtherApplicationIdentifier3;
            ApplicationDateTime = new TimeStamp(instantIdSearchBy.ApplicationDateTime);
        }

        public IName Name { get; set; }

        public IAddress Address { get; set; }

        public IDate DOB { get; set; }

        public string Age { get; set; }

        public string SSN { get; set; }

        public string SSNLast4 { get; set; }

        public string DriverLicenseNumber { get; set; }

        public string DriverLicenseState { get; set; }

        public string IPAddress { get; set; }

        public string HomePhone { get; set; }

        public string WorkPhone { get; set; }

        public bool UseDOBFilter { get; set; }

        public string DOBRadius { get; set; }

        public IPassport Passport { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }

        public string Income { get; set; }

        public string LocationIdentifier { get; set; }

        public string OtherApplicationIdentifier1 { get; set; }

        public string OtherApplicationIdentifier2 { get; set; }

        public string OtherApplicationIdentifier3 { get; set; }

        public ITimeStamp ApplicationDateTime { get; set; }
    }
}
