﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDSearchBy
    {
        IFlexIDName Name { get; set; }

        IFlexIDAddress Address { get; set; }

        IFlexIDDate DOB { get; set; }

        uint Age { get; set; }

        string SSN { get; set; }

        string SSNLast4 { get; set; }

        string DriverLicenseNumber { get; set; }

        string DriverLicenseState { get; set; }

        string IPAddress { get; set; }

        string HomePhone { get; set; }

        string WorkPhone { get; set; }

        IFlexIDPassport Passport { get; set; }

        string Gender { get; set; }
    }
}
