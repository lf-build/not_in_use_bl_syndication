﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDComprehensiveVerificationStruct
    {
        int ComprehensiveVerificationIndex { get; set; }

        IFlexIDSequencedRiskIndicator[] RiskIndicators { get; set; }

        IFlexIDRiskIndicator[] PotentialFollowupActions { get; set; }
    }
}
