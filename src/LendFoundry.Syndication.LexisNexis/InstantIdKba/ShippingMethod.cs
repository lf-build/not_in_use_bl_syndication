﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum ShippingMethod
    {
        Ground,
        PriorityOvernight,
        NextDayAir,
        InstorePickup,
        SecondDayAir,
        PriorityGround,
    }
}
