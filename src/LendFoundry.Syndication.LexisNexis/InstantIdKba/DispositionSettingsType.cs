﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class DispositionSettingsType : IDispositionSettingsType
    {
        public DispositionSettingsType(ServiceReference.dispositionsettingstype dispositionSettingsType)
        {

            if (dispositionSettingsType == null)
                return;
            TransactionId = dispositionSettingsType.transactionid;
            AccountName = dispositionSettingsType.accountname;
            Mode = (ModeType)(int)dispositionSettingsType.mode;
            RuleSet = dispositionSettingsType.ruleset;
            SimulatorMode = (SimulatorModeType)(int)dispositionSettingsType.simulatormode;
            AttachmentType = (AttachmentType)(int)dispositionSettingsType.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (dispositionSettingsType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in dispositionSettingsType.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (dispositionSettingsType.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in dispositionSettingsType.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = dispositionSettingsType.referenceid;
            ModeSpecified = dispositionSettingsType.modeSpecified;
            AttachmentTypeSpecified = dispositionSettingsType.attachmenttypeSpecified;
            SimulatorModeSpecified = dispositionSettingsType.simulatormodeSpecified;
        }
        public long TransactionId { get; set; }

        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
