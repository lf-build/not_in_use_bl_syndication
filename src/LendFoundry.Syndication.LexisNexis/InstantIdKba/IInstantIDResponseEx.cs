﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIDResponseEx
    {
        IInstantIDResponse Response { get; set; }
    }
}
