﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class BatchType : IBatchType
    {
        public BatchType(ServiceReference.batchtype batchType)
        {
            if (batchType == null)
                return;
            BatchId = batchType.batchid;
        }
        public string BatchId { get; set; }
    }
}
