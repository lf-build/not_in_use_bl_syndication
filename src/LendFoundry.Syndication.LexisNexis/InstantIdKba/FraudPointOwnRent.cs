﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum FraudPointOwnRent
    {
        Item,
        Own,
        Rent,
    }
}
