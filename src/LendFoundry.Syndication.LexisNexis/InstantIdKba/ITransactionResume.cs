﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionResume
    {
        IResumeSettingsType Settings { get; set; }

        IAccountType Account { get; set; }
    }
}
