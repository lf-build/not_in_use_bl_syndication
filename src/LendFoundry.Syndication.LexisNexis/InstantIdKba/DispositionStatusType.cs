﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum DispositionStatusType
    {
        None,
        Open,
        Closed,
        Unknown,
        NotApplicable,
    }
}
