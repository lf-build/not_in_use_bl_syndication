﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class IdentityReversePhone : IIdentityReversePhone
    {
        public IdentityReversePhone(ServiceReference.IdentityReversePhone identityReversePhone)
        {
            if (identityReversePhone == null)
                return;
            Name = new Name(identityReversePhone.Name);
            Address = new Address(identityReversePhone.Address);
        }
        public IName Name { get; set; }

        public IAddress Address { get; set; }
    }
}
