﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IIvrType
    {
        CredentialMethodType[] CredentialMethod { get; set; }
    }
}
