﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDValidElementSummary : IFlexIDValidElementSummary
    {
        public FlexIDValidElementSummary(ServiceReference.FlexIDValidElementSummary flexIdValidelementSummary)
        {
            if (flexIdValidelementSummary == null)
                return;
            SSNValid = flexIdValidelementSummary.SSNValid;
            SSNDeceased = flexIdValidelementSummary.SSNDeceased;
            DLValid = flexIdValidelementSummary.DLValid;
            PassportValid = flexIdValidelementSummary.PassportValid;
            AddressPOBox = flexIdValidelementSummary.AddressPOBox;
            AddressCMRA = flexIdValidelementSummary.AddressCMRA;
            SSNFoundForLexID = flexIdValidelementSummary.SSNFoundForLexID;
        }
        public bool SSNValid { get; set; }

        public bool SSNDeceased { get; set; }

        public bool DLValid { get; set; }

        public bool PassportValid { get; set; }

        public bool AddressPOBox { get; set; }

        public bool AddressCMRA { get; set; }

        public bool SSNFoundForLexID { get; set; }

    }
}
