﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IRiskIndicator
    {
        string RiskCode { get; set; }

        string Description { get; set; }
    }
}
