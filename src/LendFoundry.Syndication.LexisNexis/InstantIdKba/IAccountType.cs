﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAccountType
    {
        string AccountNumber { get; set; }

        string AccountTransactionId { get; set; }

        string AccountName { get; set; }

        string[] CustomerId { get; set; }
    }
}
