﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAccountOriginationType
    {
        AccountCategoryType AccountCategory { get; set; }

        ISecurityQuestionType[] SecurityQuestion { get; set; }

        bool VoiceEnrollment { get; set; }

        IAccountType Account { get; set; }
    }
}
