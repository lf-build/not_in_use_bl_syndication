﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AdditionalLastName : IAdditionalLastName
    {
        public AdditionalLastName(ServiceReference.AdditionalLastName additionalLastName)
        {
            if (additionalLastName == null)
                return;
            DateLastSeen = new Date(additionalLastName.DateLastSeen);
            LastName = additionalLastName.LastName;
        }
        public IDate DateLastSeen { get; set; }

        public string LastName { get; set; }
    }
}
