﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ICustomerServiceType
    {
        CredentialMethodType[] CredentialMethod { get; set; }
    }
}
