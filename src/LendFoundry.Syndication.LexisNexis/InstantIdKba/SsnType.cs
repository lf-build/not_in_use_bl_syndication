﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum SsnType
    {
        Ssn4,
        Ssn9,
        Ssnfirst5,
        Nossn,
        Other,
    }
}
