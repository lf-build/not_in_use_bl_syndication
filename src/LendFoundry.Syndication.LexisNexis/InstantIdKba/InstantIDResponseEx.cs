﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIDResponseEx : IInstantIDResponseEx
    {
        public InstantIDResponseEx(ServiceReference.InstantIDResponseEx instantIdResponseEx)
        {
            if (instantIdResponseEx == null)
                return;
            Response = new InstantIDResponse(instantIdResponseEx.response);
        }
        public IInstantIDResponse Response { get; set; }
    }
}
