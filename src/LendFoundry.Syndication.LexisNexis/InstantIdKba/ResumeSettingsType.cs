﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ResumeSettingsType : IResumeSettingsType
    {
        public ResumeSettingsType(ServiceReference.resumesettingstype resumeSettingsType)
        {

            if (resumeSettingsType == null)
                return;
            TransactionId = resumeSettingsType.transactionid;
            AccountName = resumeSettingsType.accountname;
            Mode = (ModeType)(int)resumeSettingsType.mode;
            RuleSet = resumeSettingsType.ruleset;
            SimulatorMode = (SimulatorModeType)(int)resumeSettingsType.simulatormode;
            AttachmentType = (AttachmentType)(int)resumeSettingsType.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (resumeSettingsType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in resumeSettingsType.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (resumeSettingsType.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in resumeSettingsType.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = resumeSettingsType.referenceid;
            ModeSpecified = resumeSettingsType.modeSpecified;
            AttachmentTypeSpecified = resumeSettingsType.attachmenttypeSpecified;
            SimulatorModeSpecified = resumeSettingsType.simulatormodeSpecified;
        }
        public long TransactionId { get; set; }

        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
