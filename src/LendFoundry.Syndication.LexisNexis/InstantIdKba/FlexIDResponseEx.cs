﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDResponseEx : IFlexIDResponseEx
    {
        public FlexIDResponseEx(ServiceReference.FlexIDResponseEx flexIdResponseEx)
        {
            if (flexIdResponseEx == null)
                return;
            Response =  new FlexIDResponse(flexIdResponseEx.response);
        }
        public IFlexIDResponse Response { get; set; }
    }
}
