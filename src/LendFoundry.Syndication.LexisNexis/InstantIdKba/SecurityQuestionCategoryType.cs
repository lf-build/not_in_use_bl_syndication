﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum SecurityQuestionCategoryType
    {
        MothersMaidenName,
        Pet,
        BirthCity,
        HighSchool,
        MiddleSchool,
        GradeSchool,
        IceCream,
        UserDefined,
    }
}
