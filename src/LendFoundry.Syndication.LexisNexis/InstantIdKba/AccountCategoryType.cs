﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum AccountCategoryType
    {
        Banking,
        Brokerage,
        RetirementSavings,
        Retail,
        UtilityService,
        LoanApplication,
        CreditApplication,
        PhoneService,
        CellularPhoneService,
        CableTvService,
        SatelliteTvService,
        InternetService,
        MoneyTransfer,
        OnlineAccount,
        EmailAccount,
        MortgageApplication,
        PublicRecords,
        CreditCard
    }
}
