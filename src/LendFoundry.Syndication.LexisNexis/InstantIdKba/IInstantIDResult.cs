﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIDResult
    {
        IInstantIDSearchBy InputEcho { get; set; }

        string UniqueId { get; set; }

        IVerifiedInput VerifiedInput { get; set; }

        bool DOBVerified { get; set; }

        string NameAddressSSNSummary { get; set; }

        INameAddressPhone NameAddressPhone { get; set; }

        string ComprehensiveVerificationIndex { get; set; }

        ISequencedRiskIndicator[] RiskIndicators { get; set; }

        IRiskIndicator[] PotentialFollowupActions { get; set; }

        IInputCorrected InputCorrected { get; set; }

        INewAreaCode NewAreaCode { get; set; }

        IIdentityReversePhone ReversePhone { get; set; }

        string PhoneOfNameAddress { get; set; }

        ISSNInfo SSNInfo { get; set; }

        IChronologyHistory[] ChronologyHistories { get; set; }

        IWatchList[] WatchLists { get; set; }

        string AdditionalScore1 { get; set; }

        string AdditionalScore2 { get; set; }

        IName CurrentName { get; set; }

        IAdditionalLastName[] AdditionalLastNames { get; set; }

        IModelSequenced[] Models { get; set; }

        IRedFlagsReport RedFlagsReport { get; set; }

        bool PassportValidated { get; set; }

        string FoundSSNCount { get; set; }

        IDecedentInfo DecedentInfo { get; set; }

        string DOBMatchLevel { get; set; }
    }
}
