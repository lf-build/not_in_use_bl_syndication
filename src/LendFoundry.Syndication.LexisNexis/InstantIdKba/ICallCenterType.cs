﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ICallCenterType
    {
        string CallerPhoneNumber { get; set; }

        CredentialMethodType[] CredentialMethod { get; set; }

        string CallerId { get; set; }
    }
}
