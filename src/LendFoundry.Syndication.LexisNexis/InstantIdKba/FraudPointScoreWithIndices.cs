﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointScoreWithIndices : IFraudPointScoreWithIndices
    {
        public FraudPointScoreWithIndices(ServiceReference.FraudPointScoreWithIndices fraudPointScoreWithIndices)
        {
            if (fraudPointScoreWithIndices == null)
                return;
            Type = fraudPointScoreWithIndices.Type;
            Value = fraudPointScoreWithIndices.Value;
            List<IFraudPointNameValuePair> riskindices = new List<IFraudPointNameValuePair>();
            if (fraudPointScoreWithIndices.RiskIndices != null)
            {
                foreach (ServiceReference.FraudPointNameValuePair riskindice in fraudPointScoreWithIndices.RiskIndices)
                {
                    riskindices.Add(new FraudPointNameValuePair(riskindice));
                }
                RiskIndices = riskindices.ToArray();
            }

            List<IFraudPointRiskIndicator> riskIndicators = new List<IFraudPointRiskIndicator>();
            if (fraudPointScoreWithIndices.RiskIndicators != null)
            {
                foreach (ServiceReference.FraudPointRiskIndicator riskIndicator in fraudPointScoreWithIndices.RiskIndicators)
                {
                    riskIndicators.Add(new FraudPointRiskIndicator(riskIndicator));
                }
                RiskIndicators = riskIndicators.ToArray();
            }
        }
        public string Type { get; set; }

        public int Value { get; set; }

        public IFraudPointNameValuePair[] RiskIndices { get; set; }

        public IFraudPointRiskIndicator[] RiskIndicators { get; set; }
    }
}
