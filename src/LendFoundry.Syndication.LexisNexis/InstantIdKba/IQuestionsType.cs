﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IQuestionsType
    {
        long QuestionSetId { get; set; }

        IQuestionType[] Question { get; set; }
    }
}
