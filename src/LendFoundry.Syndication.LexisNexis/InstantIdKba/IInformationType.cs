﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInformationType
    {
        InformationCodeType InformationCode { get; set; }

        string DetailCode { get; set; }

        string DetailDescription { get; set; }

        IComplexDetailType[] ComplexDetail { get; set; }

        ISimpleDetailType[] SimpleDetail { get; set; }
    }
}
