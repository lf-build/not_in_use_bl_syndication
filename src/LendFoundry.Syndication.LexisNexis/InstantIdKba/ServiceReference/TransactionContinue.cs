﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference
{
    public partial class TransactionContinue
    {
        public TransactionContinue()
        { }
        public TransactionContinue(ITransactionContinue transactContinue)
        {
            if (transactContinue == null)
                return;

            #region Item
            List<answertype> answers = new List<answertype>();
            if (transactContinue.Item != null && transactContinue.Item.Answer != null)
            {
                foreach (AnswerType answer in transactContinue.Item.Answer)
                {
                    answers.Add(new answertype
                    {
                        choices = answer.Choices,
                        questionid = answer.QuestionId
                    });
                }
            }
            Item = new answerstype
            {
                answer = answers.ToArray(),
                questionsetid = transactContinue.Item.QuestionSetId
            };
            #endregion Item

            #region Settings
            List<internationalizationtype> internationalizations = new List<internationalizationtype>();
            if (transactContinue.Settings != null && transactContinue.Settings.Internationalization != null)
            {
                foreach (InternationalizationType internationalization in transactContinue.Settings.Internationalization)
                {
                    internationalizations.Add(new internationalizationtype()
                    {
                        language = (languagetype)(int)internationalization.Language,
                        languagevenue = (languagevenuetype)(int)internationalization.LanguageVenue,
                    });
                }
            }

            List<specialfeaturetype> specialfeatures = new List<specialfeaturetype>();
            if (transactContinue.Settings != null && transactContinue.Settings.SpecialFeature != null)
            {
                foreach (SpecialFeatureType specialfeature in transactContinue.Settings.SpecialFeature)
                {
                    specialfeatures.Add(new specialfeaturetype()
                    {
                        specialfeaturecode = specialfeature.SpecialFeatureCode,
                        specialfeaturevalue = specialfeature.SpecialFeatureValue
                    });
                }
            }
            settings = new continuesettingstype
            {
                accountname = transactContinue.Settings.AccountName,
                attachmenttype = (attachmenttype)(int)transactContinue.Settings.AttachmentType,
                internationalization = internationalizations.ToArray(),
                mode = (modetype)(int)transactContinue.Settings.Mode,
                referenceid = transactContinue.Settings.ReferenceId,
                ruleset = transactContinue.Settings.RuleSet,
                simulatormode = (simulatormodetype)(int)transactContinue.Settings.SimulatorMode,
                specialfeature = specialfeatures.ToArray(),
                transactionid = transactContinue.Settings.TransactionId
            };
            #endregion
        }
    }
}
