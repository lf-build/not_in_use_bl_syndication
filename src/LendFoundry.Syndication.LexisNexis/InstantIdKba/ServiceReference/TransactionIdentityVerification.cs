﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference
{
    public partial class TransactionIdentityVerification
    {
        public TransactionIdentityVerification()
        { }
        public TransactionIdentityVerification(ITransactionIdentityVerification transactIdentityVerification)
        {
            if (transactIdentityVerification == null)
                return;

            #region IdentityAssertion
            List<InstantIdKba.ServiceReference.identityassertiontype> identityassertions = new List<InstantIdKba.ServiceReference.identityassertiontype>();
            if (transactIdentityVerification.Identityassertion != null)
            {
                foreach (InstantIdKba.IdentityAssertionType identityassertion in transactIdentityVerification.Identityassertion)
                {
                    List<InstantIdKba.ServiceReference.simpledetailtype> simpleDetails = new List<InstantIdKba.ServiceReference.simpledetailtype>();
                    if (identityassertion.Data.SimpleDetail != null)
                    {
                        foreach (InstantIdKba.SimpleDetailType simpleDetail in identityassertion.Data.SimpleDetail)
                        {
                            simpleDetails.Add(new InstantIdKba.ServiceReference.simpledetailtype { text = simpleDetail.Text });
                        }
                    }

                    identityassertions.Add(new InstantIdKba.ServiceReference.identityassertiontype()
                    {
                        data = new InstantIdKba.ServiceReference.complexdetailtype
                        {
                            //complexdetail = identityassertion.Data.ComplexDetail,
                            heading = identityassertion.Data.Heading,
                            simpledetail = simpleDetails.ToArray()
                        },
                        transaction = new InstantIdKba.ServiceReference.transactiontype { Item = identityassertion.Transaction.Item }
                    });
                }
                identityassertion = identityassertions.ToArray();
            }
            #endregion

            #region Person
            List<InstantIdKba.ServiceReference.addresstype> addressTypes = new List<InstantIdKba.ServiceReference.addresstype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.Address != null)
            {
                foreach (InstantIdKba.AddressType addressType in transactIdentityVerification.Person.Address)
                {
                    addressTypes.Add(new InstantIdKba.ServiceReference.addresstype()
                    {
                        addresscity = addressType.AddressCity,
                        addresscontext = (InstantIdKba.ServiceReference.addresscontexttype)(int)addressType.AddressContext,
                        addresscountry = addressType.AddressCountry,
                        addressstate = addressType.AddressState,
                        addressstreet1 = addressType.AddressStreet1,
                        addressstreet2 = addressType.AddressStreet2,
                        addressyearsat = addressType.AddressYearsAt,
                        addresszip = addressType.AddressZip,
                        addresszipplus4 = addressType.AddressZipPlus4,
                        occupationstartdate = addressType.OccupationStartDate,
                        suite = addressType.Suite
                    });
                }
            }

            List<InstantIdKba.ServiceReference.phonenumbertype> phoneNumberTypes = new List<InstantIdKba.ServiceReference.phonenumbertype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.PhoneNumber != null)
            {
                foreach (InstantIdKba.PhoneNumberType phoneNumberType in transactIdentityVerification.Person.PhoneNumber)
                {
                    phoneNumberTypes.Add(new InstantIdKba.ServiceReference.phonenumbertype()
                    {
                        phonenumber = phoneNumberType.PhoneNumber,
                        phonenumbercontext = (InstantIdKba.ServiceReference.phonenumbercontexttype)(int)phoneNumberType.PhoneNumberContext
                    });
                }
            }

            List<InstantIdKba.ServiceReference.phonenumbertype> businessPhones = new List<InstantIdKba.ServiceReference.phonenumbertype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.Business != null && transactIdentityVerification.Person.Business.Phonenumber != null)
            {
                foreach (InstantIdKba.PhoneNumberType phoneNumberType in transactIdentityVerification.Person.Business.Phonenumber)
                {
                    businessPhones.Add(new InstantIdKba.ServiceReference.phonenumbertype()
                    {
                        phonenumber = phoneNumberType.PhoneNumber,
                        phonenumbercontext = (InstantIdKba.ServiceReference.phonenumbercontexttype)(int)phoneNumberType.PhoneNumberContext
                    });
                }
            }

            List<InstantIdKba.ServiceReference.riskcomponenttype> riskcomponents = new List<InstantIdKba.ServiceReference.riskcomponenttype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.RiskAssessment != null && transactIdentityVerification.Person.RiskAssessment.RiskComponent != null)
            {
                foreach (InstantIdKba.RiskComponentType riskcomponent in transactIdentityVerification.Person.RiskAssessment.RiskComponent)
                {
                    riskcomponents.Add(new InstantIdKba.ServiceReference.riskcomponenttype()
                    {
                        riskcategory = (InstantIdKba.ServiceReference.riskcategory)(int)riskcomponent.RiskCategory,
                        riskscore = riskcomponent.RiskScore
                    });
                }
            }

            List<InstantIdKba.ServiceReference.ukaddresstype> ukAddresses = new List<InstantIdKba.ServiceReference.ukaddresstype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.UkAddress != null)
            {
                foreach (InstantIdKba.UkAddressType ukaddress in transactIdentityVerification.Person.UkAddress)
                {
                    ukAddresses.Add(new InstantIdKba.ServiceReference.ukaddresstype()
                    {
                        addressid = ukaddress.AddressId,
                        context = (InstantIdKba.ServiceReference.addresscontexttype)(int)ukaddress.Context,
                        county = ukaddress.County,
                        district = ukaddress.District,
                        housename = ukaddress.HouseName,
                        housenumber = ukaddress.HouseNumber,
                        postcode = ukaddress.Postcode,
                        street1 = ukaddress.Street1,
                        street2 = ukaddress.Street2,
                        town = ukaddress.Town,
                        yearsat = ukaddress.YearsAt,
                    });
                }
            }


            List<InstantIdKba.ServiceReference.businesstype> businessTypes = new List<InstantIdKba.ServiceReference.businesstype>();
            if (transactIdentityVerification.Person != null && transactIdentityVerification.Person.Employer != null)
            {
                foreach (InstantIdKba.BusinessType businessType in transactIdentityVerification.Person.Employer)
                {
                    List<InstantIdKba.ServiceReference.phonenumbertype> employerPhones = new List<InstantIdKba.ServiceReference.phonenumbertype>();
                    foreach (InstantIdKba.PhoneNumberType phoneNumberType in businessType.Phonenumber)
                    {
                        employerPhones.Add(new InstantIdKba.ServiceReference.phonenumbertype()
                        {
                            phonenumber = phoneNumberType.PhoneNumber,
                            phonenumbercontext = (InstantIdKba.ServiceReference.phonenumbercontexttype)(int)phoneNumberType.PhoneNumberContext
                        });
                    }

                    var b = new InstantIdKba.ServiceReference.businesstype
                    {
                        companyname = businessType.CompanyName,
                        enddate = businessType.EndDate,
                        fein = businessType.Fein,
                        phonenumber = employerPhones.ToArray(),
                        startdate = businessType.StartDate,
                        title = businessType.Title
                    };
                    if (businessType.Address != null)
                    {
                        b.address = new InstantIdKba.ServiceReference.addresstype
                        {
                            addresscity = businessType.Address.AddressCity,
                            addresscontext = (InstantIdKba.ServiceReference.addresscontexttype)(int)businessType.Address.AddressContext,
                            addresscountry = businessType.Address.AddressCountry,
                            addressstate = businessType.Address.AddressState,
                            addressstreet1 = businessType.Address.AddressStreet1,
                            addressstreet2 = businessType.Address.AddressStreet2,
                            addressyearsat = businessType.Address.AddressYearsAt,
                            addresszip = businessType.Address.AddressZip,
                            addresszipplus4 = businessType.Address.AddressZipPlus4,
                            occupationstartdate = businessType.Address.OccupationStartDate,
                            suite = businessType.Address.Suite
                        };
                    }
                    businessTypes.Add(b);
                }
            }

            person = new InstantIdKba.ServiceReference.persontype();

            person.address = addressTypes.ToArray();
            if (transactIdentityVerification.Person.BirthDate != null)
            {
                person.birthdate = new InstantIdKba.ServiceReference.birthdatetype
                {
                    day = transactIdentityVerification.Person.BirthDate.Day,
                    month = transactIdentityVerification.Person.BirthDate.Month,
                    year = transactIdentityVerification.Person.BirthDate.Year
                };
            }
            if (transactIdentityVerification.Person.Business != null)
            {
                person.business = new InstantIdKba.ServiceReference.businesstype
                {
                    companyname = transactIdentityVerification.Person.Business.CompanyName,
                    enddate = transactIdentityVerification.Person.Business.EndDate,
                    fein = transactIdentityVerification.Person.Business.Fein,
                    phonenumber = businessPhones.ToArray(),
                    startdate = transactIdentityVerification.Person.Business.StartDate,
                    title = transactIdentityVerification.Person.Business.Title
                };
                if (transactIdentityVerification.Person.Business.Address != null)
                {
                    person.business.address = new InstantIdKba.ServiceReference.addresstype
                    {
                        addresscity = transactIdentityVerification.Person.Business.Address.AddressCity,
                        addresscontext = (InstantIdKba.ServiceReference.addresscontexttype)(int)transactIdentityVerification.Person.Business.Address.AddressContext,
                        addresscountry = transactIdentityVerification.Person.Business.Address.AddressCountry,
                        addressstate = transactIdentityVerification.Person.Business.Address.AddressState,
                        addressstreet1 = transactIdentityVerification.Person.Business.Address.AddressStreet1,
                        addressstreet2 = transactIdentityVerification.Person.Business.Address.AddressStreet2,
                        addressyearsat = transactIdentityVerification.Person.Business.Address.AddressYearsAt,
                        addresszip = transactIdentityVerification.Person.Business.Address.AddressZip,
                        addresszipplus4 = transactIdentityVerification.Person.Business.Address.AddressZipPlus4,
                        occupationstartdate = transactIdentityVerification.Person.Business.Address.OccupationStartDate,
                        suite = transactIdentityVerification.Person.Business.Address.Suite
                    };
                }
            }
            person.customerid = transactIdentityVerification.Person.CustomerId;
            person.driverslicensenumber = transactIdentityVerification.Person.DriversLicenseNumber;
            person.driverslicensestate = transactIdentityVerification.Person.DriversLicenseState;
            person.email = transactIdentityVerification.Person.Email;
            person.employer = businessTypes.ToArray();
            person.lexID = transactIdentityVerification.Person.LexID;
            person.namefirst = transactIdentityVerification.Person.NameFirst;
            person.namelast = transactIdentityVerification.Person.NameLast;
            person.namemiddle = transactIdentityVerification.Person.NameMiddle;
            person.nameprefix = transactIdentityVerification.Person.NamePrefix;
            person.namesuffix = transactIdentityVerification.Person.NameSuffix;
            person.occupation = transactIdentityVerification.Person.Occupation;
            person.phonenumber = phoneNumberTypes.ToArray();
            if (transactIdentityVerification.Person.RiskAssessment != null)
            {
                person.riskassessment = new InstantIdKba.ServiceReference.riskassessmenttype
                {
                    riskcomponent = riskcomponents.ToArray(),
                    riskscore = transactIdentityVerification.Person.RiskAssessment.RiskScore,
                    threatassessment = (InstantIdKba.ServiceReference.threatassessment)(int)transactIdentityVerification.Person.RiskAssessment.ThreatAssessment
                };
            }
            person.ssn = transactIdentityVerification.Person.Ssn;
            person.ssntype = (InstantIdKba.ServiceReference.ssntype)transactIdentityVerification.Person.SsnType;
            person.ukaddress = ukAddresses.ToArray();
            #endregion Person

            #region Settings
            if (transactIdentityVerification.Settings != null)
            {
                List<InstantIdKba.ServiceReference.internationalizationtype> internationalizations = new List<InstantIdKba.ServiceReference.internationalizationtype>();
                if (transactIdentityVerification.Settings.Internationalization != null)
                {
                    foreach (InstantIdKba.InternationalizationType internationalization in transactIdentityVerification.Settings.Internationalization)
                    {
                        internationalizations.Add(new InstantIdKba.ServiceReference.internationalizationtype()
                        {
                            language = (InstantIdKba.ServiceReference.languagetype)(int)internationalization.Language,
                            languagevenue = (InstantIdKba.ServiceReference.languagevenuetype)(int)internationalization.LanguageVenue,
                        });
                    }
                }

                List<InstantIdKba.ServiceReference.specialfeaturetype> specialfeatures = new List<InstantIdKba.ServiceReference.specialfeaturetype>();
                if (transactIdentityVerification.Settings.SpecialFeature != null)
                {
                    foreach (InstantIdKba.SpecialFeatureType specialfeature in transactIdentityVerification.Settings.SpecialFeature)
                    {
                        specialfeatures.Add(new InstantIdKba.ServiceReference.specialfeaturetype()
                        {
                            specialfeaturecode = specialfeature.SpecialFeatureCode,
                            specialfeaturevalue = specialfeature.SpecialFeatureValue
                        });
                    }
                }

                settings = new InstantIdKba.ServiceReference.identityverificationsettingstype
                {
                    accountname = transactIdentityVerification.Settings.AccountName,
                    agent = transactIdentityVerification.Settings.Agent,
                    attachmenttype = (InstantIdKba.ServiceReference.attachmenttype)(int)transactIdentityVerification.Settings.AttachmentType,
                    attachmenttypeSpecified = transactIdentityVerification.Settings.AttachmentTypeSpecified,
                    internationalization = internationalizations.ToArray(),
                    mode = (InstantIdKba.ServiceReference.modetype)(int)transactIdentityVerification.Settings.Mode,
                    modeSpecified = transactIdentityVerification.Settings.ModeSpecified,
                    parenttransactionid = transactIdentityVerification.Settings.ParentTransactionId,
                    parenttransactionidSpecified = transactIdentityVerification.Settings.ParentTransactionIdSpecified,
                    patriotactcompliance = transactIdentityVerification.Settings.PatriotActCompliance,
                    patriotactcomplianceSpecified = transactIdentityVerification.Settings.PatriotActComplianceSpecified,
                    referenceid = transactIdentityVerification.Settings.ReferenceId,
                    ruleset = transactIdentityVerification.Settings.RuleSet,
                    sequenceid = transactIdentityVerification.Settings.SequenceId,
                    simulatormode = (InstantIdKba.ServiceReference.simulatormodetype)(int)transactIdentityVerification.Settings.SimulatorMode,
                    simulatormodeSpecified = transactIdentityVerification.Settings.SimulatorModeSpecified,
                    specialfeature = specialfeatures.ToArray(),
                    task = (InstantIdKba.ServiceReference.tasktype)(int)transactIdentityVerification.Settings.Task
                };
            }
            #endregion Settings

            #region Transaction
            if (transactIdentityVerification.Transaction != null)
            {
                transaction = new InstantIdKba.ServiceReference.transactiontype { Item = transactIdentityVerification.Transaction.Item };
            }
            #endregion Transaction
        }
    }
}
