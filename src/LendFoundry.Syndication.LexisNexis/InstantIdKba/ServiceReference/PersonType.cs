﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference
{
    public partial class persontype
    {
        public persontype()
        { }
        public persontype(IInstantIdIdentityRequest personType)
        {
            if (personType == null)
                return;
            lexID = personType.LexID;
            nameprefix = personType.NamePrefix;
            namefirst = personType.NameFirst;
            namemiddle = personType.NameMiddle;
            namelast = personType.NameLast;
            namesuffix = personType.NameSuffix;
            customerid = personType.CustomerId;
            email = personType.Email;
            ssn = personType.Ssn;
            ssntype = (ssntype)(int)personType.SsnType;
            driverslicensenumber = personType.DriversLicenseNumber;
            driverslicensestate = personType.DriversLicenseState;
            if (personType.BirthDate != null)
            {
                birthdate = new birthdatetype
                {
                    year = personType.BirthDate.Year,
                    month = personType.BirthDate.Month,
                    day = personType.BirthDate.Day
                };
            }
            if (personType.Address != null)
            {
                List<addresstype> addresses = new List<addresstype>();
                foreach (AddressType address in personType.Address)
                {
                    addresses.Add(new addresstype()
                    {
                        addressstreet1 = address.AddressStreet1,
                        addressstreet2 = address.AddressStreet2,
                        suite = address.Suite,
                        addresscity = address.AddressCity,
                        addressstate = address.AddressState,
                        addresszip = address.AddressZip,
                        addresszipplus4 = address.AddressZipPlus4,
                        addresscountry = address.AddressCountry,
                        addresscontext = (addresscontexttype)(int)address.AddressContext,
                        addressyearsat = address.AddressYearsAt,
                        addressyearsatSpecified = address.AddressYearsAtSpecified,
                        occupationstartdate = address.OccupationStartDate,
                        occupationstartdateSpecified = address.OccupationStartDateSpecified
                    });
                }
                address = addresses.ToArray();
            }
            if (personType.Business != null)
            {
                business = new businesstype
                {
                    companyname = personType.Business.CompanyName,
                    enddate = personType.Business.EndDate,
                    enddateSpecified = personType.Business.EndDateSpecified,
                    fein = personType.Business.Fein,
                    startdate = personType.Business.StartDate,
                    startdateSpecified = personType.Business.StartDateSpecified,
                    title = personType.Business.Title
                };

                if (personType.Business.Address != null)
                {
                    business.address = new addresstype()
                    {
                        addresscity = personType.Business.Address.AddressCity,
                        addresscontext = (InstantIdKba.ServiceReference.addresscontexttype)(int)personType.Business.Address.AddressContext,
                        addresscountry = personType.Business.Address.AddressCountry,
                        addressstate = personType.Business.Address.AddressState,
                        addressstreet1 = personType.Business.Address.AddressStreet1,
                        addressstreet2 = personType.Business.Address.AddressStreet2,
                        addressyearsat = personType.Business.Address.AddressYearsAt,
                        addresszip = personType.Business.Address.AddressZip,
                        addresszipplus4 = personType.Business.Address.AddressZipPlus4,
                        occupationstartdate = personType.Business.Address.OccupationStartDate,
                        suite = personType.Business.Address.Suite
                    };
                }

                if (personType.Business.Phonenumber != null)
                {
                    List<InstantIdKba.ServiceReference.phonenumbertype> phoneNumberTypes = new List<InstantIdKba.ServiceReference.phonenumbertype>();
                    foreach (InstantIdKba.PhoneNumberType phoneNumberType in personType.Business.Phonenumber)
                    {
                        phoneNumberTypes.Add(new InstantIdKba.ServiceReference.phonenumbertype()
                        {
                            phonenumber = phoneNumberType.PhoneNumber,
                            phonenumbercontext = (InstantIdKba.ServiceReference.phonenumbercontexttype)(int)phoneNumberType.PhoneNumberContext
                        });
                    }
                    business.phonenumber = phoneNumberTypes.ToArray();
                }
            }
            occupation = personType.Occupation;
            if (personType.RiskAssessment != null)
            {
                riskassessment = new riskassessmenttype
                {
                    riskscore = personType.RiskAssessment.RiskScore,
                    threatassessment = (threatassessment)(int)personType.RiskAssessment.ThreatAssessment
                };
                if (personType.RiskAssessment.RiskComponent != null)
                {
                    List<InstantIdKba.ServiceReference.riskcomponenttype> riskComponents = new List<InstantIdKba.ServiceReference.riskcomponenttype>();
                    foreach (InstantIdKba.RiskComponentType riskComponent in personType.RiskAssessment.RiskComponent)
                    {
                        riskComponents.Add(new InstantIdKba.ServiceReference.riskcomponenttype()
                        {
                            riskcategory = (riskcategory)(int)riskComponent.RiskCategory,
                            riskscore = riskComponent.RiskScore
                        });
                    }
                    riskassessment.riskcomponent = riskComponents.ToArray();
                }
            }
            if (personType.UkAddress != null)
            {
                List<ukaddresstype> ukAddresses = new List<ukaddresstype>();
                foreach (UkAddressType address in personType.UkAddress)
                {
                    ukAddresses.Add(new ukaddresstype()
                    {
                        addressid = address.AddressId,
                        context = (addresscontexttype)(int)address.Context,
                        county = address.County,
                        district = address.District,
                        housename = address.HouseName,
                        housenumber = address.HouseNumber,
                        postcode = address.Postcode,
                        street1 = address.Street1,
                        street2 = address.Street2,
                        town = address.Town,
                        yearsat = address.YearsAt,
                        yearsatSpecified = address.YearsAtSpecified
                    });
                }
                ukaddress = ukAddresses.ToArray();
            }
            if (personType.PhoneNumber != null)
            {
                List<phonenumbertype> phoneNumbers = new List<phonenumbertype>();
                foreach (PhoneNumberType phoneNumber in personType.PhoneNumber)
                {
                    phoneNumbers.Add(new phonenumbertype()
                    {
                        phonenumber = phoneNumber.PhoneNumber,
                        phonenumbercontext = (phonenumbercontexttype)(int)phoneNumber.PhoneNumberContext
                    });
                }
                phonenumber = phoneNumbers.ToArray();
            }
            if (personType.Employer != null)
            {
                List<businesstype> employers = new List<businesstype>();
                foreach (BusinessType employer in personType.Employer)
                {
                    var e = new businesstype()
                    {
                        companyname = employer.CompanyName,
                        enddate = employer.EndDate,
                        enddateSpecified = employer.EndDateSpecified,
                        fein = employer.Fein,
                        startdate = employer.StartDate,
                        startdateSpecified = employer.StartDateSpecified,
                        title = employer.Title
                    };
                    if (employer.Address != null)
                    {
                        e.address = new addresstype()
                        {
                            addresscity = personType.Business.Address.AddressCity,
                            addresscontext = (InstantIdKba.ServiceReference.addresscontexttype)(int)personType.Business.Address.AddressContext,
                            addresscountry = personType.Business.Address.AddressCountry,
                            addressstate = personType.Business.Address.AddressState,
                            addressstreet1 = personType.Business.Address.AddressStreet1,
                            addressstreet2 = personType.Business.Address.AddressStreet2,
                            addressyearsat = personType.Business.Address.AddressYearsAt,
                            addresszip = personType.Business.Address.AddressZip,
                            addresszipplus4 = personType.Business.Address.AddressZipPlus4,
                            occupationstartdate = personType.Business.Address.OccupationStartDate,
                            suite = personType.Business.Address.Suite
                        };
                    }

                    if (employer.Phonenumber != null)
                    {
                        List<InstantIdKba.ServiceReference.phonenumbertype> phoneNumberTypes = new List<InstantIdKba.ServiceReference.phonenumbertype>();
                        foreach (InstantIdKba.PhoneNumberType phoneNumberType in employer.Phonenumber)
                        {
                            phoneNumberTypes.Add(new InstantIdKba.ServiceReference.phonenumbertype()
                            {
                                phonenumber = phoneNumberType.PhoneNumber,
                                phonenumbercontext = (InstantIdKba.ServiceReference.phonenumbercontexttype)(int)phoneNumberType.PhoneNumberContext
                            });
                        }
                        e.phonenumber = phoneNumberTypes.ToArray();
                    }
                    employers.Add(e);
                }
                employer = employers.ToArray();
            }
        }
    }
}
