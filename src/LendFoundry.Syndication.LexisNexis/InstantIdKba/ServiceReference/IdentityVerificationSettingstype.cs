﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference
{
    public partial class identityverificationsettingstype
    {
        public identityverificationsettingstype()
        { }
        public identityverificationsettingstype(IIdentityVerificationSettingsType settings)
        {
            accountname = settings.AccountName;
            agent = settings.Agent;
            attachmenttype = (InstantIdKba.ServiceReference.attachmenttype)(int)settings.AttachmentType;
            attachmenttypeSpecified = settings.AttachmentTypeSpecified;
            List<InstantIdKba.ServiceReference.internationalizationtype> internationalizations = new List<InstantIdKba.ServiceReference.internationalizationtype>();
            if (settings.Internationalization != null)
            {
                foreach (InstantIdKba.InternationalizationType internationalization in settings.Internationalization)
                {
                    internationalizations.Add(new InstantIdKba.ServiceReference.internationalizationtype()
                    {
                        language = (InstantIdKba.ServiceReference.languagetype)(int)internationalization.Language,
                        languagevenue = (InstantIdKba.ServiceReference.languagevenuetype)(int)internationalization.LanguageVenue,
                    });
                }
            }           
            internationalization = internationalizations.ToArray();
            mode = (InstantIdKba.ServiceReference.modetype)(int)settings.Mode;
            modeSpecified = settings.ModeSpecified;
            parenttransactionid = settings.ParentTransactionId;
            parenttransactionidSpecified = settings.ParentTransactionIdSpecified;
            patriotactcompliance = settings.PatriotActCompliance;
            patriotactcomplianceSpecified = settings.PatriotActComplianceSpecified;
            referenceid = settings.ReferenceId;
            ruleset = settings.RuleSet;
            sequenceid = settings.SequenceId;
            simulatormode = (InstantIdKba.ServiceReference.simulatormodetype)(int)settings.SimulatorMode;
            simulatormodeSpecified = settings.SimulatorModeSpecified;
            List<InstantIdKba.ServiceReference.specialfeaturetype> specialfeatures = new List<InstantIdKba.ServiceReference.specialfeaturetype>();
            if (settings.SpecialFeature != null)
            {
                foreach (InstantIdKba.SpecialFeatureType specialfeature in settings.SpecialFeature)
                {
                    specialfeatures.Add(new InstantIdKba.ServiceReference.specialfeaturetype()
                    {
                        specialfeaturecode = specialfeature.SpecialFeatureCode,
                        specialfeaturevalue = specialfeature.SpecialFeatureValue
                    });
                }
            }
            specialfeature = specialfeatures.ToArray();
            task = (InstantIdKba.ServiceReference.tasktype)(int)settings.Task;
        }
    }
}