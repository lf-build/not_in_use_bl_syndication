﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba.ServiceReference
{
    public class TransactionContinueRequest
    {
        public TransactionContinueRequest()
        { }
        public TransactionContinueRequest(IInstantIdContinuationRequest settings)
        {
            if (settings == null)
                return;

            QuestionSetId = settings.QuestionSetId;
            TransactionId = settings.TransactionId;
            if (settings.Answer != null)
            {
                List<answertype> answers = new List<answertype>();
                foreach (AnswerType answer in settings.Answer)
                {
                    answers.Add(new answertype()
                    {
                        choices = answer.Choices,
                        questionid = answer.QuestionId
                    });
                }
                Answer = answers.ToArray();
            }
        }
        public long TransactionId { get; set; }
        public long QuestionSetId { get; set; }
        public answertype[] Answer { get; set; }
    }
}
