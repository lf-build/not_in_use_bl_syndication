﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IWsException
    {
        string Source { get; set; }

        string Code { get; set; }

        string Location { get; set; }

        string Message { get; set; }
    }
}
