﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IRedFlagsReport
    {
        string Version { get; set; }

        IRedFlag[] RedFlags { get; set; }
    }
}
