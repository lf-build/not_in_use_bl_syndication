﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointTimeStamp : IFraudPointTimeStamp
    {
        public FraudPointTimeStamp(ServiceReference.FraudPointTimeStamp fraudPointTimeStamp)
        {
            if (fraudPointTimeStamp == null)
                return;
            Year = fraudPointTimeStamp.Year;
            Month = fraudPointTimeStamp.Month;
            Day = fraudPointTimeStamp.Day;
            Hour24 = fraudPointTimeStamp.Hour24;
            Minute = fraudPointTimeStamp.Minute;
            Second = fraudPointTimeStamp.Second;
        }
        public short Year { get; set; }

        public short Month { get; set; }

        public short Day { get; set; }

        public short Hour24 { get; set; }

        public short Minute { get; set; }


        public short Second { get; set; }

    }
}
