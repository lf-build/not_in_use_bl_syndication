﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInstantIdContinuationRequest
    {
        long TransactionId { get; set; }
        long QuestionSetId { get; set; }
        IAnswerType[] Answer { get; set; }
    }
}
