﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class RiskComponentType : IRiskComponentType
    {
        public RiskComponentType(ServiceReference.riskcomponenttype riskComponentType)
        {
            if (riskComponentType == null)
                return;
            RiskCategory = (RiskCategory)(int) riskComponentType.riskcategory;
            RiskScore = riskComponentType.riskscore;
        }
        public RiskCategory RiskCategory { get; set; }

        public double RiskScore { get; set; }
    }
}
