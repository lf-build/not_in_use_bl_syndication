﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class VenueType : IVenueType
    {
        public VenueType(ServiceReference.venuetype venueType)
        {
            if (venueType == null)
                return;
            Item = venueType.Item;
            ItemElementName = (ItemChoiceType)(int)venueType.ItemElementName;
        }
        public object Item { get; set; }

        public ItemChoiceType ItemElementName { get; set; }
    }
}
