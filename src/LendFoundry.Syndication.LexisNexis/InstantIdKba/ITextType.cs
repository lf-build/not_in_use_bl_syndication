﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITextType
    {
        LanguageType Language { get; set; }

        string Statement { get; set; }
    }
}
