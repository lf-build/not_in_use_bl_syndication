﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionResponse : ITransactionResponse
    {
        public TransactionResponse(ServiceReference.transactionresponse transactionResponse)
        {
            if (transactionResponse == null)
                return;
            TransactionStatus = new TransactionStatusType(transactionResponse.transactionstatus);
            Questions = new QuestionsType(transactionResponse.questions);
            List<IInformationType> informations = new List<IInformationType>();
            if (transactionResponse.information != null)
            {
                foreach (ServiceReference.informationtype information in transactionResponse.information)
                {
                    informations.Add(new InformationType(information));
                }
                Information = informations.ToArray();
            }
            ExternalResponses = new ExternalResponsesType(transactionResponse.externalresponses);
        }
        public ITransactionStatusType TransactionStatus { get; set; }

        public IQuestionsType Questions { get; set; }

        public IInformationType[] Information { get; set; }

        public IExternalResponsesType ExternalResponses { get; set; }
    }
}
