﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointInputEcho : IFraudPointInputEcho
    {
        public FraudPointInputEcho(ServiceReference.FraudPointInputEcho fraudPointEcho)
        {
            if (fraudPointEcho == null)
                return;
            Name = new FraudPointName(fraudPointEcho.Name);
            Address = new FraudPointAddress(fraudPointEcho.Address);
            DOB = new FraudPointDate(fraudPointEcho.DOB);
            SSN = fraudPointEcho.SSN;
            Phone10 = fraudPointEcho.Phone10;
            WorkPhone = fraudPointEcho.WorkPhone;
            DriverLicenseNumber = fraudPointEcho.DriverLicenseNumber;
            DriverLicenseState = fraudPointEcho.DriverLicenseState;
            IPAddress = fraudPointEcho.IPAddress;
            Email = fraudPointEcho.Email;
            Channel = (FraudPointChannelIdentifier)(int)fraudPointEcho.Channel;
            Income = fraudPointEcho.Income;
            OwnOrRent = (FraudPointOwnRent)(int)fraudPointEcho.OwnOrRent;
            LocationIdentifier = fraudPointEcho.LocationIdentifier;
            OtherApplicationIdentifier1 = fraudPointEcho.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = fraudPointEcho.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = fraudPointEcho.OtherApplicationIdentifier3;
            ApplicationDateTime = new FraudPointTimeStamp(fraudPointEcho.ApplicationDateTime);
            Grade = fraudPointEcho.Grade;
        }
        public IFraudPointName Name { get; set; }

        public IFraudPointAddress Address { get; set; }

        public IFraudPointDate DOB { get; set; }

        public string SSN { get; set; }

        public string Phone10 { get; set; }

        public string WorkPhone { get; set; }

        public string DriverLicenseNumber { get; set; }

        public string DriverLicenseState { get; set; }

        public string IPAddress { get; set; }

        public string Email { get; set; }

        public FraudPointChannelIdentifier Channel { get; set; }

        public string Income { get; set; }

        public FraudPointOwnRent OwnOrRent { get; set; }

        public string LocationIdentifier { get; set; }

        public string OtherApplicationIdentifier1 { get; set; }

        public string OtherApplicationIdentifier2 { get; set; }

        public string OtherApplicationIdentifier3 { get; set; }

        public IFraudPointTimeStamp ApplicationDateTime { get; set; }

        public string Grade { get; set; }
    }
}
