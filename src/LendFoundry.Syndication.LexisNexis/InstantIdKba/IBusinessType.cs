﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IBusinessType
    {
        string CompanyName { get; set; }

        string Fein { get; set; }

        IAddressType Address { get; set; }

        IPhoneNumberType[] Phonenumber { get; set; }

        string Title { get; set; }

        System.DateTime StartDate { get; set; }

        bool StartDateSpecified { get; set; }

        System.DateTime EndDate { get; set; }

        bool EndDateSpecified { get; set; }

    }
}
