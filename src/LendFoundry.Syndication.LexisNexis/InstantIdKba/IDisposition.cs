﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IDisposition
    {
        IDispositionSettingsType Settings { get; set; }

        IAccountType Account { get; set; }

        FraudCategoryType FraudCategory { get; set; }

        DispositionStatusType DispositionStatus { get; set; }

        System.DateTime DispositionDate { get; set; }

    }
}
