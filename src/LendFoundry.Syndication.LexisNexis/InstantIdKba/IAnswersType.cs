﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAnswersType
    {
        long QuestionSetId { get; set; }

        IAnswerType[] Answer { get; set; }
    }
}
