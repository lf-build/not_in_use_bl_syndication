﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IQuestionType
    {
        long QuestionId { get; set; }

        AnswerSelectionType AnswerType { get; set; }

        ITextType[] Text { get; set; }

        IChoiceType[] Choice { get; set; }

        ITextType[] HelpText { get; set; }
    }
}
