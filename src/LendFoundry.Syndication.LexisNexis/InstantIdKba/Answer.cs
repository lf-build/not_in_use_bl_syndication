﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class Answer
    {
        public long QuestionId { get; set; }
        public List<long> ChoiceIds { get; set; }
    }
}
