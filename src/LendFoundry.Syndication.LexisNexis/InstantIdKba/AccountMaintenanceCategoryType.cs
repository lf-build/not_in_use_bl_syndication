﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum AccountMaintenanceCategoryType
    {
        AddressChange,
        PhoneNumberChange,
        CloseAccount,
        BeneficiaryChange,
        AdditionalAccount,
        NameChange,
        PasswordReset,
        PasswordChange,
        AddUser,
        RemoveUser,
        ChangeBillingInformation,
        AddPayee,
        ChangePayee,
        LinkAccount,
        PinChange,
        EmailChange,
        ViewEvent,
        AddJointAccountHolder,
        ReorderChecks,
        OrderCard,
        StopPayment,
        AddAlerts
    }
}
