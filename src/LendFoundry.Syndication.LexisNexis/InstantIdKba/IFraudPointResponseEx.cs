﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointResponseEx
    {
        IFraudPointResponse Response { get; set; }
    }
}
