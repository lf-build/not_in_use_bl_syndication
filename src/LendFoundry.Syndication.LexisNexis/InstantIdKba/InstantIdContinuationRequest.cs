﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIdContinuationRequest : IInstantIdContinuationRequest
    {
        public long TransactionId { get; set; }
        public long QuestionSetId { get; set; }
        public IAnswerType[] Answer { get; set; }
    }
}
