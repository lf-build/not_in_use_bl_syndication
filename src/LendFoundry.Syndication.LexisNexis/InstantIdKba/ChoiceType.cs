﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ChoiceType : IChoiceType
    {
        public ChoiceType(ServiceReference.choicetype choiceType)
        {
            if (choiceType == null)
                return;
            ChoiceId = choiceType.choiceid;
            List<ITextType> texts = new List<ITextType>();
            if (choiceType.text != null)
            {
                foreach (ServiceReference.texttype text in choiceType.text)
                {
                    texts.Add(new TextType(text));
                }
                Text = texts.ToArray();
            }
        }
        public long ChoiceId { get; set; }

        public ITextType[] Text { get; set; }
    }
}
