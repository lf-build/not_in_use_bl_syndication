﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class IvrType : IIvrType
    {
        public IvrType(ServiceReference.ivrtype ivrType)
        {
            if (ivrType == null)
                return;
            List<CredentialMethodType> credentials = new List<CredentialMethodType>();
            if (ivrType.credentialmethod != null)
            {
                foreach (var credential in ivrType.credentialmethod)
                {
                    credentials.Add((CredentialMethodType)(int)credential);
                }
                CredentialMethod = credentials.ToArray();
            }
        }
        public CredentialMethodType[] CredentialMethod { get; set; }
    }
}
