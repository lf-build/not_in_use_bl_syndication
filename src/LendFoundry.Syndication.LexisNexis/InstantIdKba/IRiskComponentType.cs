﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IRiskComponentType
    {
        RiskCategory RiskCategory { get; set; }

        double RiskScore { get; set; }
    }
}
