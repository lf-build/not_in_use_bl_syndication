﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class PersonType : IPersonType
    {
        public PersonType(ServiceReference.persontype personType)
        {
            if (personType == null)
                return;
            LexID = personType.lexID;
            NamePrefix = personType.nameprefix;
            NameFirst = personType.namefirst;
            NameMiddle = personType.namemiddle;
            NameLast = personType.namelast;
            NameSuffix = personType.namesuffix;
            CustomerId = personType.customerid;
            Email = personType.email;
            Ssn = personType.ssn;
            SsnType = (SsnType)(int)personType.ssntype;
            DriversLicenseNumber = personType.driverslicensenumber;
            DriversLicenseState = personType.driverslicensestate;
            BirthDate = new BirthDateType(personType.birthdate);
            List<IAddressType> addresses = new List<IAddressType>();
            if (personType.address != null)
            {
                foreach (ServiceReference.addresstype address in personType.address)
                {
                    addresses.Add(new AddressType(address));
                }
                Address = addresses.ToArray();
            }

            List<IUkAddressType> ukaddresses = new List<IUkAddressType>();
            if (personType.ukaddress != null)
            {
                foreach (ServiceReference.ukaddresstype ukaddress in personType.ukaddress)
                {
                    ukaddresses.Add(new UkAddressType(ukaddress));
                }
                UkAddress = ukaddresses.ToArray();
            }

            List<IPhoneNumberType> phonenumbers = new List<IPhoneNumberType>();
            if (personType.phonenumber != null)
            {
                foreach (ServiceReference.phonenumbertype phonenumber in personType.phonenumber)
                {
                    phonenumbers.Add(new PhoneNumberType(phonenumber));
                }
                PhoneNumber = phonenumbers.ToArray();
            }

            Business = new BusinessType(personType.business);

            List<IBusinessType> employers = new List<IBusinessType>();
            if (personType.employer != null)
            {
                foreach (ServiceReference.businesstype employer in personType.employer)
                {
                    employers.Add(new BusinessType(employer));
                }
                Employer = employers.ToArray();
            }

            Occupation = personType.occupation;
            RiskAssessment = new RiskAssessmentType(personType.riskassessment);
        }
        public string LexID { get; set; }

        public string NamePrefix { get; set; }

        public string NameFirst { get; set; }

        public string NameMiddle { get; set; }

        public string NameLast { get; set; }

        public string NameSuffix { get; set; }

        public string[] CustomerId { get; set; }

        public string[] Email { get; set; }

        public string Ssn { get; set; }

        public SsnType SsnType { get; set; }

        public string DriversLicenseNumber { get; set; }

        public string DriversLicenseState { get; set; }

        public IBirthDateType BirthDate { get; set; }

        public IAddressType[] Address { get; set; }

        public IUkAddressType[] UkAddress { get; set; }

        public IPhoneNumberType[] PhoneNumber { get; set; }

        public IBusinessType Business { get; set; }

        public IBusinessType[] Employer { get; set; }

        public string Occupation { get; set; }

        public IRiskAssessmentType RiskAssessment { get; set; }
    }
}
