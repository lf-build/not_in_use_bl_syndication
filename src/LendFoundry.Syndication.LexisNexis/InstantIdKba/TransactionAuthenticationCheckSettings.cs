﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionAuthenticationCheckSettings : ITransactionAuthenticationCheckSettings
    {
        public TransactionAuthenticationCheckSettings(ServiceReference.transactionauthenticationcheckSettings transactionAuthenticationCheckSettings)
        {
            if (transactionAuthenticationCheckSettings == null)
                return;
            AccountName = transactionAuthenticationCheckSettings.accountname;
            Mode = (ModeType)(int)transactionAuthenticationCheckSettings.mode;
            RuleSet = transactionAuthenticationCheckSettings.ruleset;
            SimulatorMode = (SimulatorModeType)(int)transactionAuthenticationCheckSettings.simulatormode;
            AttachmentType = (AttachmentType)(int)transactionAuthenticationCheckSettings.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (transactionAuthenticationCheckSettings.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in transactionAuthenticationCheckSettings.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (transactionAuthenticationCheckSettings.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in transactionAuthenticationCheckSettings.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = transactionAuthenticationCheckSettings.referenceid;
            ModeSpecified = transactionAuthenticationCheckSettings.modeSpecified;
            AttachmentTypeSpecified = transactionAuthenticationCheckSettings.attachmenttypeSpecified;
            SimulatorModeSpecified = transactionAuthenticationCheckSettings.simulatormodeSpecified;
        }
        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
