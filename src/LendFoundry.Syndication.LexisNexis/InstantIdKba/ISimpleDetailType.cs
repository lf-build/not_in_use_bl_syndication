﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISimpleDetailType
    {
        string Text { get; set; }
    }
}
