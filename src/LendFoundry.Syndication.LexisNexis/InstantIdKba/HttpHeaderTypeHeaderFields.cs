﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class HttpHeaderTypeHeaderFields : IHttpHeaderTypeHeaderFields
    {
        public HttpHeaderTypeHeaderFields(ServiceReference.httpheadertypeHeaderfields httpHeaderTypeHeaderFields)
        {
            if (httpHeaderTypeHeaderFields == null)
                return;
            HeaderField = httpHeaderTypeHeaderFields.headerfield;
            HeaderValue = httpHeaderTypeHeaderFields.headervalue;
        }
        public string HeaderField { get; set; }

        public string HeaderValue { get; set; }
    }
}
