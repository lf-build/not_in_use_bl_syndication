﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ISecurityQuestionType
    {
        SecurityQuestionCategoryType SecurityQuestionQuestionCategory { get; set; }

        string SecurityQuestionQuesitonStatement { get; set; }

        string SecurityQuestionAnswer { get; set; }
    }
}
