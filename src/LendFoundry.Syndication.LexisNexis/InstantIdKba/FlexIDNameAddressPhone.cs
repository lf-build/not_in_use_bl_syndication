﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDNameAddressPhone : IFlexIDNameAddressPhone
    {
        public FlexIDNameAddressPhone(ServiceReference.FlexIDNameAddressPhone flexIdNameAddressPhone)
        {
            if (flexIdNameAddressPhone == null)
                return;
            Summary = flexIdNameAddressPhone.Summary;
            Type = flexIdNameAddressPhone.Type;
            Status = flexIdNameAddressPhone.Status;
        }
        public string Summary { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }
    }
}
