﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class PurchaseType : IPurchaseType
    {
        public PurchaseType(ServiceReference.purchasetype purchaseType)
        {
            if (purchaseType == null)
                return;
            Order = new PurchaseTypeOrder(purchaseType.order);
            PaymentMethod = (PaymentMethod)(int)purchaseType.paymentmethod;
            Shipping = new ShippingType(purchaseType.shipping);
            Account = new AccountType(purchaseType.account);
            Venue = new VenueType(purchaseType.venue);
            TransactionDate = purchaseType.transactiondate;
            RiskAssessment = new RiskAssessmentType(purchaseType.riskassessment);
        }
        public IPurchaseTypeOrder Order { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public IShippingType Shipping { get; set; }

        public IAccountType Account { get; set; }

        public IVenueType Venue { get; set; }

        public System.DateTime TransactionDate { get; set; }

        public IRiskAssessmentType RiskAssessment { get; set; }
    }
}
