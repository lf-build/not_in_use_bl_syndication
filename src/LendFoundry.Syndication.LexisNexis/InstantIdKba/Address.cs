﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class Address : IAddress
    {
        public Address(ServiceReference.Address address)
        {
            if (address == null)
                return;
            StreetNumber = address.StreetNumber;
            StreetPreDirection = address.StreetPreDirection;
            StreetName = address.StreetName;
            StreetSuffix = address.StreetSuffix;
            StreetPostDirection = address.StreetPostDirection;
            UnitDesignation = address.UnitDesignation;
            UnitNumber = address.UnitNumber;
            StreetAddress1 = address.StreetAddress1;
            StreetAddress2 = address.StreetAddress2;
            City = address.City;
            State = address.State;
            Zip4 = address.Zip4;
            Zip5 = address.Zip5;
            County = address.County;
            PostalCode = address.PostalCode;
            StateCityZip = address.StateCityZip;
        }
        public string StreetNumber { get; set; }

        public string StreetPreDirection { get; set; }

        public string StreetName { get; set; }

        public string StreetSuffix { get; set; }

        public string StreetPostDirection { get; set; }

        public string UnitDesignation { get; set; }

        public string UnitNumber { get; set; }

        public string StreetAddress1 { get; set; }

        public string StreetAddress2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip5 { get; set; }

        public string Zip4 { get; set; }

        public string County { get; set; }

        public string PostalCode { get; set; }

        public string StateCityZip { get; set; }
    }
}
