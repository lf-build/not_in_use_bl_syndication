﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ContinueSettingsType : IContinueSettingsType
    {
        public ContinueSettingsType() { }
        public ContinueSettingsType(ServiceReference.continuesettingstype continueSettingsType)
        {
            if (continueSettingsType == null)
                return;
            TransactionId = continueSettingsType.transactionid;
            AccountName = continueSettingsType.accountname;
            Mode = (ModeType)(int)continueSettingsType.mode;
            RuleSet = continueSettingsType.ruleset;
            SimulatorMode = (SimulatorModeType)(int) continueSettingsType.simulatormode;
            AttachmentType = (AttachmentType)(int)continueSettingsType.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (continueSettingsType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in continueSettingsType.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (continueSettingsType.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in continueSettingsType.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = continueSettingsType.referenceid;
            AttachmentTypeSpecified = continueSettingsType.attachmenttypeSpecified;
            SimulatorModeSpecified = continueSettingsType.simulatormodeSpecified;
            ModeSpecified = continueSettingsType.modeSpecified;
        }

        public long TransactionId { get; set; }

        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
