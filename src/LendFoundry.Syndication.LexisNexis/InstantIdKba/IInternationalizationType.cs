﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IInternationalizationType
    {
        LanguageType Language { get; set; }

        LanguageVenueType LanguageVenue { get; set; }

    }
}
