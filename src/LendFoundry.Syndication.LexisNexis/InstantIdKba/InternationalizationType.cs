﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InternationalizationType : IInternationalizationType
    {
        public InternationalizationType(ServiceReference.internationalizationtype internationalizationType)
        {
            if (internationalizationType == null)
                return;
            Language = (LanguageType)(int)internationalizationType.language;
            LanguageVenue = (LanguageVenueType)(int) internationalizationType.languagevenue;
        }
        public LanguageType Language { get; set; }

        public LanguageVenueType LanguageVenue { get; set; }

    }
}
