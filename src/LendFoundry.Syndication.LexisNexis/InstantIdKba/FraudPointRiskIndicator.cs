﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FraudPointRiskIndicator : IFraudPointRiskIndicator
    {
        public FraudPointRiskIndicator(ServiceReference.FraudPointRiskIndicator fraudPointRiskindicator)
        {
            if (fraudPointRiskindicator == null)
                return;
            RiskCode = fraudPointRiskindicator.RiskCode;
            Description = fraudPointRiskindicator.Description;
        }
        public string RiskCode { get; set; }

        public string Description { get; set; }
    }
}
