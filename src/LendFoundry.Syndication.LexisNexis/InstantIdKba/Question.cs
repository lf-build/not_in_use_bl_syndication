﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis
{
    public class Question
    {
        public long QuestionId { get; set; }

        public string QuestionText { get; set; }

        public IDictionary<long,string> Choices{get;set;}
    }
}
