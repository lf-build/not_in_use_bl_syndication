﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum AnswerSelectionType
    {
        Single,
        Multiple,
    }
}
