﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDNameValuePair
    {
        string Name { get; set; }

        string Value { get; set; }
    }
}
