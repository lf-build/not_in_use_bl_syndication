﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum ThreatAssessment
    {
        High,
        Medium,
        Low,
        Indeterminate,
    }
}
