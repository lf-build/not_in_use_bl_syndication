﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IDispositionSettingsType : ISettingsType
    {
        long TransactionId { get; set; }
    }
}
