﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum TransactionDirectionType
    {
        Buy,
        Sell,
    }
}
