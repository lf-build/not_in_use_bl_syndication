﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDScoreSequencedHRI
    {
        string Type { get; set; }

        int Value { get; set; }

        IFlexIDNameValuePair[] RiskIndices { get; set; }

        IFlexIDSequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
