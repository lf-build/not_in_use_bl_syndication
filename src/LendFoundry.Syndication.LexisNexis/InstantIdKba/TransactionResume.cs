﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionResume : ITransactionResume
    {
        public TransactionResume(ServiceReference.transactionresume transactionResume)
        {
            if (transactionResume == null)
                return;
            Settings = new ResumeSettingsType(transactionResume.settings);
            Account = new AccountType(transactionResume.account);
        }
        public IResumeSettingsType Settings { get; set; }

        public IAccountType Account { get; set; }
    }
}
