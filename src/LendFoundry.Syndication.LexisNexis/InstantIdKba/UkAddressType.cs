﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class UkAddressType : IUkAddressType
    {
        public UkAddressType(ServiceReference.ukaddresstype ukAddressType)
        {
            if (ukAddressType == null)
                return;
            AddressId = ukAddressType.addressid;
            HouseName = ukAddressType.housename;
            HouseNumber = ukAddressType.housenumber;
            Street1 = ukAddressType.street1;
            Street2 = ukAddressType.street2;
            District = ukAddressType.district;
            Town = ukAddressType.town;
            County = ukAddressType.county;
            Postcode = ukAddressType.postcode;
            Context = (AddressContextType)(int)ukAddressType.context;
            YearsAt = ukAddressType.yearsat;
            YearsAtSpecified = ukAddressType.yearsatSpecified;
        }
        public string AddressId { get; set; }

        public string HouseName { get; set; }

        public string HouseNumber { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string District { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string Postcode { get; set; }

        public AddressContextType Context { get; set; }

        public decimal YearsAt { get; set; }

        public bool YearsAtSpecified { get; set; }

    }
}
