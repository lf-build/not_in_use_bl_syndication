﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IHttpHeaderType
    {
        object[] Items { get; set; }
    }
}
