﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class RiskAssessmentType:IRiskAssessmentType
    {
        public RiskAssessmentType(ServiceReference.riskassessmenttype riskAssesmentType)
        {
            if (riskAssesmentType == null)
                return;
            RiskScore = riskAssesmentType.riskscore;
            ThreatAssessment = (ThreatAssessment)(int)riskAssesmentType.threatassessment;
            List<IRiskComponentType> riskcomponents = new List<IRiskComponentType>();
            if (riskAssesmentType.riskcomponent != null)
            {
                foreach (ServiceReference.riskcomponenttype riskcomponent in riskAssesmentType.riskcomponent)
                {
                    riskcomponents.Add(new RiskComponentType(riskcomponent));
                }
                RiskComponent = riskcomponents.ToArray();
            }
        }
        public double RiskScore { get; set; }

        public ThreatAssessment ThreatAssessment { get; set; }

        public IRiskComponentType[] RiskComponent { get; set; }
    }
}
