﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ShippingType : IShippingType
    {
        public ShippingType(ServiceReference.shippingtype shippingType)
        {
            if (shippingType == null)
                return;
            BillTo = new BillToType(shippingType.billto);
            ShipTo = new ShipToType(shippingType.shipto);
            ShippingMethod = (ShippingMethod)(int)shippingType.shippingmethod;
        }
        public IBillToType BillTo { get; set; }

        public IShipToType ShipTo { get; set; }

        public ShippingMethod ShippingMethod { get; set; }

    }
}
