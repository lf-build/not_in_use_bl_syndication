﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ICredentialType
    {
        CredentialMethodType[] CredentialMethod { get; set; }

        string Username { get; set; }

        string Domain { get; set; }

        string OrganizationalUnit { get; set; }

        IHttpHeaderType HttpHeader { get; set; }

        string IpAddress { get; set; }

        string[] MachineId { get; set; }
    }
}
