﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IModelSequenced
    {
        string Name { get; set; }

        IScoreSequenced[] Scores { get; set; }
    }
}
