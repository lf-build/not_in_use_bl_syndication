﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SimpleDetailType : ISimpleDetailType
    {
        public SimpleDetailType(ServiceReference.simpledetailtype simpleDetailType)
        {
            if (simpleDetailType == null)
                return;
            Text = simpleDetailType.text;
        }
        public string Text { get; set; }
    }
}
