﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IContinueSettingsType : ISettingsType
    {
        long TransactionId { get; set; }
    }
}
