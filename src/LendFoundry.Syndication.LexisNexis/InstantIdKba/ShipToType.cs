﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ShipToType : IShipToType
    {
        public ShipToType(ServiceReference.shiptotype shipToType)
        {
            if (shipToType == null)
                return;
            Item = shipToType.Item;
        }
        public object Item { get; set; }
    }
}
