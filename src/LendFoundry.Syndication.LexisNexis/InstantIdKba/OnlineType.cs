﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class OnlineType : IOnlineType
    {
        public OnlineType(ServiceReference.onlinetype onlineType)
        {
            if (onlineType == null)
                return;
            Credential = new CredentialType(onlineType.credential);
        }
        public ICredentialType Credential { get; set; }
    }
}
