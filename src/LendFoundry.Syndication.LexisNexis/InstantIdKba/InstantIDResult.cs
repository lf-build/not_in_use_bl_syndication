﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIDResult : IInstantIDResult
    {
        public InstantIDResult(ServiceReference.InstantIDResult instantIdResult)
        {
            if (instantIdResult == null)
                return;
            InputEcho = new InstantIDSearchBy(instantIdResult.InputEcho);
            UniqueId = instantIdResult.UniqueId;
            VerifiedInput = new VerifiedInput(instantIdResult.VerifiedInput);
            DOBVerified = instantIdResult.DOBVerified;
            NameAddressSSNSummary = instantIdResult.NameAddressSSNSummary;
            NameAddressPhone = new NameAddressPhone(instantIdResult.NameAddressPhone);
            ComprehensiveVerificationIndex = instantIdResult.ComprehensiveVerificationIndex;

            List<ISequencedRiskIndicator> riskindicators = new List<ISequencedRiskIndicator>();
            if (instantIdResult.RiskIndicators != null)
            {
                foreach (ServiceReference.SequencedRiskIndicator riskindicator in instantIdResult.RiskIndicators)
                {
                    riskindicators.Add(new SequencedRiskIndicator(riskindicator));
                }
                RiskIndicators = riskindicators.ToArray();
            }

            List<IRiskIndicator> potentialfollowupactions = new List<IRiskIndicator>();
            if (instantIdResult.PotentialFollowupActions != null)
            {
                foreach (ServiceReference.RiskIndicator potentialfollowupaction in instantIdResult.PotentialFollowupActions)
                {
                    potentialfollowupactions.Add(new RiskIndicator(potentialfollowupaction));
                }
                PotentialFollowupActions = potentialfollowupactions.ToArray();
            }

            InputCorrected = new InputCorrected(instantIdResult.InputCorrected);
            NewAreaCode = new NewAreaCode(instantIdResult.NewAreaCode);
            ReversePhone = new IdentityReversePhone(instantIdResult.ReversePhone);
            PhoneOfNameAddress = instantIdResult.PhoneOfNameAddress;
            SSNInfo = new SSNInfo(instantIdResult.SSNInfo);

            List<IChronologyHistory> chronologyhistories = new List<IChronologyHistory>();
            if (instantIdResult.ChronologyHistories != null)
            {
                foreach (ServiceReference.ChronologyHistory chronologyhistory in instantIdResult.ChronologyHistories)
                {
                    chronologyhistories.Add(new ChronologyHistory(chronologyhistory));
                }
                ChronologyHistories = chronologyhistories.ToArray();
            }

            List<IWatchList> watchlists = new List<IWatchList>();
            if (instantIdResult.WatchLists != null)
            {
                foreach (ServiceReference.WatchList watchlist in instantIdResult.WatchLists)
                {
                    watchlists.Add(new WatchList(watchlist));
                }
                WatchLists = watchlists.ToArray();
            }
            AdditionalScore1 = instantIdResult.AdditionalScore1;
            AdditionalScore2 = instantIdResult.AdditionalScore2;
            CurrentName = new Name(instantIdResult.CurrentName);

            List<IAdditionalLastName> additionallastnames = new List<IAdditionalLastName>();
            if (instantIdResult.AdditionalLastNames != null)
            {
                foreach (ServiceReference.AdditionalLastName additionallastname in instantIdResult.AdditionalLastNames)
                {
                    additionallastnames.Add(new AdditionalLastName(additionallastname));
                }
                AdditionalLastNames = additionallastnames.ToArray();
            }

            List<IModelSequenced> models = new List<IModelSequenced>();
            if (instantIdResult.Models != null)
            {
                foreach (ServiceReference.ModelSequenced model in instantIdResult.Models)
                {
                    models.Add(new ModelSequenced(model));
                }
                Models = models.ToArray();
            }

            RedFlagsReport = new RedFlagsReport(instantIdResult.RedFlagsReport);
            PassportValidated = instantIdResult.PassportValidated;
            FoundSSNCount = instantIdResult.FoundSSNCount;
            DecedentInfo = new DecedentInfo(instantIdResult.DecedentInfo);
            DOBMatchLevel = instantIdResult.DOBMatchLevel;
        }
        public IInstantIDSearchBy InputEcho { get; set; }

        public string UniqueId { get; set; }

        public IVerifiedInput VerifiedInput { get; set; }

        public bool DOBVerified { get; set; }

        public string NameAddressSSNSummary { get; set; }

        public INameAddressPhone NameAddressPhone { get; set; }

        public string ComprehensiveVerificationIndex { get; set; }

        public ISequencedRiskIndicator[] RiskIndicators { get; set; }

        public IRiskIndicator[] PotentialFollowupActions { get; set; }

        public IInputCorrected InputCorrected { get; set; }

        public INewAreaCode NewAreaCode { get; set; }

        public IIdentityReversePhone ReversePhone { get; set; }

        public string PhoneOfNameAddress { get; set; }

        public ISSNInfo SSNInfo { get; set; }

        public IChronologyHistory[] ChronologyHistories { get; set; }

        public IWatchList[] WatchLists { get; set; }

        public string AdditionalScore1 { get; set; }

        public string AdditionalScore2 { get; set; }

        public IName CurrentName { get; set; }

        public IAdditionalLastName[] AdditionalLastNames { get; set; }

        public IModelSequenced[] Models { get; set; }

        public IRedFlagsReport RedFlagsReport { get; set; }

        public bool PassportValidated { get; set; }

        public string FoundSSNCount { get; set; }

        public IDecedentInfo DecedentInfo { get; set; }

        public string DOBMatchLevel { get; set; }
    }
}
