﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IUkAddressType
    {
        string AddressId { get; set; }

        string HouseName { get; set; }

        string HouseNumber { get; set; }

        string Street1 { get; set; }

        string Street2 { get; set; }

        string District { get; set; }

        string Town { get; set; }

        string County { get; set; }

        string Postcode { get; set; }

        AddressContextType Context { get; set; }

        decimal YearsAt { get; set; }

        bool YearsAtSpecified { get; set; }

    }
}
