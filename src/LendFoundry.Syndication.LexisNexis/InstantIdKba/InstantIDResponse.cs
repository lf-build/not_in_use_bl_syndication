﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class InstantIDResponse : IInstantIDResponse
    {
        public InstantIDResponse(ServiceReference.InstantIDResponse instantIdResponse)
        {
            if (instantIdResponse == null)
                return;
            Header = new ResponseHeader(instantIdResponse.Header);
            Result = new InstantIDResult(instantIdResponse.Result);
        }
        public IResponseHeader Header { get; set; }

        public IInstantIDResult Result { get; set; }
    }
}
