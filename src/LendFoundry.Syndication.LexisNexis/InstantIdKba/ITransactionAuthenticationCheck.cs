﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface ITransactionAuthenticationCheck
    {
        ITransactionAuthenticationCheckSettings Settings { get; set; }

        ICredentialType Credential { get; set; }

        IAccountType Account { get; set; }
    }
}
