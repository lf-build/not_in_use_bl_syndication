﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class AnswerType : IAnswerType
    {
        public AnswerType(ServiceReference.answertype answerType)
        {
            if (answerType == null)
                return;
            QuestionId = answerType.questionid;
            Choices = answerType.choices;
        }
        public long QuestionId { get; set; }

        public long[] Choices { get; set; }
    }
}
