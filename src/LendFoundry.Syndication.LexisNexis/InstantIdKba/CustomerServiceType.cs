﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class CustomerServiceType : ICustomerServiceType
    {
        public CustomerServiceType(ServiceReference.customerservicetype customerServiceType)
        {
            if (customerServiceType == null)
                return;
            List<CredentialMethodType> credentials = new List<CredentialMethodType>();
            if (customerServiceType.credentialmethod != null)
            {
                foreach (var credential in customerServiceType.credentialmethod)
                {
                    credentials.Add((CredentialMethodType)(int)credential);
                }
                CredentialMethod = credentials.ToArray();
            }
        }
        public CredentialMethodType[] CredentialMethod { get; set; }
    }
}
