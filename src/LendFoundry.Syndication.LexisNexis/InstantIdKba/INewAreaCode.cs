﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface INewAreaCode
    {
        string AreaCode { get; set; }

        IDate EffectiveDate { get; set; }
    }
}
