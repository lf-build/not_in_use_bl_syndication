﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ExternalResponsesType : IExternalResponsesType
    {
        public ExternalResponsesType(ServiceReference.externalresponsestype externalResponseType)
        {
            if (externalResponseType == null)
                return;
            InstantIDResponse = new InstantIDResponseEx(externalResponseType.instantIDResponse);
            FlexIDResponse = new FlexIDResponseEx(externalResponseType.flexIDResponse);
            FraudPointResponse = new FraudPointResponseEx(externalResponseType.fraudPointResponse);
        }
        public IInstantIDResponseEx InstantIDResponse { get; set; }

        public IFlexIDResponseEx FlexIDResponse { get; set; }

        public IFraudPointResponseEx FraudPointResponse { get; set; }
    }
}
