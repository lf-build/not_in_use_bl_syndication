﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class TransactionStatusType : ITransactionStatusType
    {
        public TransactionStatusType(ServiceReference.transactionstatustype transactionStatusType)
        {
            if (transactionStatusType == null)
                return;
            TransactionId = transactionStatusType.transactionid;
            TransactionRequestId = transactionStatusType.transactionrequestid;
            AccountsTransactionId = transactionStatusType.accountstransactionid;
            List<string> referenceIds = new List<string>();
            if (transactionStatusType.referenceid != null)
            {
                foreach (string referenceId in transactionStatusType.referenceid)
                {
                    referenceIds.Add(referenceId);
                }
                ReferenceId = referenceIds.ToArray();
            }

            TransactionResult = (TransactionResultType)(int)transactionStatusType.transactionresult;
            RiskAssessment = new RiskAssessmentType(transactionStatusType.riskassessment);
            List<ISpecialFeatureType> specialFeatures = new List<ISpecialFeatureType>();
            if (transactionStatusType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in transactionStatusType.specialfeature)
                {
                    specialFeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialFeatures.ToArray();
            }
        }
        public long TransactionId { get; set; }

        public long TransactionRequestId { get; set; }

        public string AccountsTransactionId { get; set; }

        public string[] ReferenceId { get; set; }

        public TransactionResultType TransactionResult { get; set; }

        public IRiskAssessmentType RiskAssessment { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }
    }
}
