﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum AddressContextType
    {
        Primary,
        Secondary,
        Previous,
        Vacation,
        Business,
        Other
    }
}
