﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SettingsType : ISettingsType
    {
        public SettingsType(ServiceReference.settingstype settingsType)
        {
            if (settingsType == null)
                return;
            AccountName = settingsType.accountname;
            Mode = (ModeType)(int)settingsType.mode;
            RuleSet = settingsType.ruleset;
            SimulatorMode = (SimulatorModeType)(int)settingsType.simulatormode;
            AttachmentType = (AttachmentType)(int)settingsType.attachmenttype;
            List<ISpecialFeatureType> specialfeatures = new List<ISpecialFeatureType>();
            if (settingsType.specialfeature != null)
            {
                foreach (ServiceReference.specialfeaturetype specialfeature in settingsType.specialfeature)
                {
                    specialfeatures.Add(new SpecialFeatureType(specialfeature));
                }
                SpecialFeature = specialfeatures.ToArray();
            }
            List<IInternationalizationType> internationalizations = new List<IInternationalizationType>();
            if (settingsType.internationalization != null)
            {
                foreach (ServiceReference.internationalizationtype internationalization in settingsType.internationalization)
                {
                    internationalizations.Add(new InternationalizationType(internationalization));
                }
                Internationalization = internationalizations.ToArray();
            }
            ReferenceId = settingsType.referenceid;
            SimulatorModeSpecified = settingsType.simulatormodeSpecified;
            ModeSpecified = settingsType.modeSpecified;
            AttachmentTypeSpecified = settingsType.attachmenttypeSpecified;
        }

        public string AccountName { get; set; }

        public ModeType Mode { get; set; }
        public bool ModeSpecified { get; set; }

        public string RuleSet { get; set; }

        public SimulatorModeType SimulatorMode { get; set; }
        public bool SimulatorModeSpecified { get; set; }

        public AttachmentType AttachmentType { get; set; }
        public bool AttachmentTypeSpecified { get; set; }

        public ISpecialFeatureType[] SpecialFeature { get; set; }

        public IInternationalizationType[] Internationalization { get; set; }

        public string[] ReferenceId { get; set; }
    }
}
