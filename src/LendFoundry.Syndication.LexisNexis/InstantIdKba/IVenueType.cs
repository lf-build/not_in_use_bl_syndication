﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IVenueType
    {
        object Item { get; set; }

        ItemChoiceType ItemElementName { get; set; }
    }
}
