﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class RedFlagsReport : IRedFlagsReport
    {
        public RedFlagsReport(ServiceReference.RedFlagsReport redFlagsReport)
        {
            if (redFlagsReport == null)
                return;
            Version = redFlagsReport.Version;
            List<IRedFlag> redflags = new List<IRedFlag>();
            if (redFlagsReport.RedFlags != null)
            {
                foreach (ServiceReference.RedFlag redflag in redFlagsReport.RedFlags)
                {
                    redflags.Add(new RedFlag(redflag));
                }
                RedFlags = redflags.ToArray();
            }
        }
        public string Version { get; set; }

        public IRedFlag[] RedFlags { get; set; }
    }
}
