﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum ModeType
    {
        Simulated,
        Testing,
        Pilot,
        Live,
        Verid,
        Verid_cs,
    }
}
