﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class FlexIDNameValuePair : IFlexIDNameValuePair
    {
        public FlexIDNameValuePair(ServiceReference.FlexIDNameValuePair flexIdNameValuePair)
        {
            if (flexIdNameValuePair == null)
                return;
            Name = flexIdNameValuePair.Name;
            Value = flexIdNameValuePair.Value;
        }
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
