﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class ScoreSequenced : IScoreSequenced
    {
        public ScoreSequenced(ServiceReference.ScoreSequenced scoreSequenced)
        {
            if (scoreSequenced == null)
                return;
            Type = scoreSequenced.Type;
            Value = scoreSequenced.Value;
            List<INameValuePair> riskindices = new List<INameValuePair>();
            if (scoreSequenced.RiskIndices != null)
            {
                foreach (ServiceReference.NameValuePair riskindice in scoreSequenced.RiskIndices)
                {
                    riskindices.Add(new NameValuePair(riskindice));
                }
                RiskIndices = riskindices.ToArray();
            }

            List<ISequencedRiskIndicator> riskindicators = new List<ISequencedRiskIndicator>();
            if (scoreSequenced.RiskIndicators != null)
            {
                foreach (ServiceReference.SequencedRiskIndicator riskindicator in scoreSequenced.RiskIndicators)
                {
                    riskindicators.Add(new SequencedRiskIndicator(riskindicator));
                }
                RiskIndicators = riskindicators.ToArray();
            }
        }
        public string Type { get; set; }

        public string Value { get; set; }

        public INameValuePair[] RiskIndices { get; set; }

        public ISequencedRiskIndicator[] RiskIndicators { get; set; }
    }
}
