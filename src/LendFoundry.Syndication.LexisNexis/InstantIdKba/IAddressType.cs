﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IAddressType
    {
        string AddressStreet1 { get; set; }

        string AddressStreet2 { get; set; }

        string Suite { get; set; }

        string AddressCity { get; set; }

        string AddressState { get; set; }

        string AddressZip { get; set; }

        string AddressZipPlus4 { get; set; }

        string AddressCountry { get; set; }

        AddressContextType AddressContext { get; set; }

        decimal AddressYearsAt { get; set; }

        System.DateTime OccupationStartDate { get; set; }

    }
}
