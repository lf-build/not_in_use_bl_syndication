﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDModelSequencedHRI
    {
        string Name { get; set; }

        IFlexIDScoreSequencedHRI[] Scores { get; set; }
    }
}
