﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IRiskAssessmentType
    {
        double RiskScore { get; set; }

        ThreatAssessment ThreatAssessment { get; set; }

        IRiskComponentType[] RiskComponent { get; set; }
    }
}
