﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFlexIDCustomComprehensiveVerificationStruct
    {
        int ComprehensiveVerificationIndex { get; set; }

        IFlexIDSequencedRiskIndicator[] RiskIndicators { get; set; }

        IFlexIDRiskIndicator[] PotentialFollowupActions { get; set; }

        string Name { get; set; }
    }
}
