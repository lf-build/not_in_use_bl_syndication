﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public enum TaskType
    {
        ICheck,
        IAuth,
        IAuto,
        IAge,
        IMonitor,
        IKba,
    }
}
