﻿using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class CallCenterType : ICallCenterType
    {
        public CallCenterType(ServiceReference.callcentertype callCenterType)
        {
            if (callCenterType == null)
                return;
            CallerPhoneNumber = callCenterType.callerphonenumber;
            List<CredentialMethodType> credentials = new List<CredentialMethodType>();
            if (callCenterType.credentialmethod != null)
            {
                foreach (var credential in callCenterType.credentialmethod)
                {
                    credentials.Add((CredentialMethodType)(int)credential);
                }
                CredentialMethod = credentials.ToArray();
            }
            CallerId = callCenterType.callerid;
        }
        public string CallerPhoneNumber { get; set; }

        public CredentialMethodType[] CredentialMethod { get; set; }

        public string CallerId { get; set; }
    }
}
