﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface INetViewType
    {
        string NetViewForm { get; set; }
    }
}
