﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IPurchaseTypeOrder
    {
        IProductType[] Item { get; set; }

        decimal TotalAmount { get; set; }

        decimal TotalTax { get; set; }

        decimal TotalShipping { get; set; }

        string Merchant { get; set; }
    }
}
