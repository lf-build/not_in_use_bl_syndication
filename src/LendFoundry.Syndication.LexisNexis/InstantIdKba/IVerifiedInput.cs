﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IVerifiedInput
    {
        IName Name { get; set; }

        IAddress Address { get; set; }

        string SSN { get; set; }

        string HomePhone { get; set; }

        IMaskableDate DOB { get; set; }

        string DriverLicenseNumber { get; set; }
    }
}
