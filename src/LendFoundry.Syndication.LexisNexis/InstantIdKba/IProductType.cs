﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IProductType
    {
        ProductCategoryType ProductCategory { get; set; }

        string Model { get; set; }

        string Vendor { get; set; }

        string Code { get; set; }

        string Symbol { get; set; }

        TransactionDirectionType TransactionDirection { get; set; }

        string ProductQuantity { get; set; }

        decimal PerItemAmount { get; set; }

        System.DateTime TransactionDate { get; set; }

        IComplexDetailType Data { get; set; }
    }
}
