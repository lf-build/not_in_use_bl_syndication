﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IFraudPointNameValuePair
    {
        string Name { get; set; }

        string Value { get; set; }
    }
}
