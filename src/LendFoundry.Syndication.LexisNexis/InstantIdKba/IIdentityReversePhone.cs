﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IIdentityReversePhone
    {
        IName Name { get; set; }

        IAddress Address { get; set; }
    }
}
