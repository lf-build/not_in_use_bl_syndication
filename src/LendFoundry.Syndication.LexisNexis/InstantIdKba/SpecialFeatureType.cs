﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public class SpecialFeatureType : ISpecialFeatureType
    {
        public SpecialFeatureType(ServiceReference.specialfeaturetype specialFeatureType)
        {
            if (specialFeatureType == null)
                return;
            SpecialFeatureCode = specialFeatureType.specialfeaturecode;
            SpecialFeatureValue = specialFeatureType.specialfeaturevalue;
        }
        public string SpecialFeatureCode { get; set; }

        public string SpecialFeatureValue { get; set; }
    }
}
