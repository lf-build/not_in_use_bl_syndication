﻿namespace LendFoundry.Syndication.LexisNexis.InstantIdKba
{
    public interface IBillToType
    {
        object Item { get; set; }
    }
}
