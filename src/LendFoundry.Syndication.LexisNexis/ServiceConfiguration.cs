﻿namespace LendFoundry.Syndication.LexisNexis
{
    public class ServiceConfiguration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
