﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class FlexIdServiceConfiguration
    {
        public string FlexIdUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.85";
        public string UserName { get; set; }
        public string Password { get; set; }
        [JsonConverter(typeof(InterfaceConverter<FlexId.IUser, FlexId.User>))]
        public FlexId.IUser EndUser { get; set; }
        [JsonConverter(typeof(InterfaceConverter<FlexId.IFlexIdOption, FlexId.FlexIdOption>))]
        public FlexId.IFlexIdOption Options { get; set; }
    }
}