namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class InputCorrected : IInputCorrected
	{
        public InputCorrected(ServiceReference.InputCorrected inputCorrected)
        {
            if (inputCorrected == null)
                return;

            Name = new Name(inputCorrected.Name);
            Address = new Address(inputCorrected.Address);
            SSN = inputCorrected.SSN;
            HomePhone = inputCorrected.HomePhone;
            DOB = new Date(inputCorrected.DOB);
        }

		public IName Name { get; set; }
		public IAddress Address { get; set; }
		public string SSN { get; set; }
		public string HomePhone { get; set; }
		public IDate DOB { get; set; }
	}
}
