namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IInputCorrected
	{
		 IName Name { get; set; }
		 IAddress Address { get; set; }
		 string SSN { get; set; }
		 string HomePhone { get; set; }
		 IDate DOB { get; set; }
	}
}
