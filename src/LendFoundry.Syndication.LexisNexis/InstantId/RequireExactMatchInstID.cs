namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class RequireExactMatchInstId : IRequireExactMatchInstId
    {
        public RequireExactMatchInstId()
        {
        }

        public RequireExactMatchInstId(ServiceReference.RequireExactMatchInstID requireExactMatch)
        {
            if (requireExactMatch == null)
                return;

            LastName = requireExactMatch.LastName;
            FirstName = requireExactMatch.FirstName;
            FirstNameAllowNickname = requireExactMatch.FirstNameAllowNickname;
            HomePhone = requireExactMatch.HomePhone;
            Ssn = requireExactMatch.SSN;
        }

        public bool LastName { get; set; } = false;
        public bool FirstName { get; set; } = false;
        public bool FirstNameAllowNickname { get; set; } = true;
        public bool HomePhone { get; set; } = false;
        public bool Ssn { get; set; } = false;
    }
}