using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class ModelRequest : IModelRequest
    {
        public ModelRequest()
        {
        }

        public ModelRequest(ServiceReference.ModelRequest modelRequest)
        {
            if (modelRequest == null)
                return;

            ModelName = modelRequest.ModelName;
            if (modelRequest.ModelOptions != null)
            {
                List<IModelOption> modelOptions = new List<IModelOption>();
                foreach (ServiceReference.ModelOption modelOption in modelRequest.ModelOptions)
                {
                    modelOptions.Add(new ModelOption(modelOption));
                }

                ModelOptions = modelOptions.ToArray();
            }
        }

        public string ModelName { get; set; }
        public IModelOption[] ModelOptions { get; set; }
    }
}