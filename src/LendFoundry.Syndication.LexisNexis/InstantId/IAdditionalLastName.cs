namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IAdditionalLastName
	{
		 IDate DateLastSeen { get; set; }
		 string LastName { get; set; }
	}
}
