namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class Date : IDate
    {
        public Date()
        {
        }

        public Date(ServiceReference.Date date)
        {
            if (date == null)
                return;

            Day = date.Day;
            Month = date.Month;
            Year = date.Year;
        }

        public short Year { get; set; }
        public short Month { get; set; }
        public short Day { get; set; }
    }
}