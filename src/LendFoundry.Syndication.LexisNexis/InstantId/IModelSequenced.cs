namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IModelSequenced
	{
		 string Name { get; set; }
		 IScoreSequenced[] Scores { get; set; }
	}
}
