namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IDateOfBirthMatchOptions
	{
		 DateOfBirthMatchType MatchType { get; set; }
		 int MatchYearRadius { get; set; }
	}
}
