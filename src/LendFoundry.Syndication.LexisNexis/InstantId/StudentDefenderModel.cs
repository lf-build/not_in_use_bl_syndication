namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class StudentDefenderModel : IStudentDefenderModel
    {
        public StudentDefenderModel()
        {
        }

        public StudentDefenderModel(ServiceReference.StudentDefenderModel student)
        {
            if (student == null)
                return;

            StudentDefender = student.StudentDefender;
            IsStudentApplicant = student.IsStudentApplicant;
        }

        public bool StudentDefender { get; set; }
        public bool IsStudentApplicant { get; set; }
    }
}