namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public interface IDate
    {
        short Year { get; set; }
        short Month { get; set; }
        short Day { get; set; }
    }
}
