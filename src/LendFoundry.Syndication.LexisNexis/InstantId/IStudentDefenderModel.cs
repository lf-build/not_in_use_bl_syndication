namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IStudentDefenderModel
	{
		 bool StudentDefender { get; set; }
		 bool IsStudentApplicant { get; set; }
	}
}
