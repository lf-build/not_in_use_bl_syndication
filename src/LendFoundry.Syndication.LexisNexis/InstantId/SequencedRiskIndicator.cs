namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class SequencedRiskIndicator : ISequencedRiskIndicator
	{
        public SequencedRiskIndicator(ServiceReference.SequencedRiskIndicator risk)
        {
            if (risk == null)
                return;

            RiskCode = risk.RiskCode;
            Description = risk.Description;
            Sequence = risk.Sequence;
        }

		public string RiskCode { get; set; }
		public string Description { get; set; }
		public int Sequence { get; set; }
	}
}
