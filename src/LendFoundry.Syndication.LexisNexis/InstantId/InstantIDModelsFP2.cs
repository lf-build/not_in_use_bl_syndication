using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class InstantIdModelsFp2 : IInstantIdModelsFp2
    {
        public InstantIdModelsFp2()
        {
        }

        public InstantIdModelsFp2(ServiceReference.InstantIDModelsFP2 instantIdModelsFp2)
        {
            if (instantIdModelsFp2 == null)
                return;

            Thindex = instantIdModelsFp2.Thindex;
            StudentDefenderModel = new StudentDefenderModel(instantIdModelsFp2.StudentDefenderModel);

            if (instantIdModelsFp2.ModelRequests != null)
            {
                var modelRequests = new List<IModelRequest>();
                foreach (ServiceReference.ModelRequest modelRequest in instantIdModelsFp2.ModelRequests)
                {
                    modelRequests.Add(new ModelRequest(modelRequest));
                }

                ModelRequests = modelRequests.ToArray();
            }

            FraudPointModel = new FraudPointModelWithOptions(instantIdModelsFp2.FraudPointModel);
        }

        public bool Thindex { get; set; }
        public IStudentDefenderModel StudentDefenderModel { get; set; }
        public IModelRequest[] ModelRequests { get; set; }
        public IFraudPointModelWithOptions FraudPointModel { get; set; }
    }
}