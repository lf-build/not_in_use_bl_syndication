namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IRedFlag
	{
		 string Name { get; set; }
		 ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
	}
}
