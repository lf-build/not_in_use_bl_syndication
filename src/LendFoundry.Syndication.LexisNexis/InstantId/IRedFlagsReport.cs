namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IRedFlagsReport
	{
		 int Version { get; set; }
		 IRedFlag[] RedFlags { get; set; }
	}
}
