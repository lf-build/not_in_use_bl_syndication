namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IIdentityReversePhone
	{
		 IName Name { get; set; }
		 IAddress Address { get; set; }
	}
}
