using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class ComprehensiveVerificationStruct : IComprehensiveVerificationStruct
	{
        public ComprehensiveVerificationStruct(ServiceReference.ComprehensiveVerificationStruct comprehensiveVerificationStruct)
        {
            if (comprehensiveVerificationStruct == null)
                return;

            ComprehensiveVerificationIndex = comprehensiveVerificationStruct.ComprehensiveVerificationIndex;

            if(comprehensiveVerificationStruct.RiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach(ServiceReference.SequencedRiskIndicator riskIndicator in comprehensiveVerificationStruct.RiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(riskIndicator));
                }

                RiskIndicators = riskIndicators.ToArray();
            }

            if(comprehensiveVerificationStruct.PotentialFollowupActions != null)
            {
                List<IRiskIndicator> followUpActions = new List<IRiskIndicator>();
                foreach(ServiceReference.RiskIndicator followUpAction in comprehensiveVerificationStruct.PotentialFollowupActions)
                {
                    followUpActions.Add(new RiskIndicator(followUpAction));
                }

                PotentialFollowupActions = followUpActions.ToArray();
            }
        }

		public int ComprehensiveVerificationIndex { get; set; }
		public ISequencedRiskIndicator[] RiskIndicators { get; set; }
		public IRiskIndicator[] PotentialFollowupActions { get; set; }
	}
}
