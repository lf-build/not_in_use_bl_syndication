namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class NameAddressPhone : INameAddressPhone
	{
        public NameAddressPhone(ServiceReference.NameAddressPhone nameAddressPhone)
        {
            if (nameAddressPhone == null)
                return;

            Summary = nameAddressPhone.Summary;
            Type = nameAddressPhone.Type;
            Status = nameAddressPhone.Status;
        }

		public string Summary { get; set; }
		public string Type { get; set; }
		public string Status { get; set; }
	}
}
