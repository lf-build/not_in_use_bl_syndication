namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface INameValuePair
	{
		 string Name { get; set; }
		 string Value { get; set; }
	}
}
