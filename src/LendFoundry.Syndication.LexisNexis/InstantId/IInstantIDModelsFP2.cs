namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IInstantIdModelsFp2
	{
		 bool Thindex { get; set; }


		 IStudentDefenderModel StudentDefenderModel { get; set; }
		 IModelRequest[] ModelRequests { get; set; }
		 IFraudPointModelWithOptions FraudPointModel { get; set; }
	}
}
