namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IInstantIdResponse
	{
		 IResponseHeader Header { get; set; }
		 IInstantIdResult Result { get; set; }
	}
}
