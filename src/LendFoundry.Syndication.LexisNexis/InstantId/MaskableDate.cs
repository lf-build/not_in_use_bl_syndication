namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class MaskableDate : IMaskableDate
	{
        public MaskableDate(ServiceReference.MaskableDate maskableDate)
        {
            if (maskableDate == null)
                return;

            Year = maskableDate.Year;
            Month = maskableDate.Month;
            Day = maskableDate.Day;
        }

		public string Year { get; set; }
		public string Month { get; set; }
		public string Day { get; set; }
	}
}
