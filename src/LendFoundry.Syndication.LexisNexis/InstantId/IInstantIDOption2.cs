using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public interface IInstantIdOption2
    {
        string[] WatchLists { get; set; }
        bool IncludeClOverride { get; set; }
        bool IncludeMsOverride { get; set; }
        bool IncludeDrivingLicenseVerification { get; set; }
        bool PoBoxCompliance { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IInstantIdModelsFp2, InstantIdModelsFp2>))]
        IInstantIdModelsFp2 IncludeModels { get; set; }
        string RedFlagsReport { get; set; }
        string GlobalWatchlistThreshold { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDateOfBirthMatchOptions, DateOfBirthMatchOptions>))]
        IDateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        bool IncludeAllRiskIndicators { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRequireExactMatchInstId, RequireExactMatchInstId>))]
        IRequireExactMatchInstId RequireExactMatch { get; set; }
        string CustomCviModelName { get; set; }
        string LastSeenThreshold { get; set; }
        bool IncludeMiOverride { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IOptionsForCviCalculation, OptionsForCviCalculation>))]
        IOptionsForCviCalculation CviCalculationOptions { get; set; }
        string InstantIdVersion { get; set; }
        bool IncludeDeliveryPointBarcode { get; set; }
    }
}