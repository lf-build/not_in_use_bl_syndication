using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class InstantIdResult : IInstantIdResult
	{
        public InstantIdResult(ServiceReference.InstantIDResult instantIdResult)
        {
            if (instantIdResult == null)
                return;

            InputEcho = new InstantIdSearchBy(instantIdResult.InputEcho);
            UniqueId = instantIdResult.UniqueId;
            VerifiedInput = new VerifiedInput(instantIdResult.VerifiedInput);
            DateOfBirthVerified = instantIdResult.DOBVerified;
            NameAddressSsnSummary = instantIdResult.NameAddressSSNSummary;
            NameAddressPhone = new NameAddressPhone(instantIdResult.NameAddressPhone);
            ComprehensiveVerification = new ComprehensiveVerificationStruct(instantIdResult.ComprehensiveVerification);
            CustomComprehensiveVerification = new CustomComprehensiveVerificationStruct(instantIdResult.CustomComprehensiveVerification);
            InputCorrected = new InputCorrected(instantIdResult.InputCorrected);
            NewAreaCode = new NewAreaCode(instantIdResult.NewAreaCode);
            ReversePhone = new IdentityReversePhone(instantIdResult.ReversePhone);
            PhoneOfNameAddress = instantIdResult.PhoneOfNameAddress;
            SsnInfo = new SsnInfo(instantIdResult.SSNInfo);

            if(instantIdResult.ChronologyHistories != null)
            {
                List<IChronologyHistory> chronologyHistories = new List<IChronologyHistory>();
                foreach(ServiceReference.ChronologyHistory chronologyHistory in instantIdResult.ChronologyHistories)
                {
                    chronologyHistories.Add(new ChronologyHistory(chronologyHistory));
                }

                ChronologyHistories = chronologyHistories.ToArray();
            }

            if(instantIdResult.WatchLists != null)
            {
                List<IWatchList> watchLists = new List<IWatchList>();
                foreach(ServiceReference.WatchList watchList in instantIdResult.WatchLists)
                {
                    watchLists.Add(new WatchList(watchList));
                }
                WatchLists = watchLists.ToArray();
            }

            AdditionalScore1 = instantIdResult.AdditionalScore1;
            AdditionalScore2 = instantIdResult.AdditionalScore2;
            CurrentName = new Name(instantIdResult.CurrentName);

            if(instantIdResult.AdditionalLastNames != null)
            {
                List<IAdditionalLastName> additionalLastNames = new List<IAdditionalLastName>();
                foreach(ServiceReference.AdditionalLastName additionalLastName in instantIdResult.AdditionalLastNames)
                {
                    additionalLastNames.Add(new AdditionalLastName(additionalLastName));
                }

                AdditionalLastNames = additionalLastNames.ToArray();
            }

            if(instantIdResult.Models != null)
            {
                List<IModelSequenced> models = new List<IModelSequenced>();
                foreach(ServiceReference.ModelSequenced model in instantIdResult.Models)
                {
                    models.Add(new ModelSequenced(model));
                }

                Models = models.ToArray();
            }

            RedFlagsReport = new RedFlagsReport(instantIdResult.RedFlagsReport);

            PassportValidated = instantIdResult.PassportValidated;
            FoundSsnCount = instantIdResult.FoundSSNCount;
            DecedentInfo = new DecedentInfo(instantIdResult.DecedentInfo);
            DateOfBirthMatchLevel = instantIdResult.DOBMatchLevel;
            SsnFoundForLexId = instantIdResult.SSNFoundForLexID;
            AddressPoBox = instantIdResult.AddressPOBox;
            AddressCmra = instantIdResult.AddressCMRA;
            InstantIdVersion = instantIdResult.InstantIDVersion;
        }

		public IInstantIdSearchBy InputEcho { get; set; }
		public string UniqueId { get; set; }
		public IVerifiedInput VerifiedInput { get; set; }
		public bool DateOfBirthVerified { get; set; }
		public int NameAddressSsnSummary { get; set; }
		public INameAddressPhone NameAddressPhone { get; set; }
		public IComprehensiveVerificationStruct ComprehensiveVerification { get; set; }
		public ICustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }
		public IInputCorrected InputCorrected { get; set; }
		public INewAreaCode NewAreaCode { get; set; }
		public IIdentityReversePhone ReversePhone { get; set; }
		public string PhoneOfNameAddress { get; set; }
		public ISsnInfo SsnInfo { get; set; }
		public IChronologyHistory[] ChronologyHistories { get; set; }
		public IWatchList[] WatchLists { get; set; }
		public string AdditionalScore1 { get; set; }
		public string AdditionalScore2 { get; set; }
		public IName CurrentName { get; set; }
		public IAdditionalLastName[] AdditionalLastNames { get; set; }
		public IModelSequenced[] Models { get; set; }
		public IRedFlagsReport RedFlagsReport { get; set; }
		public bool PassportValidated { get; set; }
		public int FoundSsnCount { get; set; }
		public IDecedentInfo DecedentInfo { get; set; }
		public int DateOfBirthMatchLevel { get; set; }
		public bool SsnFoundForLexId { get; set; }
		public bool AddressPoBox { get; set; }
		public bool AddressCmra { get; set; }
		public string InstantIdVersion { get; set; }
	}
}
