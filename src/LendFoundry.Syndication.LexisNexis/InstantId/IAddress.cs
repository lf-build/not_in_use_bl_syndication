namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IAddress
	{
		 string StreetNumber { get; set; }
		 string StreetPreDirection { get; set; }
		 string StreetName { get; set; }
		 string StreetSuffix { get; set; }
		 string StreetPostDirection { get; set; }
		 string UnitDesignation { get; set; }
		 string UnitNumber { get; set; }
		 string StreetAddress1 { get; set; }
		 string StreetAddress2 { get; set; }
		 string City { get; set; }
		 string State { get; set; }
		 string Zip5 { get; set; }
		 string Zip4 { get; set; }
		 string County { get; set; }
		 string PostalCode { get; set; }
		 string StateCityZip { get; set; }
	}
}
