﻿namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class InstantIdRequest : IInstantIdRequest
    {
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IDate DateOfBirth { get; set; }
        public int Age { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string SocialSecurityNumberLast4 { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public bool UseDateOfBirthFilter { get; set; }
        public int DateOfBirthRadius { get; set; }
        public IPassport Passport { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public ChannelIdentifier Channel { get; set; }
        public string Income { get; set; }
        public OwnRent OwnOrRent { get; set; }
        public string LocationIdentifier { get; set; }
        public string OtherApplicationIdentifier1 { get; set; }
        public string OtherApplicationIdentifier2 { get; set; }
        public string OtherApplicationIdentifier3 { get; set; }
        public ITimeStamp ApplicationDateTime { get; set; }
    }
}