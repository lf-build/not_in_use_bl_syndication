namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface ISsnInfo
	{
		 string Ssn { get; set; }
		 string Valid { get; set; }
		 string IssuedLocation { get; set; }
		 IDate IssuedStartDate { get; set; }
		 IDate IssuedEndDate { get; set; }
	}
}
