﻿namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public interface IInstantIdClient
    {
        ServiceReference.InstantIDResponse Verify(ServiceReference.InstantIDSearchBy searchBy);
    }
}
