﻿namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public enum OwnRent
    {
        Item,
        Own,
        Rent
    }
}
