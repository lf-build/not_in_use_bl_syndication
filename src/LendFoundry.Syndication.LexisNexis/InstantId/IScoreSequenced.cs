namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IScoreSequenced
	{
		 string Type { get; set; }
		 int Value { get; set; }
		 INameValuePair[] RiskIndices { get; set; }
		 ISequencedRiskIndicator[] RiskIndicators { get; set; }
	}
}
