namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public interface IUser
    {
        string ReferenceCode { get; set; }
        string BillingCode { get; set; }
        string QueryId { get; set; }
        string GrammLeachBlileyPurpose { get; set; }
        string DriverLicensePurpose { get; set; }
        IEndUserInfo EndUser { get; set; }
        int MaxWaitSeconds { get; set; }
        string AccountNumber { get; set; }
    }
}