namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IRequireExactMatchInstId
	{
		 bool LastName { get; set; }
		 bool FirstName { get; set; }
		 bool FirstNameAllowNickname { get; set; }
		 bool HomePhone { get; set; }
		 bool Ssn { get; set; }
	}
}
