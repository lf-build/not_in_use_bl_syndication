namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class WsException : IWsException
	{
        public WsException(ServiceReference.WsException exception)
        {
            if (exception == null)
                return;

            Source = exception.Source;
            Code = exception.Code;
            Location = exception.Location;
            Message = exception.Message;
        }

		public string Source { get; set; }
		public int Code { get; set; }
		public string Location { get; set; }
		public string Message { get; set; }
	}
}
