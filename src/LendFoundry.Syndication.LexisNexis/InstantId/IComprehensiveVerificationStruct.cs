namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IComprehensiveVerificationStruct
	{
		 int ComprehensiveVerificationIndex { get; set; }
		 ISequencedRiskIndicator[] RiskIndicators { get; set; }
		 IRiskIndicator[] PotentialFollowupActions { get; set; }
	}
}
