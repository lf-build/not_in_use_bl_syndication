namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface INameAddressPhone
	{
		 string Summary { get; set; }
		 string Type { get; set; }
		 string Status { get; set; }
	}
}
