﻿namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public enum ChannelIdentifier
    {
        Item,
        Mail,
        PointOfSale,
        Kiosk,
        Internet,
        Branch,
        Telephonic,
        Other
    }
}
