namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IRiskIndicator
	{
		 string RiskCode { get; set; }
		 string Description { get; set; }
	}
}
