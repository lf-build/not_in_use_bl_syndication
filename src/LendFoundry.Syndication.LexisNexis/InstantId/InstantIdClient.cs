﻿using System;
using System.Net;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class InstantIdClient : IInstantIdClient
    {
        public InstantIdClient(ILexisNexisConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.InstantId == null)
                throw new ArgumentNullException(nameof(configuration.InstantId));

            if (configuration.InstantId.UserName == null)
                throw new ArgumentNullException(nameof(configuration.InstantId.UserName));

            if (configuration.InstantId.Password == null)
                throw new ArgumentNullException(nameof(configuration.InstantId.Password));

            Configuration = configuration.InstantId;
        }

        private InstantIdServiceConfiguration Configuration { get; }

        public ServiceReference.InstantIDResponse Verify(ServiceReference.InstantIDSearchBy searchBy)
        {
            try
            {
                if (searchBy == null)
                    throw new ArgumentNullException(nameof(searchBy));

                ServiceReference.WsIdentity soapClient = new ServiceReference.WsIdentity();
                soapClient.Url = Configuration.InstantIdUrl;
                soapClient.Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password);

                ServiceReference.User user = new ServiceReference.User(Configuration.EndUser);
                ServiceReference.InstantIDOption2 options = new ServiceReference.InstantIDOption2(Configuration.Options);

                return soapClient.InstantID(user, options, searchBy);
            }
            catch (Exception ex)
            {
                throw new LexisNexisException($"The method Verify({searchBy}) raised an error:{ex.Message}", ex);
            }
        }
    }
}