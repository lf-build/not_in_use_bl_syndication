namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IModelRequest
	{
		 string ModelName { get; set; }
		 IModelOption[] ModelOptions { get; set; }
	}
}
