using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class CustomComprehensiveVerificationStruct : ICustomComprehensiveVerificationStruct
	{
        public CustomComprehensiveVerificationStruct(ServiceReference.CustomComprehensiveVerificationStruct customComprehensiveVerificationStruct)
        {
            if (customComprehensiveVerificationStruct == null)
                return;

            ComprehensiveVerificationIndex = customComprehensiveVerificationStruct.ComprehensiveVerificationIndex;

            if (customComprehensiveVerificationStruct.RiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach (ServiceReference.SequencedRiskIndicator riskIndicator in customComprehensiveVerificationStruct.RiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(riskIndicator));
                }

                RiskIndicators = riskIndicators.ToArray();
            }

            if (customComprehensiveVerificationStruct.PotentialFollowupActions != null)
            {
                List<IRiskIndicator> followUpActions = new List<IRiskIndicator>();
                foreach (ServiceReference.RiskIndicator followUpAction in customComprehensiveVerificationStruct.PotentialFollowupActions)
                {
                    followUpActions.Add(new RiskIndicator(followUpAction));
                }

                PotentialFollowupActions = followUpActions.ToArray();
            }

            Name = customComprehensiveVerificationStruct.Name;
        }

		public int ComprehensiveVerificationIndex { get; set; }
		public ISequencedRiskIndicator[] RiskIndicators { get; set; }
		public IRiskIndicator[] PotentialFollowupActions { get; set; }
		public string Name { get; set; }
	}
}
