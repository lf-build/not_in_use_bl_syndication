namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IInstantIdResult
	{
		 IInstantIdSearchBy InputEcho { get; set; }
		 string UniqueId { get; set; }
		 IVerifiedInput VerifiedInput { get; set; }
		 bool DateOfBirthVerified { get; set; }
		 int NameAddressSsnSummary { get; set; }
		 INameAddressPhone NameAddressPhone { get; set; }
		 IComprehensiveVerificationStruct ComprehensiveVerification { get; set; }
		 ICustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }
		 IInputCorrected InputCorrected { get; set; }
		 INewAreaCode NewAreaCode { get; set; }
		 IIdentityReversePhone ReversePhone { get; set; }
		 string PhoneOfNameAddress { get; set; }
		 ISsnInfo SsnInfo { get; set; }
		 IChronologyHistory[] ChronologyHistories { get; set; }
		 IWatchList[] WatchLists { get; set; }
		 string AdditionalScore1 { get; set; }
		 string AdditionalScore2 { get; set; }
		 IName CurrentName { get; set; }
		 IAdditionalLastName[] AdditionalLastNames { get; set; }
		 IModelSequenced[] Models { get; set; }
		 IRedFlagsReport RedFlagsReport { get; set; }
		 bool PassportValidated { get; set; }
		 int FoundSsnCount { get; set; }
		 IDecedentInfo DecedentInfo { get; set; }
		 int DateOfBirthMatchLevel { get; set; }
		 bool SsnFoundForLexId { get; set; }
		 bool AddressPoBox { get; set; }
		 bool AddressCmra { get; set; }
		 string InstantIdVersion { get; set; }
	}
}
