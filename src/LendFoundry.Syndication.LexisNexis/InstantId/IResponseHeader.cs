namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IResponseHeader
	{
		 int Status { get; set; }
		 string Message { get; set; }
		 string QueryId { get; set; }
		 string TransactionId { get; set; }
		 IWsException[] Exceptions { get; set; }
	}
}
