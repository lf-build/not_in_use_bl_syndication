namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IModelOption
	{
		 string OptionName { get; set; }
		 string OptionValue { get; set; }
	}
}
