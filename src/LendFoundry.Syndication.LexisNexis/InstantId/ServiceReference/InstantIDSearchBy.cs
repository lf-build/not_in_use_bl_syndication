﻿namespace LendFoundry.Syndication.LexisNexis.InstantId.ServiceReference
{
    public partial class InstantIDSearchBy
    {
        public InstantIDSearchBy()
        {
        }

        public InstantIDSearchBy(IInstantIdRequest searchBy)
        {
            if (searchBy == null)
                return;

            if (searchBy.Name != null)
                Name = new Name
                {
                    First = searchBy.Name.First,
                    Full = searchBy.Name.Full,
                    Last = searchBy.Name.Last,
                    Middle = searchBy.Name.Middle,
                    Prefix = searchBy.Name.Prefix,
                    Suffix = searchBy.Name.Suffix
                };


            if (searchBy.Address != null)
                Address = new Address
                {
                    City = searchBy.Address.City,
                    County = searchBy.Address.County,
                    PostalCode = searchBy.Address.PostalCode,
                    State = searchBy.Address.State,
                    StateCityZip = searchBy.Address.StateCityZip,
                    StreetAddress1 = searchBy.Address.StreetAddress1,
                    StreetAddress2 = searchBy.Address.StreetAddress2,
                    StreetName = searchBy.Address.StreetName,
                    StreetNumber = searchBy.Address.StreetNumber,
                    StreetPostDirection = searchBy.Address.StreetPostDirection,
                    StreetPreDirection = searchBy.Address.StreetPreDirection,
                    StreetSuffix = searchBy.Address.StreetSuffix,
                    UnitDesignation = searchBy.Address.UnitDesignation,
                    UnitNumber = searchBy.Address.UnitNumber,
                    Zip4 = searchBy.Address.Zip4,
                    Zip5 = searchBy.Address.Zip5
                };

            if (searchBy.DateOfBirth != null)
                DOB = new Date
                {
                    Day = searchBy.DateOfBirth.Day,
                    Month = searchBy.DateOfBirth.Month,
                    Year = searchBy.DateOfBirth.Year,
                };
            
            Age = searchBy.Age;
            SSN = searchBy.SocialSecurityNumber;
            SSNLast4 = searchBy.SocialSecurityNumberLast4;
            DriverLicenseNumber = searchBy.DriverLicenseNumber;
            DriverLicenseState = searchBy.DriverLicenseState;
            IPAddress = searchBy.IpAddress;
            HomePhone = searchBy.HomePhone;
            WorkPhone = searchBy.WorkPhone;
            UseDOBFilter = searchBy.UseDateOfBirthFilter;
            DOBRadius = searchBy.DateOfBirthRadius;

            Passport = new Passport();
            if (searchBy.Passport != null)
                Passport = new Passport
                {
                    Number = searchBy.Passport.Number,
                    Country = searchBy.Passport.Country,
                    MachineReadableLine1 = searchBy.Passport.MachineReadableLine1,
                    MachineReadableLine2 = searchBy.Passport.MachineReadableLine2
                };

            if (searchBy.Passport != null && searchBy.Passport.ExpirationDate != null)
                Passport.ExpirationDate = new Date
                {
                    Day = searchBy.Passport.ExpirationDate.Day,
                    Month = searchBy.Passport.ExpirationDate.Month,
                    Year = searchBy.Passport.ExpirationDate.Year,
                };


            Gender = searchBy.Gender;
            Email = searchBy.Email;
            Channel = (ChannelIdentifier)(int)searchBy.Channel;
            Income = searchBy.Income;
            OwnOrRent = (OwnRent)(int)searchBy.OwnOrRent;
            LocationIdentifier = searchBy.LocationIdentifier;
            OtherApplicationIdentifier1 = searchBy.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = searchBy.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = searchBy.OtherApplicationIdentifier3;

            if (searchBy.ApplicationDateTime != null)
                ApplicationDateTime = new TimeStamp
                {
                    Day = searchBy.ApplicationDateTime.Day,
                    Hour24 = searchBy.ApplicationDateTime.Hour24,
                    Minute = searchBy.ApplicationDateTime.Minute,
                    Month = searchBy.ApplicationDateTime.Month,
                    Second = searchBy.ApplicationDateTime.Second,
                    Year = searchBy.ApplicationDateTime.Year
                };
        }
    }
}