﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId.ServiceReference
{
    public partial class InstantIDOption2
    {
        public InstantIDOption2(IInstantIdOption2 instantIdOption2)
        {
            if (instantIdOption2 == null)
                return;

            WatchLists = instantIdOption2.WatchLists;
            IncludeCLOverride = instantIdOption2.IncludeClOverride;
            IncludeMSOverride = instantIdOption2.IncludeMsOverride;
            IncludeDLVerification = instantIdOption2.IncludeDrivingLicenseVerification;
            PoBoxCompliance = instantIdOption2.PoBoxCompliance;

            if (instantIdOption2.IncludeModels != null)
            {
                IncludeModels = new InstantIDModelsFP2
                {
                    Thindex = instantIdOption2.IncludeModels.Thindex
                };

                if (instantIdOption2.IncludeModels.FraudPointModel != null)
                {
                    IncludeModels.FraudPointModel = new FraudPointModelWithOptions
                    {
                        IncludeRiskIndices = instantIdOption2.IncludeModels.FraudPointModel.IncludeRiskIndices,
                        ModelName = instantIdOption2.IncludeModels.FraudPointModel.ModelName
                    };
                }

                var modelRequests = new List<ModelRequest>();
                if (instantIdOption2.IncludeModels.ModelRequests != null)
                {
                    foreach (InstantId.ModelRequest sourceRequest in instantIdOption2.IncludeModels.ModelRequests)
                    {
                        var modelRequest = new ModelRequest { ModelName = sourceRequest.ModelName };
                        var modelOptions = new List<ModelOption>();
                        if (sourceRequest.ModelOptions != null)
                        {
                            foreach (InstantId.ModelOption sourceOption in sourceRequest.ModelOptions)
                            {
                                var modelOption = new ModelOption
                                {
                                    OptionName = sourceOption.OptionName,
                                    OptionValue = sourceOption.OptionValue
                                };

                                modelOptions.Add(modelOption);
                            }

                            modelRequest.ModelOptions = modelOptions.ToArray();
                        }

                        modelRequests.Add(modelRequest);
                    }

                    IncludeModels.ModelRequests = modelRequests.ToArray();
                }

                if (instantIdOption2.IncludeModels.StudentDefenderModel != null)
                {
                    IncludeModels.StudentDefenderModel = new StudentDefenderModel
                    {
                        IsStudentApplicant = instantIdOption2.IncludeModels.StudentDefenderModel.IsStudentApplicant,
                        StudentDefender = instantIdOption2.IncludeModels.StudentDefenderModel.StudentDefender
                    };
                }
            }
            
            RedFlagsReport = instantIdOption2.RedFlagsReport;
            GlobalWatchlistThreshold = instantIdOption2.GlobalWatchlistThreshold;

            if (instantIdOption2.DateOfBirthMatch != null)
            {
                DOBMatch = new DOBMatchOptions
                {
                    MatchType = (DOBMatchType)(int)instantIdOption2.DateOfBirthMatch.MatchType,
                    MatchYearRadius = instantIdOption2.DateOfBirthMatch.MatchYearRadius
                };
            }
            
            IncludeAllRiskIndicators = instantIdOption2.IncludeAllRiskIndicators;

            if (instantIdOption2.RequireExactMatch != null)
            {
                RequireExactMatch = new RequireExactMatchInstID
                {
                    FirstName = instantIdOption2.RequireExactMatch.FirstName,
                    FirstNameAllowNickname = instantIdOption2.RequireExactMatch.FirstNameAllowNickname,
                    HomePhone = instantIdOption2.RequireExactMatch.HomePhone,
                    LastName = instantIdOption2.RequireExactMatch.LastName,
                    SSN = instantIdOption2.RequireExactMatch.Ssn
                };
            }

            CustomCVIModelName = instantIdOption2.CustomCviModelName;
            LastSeenThreshold = instantIdOption2.LastSeenThreshold;
            IncludeMIOverride = instantIdOption2.IncludeMiOverride;

            if (instantIdOption2.CviCalculationOptions != null)
            {
                CVICalculationOptions = new OptionsForCVICalculation
                {
                    DisableCustomerNetworkOption = instantIdOption2.CviCalculationOptions.DisableCustomerNetworkOption,
                    IncludeDOB = instantIdOption2.CviCalculationOptions.IncludeDateOfBirth,
                    IncludeDriverLicense = instantIdOption2.CviCalculationOptions.IncludeDriverLicense
                };
            }

            InstantIDVersion = instantIdOption2.InstantIdVersion;
            IncludeDeliveryPointBarcode = instantIdOption2.IncludeDeliveryPointBarcode;
        }
    }
}