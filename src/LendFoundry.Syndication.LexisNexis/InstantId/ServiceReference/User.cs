﻿namespace LendFoundry.Syndication.LexisNexis.InstantId.ServiceReference
{
    public partial class User
    {
        public User()
        {

        }

        public User(LendFoundry.Syndication.LexisNexis.InstantId.IUser user)
        {
            if (user == null)
                return;

            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GrammLeachBlileyPurpose;
            DLPurpose = user.DriverLicensePurpose;
            EndUser = new EndUserInfo();
            EndUser.City = user.EndUser.City;
            EndUser.CompanyName = user.EndUser.CompanyName;
            EndUser.State = user.EndUser.State;
            EndUser.StreetAddress1 = user.EndUser.StreetAddress1;
            EndUser.Zip5 = user.EndUser.Zip5;
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }
    }
}
