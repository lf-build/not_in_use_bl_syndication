namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IOptionsForCviCalculation
	{
		 bool IncludeDateOfBirth { get; set; }
		 bool IncludeDriverLicense { get; set; }
		 bool DisableCustomerNetworkOption { get; set; }
	}
}
