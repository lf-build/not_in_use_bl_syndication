using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class ResponseHeader : IResponseHeader
	{
        public ResponseHeader(ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;

            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;

            if(responseHeader.Exceptions != null)
            {
                List<IWsException> exceptions = new List<IWsException>();
                foreach(ServiceReference.WsException exception in responseHeader.Exceptions)
                {
                    exceptions.Add(new WsException(exception));
                }
                Exceptions = exceptions.ToArray();
            }
        }

		public int Status { get; set; }
		public string Message { get; set; }
		public string QueryId { get; set; }
		public string TransactionId { get; set; }
		public IWsException[] Exceptions { get; set; }
	}
}
