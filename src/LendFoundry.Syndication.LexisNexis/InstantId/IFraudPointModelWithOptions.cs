namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IFraudPointModelWithOptions
	{
		 string ModelName { get; set; }
		 bool IncludeRiskIndices { get; set; }
	}
}
