﻿namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public enum DateOfBirthMatchType
    {
        FuzzyCCYYMMDD,
        FuzzyCCYYMM,
        RadiusCCYY,
        ExactCCYYMMDD,
        ExactCCYYMM
    }
}
