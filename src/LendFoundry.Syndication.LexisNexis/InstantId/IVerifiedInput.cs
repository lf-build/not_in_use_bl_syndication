namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IVerifiedInput
	{
		 IName Name { get; set; }
		 IAddressWithDpbc Address { get; set; }
		 string Ssn { get; set; }
		 string HomePhone { get; set; }
		 IMaskableDate DOB { get; set; }
		 string DriverLicenseNumber { get; set; }
	}
}
