namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IDecedentInfo
	{
		 IName Name { get; set; }
		 IDate DateOfDeath { get; set; }
		 IDate DateOfBirth { get; set; }
	}
}
