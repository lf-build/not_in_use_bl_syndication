namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class FraudPointModelWithOptions : IFraudPointModelWithOptions
    {
        public FraudPointModelWithOptions()
        {
        }

        public FraudPointModelWithOptions(ServiceReference.FraudPointModelWithOptions fraudPointModelWithOptions)
        {
            if (fraudPointModelWithOptions == null)
                return;

            ModelName = fraudPointModelWithOptions.ModelName;
            IncludeRiskIndices = fraudPointModelWithOptions.IncludeRiskIndices;
        }

        public string ModelName { get; set; }
        public bool IncludeRiskIndices { get; set; } = true;
    }
}