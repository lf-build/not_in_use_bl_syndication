using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class ScoreSequenced : IScoreSequenced
	{
        public ScoreSequenced(ServiceReference.ScoreSequenced score)
        {
            if (score == null)
                return;

            Type = score.Type;
            Value = score.Value;
            if(score.RiskIndices != null)
            {
                List<INameValuePair> riskIndices = new List<INameValuePair>();
                foreach(ServiceReference.NameValuePair risk in score.RiskIndices)
                {
                    riskIndices.Add(new NameValuePair(risk));
                }
                RiskIndices = riskIndices.ToArray();
            }
            if(score.RiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach(ServiceReference.SequencedRiskIndicator risk in score.RiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(risk));
                }
                RiskIndicators = riskIndicators.ToArray();
            }
        }

		public string Type { get; set; }
		public int Value { get; set; }
		public INameValuePair[] RiskIndices { get; set; }
		public ISequencedRiskIndicator[] RiskIndicators { get; set; }
	}
}
