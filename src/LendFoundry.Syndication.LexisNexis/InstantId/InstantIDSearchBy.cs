namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class InstantIdSearchBy : IInstantIdSearchBy
	{
        public InstantIdSearchBy()
        {

        }

        public InstantIdSearchBy(ServiceReference.InstantIDSearchBy instantIdSearchBy)
        {
            if (instantIdSearchBy == null)
                return;

            Name = new Name(instantIdSearchBy.Name);
            Address = new Address(instantIdSearchBy.Address);
            DateOfBirth = new Date(instantIdSearchBy.DOB);
            Age = instantIdSearchBy.Age;
            Ssn = instantIdSearchBy.SSN;
            SsnLast4 = instantIdSearchBy.SSNLast4;
            DriverLicenseNumber = instantIdSearchBy.DriverLicenseNumber;
            DriverLicenseState = instantIdSearchBy.DriverLicenseState;
            IpAddress = instantIdSearchBy.IPAddress;
            HomePhone = instantIdSearchBy.HomePhone;
            WorkPhone = instantIdSearchBy.WorkPhone;
            UseDateOfBirthFilter = instantIdSearchBy.UseDOBFilter;
            DateOfBirthRadius = instantIdSearchBy.DOBRadius;
            Passport = new Passport(instantIdSearchBy.Passport);
            Gender = instantIdSearchBy.Gender;
            Email = instantIdSearchBy.Email;
            Channel = (ChannelIdentifier)(int)instantIdSearchBy.Channel;
            Income = instantIdSearchBy.Income;
            OwnOrRent = (OwnRent)(int)instantIdSearchBy.OwnOrRent;
            LocationIdentifier = instantIdSearchBy.LocationIdentifier;
            OtherApplicationIdentifier1 = instantIdSearchBy.OtherApplicationIdentifier1;
            OtherApplicationIdentifier2 = instantIdSearchBy.OtherApplicationIdentifier2;
            OtherApplicationIdentifier3 = instantIdSearchBy.OtherApplicationIdentifier3;
            ApplicationDateTime = new TimeStamp(instantIdSearchBy.ApplicationDateTime);
        }


		public IName Name { get; set; }
		public IAddress Address { get; set; }
		public IDate DateOfBirth { get; set; }
		public int Age { get; set; }
		public string Ssn { get; set; }
		public string SsnLast4 { get; set; }
		public string DriverLicenseNumber { get; set; }
		public string DriverLicenseState { get; set; }
		public string IpAddress { get; set; }
		public string HomePhone { get; set; }
		public string WorkPhone { get; set; }
		public bool UseDateOfBirthFilter { get; set; }
		public int DateOfBirthRadius { get; set; }
		public IPassport Passport { get; set; }
		public string Gender { get; set; }
		public string Email { get; set; }
		public ChannelIdentifier Channel { get; set; }
		public string Income { get; set; }
		public OwnRent OwnOrRent { get; set; }
		public string LocationIdentifier { get; set; }
		public string OtherApplicationIdentifier1 { get; set; }
		public string OtherApplicationIdentifier2 { get; set; }
		public string OtherApplicationIdentifier3 { get; set; }
		public ITimeStamp ApplicationDateTime { get; set; }
	}
}
