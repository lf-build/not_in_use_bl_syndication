namespace LendFoundry.Syndication.LexisNexis.InstantId
{
    public class InstantIdOption2 : IInstantIdOption2
    {
        public InstantIdOption2()
        {
        }

        public InstantIdOption2(ServiceReference.InstantIDOption2 instantIdOption2)
        {
            if (instantIdOption2 == null)
                return;

            WatchLists = instantIdOption2.WatchLists;
            IncludeClOverride = instantIdOption2.IncludeCLOverride;
            IncludeMsOverride = instantIdOption2.IncludeMSOverride;
            IncludeDrivingLicenseVerification = instantIdOption2.IncludeDLVerification;
            PoBoxCompliance = instantIdOption2.PoBoxCompliance;
            IncludeModels = new InstantIdModelsFp2(instantIdOption2.IncludeModels);
            RedFlagsReport = instantIdOption2.RedFlagsReport;
            GlobalWatchlistThreshold = instantIdOption2.GlobalWatchlistThreshold;
            DateOfBirthMatch = new DateOfBirthMatchOptions(instantIdOption2.DOBMatch);
            IncludeAllRiskIndicators = instantIdOption2.IncludeAllRiskIndicators;
            RequireExactMatch = new RequireExactMatchInstId(instantIdOption2.RequireExactMatch);
            CustomCviModelName = instantIdOption2.CustomCVIModelName;
            LastSeenThreshold = instantIdOption2.LastSeenThreshold;
            IncludeMiOverride = instantIdOption2.IncludeMIOverride;
            CviCalculationOptions = new OptionsForCviCalculation(instantIdOption2.CVICalculationOptions);
            InstantIdVersion = instantIdOption2.InstantIDVersion;
            IncludeDeliveryPointBarcode = instantIdOption2.IncludeDeliveryPointBarcode;
        }

        public string[] WatchLists { get; set; }
        public bool IncludeClOverride { get; set; } = true;
        public bool IncludeMsOverride { get; set; } = true;
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        public bool PoBoxCompliance { get; set; } = true;
        public IInstantIdModelsFp2 IncludeModels { get; set; }
        public string RedFlagsReport { get; set; } = "Version1";
        public string GlobalWatchlistThreshold { get; set; }
        public IDateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        public bool IncludeAllRiskIndicators { get; set; } = true;
        public IRequireExactMatchInstId RequireExactMatch { get; set; }
        public string CustomCviModelName { get; set; }
        public string LastSeenThreshold { get; set; }
        public bool IncludeMiOverride { get; set; } = true;
        public IOptionsForCviCalculation CviCalculationOptions { get; set; }
        public string InstantIdVersion { get; set; }
        public bool IncludeDeliveryPointBarcode { get; set; } = true;
    }
}