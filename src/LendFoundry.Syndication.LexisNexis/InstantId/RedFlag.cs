using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class RedFlag : IRedFlag
	{
        public RedFlag(ServiceReference.RedFlag redFlag)
        {
            if (redFlag == null)
                return;

            Name = redFlag.Name;
            if(redFlag.HighRiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach(ServiceReference.SequencedRiskIndicator riskIndicator in redFlag.HighRiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(riskIndicator));
                }
                HighRiskIndicators = riskIndicators.ToArray();
            }
        }

		public string Name { get; set; }
		public ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
	}
}
