namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class AddressWithDpbc : IAddressWithDpbc
	{
        public AddressWithDpbc(ServiceReference.AddressWithDPBC addressWithDpbc)
        {
            if (addressWithDpbc == null)
                return;

            StreetNumber = addressWithDpbc.StreetNumber;
            StreetPreDirection = addressWithDpbc.StreetPreDirection;
            StreetName = addressWithDpbc.StreetName;
            StreetSuffix = addressWithDpbc.StreetSuffix;
            StreetPostDirection = addressWithDpbc.StreetPostDirection;
            UnitDesignation = addressWithDpbc.UnitDesignation;
            UnitNumber = addressWithDpbc.UnitNumber;
            StreetAddress1 = addressWithDpbc.StreetAddress1;
            StreetAddress2 = addressWithDpbc.StreetAddress2;
            City = addressWithDpbc.City;
            State = addressWithDpbc.State;
            Zip5 = addressWithDpbc.Zip5;
            Zip4 = addressWithDpbc.Zip4;
            County = addressWithDpbc.County;
            PostalCode = addressWithDpbc.PostalCode;
            StateCityZip = addressWithDpbc.StateCityZip;
            DeliveryPointBarcode = addressWithDpbc.DeliveryPointBarcode;
        }

		public string StreetNumber { get; set; }
		public string StreetPreDirection { get; set; }
		public string StreetName { get; set; }
		public string StreetSuffix { get; set; }
		public string StreetPostDirection { get; set; }
		public string UnitDesignation { get; set; }
		public string UnitNumber { get; set; }
		public string StreetAddress1 { get; set; }
		public string StreetAddress2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip5 { get; set; }
		public string Zip4 { get; set; }
		public string County { get; set; }
		public string PostalCode { get; set; }
		public string StateCityZip { get; set; }
		public string DeliveryPointBarcode { get; set; }
	}
}
