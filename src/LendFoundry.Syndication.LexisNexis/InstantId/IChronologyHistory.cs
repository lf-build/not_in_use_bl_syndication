namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IChronologyHistory
	{
		 IAddressWithDpbc Address { get; set; }
		 string Phone { get; set; }
		 IDate DateFirstSeen { get; set; }
		 IDate DateLastSeen { get; set; }
		 bool IsBestAddress { get; set; }
	}
}
