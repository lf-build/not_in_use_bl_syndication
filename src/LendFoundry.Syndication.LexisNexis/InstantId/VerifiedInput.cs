namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class VerifiedInput : IVerifiedInput
	{
        public VerifiedInput(ServiceReference.VerifiedInput verifiedInput)
        {
            if (verifiedInput == null)
                return;

            Name = new Name(verifiedInput.Name);
            Address = new AddressWithDpbc(verifiedInput.Address);
            Ssn = verifiedInput.SSN;
            HomePhone = verifiedInput.HomePhone;
            DOB = new MaskableDate(verifiedInput.DOB);
            DriverLicenseNumber = verifiedInput.DriverLicenseNumber;
        }

		public IName Name { get; set; }
		public IAddressWithDpbc Address { get; set; }
		public string Ssn { get; set; }
		public string HomePhone { get; set; }
		public IMaskableDate DOB { get; set; }
		public string DriverLicenseNumber { get; set; }
	}
}
