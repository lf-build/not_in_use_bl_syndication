namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface IInstantIdSearchBy
	{
        IName Name { get; set; }
        IAddress Address { get; set; }
        IDate DateOfBirth { get; set; }
        int Age { get; set; }
        string Ssn { get; set; }
        string SsnLast4 { get; set; }
        string DriverLicenseNumber { get; set; }
        string DriverLicenseState { get; set; }
        string IpAddress { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        bool UseDateOfBirthFilter { get; set; }
        int DateOfBirthRadius { get; set; }
        IPassport Passport { get; set; }
        string Gender { get; set; }
        string Email { get; set; }
        ChannelIdentifier Channel { get; set; }
        string Income { get; set; }
        OwnRent OwnOrRent { get; set; }
        string LocationIdentifier { get; set; }
        string OtherApplicationIdentifier1 { get; set; }
        string OtherApplicationIdentifier2 { get; set; }
        string OtherApplicationIdentifier3 { get; set; }
        ITimeStamp ApplicationDateTime { get; set; }
    }
}
