namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class DecedentInfo : IDecedentInfo
	{
        public DecedentInfo(ServiceReference.DecedentInfo decedentInfo)
        {
            if (decedentInfo == null)
                return;

            Name = new Name(decedentInfo.Name);
            DateOfDeath = new Date(decedentInfo.DOD);
            DateOfBirth = new Date(decedentInfo.DOB);
        }

		public IName Name { get; set; }
		public IDate DateOfDeath { get; set; }
		public IDate DateOfBirth { get; set; }
	}
}
