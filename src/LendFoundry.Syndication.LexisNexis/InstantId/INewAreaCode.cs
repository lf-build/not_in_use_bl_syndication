namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface INewAreaCode
	{
		 string AreaCode { get; set; }
		 IDate EffectiveDate { get; set; }
	}
}
