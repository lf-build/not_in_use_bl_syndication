namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public interface ITimeStamp
	{
		 short Year { get; set; }
		 short Month { get; set; }
		 short Day { get; set; }
		 short Hour24 { get; set; }
		 short Minute { get; set; }
		 short Second { get; set; }
	}
}
