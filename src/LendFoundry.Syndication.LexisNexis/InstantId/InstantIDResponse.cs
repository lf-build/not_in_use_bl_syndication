namespace LendFoundry.Syndication.LexisNexis.InstantId
{
	public class InstantIdResponse : IInstantIdResponse
	{
        public InstantIdResponse(ServiceReference.InstantIDResponse instantIdResponse)
        {
            if (instantIdResponse == null)
                return;

            Header = new ResponseHeader(instantIdResponse.Header);
            Result = new InstantIdResult(instantIdResponse.Result);
        }

		public IResponseHeader Header { get; set; }
		public IInstantIdResult Result { get; set; }
	}
}
