using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IResponseHeader
	{
		 int Status { get; set; }
		 string Message { get; set; }
		 string QueryId { get; set; }
		 string TransactionId { get; set; }
		 List<IWsException> Exceptions { get; set; }
	}
}
