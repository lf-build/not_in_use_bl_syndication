namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IUltimateRecord
	{
		 IBusinessIdentity BusinessIds { get; set; }
		 ICompanyNameInfo CompanyNameInfo { get; set; }
		 IAddressInfo AddressInfo { get; set; }
		 IAlsoFound AlsoFound { get; set; }
		 string BusinessId { get; set; }
	}
}
