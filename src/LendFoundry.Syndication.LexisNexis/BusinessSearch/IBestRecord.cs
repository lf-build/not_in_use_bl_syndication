namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IBestRecord
	{
		 bool ParentCompanyIndicator { get; set; }
		 bool IsDefunct { get; set; }
		 bool IsActive { get; set; }
		 ICompanyNameInfo CompanyNameInfo { get; set; }
		 IAddressInfo AddressInfo { get; set; }
		 IPhoneInfo PhoneInfo { get; set; }
		 string ListingType { get; set; }
		 bool ActiveEda { get; set; }
		 bool Disconnected { get; set; }
		 string WirelessIndicator { get; set; }
		 IDate AddressFromDate { get; set; }
		 IDate AddressToDate { get; set; }
		 IDate FromDate { get; set; }
		 IDate ToDate { get; set; }
		 IDate LastReported { get; set; }
		 IUrlInfo UrlInfo { get; set; }
		 IEmailInfo EmailInfo { get; set; }
		 ITinInfo TinInfo { get; set; }
		 ISicInfo SicInfo { get; set; }
		 IContactInfo ContactInfo { get; set; }
		 IAlsoFound AlsoFound { get; set; }
	}
}
