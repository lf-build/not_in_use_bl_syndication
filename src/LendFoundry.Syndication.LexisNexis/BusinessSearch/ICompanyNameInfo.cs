namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ICompanyNameInfo
	{
		 string CompanyName { get; set; }
	}
}
