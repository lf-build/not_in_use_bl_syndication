using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class SearchRecord : ISearchRecord
    {
        public SearchRecord()
        { }
        public SearchRecord(ServiceReference.TopBusinessSearchRecord searchRecord)
        {
            if (searchRecord == null)
                return;
            BusinessIds = new BusinessIdentity(searchRecord.BusinessIds);
            DisplayReportLink = searchRecord.DisplayReportLink;
            Best = new BestRecord(searchRecord.Best);
            if(searchRecord.Matches != null)
            {
                Matches = new List<IMatchRecord>(searchRecord.Matches.Select(match => new MatchRecord(match)));
            }
            MoreMatchRecords = searchRecord.MoreMatchRecords;
            Ultimate = new UltimateRecord(searchRecord.Ultimate);
            BusinessId = searchRecord.BusinessId;
            if(searchRecord.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(searchRecord.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            IsLafn = searchRecord.IsLAFN;
            IsTruncated = searchRecord.IsTruncated;
            DnbdmiRecordOnly = searchRecord.DNBDMIRecordOnly;
        }
		public IBusinessIdentity BusinessIds { get; set; }
		public bool DisplayReportLink { get; set; }
		public IBestRecord Best { get; set; }
		public List<IMatchRecord> Matches { get; set; }
		public bool MoreMatchRecords { get; set; }
		public IUltimateRecord Ultimate { get; set; }
		public string BusinessId { get; set; }
		public List<ISourceDocInfo> SourceDocs { get; set; }
		public bool IsLafn { get; set; }
		public bool IsTruncated { get; set; }
		public bool DnbdmiRecordOnly { get; set; }
	}
}
