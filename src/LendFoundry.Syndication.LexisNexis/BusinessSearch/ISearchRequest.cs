﻿namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
    public interface ISearchRequest
    {
        string CompanyName { get; set; }
        IAddress Address { get; set; }
        int Radius { get; set; }
        string Phone10 { get; set; }
        string Tin { get; set; }
        string Ssn { get; set; }
        string Url { get; set; }
        string Email { get; set; }
        IName Name { get; set; }
        string Sic { get; set; }
        long SeleId { get; set; }
    }
}
