namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class BusinessIdentity : IBusinessIdentity
    {
        public BusinessIdentity()
        { }
        public BusinessIdentity(ServiceReference.BusinessIdentity businessIdentity)
        {
            if (businessIdentity == null)
                return;
            DotId = businessIdentity.DotID;
            EmpId = businessIdentity.EmpID;
            PowId = businessIdentity.POWID;
            ProxId = businessIdentity.ProxID;
            SeleId = businessIdentity.SeleID;
            OrgId = businessIdentity.OrgID;
            UltId = businessIdentity.UltID;
        }
        public long DotId { get; set; }
		public long EmpId { get; set; }
		public long PowId { get; set; }
		public long ProxId { get; set; }
		public long SeleId { get; set; }
		public long OrgId { get; set; }
		public long UltId { get; set; }
	}
}
