namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class AddressInfo : IAddressInfo
	{
        public AddressInfo()
        { }
        public AddressInfo(ServiceReference.TopBusinessAddressInfo addressInfo)
        {
            if (addressInfo == null)
                return;
            Address = new Address(addressInfo.Address);
            Location = new Address(addressInfo.Location);
        }
		public IAddress Address { get; set; }
		public IAddress Location { get; set; }
	}
}
