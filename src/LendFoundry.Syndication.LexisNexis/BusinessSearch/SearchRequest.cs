﻿namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
    public class SearchRequest : ISearchRequest
    {
        public SearchRequest()
        { }
        public SearchRequest(ISearchRequest searchRequest)
        {
            CompanyName = searchRequest.CompanyName;
            Address = new Address
            {
                City = searchRequest.Address.City,
                County = searchRequest.Address.County,
                PostalCode = searchRequest.Address.PostalCode,
                State = searchRequest.Address.State,
                StateCityZip = searchRequest.Address.StateCityZip,
                StreetAddress1 = searchRequest.Address.StreetAddress1,
                StreetAddress2 = searchRequest.Address.StreetAddress2,
                StreetName = searchRequest.Address.StreetName,
                StreetNumber = searchRequest.Address.StreetNumber,
                StreetPostDirection = searchRequest.Address.StreetPostDirection,
                StreetPreDirection = searchRequest.Address.StreetPreDirection,
                StreetSuffix = searchRequest.Address.StreetSuffix,
                UnitDesignation = searchRequest.Address.UnitDesignation,
                UnitNumber = searchRequest.Address.UnitNumber,
                Zip4 = searchRequest.Address.Zip4,
                Zip5 = searchRequest.Address.Zip5
            };
            Radius = searchRequest.Radius;
            Phone10 = searchRequest.Phone10;
            Tin = searchRequest.Tin;
            Ssn = searchRequest.Ssn;
            Url = searchRequest.Url;
            Email = searchRequest.Email;
            Name = new Name
            {
                First = searchRequest.Name.First,
                Full = searchRequest.Name.Full,
                Last = searchRequest.Name.Last,
                Middle = searchRequest.Name.Middle,
                Prefix = searchRequest.Name.Prefix,
                Suffix = searchRequest.Name.Suffix
            };
            Sic = searchRequest.Sic;
            SeleId = searchRequest.SeleId;

        }
        public string CompanyName { get; set; }
        public IAddress Address { get; set; }
        public int Radius { get; set; }
        public string Phone10 { get; set; }
        public string Tin { get; set; }
        public string Ssn { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
        public IName Name { get; set; }
        public string Sic { get; set; }
        public long SeleId { get; set; }
    }
}
