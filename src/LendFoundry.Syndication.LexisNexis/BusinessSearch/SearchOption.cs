namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class SearchOption : ISearchOption
	{
        public SearchOption()
        { }
        public SearchOption(ServiceReference.TopBusinessSearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            Allow7DigitMatch = searchOption.Allow7DigitMatch;
            HSort = searchOption.HSort;
        }
		public int ReturnCount { get; set; }
		public int StartingRecord { get; set; }
		public bool Allow7DigitMatch { get; set; }
		public bool HSort { get; set; }
	}
}
