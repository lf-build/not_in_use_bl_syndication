namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IBusinessIdentity
	{
		 long DotId { get; set; }
		 long EmpId { get; set; }
		 long PowId { get; set; }
		 long ProxId { get; set; }
		 long SeleId { get; set; }
		 long OrgId { get; set; }
		 long UltId { get; set; }
	}
}
