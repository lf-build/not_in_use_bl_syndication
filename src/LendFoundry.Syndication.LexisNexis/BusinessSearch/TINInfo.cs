namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class TinInfo : ITinInfo
	{
        public TinInfo()
        { }
        public TinInfo(ServiceReference.TopBusinessTINInfo tINInfo)
        {
            if (tINInfo == null)
                return;
            Tin = tINInfo.TIN;
            TinInfoMatch = tINInfo.TINInfoMatch;
            TinSource = tINInfo.TINSource;
        }
		public string Tin { get; set; }
		public bool TinInfoMatch { get; set; }
		public string TinSource { get; set; }
	}
}
