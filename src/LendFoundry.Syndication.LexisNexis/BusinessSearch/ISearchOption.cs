namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ISearchOption
	{
		 int ReturnCount { get; set; }
		 int StartingRecord { get; set; }
		 bool Allow7DigitMatch { get; set; }
		 bool HSort { get; set; }
	}
}
