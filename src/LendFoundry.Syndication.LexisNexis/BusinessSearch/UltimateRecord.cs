namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class UltimateRecord : IUltimateRecord
	{
        public UltimateRecord()
        { }
        public UltimateRecord(ServiceReference.TopBusinessUltimateRecord ultimateRecord)
        {
            if (ultimateRecord == null)
                return;
            BusinessIds = new BusinessIdentity(ultimateRecord.BusinessIds);
            CompanyNameInfo = new CompanyNameInfo(ultimateRecord.CompanyNameInfo);
            AddressInfo = new AddressInfo(ultimateRecord.AddressInfo);
            AlsoFound = new AlsoFound(ultimateRecord.AlsoFound);
            BusinessId = ultimateRecord.BusinessId;
        }
		public IBusinessIdentity BusinessIds { get; set; }
		public ICompanyNameInfo CompanyNameInfo { get; set; }
		public IAddressInfo AddressInfo { get; set; }
		public IAlsoFound AlsoFound { get; set; }
		public string BusinessId { get; set; }
	}
}
