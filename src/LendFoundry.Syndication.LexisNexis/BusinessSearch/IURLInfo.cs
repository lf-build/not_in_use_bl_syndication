namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IUrlInfo
	{
		 string Url { get; set; }
		 bool UrlInfoMatch { get; set; }
	}
}
