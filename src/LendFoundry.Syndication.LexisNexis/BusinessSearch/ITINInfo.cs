namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ITinInfo
	{
		 string Tin { get; set; }
		 bool TinInfoMatch { get; set; }
		 string TinSource { get; set; }
	}
}
