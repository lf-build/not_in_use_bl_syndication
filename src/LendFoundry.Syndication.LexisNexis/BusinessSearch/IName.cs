namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IName
	{
		 string Full { get; set; }
		 string First { get; set; }
		 string Middle { get; set; }
		 string Last { get; set; }
		 string Suffix { get; set; }
		 string Prefix { get; set; }
	}
}
