namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class BestRecord : IBestRecord
    {
        public BestRecord()
        { }
        public BestRecord(ServiceReference.TopBusinessBestRecord bestRecord)
        {
            if (bestRecord == null)
                return;
            ParentCompanyIndicator = bestRecord.ParentCompanyIndicator;
            IsDefunct = bestRecord.IsDefunct;
            IsActive = bestRecord.IsActive;
            CompanyNameInfo = new CompanyNameInfo(bestRecord.CompanyNameInfo);
            AddressInfo = new AddressInfo(bestRecord.AddressInfo);
            PhoneInfo = new PhoneInfo(bestRecord.PhoneInfo);
            ListingType = bestRecord.ListingType;
            ActiveEda = bestRecord.ActiveEDA;
            Disconnected = bestRecord.Disconnected;
            WirelessIndicator = bestRecord.WirelessIndicator;
            AddressFromDate = new Date(bestRecord.AddressFromDate);
            AddressToDate = new Date(bestRecord.AddressToDate);
            FromDate = new Date(bestRecord.FromDate);
            ToDate = new Date(bestRecord.ToDate);
            LastReported = new Date(bestRecord.LastReported);
            UrlInfo = new UrlInfo(bestRecord.URLInfo);
            EmailInfo = new EmailInfo(bestRecord.EmailInfo);
            TinInfo = new TinInfo(bestRecord.TINInfo);
            SicInfo = new SicInfo(bestRecord.SICInfo);
            ContactInfo = new ContactInfo(bestRecord.ContactInfo);
            AlsoFound = new AlsoFound(bestRecord.AlsoFound);
        }
		public bool ParentCompanyIndicator { get; set; }
		public bool IsDefunct { get; set; }
		public bool IsActive { get; set; }
		public ICompanyNameInfo CompanyNameInfo { get; set; }
		public IAddressInfo AddressInfo { get; set; }
		public IPhoneInfo PhoneInfo { get; set; }
		public string ListingType { get; set; }
		public bool ActiveEda { get; set; }
		public bool Disconnected { get; set; }
		public string WirelessIndicator { get; set; }
		public IDate AddressFromDate { get; set; }
		public IDate AddressToDate { get; set; }
		public IDate FromDate { get; set; }
		public IDate ToDate { get; set; }
		public IDate LastReported { get; set; }
		public IUrlInfo UrlInfo { get; set; }
		public IEmailInfo EmailInfo { get; set; }
		public ITinInfo TinInfo { get; set; }
		public ISicInfo SicInfo { get; set; }
		public IContactInfo ContactInfo { get; set; }
		public IAlsoFound AlsoFound { get; set; }
	}
}
