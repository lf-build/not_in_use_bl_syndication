namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IPhoneInfo
	{
		 string Phone10 { get; set; }
		 string PubNonpub { get; set; }
		 string ListingPhone10 { get; set; }
		 string ListingName { get; set; }
		 string TimeZone { get; set; }
		 string ListingTimeZone { get; set; }
	}
}
