namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IContactInfo
	{
		 string UniqueId { get; set; }
		 IName Name { get; set; }
		 string Title { get; set; }
		 bool ContactInfoMatch { get; set; }
		 string Ssn { get; set; }
	}
}
