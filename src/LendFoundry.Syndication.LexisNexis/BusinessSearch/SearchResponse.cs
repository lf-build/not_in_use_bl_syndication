using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class SearchResponse : ISearchResponse
	{
        public SearchResponse()
        { }
        public SearchResponse(ServiceReference.TopBusinessSearchResponse searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if(searchResponse.Records != null)
            {
                Records = new List<ISearchRecord>(searchResponse.Records.Select(record => new SearchRecord(record)));
            }
        }
		public IResponseHeader Header { get; set; }
		public int RecordCount { get; set; }
		public List<ISearchRecord> Records { get; set; }
	}
}
