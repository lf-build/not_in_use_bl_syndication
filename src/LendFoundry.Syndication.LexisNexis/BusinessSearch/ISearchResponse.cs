using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ISearchResponse
	{
		 IResponseHeader Header { get; set; }
		 int RecordCount { get; set; }
		 List<ISearchRecord> Records { get; set; }
	}
}
