namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ISicInfo
	{
		 string Sic { get; set; }
	}
}
