namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class SearchBy : ISearchBy
	{
        public SearchBy()
        { }
        public SearchBy(ServiceReference.TopBusinessSearchBy searchBy)
        {
            if (searchBy == null)
                return;
            CompanyName = searchBy.CompanyName;
            Address = new Address(searchBy.Address);
            Radius = searchBy.Radius;
            Phone10 = searchBy.Phone10;
            Tin = searchBy.TIN;
            Ssn = searchBy.SSN;
            Url = searchBy.URL;
            Email = searchBy.Email;
            Name = new Name(searchBy.Name);
            Sic = searchBy.SIC;
            SeleId = searchBy.SeleID;
        }
		public string CompanyName { get; set; }
		public IAddress Address { get; set; }
		public int Radius { get; set; }
		public string Phone10 { get; set; }
		public string Tin { get; set; }
		public string Ssn { get; set; }
		public string Url { get; set; }
		public string Email { get; set; }
		public IName Name { get; set; }
		public string Sic { get; set; }
		public long SeleId { get; set; }
	}
}
