namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IAddressInfo
	{
		 IAddress Address { get; set; }
		 IAddress Location { get; set; }
	}
}
