namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class MatchRecord : IMatchRecord
	{
        public MatchRecord()
        { }
        public MatchRecord(ServiceReference.TopBusinessMatchRecord matchRecord)
        {
            if (matchRecord == null)
                return;
            ParentCompanyIndicator = matchRecord.ParentCompanyIndicator;
            IsDefunct = matchRecord.IsDefunct;
            IsActive = matchRecord.IsActive;
            CompanyNameInfo = new CompanyNameInfo(matchRecord.CompanyNameInfo);
            AddressInfo = new AddressInfo(matchRecord.AddressInfo);
            PhoneInfo = new PhoneInfo(matchRecord.PhoneInfo);
            ListingType = matchRecord.ListingType;
            ActiveEda = matchRecord.ActiveEDA;
            Disconnected = matchRecord.Disconnected;
            WirelessIndicator = matchRecord.WirelessIndicator;
            AddressFromDate = new Date(matchRecord.AddressFromDate);
            AddressToDate = new Date(matchRecord.AddressToDate);
            FromDate = new Date(matchRecord.FromDate);
            ToDate = new Date(matchRecord.ToDate);
            LastReported = new Date(matchRecord.LastReported);
            UrlInfo = new UrlInfo(matchRecord.URLInfo);
            EmailInfo = new EmailInfo(matchRecord.EmailInfo);
            TinInfo = new TinInfo(matchRecord.TINInfo);
            SicInfo = new SicInfo(matchRecord.SICInfo);
            ContactInfo = new ContactInfo(matchRecord.ContactInfo);
            AlsoFound = new AlsoFound(matchRecord.AlsoFound);
            BusinessIds = new BusinessIdentity(matchRecord.BusinessIds);
        }
		public bool ParentCompanyIndicator { get; set; }
		public bool IsDefunct { get; set; }
		public bool IsActive { get; set; }
		public ICompanyNameInfo CompanyNameInfo { get; set; }
		public IAddressInfo AddressInfo { get; set; }
		public IPhoneInfo PhoneInfo { get; set; }
		public string ListingType { get; set; }
		public bool ActiveEda { get; set; }
		public bool Disconnected { get; set; }
		public string WirelessIndicator { get; set; }
		public IDate AddressFromDate { get; set; }
		public IDate AddressToDate { get; set; }
		public IDate FromDate { get; set; }
		public IDate ToDate { get; set; }
		public IDate LastReported { get; set; }
		public IUrlInfo UrlInfo { get; set; }
		public IEmailInfo EmailInfo { get; set; }
		public ITinInfo TinInfo { get; set; }
		public ISicInfo SicInfo { get; set; }
		public IContactInfo ContactInfo { get; set; }
		public IAlsoFound AlsoFound { get; set; }
		public IBusinessIdentity BusinessIds { get; set; }
	}
}
