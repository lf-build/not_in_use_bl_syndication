namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class SicInfo : ISicInfo
	{
        public SicInfo()
        { }
        public SicInfo(ServiceReference.TopBusinessSICInfo sicInfo)
        {
            if (sicInfo == null)
                return;
            Sic = sicInfo.SIC;
        }
		public string Sic { get; set; }
	}
}
