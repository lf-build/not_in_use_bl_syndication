namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class ContactInfo : IContactInfo
	{
        public ContactInfo()
        { }
        public ContactInfo(ServiceReference.TopBusinessContactInfo contactInfo)
        {
            if (contactInfo == null)
                return;
            UniqueId = contactInfo.UniqueId;
            Name = new Name(contactInfo.Name);
            Title = contactInfo.Title;
            ContactInfoMatch = contactInfo.ContactInfoMatch;
            Ssn = contactInfo.SSN;
        }
		public string UniqueId { get; set; }
		public IName Name { get; set; }
		public string Title { get; set; }
		public bool ContactInfoMatch { get; set; }
		public string Ssn { get; set; }
	}
}
