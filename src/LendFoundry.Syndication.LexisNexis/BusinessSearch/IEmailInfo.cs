namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IEmailInfo
	{
		 string Email { get; set; }
		 bool EmailInfoMatch { get; set; }
	}
}
