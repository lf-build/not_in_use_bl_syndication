using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class ResponseHeader : IResponseHeader
	{
        public ResponseHeader()
        { }
        public ResponseHeader(ServiceReference.ResponseHeader responseHeader)
        {
            if (responseHeader == null)
                return;
            Status = responseHeader.Status;
            Message = responseHeader.Message;
            QueryId = responseHeader.QueryId;
            TransactionId = responseHeader.TransactionId;
            if(responseHeader.Exceptions != null)
            {
                Exceptions = new List<IWsException>(responseHeader.Exceptions.Select(exception => new WsException(exception)));
            }
        }
		public int Status { get; set; }
		public string Message { get; set; }
		public string QueryId { get; set; }
		public string TransactionId { get; set; }
		public List<IWsException> Exceptions { get; set; }
	}
}
