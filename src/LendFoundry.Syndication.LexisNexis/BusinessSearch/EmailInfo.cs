namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class EmailInfo : IEmailInfo
	{
        public EmailInfo()
        { }
        public EmailInfo(ServiceReference.TopBusinessEmailInfo emailInfo)
        {
            if (emailInfo == null)
                return;
            Email = emailInfo.Email;
            EmailInfoMatch = emailInfo.EmailInfoMatch;
        }
		public string Email { get; set; }
		public bool EmailInfoMatch { get; set; }
	}
}
