namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class AlsoFound : IAlsoFound
    {
        public AlsoFound()
        { }
        public AlsoFound(ServiceReference.TopBusinessAlsoFound alsoFound)
        {
            if (alsoFound == null)
                return;
            Incorporations = alsoFound.Incorporations;
            UcCs = alsoFound.UCCs;
            Properties = alsoFound.Properties;
            MvRs = alsoFound.MVRs;
            Contacts = alsoFound.Contacts;
        }
		public bool Incorporations { get; set; }
		public bool UcCs { get; set; }
		public bool Properties { get; set; }
		public bool MvRs { get; set; }
		public bool Contacts { get; set; }
	}
}
