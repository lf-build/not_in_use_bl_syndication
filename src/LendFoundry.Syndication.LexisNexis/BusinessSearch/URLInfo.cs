namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class UrlInfo : IUrlInfo
	{
        public UrlInfo()
        { }
        public UrlInfo(ServiceReference.TopBusinessURLInfo uRLInfo)
        {
            if (uRLInfo == null)
                return;
            Url = uRLInfo.URL;
            UrlInfoMatch = uRLInfo.UrlInfoMatch;
        }
		public string Url { get; set; }
		public bool UrlInfoMatch { get; set; }
	}
}
