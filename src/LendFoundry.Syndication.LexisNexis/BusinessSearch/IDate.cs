namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IDate
	{
		 short Year { get; set; }
		 short Month { get; set; }
		 short Day { get; set; }
	}
}
