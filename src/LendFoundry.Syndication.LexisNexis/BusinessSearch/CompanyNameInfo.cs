namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class CompanyNameInfo : ICompanyNameInfo
	{
        public CompanyNameInfo()
        { }
        public CompanyNameInfo(ServiceReference.TopBusinessCompanyNameInfo companyNameInfo)
        {
            if (companyNameInfo == null)
                return;
            CompanyName = companyNameInfo.CompanyName;
        }
		public string CompanyName { get; set; }
	}
}
