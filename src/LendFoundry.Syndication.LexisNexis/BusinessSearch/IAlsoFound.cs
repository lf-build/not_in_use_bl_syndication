namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface IAlsoFound
	{
		 bool Incorporations { get; set; }
		 bool UcCs { get; set; }
		 bool Properties { get; set; }
		 bool MvRs { get; set; }
		 bool Contacts { get; set; }
	}
}
