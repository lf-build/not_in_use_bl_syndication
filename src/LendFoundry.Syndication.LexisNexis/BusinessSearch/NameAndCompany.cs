namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public class NameAndCompany : INameAndCompany
	{
        public NameAndCompany()
        { }
        public NameAndCompany(ServiceReference.NameAndCompany nameAndCompany)
        {
            if (nameAndCompany == null)
                return;
            Full = nameAndCompany.Full;
            First = nameAndCompany.First;
            Middle = nameAndCompany.Middle;
            Last = nameAndCompany.Last;
            Suffix = nameAndCompany.Suffix;
            Prefix = nameAndCompany.Prefix;
            CompanyName = nameAndCompany.CompanyName;
        }
		public string Full { get; set; }
		public string First { get; set; }
		public string Middle { get; set; }
		public string Last { get; set; }
		public string Suffix { get; set; }
		public string Prefix { get; set; }
		public string CompanyName { get; set; }
	}
}
