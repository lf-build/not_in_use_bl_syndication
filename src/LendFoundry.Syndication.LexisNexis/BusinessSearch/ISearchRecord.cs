using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessSearch
{
	public interface ISearchRecord
	{
		 IBusinessIdentity BusinessIds { get; set; }
		 bool DisplayReportLink { get; set; }
		 IBestRecord Best { get; set; }
		 List<IMatchRecord> Matches { get; set; }
		 bool MoreMatchRecords { get; set; }
		 IUltimateRecord Ultimate { get; set; }
		 string BusinessId { get; set; }
		 List<ISourceDocInfo> SourceDocs { get; set; }
		 bool IsLafn { get; set; }
		 bool IsTruncated { get; set; }
		 bool DnbdmiRecordOnly { get; set; }
	}
}
