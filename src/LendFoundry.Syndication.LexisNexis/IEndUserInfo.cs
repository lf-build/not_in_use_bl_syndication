namespace LendFoundry.Syndication.LexisNexis
{
	public interface IEndUserInfo
	{
		 string CompanyName { get; set; }
		 string StreetAddress1 { get; set; }
		 string City { get; set; }
		 string State { get; set; }
		 string Zip5 { get; set; }
	}
}
