﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class KbaServiceConfiguration : InstantIdServiceConfiguration
    {
        //public string AccountName {get; set;}
        //public string RuleSet {get; set;} = "public.testing.redherrings";
        //public int Mode {get; set;} = (int)InstantIdKba.ModeType.Testing;
        //public int AccountCategory {get; set;} = (int)InstantIdKba.AccountCategoryType.Banking;
        //public string CustomerId {get; set;} = "90439049039-0349";

        [JsonConverter(typeof(InterfaceConverter<InstantIdKba.IIdentityVerificationSettingsType, InstantIdKba.IdentityVerificationSettingsType>))]
        public InstantIdKba.IIdentityVerificationSettingsType IdentityVerificationSettings { get; set; }

        [JsonConverter(typeof(InterfaceConverter<InstantIdKba.IContinueSettingsType, InstantIdKba.ContinueSettingsType>))]
        public InstantIdKba.IContinueSettingsType ContinueSettings { get; set; }
    }
    
}