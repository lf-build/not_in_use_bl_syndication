﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    public interface ICriminalRecordReportRequest
    {
        string UniqueId { get; set; }
        string QueryId { get; set; }
        string OwnerId { get; set; }
    }
}
