﻿

using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public interface ICriminalRecordProxy
    {
        Task<FcraCriminalSearchResponse> CriminalRecordSearch(CriminalSearch.ServiceReference.User user, FcraCriminalSearchBy searchBy, FcraCriminalSearchOption options);
        Task<FcraCriminalReportResponse> CriminalRecordReport(CriminalReport.ServiceReference.User user, FcraCriminalReportBy reportBy, FcraCriminalReportOption options);
    }
}   