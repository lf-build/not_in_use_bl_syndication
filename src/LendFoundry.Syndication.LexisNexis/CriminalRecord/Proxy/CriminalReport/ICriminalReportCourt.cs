﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportCourt
    {
         string CaseNumber { get; set; }

         string Costs { get; set; }

         string Description { get; set; }

         string Disposition { get; set; }

         IDate DispositionDate { get; set; }

         string Fine { get; set; }

         string Level { get; set; }

         string Offense { get; set; }

         string Plea { get; set; }

         string Statute { get; set; }

         string SuspendedFine { get; set; }
    }
}
