﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportResponse
    {
        IResponseHeader Header { get; set; }
        int RecordCount { get; set; }
        List<ICriminalReportRecord> Records { get; set; }
    }
}
