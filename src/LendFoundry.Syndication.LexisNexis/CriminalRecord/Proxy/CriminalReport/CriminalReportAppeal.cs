﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportAppeal : ICriminalReportAppeal
    {
        public CriminalReportAppeal()
        {
        }

        public CriminalReportAppeal(CrimReportAppeal record)
        {
            if (record == null)
                return;
            Date = record.Date != null ? new BusinessReport.Date { Day = record.Date.Day, Month = record.Date.Month, Year = record.Date.Year } : null;
            Disposition = record.Disposition;
            FinalDisposition = record.FinalDisposition;
        }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Date { get; set; }

        public string Disposition { get; set; }

        public string FinalDisposition { get; set; }
    }
}