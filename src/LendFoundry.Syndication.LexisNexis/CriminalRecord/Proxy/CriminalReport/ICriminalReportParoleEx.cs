﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
     public interface ICriminalReportParoleEx
    {
         IDate ActualEndDate { get; set; }

         string County { get; set; }

         string CurrentStatus { get; set; }

         string Length { get; set; }

         IDate ScheduledEndDate { get; set; }

         IDate StartDate { get; set; }

         IDate DateReported { get; set; }

         string PubicSaftyId { get; set; }

         string InmateId { get; set; }

         string ParoleInAbsentiaId { get; set; }

         IName Name { get; set; }

         string Race { get; set; }

         string Gender { get; set; }

         IDate Dob { get; set; }

         string CountyOfBirth { get; set; }

         string StateOfBirth { get; set; }

         int HeightFeet { get; set; }

         bool HeightFeetSpecified { get; set; }

        int HeightInches { get; set; }

         bool HeightInchesSpecified { get; set; }

        int WeightInPounds { get; set; }

         bool WeightInPoundsSpecified { get; set; }

        string HairColor { get; set; }

         string SkinColor { get; set; }

         string EyeColor { get; set; }

         List<string> ScarsMarksTattoos { get; set; }

         string PrisonFacilityType { get; set; }

         string PrisonFacilityName { get; set; }

         IDate AdmittedDate { get; set; }

         string PrisonStatus { get; set; }

         string LastReceiveOrDepartCode { get; set; }

         IDate LastReceiveOrDepartCDate { get; set; }

         TimeStamp RecordCreatedTimeStamp { get; set; }

         string CurrentStatusFlag { get; set; }

         IDate CurrentStatusEffectiveDate { get; set; }

         string ParoleRegion { get; set; }

         string SupervisingOffice { get; set; }

         string SupervisingOfficerName { get; set; }

         string SupervisingOfficerPhone { get; set; }

         IAddress LastKnownResidence { get; set; }

         IDate ReleaseArrivalDate { get; set; }

         string ReleaseType { get; set; }

         string ReleaseCounty { get; set; }

         string LegalResidenceCounty { get; set; }

         IDate LastParoleReviewDate { get; set; }

         int OffenseCount { get; set; }

         bool OffenseCountSpecified { get; set; }

         bool Is3GOffender { get; set; }

         bool Is3GOffenderSpecified { get; set; }

         bool IsViolentOffender { get; set; }

         bool IsViolentOffenderSpecified { get; set; }

         bool IsSexOffender { get; set; }

         bool IsSexOffenderSpecified { get; set; }

         bool IsOnViolentOffenderProgram { get; set; }

         bool IsOnViolentOffenderProgramSpecified { get; set; }

         Duration LongestTimeToServe { get; set; }

         string LongestTimeToServeDescription { get; set; }

         IDate DischargeDate { get; set; }

         List<ICriminalReportParoleOffense> Offenses { get; set; }
    }
}
