﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportParoleOffense
    {
         IDate SentenceDate { get; set; }

         int Length { get; set; }

         bool LengthSpecified { get; set; }

        string OffenseCounty { get; set; }

         string CauseNo { get; set; }

         string NcicCode { get; set; }

         int OffenseCount { get; set; }

         bool OffenseCountSpecified { get; set; }

        IDate OffenseDate { get; set; }
    }
}
