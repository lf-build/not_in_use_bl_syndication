﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportCourtSentence
    {
         string Jail { get; set; }

         string Probation { get; set; }

         string Suspended { get; set; }
    }
}
