﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference
{
    public partial class FcraCriminalReportBy
    {
        public FcraCriminalReportBy()
        { }
        public FcraCriminalReportBy(ICriminalRecordReportRequest reportBy)
        {
            if (reportBy == null)
                return;

            UniqueId = reportBy.UniqueId;

        }
    }
}
