﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference
{
    public partial class FcraCriminalReportOption
    {
       
        public FcraCriminalReportOption(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;

            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
