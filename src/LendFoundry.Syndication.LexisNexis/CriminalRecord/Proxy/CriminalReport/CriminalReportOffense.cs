﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportOffense : ICriminalReportOffense
    {
        public CriminalReportOffense()
        {
        }

        public CriminalReportOffense(CrimReportOffense offenseRecord)
        {
            if (offenseRecord == null)
                return;

            AdjudicationWithheld = offenseRecord.AdjudicationWithheld;
            CaseNumber = offenseRecord.CaseNumber;
            CaseType = offenseRecord.CaseType;
            CaseTypeDescription = offenseRecord.CaseTypeDescription;
            Count = offenseRecord.Count;
            County = offenseRecord.County;
            Description = offenseRecord.Description;
            MaximumTerm = offenseRecord.MaximumTerm;
            MinimumTerm = offenseRecord.MinimumTerm;
            NumberCounts = offenseRecord.NumberCounts;
            OffenseType = offenseRecord.OffenseType;
            Sentence = offenseRecord.Sentence;
            SentenceLengthDescription = offenseRecord.SentenceLengthDescription;
            OffenseDate = offenseRecord.OffenseDate != null ? new BusinessReport.Date { Day = offenseRecord.OffenseDate.Day, Month = offenseRecord.OffenseDate.Month, Year = offenseRecord.OffenseDate.Year } : null;
            SentenceDate = offenseRecord.SentenceDate != null ? new BusinessReport.Date { Day = offenseRecord.SentenceDate.Day, Month = offenseRecord.SentenceDate.Month, Year = offenseRecord.SentenceDate.Year } : null;
            Appeal = offenseRecord.Appeal != null ? new CriminalReportAppeal(offenseRecord.Appeal) : null;
            Arrest = offenseRecord.Arrest != null ? new CriminalReportArrest(offenseRecord.Arrest) : null;
            Court = offenseRecord.Court != null ? new CriminalReportCourt(offenseRecord.Court) : null;
            CourtSentence = offenseRecord.CourtSentence != null ? new CriminalReportCourtSentence(offenseRecord.CourtSentence) : null;
        }

        public string AdjudicationWithheld { get; set; }

        public string CaseNumber { get; set; }

        public string CaseType { get; set; }

        public string CaseTypeDescription { get; set; }

        public string Count { get; set; }

        public string County { get; set; }

        public string Description { get; set; }

        public string MaximumTerm { get; set; }

        public string MinimumTerm { get; set; }

        public string NumberCounts { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OffenseDate { get; set; }

        public string OffenseType { get; set; }

        public string Sentence { get; set; }

        public string SentenceLengthDescription { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate SentenceDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate IncarcerationDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICriminalReportAppeal, CriminalReportAppeal>))]
        public ICriminalReportAppeal Appeal { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICriminalReportArrest, CriminalReportArrest>))]
        public ICriminalReportArrest Arrest { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICriminalReportCourt, CriminalReportCourt>))]
        public ICriminalReportCourt Court { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICriminalReportCourtSentence, CriminalReportCourtSentence>))]
        public ICriminalReportCourtSentence CourtSentence { get; set; }
    }
}