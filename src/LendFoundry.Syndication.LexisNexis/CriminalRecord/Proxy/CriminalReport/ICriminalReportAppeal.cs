﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportAppeal
    {
        IDate Date { get; set; }
        string Disposition { get; set; }
        string FinalDisposition { get; set; }
    }
}