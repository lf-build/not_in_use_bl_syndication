﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportPrison
    {
         IDate AdmittedDate { get; set; }

         string CurrentStatus { get; set; }

         string CustodyType { get; set; }

         IDate CustodyTypeChangeDate { get; set; }

         string GainTimeGranted { get; set; }

         IDate LastGainTime { get; set; }

         string Location { get; set; }

         IDate ScheduledReleaseDate { get; set; }

         string Sentence { get; set; }
    }
}
