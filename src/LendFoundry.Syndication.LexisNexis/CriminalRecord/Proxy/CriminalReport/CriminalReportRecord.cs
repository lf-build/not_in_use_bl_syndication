﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportRecord : ICriminalReportRecord
    {
        public CriminalReportRecord()
        {
        }

        public CriminalReportRecord(CrimReportRecord record)
        {
            if (record == null)
                return;
            OffenderId = record.OffenderId;
            CaseNumber = record.CaseNumber;
            CountyOfOrigin = record.CountyOfOrigin;
            DocNumber = record.DOCNumber;
            CaseFilingDate = record.CaseFilingDate != null ? new BusinessReport.Date { Day = record.CaseFilingDate.Day, Month = record.CaseFilingDate.Month, Year = record.CaseFilingDate.Year } : null;
            Eyes = record.Eyes;
            Hair = record.Hair;
            Height = record.Height;
            Weight = record.Weight;
            Race = record.Race;
            Sex = record.Sex;
            Skin = record.Skin;
            DataSource = record.DataSource;
            Ssn = record.SSN;
            UniqueId = record.UniqueId;
            StateOfBirth = record.StateOfBirth;
            StateOfOrigin = record.StateOfOrigin;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;

            Status = record.Status;
            Address = new Address(record.Address);
            Name = new Name(record.Name);
            CaseTypeDescription = record.CaseTypeDescription;
            Akas = record.AKAs != null ? new List<IName>(record.AKAs.Select(aka => new Name(aka))) : null;

            Offenses = record.Offenses != null ? new List<ICriminalReportOffense>(record.Offenses.Select(offense => new CriminalReportOffense(offense))) : null;
            PrisonSentences = record.PrisonSentences != null ? new List<ICriminalReportPrison>(record.PrisonSentences.Select(sentence => new CriminalReportPrison(sentence))) : null;
            ParoleSentences = record.ParoleSentences != null ? new List<ICriminalReportParoleEx>(record.ParoleSentences.Select(sentence => new CriminalReportParoleEx(sentence))) : null;
            Activities = record.Activities != null ? new List<ICriminalReportEvent>(record.Activities.Select(activity => new CriminalReportEvent(activity))) : null;
        }

        public string OffenderId { get; set; }

        public string CaseNumber { get; set; }

        public string CountyOfOrigin { get; set; }

        public string DocNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CaseFilingDate { get; set; }

        public string Eyes { get; set; }

        public string Hair { get; set; }

        public string Height { get; set; }

        public string Weight { get; set; }

        public string Race { get; set; }

        public string Sex { get; set; }

        public string Skin { get; set; }

        public string DataSource { get; set; }

        public string Ssn { get; set; }

        public string UniqueId { get; set; }

        public string StateOfBirth { get; set; }

        public string StateOfOrigin { get; set; }

        public string Status { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        public string CaseTypeDescription { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IName, Name>))]
        public List<IName> Akas { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportOffense, CriminalReportOffense>))]
        public List<ICriminalReportOffense> Offenses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportPrison, CriminalReportPrison>))]
        public List<ICriminalReportPrison> PrisonSentences { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportParoleEx, CriminalReportParoleEx>))]
        public List<ICriminalReportParoleEx> ParoleSentences { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportEvent, CriminalReportEvent>))]
        public List<ICriminalReportEvent> Activities { get; set; }
    }
}