﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportArrest : ICriminalReportArrest
    {
        public CriminalReportArrest()
        {
        }

        public CriminalReportArrest(CrimReportArrest record)
        {
            if (record == null)
                return;
            Agency = record.Agency;
            CaseNumber = record.CaseNumber;
            Disposition = record.Disposition;
            Level = record.Level;
            Date = record.Date != null ? new BusinessReport.Date { Day = record.Date.Day, Month = record.Date.Month, Year = record.Date.Year } : null;
            DispositionDate = record.Date != null ? new BusinessReport.Date { Day = record.DispositionDate.Day, Month = record.DispositionDate.Month, Year = record.DispositionDate.Year } : null;
            Offense = record.Offense;
            Statute = record.Statute;
            Type = record.Type;
        }

        public string Agency { get; set; }

        public string CaseNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Date { get; set; }

        public string Disposition { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DispositionDate { get; set; }

        public string Level { get; set; }

        public string Offense { get; set; }

        public string Statute { get; set; }

        public string Type { get; set; }
    }
}