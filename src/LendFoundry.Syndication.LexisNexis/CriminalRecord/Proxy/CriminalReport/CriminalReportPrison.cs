﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportPrison : ICriminalReportPrison
    {
        public CriminalReportPrison()
        {
        }

        public CriminalReportPrison(CrimReportPrison record)
        {
            if (record == null)
                return;

            CurrentStatus = record.CurrentStatus;
            GainTimeGranted = record.GainTimeGranted;
            CustodyType = record.CustodyType;
            Location = record.Location;
            AdmittedDate = record.AdmittedDate != null ? new BusinessReport.Date { Day = record.AdmittedDate.Day, Month = record.AdmittedDate.Month, Year = record.AdmittedDate.Year } : null;
            CustodyTypeChangeDate = record.CustodyTypeChangeDate != null ? new BusinessReport.Date { Day = record.CustodyTypeChangeDate.Day, Month = record.CustodyTypeChangeDate.Month, Year = record.CustodyTypeChangeDate.Year } : null;
            Sentence = record.Sentence;
            ScheduledReleaseDate = record.ScheduledReleaseDate != null ? new BusinessReport.Date { Day = record.ScheduledReleaseDate.Day, Month = record.ScheduledReleaseDate.Month, Year = record.ScheduledReleaseDate.Year } : null;
            LastGainTime = record.LastGainTime != null ? new BusinessReport.Date { Day = record.LastGainTime.Day, Month = record.LastGainTime.Month, Year = record.LastGainTime.Year } : null;
        }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate AdmittedDate { get; set; }

        public string CurrentStatus { get; set; }

        public string CustodyType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CustodyTypeChangeDate { get; set; }

        public string GainTimeGranted { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate LastGainTime { get; set; }

        public string Location { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ScheduledReleaseDate { get; set; }

        public string Sentence { get; set; }
    }
}