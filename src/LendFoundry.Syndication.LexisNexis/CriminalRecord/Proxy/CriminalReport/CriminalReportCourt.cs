﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportCourt : ICriminalReportCourt
    {
        public CriminalReportCourt()
        {
        }

        public CriminalReportCourt(CrimReportCourt record)
        {
            if (record == null)
                return;
            Costs = record.Costs;
            CaseNumber = record.CaseNumber;
            Disposition = record.Disposition;
            Level = record.Level;
            DispositionDate = record.DispositionDate != null ? new BusinessReport.Date { Day = record.DispositionDate.Day, Month = record.DispositionDate.Month, Year = record.DispositionDate.Year } : null;
            Offense = record.Offense;
            Statute = record.Statute;
            Fine = record.Fine;
            Plea = record.Plea;
            SuspendedFine = record.SuspendedFine;
        }

        public string CaseNumber { get; set; }

        public string Costs { get; set; }

        public string Description { get; set; }

        public string Disposition { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DispositionDate { get; set; }

        public string Fine { get; set; }

        public string Level { get; set; }

        public string Offense { get; set; }

        public string Plea { get; set; }

        public string Statute { get; set; }

        public string SuspendedFine { get; set; }
    }
}