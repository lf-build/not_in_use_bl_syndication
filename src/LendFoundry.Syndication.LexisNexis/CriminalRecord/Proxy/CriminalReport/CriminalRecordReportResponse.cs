﻿using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportResponse : ICriminalReportResponse
    {
        public CriminalReportResponse()
        {
        }

        public CriminalReportResponse(FcraCriminalReportResponse response)
        {
            if (response == null)
                return;
            Header = new ResponseHeader(response.Header);
            //RecordCount = response.;
            if (response.CriminalRecords != null)
            {
                Records = new List<ICriminalReportRecord>(response.CriminalRecords.Select(record => new CriminalReportRecord(record)));
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }

        public int RecordCount { get; set; }

        //public int RecordCount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportRecord, CriminalReportRecord>))]
        public List<ICriminalReportRecord> Records { get; set; }
    }
}