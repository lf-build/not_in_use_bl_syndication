﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class ICriminalReportRecord
    {
         string OffenderId { get; set; }

         string CaseNumber { get; set; }

         string CountyOfOrigin { get; set; }

         string DocNumber { get; set; }

         IDate CaseFilingDate { get; set; }

         string Eyes { get; set; }

         string Hair { get; set; }

         string Height { get; set; }

         string Weight { get; set; }

         string Race { get; set; }

         string Sex { get; set; }

         string Skin { get; set; }

         string DataSource { get; set; }

         string Ssn { get; set; }

         string UniqueId { get; set; }

         string StateOfBirth { get; set; }

         string StateOfOrigin { get; set; }

         string Status { get; set; }

         IAddress Address { get; set; }

         IName Name { get; set; }

         IDate Dob { get; set; }

         string CaseTypeDescription { get; set; }

         List<IName> Akas { get; set; }

         List<ICriminalReportOffense> offenses { get; set; }

         List<ICriminalReportPrison> prisonSentences { get; set; }

         List<CriminalReportParoleEx> paroleSentences { get; set; }

         List<ICriminalReportEvent> activities { get; set; }
    }
}
