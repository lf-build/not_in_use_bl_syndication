﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportEvent
    {
        IDate Date { get; set; }
        string Description { get; set; }
    }
}