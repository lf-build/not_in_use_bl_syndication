﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportArrest
    {
        string Agency { get; set; }
        string CaseNumber { get; set; }
        IDate Date { get; set; }
        string Disposition { get; set; }
        IDate DispositionDate { get; set; }
        string Level { get; set; }
        string Offense { get; set; }
        string Statute { get; set; }
        string Type { get; set; }
    }
}