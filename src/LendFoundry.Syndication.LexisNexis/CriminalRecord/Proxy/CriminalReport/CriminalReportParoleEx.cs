﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportParoleEx : ICriminalReportParoleEx
    {
        public CriminalReportParoleEx()
        {
        }

        public CriminalReportParoleEx(CrimReportParoleEx record)
        {
            if (record == null)
                return;

            CurrentStatus = record.CurrentStatus;
            County = record.County;
            Length = record.Length;
            PubicSaftyId = record.PubicSaftyId;
            InmateId = record.InmateId;
            ParoleInAbsentiaId = record.ParoleInAbsentiaId;
            Name = new Name(record.Name);
            Race = record.Race;
            Gender = record.Gender;
            CountyOfBirth = record.CountyOfBirth;

            StateOfBirth = record.StateOfBirth;
            HeightFeet = record.HeightFeet;
            HeightFeetSpecified = record.HeightFeetSpecified;
            HeightInches = record.HeightInches;
            HeightInchesSpecified = record.HeightInchesSpecified;
            WeightInPounds = record.WeightInPounds;
            WeightInPoundsSpecified = record.WeightInPoundsSpecified;
            HairColor = record.HairColor;
            SkinColor = record.SkinColor;
            ScarsMarksTattoos = record.ScarsMarksTattoos != null ? new List<string>(record.ScarsMarksTattoos.Select(tattoo => tattoo)) : null;

            PrisonFacilityType = record.PrisonFacilityType;
            PrisonFacilityName = record.PrisonFacilityName;
            PrisonStatus = record.PrisonStatus;
            LastReceiveOrDepartCode = record.LastReceiveOrDepartCode;
            CurrentStatusFlag = record.CurrentStatusFlag;
            ParoleRegion = record.ParoleRegion;
            SupervisingOffice = record.SupervisingOffice;
            SupervisingOfficerName = record.SupervisingOfficerName;
            SupervisingOfficerPhone = record.SupervisingOfficerPhone;
            ReleaseType = record.ReleaseType;

            RecordCreatedTimeStamp = new TimeStamp(record.RecordCreatedTimeStamp);
            LastKnownResidence = new Address(record.LastKnownResidence);

            ReleaseCounty = record.ReleaseCounty;
            LegalResidenceCounty = record.LegalResidenceCounty;
            OffenseCountSpecified = record.OffenseCountSpecified;
            OffenseCount = record.OffenseCount;

            Is3GOffender = record.Is3GOffender;
            Is3GOffenderSpecified = record.Is3GOffenderSpecified;
            IsViolentOffenderSpecified = record.IsViolentOffenderSpecified;
            IsViolentOffender = record.IsViolentOffender;

            IsSexOffender = record.IsSexOffender;
            IsSexOffenderSpecified = record.IsSexOffenderSpecified;
            IsOnViolentOffenderProgram = record.IsOnViolentOffenderProgram;
            IsOnViolentOffenderProgramSpecified = record.IsOnViolentOffenderProgramSpecified;

            LongestTimeToServeDescription = record.LongestTimeToServeDescription;
            LongestTimeToServe = new Duration(record.LongestTimeToServe);

            Offenses = record.Offenses != null ? new List<ICriminalReportParoleOffense>(record.Offenses.Select(offense => new CriminalReportParoleOffense(offense))) : null;

            ActualEndDate = record.ActualEndDate != null ? new BusinessReport.Date { Day = record.ActualEndDate.Day, Month = record.ActualEndDate.Month, Year = record.ActualEndDate.Year } : null;
            ScheduledEndDate = record.ScheduledEndDate != null ? new BusinessReport.Date { Day = record.ScheduledEndDate.Day, Month = record.ScheduledEndDate.Month, Year = record.ScheduledEndDate.Year } : null;
            AdmittedDate = record.AdmittedDate != null ? new BusinessReport.Date { Day = record.AdmittedDate.Day, Month = record.AdmittedDate.Month, Year = record.AdmittedDate.Year } : null;
            StartDate = record.StartDate != null ? new BusinessReport.Date { Day = record.StartDate.Day, Month = record.StartDate.Month, Year = record.StartDate.Year } : null;

            DateReported = record.DateReported != null ? new BusinessReport.Date { Day = record.DateReported.Day, Month = record.DateReported.Month, Year = record.DateReported.Year } : null;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;
            LastReceiveOrDepartCDate = record.LastReceiveOrDepartCDate != null ? new BusinessReport.Date { Day = record.LastReceiveOrDepartCDate.Day, Month = record.LastReceiveOrDepartCDate.Month, Year = record.LastReceiveOrDepartCDate.Year } : null;
            CurrentStatusEffectiveDate = record.CurrentStatusEffectiveDate != null ? new BusinessReport.Date { Day = record.CurrentStatusEffectiveDate.Day, Month = record.CurrentStatusEffectiveDate.Month, Year = record.CurrentStatusEffectiveDate.Year } : null;

            ReleaseArrivalDate = record.ReleaseArrivalDate != null ? new BusinessReport.Date { Day = record.ReleaseArrivalDate.Day, Month = record.ReleaseArrivalDate.Month, Year = record.ReleaseArrivalDate.Year } : null;
            LastParoleReviewDate = record.LastParoleReviewDate != null ? new BusinessReport.Date { Day = record.LastParoleReviewDate.Day, Month = record.LastParoleReviewDate.Month, Year = record.LastParoleReviewDate.Year } : null;
            DischargeDate = record.DischargeDate != null ? new BusinessReport.Date { Day = record.DischargeDate.Day, Month = record.DischargeDate.Month, Year = record.DischargeDate.Year } : null;
        }

        public IDate ActualEndDate { get; set; }

        public string County { get; set; }

        public string CurrentStatus { get; set; }

        public string Length { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ScheduledEndDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate StartDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DateReported { get; set; }

        public string PubicSaftyId { get; set; }

        public string InmateId { get; set; }

        public string ParoleInAbsentiaId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        public string Race { get; set; }

        public string Gender { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        public string CountyOfBirth { get; set; }

        public string StateOfBirth { get; set; }

        public int HeightFeet { get; set; }

        public bool HeightFeetSpecified { get; set; }

        public int HeightInches { get; set; }

        public bool HeightInchesSpecified { get; set; }

        public int WeightInPounds { get; set; }

        public bool WeightInPoundsSpecified { get; set; }

        public string HairColor { get; set; }

        public string SkinColor { get; set; }

        public string EyeColor { get; set; }

        public List<string> ScarsMarksTattoos { get; set; }

        public string PrisonFacilityType { get; set; }

        public string PrisonFacilityName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate AdmittedDate { get; set; }

        public string PrisonStatus { get; set; }

        public string LastReceiveOrDepartCode { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate LastReceiveOrDepartCDate { get; set; }

        public TimeStamp RecordCreatedTimeStamp { get; set; }

        public string CurrentStatusFlag { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CurrentStatusEffectiveDate { get; set; }

        public string ParoleRegion { get; set; }

        public string SupervisingOffice { get; set; }

        public string SupervisingOfficerName { get; set; }

        public string SupervisingOfficerPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress LastKnownResidence { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate ReleaseArrivalDate { get; set; }

        public string ReleaseType { get; set; }

        public string ReleaseCounty { get; set; }

        public string LegalResidenceCounty { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate LastParoleReviewDate { get; set; }

        public int OffenseCount { get; set; }

        public bool OffenseCountSpecified { get; set; }

        public bool Is3GOffender { get; set; }

        public bool Is3GOffenderSpecified { get; set; }

        public bool IsViolentOffender { get; set; }

        public bool IsViolentOffenderSpecified { get; set; }

        public bool IsSexOffender { get; set; }

        public bool IsSexOffenderSpecified { get; set; }

        public bool IsOnViolentOffenderProgram { get; set; }

        public bool IsOnViolentOffenderProgramSpecified { get; set; }

        public Duration LongestTimeToServe { get; set; }

        public string LongestTimeToServeDescription { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DischargeDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalReportParoleOffense, CriminalReportParoleOffense>))]
        public List<ICriminalReportParoleOffense> Offenses { get; set; }
    }
}