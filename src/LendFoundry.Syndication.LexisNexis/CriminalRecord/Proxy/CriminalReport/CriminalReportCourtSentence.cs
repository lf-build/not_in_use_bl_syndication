﻿using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportCourtSentence: ICriminalReportCourtSentence
    {
        public CriminalReportCourtSentence() { }
        public CriminalReportCourtSentence(CrimReportCourtSentence record) {
            if (record == null)
                return;
            Jail = record.Jail;
            Probation = record.Probation;
            Suspended = record.Suspended;
            
        }
        public string Jail { get; set; }

        public string Probation { get; set; }

        public string Suspended { get; set; }
    }
}
