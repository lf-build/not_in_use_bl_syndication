﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public class CriminalReportParoleOffense : ICriminalReportParoleOffense
    {
        public CriminalReportParoleOffense()
        {
        }

        public CriminalReportParoleOffense(CrimReportParoleOffense record)
        {
            if (record == null)
                return;

            Length = record.Length;
            LengthSpecified = record.LengthSpecified;
            OffenseCounty = record.OffenseCounty;
            CauseNo = record.CauseNo;

            NcicCode = record.NCICCode;
            OffenseCount = record.OffenseCount;
            OffenseCountSpecified = record.OffenseCountSpecified;
            CauseNo = record.CauseNo;

            SentenceDate = record.SentenceDate != null ? new BusinessReport.Date { Day = record.SentenceDate.Day, Month = record.SentenceDate.Month, Year = record.SentenceDate.Year } : null;
            OffenseDate = record.OffenseDate != null ? new BusinessReport.Date { Day = record.OffenseDate.Day, Month = record.OffenseDate.Month, Year = record.OffenseDate.Year } : null;
        }

        public IDate SentenceDate { get; set; }

        public int Length { get; set; }

        public bool LengthSpecified { get; set; }

        public string OffenseCounty { get; set; }

        public string CauseNo { get; set; }

        public string NcicCode { get; set; }

        public int OffenseCount { get; set; }

        public bool OffenseCountSpecified { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate OffenseDate { get; set; }
    }
}