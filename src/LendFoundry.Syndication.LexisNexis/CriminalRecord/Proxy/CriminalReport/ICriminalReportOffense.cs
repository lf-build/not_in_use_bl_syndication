﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport
{
    public interface ICriminalReportOffense
    {
        
         string AdjudicationWithheld { get; set; }

         string CaseNumber { get; set; }

         string CaseType { get; set; }

         string CaseTypeDescription { get; set; }

         string Count { get; set; }

         string County { get; set; }

         string Description { get; set; }

         string MaximumTerm { get; set; }

         string MinimumTerm { get; set; }

         string NumberCounts { get; set; }

         IDate OffenseDate { get; set; }

         string OffenseType { get; set; }

         string Sentence { get; set; }

         string SentenceLengthDescription { get; set; }

         IDate SentenceDate { get; set; }

         IDate IncarcerationDate { get; set; }

         ICriminalReportAppeal Appeal { get; set; }

         ICriminalReportArrest Arrest { get; set; }

         ICriminalReportCourt Court { get; set; }

         ICriminalReportCourtSentence CourtSentence { get; set; }
    }
}
