﻿using System;
using System.Net;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public class CriminalRecordProxy : ICriminalRecordProxy
    {
        public CriminalRecordProxy(LexisNexisConfiguration configuration)
        {
            if (configuration.CriminalRecord == null)
                throw new ArgumentNullException(nameof(configuration.CriminalRecord));

            if (string.IsNullOrWhiteSpace(configuration.CriminalRecord.CriminalRecordUrl))
                throw new ArgumentNullException(nameof(configuration.CriminalRecord.CriminalRecordUrl));

            if (string.IsNullOrWhiteSpace(configuration.CriminalRecord.UserName))
                throw new ArgumentNullException(nameof(configuration.CriminalRecord.UserName));

            if (string.IsNullOrWhiteSpace(configuration.CriminalRecord.Password))
                throw new ArgumentNullException(nameof(configuration.CriminalRecord.Password));

            if (configuration.CriminalRecord.EndUser == null)
                throw new ArgumentNullException(nameof(configuration.CriminalRecord.EndUser));

            if (configuration.CriminalRecord.Options == null)
                throw new ArgumentNullException(nameof(configuration.CriminalRecord.Options));

            Configuration = configuration;
        }

        private LexisNexisConfiguration Configuration { get; }


        public async Task<FcraCriminalSearchResponse> CriminalRecordSearch(CriminalSearch.ServiceReference.User user, FcraCriminalSearchBy searchBy, FcraCriminalSearchOption options)
        {

            return await Task.Run(() =>
            {
                if (searchBy == null)
                    throw new ArgumentNullException(nameof(searchBy));

                var soapClient = new CriminalSearch.ServiceReference.WsAccurintFCRA
                {
                    Credentials = new NetworkCredential(Configuration.CriminalRecord.UserName, Configuration.CriminalRecord.Password),
                    Url = Configuration.CriminalRecord.CriminalRecordUrl
                };
                
                //XmlDocument doc = new XmlDocument();
                //doc.LoadXml("<?xml version='1.0' encoding='UTF - 8'?><FcraBankruptcySearch3ResponseEx><response><Records><Record><TMSId>BKMI0039831080</TMSId><CaseNumber>xxx0032</CaseNumber><CourtCode>MI003</CourtCode><CourtName>MICHIGAN EASTERN - FLINT</CourtName><CourtLocation>FLINT</CourtLocation><FilerType>JOINT</FilerType><OriginalChapter>7</OriginalChapter><MatchedParty><PartyType>P</PartyType><Address><StreetNumber>1910</StreetNumber><StreetName>ALIWARE</StreetName><StreetSuffix>ST</StreetSuffix><City>GAGLEPARK</City><State>MI</State><Zip5>96939</Zip5><Zip4>7805</Zip4></Address><ParsedParty><Last>ARDUR</Last><First>JAMES</First><Middle>R</Middle></ParsedParty><UniqueId>999931269275</UniqueId></MatchedParty><Debtors><Debtor><BusinessId>0</BusinessId><AppendedSSN>77327xxxx</AppendedSSN><SSN>77327xxxx</SSN><Addresses><Address><StreetNumber>1910</StreetNumber><StreetName>ALIWARE</StreetName><StreetSuffix>ST</StreetSuffix><City>GAGLEPARK</City><State>MI</State><Zip5>96939</Zip5><Zip4>7805</Zip4></Address></Addresses><Names><Name><Last>ARDUR</Last><First>JAMES</First><Middle>R</Middle><Type>P</Type><UniqueId>999931269275</UniqueId></Name></Names><UniqueId>999931269275</UniqueId></Debtor></Debtors><OriginalFilingDate><Year>2013</Year><Month>02</Month><Day>25</Day></OriginalFilingDate><FilingDate><Year>2013</Year><Month>02</Month><Day>25</Day></FilingDate><Disposition>Discharged</Disposition></Record></Records><RecordCount>1</RecordCount><Header><TransactionId>158786733R5534</TransactionId><Status>0</Status></Header></response></FcraBankruptcySearch3ResponseEx>");
                var soapResponse = soapClient.CriminalSearch(user, searchBy, options);
                return soapResponse;
            });
        }

        public async Task<FcraCriminalReportResponse> CriminalRecordReport(CriminalReport.ServiceReference.User user, FcraCriminalReportBy reportBy, FcraCriminalReportOption options)
        {
            return await Task.Run(() =>
            {
                if (reportBy == null)
                    throw new ArgumentNullException(nameof(reportBy));

                var soapClient = new CriminalReport.ServiceReference.WsAccurintFCRA
                {
                    Credentials = new NetworkCredential(Configuration.CriminalRecord.UserName, Configuration.CriminalRecord.Password),
                    Url = Configuration.CriminalRecord.CriminalRecordUrl
                };

                var soapResponse = soapClient.CriminalReport(user, options, reportBy);
                return soapResponse;
            });
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request)
        {
            var response = client.Execute(request);

            if (response.ErrorException != null)
                throw new LexisNexisException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new LexisNexisException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                    throw new LexisNexisException(
                        $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }

    }
}