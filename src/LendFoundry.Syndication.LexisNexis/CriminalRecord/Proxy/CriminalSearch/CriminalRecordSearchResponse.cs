﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public class CriminalRecordSearchResponse : ICriminalRecordSearchResponse
    {
        public CriminalRecordSearchResponse(FcraCriminalSearchResponse searchResponse)
        {
            if (searchResponse == null)
                return;
            Header = new ResponseHeader(searchResponse.Header);
            RecordCount = searchResponse.RecordCount;
            if (searchResponse.Records != null)
            {
                Records = new List<ICriminalSearchRecord>(searchResponse.Records.Select(record => new CriminalSearchRecord(record)));
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IResponseHeader, ResponseHeader>))]
        public IResponseHeader Header { get; set; }

        public int RecordCount { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalSearchRecord, CriminalSearchRecord>))]
        public List<ICriminalSearchRecord> Records { get; set; }
    }
}