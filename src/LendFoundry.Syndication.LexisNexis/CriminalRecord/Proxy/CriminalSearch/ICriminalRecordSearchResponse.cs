﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public interface ICriminalRecordSearchResponse
    {
        IResponseHeader Header { get; set; }
        int RecordCount { get; set; }
        List<ICriminalSearchRecord> Records { get; set; }
    }
}
