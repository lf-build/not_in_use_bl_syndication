﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.BusinessReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public class CriminalSearchRecord : ICriminalSearchRecord
    {
        public CriminalSearchRecord()
        {
        }

        public CriminalSearchRecord(CrimSearchRecord record)
        {
            if (record == null)
                return;

            AlsoFoundSpecified = record.AlsoFoundSpecified;
            AlsoFound = record.AlsoFound;
            DataSource = record.DataSource;
            OffenderId = record.OffenderId;
            CaseNumber = record.CaseNumber;
            Name = new Name(record.Name);
            Address = new Address(record.Address);
            Ssn = record.SSN;
            Dob = record.DOB != null ? new BusinessReport.Date { Day = record.DOB.Day, Month = record.DOB.Month, Year = record.DOB.Year } : null;
            StateOfOrigin = record.StateOfOrigin;
            UniqueId = record.UniqueId;
            CountyOfOrigin = record.CountyOfOrigin;
            DocNumber = record.DOCNumber;
            CaseFilingDate = record.CaseFilingDate != null ? new BusinessReport.Date { Day = record.CaseFilingDate.Day, Month = record.CaseFilingDate.Month, Year = record.CaseFilingDate.Year } : null;
            DateLastSeen = record.DateLastSeen != null ? new BusinessReport.Date { Day = record.DateLastSeen.Day, Month = record.DateLastSeen.Month, Year = record.DateLastSeen.Year } : null;
        }

        public bool AlsoFound { get; set; }

        public bool AlsoFoundSpecified { get; set; }

        public string DataSource { get; set; }

        public string OffenderId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        public string Ssn { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate Dob { get; set; }

        public string StateOfOrigin { get; set; }

        public string StateOfBirth { get; set; }

        public string UniqueId { get; set; }

        public string CountyOfOrigin { get; set; }

        public string CaseNumber { get; set; }

        public string DocNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate CaseFilingDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDate, LendFoundry.Syndication.LexisNexis.BusinessReport.Date>))]
        public IDate DateLastSeen { get; set; }
    }
}