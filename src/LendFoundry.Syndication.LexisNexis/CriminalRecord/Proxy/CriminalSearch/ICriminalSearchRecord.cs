﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy
{
    public interface ICriminalSearchRecord
    {
        bool AlsoFound { get; set; }

        bool AlsoFoundSpecified { get; set; }

        string DataSource { get; set; }

        string OffenderId { get; set; }

        IName Name { get; set; }

        IAddress Address { get; set; }

        string Ssn { get; set; }

        IDate Dob { get; set; }

        string StateOfOrigin { get; set; }

        string StateOfBirth { get; set; }

        string UniqueId { get; set; }

        string CountyOfOrigin { get; set; }

        string CaseNumber { get; set; }

        IDate CaseFilingDate { get; set; }
        IDate DateLastSeen { get; set; }
    }
}
