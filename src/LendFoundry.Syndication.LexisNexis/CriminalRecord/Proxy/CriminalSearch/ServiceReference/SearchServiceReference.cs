﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

// 
// This source code was auto-generated by wsdl, Version=4.6.1055.0.
// 

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "WsAccurintFCRAServiceSoap", Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class WsAccurintFCRA : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private System.Threading.SendOrPostCallback CriminalSearchOperationCompleted;

        /// <remarks/>
        public WsAccurintFCRA()
        {
            //this.Url = "http://172.16.70.155:7516/WsAccurintFCRA?ver_=1.89&fcra=1"        
        }

        /// <remarks/>
        public event CriminalSearchCompletedEventHandler CriminalSearchCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("WsAccurintFCRA/CriminalSearch?ver_=1.89&fcra=1", RequestElementName = "FcraCriminalSearchRequest", RequestNamespace = "http://webservices.seisint.com/WsAccurintFCRA", ResponseElementName = "FcraCriminalSearchResponseEx", ResponseNamespace = "http://webservices.seisint.com/WsAccurintFCRA", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("response")]
        public FcraCriminalSearchResponse CriminalSearch(User User, FcraCriminalSearchBy SearchBy, FcraCriminalSearchOption Options)
        {
            object[] results = this.Invoke("CriminalSearch", new object[] {
                    User,
                    SearchBy,
                    Options});
            return ((FcraCriminalSearchResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginCriminalSearch(User User, FcraCriminalSearchBy SearchBy, FcraCriminalSearchOption Options, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("CriminalSearch", new object[] {
                    User,
                    SearchBy,
                    Options}, callback, asyncState);
        }

        /// <remarks/>
        public FcraCriminalSearchResponse EndCriminalSearch(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FcraCriminalSearchResponse)(results[0]));
        }

        /// <remarks/>
        public void CriminalSearchAsync(User User, FcraCriminalSearchBy SearchBy, FcraCriminalSearchOption Options)
        {
            this.CriminalSearchAsync(User, SearchBy, Options, null);
        }

        /// <remarks/>
        public void CriminalSearchAsync(User User, FcraCriminalSearchBy SearchBy, FcraCriminalSearchOption Options, object userState)
        {
            if ((this.CriminalSearchOperationCompleted == null))
            {
                this.CriminalSearchOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCriminalSearchOperationCompleted);
            }
            this.InvokeAsync("CriminalSearch", new object[] {
                    User,
                    SearchBy,
                    Options}, this.CriminalSearchOperationCompleted, userState);
        }

        private void OnCriminalSearchOperationCompleted(object arg)
        {
            if ((this.CriminalSearchCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CriminalSearchCompleted(this, new CriminalSearchCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class User
    {

        private string referenceCodeField;

        private string billingCodeField;

        private string queryIdField;

        private string gLBPurposeField;

        private string dLPurposeField;

        private EndUserInfo endUserField;

        private int maxWaitSecondsField;

        private bool maxWaitSecondsFieldSpecified;

        private string accountNumberField;

        /// <remarks/>
        public string ReferenceCode
        {
            get
            {
                return this.referenceCodeField;
            }
            set
            {
                this.referenceCodeField = value;
            }
        }

        /// <remarks/>
        public string BillingCode
        {
            get
            {
                return this.billingCodeField;
            }
            set
            {
                this.billingCodeField = value;
            }
        }

        /// <remarks/>
        public string QueryId
        {
            get
            {
                return this.queryIdField;
            }
            set
            {
                this.queryIdField = value;
            }
        }

        /// <remarks/>
        public string GLBPurpose
        {
            get
            {
                return this.gLBPurposeField;
            }
            set
            {
                this.gLBPurposeField = value;
            }
        }

        /// <remarks/>
        public string DLPurpose
        {
            get
            {
                return this.dLPurposeField;
            }
            set
            {
                this.dLPurposeField = value;
            }
        }

        /// <remarks/>
        public EndUserInfo EndUser
        {
            get
            {
                return this.endUserField;
            }
            set
            {
                this.endUserField = value;
            }
        }

        /// <remarks/>
        public int MaxWaitSeconds
        {
            get
            {
                return this.maxWaitSecondsField;
            }
            set
            {
                this.maxWaitSecondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MaxWaitSecondsSpecified
        {
            get
            {
                return this.maxWaitSecondsFieldSpecified;
            }
            set
            {
                this.maxWaitSecondsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class EndUserInfo
    {

        private string companyNameField;

        private string streetAddress1Field;

        private string cityField;

        private string stateField;

        private string zip5Field;

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public string StreetAddress1
        {
            get
            {
                return this.streetAddress1Field;
            }
            set
            {
                this.streetAddress1Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string Zip5
        {
            get
            {
                return this.zip5Field;
            }
            set
            {
                this.zip5Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class CrimSearchRecord
    {

        private bool alsoFoundField;

        private bool alsoFoundFieldSpecified;

        private string dataSourceField;

        private string offenderIdField;

        private Name nameField;

        private Address addressField;

        private string sSNField;

        private Date dOBField;

        private string stateOfOriginField;

        private string stateOfBirthField;

        private string uniqueIdField;

        private string countyOfOriginField;

        private string caseNumberField;

        private string dOCNumberField;

        private Date caseFilingDateField;

        private Date dateLastSeenField;

        /// <remarks/>
        public bool AlsoFound
        {
            get
            {
                return this.alsoFoundField;
            }
            set
            {
                this.alsoFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AlsoFoundSpecified
        {
            get
            {
                return this.alsoFoundFieldSpecified;
            }
            set
            {
                this.alsoFoundFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string DataSource
        {
            get
            {
                return this.dataSourceField;
            }
            set
            {
                this.dataSourceField = value;
            }
        }

        /// <remarks/>
        public string OffenderId
        {
            get
            {
                return this.offenderIdField;
            }
            set
            {
                this.offenderIdField = value;
            }
        }

        /// <remarks/>
        public Name Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string SSN
        {
            get
            {
                return this.sSNField;
            }
            set
            {
                this.sSNField = value;
            }
        }

        /// <remarks/>
        public Date DOB
        {
            get
            {
                return this.dOBField;
            }
            set
            {
                this.dOBField = value;
            }
        }

        /// <remarks/>
        public string StateOfOrigin
        {
            get
            {
                return this.stateOfOriginField;
            }
            set
            {
                this.stateOfOriginField = value;
            }
        }

        /// <remarks/>
        public string StateOfBirth
        {
            get
            {
                return this.stateOfBirthField;
            }
            set
            {
                this.stateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string UniqueId
        {
            get
            {
                return this.uniqueIdField;
            }
            set
            {
                this.uniqueIdField = value;
            }
        }

        /// <remarks/>
        public string CountyOfOrigin
        {
            get
            {
                return this.countyOfOriginField;
            }
            set
            {
                this.countyOfOriginField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string DOCNumber
        {
            get
            {
                return this.dOCNumberField;
            }
            set
            {
                this.dOCNumberField = value;
            }
        }

        /// <remarks/>
        public Date CaseFilingDate
        {
            get
            {
                return this.caseFilingDateField;
            }
            set
            {
                this.caseFilingDateField = value;
            }
        }

        /// <remarks/>
        public Date DateLastSeen
        {
            get
            {
                return this.dateLastSeenField;
            }
            set
            {
                this.dateLastSeenField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class Name
    {

        private string fullField;

        private string firstField;

        private string middleField;

        private string lastField;

        private string suffixField;

        private string prefixField;

        /// <remarks/>
        public string Full
        {
            get
            {
                return this.fullField;
            }
            set
            {
                this.fullField = value;
            }
        }

        /// <remarks/>
        public string First
        {
            get
            {
                return this.firstField;
            }
            set
            {
                this.firstField = value;
            }
        }

        /// <remarks/>
        public string Middle
        {
            get
            {
                return this.middleField;
            }
            set
            {
                this.middleField = value;
            }
        }

        /// <remarks/>
        public string Last
        {
            get
            {
                return this.lastField;
            }
            set
            {
                this.lastField = value;
            }
        }

        /// <remarks/>
        public string Suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }

        /// <remarks/>
        public string Prefix
        {
            get
            {
                return this.prefixField;
            }
            set
            {
                this.prefixField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class Address
    {

        private string streetNumberField;

        private string streetPreDirectionField;

        private string streetNameField;

        private string streetSuffixField;

        private string streetPostDirectionField;

        private string unitDesignationField;

        private string unitNumberField;

        private string streetAddress1Field;

        private string streetAddress2Field;

        private string cityField;

        private string stateField;

        private string zip5Field;

        private string zip4Field;

        private string countyField;

        private string postalCodeField;

        private string stateCityZipField;

        /// <remarks/>
        public string StreetNumber
        {
            get
            {
                return this.streetNumberField;
            }
            set
            {
                this.streetNumberField = value;
            }
        }

        /// <remarks/>
        public string StreetPreDirection
        {
            get
            {
                return this.streetPreDirectionField;
            }
            set
            {
                this.streetPreDirectionField = value;
            }
        }

        /// <remarks/>
        public string StreetName
        {
            get
            {
                return this.streetNameField;
            }
            set
            {
                this.streetNameField = value;
            }
        }

        /// <remarks/>
        public string StreetSuffix
        {
            get
            {
                return this.streetSuffixField;
            }
            set
            {
                this.streetSuffixField = value;
            }
        }

        /// <remarks/>
        public string StreetPostDirection
        {
            get
            {
                return this.streetPostDirectionField;
            }
            set
            {
                this.streetPostDirectionField = value;
            }
        }

        /// <remarks/>
        public string UnitDesignation
        {
            get
            {
                return this.unitDesignationField;
            }
            set
            {
                this.unitDesignationField = value;
            }
        }

        /// <remarks/>
        public string UnitNumber
        {
            get
            {
                return this.unitNumberField;
            }
            set
            {
                this.unitNumberField = value;
            }
        }

        /// <remarks/>
        public string StreetAddress1
        {
            get
            {
                return this.streetAddress1Field;
            }
            set
            {
                this.streetAddress1Field = value;
            }
        }

        /// <remarks/>
        public string StreetAddress2
        {
            get
            {
                return this.streetAddress2Field;
            }
            set
            {
                this.streetAddress2Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string Zip5
        {
            get
            {
                return this.zip5Field;
            }
            set
            {
                this.zip5Field = value;
            }
        }

        /// <remarks/>
        public string Zip4
        {
            get
            {
                return this.zip4Field;
            }
            set
            {
                this.zip4Field = value;
            }
        }

        /// <remarks/>
        public string County
        {
            get
            {
                return this.countyField;
            }
            set
            {
                this.countyField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string StateCityZip
        {
            get
            {
                return this.stateCityZipField;
            }
            set
            {
                this.stateCityZipField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class Date
    {

        private short yearField;

        private bool yearFieldSpecified;

        private short monthField;

        private bool monthFieldSpecified;

        private short dayField;

        private bool dayFieldSpecified;

        /// <remarks/>
        public short Year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool YearSpecified
        {
            get
            {
                return this.yearFieldSpecified;
            }
            set
            {
                this.yearFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short Month
        {
            get
            {
                return this.monthField;
            }
            set
            {
                this.monthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MonthSpecified
        {
            get
            {
                return this.monthFieldSpecified;
            }
            set
            {
                this.monthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short Day
        {
            get
            {
                return this.dayField;
            }
            set
            {
                this.dayField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DaySpecified
        {
            get
            {
                return this.dayFieldSpecified;
            }
            set
            {
                this.dayFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class WsException
    {

        private string sourceField;

        private int codeField;

        private bool codeFieldSpecified;

        private string locationField;

        private string messageField;

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public int Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodeSpecified
        {
            get
            {
                return this.codeFieldSpecified;
            }
            set
            {
                this.codeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class ResponseHeader
    {

        private int statusField;

        private bool statusFieldSpecified;

        private string messageField;

        private string queryIdField;

        private string transactionIdField;

        private WsException[] exceptionsField;

        /// <remarks/>
        public int Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified
        {
            get
            {
                return this.statusFieldSpecified;
            }
            set
            {
                this.statusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string QueryId
        {
            get
            {
                return this.queryIdField;
            }
            set
            {
                this.queryIdField = value;
            }
        }

        /// <remarks/>
        public string TransactionId
        {
            get
            {
                return this.transactionIdField;
            }
            set
            {
                this.transactionIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Item", IsNullable = false)]
        public WsException[] Exceptions
        {
            get
            {
                return this.exceptionsField;
            }
            set
            {
                this.exceptionsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class FcraCriminalSearchResponse
    {

        private ResponseHeader headerField;

        private int recordCountField;

        private bool recordCountFieldSpecified;

        private CrimSearchRecord[] recordsField;

        /// <remarks/>
        public ResponseHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public int RecordCount
        {
            get
            {
                return this.recordCountField;
            }
            set
            {
                this.recordCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RecordCountSpecified
        {
            get
            {
                return this.recordCountFieldSpecified;
            }
            set
            {
                this.recordCountFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Record", IsNullable = false)]
        public CrimSearchRecord[] Records
        {
            get
            {
                return this.recordsField;
            }
            set
            {
                this.recordsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class FcraCriminalSearchOption
    {

        private bool useNicknamesField;

        private bool includeAlsoFoundField;

        private bool usePhoneticsField;

        private int fCRAPurposeField;

        private bool fCRAPurposeFieldSpecified;

        private int returnCountField;

        private int startingRecordField;

        public FcraCriminalSearchOption()
        {
            this.useNicknamesField = true;
            this.includeAlsoFoundField = false;
            this.usePhoneticsField = false;
            this.returnCountField = 10;
            this.startingRecordField = 1;
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(true)]
        public bool UseNicknames
        {
            get
            {
                return this.useNicknamesField;
            }
            set
            {
                this.useNicknamesField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool IncludeAlsoFound
        {
            get
            {
                return this.includeAlsoFoundField;
            }
            set
            {
                this.includeAlsoFoundField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool UsePhonetics
        {
            get
            {
                return this.usePhoneticsField;
            }
            set
            {
                this.usePhoneticsField = value;
            }
        }

        /// <remarks/>
        public int FCRAPurpose
        {
            get
            {
                return this.fCRAPurposeField;
            }
            set
            {
                this.fCRAPurposeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FCRAPurposeSpecified
        {
            get
            {
                return this.fCRAPurposeFieldSpecified;
            }
            set
            {
                this.fCRAPurposeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(10)]
        public int ReturnCount
        {
            get
            {
                return this.returnCountField;
            }
            set
            {
                this.returnCountField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(1)]
        public int StartingRecord
        {
            get
            {
                return this.startingRecordField;
            }
            set
            {
                this.startingRecordField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.seisint.com/WsAccurintFCRA")]
    public partial class FcraCriminalSearchBy
    {

        private Name nameField;

        private Address addressField;

        private Date dOBField;

        private string sSNField;

        private string phone10Field;

        private string uniqueIdField;

        /// <remarks/>
        public Name Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public Date DOB
        {
            get
            {
                return this.dOBField;
            }
            set
            {
                this.dOBField = value;
            }
        }

        /// <remarks/>
        public string SSN
        {
            get
            {
                return this.sSNField;
            }
            set
            {
                this.sSNField = value;
            }
        }

        /// <remarks/>
        public string Phone10
        {
            get
            {
                return this.phone10Field;
            }
            set
            {
                this.phone10Field = value;
            }
        }

        /// <remarks/>
        public string UniqueId
        {
            get
            {
                return this.uniqueIdField;
            }
            set
            {
                this.uniqueIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    public delegate void CriminalSearchCompletedEventHandler(object sender, CriminalSearchCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "4.6.1055.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CriminalSearchCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal CriminalSearchCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FcraCriminalSearchResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FcraCriminalSearchResponse)(this.results[0]));
            }
        }
    }
}