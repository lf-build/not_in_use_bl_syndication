﻿using LendFoundry.Syndication.LexisNexis.BusinessSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{
    public partial class FcraCriminalSearchBy
    {
        public FcraCriminalSearchBy()
        { }
        public FcraCriminalSearchBy(ICriminalRecordSearchRequest searchBy)
        {
            if (searchBy == null)
                return;

            SSN = searchBy.Ssn;
            Name = new Name
            {
                First = searchBy.FirstName,
                Last = searchBy.LastName
            };
            Address = new Address
            {
                StreetAddress1 = searchBy.StreetAddress1,
                State = searchBy.State,
                City = searchBy.City,
                Zip5 = searchBy.Zip5
            };
        }
    }
}

