﻿using LendFoundry.Syndication.LexisNexis.Bankruptcy;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference
{
    public partial class FcraCriminalSearchOption
    {
        public FcraCriminalSearchOption(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            FCRAPurpose = searchOption.FCRAPurpose;
            FCRAPurposeSpecified = true;
        }
    }
}
