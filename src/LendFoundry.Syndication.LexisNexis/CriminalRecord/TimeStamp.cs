﻿namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    public class TimeStamp
    {
        public TimeStamp()
        {}
        public TimeStamp(Proxy.CriminalReport.ServiceReference.TimeStamp record)
        {
            if (record == null)
                return;

            Year = record.Year;
                YearSpecified = record.YearSpecified;
                Month = record.Month;
                MonthSpecified = record.MonthSpecified;
                Day = record.Day;
                DaySpecified = record.DaySpecified;
                Hour24 = record.Hour24;
                Minute = record.Minute;
                Hour24Specified = record.Hour24Specified;
                Second = record.Second;
                SecondSpecified = record.SecondSpecified;
        }
        public short Year { get; set; }

        public bool YearSpecified { get; set; }

        public short Month { get; set; }

        public bool MonthSpecified { get; set; }

        public short Day { get; set; }

        public bool DaySpecified { get; set; }

        public short Hour24 { get; set; }

        public bool Hour24Specified { get; set; }

        public short Minute { get; set; }

        public bool MinuteSpecified { get; set; }

        public short Second { get; set; }

        public bool SecondSpecified { get; set; }
    }
}
