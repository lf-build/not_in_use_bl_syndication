﻿using LendFoundry.Syndication.LexisNexis.BusinessReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
   
    public interface IResponseHeader
    {
        int Status { get; set; }
        string Message { get; set; }
        string QueryId { get; set; }
        string TransactionId { get; set; }
        List<IException> Exceptions { get; set; }

    }
}
