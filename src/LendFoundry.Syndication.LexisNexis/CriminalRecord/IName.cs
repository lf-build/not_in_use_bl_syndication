﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    public interface IName
    {
         string Full { get; set; }

         string First { get; set; }

        string Middle { get; set; }

        string Last { get; set; }

        string Suffix { get; set; }

        string Prefix { get; set; }
    }
}
