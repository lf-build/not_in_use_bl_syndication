﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{ 
    public interface ICriminalRecordSearchRequest
    {
        string Ssn { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string StreetAddress1 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip5 { get; set; }
    }
}
