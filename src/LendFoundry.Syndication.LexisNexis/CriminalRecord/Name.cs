﻿
namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    public class Name : IName
    {
        public Name() { }

        public Name(Proxy.CriminalSearch.ServiceReference.Name name)
        {
            if (name == null)
                return;

            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
        }
        public Name(Proxy.CriminalReport.ServiceReference.Name name)
        {
            if (name == null)
                return;

            Full = name.Full;
            First = name.First;
            Middle = name.Middle;
            Last = name.Last;
            Suffix = name.Suffix;
            Prefix = name.Prefix;
        }
        public string Full { get; set; }

        public string First { get; set; }

        public string Middle { get; set; }

        public string Last { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
