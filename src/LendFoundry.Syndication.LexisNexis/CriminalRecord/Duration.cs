﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.LexisNexis.CriminalRecord
{
    public class Duration
    {
        public Duration() { }

        public Duration(Proxy.CriminalReport.ServiceReference.Duration record) {
            if (record == null)
                return;

            Years = record.Years;
            YearsSpecified = record.YearsSpecified;
            Months = record.Months;
            MonthsSpecified = record.MonthsSpecified;
            Days = record.Days;
            DaysSpecified = record.DaysSpecified;
        }
        public short Years { get; set; }

        public bool YearsSpecified { get; set; }

        public short Months { get; set; }

        public bool MonthsSpecified { get; set; }

        public short Days { get; set; }

        public bool DaysSpecified { get; set; }
    }
}
