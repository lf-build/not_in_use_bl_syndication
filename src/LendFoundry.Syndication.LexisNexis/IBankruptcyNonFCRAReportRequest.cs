﻿namespace LendFoundry.Syndication.LexisNexis.Bankruptcy
{
    public interface IBankruptcyReportRequest
    {        
        string UniqueId { get; set; }
        string BusinessId { get; set; }
        string TMSId { get; set; }
        string OwnerId { get; set; }
    }
}