﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IScoreSequencedHri
    {

        string Type { get; set; }
        int Value { get; set; }
        INameValuePair[] RiskIndices { get; set; }
        ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
