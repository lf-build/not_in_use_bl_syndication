﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class DateOfBirthMatchOptions : IDateOfBirthMatchOptions
    {
        public DateOfBirthMatchOptions()
        {
        }

        public DateOfBirthMatchOptions(ServiceReference.DOBMatchOptions dobMatchOptions)
        {
            if (dobMatchOptions == null)
                return;

            MatchType = (DateOfBirthMatchType)(int)dobMatchOptions.MatchType;
            MatchYearRadius = dobMatchOptions.MatchYearRadius;
        }

        public DateOfBirthMatchType MatchType { get; set; } = DateOfBirthMatchType.FuzzyCCYYMMDD;
        public int MatchYearRadius { get; set; } = 5;
    }
}