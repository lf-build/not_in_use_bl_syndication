﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdSearchBy
    {
        IName Name { get; set; }
        IAddress Address { get; set; }
        IDate DateOfBirth { get; set; }
        uint Age { get; set; }
        string Ssn { get; set; }
        string SsnLast4 { get; set; }
        string DriverLicenseNumber { get; set; }
        string DriverLicenseState { get; set; }
        string IpAddress { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        IPassport Passport { get; set; }
        string Gender { get; set; }
    }
}
