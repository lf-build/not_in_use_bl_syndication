﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFraudPointModelWithOptions
    {
        string ModelName { get; set; }
        bool IncludeRiskIndices { get; set; }
    }
}
