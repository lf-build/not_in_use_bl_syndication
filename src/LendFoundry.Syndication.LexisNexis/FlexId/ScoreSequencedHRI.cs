﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class ScoreSequencedHri : IScoreSequencedHri
    {
        public ScoreSequencedHri(ServiceReference.ScoreSequencedHRI score)
        {
            if (score == null)
                return;

            Type = score.Type;
            Value = score.Value;

            if (score.RiskIndices != null)
            {
                List<INameValuePair> risks = new List<INameValuePair>();
                foreach (ServiceReference.NameValuePair risk in score.RiskIndices)
                {
                    risks.Add(new NameValuePair(risk));
                }

                RiskIndices = risks.ToArray();
            }

            if (score.HighRiskIndicators != null)
            {
                List<ISequencedRiskIndicator> highRisks = new List<ISequencedRiskIndicator>();
                foreach (ServiceReference.SequencedRiskIndicator highRisk in score.HighRiskIndicators)
                {
                    highRisks.Add(new SequencedRiskIndicator(highRisk));
                }

                HighRiskIndicators = highRisks.ToArray();
            }
        }

        public string Type { get; set; }
        public int Value { get; set; }
        public INameValuePair[] RiskIndices { get; set; }
        public ISequencedRiskIndicator[] HighRiskIndicators { get; set; }
    }
}
