﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IModelSequencedHri
    {
        string Name { get; set; }
        IScoreSequencedHri[] Scores { get; set; }
    }
}
