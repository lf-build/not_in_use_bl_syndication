﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IWsException
    {
        string Source { get; set; }
        int Code { get; set; }
        string Location { get; set; }
        string Message { get; set; }
    }
}
