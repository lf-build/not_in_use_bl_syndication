﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdRequest: IFlexIdRequest
    {
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IDate DateOfBirth { get; set; }
        public uint Age { get; set; }
        public string SocialSecurityNumber { get; set; }
        public string SocialSecurityNumberLast4 { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public IPassport Passport { get; set; }
        public string Gender { get; set; }
    }
}