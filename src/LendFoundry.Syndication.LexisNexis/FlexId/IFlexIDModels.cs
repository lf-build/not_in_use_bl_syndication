﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdModels
    {
        IFraudPointModelWithOptions FraudPointModel { get; set; }
        IModelRequest[] ModelRequests { get; set; }
    }
}
