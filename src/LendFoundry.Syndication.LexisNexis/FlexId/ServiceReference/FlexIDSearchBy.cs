﻿namespace LendFoundry.Syndication.LexisNexis.FlexId.ServiceReference
{
    public partial class FlexIDSearchBy
    {
        public FlexIDSearchBy() {}

        public FlexIDSearchBy(IFlexIdRequest searchBy)
        {
            if (searchBy == null)
                return;

            if(searchBy.Address != null)
                Address = new Address()
                {
                    City = searchBy.Address.City,
                    County = searchBy.Address.County,
                    PostalCode = searchBy.Address.PostalCode,
                    State = searchBy.Address.State,
                    StateCityZip = searchBy.Address.StateCityZip,
                    StreetAddress1 = searchBy.Address.StreetAddress1,
                    StreetAddress2 = searchBy.Address.StreetAddress2,
                    StreetName = searchBy.Address.StreetName,
                    StreetNumber = searchBy.Address.StreetNumber,
                    StreetPostDirection = searchBy.Address.StreetPostDirection,
                    StreetPreDirection = searchBy.Address.StreetPreDirection,
                    StreetSuffix = searchBy.Address.StreetSuffix,
                    UnitDesignation = searchBy.Address.UnitDesignation,
                    UnitNumber = searchBy.Address.UnitNumber,
                    Zip4 = searchBy.Address.Zip4,
                    Zip5 = searchBy.Address.Zip5
                };

            Age = searchBy.Age;

            if (searchBy.DateOfBirth != null)
                DOB = new Date()
                {
                    Day = searchBy.DateOfBirth.Day,
                    Month = searchBy.DateOfBirth.Month,
                    Year = searchBy.DateOfBirth.Year
                };

            DriverLicenseNumber = searchBy.DriverLicenseNumber;
            DriverLicenseState = searchBy.DriverLicenseState;
            Gender = searchBy.Gender;
            HomePhone = searchBy.HomePhone;
            IPAddress = searchBy.IpAddress;
            if (searchBy.Name != null)
                Name = new Name()
                {
                    First = searchBy.Name.First,
                    Full = searchBy.Name.Full,
                    Last = searchBy.Name.Last,
                    Middle = searchBy.Name.Middle,
                    Prefix = searchBy.Name.Prefix,
                    Suffix = searchBy.Name.Suffix
                };

            Passport = new Passport();
            if (searchBy.Passport != null)
            {
                Passport = new Passport()
                {
                    Number = searchBy.Passport.Number,
                    Country = searchBy.Passport.Country,
                    MachineReadableLine1 = searchBy.Passport.MachineReadableLine1,
                    MachineReadableLine2 = searchBy.Passport.MachineReadableLine2
                };

                if (searchBy.Passport.ExpirationDate != null)
                    Passport.ExpirationDate = new Date()
                    {
                        Day = searchBy.Passport.ExpirationDate.Day,
                        Month = searchBy.Passport.ExpirationDate.Month,
                        Year = searchBy.Passport.ExpirationDate.Year
                    };
            }

            SSN = searchBy.SocialSecurityNumber;
            SSNLast4 = searchBy.SocialSecurityNumberLast4;
            WorkPhone = searchBy.WorkPhone;
        }
    }
}