﻿namespace LendFoundry.Syndication.LexisNexis.FlexId.ServiceReference
{
    public partial class User
    {
        public User()
        {

        }

        public User(IUser user)
        {
            if (user == null)
                return;

            AccountNumber = user.AccountNumber;
            BillingCode = user.BillingCode;
            DLPurpose = user.DriverLicensePurpose;
            GLBPurpose = user.GrammLeachBlileyPurpose;
            EndUser = new FlexId.ServiceReference.EndUserInfo();
            EndUser.CompanyName = user.EndUser.CompanyName;
            EndUser.City = user.EndUser.City;
            EndUser.StreetAddress1 = user.EndUser.StreetAddress1;
            EndUser.State = user.EndUser.State;
            EndUser.Zip5 = user.EndUser.Zip5;
            MaxWaitSeconds = user.MaxWaitSeconds;
            QueryId = user.QueryId;
            ReferenceCode = user.ReferenceCode;
        }
    }
}
