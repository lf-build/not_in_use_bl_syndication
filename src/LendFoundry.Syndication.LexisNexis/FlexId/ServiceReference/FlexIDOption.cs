﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId.ServiceReference
{
    public partial class FlexIDOption
    {
        public FlexIDOption(IFlexIdOption options)
        {
            if (options == null)
                return;

            CustomCVIModelName = options.CustomCviModelName;
            CVICalculationOptions = new FlexId.ServiceReference.OptionsForCVICalculation()
            {
                DisableCustomerNetworkOption = options.CviCalculationOptions.DisableCustomerNetworkOption,
                IncludeDOB = options.CviCalculationOptions.IncludeDateOfBirth,
                IncludeDriverLicense = options.CviCalculationOptions.IncludeDriverLicense
            };
            DOBMatch = new FlexId.ServiceReference.DOBMatchOptions()
            {
                MatchType = (FlexId.ServiceReference.DOBMatchType)(int)options.DateOfBirthMatch.MatchType,
                MatchYearRadius = options.DateOfBirthMatch.MatchYearRadius
            };
            DOBRadius = options.DateOfBirthRadius;
            IncludeAllRiskIndicators = options.IncludeAllRiskIndicators;
            IncludeDLVerification = options.IncludeDrivingLicenseVerification;
            IncludeMIOverride = options.IncludeMiOverride;
            IncludeModels = new FlexId.ServiceReference.FlexIDModels();
            IncludeModels.FraudPointModel = new FlexId.ServiceReference.FraudPointModelWithOptions();
            IncludeModels.FraudPointModel.IncludeRiskIndices = options.IncludeModels.FraudPointModel.IncludeRiskIndices;
            IncludeModels.FraudPointModel.ModelName = options.IncludeModels.FraudPointModel.ModelName;

            List<FlexId.ServiceReference.ModelRequest> models = new List<FlexId.ServiceReference.ModelRequest>();
            if (options.IncludeModels.ModelRequests != null)
            {
                foreach (FlexId.ModelRequest requestModel in options.IncludeModels.ModelRequests)
                {
                    FlexId.ServiceReference.ModelRequest modelRequest = new FlexId.ServiceReference.ModelRequest();
                    modelRequest.ModelName = requestModel.ModelName;

                    List<FlexId.ServiceReference.ModelOption> modelOptions = new List<FlexId.ServiceReference.ModelOption>();
                    foreach (FlexId.ModelOption modelOption in requestModel.ModelOptions)
                    {
                        modelOptions.Add(new FlexId.ServiceReference.ModelOption()
                        {
                            OptionName = modelOption.OptionName,
                            OptionValue = modelOption.OptionValue
                        });
                    }
                    modelRequest.ModelOptions = modelOptions.ToArray();
                    models.Add(modelRequest);
                }
            }

            IncludeModels.ModelRequests = models.ToArray();
            IncludeMSOverride = options.IncludeMsOverride;
            IncludeSSNVerification = options.IncludeSsnVerification;
            IncludeVerifiedElementSummary = options.IncludeVerifiedElementSummary;
            InstantIDVersion = options.InstantIdVersion;
            LastSeenThreshold = options.LastSeenThreshold;
            PoBoxCompliance = options.PoBoxCompliance;
            RequireExactMatch = new FlexId.ServiceReference.RequireExactMatchFlexID()
            {
                Address = options.RequireExactMatch.Address,
                DriverLicense = options.RequireExactMatch.DriverLicense,
                FirstName = options.RequireExactMatch.FirstName,
                FirstNameAllowNickname = options.RequireExactMatch.FirstNameAllowNickname,
                HomePhone = options.RequireExactMatch.HomePhone,
                LastName = options.RequireExactMatch.LastName,
                SSN = options.RequireExactMatch.Ssn
            };

            UseDOBFilter = options.UseDateOfBirthFilter;
            WatchLists = options.WatchLists;

        }
    }
}