﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class ResponseHeader : IResponseHeader
    {
        public ResponseHeader(ServiceReference.ResponseHeader header)
        {
            if (header == null)
                return;

            Status = header.Status;
            Message = header.Message;
            QueryId = header.QueryId;
            TransactionId = header.TransactionId;

            if (header.Exceptions != null)
            {
                List<IWsException> exceptions = new List<IWsException>();
                foreach (ServiceReference.WsException exception in header.Exceptions)
                {
                    exceptions.Add(new WsException(exception));
                }

                Exceptions = exceptions.ToArray();
            }
        }

        public int Status { get; set; }
        public string Message { get; set; }
        public string QueryId { get; set; }
        public string TransactionId { get; set; }
        public IWsException[] Exceptions { get; set; }
    }
}
