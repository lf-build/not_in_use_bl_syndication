﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class CustomComprehensiveVerificationStruct : ICustomComprehensiveVerificationStruct
    {
        public CustomComprehensiveVerificationStruct(ServiceReference.CustomComprehensiveVerificationStruct customComprehensiveVerificationStruct)
        {
            if (customComprehensiveVerificationStruct == null)
                return;

            ComprehensiveVerificationIndex = customComprehensiveVerificationStruct.ComprehensiveVerificationIndex;

            if (customComprehensiveVerificationStruct.RiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach (ServiceReference.SequencedRiskIndicator riskIndicator in customComprehensiveVerificationStruct.RiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(riskIndicator));
                }

                RiskIndicators = riskIndicators.ToArray();
            }

            if (customComprehensiveVerificationStruct.PotentialFollowupActions != null)
            {
                List<IRiskIndicator> followupActions = new List<IRiskIndicator>();
                foreach (ServiceReference.RiskIndicator riskIndicator in customComprehensiveVerificationStruct.PotentialFollowupActions)
                {
                    followupActions.Add(new RiskIndicator(riskIndicator));
                }

                PotentialFollowupActions = followupActions.ToArray();
            }

            Name = customComprehensiveVerificationStruct.Name;
        }

        public int ComprehensiveVerificationIndex { get; set; }
        public ISequencedRiskIndicator[] RiskIndicators { get; set; }
        public IRiskIndicator[] PotentialFollowupActions { get; set; }
        public string Name { get; set; }
    }
}
