﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public enum DateOfBirthMatchType
    {
        FuzzyCCYYMMDD,
        FuzzyCCYYMM,
        RadiusCCYY,
        ExactCCYYMMDD,
        ExactCCYYMM
    }
}
