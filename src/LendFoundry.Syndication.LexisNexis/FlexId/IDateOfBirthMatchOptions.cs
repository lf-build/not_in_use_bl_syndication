﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IDateOfBirthMatchOptions
    {
        DateOfBirthMatchType MatchType { get; set; }
        int MatchYearRadius { get; set; }
    }
}
