﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdOption
    {
        string[] WatchLists { get; set; }
        bool UseDateOfBirthFilter { get; set; }
        int DateOfBirthRadius { get; set; }
        bool IncludeMsOverride { get; set; }
        bool PoBoxCompliance { get; set; }
        [JsonConverter(typeof(InterfaceConverter<FlexId.IRequireExactMatchFlexId, FlexId.RequireExactMatchFlexId>))]
        IRequireExactMatchFlexId RequireExactMatch { get; set; }
        bool IncludeAllRiskIndicators { get; set; }
        bool IncludeVerifiedElementSummary { get; set; }
        bool IncludeDrivingLicenseVerification { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDateOfBirthMatchOptions, DateOfBirthMatchOptions>))]
        IDateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        [JsonConverter(typeof(InterfaceConverter<FlexId.IFlexIdModels, FlexId.FlexIdModels>))]
        IFlexIdModels IncludeModels { get; set; }
        string CustomCviModelName { get; set; }
        string LastSeenThreshold { get; set; }
        bool IncludeMiOverride { get; set; }
        bool IncludeSsnVerification { get; set; }

        [JsonConverter(typeof(InterfaceConverter<FlexId.IOptionsForCviCalculation, FlexId.OptionsForCviCalculation>))]
        IOptionsForCviCalculation CviCalculationOptions { get; set; }
        string InstantIdVersion { get; set; }
    }
}
