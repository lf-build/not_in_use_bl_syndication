﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class ValidElementSummary : IValidElementSummary
    {
        public ValidElementSummary(ServiceReference.ValidElementSummary summary)
        {
            if (summary == null)
                return;

            SsnValid = summary.SSNValid;
            SsnDeceased = summary.SSNDeceased;
            DrivingLicenseValid = summary.DLValid;
            PassportValid = summary.PassportValid;
            AddressPoBox = summary.AddressPOBox;
            AddressCmra = summary.AddressCMRA;
            SsnFoundForLexId = summary.SSNFoundForLexID;
        }

        public bool SsnValid { get; set; }
        public bool SsnDeceased { get; set; }
        public bool DrivingLicenseValid { get; set; }
        public bool PassportValid { get; set; }
        public bool AddressPoBox { get; set; }
        public bool AddressCmra { get; set; }
        public bool SsnFoundForLexId { get; set; }
    }
}
