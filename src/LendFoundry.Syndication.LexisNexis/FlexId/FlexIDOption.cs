﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdOption : IFlexIdOption
    {
        public FlexIdOption()
        {
        }

        public FlexIdOption(ServiceReference.FlexIDOption option)
        {
            if (option == null)
                return;

            WatchLists = option.WatchLists;
            UseDateOfBirthFilter = option.UseDOBFilter;
            DateOfBirthRadius = option.DOBRadius;
            IncludeMsOverride = option.IncludeMSOverride;
            PoBoxCompliance = option.PoBoxCompliance;
            RequireExactMatch = new RequireExactMatchFlexId(option.RequireExactMatch);
            IncludeAllRiskIndicators = option.IncludeAllRiskIndicators;
            IncludeVerifiedElementSummary = option.IncludeVerifiedElementSummary;
            IncludeDrivingLicenseVerification = option.IncludeDLVerification;
            DateOfBirthMatch = new DateOfBirthMatchOptions(option.DOBMatch);
            IncludeModels = new FlexIdModels(option.IncludeModels);
            CustomCviModelName = option.CustomCVIModelName;
            LastSeenThreshold = option.LastSeenThreshold;
            IncludeMiOverride = option.IncludeMIOverride;
            IncludeSsnVerification = option.IncludeSSNVerification;
            CviCalculationOptions = new OptionsForCviCalculation(option.CVICalculationOptions);
            InstantIdVersion = option.InstantIDVersion;
        }

        public string[] WatchLists { get; set; }
        public bool UseDateOfBirthFilter { get; set; } = true;
        public int DateOfBirthRadius { get; set; } = 5;
        public bool IncludeMsOverride { get; set; } = true;
        public bool PoBoxCompliance { get; set; } = true;
        public IRequireExactMatchFlexId RequireExactMatch { get; set; }
        public bool IncludeAllRiskIndicators { get; set; } = true;
        public bool IncludeVerifiedElementSummary { get; set; } = true;
        public bool IncludeDrivingLicenseVerification { get; set; } = true;
        public IDateOfBirthMatchOptions DateOfBirthMatch { get; set; }
        public IFlexIdModels IncludeModels { get; set; }
        public string CustomCviModelName { get; set; }
        public string LastSeenThreshold { get; set; }
        public bool IncludeMiOverride { get; set; } = true;
        public bool IncludeSsnVerification { get; set; } = true;
        public IOptionsForCviCalculation CviCalculationOptions { get; set; }
        public string InstantIdVersion { get; set; }
    }
}