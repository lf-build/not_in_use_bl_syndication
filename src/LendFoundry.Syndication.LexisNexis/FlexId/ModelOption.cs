﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class ModelOption : IModelOption
    {
        public ModelOption() {}

        public ModelOption(ServiceReference.ModelOption modelOption)
        {
            if (modelOption == null)
                return;

            OptionName = modelOption.OptionName;
            OptionValue = modelOption.OptionValue;
        }

        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }
}