﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdRequest
    {
        IName Name { get; set; }
        IAddress Address { get; set; }
        IDate DateOfBirth { get; set; }
        uint Age { get; set; }
        string SocialSecurityNumber { get; set; }
        string SocialSecurityNumberLast4 { get; set; }
        string DriverLicenseNumber { get; set; }
        string DriverLicenseState { get; set; }
        string IpAddress { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        IPassport Passport { get; set; }
        string Gender { get; set; }
    }
}
