﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IRequireExactMatchFlexId
    {
        bool LastName { get; set; }
        bool FirstName { get; set; }
        bool FirstNameAllowNickname { get; set; }
        bool Address { get; set; }
        bool HomePhone { get; set; }
        bool Ssn { get; set; }
        bool DriverLicense { get; set; }
    }
}
