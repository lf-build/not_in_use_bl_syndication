﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FraudPointModelWithOptions : IFraudPointModelWithOptions
    {
        public FraudPointModelWithOptions()
        {
        }

        public FraudPointModelWithOptions(ServiceReference.FraudPointModelWithOptions options)
        {
            if (options == null)
                return;

            ModelName = options.ModelName;
            IncludeRiskIndices = options.IncludeRiskIndices;
        }

        public string ModelName { get; set; }
        public bool IncludeRiskIndices { get; set; } = true;
    }
}