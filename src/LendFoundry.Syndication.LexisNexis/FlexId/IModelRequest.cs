﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IModelRequest
    {
        string ModelName { get; set; }
        IModelOption[] ModelOptions { get; set; }
    }
}
