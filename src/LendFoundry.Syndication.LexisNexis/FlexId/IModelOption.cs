﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IModelOption
    {
        string OptionName { get; set; }
        string OptionValue { get; set; }
    }
}
