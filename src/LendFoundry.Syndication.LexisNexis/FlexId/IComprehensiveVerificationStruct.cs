﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IComprehensiveVerificationStruct
    {
        int ComprehensiveVerificationIndex { get; set; }
        ISequencedRiskIndicator[] RiskIndicators { get; set; }
        IRiskIndicator[] PotentialFollowupActions { get; set; }
    }
}
