﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdResponse : IFlexIdResponse
    {
        public FlexIdResponse(ServiceReference.FlexIDResponse response)
        {
            if (response == null)
                return;

            Header = new ResponseHeader(response.Header);
            Result = new FlexIdResult(response.Result);
        }

        public IResponseHeader Header { get; set; }
        public IFlexIdResult Result { get; set; }
    }
}
