﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IOptionsForCviCalculation
    {
        bool IncludeDateOfBirth { get; set; }
        bool IncludeDriverLicense { get; set; }
        bool DisableCustomerNetworkOption { get; set; }
    }
}
