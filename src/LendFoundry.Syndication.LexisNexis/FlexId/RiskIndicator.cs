﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class RiskIndicator : IRiskIndicator
    {
        public RiskIndicator(ServiceReference.RiskIndicator risk)
        {
            if (risk == null)
                return;

            RiskCode = risk.RiskCode;
            Description = risk.Description;
        }


        public string RiskCode { get; set; }
        public string Description { get; set; }
    }
}
