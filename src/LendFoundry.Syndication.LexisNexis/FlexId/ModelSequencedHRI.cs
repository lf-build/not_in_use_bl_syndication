﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{

    public class ModelSequencedHri : IModelSequencedHri
    {
        public ModelSequencedHri(ServiceReference.ModelSequencedHRI sequence)
        {
            if (sequence == null)
                return;

            Name = sequence.Name;

            if (sequence.Scores != null)
            {
                List<IScoreSequencedHri> scores = new List<IScoreSequencedHri>();
                foreach (ServiceReference.ScoreSequencedHRI score in sequence.Scores)
                {
                    scores.Add(new ScoreSequencedHri(score));
                }

                Scores = scores.ToArray();
            }
        }


        public string Name { get; set; }
        public IScoreSequencedHri[] Scores { get; set; }
    }

}
