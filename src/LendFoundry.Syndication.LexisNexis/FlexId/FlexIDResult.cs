﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdResult : IFlexIdResult
    {
        public FlexIdResult(ServiceReference.FlexIDResult result)
        {
            if (result == null)
                return;

            InputEcho = new FlexIdSearchBy(result.InputEcho);
            UniqueId = result.UniqueId;
            VerifiedSsn = result.VerifiedSSN;
            NameAddressPhone = new NameAddressPhone(result.NameAddressPhone);
            VerifiedElementSummary = new VerifiedElementSummary(result.VerifiedElementSummary);
            ValidElementSummary = new ValidElementSummary(result.ValidElementSummary);
            NameAddressSsnSummary = result.NameAddressSSNSummary;
            ComprehensiveVerification = new ComprehensiveVerificationStruct(result.ComprehensiveVerification);
            CustomComprehensiveVerification = new CustomComprehensiveVerificationStruct(result.CustomComprehensiveVerification);

            if (result.Models != null)
            {
                List<IModelSequencedHri> models = new List<IModelSequencedHri>();
                foreach (ServiceReference.ModelSequencedHRI model in result.Models)
                {
                    models.Add(new ModelSequencedHri(model));
                }
                Models = models.ToArray();
            }

            InstantIdVersion = result.InstantIDVersion;
        }

        public IFlexIdSearchBy InputEcho { get; set; }
        public string UniqueId { get; set; }
        public string VerifiedSsn { get; set; }
        public INameAddressPhone NameAddressPhone { get; set; }
        public IVerifiedElementSummary VerifiedElementSummary { get; set; }
        public IValidElementSummary ValidElementSummary { get; set; }
        public uint NameAddressSsnSummary { get; set; }
        public IComprehensiveVerificationStruct ComprehensiveVerification { get; set; }
        public ICustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }
        public IModelSequencedHri[] Models { get; set; }
        public string InstantIdVersion { get; set; }
    }
}
