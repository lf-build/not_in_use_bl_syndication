﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdClient
    {
        ServiceReference.FlexIDResponse Verify(ServiceReference.FlexIDSearchBy search);
    }
}