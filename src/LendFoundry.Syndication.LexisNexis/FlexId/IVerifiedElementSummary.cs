﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IVerifiedElementSummary
    {
        bool FirstName { get; set; }
        bool LastName { get; set; }
        bool StreetAddress { get; set; }
        bool City { get; set; }
        bool State { get; set; }
        bool Zip { get; set; }
        bool HomePhone { get; set; }
        bool DateOfBirth { get; set; }
        string DateOfBirthMatchLevel { get; set; }
        bool Ssn { get; set; }
        bool DrivingLicense { get; set; }
    }
}
