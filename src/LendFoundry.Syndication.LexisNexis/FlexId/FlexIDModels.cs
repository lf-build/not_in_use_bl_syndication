﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdModels : IFlexIdModels
    {
        public FlexIdModels()
        {
            FraudPointModel = new FraudPointModelWithOptions();
        }

        public FlexIdModels(ServiceReference.FlexIDModels models)
        {
            if (models == null)
                return;

            FraudPointModel = new FraudPointModelWithOptions(models.FraudPointModel);

            if (models.ModelRequests != null)
            {
                List<IModelRequest> requests = new List<IModelRequest>();
                foreach (ServiceReference.ModelRequest request in models.ModelRequests)
                {
                    requests.Add(new ModelRequest(request));
                }

                ModelRequests = requests.ToArray();
            }
        }

        public IFraudPointModelWithOptions FraudPointModel { get; set; }
        public IModelRequest[] ModelRequests { get; set; }
    }
}