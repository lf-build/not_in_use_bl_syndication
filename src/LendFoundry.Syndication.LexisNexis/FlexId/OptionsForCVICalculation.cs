﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class OptionsForCviCalculation : IOptionsForCviCalculation
    {
        public OptionsForCviCalculation()
        {
        }

        public OptionsForCviCalculation(ServiceReference.OptionsForCVICalculation options)
        {
            if (options == null)
                return;

            IncludeDateOfBirth = options.IncludeDOB;
            IncludeDriverLicense = options.IncludeDriverLicense;
            DisableCustomerNetworkOption = options.DisableCustomerNetworkOption;
        }

        public bool IncludeDateOfBirth { get; set; } = true;
        public bool IncludeDriverLicense { get; set; } = true;
        public bool DisableCustomerNetworkOption { get; set; } = true;
    }
}