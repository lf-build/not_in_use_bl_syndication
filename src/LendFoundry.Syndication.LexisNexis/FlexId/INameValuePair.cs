﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface INameValuePair
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}
