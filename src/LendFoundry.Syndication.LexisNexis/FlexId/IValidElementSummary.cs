﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IValidElementSummary
    {
        bool SsnValid { get; set; }
        bool SsnDeceased { get; set; }
        bool DrivingLicenseValid { get; set; }
        bool PassportValid { get; set; }
        bool AddressPoBox { get; set; }
        bool AddressCmra { get; set; }
        bool SsnFoundForLexId { get; set; }
    }
}
