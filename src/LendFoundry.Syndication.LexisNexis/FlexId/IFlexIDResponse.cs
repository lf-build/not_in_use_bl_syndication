﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdResponse
    {
        IResponseHeader Header { get; set; }
        IFlexIdResult Result { get; set; }
    }
}
