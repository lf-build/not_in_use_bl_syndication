﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdSearchBy : IFlexIdSearchBy
    {
        public FlexIdSearchBy(ServiceReference.FlexIDSearchBy searchBy)
        {
            if (searchBy == null)
                return;

            Name = new Name(searchBy.Name);
            Address = new Address(searchBy.Address);
            DateOfBirth = new Date(searchBy.DOB);
            Age = searchBy.Age;
            Ssn = searchBy.SSN;
            SsnLast4 = searchBy.SSNLast4;
            DriverLicenseNumber = searchBy.DriverLicenseNumber;
            DriverLicenseState = searchBy.DriverLicenseState;
            IpAddress = searchBy.IPAddress;
            HomePhone = searchBy.HomePhone;
            WorkPhone = searchBy.WorkPhone;
            Passport = new Passport(searchBy.Passport);
            Gender = searchBy.Gender;
        }

        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public IDate DateOfBirth { get; set; }
        public uint Age { get; set; }
        public string Ssn { get; set; }
        public string SsnLast4 { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string DriverLicenseState { get; set; }
        public string IpAddress { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public IPassport Passport { get; set; }
        public string Gender { get; set; }
    }
}
