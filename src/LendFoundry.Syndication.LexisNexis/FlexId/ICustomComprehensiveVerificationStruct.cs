﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface ICustomComprehensiveVerificationStruct
    {

        int ComprehensiveVerificationIndex { get; set; }
        ISequencedRiskIndicator[] RiskIndicators { get; set; }
        IRiskIndicator[] PotentialFollowupActions { get; set; }
        string Name { get; set; }
    }
}
