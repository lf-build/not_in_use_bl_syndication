﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class VerifiedElementSummary : IVerifiedElementSummary
    {
        public VerifiedElementSummary(ServiceReference.VerifiedElementSummary summary)
        {
            if (summary == null)
                return;

            FirstName = summary.FirstName;
            LastName = summary.LastName;
            StreetAddress = summary.StreetAddress;
            City = summary.City;
            State = summary.State;
            Zip = summary.Zip;
            DateOfBirth = summary.DOB;
            DateOfBirthMatchLevel = summary.DOBMatchLevel;
            Ssn = summary.SSN;
            DrivingLicense = summary.DL;
        }

        public bool FirstName { get; set; }
        public bool LastName { get; set; }
        public bool StreetAddress { get; set; }
        public bool City { get; set; }
        public bool State { get; set; }
        public bool Zip { get; set; }
        public bool HomePhone { get; set; }
        public bool DateOfBirth { get; set; }
        public string DateOfBirthMatchLevel { get; set; }
        public bool Ssn { get; set; }
        public bool DrivingLicense { get; set; }
    }
}
