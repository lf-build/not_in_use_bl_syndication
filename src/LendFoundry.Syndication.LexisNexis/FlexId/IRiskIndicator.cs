﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IRiskIndicator
    {
        string RiskCode { get; set; }
        string Description { get; set; }
    }
}
