﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class RequireExactMatchFlexId : IRequireExactMatchFlexId
    {
        public RequireExactMatchFlexId()
        {
        }

        public RequireExactMatchFlexId(ServiceReference.RequireExactMatchFlexID flexId)
        {
            if (flexId == null)
                return;

            LastName = flexId.LastName;
            FirstName = flexId.FirstName;
            FirstNameAllowNickname = flexId.FirstNameAllowNickname;
            Address = flexId.Address;
            HomePhone = flexId.HomePhone;
            Ssn = flexId.SSN;
            DriverLicense = flexId.DriverLicense;
        }

        public bool LastName { get; set; } = false;
        public bool FirstName { get; set; } = false;
        public bool FirstNameAllowNickname { get; set; } = true;
        public bool Address { get; set; } = false;
        public bool HomePhone { get; set; } = false;
        public bool Ssn { get; set; } = false;
        public bool DriverLicense { get; set; } = false;
    }
}
