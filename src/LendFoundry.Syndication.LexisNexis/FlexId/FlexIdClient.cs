﻿using System;
using System.Net;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class FlexIdClient : IFlexIdClient
    {
        public FlexIdClient(ILexisNexisConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.FlexId == null)
                throw new ArgumentNullException(nameof(configuration.FlexId));

            if (configuration.FlexId.UserName == null)
                throw new ArgumentNullException(nameof(configuration.FlexId.UserName));

            if (configuration.FlexId.Password == null)
                throw new ArgumentNullException(nameof(configuration.FlexId.Password));

            Configuration = configuration.FlexId;
        }

        private FlexIdServiceConfiguration Configuration { get;}

        public ServiceReference.FlexIDResponse Verify(ServiceReference.FlexIDSearchBy search)
        {
            try
            {
                if (search == null)
                    throw new ArgumentNullException(nameof(search));

                ServiceReference.WsIdentity soapClient = new ServiceReference.WsIdentity();
                soapClient.Url = Configuration.FlexIdUrl;
                soapClient.Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password);

                ServiceReference.User user = new ServiceReference.User(Configuration.EndUser);
                ServiceReference.FlexIDOption options = new ServiceReference.FlexIDOption(Configuration.Options);

                return soapClient.FlexID(user, options, search);
            }
            catch (Exception ex)
            {
                throw new LexisNexisException($"The method Verify({search}) raised an error:{ex.Message}", ex);
            }
        }
    }
}