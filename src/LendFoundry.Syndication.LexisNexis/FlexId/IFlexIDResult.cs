﻿namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public interface IFlexIdResult
    {
        IFlexIdSearchBy InputEcho { get; set; }
        string UniqueId { get; set; }
        string VerifiedSsn { get; set; }
        INameAddressPhone NameAddressPhone { get; set; }
        IVerifiedElementSummary VerifiedElementSummary { get; set; }
        IValidElementSummary ValidElementSummary { get; set; }
        uint NameAddressSsnSummary { get; set; }
        IComprehensiveVerificationStruct ComprehensiveVerification { get; set; }
        ICustomComprehensiveVerificationStruct CustomComprehensiveVerification { get; set; }
        IModelSequencedHri[] Models { get; set; }
        string InstantIdVersion { get; set; }
    }
}
