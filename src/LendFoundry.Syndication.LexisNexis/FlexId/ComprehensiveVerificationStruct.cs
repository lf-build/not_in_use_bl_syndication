﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.LexisNexis.FlexId
{
    public class ComprehensiveVerificationStruct : IComprehensiveVerificationStruct
    {
        public ComprehensiveVerificationStruct(ServiceReference.ComprehensiveVerificationStruct comprehensiveVerificationStruct)
        {
            if (comprehensiveVerificationStruct == null)
                return;

            ComprehensiveVerificationIndex = comprehensiveVerificationStruct.ComprehensiveVerificationIndex;

            if (comprehensiveVerificationStruct.RiskIndicators != null)
            {
                List<ISequencedRiskIndicator> riskIndicators = new List<ISequencedRiskIndicator>();
                foreach (ServiceReference.SequencedRiskIndicator riskIndicator in comprehensiveVerificationStruct.RiskIndicators)
                {
                    riskIndicators.Add(new SequencedRiskIndicator(riskIndicator));
                }

                RiskIndicators = riskIndicators.ToArray();
            }

            if (comprehensiveVerificationStruct.PotentialFollowupActions != null)
            {
                List<IRiskIndicator> followupActions = new List<IRiskIndicator>();
                foreach (ServiceReference.RiskIndicator riskIndicator in comprehensiveVerificationStruct.PotentialFollowupActions)
                {
                    followupActions.Add(new RiskIndicator(riskIndicator));
                }

                PotentialFollowupActions = followupActions.ToArray();
            }
        }

        public int ComprehensiveVerificationIndex { get; set; }
        public ISequencedRiskIndicator[] RiskIndicators { get; set; }
        public IRiskIndicator[] PotentialFollowupActions { get; set; }
    }
}
