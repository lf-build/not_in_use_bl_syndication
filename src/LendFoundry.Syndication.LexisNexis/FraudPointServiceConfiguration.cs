﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.LexisNexis
{
    public class FraudPointServiceConfiguration
    {
        public string FraudPointUrl { get; set; } = "https://wsonline.seisint.com/WsIdentity?ver_=1.88";
        public string UserName { get; set; }
        public string Password { get; set; }

        [JsonConverter(typeof(InterfaceConverter<FraudPoint.IUser, FraudPoint.User>))]
        public FraudPoint.IUser EndUser { get; set; }

        [JsonConverter(typeof(InterfaceConverter<FraudPoint.IFraudPointOption, FraudPoint.FraudPointOption>))]
        public FraudPoint.IFraudPointOption Options { get; set; }
    }
}