﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using System;
using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.Events;
using System.Collections.Generic;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference;
using System.Text.RegularExpressions;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Syndication.LexisNexis
{
    public class LexisNexisService : ILexisNexisService
    {
        public LexisNexisService
        (
            //FlexId.IFlexIdClient flexIdClient,
            //FraudPoint.IFraudPointClient fraudPointClient,
            //InstantId.IInstantIdClient instantIdClient,
            //InstantIdKba.IInstantIdKbaClient instantIdKbaClient,
            ICriminalRecordProxy criminalServiceProxy,
            IBankruptcyProxy bankruptcyServiceProxy, LexisNexisConfiguration configuration, ILookupService lookupService, ILogger logger, IEventHubClient eventHubClient,ITenantTime tenantTime
        )
        {
            //if (flexIdClient == null)
            //    throw new ArgumentNullException(nameof(flexIdClient));

            //if (fraudPointClient == null)
            //    throw new ArgumentNullException(nameof(fraudPointClient));

            //if (instantIdClient == null)
            //    throw new ArgumentNullException(nameof(instantIdClient));

            //if (instantIdKbaClient == null)
            //    throw new ArgumentNullException(nameof(instantIdKbaClient));


            if (bankruptcyServiceProxy == null)
                throw new ArgumentNullException(nameof(bankruptcyServiceProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (criminalServiceProxy == null)
                throw new ArgumentNullException(nameof(criminalServiceProxy));

            //FlexIdClient = flexIdClient;
            //FraudPointClient = fraudPointClient;
            //InstantIdClient = instantIdClient;
            //InstantIdKbaClient = instantIdKbaClient;

            BankruptcyServiceProxy = bankruptcyServiceProxy;
            Configuration = configuration;
            Logger = logger;
            EventHubClient = eventHubClient;
            LookupService = lookupService;
            CriminalServiceProxy = criminalServiceProxy;
            TenantTime = tenantTime;
        }

        private IBankruptcyProxy BankruptcyServiceProxy { get; }
        private LexisNexisConfiguration Configuration { get; }
        private ILookupService LookupService { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private ICriminalRecordProxy CriminalServiceProxy { get; }
        public async Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId, ISearchBankruptcyRequest request)
        {
            IBankruptcySearchResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validation
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.Ssn))
                    throw new ArgumentNullException($"The {nameof(request.Ssn)} cannot be null");

                if (!Regex.IsMatch(request.Ssn, "^([0-9]{9}|[0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException($"Invalid  {nameof(request.Ssn)}");

                if (string.IsNullOrWhiteSpace(request.FirstName))
                    throw new ArgumentNullException($"The {nameof(request.FirstName)} cannot be null");

                if (!Regex.IsMatch(request.FirstName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.FirstName)}");

                if (string.IsNullOrWhiteSpace(request.LastName))
                    throw new ArgumentNullException($"The {nameof(request.LastName)} cannot be null");

                if (!Regex.IsMatch(request.LastName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.LastName)}");

                #endregion Validation

                Logger.Info("Started Execution for FcraBankruptcySearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf LexisNexis: BankruptcySearch");

                var user = new Bankruptcy.Proxy.BankruptcySearch.ServiceReference.User(Configuration.Bankruptcy.EndUser);
                var options = new FcraBankruptcySearch3Option(Configuration.Bankruptcy.Options);
                var searchBy = new FcraBankruptcySearch3By(request);
                searchResult = new BankruptcySearchResponse(await BankruptcyServiceProxy.BankruptcySearch(user, searchBy, options));
                if (searchResult != null)
                {
                    await EventHubClient.Publish(new BankruptcySearchRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = searchBy,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new BankruptcySearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new BankruptcySearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }
            Logger.Info("Completed Execution for FcraBankruptcySearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + "ReferenceNumber" + referencenumber + " TypeOf LexisNexis: BankruptcySearch");

            return searchResult;
        }

        public async Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId, IBankruptcyFcraReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            try
            {
                Logger.Info("Started Execution for FcraBankruptcyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: FcraBankruptcyReport");

                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.QueryId))
                    throw new ArgumentNullException($"The {nameof(request.QueryId)} cannot be null");

                if (!Regex.IsMatch(request.QueryId, "^([a-zA-Z0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.QueryId)}");


                if (string.IsNullOrWhiteSpace(request.UniqueId))
                    throw new ArgumentNullException($"The {nameof(request.UniqueId)} cannot be null");

                if (!Regex.IsMatch(request.UniqueId, "^([0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.UniqueId)}");


                var user = new Bankruptcy.Proxy.BankruptcyReport.ServiceReference.User(request);
                var options = new FcraBankruptcyReport3By(request);
                var searchBy = new FcraBankruptcyReport3Option(Configuration.Bankruptcy.Options);
                searchResult = new BankruptcyReportResponse(await BankruptcyServiceProxy.BankruptcyReport(user, options, searchBy), true);

            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
            Logger.Info("Completed Execution for FcraBankruptcyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: FcraBankruptcyReport");

            return searchResult;
        }

        public async Task<IBankruptcyReportResponse> NonFcraBankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            try
            {
                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);

                var user = new Bankruptcy.Proxy.BankruptcyReport.NonFCRAServiceReference.User(request);
                var options = new BankruptcyReport2Option(Configuration.Bankruptcy.Options);
                var reportBy = new BankruptcyReport2By(request);
                searchResult = new BankruptcyReportResponse(await BankruptcyServiceProxy.NonFCRABankruptcyReport(user, reportBy, options));


            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                throw ex;
            }
            return searchResult;
        }

        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest request)
        {
            IBankruptcyReportResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                Logger.Info("Started Execution for BankruptcyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: BankruptcyReport");

                if (string.IsNullOrEmpty(entityType))
                    throw new ArgumentNullException(nameof(entityType));

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                entityType = EnsureEntityType(entityType);

                IBankruptcyReportResponse fcraResult = await FcraBankruptcyReport(entityType, entityId, new BankruptcyFcraReportRequest
                {
                    QueryId = request.TMSId,
                    UniqueId = request.UniqueId
                });

                IBankruptcyReportResponse nonFcraResult = await NonFcraBankruptcyReport(entityType, entityId, request);

                Bankruptcy.IResponseHeader header = null;
                List<IBankruptcyReportRecord> records = new List<IBankruptcyReportRecord>();
                int count = 0;

                if (fcraResult != null)
                {
                    header = fcraResult.Header;
                    records = fcraResult.Records;
                    count += fcraResult.RecordCount;
                }
                else
                {
                    if (nonFcraResult != null)
                    {
                        header = nonFcraResult.Header;
                        records.AddRange(nonFcraResult.Records);
                        count += nonFcraResult.RecordCount;
                    }
                }
                if (nonFcraResult != null)
                {
                    header = header == null ? nonFcraResult.Header : header;
                    records.AddRange(nonFcraResult.Records);
                    count += nonFcraResult.RecordCount;
                }
                searchResult = new BankruptcyReportResponse();
                searchResult.Header = header;
                searchResult.RecordCount = count;
                searchResult.Records = records;

                if (searchResult != null)
                {
                    await EventHubClient.Publish(new BankruptcyReportRequested
                    {

                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new BankruptcyReportRequesteFail
                {

                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber,
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new BankruptcyReportRequesteFail
                {

                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }
            Logger.Info("Completed Execution for BankruptcyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: BankruptcyReport");

            return searchResult;
        }


        public async Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest request)
        {
            ICriminalRecordSearchResponse searchResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validations
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.Ssn))
                    throw new ArgumentNullException($"The {nameof(request.Ssn)} cannot be null");

                if (!Regex.IsMatch(request.Ssn, "^([0-9]{3}-[0-9]{2}-[0-9]{4}|[0-9]+)$"))
                    throw new ArgumentException("Invalid Ssn Number format");

                if (string.IsNullOrWhiteSpace(request.FirstName))
                    throw new ArgumentNullException($"The {nameof(request.FirstName)} cannot be null");

                if (!Regex.IsMatch(request.FirstName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.FirstName)}");

                if (string.IsNullOrWhiteSpace(request.LastName))
                    throw new ArgumentNullException($"The {nameof(request.LastName)} cannot be null");

                if (!Regex.IsMatch(request.LastName, "^([a-zA-Z\\s]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.LastName)}");

                #endregion Validations
                Logger.Info("Started Execution for CriminalSearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: CriminalSearch");
                var user = new CriminalRecord.Proxy.CriminalSearch.ServiceReference.User(Configuration.CriminalRecord.EndUser);
                var options = new FcraCriminalSearchOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalSearchBy(request);
                searchResult = new CriminalRecordSearchResponse(await CriminalServiceProxy.CriminalRecordSearch(user, searchBy, options));
                if (searchResult != null)
                {
                    await EventHubClient.Publish(new CriminalRecordSearchRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = searchResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new CriminalRecordSearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new CriminalRecordSearchRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }
            Logger.Info("Completed Execution for CriminalSearch Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: CriminalSearch");

            return searchResult;
        }

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest request)
        {
            ICriminalReportResponse reportResult = null;
            var referencenumber = Guid.NewGuid().ToString("N");
            try
            {
                #region Validations
                entityType = EnsureEntityType(entityType);

                if (string.IsNullOrEmpty(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                if (request == null)
                    throw new ArgumentNullException($"The {nameof(request)} cannot be null");

                if (string.IsNullOrWhiteSpace(request.QueryId))
                    throw new ArgumentNullException($"The {nameof(request.QueryId)} cannot be null");

                if (!Regex.IsMatch(request.QueryId, "^([a-zA-Z0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.QueryId)}");

                if (string.IsNullOrWhiteSpace(request.UniqueId))
                    throw new ArgumentNullException($"The {nameof(request.UniqueId)} cannot be null");

                if (!Regex.IsMatch(request.UniqueId, "^([0-9]+)$"))
                    throw new ArgumentException($"Invalid {nameof(request.UniqueId)}");
                #endregion Validations
                Logger.Info("Started Execution for CriminalRecordReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: CriminalRecordReport");

                var user = new CriminalRecord.Proxy.CriminalReport.ServiceReference.User(request);
                var options = new FcraCriminalReportOption(Configuration.CriminalRecord.Options);
                var searchBy = new FcraCriminalReportBy(request);
                reportResult = new CriminalReportResponse(await CriminalServiceProxy.CriminalRecordReport(user, searchBy, options));
                if (reportResult != null)
                {
                    await EventHubClient.Publish(new CriminalRecordReportRequested
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = reportResult,
                        Request = request,
                        ReferenceNumber = referencenumber
                    });
                }
            }
            catch (LexisNexisException ex)
            {
                Logger.Error($"The method Bankruptcy Search({request}) raised an error: {ex.Message}");
                await EventHubClient.Publish(new CriminalRecordReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw new NotFoundException($"Information not found for this request");
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex.Message);
                await EventHubClient.Publish(new CriminalRecordReportRequesteFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = ex.Message,
                    Request = request,
                    ReferenceNumber = referencenumber
                });
                throw ex;
            }
            Logger.Info("Completed Execution for CriminalRecordReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf LexisNexis: CriminalRecordReport");

            return reportResult;
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (LookupService.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
        public FlexId.IFlexIdClient FlexIdClient { get; }
        public FraudPoint.IFraudPointClient FraudPointClient { get; }
        public InstantId.IInstantIdClient InstantIdClient { get; }
        public InstantIdKba.IInstantIdKbaClient InstantIdKbaClient { get; }

        public FlexId.IFlexIdResponse GetFlexIdVerificationResult(FlexId.IFlexIdRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return new FlexId.FlexIdResponse(FlexIdClient.Verify(new FlexId.ServiceReference.FlexIDSearchBy(request)));
        }

        public FraudPoint.IFraudPointResponse GetFraudPointVerificationResult(FraudPoint.IFraudPointRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return new FraudPoint.FraudPointResponse(FraudPointClient.Verify(new FraudPoint.ServiceReference.FraudPointSearchBy(request)));
        }

        public InstantId.IInstantIdResponse GetInstantIdVerificationResult(InstantId.IInstantIdRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return new InstantId.InstantIdResponse(InstantIdClient.Verify(new InstantId.ServiceReference.InstantIDSearchBy(request)));
        }

        public InstantIdKba.ITransactionResponse GetInstantIdKbaIdentityVerificationResult(InstantIdKba.IInstantIdIdentityRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return new InstantIdKba.TransactionResponse(InstantIdKbaClient.VerifyIdentity(new InstantIdKba.ServiceReference.persontype(request)));
        }

        public InstantIdKba.ITransactionResponse GetInstantIdKbaContinuationVerificationResult(InstantIdKba.IInstantIdContinuationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            return new InstantIdKba.TransactionResponse(InstantIdKbaClient.VerifyContinuation(new InstantIdKba.ServiceReference.TransactionContinueRequest(request)));
        }

    }
}