using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IOpSiteAddress
    {
        IAddress Address { get; set; }
        string MsaName { get; set; }
        string PropertyLink { get; set; }
        List<IOpSitePhone> Phones { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
