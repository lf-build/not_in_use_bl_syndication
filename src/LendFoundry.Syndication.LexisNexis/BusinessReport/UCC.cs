using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Ucc : IUcc
    {
        public Ucc()
        { }
        public Ucc(ServiceReference.TopBusinessUCC ucc)
        {
            if (ucc == null)
                return;
            FilingJurisdiction = ucc.FilingJurisdiction;
            OriginalFilingNumber = ucc.OriginalFilingNumber;
            OriginalFilingDate = new Date(ucc.OriginalFilingDate);
            LatestFilingType = ucc.LatestFilingType;
            FilingAgencyName = ucc.FilingAgencyName;
            FilingAgencyAddress = new Address(ucc.FilingAgencyAddress);
            if (ucc.Filings != null)
            {
                Filings = new List<IUccFiling>(ucc.Filings.Select(filing => new UccFiling(filing)));
            }
            if (ucc.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(ucc.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string FilingJurisdiction { get; set; }
        public string OriginalFilingNumber { get; set; }
        public IDate OriginalFilingDate { get; set; }
        public string LatestFilingType { get; set; }
        public string FilingAgencyName { get; set; }
        public IAddress FilingAgencyAddress { get; set; }
        public List<IUccFiling> Filings { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
