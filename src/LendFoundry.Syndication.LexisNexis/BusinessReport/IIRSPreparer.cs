namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIrsPreparer
	{
		 string Name { get; set; }
		 IAddress Address { get; set; }
		 string Fein { get; set; }
		 string Phone { get; set; }
	}
}
