using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIncorporationSection
    {
        List<IIncorporationInfo> CorpFilings { get; set; }
        int ReturnedCorpFilings { get; set; }
        int TotalCorpFilings { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
