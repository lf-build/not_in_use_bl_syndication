namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IndustrySic : IIndustrySic
    {
        public IndustrySic()
        { }
        public IndustrySic(ServiceReference.TopBusinessIndustrySIC industrySic)
        {
            if (industrySic == null)
                return;
            SicCode = industrySic.SICCode;
            SicCodeDescription = industrySic.SICCodeDescription;
        }
        public string SicCode { get; set; }
        public string SicCodeDescription { get; set; }
    }
}
