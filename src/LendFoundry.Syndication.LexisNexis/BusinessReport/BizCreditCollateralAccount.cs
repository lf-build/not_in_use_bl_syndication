using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditCollateralAccount : IBizCreditCollateralAccount
	{
        public BizCreditCollateralAccount()
        { }
        public BizCreditCollateralAccount(ServiceReference.BizCreditCollateralAccount bizCreditCollateralAccount)
        {
            if (bizCreditCollateralAccount == null)
                return;
            TotalUccFiled = bizCreditCollateralAccount.TotalUCCFiled;
            Count = bizCreditCollateralAccount.Count;
            if(bizCreditCollateralAccount.Collaterals != null)
            {
                Collaterals = new List<ICodeMap>(bizCreditCollateralAccount.Collaterals.Select(collateral => new CodeMap(collateral)));
            }
            AdditionalCode = bizCreditCollateralAccount.AdditionalCode;
        }
	    public int TotalUccFiled { get; set; }
	    public int Count { get; set; }
		public List<ICodeMap> Collaterals { get; set; }
		public string AdditionalCode { get; set; }
	}
}
