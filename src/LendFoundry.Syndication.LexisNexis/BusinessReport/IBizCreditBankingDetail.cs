namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditBankingDetail
	{
		 string Name { get; set; }
		 IAddress OrigAddress { get; set; }
		 string StateName { get; set; }
		 IAddress Address { get; set; }
		 string PhoneNumber { get; set; }
		 string TimeZone { get; set; }
		 int Balance { get; set; }
		 string Relationship { get; set; }
		 IDate DateOpened { get; set; }
		 IDate DateClosed { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
	}
}
