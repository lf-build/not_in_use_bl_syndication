namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ISanctionFiling
	{
		 string BusinessName { get; set; }
		 IDate Dob { get; set; }
		 string TaxIdNumber { get; set; }
		 string UPin { get; set; }
		 string ProviderType { get; set; }
		 IDate Date { get; set; }
		 string LicenseNumber { get; set; }
		 string State { get; set; }
		 string BoardType { get; set; }
		 string SourceDescription { get; set; }
		 string Type { get; set; }
		 string Terms { get; set; }
		 string Reason { get; set; }
		 string Condition { get; set; }
		 string Fines { get; set; }
		 IDate Update { get; set; }
		 IDate DateFirstReported { get; set; }
		 IDate DateLastReported { get; set; }
		 IDate ReinstateDate { get; set; }
		 IDate ProcessDate { get; set; }
		 IDate DateFirstSeen { get; set; }
		 IDate DateLastSeen { get; set; }
		 string FraudAbuseFlag { get; set; }
		 string LossOfLicenseIndicator { get; set; }
	}
}
