namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIrs5500Record
	{
		 string FormNumber { get; set; }
		 IDate FormDate { get; set; }
		 string Rsi { get; set; }
		 string DocumentLocatorNumber { get; set; }
		 string PlanName { get; set; }
		 string PlanNumber { get; set; }
		 IDate TransactionDate { get; set; }
		 IDate FormPlanYearBeginDate { get; set; }
		 string FormTaxPrd { get; set; }
		 string TypePlanEntityIndicator { get; set; }
		 string TypeDfePlanEntity { get; set; }
		 string TypePlanFilingIndicator { get; set; }
		 string CollectiveBargainIndicator { get; set; }
		 string ExtApplicationFiledIndicator { get; set; }
		 IDate PlanEffectiveDate { get; set; }
		 string Fein { get; set; }
		 string SponsorName { get; set; }
		 string SponsorDfeDbaName { get; set; }
		 string SponsorDfeCareOfName { get; set; }
		 string SponsorDfeMailStrAddress { get; set; }
		 string SponsorDfeLoc01Address { get; set; }
		 string SponsorDfeLoc02Address { get; set; }
		 string SponsorDfeForeignRouteCd { get; set; }
		 string SponsorDfeForeignMailCountry { get; set; }
		 string AdminStreetAddress { get; set; }
		 string AdminForeignRouteCd { get; set; }
		 string AdminForeignMailingCountry { get; set; }
		 string LastReportedSponsorName { get; set; }
		 string LastReportedSponsorFein { get; set; }
		 string LastReportedPlanNumber { get; set; }
		 string PreParerStrAddress { get; set; }
		 string PreparerForeignRouteCd { get; set; }
		 string PreparerFrgnMailingCountry { get; set; }
		 string AdminSignatureInd { get; set; }
		 string AdminSignedDate { get; set; }
		 string AdminSignedName { get; set; }
		 string SponsorSignatureIndicator { get; set; }
		 string SponsorSignedDate { get; set; }
		 string SponsorSignedName { get; set; }
		 string TotPartcpBoyCnt { get; set; }
		 string TotActivePartcpCnt { get; set; }
		 string RtdSepPartcpRcvgCnt { get; set; }
		 string RtdSepPartcpFutCnt { get; set; }
		 string SubtActRtdSepCnt { get; set; }
		 string BenefRcvgBnftCnt { get; set; }
		 string TotActRtdSepBenefCnt { get; set; }
		 string PartcpAccountBalCnt { get; set; }
		 string SepPartcpParttlVstCnt { get; set; }
		 string SsaPartCpPartlVstdCnt { get; set; }
		 string PensionBenefitPlanId { get; set; }
		 string TypePensionBnftCode { get; set; }
		 string WelfareBenefitPlanIndicator { get; set; }
		 string TypeWelfareBnftCode { get; set; }
		 string FringeBenefitPlanIndicator { get; set; }
		 string FundingArrangementCode { get; set; }
		 string BenefitCode { get; set; }
		 IIrsScheduleAttachment ScheduleAttachments { get; set; }
		 IName Name { get; set; }
		 string Score { get; set; }
		 bool IsMailingAddr { get; set; }
		 IAddress Address { get; set; }
		 string SponsorDfePhoneNumber { get; set; }
		 string BusinessCode { get; set; }
		 IIrsSponsor Sponsor { get; set; }
		 IIrsAdmin Administrator { get; set; }
		 IIrsPreparer Preparer { get; set; }
	}
}
