using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ContactSection : IContactSection
    {
        public ContactSection()
        { }
        public ContactSection(ServiceReference.TopBusinessContactSection contactSection)
        {
            if (contactSection == null)
                return;
            OtherAssociatedPersonCount = contactSection.OtherAssociatedPersonCount;
            if (contactSection.OtherAssociatedPersons != null)
            {
                OtherAssociatedPersons = new List<IIndividual>(contactSection.OtherAssociatedPersons.Select(otherAssociatedPerson => new Individual(otherAssociatedPerson)));
            }
            CurrentContactCount = contactSection.CurrentContactCount;
            TotalCurrentContactCount = contactSection.TotalCurrentContactCount;
            if (contactSection.CurrentIndividuals != null)
            {
                CurrentIndividuals = new List<IIndividual>(contactSection.CurrentIndividuals.Select(currentIndividual => new Individual(currentIndividual)));
            }
            CurrentExecutiveCount = contactSection.CurrentExecutiveCount;
            TotalCurrentExecutiveCount = contactSection.TotalCurrentExecutiveCount;
            if (contactSection.CurrentExecutives != null)
            {
                CurrentExecutives = new List<IIndividual>(contactSection.CurrentExecutives.Select(currentExecutive => new Individual(currentExecutive)));
            }
            if (contactSection.CurrentSourceDocs != null)
            {
                CurrentSourceDocs = new List<ISourceDocInfo>(contactSection.CurrentSourceDocs.Select(currentSourceDoc => new SourceDocInfo(currentSourceDoc)));
            }
            PriorContactCount = contactSection.PriorContactCount;
            TotalPriorContactCount = contactSection.TotalPriorContactCount;
            if (contactSection.PriorIndividuals != null)
            {
                PriorIndividuals = new List<IIndividual>(contactSection.PriorIndividuals.Select(priorIndividual => new Individual(priorIndividual)));
            }
            PriorExecutiveCount = contactSection.PriorExecutiveCount;
            TotalPriorExecutiveCount = contactSection.TotalPriorExecutiveCount;
            if (contactSection.PriorExecutives != null)
            {
                PriorExecutives = new List<IIndividual>(contactSection.PriorExecutives.Select(priorExecutive => new Individual(priorExecutive)));
            }
        }
        public short OtherAssociatedPersonCount { get; set; }
        public List<IIndividual> OtherAssociatedPersons { get; set; }
        public short CurrentContactCount { get; set; }
        public short TotalCurrentContactCount { get; set; }
        public List<IIndividual> CurrentIndividuals { get; set; }
        public short CurrentExecutiveCount { get; set; }
        public short TotalCurrentExecutiveCount { get; set; }
        public List<IIndividual> CurrentExecutives { get; set; }
        public List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        public short PriorContactCount { get; set; }
        public short TotalPriorContactCount { get; set; }
        public List<IIndividual> PriorIndividuals { get; set; }
        public short PriorExecutiveCount { get; set; }
        public short TotalPriorExecutiveCount { get; set; }
        public List<IIndividual> PriorExecutives { get; set; }
    }
}
