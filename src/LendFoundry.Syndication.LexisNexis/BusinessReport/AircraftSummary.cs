using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class AircraftSummary : IAircraftSummary
    {
        public AircraftSummary()
        {
            
        }

        public AircraftSummary(ServiceReference.TopBusinessAircraftSummary aircraftSummary)
        {
            if (aircraftSummary == null)
                return;
            CurrentRecordCount = aircraftSummary.CurrentRecordCount;
            TotalCurrentRecordCount = aircraftSummary.TotalCurrentRecordCount;
            if (aircraftSummary.CurrentAircrafts != null)
            {
                CurrentAircrafts =
                    new List<IAircraft>(
                        aircraftSummary.CurrentAircrafts.Select(currentAircraft => new Aircraft(currentAircraft)));
            }

            if (aircraftSummary.CurrentSourceDocs != null)
            {
                CurrentSourceDocs =
                    new List<ISourceDocInfo>(
                        aircraftSummary.CurrentSourceDocs.Select(currentSourceDoc => new SourceDocInfo(currentSourceDoc)));
            }

            PriorRecordCount = aircraftSummary.PriorRecordCount;
            if (aircraftSummary.PriorAircrafts != null)
            {
                PriorAircrafts = new List<IAircraft>(aircraftSummary.PriorAircrafts.Select(priorAircraft => new Aircraft(priorAircraft)));
            }
            if (aircraftSummary.PriorSourceDocs != null)
            {
                PriorSourceDocs = new List<ISourceDocInfo>(aircraftSummary.PriorSourceDocs.Select(priorSourceDoc => new SourceDocInfo(priorSourceDoc)));
            }
        }

        public int CurrentRecordCount { get; set; }
        public int TotalCurrentRecordCount { get; set; }
        public List<IAircraft> CurrentAircrafts { get; set; }
        public List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        public int PriorRecordCount { get; set; }
        public int TotalPriorRecordCount { get; set; }
        public List<IAircraft> PriorAircrafts { get; set; }
        public List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
