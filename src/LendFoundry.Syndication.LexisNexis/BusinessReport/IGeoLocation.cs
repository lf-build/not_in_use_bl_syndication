namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IGeoLocation
	{
		 string Latitude { get; set; }
		 string Longitude { get; set; }
	}
}
