namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Deeds : IDeeds
    {
        public Deeds()
        { }
        public Deeds(ServiceReference.TopBusinessDeeds deeds)
        {
            if (deeds == null)
                return;
            State = deeds.State;
            CountyName = deeds.CountyName;
            LenderName = deeds.LenderName;
            LegalBriefDescription = deeds.LegalBriefDescription;
            DocumentTypeCode = deeds.DocumentTypeCode;
            DocumentTypeDesc = deeds.DocumentTypeDesc;
            ArmResetDate = deeds.ArmResetDate;
            DocumentNumber = deeds.DocumentNumber;
            RecorderBookNumber = deeds.RecorderBookNumber;
            RecorderPageNumber = deeds.RecorderPageNumber;
            LandLotSize = deeds.LandLotSize;
            CityTransferTax = deeds.CityTransferTax;
            CountyTransferTax = deeds.CountyTransferTax;
            TotalTransferTax = deeds.TotalTransferTax;
            PropertyUseCode = deeds.PropertyUseCode;
            PropertyUseDesc = deeds.PropertyUseDesc;
            FirstTdLoanAmount = deeds.FirstTdLoanAmount;
            FirstTdLoanTypeCode = deeds.FirstTdLoanTypeCode;
            FirstTdLoanTypeDesc = deeds.FirstTdLoanTypeDesc;
            TypeFinancing = deeds.TypeFinancing;
            FirstTdInterestRate = deeds.FirstTdInterestRate;
            FirstTdDueDate = deeds.FirstTdDueDate;
            TitleCompanyName = deeds.TitleCompanyName;
            FaresTransactionType = deeds.FaresTransactionType;
            FaresTransactionTypeDesc = deeds.FaresTransactionTypeDesc;
            FaresMortgageDeedType = deeds.FaresMortgageDeedType;
            FaresMortgageDeedTypeDesc = deeds.FaresMortgageDeedTypeDesc;
            FaresMortgageTermCode = deeds.FaresMortgageTermCode;
            FaresMortgageTermCodeDesc = deeds.FaresMortgageTermCodeDesc;
            FaresMortgageTerm = deeds.FaresMortgageTerm;
            FaresIrisApn = deeds.FaresIrisApn;
        }
        public string State { get; set; }
        public string CountyName { get; set; }
        public string LenderName { get; set; }
        public string LegalBriefDescription { get; set; }
        public string DocumentTypeCode { get; set; }
        public string DocumentTypeDesc { get; set; }
        public string ArmResetDate { get; set; }
        public string DocumentNumber { get; set; }
        public string RecorderBookNumber { get; set; }
        public string RecorderPageNumber { get; set; }
        public string LandLotSize { get; set; }
        public string CityTransferTax { get; set; }
        public string CountyTransferTax { get; set; }
        public string TotalTransferTax { get; set; }
        public string PropertyUseCode { get; set; }
        public string PropertyUseDesc { get; set; }
        public string FirstTdLoanAmount { get; set; }
        public string FirstTdLoanTypeCode { get; set; }
        public string FirstTdLoanTypeDesc { get; set; }
        public string TypeFinancing { get; set; }
        public string FirstTdInterestRate { get; set; }
        public string FirstTdDueDate { get; set; }
        public string TitleCompanyName { get; set; }
        public string FaresTransactionType { get; set; }
        public string FaresTransactionTypeDesc { get; set; }
        public string FaresMortgageDeedType { get; set; }
        public string FaresMortgageDeedTypeDesc { get; set; }
        public string FaresMortgageTermCode { get; set; }
        public string FaresMortgageTermCodeDesc { get; set; }
        public string FaresMortgageTerm { get; set; }
        public string FaresIrisApn { get; set; }
    }
}
