using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBusinessRegistrationSection
    {
        uint BusinessRegistrationsRecordCount { get; set; }
        List<IBusinessRegistrationRecord> BusinessRegistrations { get; set; }
    }
}
