using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IPropertyForeclosure
    {
        IDate RecordingDate { get; set; }
        IDate AuctionDate { get; set; }
        string PlaintiffName1 { get; set; }
        string PlaintiffName2 { get; set; }
        string PlaintiffCompanyName { get; set; }
        string DefendantName1 { get; set; }
        string DefendantName2 { get; set; }
        string DefendantName3 { get; set; }
        string DefendantName4 { get; set; }
        string DefendantCompanyName1 { get; set; }
        string DefendantCompanyName2 { get; set; }
        string DefendantCompanyName3 { get; set; }
        string DefendantCompanyName4 { get; set; }
        IAddress SiteAddress1 { get; set; }
        IAddress SiteAddress2 { get; set; }
        string LenderFirstName { get; set; }
        string LenderLastName { get; set; }
        string LenderCompanyName { get; set; }
        string AttorneyName { get; set; }
        string AttorneyPhoneNumber { get; set; }
        IAddress Address { get; set; }
        string DocumentType { get; set; }
        List<ISourceDocInfo> FSourceDocs { get; set; }
    }
}
