namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IWsException
	{
		 string Source { get; set; }
		 int Code { get; set; }
		 string Location { get; set; }
		 string Message { get; set; }
	}
}
