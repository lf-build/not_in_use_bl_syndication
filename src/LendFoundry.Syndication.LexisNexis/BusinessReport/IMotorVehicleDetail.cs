using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IMotorVehicleDetail
    {
        List<ISourceDocInfo> SourceDocs { get; set; }
        string Vin { get; set; }
        string VehicleType { get; set; }
        int ModelYear { get; set; }
        string Make { get; set; }
        string Series { get; set; }
        string Style { get; set; }
        string BasePrice { get; set; }
        string StateOfOrigin { get; set; }
        string Color { get; set; }
        string VehicleUse { get; set; }
        string NumberOfCylinders { get; set; }
        string EngineSize { get; set; }
        string FuelTypeName { get; set; }
        string Restraints { get; set; }
        string AntiLockBrakes { get; set; }
        string AirConditioning { get; set; }
        string DaytimeRunningLights { get; set; }
        string PowerSteering { get; set; }
        string PowerBrakes { get; set; }
        string PowerWindows { get; set; }
        string SecuritySystem { get; set; }
        string Roof { get; set; }
        string Radio { get; set; }
        string FrontWheelDrive { get; set; }
        string FourWheelDrive { get; set; }
        string TiltWheel { get; set; }
        bool NonDmvSource { get; set; }
        List<IMotorVehicleParty> Parties { get; set; }
    }
}
