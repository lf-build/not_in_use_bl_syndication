namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IncorporationHistory : IIncorporationHistory
    {
        public IncorporationHistory()
        { }
        public IncorporationHistory(ServiceReference.TopBusinessIncorporationHistory incorporationHistory)
        {
            if (incorporationHistory == null)
                return;
            Date = new Date(incorporationHistory.Date);
            Description = incorporationHistory.Description;
        }
        public IDate Date { get; set; }
        public string Description { get; set; }
    }
}
