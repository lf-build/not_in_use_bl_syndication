namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IncorporationAddress : IIncorporationAddress
    {
        public IncorporationAddress()
        { }
        public IncorporationAddress(ServiceReference.TopBusinessIncorporationAddress incorporationAddress)
        {
            if (incorporationAddress == null)
                return;
            Address = new Address(incorporationAddress.Address);
            AddressType = incorporationAddress.AddressType;
        }
        public IAddress Address { get; set; }
        public string AddressType { get; set; }
    }
}
