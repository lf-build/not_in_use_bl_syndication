namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ISanctionRecord
	{
		 string SanctionId { get; set; }
		 string UniqueId { get; set; }
		 ISanctionProvider ProviderDetail { get; set; }
		 ISanctionFiling FilingDetail { get; set; }
	}
}
