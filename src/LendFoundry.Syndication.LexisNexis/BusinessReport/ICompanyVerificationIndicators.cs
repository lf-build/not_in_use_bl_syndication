namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ICompanyVerificationIndicators
	{
		 bool CompanyName { get; set; }
		 bool Address { get; set; }
		 bool City { get; set; }
		 bool State { get; set; }
		 bool Zip { get; set; }
		 bool Phone10 { get; set; }
		 bool Fein { get; set; }
	}
}
