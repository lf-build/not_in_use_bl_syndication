using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IAircraftSummary
    {
        int CurrentRecordCount { get; set; }
        int TotalCurrentRecordCount { get; set; }
        List<IAircraft> CurrentAircrafts { get; set; }
        List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        int PriorRecordCount { get; set; }
        int TotalPriorRecordCount { get; set; }
        List<IAircraft> PriorAircrafts { get; set; }
        List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
