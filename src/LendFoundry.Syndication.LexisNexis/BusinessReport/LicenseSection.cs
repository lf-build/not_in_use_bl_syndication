namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class LicenseSection : ILicenseSection
    {
        public LicenseSection()
        { }
        public LicenseSection(ServiceReference.TopBusinessLicenseSection licenseSection)
        {
            if (licenseSection == null)
                return;
            LicenseRecordCount = licenseSection.LicenseRecordCount;
            LicenseTotalCount = licenseSection.LicenseTotalCount;
            LicenseRecords = new LicenseSummary(licenseSection.LicenseRecords);
        }
        public uint LicenseRecordCount { get; set; }
        public uint LicenseTotalCount { get; set; }
        public ILicenseSummary LicenseRecords { get; set; }
    }
}
