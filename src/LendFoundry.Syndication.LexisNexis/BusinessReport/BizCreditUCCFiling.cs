using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditUccFiling : IBizCreditUccFiling
    {
        public BizCreditUccFiling()
        { }
        public BizCreditUccFiling(ServiceReference.BizCreditUCCFiling bizCreditUccFiling)
        {
            if (bizCreditUccFiling == null)
                return;
            Type = bizCreditUccFiling.Type;
            Action = bizCreditUccFiling.Action;
            DocumentNumber = bizCreditUccFiling.DocumentNumber;
            FilingLocation = bizCreditUccFiling.FilingLocation;
            if (bizCreditUccFiling.Collaterals != null)
            {
                Collaterals = new List<ICodeMap>(bizCreditUccFiling.Collaterals.Select(collateral => new CodeMap(collateral)));
            }
            AdditionalCollateralCodes = bizCreditUccFiling.AdditionalCollateralCodes;
            SecuredParty = bizCreditUccFiling.SecuredParty;
            Assignee = bizCreditUccFiling.Assignee;
            FileState = bizCreditUccFiling.FileState;
            FileNumber = bizCreditUccFiling.FileNumber;
            DisputeIndicator = bizCreditUccFiling.DisputeIndicator;
            DisputeCode = bizCreditUccFiling.DisputeCode;
            DateFiled = new Date(bizCreditUccFiling.DateFiled);
        }
        public string Type { get; set; }
        public string Action { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public List<ICodeMap> Collaterals { get; set; }
        public string AdditionalCollateralCodes { get; set; }
        public string SecuredParty { get; set; }
        public string Assignee { get; set; }
        public string FileState { get; set; }
        public string FileNumber { get; set; }
        public string DisputeIndicator { get; set; }
        public string DisputeCode { get; set; }
        public IDate DateFiled { get; set; }
    }
}
