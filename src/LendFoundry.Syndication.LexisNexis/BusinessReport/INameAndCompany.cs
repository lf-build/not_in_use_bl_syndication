namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface INameAndCompany
	{
		 string Full { get; set; }
		 string First { get; set; }
		 string Middle { get; set; }
		 string Last { get; set; }
		 string Suffix { get; set; }
		 string Prefix { get; set; }
		 string CompanyName { get; set; }
	}
}
