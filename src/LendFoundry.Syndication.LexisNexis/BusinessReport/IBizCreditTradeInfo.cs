namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTradeInfo
	{
		 string HighestCredit { get; set; }
		 string MedianCredit { get; set; }
		 string NumberCombinedLines { get; set; }
		 string CombinedDaysBeyondTermTotal { get; set; }
		 string CombinedBalance { get; set; }
		 string NumberAgedLines { get; set; }
		 string TotalAccountBalance { get; set; }
		 string CombinedAccountBalance { get; set; }
		 string CollectionCount { get; set; }
		 string NumberAgedTradesDelinquentCurrent { get; set; }
		 string NumberAgedTradesDelinquent1To30 { get; set; }
		 string NumberAgedTradesDelinquent31To60 { get; set; }
		 string NumberAgedTradesDelinquent61To90 { get; set; }
		 string NumberAgedTradesDelinquent91Plus { get; set; }
		 IBizCreditDaysBeyondTerm DaysBeyondTerm { get; set; }
	}
}
