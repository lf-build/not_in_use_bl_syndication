using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BusinessRegistrationSection : IBusinessRegistrationSection
    {
        public BusinessRegistrationSection()
        { }
        public BusinessRegistrationSection(ServiceReference.TopBusinessBusinessRegistrationSection businessRegistrationSection)
        {
            if (businessRegistrationSection == null)
                return;
            BusinessRegistrationsRecordCount = businessRegistrationSection.BusinessRegistrationsRecordCount;
            if (businessRegistrationSection.BusinessRegistrations != null)
            {
                var businessRegistrations = new List<IBusinessRegistrationRecord>();
                foreach (var businessRegistration in businessRegistrationSection.BusinessRegistrations)
                {
                    businessRegistrations.Add(new BusinessRegistrationRecord(businessRegistration));
                }
                BusinessRegistrations = businessRegistrations;
            }
        }
        public uint BusinessRegistrationsRecordCount { get; set; }
        public List<IBusinessRegistrationRecord> BusinessRegistrations { get; set; }
    }
}
