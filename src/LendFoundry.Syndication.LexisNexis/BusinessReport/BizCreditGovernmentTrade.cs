namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditGovernmentTrade : IBizCreditGovernmentTrade
	{
        public BizCreditGovernmentTrade()
        { }
        public BizCreditGovernmentTrade(ServiceReference.BizCreditGovernmentTrade bizCreditGovernmentTrade)
        {
            if (bizCreditGovernmentTrade == null)
                return;
            PaymentIndicator = bizCreditGovernmentTrade.PaymentIndicator;
            BusinessCategory = bizCreditGovernmentTrade.BusinessCategory;
            PaymentTerms = bizCreditGovernmentTrade.PaymentTerms;
            HighCreditMask = bizCreditGovernmentTrade.HighCreditMask;
            RecentHighCredit = bizCreditGovernmentTrade.RecentHighCredit;
            AccountBalanceMask = bizCreditGovernmentTrade.AccountBalanceMask;
            MaskedAccountBalance = bizCreditGovernmentTrade.MaskedAccountBalance;
            DbtPercentages = new DebtBeyondTermsPercent(bizCreditGovernmentTrade.DBTPercentages);
            Comments = bizCreditGovernmentTrade.Comments;
            NewTradeFlag = bizCreditGovernmentTrade.NewTradeFlag;
            DateReported = new Date(bizCreditGovernmentTrade.DateReported);
            DateLastSale = new Date(bizCreditGovernmentTrade.DateLastSale);
            DisputeIndicator = bizCreditGovernmentTrade.DisputeIndicator;
            DisputeCode = bizCreditGovernmentTrade.DisputeCode;
        }
        public string PaymentIndicator { get; set; }
		public string BusinessCategory { get; set; }
		public string PaymentTerms { get; set; }
		public string HighCreditMask { get; set; }
		public int RecentHighCredit { get; set; }
		public string AccountBalanceMask { get; set; }
		public int MaskedAccountBalance { get; set; }
	    public IDebtBeyondTermsPercent DbtPercentages { get; set; }
		public string Comments { get; set; }
		public string NewTradeFlag { get; set; }
		public string TradeType { get; set; }
		public IDate DateReported { get; set; }
		public IDate DateLastSale { get; set; }
		public string DisputeIndicator { get; set; }
		public string DisputeCode { get; set; }
	}
}
