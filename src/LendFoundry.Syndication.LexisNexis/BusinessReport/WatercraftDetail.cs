using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class WatercraftDetail : IWatercraftDetail
    {
        public WatercraftDetail()
        { }
        public WatercraftDetail(ServiceReference.TopBusinessWatercraftDetail watercraftDetail)
        {
            if (watercraftDetail == null)
                return;
            if (watercraftDetail.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(watercraftDetail.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }

            StateJurisdiction = watercraftDetail.StateJurisdiction;
            HullNumber = watercraftDetail.HullNumber;
            ModelYear = watercraftDetail.ModelYear;
            VesselMake = watercraftDetail.VesselMake;
            VesselModel = watercraftDetail.VesselModel;
            Propulsion = watercraftDetail.Propulsion;
            Length = watercraftDetail.Length;
            Use = watercraftDetail.Use;
            VesselName = watercraftDetail.VesselName;
            RegistrationStatus = watercraftDetail.RegistrationStatus;
            RegistrationNumber = watercraftDetail.RegistrationNumber;
            RegistrationDate = new Date(watercraftDetail.RegistrationDate);
            ExpirationDate = new Date(watercraftDetail.ExpirationDate);
            NonDmvSource = watercraftDetail.NonDMVSource;

            if (watercraftDetail.PriorParties != null)
            {
                PriorParties = new List<IWatercraftParty>(watercraftDetail.PriorParties.Select(priorParty => new WatercraftParty(priorParty)));
            }
            if (watercraftDetail.CurrentParties != null)
            {
                CurrentParties = new List<IWatercraftParty>(watercraftDetail.CurrentParties.Select(currentParty => new WatercraftParty(currentParty)));
            }
        }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public string StateJurisdiction { get; set; }
        public string HullNumber { get; set; }
        public int ModelYear { get; set; }
        public bool ModelYearSpecified { get; set; }
        public string VesselMake { get; set; }
        public string VesselModel { get; set; }
        public string Propulsion { get; set; }
        public string Length { get; set; }
        public string Use { get; set; }
        public string VesselName { get; set; }
        public string RegistrationStatus { get; set; }
        public string RegistrationNumber { get; set; }
        public IDate RegistrationDate { get; set; }
        public IDate ExpirationDate { get; set; }
        public bool NonDmvSource { get; set; }
        public bool NonDmvSourceSpecified { get; set; }
        public List<IWatercraftParty> PriorParties { get; set; }
        public List<IWatercraftParty> CurrentParties { get; set; }
    }
}
