namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditJudgmentTotal
	{
		 int Filed { get; set; }
		 int FiledLast24Months { get; set; }
		 int NonFiled { get; set; }
		 string LiabilityAmountFiled { get; set; }
		 string LiabilityAmountFiledLast24Months { get; set; }
		 string LiabilityAmountNonFiled { get; set; }
		 int MonthsSinceLastFiling { get; set; }
		 int MonthsSinceLastNonFiling { get; set; }
		 string JudgmentAmount { get; set; }
		 string PercentJudgmentAmountToTradeBalance { get; set; }
		 bool PaymentReceived { get; set; }
	}
}
