namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IRegisteredAgentEntity
	{
		 IName Name { get; set; }
		 IAddress Address { get; set; }
		 string CompanyName { get; set; }
		 string Phone { get; set; }
		 string UniqueId { get; set; }
		 IBusinessIdentity BusinessIds { get; set; }
		 string Title { get; set; }
		 bool IsDeceased { get; set; }
		 bool HasDerog { get; set; }
		 IDate FromDate { get; set; }
		 IDate ToDate { get; set; }
	}
}
