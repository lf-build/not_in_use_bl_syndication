namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IGeoAddress
	{
		 IAddress Address { get; set; }
		 IGeoLocation Location { get; set; }
	}
}
