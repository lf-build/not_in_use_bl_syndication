using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIndustryRecord
    {
        List<ISourceDocInfo> SourceDocs { get; set; }
        List<IIndustrySic> Sics { get; set; }
        List<IIndustryNaics> Naics { get; set; }
        string IndustryDescription { get; set; }
        string BusinessDescription { get; set; }
    }
}
