using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PropertySummary : IPropertySummary
    {
        public PropertySummary()
        { }
        public PropertySummary(ServiceReference.TopBusinessPropertySummary propertySummary)
        {
            if (propertySummary == null)
                return;
            CurrentRecordsCount = propertySummary.CurrentRecordsCount;
            TotalCurrentRecordsCount = propertySummary.TotalCurrentRecordsCount;
            PriorRecordsCount = propertySummary.PriorRecordsCount;
            TotalPriorRecordsCount = propertySummary.TotalPriorRecordsCount;
            if (propertySummary.Properties != null)
            {
                Properties = new List<IProperty>(propertySummary.Properties.Select(property => new Property(property)));
            }
            if (propertySummary.CurrentSourceDocs != null)
            {
                CurrentSourceDocs = new List<ISourceDocInfo>(propertySummary.CurrentSourceDocs.Select(currentSourceDoc => new SourceDocInfo(currentSourceDoc)));
            }
            if (propertySummary.PriorSourceDocs != null)
            {
                PriorSourceDocs = new List<ISourceDocInfo>(propertySummary.PriorSourceDocs.Select(priorSourceDoc => new SourceDocInfo(priorSourceDoc)));
            }
            DerogSummaryCntForeclosureNod = propertySummary.DerogSummaryCntForeclosureNOD;
            ForeclosureNodRecordCount = propertySummary.ForeclosureNODRecordCount;
        }
        public int CurrentRecordsCount { get; set; }
        public int TotalCurrentRecordsCount { get; set; }
        public int PriorRecordsCount { get; set; }
        public int TotalPriorRecordsCount { get; set; }
        public List<IProperty> Properties { get; set; }
        public List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        public List<ISourceDocInfo> PriorSourceDocs { get; set; }
        public int DerogSummaryCntForeclosureNod { get; set; }
        public int ForeclosureNodRecordCount { get; set; }
    }
}
