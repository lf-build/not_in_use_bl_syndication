using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SourceSection : ISourceSection
    {
        public SourceSection()
        { }
        public SourceSection(ServiceReference.TopBusinessSourceSection sourceSection)
        {
            if (sourceSection == null)
                return;
            AllSourcesCount = sourceSection.AllSourcesCount;
            if (sourceSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(sourceSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            if (sourceSection.Categories != null)
            {
                Categories = new List<ISourceCategory>(sourceSection.Categories.Select(category => new SourceCategory(category)));
            }
        }
        public uint AllSourcesCount { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public List<ISourceCategory> Categories { get; set; }
    }
}
