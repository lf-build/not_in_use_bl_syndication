namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIncorporationHistory
	{
		 IDate Date { get; set; }
		 string Description { get; set; }
	}
}
