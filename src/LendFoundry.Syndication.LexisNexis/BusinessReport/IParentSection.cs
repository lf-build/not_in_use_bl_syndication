using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IParentSection
    {
        List<IRelativeRec> Parents { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
