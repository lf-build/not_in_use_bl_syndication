using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIndividual
    {
        IName Name { get; set; }
        IAddress Address { get; set; }
        string UniqueId { get; set; }
        string Ssn { get; set; }
        bool IsDeceased { get; set; }
        bool HasDerog { get; set; }
        long ExecutiveElsewhere { get; set; }
        IPosition BestPosition { get; set; }
        List<IPosition> OtherCurrents { get; set; }
        List<IPosition> AllOthers { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
