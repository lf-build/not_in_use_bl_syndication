using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UccFiling : IUccFiling
    {
        public UccFiling()
        { }
        public UccFiling(ServiceReference.TopBusinessUCCFiling uccFiling)
        {
            if (uccFiling == null)
                return;
            FilingNumber = uccFiling.FilingNumber;
            FilingDate = new Date(uccFiling.FilingDate);
            FilingType = uccFiling.FilingType;
            ExpirationDate = new Date(uccFiling.ExpirationDate);
            if (uccFiling.Debtors != null)
            {
                Debtors = new List<IUccParty>(uccFiling.Debtors.Select(debtor => new UccParty(debtor)));
            }
            if (uccFiling.Secureds != null)
            {
                Secureds = new List<IUccParty>(uccFiling.Secureds.Select(secured => new UccParty(secured)));
            }
            if (uccFiling.Assignees != null)
            {
                Assignees = new List<IUccParty>(uccFiling.Assignees.Select(assignee => new UccParty(assignee)));
            }
            if (uccFiling.Collaterals != null)
            {
                Collaterals = new List<IUccCollateral>(uccFiling.Collaterals.Select(collateral => new UccCollateral(collateral)));
            }
        }
        public string FilingNumber { get; set; }
        public IDate FilingDate { get; set; }
        public string FilingType { get; set; }
        public IDate ExpirationDate { get; set; }
        public List<IUccParty> Debtors { get; set; }
        public List<IUccParty> Secureds { get; set; }
        public List<IUccParty> Assignees { get; set; }
        public List<IUccCollateral> Collaterals { get; set; }
    }
}
