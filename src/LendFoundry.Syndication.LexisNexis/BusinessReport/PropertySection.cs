namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class PropertySection : IPropertySection
	{
        public PropertySection()
        { }
        public PropertySection(ServiceReference.TopBusinessPropertySection propertySection)
        {
            if (propertySection == null)
                return;
            PropertyRecordCount = propertySection.PropertyRecordCount;
            TotalPropertyRecordCount = propertySection.TotalPropertyRecordCount;
            PropertyRecords = new PropertySummary(propertySection.PropertyRecords);
        }
        public int PropertyRecordCount { get; set; }
		public int TotalPropertyRecordCount { get; set; }
		public IPropertySummary PropertyRecords { get; set; }
	}
}
