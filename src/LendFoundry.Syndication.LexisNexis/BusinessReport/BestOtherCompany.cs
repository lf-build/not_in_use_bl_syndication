using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BestOtherCompany : IBestOtherCompany
    {
        public BestOtherCompany()
        { }
        public BestOtherCompany(ServiceReference.TopBusinessBestOtherCompany bestOtherCompany)
        {
            if (bestOtherCompany == null)
                return;
            CompanyName = bestOtherCompany.CompanyName;
            DateFirstSeen = new Date(bestOtherCompany.DateFirstSeen);
            DateLastSeen = new Date(bestOtherCompany.DateLastSeen);
            Status = bestOtherCompany.Status;
            if (bestOtherCompany.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(bestOtherCompany.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string CompanyName { get; set; }
        public IDate DateFirstSeen { get; set; }
        public IDate DateLastSeen { get; set; }
        public bool Status { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
