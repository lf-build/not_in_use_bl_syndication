using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ISanctionSection
    {
        List<ISanctionRecord> Sanctions { get; set; }
    }
}
