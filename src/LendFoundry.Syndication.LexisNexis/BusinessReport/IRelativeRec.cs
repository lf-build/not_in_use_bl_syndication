using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IRelativeRec
    {
        List<ISourceDocInfo> SourceDocs { get; set; }
        List<IRelationshipRec> Relationships { get; set; }
        IName Name { get; set; }
        string UniqueId { get; set; }
        string CompanyName { get; set; }
        IBusinessIdentity BusinessIds { get; set; }
        IAddress Address { get; set; }
        IPhoneInfo PhoneInfo { get; set; }
        string PhoneType { get; set; }
        bool ActiveEDA { get; set; }
        bool Disconnected { get; set; }
        string WirelessIndicator { get; set; }
        bool HasDerog { get; set; }
    }
}
