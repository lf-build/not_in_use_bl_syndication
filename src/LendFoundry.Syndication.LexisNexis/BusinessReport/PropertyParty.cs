using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PropertyParty : IPropertyParty
    {
        public PropertyParty()
        { }
        public PropertyParty(ServiceReference.TopBusinessPropertyParty propertyParty)
        {
            if (propertyParty.Owners != null)
            {
                Owners = new List<IPropertyTransaction>(propertyParty.Owners.Select(owner => new PropertyTransaction(owner)));
            }
            if (propertyParty.Sellers != null)
            {
                Sellers = new List<IPropertyTransaction>(propertyParty.Sellers.Select(seller => new PropertyTransaction(seller)));
            }
            if (propertyParty.Borrowers != null)
            {
                Borrowers = new List<IPropertyTransaction>(propertyParty.Borrowers.Select(borrower => new PropertyTransaction(borrower)));
            }
            if (propertyParty.Mortgages != null)
            {
                Mortgages = new List<IPropertyMortgageInfo>(propertyParty.Mortgages.Select(mortgage => new PropertyMortgageInfo(mortgage)));
            }
            if (propertyParty.Foreclosures != null)
            {
                Foreclosures = new List<IPropertyForeclosure>(propertyParty.Foreclosures.Select(foreclosure => new PropertyForeclosure(foreclosure)));
            }
            if (propertyParty.NoticeOfDefaults != null)
            {
                NoticeOfDefaults = new List<IPropertyForeclosure>(propertyParty.NoticeOfDefaults.Select(noticeOfDefault => new PropertyForeclosure(noticeOfDefault)));
            }
        }
        public List<IPropertyTransaction> Owners { get; set; }
        public List<IPropertyTransaction> Sellers { get; set; }
        public List<IPropertyTransaction> Borrowers { get; set; }
        public List<IPropertyMortgageInfo> Mortgages { get; set; }
        public List<IPropertyForeclosure> Foreclosures { get; set; }
        public List<IPropertyForeclosure> NoticeOfDefaults { get; set; }
    }
}
