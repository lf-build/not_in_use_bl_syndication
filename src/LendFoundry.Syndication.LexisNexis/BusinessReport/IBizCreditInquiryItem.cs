using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditInquiryItem
    {
        string InquiryType { get; set; }
        List<IBizCreditInquiryCount> InquiryCounts { get; set; }
    }
}
