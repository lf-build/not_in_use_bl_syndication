namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditExecutiveSummary : IBizCreditExecutiveSummary
    {
        public BizCreditExecutiveSummary()
        { }
        public BizCreditExecutiveSummary(ServiceReference.BizCreditExecutiveSummary bizCreditExecutiveSummary)
        {
            if (bizCreditExecutiveSummary == null)
                return;
            CurrentDaysBeyondTerms = bizCreditExecutiveSummary.CurrentDaysBeyondTerms;
            PredictedDaysBeyondTerms = bizCreditExecutiveSummary.PredictedDaysBeyondTerms;
            DateOfPrediction = new Date(bizCreditExecutiveSummary.DateOfPrediction);
            AverageIndustryDbt = bizCreditExecutiveSummary.AverageIndustryDBT;
            IndustryDescription = bizCreditExecutiveSummary.IndustryDescription;
            AverageAllIndustryDbt = bizCreditExecutiveSummary.AverageAllIndustryDBT;
            ConfidencePercent = bizCreditExecutiveSummary.ConfidencePercent;
            ConfidenceSlope = bizCreditExecutiveSummary.ConfidenceSlope;
            LowBalance = bizCreditExecutiveSummary.LowBalance;
            HighBalance = bizCreditExecutiveSummary.HighBalance;
            CurrentBalance = bizCreditExecutiveSummary.CurrentBalance;
            HighestCreditExtended = bizCreditExecutiveSummary.HighestCreditExtended;
            MedianOfCreditExtended = bizCreditExecutiveSummary.MedianOfCreditExtended;
            PaymentPerformance = bizCreditExecutiveSummary.PaymentPerformance;
            PaymentTrend = bizCreditExecutiveSummary.PaymentTrend;
        }
        public int CurrentDaysBeyondTerms { get; set; }
        public int PredictedDaysBeyondTerms { get; set; }
        public IDate DateOfPrediction { get; set; }
        public int AverageIndustryDbt { get; set; }
        public string IndustryDescription { get; set; }
        public int AverageAllIndustryDbt { get; set; }
        public int ConfidencePercent { get; set; }
        public int ConfidenceSlope { get; set; }
        public int LowBalance { get; set; }
        public int HighBalance { get; set; }
        public int CurrentBalance { get; set; }
        public int HighestCreditExtended { get; set; }
        public int MedianOfCreditExtended { get; set; }
        public string PaymentPerformance { get; set; }
        public string PaymentTrend { get; set; }
    }
}
