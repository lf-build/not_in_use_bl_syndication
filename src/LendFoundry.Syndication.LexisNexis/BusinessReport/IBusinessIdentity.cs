namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBusinessIdentity
    {
        long DotId { get; set; }
        long EmpId { get; set; }
        long PowId { get; set; }
        long ProxId { get; set; }
        long SeleId { get; set; }
        long OrgId { get; set; }
        long UltId { get; set; }
        bool DotIdSpecified { get; set; }
        bool EmpIdSpecified { get; set; }
        bool PowIdSpecified { get; set; }
        bool ProxIdSpecified { get; set; }
        bool SeleIdSpecified { get; set; }
        bool OrgIdSpecified { get; set; }
        bool UltIdSpecified { get; set; }
    }
}
