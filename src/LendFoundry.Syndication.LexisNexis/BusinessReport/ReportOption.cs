using LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ReportOption : IReportOption
    {
        public ReportOption()
        { }
        public ReportOption(TopBusinessReportOption reportOption)
        {
            if (reportOption == null)
                return;
            IncludeAircrafts = reportOption.IncludeAircrafts;
            IncludeAssociatedBusinesses = reportOption.IncludeAssociatedBusinesses;
            IncludeBankruptcies = reportOption.IncludeBankruptcies;
            IncludeContacts = reportOption.IncludeContacts;
            IncludeFinances = reportOption.IncludeFinances;
            IncludeIndustries = reportOption.IncludeIndustries;
            IncludeProfessionalLicenses = reportOption.IncludeProfessionalLicenses;
            IncludeLiensJudgments = reportOption.IncludeLiensJudgments;
            IncludeMotorVehicles = reportOption.IncludeMotorVehicles;
            IncludeOpsSites = reportOption.IncludeOpsSites;
            IncludeIncorporation = reportOption.IncludeIncorporation;
            IncludeParents = reportOption.IncludeParents;
            IncludeProperties = reportOption.IncludeProperties;
            IncludeUccFilings = reportOption.IncludeUCCFilings;
            IncludeUccFilingsSecureds = reportOption.IncludeUCCFilingsSecureds;
            IncludeInternetDomains = reportOption.IncludeInternetDomains;
            IncludeWatercrafts = reportOption.IncludeWatercrafts;
            IncludeSourceCounts = reportOption.IncludeSourceCounts;
            IncludeRegisteredAgents = reportOption.IncludeRegisteredAgents;
            IncludeConnectedBusinesses = reportOption.IncludeConnectedBusinesses;
            IncludeNameVariations = reportOption.IncludeNameVariations;
            IncludeIrs5500 = reportOption.IncludeIRS5500;
            IncludeExperianBusinessReports = reportOption.IncludeExperianBusinessReports;
            IncludeCompanyVerification = reportOption.IncludeCompanyVerification;
            IncludeDunBradStreet = reportOption.IncludeDunBradStreet;
            IncludeSanctions = reportOption.IncludeSanctions;
            IncludeBusinessRegistrations = reportOption.IncludeBusinessRegistrations;
            BusinessReportFetchLevel = (LevelType)(int)reportOption.BusinessReportFetchLevel;
        }
        public bool IncludeAircrafts { get; set; }
        public bool IncludeAssociatedBusinesses { get; set; }
        public bool IncludeBankruptcies { get; set; }
        public bool IncludeContacts { get; set; }
        public bool IncludeFinances { get; set; }
        public bool IncludeIndustries { get; set; }
        public bool IncludeProfessionalLicenses { get; set; }
        public bool IncludeLiensJudgments { get; set; }
        public bool IncludeMotorVehicles { get; set; }
        public bool IncludeOpsSites { get; set; }
        public bool IncludeIncorporation { get; set; }
        public bool IncludeParents { get; set; }
        public bool IncludeProperties { get; set; }
        public bool IncludeUccFilings { get; set; }
        public bool IncludeUccFilingsSecureds { get; set; }
        public bool IncludeInternetDomains { get; set; }
        public bool IncludeWatercrafts { get; set; }
        public bool IncludeSourceCounts { get; set; }
        public bool IncludeRegisteredAgents { get; set; }
        public bool IncludeConnectedBusinesses { get; set; }
        public bool IncludeNameVariations { get; set; }
        public bool IncludeIrs5500 { get; set; }
        public bool IncludeExperianBusinessReports { get; set; }
        public bool IncludeCompanyVerification { get; set; }
        public bool IncludeDunBradStreet { get; set; }
        public bool IncludeSanctions { get; set; }
        public bool IncludeBusinessRegistrations { get; set; }
        public LevelType BusinessReportFetchLevel { get; set; }
    }
}
