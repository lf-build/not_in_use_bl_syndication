namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditDemographic5610 : IBizCreditDemographic5610
	{
        public BizCreditDemographic5610()
        { }
        public BizCreditDemographic5610(ServiceReference.BizCreditDemographic5610 bizCreditDemographic5610)
        {
            if (bizCreditDemographic5610 == null)
                return;
            FiscalYearEndMonth = bizCreditDemographic5610.FiscalYearEndMonth;
            ProfitRange = bizCreditDemographic5610.ProfitRange;
            ProfitRangeActual = bizCreditDemographic5610.ProfitRangeActual;
            NetWorth = bizCreditDemographic5610.NetWorth;
            NetWorthActual = bizCreditDemographic5610.NetWorthActual;
            InBuildingSinceYear = bizCreditDemographic5610.InBuildingSinceYear;
            RentOrOwn = bizCreditDemographic5610.RentOrOwn;
            BuildingSquareFeet = bizCreditDemographic5610.BuildingSquareFeet;
            ActiveCustomerCount = bizCreditDemographic5610.ActiveCustomerCount;
            Ownership = bizCreditDemographic5610.Ownership;
            CorporateName = bizCreditDemographic5610.CorporateName;
            CorporateCity = bizCreditDemographic5610.CorporateCity;
            CorporateState = bizCreditDemographic5610.CorporateState;
            CorporateStateName = bizCreditDemographic5610.CorporateStateName;
            CorporatePhone = bizCreditDemographic5610.CorporatePhone;
            TimeZone = bizCreditDemographic5610.TimeZone;
            OfficerTitle = bizCreditDemographic5610.OfficerTitle;
            OrigOfficerName = new Name(bizCreditDemographic5610.OrigOfficerName);
            OfficerName = new Name(bizCreditDemographic5610.OfficerName);
            UniqueId = bizCreditDemographic5610.UniqueId;
            Ssn = bizCreditDemographic5610.SSN;
        }
        public int FiscalYearEndMonth { get; set; }
		public string ProfitRange { get; set; }
		public int ProfitRangeActual { get; set; }
		public string NetWorth { get; set; }
		public int NetWorthActual { get; set; }
		public int InBuildingSinceYear { get; set; }
		public string RentOrOwn { get; set; }
		public int BuildingSquareFeet { get; set; }
		public int ActiveCustomerCount { get; set; }
		public string Ownership { get; set; }
		public string CorporateName { get; set; }
		public string CorporateCity { get; set; }
		public string CorporateState { get; set; }
		public string CorporateStateName { get; set; }
		public string CorporatePhone { get; set; }
		public string TimeZone { get; set; }
		public string OfficerTitle { get; set; }
		public IName OrigOfficerName { get; set; }
		public IName OfficerName { get; set; }
		public string UniqueId { get; set; }
		public string Ssn { get; set; }
	}
}
