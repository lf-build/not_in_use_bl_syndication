namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SanctionFiling : ISanctionFiling
    {
        public SanctionFiling()
        { }
        public SanctionFiling(ServiceReference.TopBusinessSanctionFiling sanctionFiling)
        {
            if (sanctionFiling == null)
                return;
            BusinessName = sanctionFiling.BusinessName;
            Dob = new Date(sanctionFiling.DOB);
            TaxIdNumber = sanctionFiling.TaxIdNumber;
            UPin = sanctionFiling.UPIN;
            ProviderType = sanctionFiling.ProviderType;
            Date = new Date(sanctionFiling.Date);
            LicenseNumber = sanctionFiling.LicenseNumber;
            State = sanctionFiling.State;
            BoardType = sanctionFiling.BoardType;
            SourceDescription = sanctionFiling.SourceDescription;
            Type = sanctionFiling._Type;
            Terms = sanctionFiling.Terms;
            Reason = sanctionFiling.Reason;
            Condition = sanctionFiling.Condition;
            Fines = sanctionFiling.Fines;
            Update = new Date(sanctionFiling.Update);
            DateFirstReported = new Date(sanctionFiling.DateFirstReported);
            DateLastReported = new Date(sanctionFiling.DateLastReported);
            ReinstateDate = new Date(sanctionFiling.ReinstateDate);
            ProcessDate = new Date(sanctionFiling.ProcessDate);
            DateFirstSeen = new Date(sanctionFiling.DateFirstSeen);
            DateLastReported = new Date(sanctionFiling.DateLastSeen);
            FraudAbuseFlag = sanctionFiling.FraudAbuseFlag;
            LossOfLicenseIndicator = sanctionFiling.LossOfLicenseIndicator;
        }
        public string BusinessName { get; set; }
        public IDate Dob { get; set; }
        public string TaxIdNumber { get; set; }
        public string UPin { get; set; }
        public string ProviderType { get; set; }
        public IDate Date { get; set; }
        public string LicenseNumber { get; set; }
        public string State { get; set; }
        public string BoardType { get; set; }
        public string SourceDescription { get; set; }
        public string Type { get; set; }
        public string Terms { get; set; }
        public string Reason { get; set; }
        public string Condition { get; set; }
        public string Fines { get; set; }
        public IDate Update { get; set; }
        public IDate DateFirstReported { get; set; }
        public IDate DateLastReported { get; set; }
        public IDate ReinstateDate { get; set; }
        public IDate ProcessDate { get; set; }
        public IDate DateFirstSeen { get; set; }
        public IDate DateLastSeen { get; set; }
        public string FraudAbuseFlag { get; set; }
        public string LossOfLicenseIndicator { get; set; }
    }
}
