using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class LicenseRecord : ILicenseRecord
    {
        public LicenseRecord()
        { }
        public LicenseRecord(ServiceReference.TopBusinessLicenseRecord licenseRecord)
        {
            if (licenseRecord == null)
                return;
            LicenseNumber = licenseRecord.LicenseNumber;
            Description = licenseRecord.Description;
            Issuer = licenseRecord.Issuer;
            IssueDate = new Date(licenseRecord.IssueDate);
            ExpirationDate = new Date(licenseRecord.ExpirationDate);
            if (licenseRecord.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(licenseRecord.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string LicenseNumber { get; set; }
        public string Description { get; set; }
        public string Issuer { get; set; }
        public IDate IssueDate { get; set; }
        public IDate ExpirationDate { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
