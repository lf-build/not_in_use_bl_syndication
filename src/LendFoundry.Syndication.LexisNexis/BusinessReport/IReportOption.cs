namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IReportOption
	{
		 bool IncludeAircrafts { get; set; }
		 bool IncludeAssociatedBusinesses { get; set; }
		 bool IncludeBankruptcies { get; set; }
		 bool IncludeContacts { get; set; }
		 bool IncludeFinances { get; set; }
		 bool IncludeIndustries { get; set; }
		 bool IncludeProfessionalLicenses { get; set; }
		 bool IncludeLiensJudgments { get; set; }
		 bool IncludeMotorVehicles { get; set; }
		 bool IncludeOpsSites { get; set; }
		 bool IncludeIncorporation { get; set; }
		 bool IncludeParents { get; set; }
		 bool IncludeProperties { get; set; }
		 bool IncludeUccFilings { get; set; }
		 bool IncludeUccFilingsSecureds { get; set; }
		 bool IncludeInternetDomains { get; set; }
		 bool IncludeWatercrafts { get; set; }
		 bool IncludeSourceCounts { get; set; }
		 bool IncludeRegisteredAgents { get; set; }
		 bool IncludeConnectedBusinesses { get; set; }
		 bool IncludeNameVariations { get; set; }
		 bool IncludeIrs5500 { get; set; }
		 bool IncludeExperianBusinessReports { get; set; }
		 bool IncludeCompanyVerification { get; set; }
		 bool IncludeDunBradStreet { get; set; }
		 bool IncludeSanctions { get; set; }
		 bool IncludeBusinessRegistrations { get; set; }
		 LevelType BusinessReportFetchLevel { get; set; }
	}
}
