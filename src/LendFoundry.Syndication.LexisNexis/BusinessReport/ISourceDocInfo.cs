namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ISourceDocInfo
	{
		 IBusinessIdentity BusinessIds { get; set; }
		 string IdType { get; set; }
		 string IdValue { get; set; }
		 string Section { get; set; }
		 string Source { get; set; }
		 IAddress Address { get; set; }
		 INameAndCompany Name { get; set; }
	}
}
