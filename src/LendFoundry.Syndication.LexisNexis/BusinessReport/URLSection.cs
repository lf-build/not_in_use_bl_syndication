using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UrlSection : IUrlSection
    {
        public UrlSection()
        { }
        public UrlSection(ServiceReference.TopBusinessURLSection urlSection)
        {
            if (urlSection == null)
                return;
            UrlRecordCount = urlSection.URLRecordCount;
            if (urlSection.URLs != null)
            {
                Urls = new List<IUrl>(urlSection.URLs.Select(url => new URL(url)));
            }
            if (urlSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(urlSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public int UrlRecordCount { get; set; }
        public List<IUrl> Urls { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
