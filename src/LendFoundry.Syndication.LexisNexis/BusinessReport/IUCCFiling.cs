using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IUccFiling
    {
        string FilingNumber { get; set; }
        IDate FilingDate { get; set; }
        string FilingType { get; set; }
        IDate ExpirationDate { get; set; }
        List<IUccParty> Debtors { get; set; }
        List<IUccParty> Secureds { get; set; }
        List<IUccParty> Assignees { get; set; }
        List<IUccCollateral> Collaterals { get; set; }
    }
}
