namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditBankruptcyTotal
	{
		 string FiledLast9Years9Months { get; set; }
		 int Filed { get; set; }
		 int FiledLast24Months { get; set; }
		 int NonFiled { get; set; }
		 int NonSatisfiedLast24Months { get; set; }
		 string LiabilityBalanceFiled { get; set; }
		 string LiabilityBalanceNonFiled { get; set; }
		 int MonthsSinceLastFiling { get; set; }
		 int MonthsSinceLastNonFiling { get; set; }
		 bool PaymentReceived { get; set; }
	}
}
