using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Bankruptcy : IBankruptcy
    {
        public Bankruptcy()
        { }
        public Bankruptcy(ServiceReference.TopBusinessBankruptcy bankruptcy)
        {
            if (bankruptcy == null)
                return;
            CourtCode = bankruptcy.CourtCode;
            CourtLocation = bankruptcy.CourtLocation;
            Ssn = bankruptcy.SSN;
            TaxId = bankruptcy.TaxID;
            UniqueId = bankruptcy.UniqueID;
            Comment = bankruptcy.Comment;
            FilingStatus = bankruptcy.FilingStatus;
            OriginalFilingType = bankruptcy.OriginalFilingType;
            StatusHistory = bankruptcy.StatusHistory;
            OriginalCaseNumber = bankruptcy.OriginalCaseNumber;
            Status = bankruptcy.Status;
            StatusRaw = bankruptcy.StatusRaw;
            FilingType = bankruptcy.FilingType;
            FilerType = bankruptcy.FilerType;
            StatusDate = new Date(bankruptcy.StatusDate);
            OriginalFilingDate = new Date(bankruptcy.OriginalFilingDate);
            if (bankruptcy.Debtors != null)
            {
                Debtors = new List<IBankruptcyParty>(bankruptcy.Debtors.Select(debtor => new BankruptcyParty(debtor)));
            }
            if (bankruptcy.Attorneys != null)
            {
                Attorneys = new List<IBankruptcyParty>(bankruptcy.Attorneys.Select(attorney => new BankruptcyParty(attorney)));
            }
            if (bankruptcy.Trustees != null)
            {
                Trustees = new List<IBankruptcyParty>(bankruptcy.Trustees.Select(trustee => new BankruptcyParty(trustee)));
            }
            if (bankruptcy.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(bankruptcy.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string CourtCode { get; set; }
        public string CourtLocation { get; set; }
        public string Ssn { get; set; }
        public string TaxId { get; set; }
        public string UniqueId { get; set; }
        public string Comment { get; set; }
        public string FilingStatus { get; set; }
        public string OriginalFilingType { get; set; }
        public string StatusHistory { get; set; }
        public string OriginalCaseNumber { get; set; }
        public string Status { get; set; }
        public string StatusRaw { get; set; }
        public string FilingType { get; set; }
        public string FilerType { get; set; }
        public IDate StatusDate { get; set; }
        public IDate OriginalFilingDate { get; set; }
        public List<IBankruptcyParty> Debtors { get; set; }
        public List<IBankruptcyParty> Attorneys { get; set; }
        public List<IBankruptcyParty> Trustees { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
