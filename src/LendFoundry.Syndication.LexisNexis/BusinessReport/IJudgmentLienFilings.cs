namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IJudgmentLienFilings
	{
		 string FilingNumber { get; set; }
		 string FilingType { get; set; }
		 string FilingStatus { get; set; }
		 IDate FilingDate { get; set; }
		 string Agency { get; set; }
		 string AgencyState { get; set; }
		 string AgencyCounty { get; set; }
	}
}
