namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBankruptcyParty
	{
		 string CompanyName { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
		 string TaxId { get; set; }
		 string Ssn { get; set; }
	}
}
