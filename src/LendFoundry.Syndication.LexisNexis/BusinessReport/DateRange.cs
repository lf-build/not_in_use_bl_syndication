namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class DateRange : IDateRange
	{
        public DateRange()
        { }
        public DateRange(ServiceReference.DateRange dateRange)
        {
            if (dateRange == null)
                return;
            StartDate = new Date(dateRange.StartDate);
            EndDate = new Date(dateRange.EndDate);
        }
		public IDate StartDate { get; set; }
		public IDate EndDate { get; set; }
	}
}
