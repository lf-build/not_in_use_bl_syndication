namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIrsSponsor
	{
		 IAddress Address { get; set; }
		 IName Name { get; set; }
		 uint SponsSignNameScore { get; set; }
		 string FullName { get; set; }
		 string DoingBusinessAs { get; set; }
		 string Fein { get; set; }
		 string Phone { get; set; }
		 string BusinessCode { get; set; }
		 string LastReportedName { get; set; }
		 string LastReportedFein { get; set; }
		 string LastReportedPlanNumber { get; set; }
		 IDate SignatureDate { get; set; }
	}
}
