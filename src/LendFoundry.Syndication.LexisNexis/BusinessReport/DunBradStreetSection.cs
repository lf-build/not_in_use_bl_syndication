namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class DunBradStreetSection : IDunBradStreetSection
    {
        public DunBradStreetSection()
        { }
        public DunBradStreetSection(ServiceReference.TopBusinessDunBradStreetSection dunBradStreetSection)
        {
            if (dunBradStreetSection == null)
                return;
            DnbRecordCount = dunBradStreetSection.DnbRecordCount;
            Dnb = new DnbRecord(dunBradStreetSection.Dnb);
        }
        public uint DnbRecordCount { get; set; }
        public IDnbRecord Dnb { get; set; }
    }
}
