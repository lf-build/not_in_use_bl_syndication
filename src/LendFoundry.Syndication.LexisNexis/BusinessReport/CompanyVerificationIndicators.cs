namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class CompanyVerificationIndicators : ICompanyVerificationIndicators
	{
        public CompanyVerificationIndicators()
        { }
        public CompanyVerificationIndicators(ServiceReference.CompanyVerificationIndicators companyVerificationIndicators)
        {
            if (companyVerificationIndicators == null)
                return;
            CompanyName = companyVerificationIndicators.CompanyName;
            Address = companyVerificationIndicators.Address;
            City = companyVerificationIndicators.City;
            State = companyVerificationIndicators.State;
            Zip = companyVerificationIndicators.Zip;
            Phone10 = companyVerificationIndicators.Phone10;
            Fein = companyVerificationIndicators.FEIN;
        }
        public bool CompanyName { get; set; }
		public bool Address { get; set; }
		public bool City { get; set; }
		public bool State { get; set; }
		public bool Zip { get; set; }
		public bool Phone10 { get; set; }
		public bool Fein { get; set; }
	}
}
