namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditInquiryCount
	{
		 IDate Date { get; set; }
		 int Count { get; set; }
	}
}
