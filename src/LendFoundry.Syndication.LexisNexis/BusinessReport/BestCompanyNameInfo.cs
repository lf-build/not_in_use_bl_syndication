namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BestCompanyNameInfo : IBestCompanyNameInfo
    {
        public BestCompanyNameInfo()
        { }
        public BestCompanyNameInfo(ServiceReference.TopBusinessBestCompanyNameInfo bestCompanyNameInfo)
        {
            if (bestCompanyNameInfo == null)
                return;
            CompanyName = bestCompanyNameInfo.CompanyName;
        }
        public string CompanyName { get; set; }
    }
}
