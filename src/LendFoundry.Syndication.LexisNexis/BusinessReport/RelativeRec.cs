using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class RelativeRec : IRelativeRec
    {
        public RelativeRec()
        { }
        public RelativeRec(ServiceReference.TopBusinessRelativeRec relativeRec)
        {
            if (relativeRec.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(relativeRec.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            if (relativeRec.Relationships != null)
            {
                Relationships = new List<IRelationshipRec>(relativeRec.Relationships.Select(relationship => new RelationshipRec(relationship)));
            }
            Name = new Name(relativeRec.Name);
            UniqueId = relativeRec.UniqueId;
            CompanyName = relativeRec.CompanyName;
            BusinessIds = new BusinessIdentity(relativeRec.BusinessIds);
            Address = new Address(relativeRec.Address);
            PhoneInfo = new PhoneInfo(relativeRec.PhoneInfo);
            PhoneType = relativeRec.PhoneType;
            ActiveEDA = relativeRec.ActiveEDA;
            Disconnected = relativeRec.Disconnected;
            WirelessIndicator = relativeRec.WirelessIndicator;
            HasDerog = relativeRec.HasDerog;
        }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public List<IRelationshipRec> Relationships { get; set; }
        public IName Name { get; set; }
        public string UniqueId { get; set; }
        public string CompanyName { get; set; }
        public IBusinessIdentity BusinessIds { get; set; }
        public IAddress Address { get; set; }
        public IPhoneInfo PhoneInfo { get; set; }
        public string PhoneType { get; set; }
        public bool ActiveEDA { get; set; }
        public bool Disconnected { get; set; }
        public string WirelessIndicator { get; set; }
        public bool HasDerog { get; set; }
    }
}
