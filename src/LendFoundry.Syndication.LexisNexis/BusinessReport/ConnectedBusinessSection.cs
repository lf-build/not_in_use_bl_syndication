using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ConnectedBusinessSection : IConnectedBusinessSection
    {
        public ConnectedBusinessSection()
        { }
        public ConnectedBusinessSection(ServiceReference.TopBusinessConnectedBusinessSection connectedBusinessSection)
        {
            if (connectedBusinessSection == null)
                return;
            CountConnectedBusinesses = connectedBusinessSection.CountConnectedBusinesses;
            TotalCountConnectedBusinesses = connectedBusinessSection.TotalCountConnectedBusinesses;
            if (connectedBusinessSection.ConnectedBusinessRecords != null)
            {
                var connectedBusinessRecords = new List<IConnectedBusiness>();
                foreach (var connectBusinessRecord in connectedBusinessSection.ConnectedBusinessRecords)
                {
                    connectedBusinessRecords.Add(new ConnectedBusiness(connectBusinessRecord));
                }
                ConnectedBusinessRecords = connectedBusinessRecords;
            }
        }
        public int CountConnectedBusinesses { get; set; }
        public int TotalCountConnectedBusinesses { get; set; }
        public List<IConnectedBusiness> ConnectedBusinessRecords { get; set; }
    }
}
