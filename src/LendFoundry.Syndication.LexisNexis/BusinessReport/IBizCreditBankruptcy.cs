namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditBankruptcy
	{
		 string Type { get; set; }
		 string Action { get; set; }
		 string DocumentNumber { get; set; }
		 int LiabilityAmount { get; set; }
		 int AssetAmount { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
		 IDate DateFiled { get; set; }
	}
}
