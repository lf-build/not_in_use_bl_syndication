namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditUccTotal : IBizCreditUccTotal
    {
        public BizCreditUccTotal()
        { }
        public BizCreditUccTotal(ServiceReference.BizCreditUCCTotal bizCreditUccTotal)
        {
            if (bizCreditUccTotal == null)
                return;
            Filed = bizCreditUccTotal.Filed;
            DetailsFiled = bizCreditUccTotal.DetailsFiled;
            SummariesFiled = bizCreditUccTotal.SummariesFiled;
            UnsatisfiedLast24Months = bizCreditUccTotal.UnsatisfiedLast24Months;
            DataIndicator = bizCreditUccTotal.DataIndicator;
        }
        public int Filed { get; set; }
        public int DetailsFiled { get; set; }
        public int SummariesFiled { get; set; }
        public int UnsatisfiedLast24Months { get; set; }
        public string DataIndicator { get; set; }
    }
}
