namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditJudgment : IBizCreditJudgment
	{
        public BizCreditJudgment()
        { }
        public BizCreditJudgment(ServiceReference.BizCreditJudgment bizCreditJudgment)
        {
            if (bizCreditJudgment == null)
                return;
            Type = bizCreditJudgment.Type;
            Action = bizCreditJudgment.Action;
            DocumentNumber = bizCreditJudgment.DocumentNumber;
            FilingLocation = bizCreditJudgment.FilingLocation;
            LiabilityAmount = bizCreditJudgment.LiabilityAmount;
            CreditorName = bizCreditJudgment.CreditorName;
            DisputeIndicator = bizCreditJudgment.DisputeIndicator;
            DisputeCode = bizCreditJudgment.DisputeCode;
            DateFiled = new Date(bizCreditJudgment.DateFiled);
        }
        public string Type { get; set; }
		public string Action { get; set; }
		public string DocumentNumber { get; set; }
		public string FilingLocation { get; set; }
		public int LiabilityAmount { get; set; }
		public string CreditorName { get; set; }
		public string DisputeIndicator { get; set; }
		public string DisputeCode { get; set; }
		public IDate DateFiled { get; set; }
	}
}
