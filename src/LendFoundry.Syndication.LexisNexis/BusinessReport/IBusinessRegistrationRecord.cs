namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBusinessRegistrationRecord
	{
		 IDate RecordDate { get; set; }
		 string CompanyName { get; set; }
		 string Description { get; set; }
		 string Status { get; set; }
		 string FilingNumber { get; set; }
		 IDate FileDate { get; set; }
		 IDate ExpirationDate { get; set; }
		 IDate ProccessDate { get; set; }
		 IAddress Address { get; set; }
		 string CompanyPhone10 { get; set; }
		 string CorpCodeDecode { get; set; }
		 string SosCodeDecode { get; set; }
		 string FilingCodDecode { get; set; }
		 string StatusDecode { get; set; }
		 string FileDateDecode { get; set; }
		 string ProcDateDecode { get; set; }
	}
}
