using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class OperationsSitesSection : IOperationsSitesSection
    {
        public OperationsSitesSection()
        { }
        public OperationsSitesSection(ServiceReference.TopBusinessOperationsSitesSection operationsSitesSection)
        {
            if (operationsSitesSection == null)
                return;
            ReturnedRecordCount = operationsSitesSection.ReturnedRecordCount;
            TotalRecordCount = operationsSitesSection.TotalRecordCount;
            if (operationsSitesSection.OperationsSites != null)
            {
                OperationsSites = new List<IOperationSite>(operationsSitesSection.OperationsSites.Select(operationSite => new OperationSite(operationSite)));
            }
            if (operationsSitesSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(operationsSitesSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public int ReturnedRecordCount { get; set; }
        public int TotalRecordCount { get; set; }
        public List<IOperationSite> OperationsSites { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
