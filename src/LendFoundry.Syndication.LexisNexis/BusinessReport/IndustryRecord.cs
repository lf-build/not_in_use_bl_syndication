using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IndustryRecord : IIndustryRecord
    {
        public IndustryRecord()
        { }
        public IndustryRecord(ServiceReference.TopBusinessIndustryRecord industryRecord)
        {
            if (industryRecord == null)
                return;
            if (industryRecord.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(industryRecord.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            if (industryRecord.SICs != null)
            {
                Sics = new List<IIndustrySic>(industryRecord.SICs.Select(sic => new IndustrySic(sic)));
            }
            if (industryRecord.NAICSs != null)
            {
                Naics = new List<IIndustryNaics>(industryRecord.NAICSs.Select(naic => new IndustryNaics(naic)));
            }
            IndustryDescription = industryRecord.IndustryDescription;
            BusinessDescription = industryRecord.BusinessDescription;
        }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public List<IIndustrySic> Sics { get; set; }
        public List<IIndustryNaics> Naics { get; set; }
        public string IndustryDescription { get; set; }
        public string BusinessDescription { get; set; }
    }
}
