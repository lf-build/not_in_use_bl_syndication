using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ILicenseSummary
    {
        List<ILicenseRecord> Licenses { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
