using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class LienSection : ILienSection
    {
        public LienSection()
        { }
        public LienSection(ServiceReference.TopBusinessLienSection lienSection)
        {
            if (lienSection == null)
                return;
            DerogSummaryCntJl = lienSection.DerogSummaryCntJL;
            ReturnedRecordCount = lienSection.ReturnedRecordCount;
            TotalRecordCount = lienSection.TotalRecordCount;
            if (lienSection.JudgmentsLiens != null)
            {
                JudgmentsLiens = new List<IJudgmentLienDetail>(lienSection.JudgmentsLiens.Select(judgementLien => new JudgmentLienDetail(judgementLien)));
            }
            if (lienSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(lienSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public int DerogSummaryCntJl { get; set; }
        public int ReturnedRecordCount { get; set; }
        public int TotalRecordCount { get; set; }
        public List<IJudgmentLienDetail> JudgmentsLiens { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
