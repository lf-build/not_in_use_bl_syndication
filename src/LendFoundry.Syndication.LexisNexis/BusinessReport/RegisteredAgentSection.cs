using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class RegisteredAgentSection : IRegisteredAgentSection
    {
        public RegisteredAgentSection()
        { }
        public RegisteredAgentSection(ServiceReference.TopBusinessRegisteredAgentSection registeredAgentSection)
        {
            if (registeredAgentSection == null)
                return;
            RegisteredAgentCount = registeredAgentSection.RegisteredAgentCount;
            if (registeredAgentSection.RegisteredAgents != null)
            {
                RegisteredAgents = new List<IRegisteredAgentEntity>(registeredAgentSection.RegisteredAgents.Select(registeredAgent => new RegisteredAgentEntity(registeredAgent)));
            }
        }
        public uint RegisteredAgentCount { get; set; }
        public List<IRegisteredAgentEntity> RegisteredAgents { get; set; }
    }
}
