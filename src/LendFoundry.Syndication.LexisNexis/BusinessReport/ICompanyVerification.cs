namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ICompanyVerification
	{
		 ICompanyVerificationIndicators VerifiedIndicators { get; set; }
		 ICompanyVerificationData VerifiedInputs { get; set; }
	}
}
