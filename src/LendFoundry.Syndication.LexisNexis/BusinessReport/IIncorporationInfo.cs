using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIncorporationInfo
    {
        string StateOfOrigin { get; set; }
        string CorporationName { get; set; }
        string FilingNumber { get; set; }
        string BusinessType { get; set; }
        IDate FilingDate { get; set; }
        string BusinessStatus { get; set; }
        string NameStatus { get; set; }
        string ForeignDomesticIndicator { get; set; }
        string ForeignState { get; set; }
        string FilingType { get; set; }
        string ForProfitIndicator { get; set; }
        string Origin { get; set; }
        List<IIncorporationHistory> CorpHistories { get; set; }
        IIncorporationExistenceTerm TermOfExistence { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
        string NameType { get; set; }
        List<IIncorporationAddress> Addresses { get; set; }
        string InGoodStanding { get; set; }
        IDate ForeignIncorporationDate { get; set; }
        string Purpose { get; set; }
        string AdditionalInfo { get; set; }
        INameAndCompany RegisteredAgentName { get; set; }
        IAddress RegisteredAgentAddress { get; set; }
    }
}
