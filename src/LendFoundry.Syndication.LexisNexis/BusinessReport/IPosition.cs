namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IPosition
	{
		 string CompanyTitle { get; set; }
		 IDate FromDate { get; set; }
		 IDate ToDate { get; set; }
	}
}
