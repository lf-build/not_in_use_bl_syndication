using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class OpSiteAddress : IOpSiteAddress
    {
        public OpSiteAddress()
        { }
        public OpSiteAddress(ServiceReference.TopBusinessOpSiteAddress opSiteAddress)
        {
            if (opSiteAddress == null)
                return;
            Address = new Address(opSiteAddress.Address);
            MsaName = opSiteAddress.MsaName;
            PropertyLink = opSiteAddress.PropertyLink;
            if (opSiteAddress.Phones != null)
            {
                Phones = new List<IOpSitePhone>(opSiteAddress.Phones.Select(phone => new OpSitePhone(phone)));
            }
            if (opSiteAddress.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(opSiteAddress.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public IAddress Address { get; set; }
        public string MsaName { get; set; }
        public string PropertyLink { get; set; }
        public List<IOpSitePhone> Phones { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
