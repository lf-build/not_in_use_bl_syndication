namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIncorporationAddress
	{
		 IAddress Address { get; set; }
		 string AddressType { get; set; }
	}
}
