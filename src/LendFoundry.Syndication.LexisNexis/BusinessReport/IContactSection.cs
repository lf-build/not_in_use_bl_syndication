using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IContactSection
    {
        short OtherAssociatedPersonCount { get; set; }
        List<IIndividual> OtherAssociatedPersons { get; set; }
        short CurrentContactCount { get; set; }
        short TotalCurrentContactCount { get; set; }
        List<IIndividual> CurrentIndividuals { get; set; }
        short CurrentExecutiveCount { get; set; }
        short TotalCurrentExecutiveCount { get; set; }
        List<IIndividual> CurrentExecutives { get; set; }
        List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        short PriorContactCount { get; set; }
        short TotalPriorContactCount { get; set; }
        List<IIndividual> PriorIndividuals { get; set; }
        short PriorExecutiveCount { get; set; }
        short TotalPriorExecutiveCount { get; set; }
        List<IIndividual> PriorExecutives { get; set; }
    }
}
