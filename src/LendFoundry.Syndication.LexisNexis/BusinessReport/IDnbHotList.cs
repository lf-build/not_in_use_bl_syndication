namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IDnbHotList
	{
		 string NewIndicator { get; set; }
		 string OwnershipChangeIndicator { get; set; }
		 string CeoChangeIndicator { get; set; }
		 string CompanyNameChangeInd { get; set; }
		 string AddressChangeIndicator { get; set; }
		 string TelephoneChangeIndicator { get; set; }
		 IDate NewChangeDate { get; set; }
		 IDate OwnershipChangeDate { get; set; }
		 IDate CeoChangeDate { get; set; }
		 IDate CompanyNameChgDate { get; set; }
		 IDate AddressChangeDate { get; set; }
		 IDate TelephoneChangeDate { get; set; }
	}
}
