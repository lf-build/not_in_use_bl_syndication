namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PropertyTransaction : IPropertyTransaction
    {
        public PropertyTransaction()
        { }
        public PropertyTransaction(ServiceReference.TopBusinessPropertyTransaction propertyTransaction)
        {
            if (propertyTransaction == null)
                return;
            PartyType = propertyTransaction.PartyType;
            CompanyName = propertyTransaction.CompanyName;
            Name = new Name(propertyTransaction.Name);
            Address = new Address(propertyTransaction.Address);
            PartyTypeName = propertyTransaction.PartyTypeName;
            PropertyAddress = new GeoAddressMatch(propertyTransaction.PropertyAddress);
            Cart = propertyTransaction.Cart;
            CrSortSz = propertyTransaction.CrSortSz;
            Lot = propertyTransaction.Lot;
            LotOrder = propertyTransaction.LotOrder;
            Dbpc = propertyTransaction.DBPC;
            CheckDigit = propertyTransaction.CheckDigit;
            RecordType = propertyTransaction.RecordType;
            MsaNumber = propertyTransaction.MSANumber;
            GeoBlk = propertyTransaction.GeoBlk;
            GeoMatch = propertyTransaction.GeoMatch;
            Phone10 = propertyTransaction.Phone10;
            UniqueId = propertyTransaction.UniqueID;
            BusinessIdentity = new BusinessIdentity(propertyTransaction.BusinessIdentity);
            Ssn = propertyTransaction.SSN;
            ProxyNameSeq = propertyTransaction.ProxyNameSeq;
            OriginalName = propertyTransaction.OriginalName;
            IdCode = propertyTransaction.IdCode;
            IdDescription = propertyTransaction.IdDescription;
        }
        public string PartyType { get; set; }
        public string CompanyName { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public string PartyTypeName { get; set; }
        public IGeoAddressMatch PropertyAddress { get; set; }
        public string Cart { get; set; }
        public string CrSortSz { get; set; }
        public string Lot { get; set; }
        public string LotOrder { get; set; }
        public string Dbpc { get; set; }
        public string CheckDigit { get; set; }
        public string RecordType { get; set; }
        public string MsaNumber { get; set; }
        public string GeoBlk { get; set; }
        public string GeoMatch { get; set; }
        public string Phone10 { get; set; }
        public string UniqueId { get; set; }
        public IBusinessIdentity BusinessIdentity { get; set; }
        public string Ssn { get; set; }
        public uint ProxyNameSeq { get; set; }
        public string OriginalName { get; set; }
        public string IdCode { get; set; }
        public string IdDescription { get; set; }
    }
}
