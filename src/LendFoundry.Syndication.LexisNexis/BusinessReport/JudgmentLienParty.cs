namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class JudgmentLienParty : IJudgmentLienParty
    {
        public JudgmentLienParty()
        { }
        public JudgmentLienParty(ServiceReference.TopBusinessJudgmentLienParty judgmentLienParty)
        {
            if (judgmentLienParty == null)
                return;
            CompanyName = judgmentLienParty.CompanyName;
            TaxId = judgmentLienParty.TaxId;
            Ssn = judgmentLienParty.SSN;
            Name = new Name(judgmentLienParty.Name);
            Address = new Address(judgmentLienParty.Address);
        }
        public string CompanyName { get; set; }
        public string TaxId { get; set; }
        public string Ssn { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
    }
}
