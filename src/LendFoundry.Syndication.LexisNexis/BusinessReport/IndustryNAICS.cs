namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IndustryNaics : IIndustryNaics
    {
        public IndustryNaics()
        { }
        public IndustryNaics(ServiceReference.TopBusinessIndustryNAICS industryNaics)
        {
            if (industryNaics == null)
                return;
            Naics = industryNaics.NAICS;
            NaicsDescription = industryNaics.NAICSDescription;
        }
        public string Naics { get; set; }
        public string NaicsDescription { get; set; }
    }
}
