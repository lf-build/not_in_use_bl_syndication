namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class AssociateBusiness : IAssociateBusiness
	{
	    public AssociateBusiness()
	    {
	        
	    }

        public AssociateBusiness(ServiceReference.TopBusinessAssociateBusiness associateBusiness)
        {
            if (associateBusiness == null)
                return;
            CompanyName = associateBusiness.CompanyName;
            Address = new Address(associateBusiness.Address);
            Role = associateBusiness.Role;
            BusinessIds = new BusinessIdentity(associateBusiness.BusinessIds);
        }

        public string CompanyName { get; set; }
		public IAddress Address { get; set; }
		public string Role { get; set; }
		public IBusinessIdentity BusinessIds { get; set; }
	}
}
