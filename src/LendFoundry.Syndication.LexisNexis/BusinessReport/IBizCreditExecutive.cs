namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditExecutive
	{
		 IName Name { get; set; }
		 string Title { get; set; }
	}
}
