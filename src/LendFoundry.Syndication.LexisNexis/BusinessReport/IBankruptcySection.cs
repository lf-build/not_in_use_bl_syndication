using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBankruptcySection
    {
        int DerogSummaryCntBankruptcy { get; set; }
        int TotalDerogSummaryCntBankruptcy { get; set; }
        List<IBankruptcy> AsDebtors { get; set; }
        List<ISourceDocInfo> DebtorSourceDocs { get; set; }
    }
}
