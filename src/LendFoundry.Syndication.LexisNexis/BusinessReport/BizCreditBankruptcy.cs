namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public partial class BizCreditBankruptcy : IBizCreditBankruptcy
	{
        public BizCreditBankruptcy()
        { }
        public BizCreditBankruptcy(ServiceReference.BizCreditBankruptcy bizCreditBanruptcy)
        {
            if (bizCreditBanruptcy == null)
                return;
            Type = bizCreditBanruptcy.Type;
            Action = bizCreditBanruptcy.Action;
            DocumentNumber = bizCreditBanruptcy.DocumentNumber;
            LiabilityAmount = bizCreditBanruptcy.LiabilityAmount;
            AssetAmount = bizCreditBanruptcy.AssetAmount;
            DisputeIndicator = bizCreditBanruptcy.DisputeIndicator;
            DisputeCode = bizCreditBanruptcy.DisputeCode;
            DateFiled = new Date(bizCreditBanruptcy.DateFiled);
        }
		public string Type { get; set; }
		public string Action { get; set; }
		public string DocumentNumber { get; set; }
		public int LiabilityAmount { get; set; }
		public int AssetAmount { get; set; }
		public string DisputeIndicator { get; set; }
		public string DisputeCode { get; set; }
		public IDate DateFiled { get; set; }
	}
}
