using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UccRole : IUccRole
    {
        public UccRole()
        { }
        public UccRole(ServiceReference.TopBusinessUCCRole uccRole)
        {
            if (uccRole == null)
                return;
            if (uccRole.ActiveUCCs != null)
            {
                ActiveUcCs = new List<IUcc>(uccRole.ActiveUCCs.Select(activeUcc => new Ucc(activeUcc)));
            }
            if (uccRole.ActiveSourceDocs != null)
            {
                ActiveSourceDocs = new List<ISourceDocInfo>(uccRole.ActiveSourceDocs.Select(activeSourceDoc => new SourceDocInfo(activeSourceDoc)));
            }
            if (uccRole.TerminatedUCCs != null)
            {
                TerminatedUcCs = uccRole.TerminatedUCCs.Select(terminatedUcc => new Ucc(terminatedUcc)).Cast<IUcc>().ToList();
            }
            if (uccRole.TerminatedSourceDocs != null)
            {
                TerminatedSourceDocs = new List<ISourceDocInfo>(uccRole.TerminatedSourceDocs.Select(terminatedSourceDoc => new SourceDocInfo(terminatedSourceDoc)));
            }
        }
        public List<IUcc> ActiveUcCs { get; set; }
        public List<ISourceDocInfo> ActiveSourceDocs { get; set; }
        public List<IUcc> TerminatedUcCs { get; set; }
        public List<ISourceDocInfo> TerminatedSourceDocs { get; set; }
    }
}
