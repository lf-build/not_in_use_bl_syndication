namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BusinessRegistrationRecord : IBusinessRegistrationRecord
    {
        public BusinessRegistrationRecord()
        { }
        public BusinessRegistrationRecord(ServiceReference.TopBusinessBusinessRegistrationRecord businessRegistrationRecord)
        {
            if (businessRegistrationRecord == null)
                return;
            RecordDate = new Date(businessRegistrationRecord.RecordDate);
            CompanyName = businessRegistrationRecord.CompanyName;
            Description = businessRegistrationRecord.Description;
            Status = businessRegistrationRecord.Status;
            FilingNumber = businessRegistrationRecord.FilingNumber;
            FileDate = new Date(businessRegistrationRecord.FileDate);
            ExpirationDate = new Date(businessRegistrationRecord.ExpirationDate);
            ProccessDate = new Date(businessRegistrationRecord.ProccessDate);
            Address = new Address(businessRegistrationRecord.Address);
            CompanyPhone10 = businessRegistrationRecord.CompanyPhone10;
            CorpCodeDecode = businessRegistrationRecord.CorpCodeDecode;
            SosCodeDecode = businessRegistrationRecord.SOSCodeDecode;
            FilingCodDecode = businessRegistrationRecord.FilingCodDecode;
            StatusDecode = businessRegistrationRecord.StatusDecode;
            FileDateDecode = businessRegistrationRecord.FileDateDecode;
            ProcDateDecode = businessRegistrationRecord.ProcDateDecode;
        }
        public IDate RecordDate { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string FilingNumber { get; set; }
        public IDate FileDate { get; set; }
        public IDate ExpirationDate { get; set; }
        public IDate ProccessDate { get; set; }
        public IAddress Address { get; set; }
        public string CompanyPhone10 { get; set; }
        public string CorpCodeDecode { get; set; }
        public string SosCodeDecode { get; set; }
        public string FilingCodDecode { get; set; }
        public string StatusDecode { get; set; }
        public string FileDateDecode { get; set; }
        public string ProcDateDecode { get; set; }
    }
}
