using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class Aircraft : IAircraft
	{
        public Aircraft()
        { }
        public Aircraft(ServiceReference.TopBusinessAircraft aircraft)
        {
            if (aircraft == null)
                return;
            AircraftNumber = aircraft.AircraftNumber;
            ModelYear = aircraft.ModelYear;
            Manufacturer = aircraft.Manufacturer;
            Model = aircraft.Model;
            AircraftType = aircraft.AircraftType;
            SerialNumber = aircraft.SerialNumber;
            Engines = aircraft.Engines;
            RegistrationDate = new Date(aircraft.RegistrationDate);
            RegistrationNumber = aircraft.RegistrationNumber;
            RegistrationAddress = new Address(aircraft.RegistrationAddress);

            if(aircraft.CurrentParties != null)
            {
                CurrentParties = new List<IAircraftParty>(
                        aircraft.CurrentParties.Select(currentParty => new AircraftParty(currentParty)));
            }

            if(aircraft.PriorParties != null)
            {
                PriorParties = new List<IAircraftParty>(aircraft.PriorParties.Select(priorParty => new AircraftParty(priorParty)));
            }

            if(aircraft.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(aircraft.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string AircraftNumber { get; set; }
		public string ModelYear { get; set; }
		public string Manufacturer { get; set; }
		public string Model { get; set; }
		public string AircraftType { get; set; }
		public string SerialNumber { get; set; }
		public int Engines { get; set; }
		public IDate RegistrationDate { get; set; }
		public string RegistrationNumber { get; set; }
		public IAddress RegistrationAddress { get; set; }
		public List<IAircraftParty> CurrentParties { get; set; }
		public List<IAircraftParty> PriorParties { get; set; }
		public List<ISourceDocInfo> SourceDocs { get; set; }
	}
}
