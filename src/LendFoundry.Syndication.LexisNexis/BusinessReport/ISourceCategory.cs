using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ISourceCategory
    {
        string Name { get; set; }
        uint DocCount { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
