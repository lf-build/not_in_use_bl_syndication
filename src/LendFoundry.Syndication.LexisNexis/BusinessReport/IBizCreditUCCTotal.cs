namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditUccTotal
	{
		 int Filed { get; set; }
		 int DetailsFiled { get; set; }
		 int SummariesFiled { get; set; }
		 int UnsatisfiedLast24Months { get; set; }
		 string DataIndicator { get; set; }
	}
}
