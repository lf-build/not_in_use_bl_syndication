namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IDunBradStreetSection
	{
		 uint DnbRecordCount { get; set; }
		 IDnbRecord Dnb { get; set; }
	}
}
