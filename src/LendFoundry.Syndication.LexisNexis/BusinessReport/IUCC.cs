using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IUcc
    {
        string FilingJurisdiction { get; set; }
        string OriginalFilingNumber { get; set; }
        IDate OriginalFilingDate { get; set; }
        string LatestFilingType { get; set; }
        string FilingAgencyName { get; set; }
        IAddress FilingAgencyAddress { get; set; }
        List<IUccFiling> Filings { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
