namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditPayment
	{
		 int TradeCount { get; set; }
		 int Debt { get; set; }
		 string HighCreditMask { get; set; }
		 int RecentHighCredit { get; set; }
		 string AccountBalanceMask { get; set; }
		 int MaskedAccountBalance { get; set; }
		 IDebtBeyondTermsPercent DbtPercentages { get; set; }
	}
}
