using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditCollateralAccount
    {
        int TotalUccFiled { get; set; }
        int Count { get; set; }
        List<ICodeMap> Collaterals { get; set; }
        string AdditionalCode { get; set; }
    }
}
