﻿namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ReportRequest : IReportRequest
    {
        public long DotId { get; set; }
        public long EmpId { get; set; }
        public long PowId { get; set; }
        public long ProxId { get; set; }
        public long SeleId { get; set; }
        public long OrgId { get; set; }
        public long UltId { get; set; }
    }
}
