namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IGeoLocationMatch
	{
		 string Latitude { get; set; }
		 string Longitude { get; set; }
		 string MatchCode { get; set; }
		 string MatchDesc { get; set; }
	}
}
