namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTradePaymentTotal
	{
		 IBizCreditPayment RegularTrades { get; set; }
		 IBizCreditPayment NewTrades { get; set; }
		 IBizCreditPayment CombinedTrades { get; set; }
		 int HighestCreditMedian { get; set; }
		 int AgedTradesCount { get; set; }
		 int AccountBalanceRegular { get; set; }
		 int AccountBalanceNew { get; set; }
		 int AccountBalanceCombined { get; set; }
	}
}
