using LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ReportBy : IReportBy
    {
        public ReportBy()
        { }
        public ReportBy(TopBusinessReportBy reportBy)
        {
            if (reportBy == null)
                return;
            BusinessIds = new BusinessIdentity(reportBy.BusinessIds);
        }
        public IBusinessIdentity BusinessIds { get; set; }
    }
}
