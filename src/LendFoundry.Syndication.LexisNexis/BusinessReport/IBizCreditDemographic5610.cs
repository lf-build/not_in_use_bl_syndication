namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditDemographic5610
	{
		 int FiscalYearEndMonth { get; set; }
		 string ProfitRange { get; set; }
		 int ProfitRangeActual { get; set; }
		 string NetWorth { get; set; }
		 int NetWorthActual { get; set; }
		 int InBuildingSinceYear { get; set; }
		 string RentOrOwn { get; set; }
		 int BuildingSquareFeet { get; set; }
		 int ActiveCustomerCount { get; set; }
		 string Ownership { get; set; }
		 string CorporateName { get; set; }
		 string CorporateCity { get; set; }
		 string CorporateState { get; set; }
		 string CorporateStateName { get; set; }
		 string CorporatePhone { get; set; }
		 string TimeZone { get; set; }
		 string OfficerTitle { get; set; }
		 IName OrigOfficerName { get; set; }
		 IName OfficerName { get; set; }
		 string UniqueId { get; set; }
		 string Ssn { get; set; }
	}
}
