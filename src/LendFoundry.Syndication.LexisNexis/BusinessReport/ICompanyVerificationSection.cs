namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ICompanyVerificationSection
	{
		 uint CompanyVerificationRecordCount { get; set; }
		 ICompanyVerification CompanyVerification { get; set; }
	}
}
