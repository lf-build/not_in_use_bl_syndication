namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAssociatePerson
	{
		 IName Name { get; set; }
		 bool IsDeceased { get; set; }
		 bool HasDerog { get; set; }
		 IAddress Address { get; set; }
		 IDate ToDate { get; set; }
		 IDate FromDate { get; set; }
		 string Role { get; set; }
		 string UniqueId { get; set; }
		 string Ssn { get; set; }
	}
}
