namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class JudgmentLienFilings : IJudgmentLienFilings
    {
        public JudgmentLienFilings()
        { }
        public JudgmentLienFilings(ServiceReference.TopBusinessJudgmentLienFilings judgmentLienFilings)
        {
            if (judgmentLienFilings == null)
                return;
            FilingNumber = judgmentLienFilings.FilingNumber;
            FilingType = judgmentLienFilings.FilingType;
            FilingStatus = judgmentLienFilings.FilingStatus;
            FilingDate = new Date(judgmentLienFilings.FilingDate);
            Agency = judgmentLienFilings.Agency;
            AgencyCounty = judgmentLienFilings.AgencyCounty;
            AgencyState = judgmentLienFilings.AgencyState;
        }
        public string FilingNumber { get; set; }
        public string FilingType { get; set; }
        public string FilingStatus { get; set; }
        public IDate FilingDate { get; set; }
        public string Agency { get; set; }
        public string AgencyState { get; set; }
        public string AgencyCounty { get; set; }
    }
}
