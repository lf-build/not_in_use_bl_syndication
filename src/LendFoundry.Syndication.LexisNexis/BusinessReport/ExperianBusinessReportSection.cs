using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ExperianBusinessReportSection : IExperianBusinessReportSection
    {
        public ExperianBusinessReportSection()
        { }
        public ExperianBusinessReportSection(ServiceReference.TopBusinessExperianBusinessReportSection experianBusinessReportSection)
        {
            if (experianBusinessReportSection?.ExperianBusinessReports != null)
            {
                ExperianBusinessReports =
                    new List<IBizCreditReportRecord>(experianBusinessReportSection.ExperianBusinessReports.Select(
                        experianBusinessReport => new BizCreditReportRecord(experianBusinessReport)));

            }
        }

        public List<IBizCreditReportRecord> ExperianBusinessReports { get; set; }
    }
}
