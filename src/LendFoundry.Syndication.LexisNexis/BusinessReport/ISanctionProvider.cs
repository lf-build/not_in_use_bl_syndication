namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ISanctionProvider
	{
		 IName Name { get; set; }
		 IGeoAddress Address { get; set; }
	}
}
