namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTaxLien
	{
		 string Type { get; set; }
		 string Action { get; set; }
		 string DocumentNumber { get; set; }
		 string FilingLocation { get; set; }
		 int LiabilityAmount { get; set; }
		 string Description { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
		 IDate DateFiled { get; set; }
	}
}
