using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditInquiry : IBizCreditInquiry
	{
        public BizCreditInquiry()
        { }
        public BizCreditInquiry(ServiceReference.BizCreditInquiry bizCreditInquiry)
        {
            if (bizCreditInquiry == null)
                return;
            DateRange = new DateRange(bizCreditInquiry.DateRange);
            if(bizCreditInquiry.Inquiries != null)
            {
                Inquiries = new List<IBizCreditInquiryItem>(bizCreditInquiry.Inquiries.Select(inquiry => new BizCreditInquiryItem(inquiry)));
            }
        }
        public IDateRange DateRange { get; set; }
		public List<IBizCreditInquiryItem> Inquiries { get; set; }
	}
}
