namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IPropertyTransaction
	{
		 string PartyType { get; set; }
		 string CompanyName { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
		 string PartyTypeName { get; set; }
		 IGeoAddressMatch PropertyAddress { get; set; }
		 string Cart { get; set; }
		 string CrSortSz { get; set; }
		 string Lot { get; set; }
		 string LotOrder { get; set; }
		 string Dbpc { get; set; }
		 string CheckDigit { get; set; }
		 string RecordType { get; set; }
		 string MsaNumber { get; set; }
		 string GeoBlk { get; set; }
		 string GeoMatch { get; set; }
		 string Phone10 { get; set; }
		 string UniqueId { get; set; }
		 IBusinessIdentity BusinessIdentity { get; set; }
		 string Ssn { get; set; }
		 uint ProxyNameSeq { get; set; }
		 string OriginalName { get; set; }
		 string IdCode { get; set; }
		 string IdDescription { get; set; }
	}
}
