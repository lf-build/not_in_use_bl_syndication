using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class MotorVehicleSummary : IMotorVehicleSummary
    {
        public MotorVehicleSummary()
        { }
        public MotorVehicleSummary(ServiceReference.TopBusinessMotorVehicleSummary motorVehicleSummary)
        {
            if (motorVehicleSummary == null)
                return;
            CurrentRecordCount = motorVehicleSummary.CurrentRecordCount;
            TotalCurrentRecordCount = motorVehicleSummary.TotalCurrentRecordCount;
            PriorRecordCount = motorVehicleSummary.PriorRecordCount;
            TotalPriorRecordCount = motorVehicleSummary.TotalPriorRecordCount;
            if (motorVehicleSummary.CurrentVehicles != null)
            {
                CurrentVehicles = new List<IMotorVehicleDetail>(motorVehicleSummary.CurrentVehicles.Select(currentVehicle => new MotorVehicleDetail(currentVehicle)));
            }
            if (motorVehicleSummary.CurrentSourceDocs != null)
            {
                CurrentSourceDocs = new List<ISourceDocInfo>(motorVehicleSummary.CurrentSourceDocs.Select(currentSourceDoc => new SourceDocInfo(currentSourceDoc)));
            }
            if (motorVehicleSummary.PriorVehicles != null)
            {
                PriorVehicles = new List<IMotorVehicleDetail>(motorVehicleSummary.PriorVehicles.Select(priorVehicle => new MotorVehicleDetail(priorVehicle)));
            }
            if (motorVehicleSummary.PriorSourceDocs != null)
            {
                PriorSourceDocs = new List<ISourceDocInfo>(motorVehicleSummary.PriorSourceDocs.Select(priorSourceDoc => new SourceDocInfo(priorSourceDoc)));
            }
        }
        public int CurrentRecordCount { get; set; }
        public int TotalCurrentRecordCount { get; set; }
        public int PriorRecordCount { get; set; }
        public int TotalPriorRecordCount { get; set; }
        public List<IMotorVehicleDetail> CurrentVehicles { get; set; }
        public List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        public List<IMotorVehicleDetail> PriorVehicles { get; set; }
        public List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
