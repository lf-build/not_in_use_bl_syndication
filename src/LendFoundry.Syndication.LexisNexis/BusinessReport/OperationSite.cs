using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class OperationSite : IOperationSite
    {
        public OperationSite()
        { }
        public OperationSite(ServiceReference.TopBusinessOperationSite operationSite)
        {
            if (operationSite == null)
                return;
            StateName = operationSite.StateName;
            StateAddressCount = operationSite.StateAddressCount;
            StateTotalAddressCount = operationSite.StateTotalAddressCount;
            if (operationSite.Addresses != null)
            {
                Addresses = operationSite.Addresses.Select(address => new OpSiteAddress(address)).Cast<IOpSiteAddress>().ToList();
            }
            if (operationSite.SourceDocs != null)
            {
                SourceDocs = operationSite.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)).Cast<ISourceDocInfo>().ToList();
            }
        }
        public string StateName { get; set; }
        public int StateAddressCount { get; set; }
        public int StateTotalAddressCount { get; set; }
        public List<IOpSiteAddress> Addresses { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
