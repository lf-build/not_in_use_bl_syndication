namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IDateRange
	{
		 IDate StartDate { get; set; }
		 IDate EndDate { get; set; }
	}
}
