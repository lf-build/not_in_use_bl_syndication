using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IProperty
    {
        string PropertyUniqueId { get; set; }
        string SourceObscure { get; set; }
        IAddress PropertyAddress { get; set; }
        bool IsNoticeOfDefault { get; set; }
        bool IsForeclosed { get; set; }
        string Apn { get; set; }
        string AddressType { get; set; }
        string PurchasePrice { get; set; }
        string SalesPrice { get; set; }
        string DocumentType { get; set; }
        string AssessedValue { get; set; }
        string MarketLandValue { get; set; }
        string TotalMarketValue { get; set; }
        IDate AssessmentDate { get; set; }
        IDate ContractDate { get; set; }
        IDate SaleDate { get; set; }
        IDate RecordingDate { get; set; }
        string CurrentRecord { get; set; }
        List<ISourceDocInfo> PSourceDocs { get; set; }
        List<ISourceDocInfo> FSourceDocs { get; set; }
        List<IPropertyParty> Parties { get; set; }
        string FIdType { get; set; }
        string FIdTypeDesc { get; set; }
        string SortByDate { get; set; }
        string VendorSourceGlag { get; set; }
        string VendorSourceDesc { get; set; }
        string AccurintCurrentRecord { get; set; }
        IDeeds Deeds { get; set; }
        IAccessor Accessor { get; set; }
        string FaresLivingSquareFeet { get; set; }
        string PartyType { get; set; }
        string PartyTypeName { get; set; }
        IGeoAddressMatch Address { get; set; }
        string Msa { get; set; }
        string GeoBlk { get; set; }
        string GeoMatch { get; set; }
        string Phone10 { get; set; }
    }
}
