namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IPropertySection
	{
		 int PropertyRecordCount { get; set; }
		 int TotalPropertyRecordCount { get; set; }
		 IPropertySummary PropertyRecords { get; set; }
	}
}
