using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BankruptcySection : IBankruptcySection
    {
        public BankruptcySection()
        { }
        public BankruptcySection(ServiceReference.TopBusinessBankruptcySection bankruptcySection)
        {
            if (bankruptcySection == null)
                return;
            DerogSummaryCntBankruptcy = bankruptcySection.DerogSummaryCntBankruptcy;
            TotalDerogSummaryCntBankruptcy = bankruptcySection.TotalDerogSummaryCntBankruptcy;
            if (bankruptcySection.AsDebtors != null)
            {
                AsDebtors = new List<IBankruptcy>(bankruptcySection.AsDebtors.Select(asDebtor => new Bankruptcy(asDebtor)));
            }
            if (bankruptcySection.DebtorSourceDocs != null)
            {
                DebtorSourceDocs = new List<ISourceDocInfo>(bankruptcySection.DebtorSourceDocs.Select(debtorSourceDoc => new SourceDocInfo(debtorSourceDoc)));
            }
        }
        public int DerogSummaryCntBankruptcy { get; set; }
        public int TotalDerogSummaryCntBankruptcy { get; set; }
        public List<IBankruptcy> AsDebtors { get; set; }
        public List<ISourceDocInfo> DebtorSourceDocs { get; set; }
    }
}
