using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditReportRecord : IBizCreditReportRecord
    {
        public BizCreditReportRecord()
        { }
        public BizCreditReportRecord(ServiceReference.BizCreditReportRecord bizCreditReportRecord)
        {
            if (bizCreditReportRecord == null)
                return;
            BusinessIds = new BusinessIdentity(bizCreditReportRecord.BusinessIds);
            IdValue = bizCreditReportRecord.IdValue;
            FileNumber = bizCreditReportRecord.FileNumber;
            BusinessHeader = new BizCreditBusinessHeader(bizCreditReportRecord.BusinessHeader);
            if (bizCreditReportRecord.ExecutiveSummaries != null)
            {
                ExecutiveSummaries = bizCreditReportRecord.ExecutiveSummaries.Select(executiveSummary => new BizCreditExecutiveSummary(executiveSummary)).Cast<IBizCreditExecutiveSummary>().ToList();
            }

            if (bizCreditReportRecord.TradePaymentDetails != null)
            {
                TradePaymentDetails = bizCreditReportRecord.TradePaymentDetails.Select(tradePaymentDetail => new BizCreditTradePaymentDetail(tradePaymentDetail)).Cast<IBizCreditTradePaymentDetail>().ToList();
            }

            if (bizCreditReportRecord.TradePaymentTotals != null)
            {
                TradePaymentTotals = bizCreditReportRecord.TradePaymentTotals.Select(tradePaymentTotal => new BizCreditTradePaymentTotal(tradePaymentTotal)).Cast<IBizCreditTradePaymentTotal>().ToList();
            }

            if (bizCreditReportRecord.TradePaymentTrends != null)
            {
                TradePaymentTrends = bizCreditReportRecord.TradePaymentTrends.Select(tradePaymentTrend => new BizCreditTradePaymentTrend(tradePaymentTrend)).Cast<IBizCreditTradePaymentTrend>().ToList();
            }

            if (bizCreditReportRecord.TradeQuarterlyAverages != null)
            {
                TradeQuarterlyAverages = bizCreditReportRecord.TradeQuarterlyAverages.Select(tradeQuarterlyAverage => new BizCreditTradeQuarterlyAverage(tradeQuarterlyAverage)).Cast<IBizCreditTradeQuarterlyAverage>().ToList();
            }

            if (bizCreditReportRecord.Bankruptcies != null)
            {
                Bankruptcies = new List<IBizCreditBankruptcy>(bizCreditReportRecord.Bankruptcies.Select(bankruptcy => new BizCreditBankruptcy(bankruptcy)));
            }

            if (bizCreditReportRecord.TaxLiens != null)
            {
                TaxLiens = new List<IBizCreditTaxLien>(bizCreditReportRecord.TaxLiens.Select(taxLien => new BizCreditTaxLien(taxLien)));
            }

            if (bizCreditReportRecord.Judgments != null)
            {
                Judgments = new List<IBizCreditJudgment>(bizCreditReportRecord.Judgments.Select(judgment => new BizCreditJudgment(judgment)));
            }

            if (bizCreditReportRecord.CollateralAccounts != null)
            {
                CollateralAccounts = new List<IBizCreditCollateralAccount>(bizCreditReportRecord.CollateralAccounts.Select(collateralAccount => new BizCreditCollateralAccount(collateralAccount)));
            }

            if (bizCreditReportRecord.UCCFilings != null)
            {
                UccFilings = new List<IBizCreditUccFiling>(bizCreditReportRecord.UCCFilings.Select(uCcFiling => new BizCreditUccFiling(uCcFiling)));
            }

            if (bizCreditReportRecord.BankingDetails != null)
            {
                BankingDetails = new List<IBizCreditBankingDetail>(bizCreditReportRecord.BankingDetails.Select(bankingDetail => new BizCreditBankingDetail(bankingDetail)));
            }

            if (bizCreditReportRecord.Demographics5600 != null)
            {
                Demographics5600 = new List<IBizCreditDemographic5600>(bizCreditReportRecord.Demographics5600.Select(demographic5600 => new BizCreditDemographic5600(demographic5600)));
            }

            if (bizCreditReportRecord.Demographics5610 != null)
            {
                Demographics5610 = new List<IBizCreditDemographic5610>(bizCreditReportRecord.Demographics5610.Select(demographic5610 => new BizCreditDemographic5610(demographic5610)));
            }

            if (bizCreditReportRecord.InquiryRecords != null)
            {
                InquiryRecords = new List<IBizCreditInquiry>(bizCreditReportRecord.InquiryRecords.Select(inquiryRecord => new BizCreditInquiry(inquiryRecord)));
            }

            if (bizCreditReportRecord.GovernmentTrades != null)
            {
                GovernmentTrades = new List<IBizCreditGovernmentTrade>(bizCreditReportRecord.GovernmentTrades.Select(governmentTrade => new BizCreditGovernmentTrade(governmentTrade)));
            }

            if (bizCreditReportRecord.GovernmentDebarredContractors != null)
            {
                GovernmentDebarredContractors = new List<IBizCreditGovernmentDebarredContractor>(bizCreditReportRecord.GovernmentDebarredContractors.Select(governmentDebarredContractor => new BizCreditGovernmentDebarredContractor(governmentDebarredContractor)));
            }

            if (bizCreditReportRecord.SNPs != null)
            {
                SnPs = new List<IBizCredSnp>(bizCreditReportRecord.SNPs.Select(snp => new BizCredSNP(snp)));
            }

            Derogatory = new BizCreditDerogatory(bizCreditReportRecord.Derogatory);
            BankruptcyTotals = new BizCreditBankruptcyTotal(bizCreditReportRecord.BankruptcyTotals);
            JudgmentTotals = new BizCreditJudgmentTotal(bizCreditReportRecord.JudgmentTotals);
            LienTotals = new BizCreditLienTotal(bizCreditReportRecord.LienTotals);
            UccTotals = new BizCreditUccTotal(bizCreditReportRecord.UCCTotals);
            TradeInfo = new BizCreditTradeInfo(bizCreditReportRecord.TradeInfo);
        }
        public IBusinessIdentity BusinessIds { get; set; }
        public string IdValue { get; set; }
        public string FileNumber { get; set; }
        public IBizCreditBusinessHeader BusinessHeader { get; set; }
        public List<IBizCreditExecutiveSummary> ExecutiveSummaries { get; set; }
        public List<IBizCreditTradePaymentDetail> TradePaymentDetails { get; set; }
        public List<IBizCreditTradePaymentTotal> TradePaymentTotals { get; set; }
        public List<IBizCreditTradePaymentTrend> TradePaymentTrends { get; set; }
        public List<IBizCreditTradeQuarterlyAverage> TradeQuarterlyAverages { get; set; }
        public List<IBizCreditBankruptcy> Bankruptcies { get; set; }
        public List<IBizCreditTaxLien> TaxLiens { get; set; }
        public List<IBizCreditJudgment> Judgments { get; set; }
        public List<IBizCreditCollateralAccount> CollateralAccounts { get; set; }
        public List<IBizCreditUccFiling> UccFilings { get; set; }
        public List<IBizCreditBankingDetail> BankingDetails { get; set; }
        public List<IBizCreditDemographic5600> Demographics5600 { get; set; }
        public List<IBizCreditDemographic5610> Demographics5610 { get; set; }
        public List<IBizCreditInquiry> InquiryRecords { get; set; }
        public List<IBizCreditGovernmentTrade> GovernmentTrades { get; set; }
        public List<IBizCreditGovernmentDebarredContractor> GovernmentDebarredContractors { get; set; }
        public List<IBizCredSnp> SnPs { get; set; }
        public IBizCreditDerogatory Derogatory { get; set; }
        public IBizCreditBankruptcyTotal BankruptcyTotals { get; set; }
        public IBizCreditJudgmentTotal JudgmentTotals { get; set; }
        public IBizCreditLienTotal LienTotals { get; set; }
        public IBizCreditUccTotal UccTotals { get; set; }
        public IBizCreditTradeInfo TradeInfo { get; set; }
    }
}
