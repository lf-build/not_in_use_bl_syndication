namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTradePaymentTrend
	{
		 IDate Date { get; set; }
		 int Debt { get; set; }
		 string AccountBalanceMask { get; set; }
		 int AccountBalance { get; set; }
		 IDebtBeyondTermsPercent DbtPercentages { get; set; }
	}
}
