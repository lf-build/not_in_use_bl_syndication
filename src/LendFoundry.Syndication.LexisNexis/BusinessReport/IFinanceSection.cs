using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IFinanceSection
    {
        List<IFinance> Finances { get; set; }
    }
}
