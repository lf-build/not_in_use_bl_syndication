namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditLienTotal
	{
		 int Filed { get; set; }
		 int FiledLast24Months { get; set; }
		 string LiabilityAmount { get; set; }
		 int MonthsSinceLastFiling { get; set; }
		 string TaxAmountLast24Months { get; set; }
		 string PercentTaxAmountToTradeBalance { get; set; }
		 bool PaymentReceived { get; set; }
	}
}
