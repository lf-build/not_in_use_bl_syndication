namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BusinessIdentity : IBusinessIdentity
    {
        public BusinessIdentity()
        { }
        public BusinessIdentity(ServiceReference.BusinessIdentity businessIdentity)
        {
            if (businessIdentity == null)
                return;
            DotId = businessIdentity.DotID;
            EmpId = businessIdentity.EmpID;
            PowId = businessIdentity.POWID;
            ProxId = businessIdentity.ProxID;
            SeleId = businessIdentity.SeleID;
            OrgId = businessIdentity.OrgID;
            UltId = businessIdentity.UltID;
            DotIdSpecified = businessIdentity.DotIDSpecified;
            EmpIdSpecified = businessIdentity.EmpIDSpecified;
            PowIdSpecified = businessIdentity.POWIDSpecified;
            ProxIdSpecified = businessIdentity.ProxIDSpecified;
            SeleIdSpecified = businessIdentity.SeleIDSpecified;
            OrgIdSpecified = businessIdentity.OrgIDSpecified;
            UltIdSpecified = businessIdentity.UltIDSpecified;
        }
        public long DotId { get; set; }
        public long EmpId { get; set; }
        public long PowId { get; set; }
        public long ProxId { get; set; }
        public long SeleId { get; set; }
        public long OrgId { get; set; }
        public long UltId { get; set; }
        public bool DotIdSpecified { get; set; }
        public bool EmpIdSpecified { get; set; }
        public bool PowIdSpecified { get; set; }
        public bool ProxIdSpecified { get; set; }
        public bool SeleIdSpecified { get; set; }
        public bool OrgIdSpecified { get; set; }
        public bool UltIdSpecified { get; set; }
    }
}
