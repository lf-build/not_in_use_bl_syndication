namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IReportRecord
	{
		 IBestSection Best { get; set; }
		 IParentSection Parent { get; set; }
		 IFinanceSection Finance { get; set; }
		 IIndustrySection Industry { get; set; }
		 IIncorporationSection Incorporation { get; set; }
		 IOperationsSitesSection OperationsSites { get; set; }
		 IContactSection Contact { get; set; }
		 ILicenseSection License { get; set; }
		 IUrlSection Url{ get; set; }
		 IBankruptcySection Bankruptcy { get; set; }
		 ILienSection Lien { get; set; }
		 IUccSection Ucc { get; set; }
		 IPropertySection Property { get; set; }
		 IMotorVehicleSection MotorVehicle { get; set; }
		 IWatercraftSection Watercraft { get; set; }
		 IAircraftSection Aircraft { get; set; }
		 IAssociateSection Associate { get; set; }
		 IRegisteredAgentSection RegisteredAgent { get; set; }
		 IIrs5500Section Irs5500 { get; set; }
		 IDunBradStreetSection DunBradStreet { get; set; }
		 ISanctionSection Sanction { get; set; }
		 IConnectedBusinessSection ConnectedBusiness { get; set; }
		 IExperianBusinessReportSection ExperianBusinessReport { get; set; }
		 ICompanyVerificationSection CompanyVerification { get; set; }
		 IBusinessRegistrationSection BusinessRegistration { get; set; }
		 ISourceSection Source { get; set; }
	}
}
