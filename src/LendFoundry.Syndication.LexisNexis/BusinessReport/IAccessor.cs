namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAccessor
	{
		 string StateCode { get; set; }
		 string FipsCode { get; set; }
		 string DuplicateApnMultipleAddressId { get; set; }
		 string AssesseeOwnershipRightsCode { get; set; }
		 string AssesseeOwnershipRightsDesc { get; set; }
		 string AssesseeRelationshipCode { get; set; }
		 string AssesseeRelationshipDesc { get; set; }
		 string OwnerOccupied { get; set; }
		 string PriorRecordingDate { get; set; }
		 string CountyLandUseDescription { get; set; }
		 string StandardizedLandUseCode { get; set; }
		 string StandardizedLandUseDesc { get; set; }
		 string LegalLotNumber { get; set; }
		 string LegalSubdivisionName { get; set; }
		 string RecordTypeCode { get; set; }
		 string RecordTypeDesc { get; set; }
		 string MortgageLoanAmount { get; set; }
		 string MortgageLoanTypeCode { get; set; }
		 string MortgageLoanTypeDesc { get; set; }
		 string MortgageLenderName { get; set; }
		 string AssessedTotalValue { get; set; }
		 string AssessedValueYear { get; set; }
		 string HomesteadHomeownerExemption { get; set; }
		 string MarketImprovementValue { get; set; }
		 string MarketTotalValue { get; set; }
		 string MarketValueYear { get; set; }
		 string TaxAmount { get; set; }
		 string TaxYear { get; set; }
		 string LandSquareFootage { get; set; }
		 string YearBuilt { get; set; }
		 string NumberOfStories { get; set; }
		 string NumberOfStoriesDesc { get; set; }
		 string NumberOfBedrooms { get; set; }
		 string NumberOfBaths { get; set; }
		 string NumberOfPartialBaths { get; set; }
		 string GarageTypeCode { get; set; }
		 string PoolCode { get; set; }
		 string PoolDesc { get; set; }
		 string ExteriorWallsCode { get; set; }
		 string ExteriorWallsDesc { get; set; }
		 string RoofTypeCode { get; set; }
		 string RoofTypeDesc { get; set; }
		 string HeatingCode { get; set; }
		 string HeatingDesc { get; set; }
		 string HeatingFuelTypeCode { get; set; }
		 string HeatingFuelTypeDesc { get; set; }
		 string AirConditioningCode { get; set; }
		 string AirConditioningDesc { get; set; }
		 string AirConditioningTypeCode { get; set; }
		 string AirConditioningTypeDesc { get; set; }
	}
}
