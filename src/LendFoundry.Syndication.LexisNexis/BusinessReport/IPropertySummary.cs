using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IPropertySummary
    {
        int CurrentRecordsCount { get; set; }
        int TotalCurrentRecordsCount { get; set; }
        int PriorRecordsCount { get; set; }
        int TotalPriorRecordsCount { get; set; }
        List<IProperty> Properties { get; set; }
        List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        List<ISourceDocInfo> PriorSourceDocs { get; set; }
        int DerogSummaryCntForeclosureNod { get; set; }
        int ForeclosureNodRecordCount { get; set; }
    }
}
