namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class GeoAddress : IGeoAddress
    {
        public GeoAddress()
        { }
        public GeoAddress(ServiceReference.GeoAddress geoAddress)
        {
            if (geoAddress == null)
                return;
            Address = new Address(geoAddress.Address);
            Location = new GeoLocation(geoAddress.Location);
        }
        public IAddress Address { get; set; }
        public IGeoLocation Location { get; set; }
    }
}
