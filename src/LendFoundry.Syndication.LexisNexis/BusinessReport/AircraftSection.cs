namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class AircraftSection : IAircraftSection
	{
	    public AircraftSection()
	    {
	        
	    }

        public AircraftSection(ServiceReference.TopBusinessAircraftSection aircraftSection)
        {
            if (aircraftSection == null)
                return;
            AircraftRecordCount = aircraftSection.AircraftRecordCount;
            TotalAircraftRecordCount = aircraftSection.TotalAircraftRecordCount;
            AircraftRecords = new AircraftSummary(aircraftSection.AircraftRecords);
        }

        public int AircraftRecordCount { get; set; }
		public int TotalAircraftRecordCount { get; set; }
		public IAircraftSummary AircraftRecords { get; set; }
	}
}
