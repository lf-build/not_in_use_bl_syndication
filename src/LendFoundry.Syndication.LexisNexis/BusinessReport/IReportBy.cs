namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IReportBy
	{
		 IBusinessIdentity BusinessIds { get; set; }
	}
}
