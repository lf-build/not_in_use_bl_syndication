namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ILicenseSection
	{
		 uint LicenseRecordCount { get; set; }
		 uint LicenseTotalCount { get; set; }
		 ILicenseSummary LicenseRecords { get; set; }
	}
}
