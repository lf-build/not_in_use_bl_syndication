namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class MotorVehicleParty : IMotorVehicleParty
    {
        public MotorVehicleParty()
        { }
        public MotorVehicleParty(ServiceReference.TopBusinessMotorVehicleParty motorVehicleParty)
        {
            if (motorVehicleParty == null)
                return;
            PartyTypeDescription = motorVehicleParty.PartyTypeDescription;
            CompanyName = motorVehicleParty.CompanyName;
            Name = new Name(motorVehicleParty.Name);
            Address = new Address(motorVehicleParty.Address);
            TitleNumber = motorVehicleParty.TitleNumber;
            TitleDate = new Date(motorVehicleParty.TitleDate);
            OriginalRegistrationDate = new Date(motorVehicleParty.OriginalRegistrationDate);
            RegistrationDate = new Date(motorVehicleParty.RegistrationDate);
            RegistrationExpirationDate = new Date(motorVehicleParty.RegistrationExpirationDate);
            LicensePlate = motorVehicleParty.LicensePlate;
            LicensePlateState = motorVehicleParty.LicensePlateState;
            LicensePlateType = motorVehicleParty.LicensePlateType;
            PreviousLicensePlate = motorVehicleParty.PreviousLicensePlate;
            TitleStatus = motorVehicleParty.TitleStatus;
            OdometerMileage = motorVehicleParty.OdometerMileage;
            DecalYear = motorVehicleParty.DecalYear;
            SourceDateFirstSeen = new Date(motorVehicleParty.SourceDateFirstSeen);
            SourceDateLastSeen = new Date(motorVehicleParty.SourceDateLastSeen);
        }
        public string PartyTypeDescription { get; set; }
        public string CompanyName { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public string TitleNumber { get; set; }
        public IDate TitleDate { get; set; }
        public IDate OriginalRegistrationDate { get; set; }
        public IDate RegistrationDate { get; set; }
        public IDate RegistrationExpirationDate { get; set; }
        public string LicensePlate { get; set; }
        public string LicensePlateState { get; set; }
        public string LicensePlateType { get; set; }
        public string PreviousLicensePlate { get; set; }
        public string TitleStatus { get; set; }
        public string OdometerMileage { get; set; }
        public string DecalYear { get; set; }
        public IDate SourceDateFirstSeen { get; set; }
        public IDate SourceDateLastSeen { get; set; }
    }
}
