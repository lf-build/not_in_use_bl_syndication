using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ISourceSection
    {
        uint AllSourcesCount { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
        List<ISourceCategory> Categories { get; set; }
    }
}
