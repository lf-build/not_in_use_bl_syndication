namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Accessor : IAccessor
    {
        public Accessor()
        { }
        public Accessor(ServiceReference.TopBusinessAccessor accessor)
        {
            if (accessor == null)
                return;
            StateCode = accessor.StateCode;
            FipsCode = accessor.FipsCode;
            DuplicateApnMultipleAddressId = accessor.DuplicateAPNMultipleAddressId;
            AssesseeOwnershipRightsCode = accessor.AssesseeOwnershipRightsCode;
            AssesseeOwnershipRightsDesc = accessor.AssesseeOwnershipRightsDesc;
            AssesseeRelationshipCode = accessor.AssesseeRelationshipCode;
            AssesseeRelationshipDesc = accessor.AssesseeRelationshipDesc;
            OwnerOccupied = accessor.OwnerOccupied;
            PriorRecordingDate = accessor.PriorRecordingDate;
            CountyLandUseDescription = accessor.CountyLandUseDescription;
            StandardizedLandUseCode = accessor.StandardizedLandUseCode;
            StandardizedLandUseDesc = accessor.StandardizedLandUseDesc;
            LegalLotNumber = accessor.LegalLotNumber;
            LegalSubdivisionName = accessor.LegalSubdivisionName;
            RecordTypeCode = accessor.RecordTypeCode;
            RecordTypeDesc = accessor.RecordTypeDesc;
            MortgageLoanAmount = accessor.MortgageLoanAmount;
            MortgageLoanTypeCode = accessor.MortgageLoanTypeCode;
            MortgageLoanTypeDesc = accessor.MortgageLoanTypeDesc;
            MortgageLenderName = accessor.MortgageLenderName;
            AssessedTotalValue = accessor.AssessedTotalValue;
            AssessedValueYear = accessor.AssessedValueYear;
            HomesteadHomeownerExemption = accessor.HomesteadHomeownerExemption;
            MarketImprovementValue = accessor.MarketImprovementValue;
            MarketTotalValue = accessor.MarketTotalValue;
            MarketValueYear = accessor.MarketValueYear;
            TaxAmount = accessor.TaxAmount;
            TaxYear = accessor.TaxYear;
            LandSquareFootage = accessor.LandSquareFootage;
            YearBuilt = accessor.YearBuilt;
            NumberOfStories = accessor.NumberOfStories;
            NumberOfStoriesDesc = accessor.NumberOfStoriesDesc;
            NumberOfBedrooms = accessor.NumberOfBedrooms;
            NumberOfBaths = accessor.NumberOfBaths;
            NumberOfPartialBaths = accessor.NumberOfPartialBaths;
            GarageTypeCode = accessor.GarageTypeCode;
            PoolCode = accessor.PoolCode;
            PoolDesc = accessor.PoolDesc;
            ExteriorWallsCode = accessor.ExteriorWallsCode;
            ExteriorWallsDesc = accessor.ExteriorWallsDesc;
            RoofTypeCode = accessor.RoofTypeCode;
            RoofTypeDesc = accessor.RoofTypeDesc;
            HeatingCode = accessor.HeatingCode;
            HeatingDesc = accessor.HeatingDesc;
            HeatingFuelTypeCode = accessor.HeatingFuelTypeCode;
            HeatingFuelTypeDesc = accessor.HeatingFuelTypeDesc;
            AirConditioningCode = accessor.AirConditioningCode;
            AirConditioningDesc = accessor.AirConditioningDesc;
            AirConditioningTypeCode = accessor.AirConditioningTypeCode;
            AirConditioningTypeDesc = accessor.AirConditioningTypeDesc;
        }
        public string StateCode { get; set; }
        public string FipsCode { get; set; }
        public string DuplicateApnMultipleAddressId { get; set; }
        public string AssesseeOwnershipRightsCode { get; set; }
        public string AssesseeOwnershipRightsDesc { get; set; }
        public string AssesseeRelationshipCode { get; set; }
        public string AssesseeRelationshipDesc { get; set; }
        public string OwnerOccupied { get; set; }
        public string PriorRecordingDate { get; set; }
        public string CountyLandUseDescription { get; set; }
        public string StandardizedLandUseCode { get; set; }
        public string StandardizedLandUseDesc { get; set; }
        public string LegalLotNumber { get; set; }
        public string LegalSubdivisionName { get; set; }
        public string RecordTypeCode { get; set; }
        public string RecordTypeDesc { get; set; }
        public string MortgageLoanAmount { get; set; }
        public string MortgageLoanTypeCode { get; set; }
        public string MortgageLoanTypeDesc { get; set; }
        public string MortgageLenderName { get; set; }
        public string AssessedTotalValue { get; set; }
        public string AssessedValueYear { get; set; }
        public string HomesteadHomeownerExemption { get; set; }
        public string MarketImprovementValue { get; set; }
        public string MarketTotalValue { get; set; }
        public string MarketValueYear { get; set; }
        public string TaxAmount { get; set; }
        public string TaxYear { get; set; }
        public string LandSquareFootage { get; set; }
        public string YearBuilt { get; set; }
        public string NumberOfStories { get; set; }
        public string NumberOfStoriesDesc { get; set; }
        public string NumberOfBedrooms { get; set; }
        public string NumberOfBaths { get; set; }
        public string NumberOfPartialBaths { get; set; }
        public string GarageTypeCode { get; set; }
        public string PoolCode { get; set; }
        public string PoolDesc { get; set; }
        public string ExteriorWallsCode { get; set; }
        public string ExteriorWallsDesc { get; set; }
        public string RoofTypeCode { get; set; }
        public string RoofTypeDesc { get; set; }
        public string HeatingCode { get; set; }
        public string HeatingDesc { get; set; }
        public string HeatingFuelTypeCode { get; set; }
        public string HeatingFuelTypeDesc { get; set; }
        public string AirConditioningCode { get; set; }
        public string AirConditioningDesc { get; set; }
        public string AirConditioningTypeCode { get; set; }
        public string AirConditioningTypeDesc { get; set; }
    }
}
