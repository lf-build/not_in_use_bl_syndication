namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IDebtBeyondTermsPercent
	{
		 int Current { get; set; }
		 int Day01To30 { get; set; }
		 int Day31To60 { get; set; }
		 int Day61To90 { get; set; }
		 int Day91ToPlus { get; set; }
	}
}
