using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SanctionSection : ISanctionSection
    {
        public SanctionSection()
        { }
        public SanctionSection(ServiceReference.TopBusinessSanctionSection sanctionSection)
        {
            if (sanctionSection?.Sanctions != null)
            {
                Sanctions = new List<ISanctionRecord>(sanctionSection.Sanctions.Select(sanction => new SanctionRecord(sanction)));
            }
        }
        public List<ISanctionRecord> Sanctions { get; set; }
    }
}
