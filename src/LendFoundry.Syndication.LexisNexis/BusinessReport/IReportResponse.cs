using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IReportResponse
    {
        IResponseHeader Header { get; set; }
        List<IReportRecord> Businesses { get; set; }
    }
}
