using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditInquiryItem : IBizCreditInquiryItem
	{
        public BizCreditInquiryItem()
        { }
        public BizCreditInquiryItem(ServiceReference.BizCreditInquiryItem bizCreditInquiryItem)
        {
            if (bizCreditInquiryItem == null)
                return;
            InquiryType = bizCreditInquiryItem.InquiryType;
            if(bizCreditInquiryItem.InquiryCounts != null)
            {
                InquiryCounts = new List<IBizCreditInquiryCount>(bizCreditInquiryItem.InquiryCounts.Select(inquiryCount => new BizCreditInquiryCount(inquiryCount)));
            }
        }
        public string InquiryType { get; set; }
		public List<IBizCreditInquiryCount> InquiryCounts { get; set; }
	}
}
