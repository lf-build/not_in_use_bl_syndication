using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SourceCategory : ISourceCategory
    {
        public SourceCategory()
        { }
        public SourceCategory(ServiceReference.TopBusinessSourceCategory sourceCategory)
        {
            if (sourceCategory == null)
                return;
            Name = sourceCategory.Name;
            DocCount = sourceCategory.DocCount;
            if (sourceCategory.SourceDocs != null)
            {
                var sourceDocs = new List<ISourceDocInfo>();
                foreach (var sourceDoc in sourceCategory.SourceDocs)
                {
                    sourceDocs.Add(new SourceDocInfo(sourceDoc));
                }
                SourceDocs = sourceDocs;
            }
        }
        public string Name { get; set; }
        public uint DocCount { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
