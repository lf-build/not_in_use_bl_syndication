using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ILienSection
    {
        int DerogSummaryCntJl { get; set; }
        int ReturnedRecordCount { get; set; }
        int TotalRecordCount { get; set; }
        List<IJudgmentLienDetail> JudgmentsLiens { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
