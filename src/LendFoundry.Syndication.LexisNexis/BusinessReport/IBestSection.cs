using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBestSection
    {
        IBusinessIdentity BusinessIds { get; set; }
        string CompanyNameSource { get; set; }
        string CompanyNameSourceDocId { get; set; }
        string TinSource { get; set; }
        string Tin { get; set; }
        string Ticker { get; set; }
        string Exchange { get; set; }
        string UrlSource { get; set; }
        string Url { get; set; }
        string Source { get; set; }
        string SourceDocId { get; set; }
        string SourceParty { get; set; }
        string CompanyName { get; set; }
        IAddress Address { get; set; }
        IPhoneInfo PhoneInfo { get; set; }
        IDate PhoneFromDate { get; set; }
        IDate PhoneToDate { get; set; }
        string PhoneType { get; set; }
        bool ActiveEda { get; set; }
        bool Disconnected { get; set; }
        string WirelessIndicator { get; set; }
        string Fax { get; set; }
        short YearStarted { get; set; }
        bool YearStartedDerived { get; set; }
        string YearStartedSource { get; set; }
        string YearStartedSourceDocId { get; set; }
        short YearsInBusiness { get; set; }
        short CountOtherTiNs { get; set; }
        short TotalCountOtherTiNs { get; set; }
        short CountOtherCompanies { get; set; }
        short TotalCountOtherCompanies { get; set; }
        bool IsDefunct { get; set; }
        bool IsActive { get; set; }
        List<IBestOtherCompany> OtherCompanyNames { get; set; }
        List<IBestOtherTins> OtherCompanyTins { get; set; }
    }
}
