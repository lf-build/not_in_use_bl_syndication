namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditJudgmentTotal : IBizCreditJudgmentTotal
    {
        public BizCreditJudgmentTotal()
        { }
        public BizCreditJudgmentTotal(ServiceReference.BizCreditJudgmentTotal bizCreditJudgmentTotal)
        {
            if (bizCreditJudgmentTotal == null)
                return;
            Filed = bizCreditJudgmentTotal.Filed;
            FiledLast24Months = bizCreditJudgmentTotal.FiledLast24Months;
            NonFiled = bizCreditJudgmentTotal.NonFiled;
            LiabilityAmountFiled = bizCreditJudgmentTotal.LiabilityAmountFiled;
            LiabilityAmountFiledLast24Months = bizCreditJudgmentTotal.LiabilityAmountFiledLast24Months;
            LiabilityAmountNonFiled = bizCreditJudgmentTotal.LiabilityAmountNonFiled;
            MonthsSinceLastFiling = bizCreditJudgmentTotal.MonthsSinceLastFiling;
            MonthsSinceLastNonFiling = bizCreditJudgmentTotal.MonthsSinceLastNonFiling;
            JudgmentAmount = bizCreditJudgmentTotal.JudgmentAmount;
            PercentJudgmentAmountToTradeBalance = bizCreditJudgmentTotal.PercentJudgmentAmountToTradeBalance;
            PaymentReceived = bizCreditJudgmentTotal.PaymentReceived;
        }
        public int Filed { get; set; }
        public int FiledLast24Months { get; set; }
        public int NonFiled { get; set; }
        public string LiabilityAmountFiled { get; set; }
        public string LiabilityAmountFiledLast24Months { get; set; }
        public string LiabilityAmountNonFiled { get; set; }
        public int MonthsSinceLastFiling { get; set; }
        public int MonthsSinceLastNonFiling { get; set; }
        public string JudgmentAmount { get; set; }
        public string PercentJudgmentAmountToTradeBalance { get; set; }
        public bool PaymentReceived { get; set; }
    }
}
