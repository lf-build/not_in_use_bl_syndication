namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class CompanyVerificationSection : ICompanyVerificationSection
    {
        public CompanyVerificationSection()
        { }
        public CompanyVerificationSection(ServiceReference.TopBusinessCompanyVerificationSection companyVerificationSection)
        {
            if (companyVerificationSection == null)
                return;
            CompanyVerificationRecordCount = companyVerificationSection.CompanyVerificationRecordCount;
            CompanyVerification = new CompanyVerification(companyVerificationSection.CompanyVerification);
        }
        public uint CompanyVerificationRecordCount { get; set; }
        public ICompanyVerification CompanyVerification { get; set; }
    }
}
