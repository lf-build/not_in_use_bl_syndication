using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class FinanceSection : IFinanceSection
    {
        public FinanceSection()
        { }
        public FinanceSection(ServiceReference.TopBusinessFinanceSection financeSection)
        {
            if (financeSection?.Finances != null)
            {
                var finances = financeSection.Finances.Select(finance => new Finance(finance)).Cast<IFinance>().ToList();
                Finances = finances;
            }
        }
        public List<IFinance> Finances { get; set; }
    }
}
