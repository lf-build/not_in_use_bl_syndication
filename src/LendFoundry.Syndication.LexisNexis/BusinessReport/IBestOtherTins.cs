using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBestOtherTins
    {
        string Tin { get; set; }
        List<IBestCompanyNameInfo> CompanyNames { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
