namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTradeQuarterlyAverage : IBizCreditTradeQuarterlyAverage
    {
        public BizCreditTradeQuarterlyAverage()
        { }
        public BizCreditTradeQuarterlyAverage(ServiceReference.BizCreditTradeQuarterlyAverage bizCreditTradeQuarterlyAverage)
        {
            if (bizCreditTradeQuarterlyAverage == null)
                return;
            Quarter = bizCreditTradeQuarterlyAverage.Quarter;
            Year = bizCreditTradeQuarterlyAverage.Year;
            Debt = bizCreditTradeQuarterlyAverage.Debt;
            AccountBalanceMask = bizCreditTradeQuarterlyAverage.AccountBalanceMask;
            AccountBalance = bizCreditTradeQuarterlyAverage.AccountBalance;
            DbtPercentages = new DebtBeyondTermsPercent(bizCreditTradeQuarterlyAverage.DBTPercentages);
        }
        public int Quarter { get; set; }
        public int Year { get; set; }
        public int Debt { get; set; }
        public string AccountBalanceMask { get; set; }
        public int AccountBalance { get; set; }
        public IDebtBeyondTermsPercent DbtPercentages { get; set; }
    }
}
