namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Position : IPosition
    {
        public Position()
        { }
        public Position(ServiceReference.TopBusinessPosition position)
        {
            if (position == null)
                return;
            CompanyTitle = position.CompanyTitle;
            FromDate = new Date(position.FromDate);
            ToDate = new Date(position.ToDate);
        }
        public string CompanyTitle { get; set; }
        public IDate FromDate { get; set; }
        public IDate ToDate { get; set; }
    }
}
