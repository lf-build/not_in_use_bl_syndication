using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class AssociateSection : IAssociateSection
	{
        public AssociateSection()
        { }
        public AssociateSection(ServiceReference.TopBusinessAssociateSection associateSection)
        {
            if (associateSection == null)
                return;
            ReturnBusinessCount = associateSection.ReturnBusinessCount;
            TotalBusinessCount = associateSection.TotalBusinessCount;
            if(associateSection.BusinessAssociates !=null)
            {
                BusinessAssociates = new List<IAssociateBusiness>(associateSection.BusinessAssociates.Select(businessAssociate => new AssociateBusiness(businessAssociate)));
            }

            ReturnPersonCount = associateSection.ReturnPersonCount;
            TotalPersonCount = associateSection.TotalPersonCount;
            if(associateSection.PersonAssociates != null)
            {
                PersonAssociates = new List<IAssociatePerson>(associateSection.PersonAssociates.Select(personAssociate => new AssociatePerson(personAssociate)));
            }
        }

        public int ReturnBusinessCount { get; set; }
		public int TotalBusinessCount { get; set; }
		public List<IAssociateBusiness> BusinessAssociates { get; set; }
		public int ReturnPersonCount { get; set; }
		public int TotalPersonCount { get; set; }
		public List<IAssociatePerson> PersonAssociates { get; set; }
	}
}
