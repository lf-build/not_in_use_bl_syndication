namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class WatercraftSection : IWatercraftSection
    {
        public WatercraftSection()
        { }
        public WatercraftSection(ServiceReference.TopBusinessWatercraftSection watercraftSection)
        {
            if (watercraftSection == null)
                return;
            WatercraftRecordCount = watercraftSection.WatercraftRecordCount;
            TotalWatercraftRecordCount = watercraftSection.TotalWatercraftRecordCount;
            WatercraftRecords = new Watercraft(watercraftSection.WatercraftRecords);
        }
        public int WatercraftRecordCount { get; set; }
        public int TotalWatercraftRecordCount { get; set; }
        public IWatercraft WatercraftRecords { get; set; }
    }
}
