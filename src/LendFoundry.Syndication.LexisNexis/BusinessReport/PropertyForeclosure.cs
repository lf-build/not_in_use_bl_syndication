using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PropertyForeclosure : IPropertyForeclosure
    {
        public PropertyForeclosure()
        { }
        public PropertyForeclosure(ServiceReference.TopBusinessPropertyForeclosure propertyForeclosure)
        {
            if (propertyForeclosure == null)
                return;
            RecordingDate = new Date(propertyForeclosure.RecordingDate);
            AuctionDate = new Date(propertyForeclosure.AuctionDate);
            PlaintiffName1 = propertyForeclosure.PlaintiffName1;
            PlaintiffName2 = propertyForeclosure.PlaintiffName2;
            PlaintiffCompanyName = propertyForeclosure.PlaintiffCompanyName;
            DefendantName1 = propertyForeclosure.DefendantName1;
            DefendantName2 = propertyForeclosure.DefendantName2;
            DefendantName3 = propertyForeclosure.DefendantName3;
            DefendantName4 = propertyForeclosure.DefendantName4;
            DefendantCompanyName1 = propertyForeclosure.DefendantCompanyName1;
            DefendantCompanyName2 = propertyForeclosure.DefendantCompanyName2;
            DefendantCompanyName3 = propertyForeclosure.DefendantCompanyName3;
            DefendantCompanyName4 = propertyForeclosure.DefendantCompanyName4;
            SiteAddress1 = new Address(propertyForeclosure.SiteAddress1);
            SiteAddress2 = new Address(propertyForeclosure.SiteAddress2);
            LenderFirstName = propertyForeclosure.LenderFirstName;
            LenderLastName = propertyForeclosure.LenderLastName;
            LenderCompanyName = propertyForeclosure.LenderCompanyName;
            AttorneyName = propertyForeclosure.AttorneyName;
            AttorneyPhoneNumber = propertyForeclosure.AttorneyPhoneNumber;
            Address = new Address(propertyForeclosure.Address);
            DocumentType = propertyForeclosure.DocumentType;
            if (propertyForeclosure.FSourceDocs != null)
            {
                var fSourceDocs = new List<ISourceDocInfo>();
                foreach (var fSourceDoc in propertyForeclosure.FSourceDocs)
                {
                    fSourceDocs.Add(new SourceDocInfo(fSourceDoc));
                }
                FSourceDocs = fSourceDocs;
            }
        }
        public IDate RecordingDate { get; set; }
        public IDate AuctionDate { get; set; }
        public string PlaintiffName1 { get; set; }
        public string PlaintiffName2 { get; set; }
        public string PlaintiffCompanyName { get; set; }
        public string DefendantName1 { get; set; }
        public string DefendantName2 { get; set; }
        public string DefendantName3 { get; set; }
        public string DefendantName4 { get; set; }
        public string DefendantCompanyName1 { get; set; }
        public string DefendantCompanyName2 { get; set; }
        public string DefendantCompanyName3 { get; set; }
        public string DefendantCompanyName4 { get; set; }
        public IAddress SiteAddress1 { get; set; }
        public IAddress SiteAddress2 { get; set; }
        public string LenderFirstName { get; set; }
        public string LenderLastName { get; set; }
        public string LenderCompanyName { get; set; }
        public string AttorneyName { get; set; }
        public string AttorneyPhoneNumber { get; set; }
        public IAddress Address { get; set; }
        public string DocumentType { get; set; }
        public List<ISourceDocInfo> FSourceDocs { get; set; }
    }
}
