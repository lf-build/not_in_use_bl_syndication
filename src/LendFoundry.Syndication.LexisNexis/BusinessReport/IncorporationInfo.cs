using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IncorporationInfo : IIncorporationInfo
    {
        public IncorporationInfo()
        { }
        public IncorporationInfo(ServiceReference.TopBusinessIncorporationInfo incorporationInfo)
        {
            if (incorporationInfo == null)
                return;
            StateOfOrigin = incorporationInfo.StateOfOrigin;
            CorporationName = incorporationInfo.CorporationName;
            FilingNumber = incorporationInfo.FilingNumber;
            BusinessType = incorporationInfo.BusinessType;
            FilingDate = new Date(incorporationInfo.FilingDate);
            BusinessStatus = incorporationInfo.BusinessStatus;
            NameStatus = incorporationInfo.NameStatus;
            ForeignDomesticIndicator = incorporationInfo.ForeignDomesticIndicator;
            ForeignState = incorporationInfo.ForeignState;
            FilingType = incorporationInfo.FilingType;
            ForProfitIndicator = incorporationInfo.ForProfitIndicator;
            Origin = incorporationInfo.Origin;
            if (incorporationInfo.CorpHistories != null)
            {
                CorpHistories = new List<IIncorporationHistory>(incorporationInfo.CorpHistories.Select(corpHistory => new IncorporationHistory(corpHistory)));
            }
            TermOfExistence = new IncorporationExistenceTerm(incorporationInfo.TermOfExistence);
            if (incorporationInfo.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(incorporationInfo.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            NameType = incorporationInfo.NameType;
            if (incorporationInfo.Addresses != null)
            {
                Addresses = new List<IIncorporationAddress>(incorporationInfo.Addresses.Select(address => new IncorporationAddress(address)));
            }
            InGoodStanding = incorporationInfo.InGoodStanding;
            ForeignIncorporationDate = new Date(incorporationInfo.ForeignIncorporationDate);
            Purpose = incorporationInfo.Purpose;
            AdditionalInfo = incorporationInfo.AdditionalInfo;
            RegisteredAgentName = new NameAndCompany(incorporationInfo.RegisteredAgentName);
            RegisteredAgentAddress = new Address(incorporationInfo.RegisteredAgentAddress);
        }
        public string StateOfOrigin { get; set; }
        public string CorporationName { get; set; }
        public string FilingNumber { get; set; }
        public string BusinessType { get; set; }
        public IDate FilingDate { get; set; }
        public string BusinessStatus { get; set; }
        public string NameStatus { get; set; }
        public string ForeignDomesticIndicator { get; set; }
        public string ForeignState { get; set; }
        public string FilingType { get; set; }
        public string ForProfitIndicator { get; set; }
        public string Origin { get; set; }
        public List<IIncorporationHistory> CorpHistories { get; set; }
        public IIncorporationExistenceTerm TermOfExistence { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public string NameType { get; set; }
        public List<IIncorporationAddress> Addresses { get; set; }
        public string InGoodStanding { get; set; }
        public IDate ForeignIncorporationDate { get; set; }
        public string Purpose { get; set; }
        public string AdditionalInfo { get; set; }
        public INameAndCompany RegisteredAgentName { get; set; }
        public IAddress RegisteredAgentAddress { get; set; }
    }
}
