﻿namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public enum LevelType
    {
        S,
        D,
        E,
        W,
        P,
        O,
        U,
        [System.Xml.Serialization.XmlEnumAttribute("")]
        Item,
    }
}
