namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTradeQuarterlyAverage
	{
		 int Quarter { get; set; }
		 int Year { get; set; }
		 int Debt { get; set; }
		 string AccountBalanceMask { get; set; }
		 int AccountBalance { get; set; }
		 IDebtBeyondTermsPercent DbtPercentages { get; set; }
	}
}
