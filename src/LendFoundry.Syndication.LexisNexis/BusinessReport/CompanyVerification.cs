namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class CompanyVerification : ICompanyVerification
    {
        public CompanyVerification()
        { }
        public CompanyVerification(ServiceReference.CompanyVerification companyVerification)
        {
            if (companyVerification == null)
                return;
            VerifiedIndicators = new CompanyVerificationIndicators(companyVerification.VerifiedIndicators);
            VerifiedInputs = new CompanyVerificationData(companyVerification.VerifiedInputs);
        }
        public ICompanyVerificationIndicators VerifiedIndicators { get; set; }
        public ICompanyVerificationData VerifiedInputs { get; set; }
    }
}
