using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IOperationSite
    {
        string StateName { get; set; }
        int StateAddressCount { get; set; }
        int StateTotalAddressCount { get; set; }
        List<IOpSiteAddress> Addresses { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
