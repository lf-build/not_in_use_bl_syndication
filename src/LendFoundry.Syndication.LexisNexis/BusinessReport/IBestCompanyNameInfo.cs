namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBestCompanyNameInfo
	{
		 string CompanyName { get; set; }
	}
}
