namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class URL : IUrl
    {
        public URL()
        {
            
        }

        public URL(ServiceReference.TopBusinessURL url)
        {
            if (url == null)
                return;
            Url = url.URL;
        }

        public string Url { get; set; }
    }
}
