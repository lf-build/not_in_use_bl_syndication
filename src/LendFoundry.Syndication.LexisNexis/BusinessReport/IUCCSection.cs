namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IUccSection
	{
		 int DerogSummaryCntUcc { get; set; }
		 int SecuredAssetsCntUcc { get; set; }
		 int ReturnedAsDebtorCount { get; set; }
		 int TotalAsDebtorCount { get; set; }
		 int ReturnedAsSecuredCount { get; set; }
		 int TotalAsSecuredCount { get; set; }
		 IUccRole AsDebtor { get; set; }
		 IUccRole AsSecured { get; set; }
	}
}
