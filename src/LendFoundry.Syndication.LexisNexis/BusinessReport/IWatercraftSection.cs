namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IWatercraftSection
	{
		 int WatercraftRecordCount { get; set; }
		 int TotalWatercraftRecordCount { get; set; }
		 IWatercraft WatercraftRecords { get; set; }
	}
}
