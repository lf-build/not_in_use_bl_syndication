namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class OpSitePhone : IOpSitePhone
    {
        public OpSitePhone()
        { }
        public OpSitePhone(ServiceReference.TopBusinessOpSitePhone opSitePhone)
        {
            if (opSitePhone == null)
                return;
            Phone10 = opSitePhone.Phone10;
            ListingType = opSitePhone.ListingType;
            ActiveEda = opSitePhone.ActiveEDA;
            Disconnected = opSitePhone.Disconnected;
            LineType = opSitePhone.LineType;
            ListingName = opSitePhone.ListingName;
            FromDate = new Date(opSitePhone.FromDate);
            ToDate = new Date(opSitePhone.ToDate);
        }
        public string Phone10 { get; set; }
        public string ListingType { get; set; }
        public bool ActiveEda { get; set; }
        public bool Disconnected { get; set; }
        public string LineType { get; set; }
        public string ListingName { get; set; }
        public IDate FromDate { get; set; }
        public IDate ToDate { get; set; }
    }
}
