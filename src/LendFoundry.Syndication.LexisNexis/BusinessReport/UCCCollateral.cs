namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UccCollateral : IUccCollateral
    {
        public UccCollateral()
        { }
        public UccCollateral(ServiceReference.TopBusinessUCCCollateral ucccollateral)
        {
            if (ucccollateral == null)
                return;
            FilingNumber = ucccollateral.FilingNumber;
            FilingDate = new Date(ucccollateral.FilingDate);
            CollateralSequence = ucccollateral.CollateralSequence;
            CollateralDescription = ucccollateral.CollateralDescription;
        }
        public string FilingNumber { get; set; }
        public IDate FilingDate { get; set; }
        public int CollateralSequence { get; set; }
        public string CollateralDescription { get; set; }
    }
}
