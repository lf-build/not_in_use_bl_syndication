namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ICompanyVerificationData
	{
		 string CompanyName { get; set; }
		 IAddress Address { get; set; }
		 string Phone10 { get; set; }
		 string Fein { get; set; }
	}
}
