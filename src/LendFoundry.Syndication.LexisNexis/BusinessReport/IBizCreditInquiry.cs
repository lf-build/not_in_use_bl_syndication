using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditInquiry
    {
        IDateRange DateRange { get; set; }
        List<IBizCreditInquiryItem> Inquiries { get; set; }
    }
}
