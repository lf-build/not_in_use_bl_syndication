namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IWatercraftParty
	{
		 string PartyTypeDescription { get; set; }
		 string CompanyName { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
	}
}
