using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BestSection : IBestSection
    {
        public BestSection()
        { }
        public BestSection(ServiceReference.TopBusinessBestSection bestSection)
        {
            if (bestSection == null)
                return;
            BusinessIds = new BusinessIdentity(bestSection.BusinessIds);
            CompanyNameSource = bestSection.CompanyNameSource;
            CompanyNameSourceDocId = bestSection.CompanyNameSourceDocId;
            TinSource = bestSection.TinSource;
            Tin = bestSection.Tin;
            Ticker = bestSection.Ticker;
            Exchange = bestSection.Exchange;
            UrlSource = bestSection.UrlSource;
            Url = bestSection.URL;
            Source = bestSection.Source;
            SourceDocId = bestSection.SourceDocId;
            SourceParty = bestSection.SourceParty;
            CompanyName = bestSection.CompanyName;
            Address = new Address(bestSection.Address);
            PhoneInfo = new PhoneInfo(bestSection.PhoneInfo);
            PhoneFromDate = new Date(bestSection.PhoneFromDate);
            PhoneToDate = new Date(bestSection.PhoneToDate);
            PhoneType = bestSection.PhoneType;
            ActiveEda = bestSection.ActiveEDA;
            Disconnected = bestSection.Disconnected;
            WirelessIndicator = bestSection.WirelessIndicator;
            Fax = bestSection.FAX;
            YearStarted = bestSection.YearStarted;
            YearStartedDerived = bestSection.YearStartedDerived;
            YearStartedSource = bestSection.YearStartedSource;
            YearStartedSourceDocId = bestSection.YearStartedSourceDocId;
            YearsInBusiness = bestSection.YearsInBusiness;
            CountOtherTiNs = bestSection.CountOtherTINs;
            TotalCountOtherTiNs = bestSection.TotalCountOtherTINs;
            CountOtherCompanies = bestSection.CountOtherCompanies;
            TotalCountOtherCompanies = bestSection.TotalCountOtherCompanies;
            IsDefunct = bestSection.IsDefunct;
            IsActive = bestSection.IsActive;
            if (bestSection.OtherCompanyNames != null)
            {
                OtherCompanyNames = new List<IBestOtherCompany>(bestSection.OtherCompanyNames.Select(otherCompanyName => new BestOtherCompany(otherCompanyName)));
            }
            if (bestSection.OtherCompanyTins != null)
            {
                OtherCompanyTins = new List<IBestOtherTins>(bestSection.OtherCompanyTins.Select(otherCompanyTin => new BestOtherTins(otherCompanyTin)));
            }
        }
        public IBusinessIdentity BusinessIds { get; set; }
        public string CompanyNameSource { get; set; }
        public string CompanyNameSourceDocId { get; set; }
        public string TinSource { get; set; }
        public string Tin { get; set; }
        public string Ticker { get; set; }
        public string Exchange { get; set; }
        public string UrlSource { get; set; }
        public string Url { get; set; }
        public string Source { get; set; }
        public string SourceDocId { get; set; }
        public string SourceParty { get; set; }
        public string CompanyName { get; set; }
        public IAddress Address { get; set; }
        public IPhoneInfo PhoneInfo { get; set; }
        public IDate PhoneFromDate { get; set; }
        public IDate PhoneToDate { get; set; }
        public string PhoneType { get; set; }
        public bool ActiveEda { get; set; }
        public bool Disconnected { get; set; }
        public string WirelessIndicator { get; set; }
        public string Fax { get; set; }
        public short YearStarted { get; set; }
        public bool YearStartedDerived { get; set; }
        public string YearStartedSource { get; set; }
        public string YearStartedSourceDocId { get; set; }
        public short YearsInBusiness { get; set; }
        public short CountOtherTiNs { get; set; }
        public short TotalCountOtherTiNs { get; set; }
        public short CountOtherCompanies { get; set; }
        public short TotalCountOtherCompanies { get; set; }
        public bool IsDefunct { get; set; }
        public bool IsActive { get; set; }
        public List<IBestOtherCompany> OtherCompanyNames { get; set; }
        public List<IBestOtherTins> OtherCompanyTins { get; set; }
    }
}
