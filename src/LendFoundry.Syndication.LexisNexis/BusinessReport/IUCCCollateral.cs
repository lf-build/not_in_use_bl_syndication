namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IUccCollateral
	{
		 string FilingNumber { get; set; }
		 IDate FilingDate { get; set; }
		 int CollateralSequence { get; set; }
		 string CollateralDescription { get; set; }
	}
}
