using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Finance : IFinance
    {
        public Finance()
        { }
        public Finance(ServiceReference.TopBusinessFinance finance)
        {
            if (finance == null)
                return;
            if (finance.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(finance.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
            AnnualSales = finance.AnnualSales;
            SalesDate = new Date(finance.SalesDate);
        }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public string AnnualSales { get; set; }
        public IDate SalesDate { get; set; }
    }
}
