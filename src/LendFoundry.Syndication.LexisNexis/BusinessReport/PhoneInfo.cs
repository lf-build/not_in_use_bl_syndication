namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PhoneInfo : IPhoneInfo
    {
        public PhoneInfo()
        { }
        public PhoneInfo(ServiceReference.PhoneInfo phoneInfo)
        {
            if (phoneInfo == null)
                return;
            Phone10 = phoneInfo.Phone10;
            PubNonpub = phoneInfo.PubNonpub;
            ListingPhone10 = phoneInfo.ListingPhone10;
            ListingName = phoneInfo.ListingName;
            TimeZone = phoneInfo.TimeZone;
            ListingTimeZone = phoneInfo.ListingTimeZone;
        }
        public string Phone10 { get; set; }
        public string PubNonpub { get; set; }
        public string ListingPhone10 { get; set; }
        public string ListingName { get; set; }
        public string TimeZone { get; set; }
        public string ListingTimeZone { get; set; }
    }
}
