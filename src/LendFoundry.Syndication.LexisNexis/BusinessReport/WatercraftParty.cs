namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class WatercraftParty : IWatercraftParty
    {
        public WatercraftParty()
        { }
        public WatercraftParty(ServiceReference.TopBusinessWatercraftParty watercraftParty)
        {
            if (watercraftParty == null)
                return;
            PartyTypeDescription = watercraftParty.PartyTypeDescription;
            CompanyName = watercraftParty.CompanyName;
            Name = new Name(watercraftParty.Name);
            Address = new Address(watercraftParty.Address);
        }
        public string PartyTypeDescription { get; set; }
        public string CompanyName { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
    }
}
