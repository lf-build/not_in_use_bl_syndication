namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTradePaymentTrend : IBizCreditTradePaymentTrend
    {
        public BizCreditTradePaymentTrend()
        { }
        public BizCreditTradePaymentTrend(ServiceReference.BizCreditTradePaymentTrend bizCreditTradePaymentTrend)
        {
            if (bizCreditTradePaymentTrend == null)
                return;
            Date = new Date(bizCreditTradePaymentTrend.Date);
            Debt = bizCreditTradePaymentTrend.Debt;
            AccountBalanceMask = bizCreditTradePaymentTrend.AccountBalanceMask;
            AccountBalance = bizCreditTradePaymentTrend.AccountBalance;
            DbtPercentages = new DebtBeyondTermsPercent(bizCreditTradePaymentTrend.DBTPercentages);
        }
        public IDate Date { get; set; }
        public int Debt { get; set; }
        public string AccountBalanceMask { get; set; }
        public int AccountBalance { get; set; }
        public IDebtBeyondTermsPercent DbtPercentages { get; set; }
    }
}
