using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class JudgmentLienDetail : IJudgmentLienDetail
    {
        public JudgmentLienDetail()
        { }
        public JudgmentLienDetail(ServiceReference.TopBusinessJudgmentLienDetail judgmentLienDetail)
        {
            if (judgmentLienDetail == null)
                return;
            if (judgmentLienDetail.Debtors != null)
            {
                Debtors = new List<IJudgmentLienParty>(judgmentLienDetail.Debtors.Select(debtor => new JudgmentLienParty(debtor)));
            }
            if (judgmentLienDetail.Creditors != null)
            {
                Creditors = new List<IJudgmentLienParty>(judgmentLienDetail.Creditors.Select(creditor => new JudgmentLienParty(creditor)));
            }
            if (judgmentLienDetail.Filings != null)
            {
                Filings = new List<IJudgmentLienFilings>(judgmentLienDetail.Filings.Select(filing => new JudgmentLienFilings(filing)));
            }
            FilingJurisdiction = judgmentLienDetail.FilingJurisdiction;
            Amount = judgmentLienDetail.Amount;
            OrigFilingNumber = judgmentLienDetail.OrigFilingNumber;
            OrigFilingType = judgmentLienDetail.OrigFilingType;
            OrigFilingDate = new Date(judgmentLienDetail.OrigFilingDate);
            Eviction = judgmentLienDetail.Eviction;
            if (judgmentLienDetail.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(judgmentLienDetail.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public List<IJudgmentLienParty> Debtors { get; set; }
        public List<IJudgmentLienParty> Creditors { get; set; }
        public List<IJudgmentLienFilings> Filings { get; set; }
        public string FilingJurisdiction { get; set; }
        public string Amount { get; set; }
        public string OrigFilingNumber { get; set; }
        public string OrigFilingType { get; set; }
        public IDate OrigFilingDate { get; set; }
        public string Eviction { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
