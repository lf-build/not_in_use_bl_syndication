using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditReportRecord
    {
        IBusinessIdentity BusinessIds { get; set; }
        string IdValue { get; set; }
        string FileNumber { get; set; }
        IBizCreditBusinessHeader BusinessHeader { get; set; }
        List<IBizCreditExecutiveSummary> ExecutiveSummaries { get; set; }
        List<IBizCreditTradePaymentDetail> TradePaymentDetails { get; set; }
        List<IBizCreditTradePaymentTotal> TradePaymentTotals { get; set; }
        List<IBizCreditTradePaymentTrend> TradePaymentTrends { get; set; }
        List<IBizCreditTradeQuarterlyAverage> TradeQuarterlyAverages { get; set; }
        List<IBizCreditBankruptcy> Bankruptcies { get; set; }
        List<IBizCreditTaxLien> TaxLiens { get; set; }
        List<IBizCreditJudgment> Judgments { get; set; }
        List<IBizCreditCollateralAccount> CollateralAccounts { get; set; }
        List<IBizCreditUccFiling> UccFilings { get; set; }
        List<IBizCreditBankingDetail> BankingDetails { get; set; }
        List<IBizCreditDemographic5600> Demographics5600 { get; set; }
        List<IBizCreditDemographic5610> Demographics5610 { get; set; }
        List<IBizCreditInquiry> InquiryRecords { get; set; }
        List<IBizCreditGovernmentTrade> GovernmentTrades { get; set; }
        List<IBizCreditGovernmentDebarredContractor> GovernmentDebarredContractors { get; set; }
        List<IBizCredSnp> SnPs { get; set; }
        IBizCreditDerogatory Derogatory { get; set; }
        IBizCreditBankruptcyTotal BankruptcyTotals { get; set; }
        IBizCreditJudgmentTotal JudgmentTotals { get; set; }
        IBizCreditLienTotal LienTotals { get; set; }
        IBizCreditUccTotal UccTotals { get; set; }
        IBizCreditTradeInfo TradeInfo { get; set; }
    }
}
