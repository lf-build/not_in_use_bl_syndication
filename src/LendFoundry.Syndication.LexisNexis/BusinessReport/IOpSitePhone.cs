namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IOpSitePhone
	{
		 string Phone10 { get; set; }
		 string ListingType { get; set; }
		 bool ActiveEda { get; set; }
		 bool Disconnected { get; set; }
		 string LineType { get; set; }
		 string ListingName { get; set; }
		 IDate FromDate { get; set; }
		 IDate ToDate { get; set; }
	}
}
