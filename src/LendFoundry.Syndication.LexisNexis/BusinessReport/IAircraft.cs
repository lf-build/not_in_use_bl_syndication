using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAircraft
	{
		 string AircraftNumber { get; set; }
		 string ModelYear { get; set; }
		 string Manufacturer { get; set; }
		 string Model { get; set; }
		 string AircraftType { get; set; }
		 string SerialNumber { get; set; }
		 int Engines { get; set; }
		 IDate RegistrationDate { get; set; }
		 string RegistrationNumber { get; set; }
		 IAddress RegistrationAddress { get; set; }
        List<IAircraftParty> CurrentParties { get; set; }
        List<IAircraftParty> PriorParties { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
	}
}
