using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Watercraft : IWatercraft
    {
        public Watercraft()
        { }
        public Watercraft(ServiceReference.TopBusinessWatercraft watercraft)
        {
            if (watercraft == null)
                return;
            CurrentRecordCount = watercraft.CurrentRecordCount;
            TotalCurrentRecordCount = watercraft.TotalCurrentRecordCount;
            if (watercraft.CurrentWatercrafts != null)
            {
                var currentWatercrafts = new List<IWatercraftDetail>();
                foreach (var currentWatercraft in watercraft.CurrentWatercrafts)
                {
                    currentWatercrafts.Add(new WatercraftDetail(currentWatercraft));
                }
                CurrentWatercrafts = currentWatercrafts;
            }
            if (watercraft.CurrentSourceDocs != null)
            {
                var currentSourceDocs = new List<ISourceDocInfo>();
                foreach (var currentSourceDoc in watercraft.CurrentSourceDocs)
                {
                    currentSourceDocs.Add(new SourceDocInfo(currentSourceDoc));
                }
                CurrentSourceDocs = currentSourceDocs;
            }
            PriorRecordCount = watercraft.PriorRecordCount;
            TotalPriorRecordCount = watercraft.TotalPriorRecordCount;
            if (watercraft.PriorWatercrafts != null)
            {
                var priorWatercrafts = new List<IWatercraftDetail>();
                foreach (var priorWatercraft in watercraft.PriorWatercrafts)
                {
                    priorWatercrafts.Add(new WatercraftDetail(priorWatercraft));
                }
                PriorWatercrafts = priorWatercrafts;
            }
            if (watercraft.PriorSourceDocs != null)
            {
                var priorSourceDocs = new List<ISourceDocInfo>();
                foreach (var priorSourceDoc in watercraft.PriorSourceDocs)
                {
                    priorSourceDocs.Add(new SourceDocInfo(priorSourceDoc));
                }
                PriorSourceDocs = priorSourceDocs;
            }
        }
        public int CurrentRecordCount { get; set; }
        public int TotalCurrentRecordCount { get; set; }
        public List<IWatercraftDetail> CurrentWatercrafts { get; set; }
        public List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        public int PriorRecordCount { get; set; }
        public int TotalPriorRecordCount { get; set; }
        public List<IWatercraftDetail> PriorWatercrafts { get; set; }
        public List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
