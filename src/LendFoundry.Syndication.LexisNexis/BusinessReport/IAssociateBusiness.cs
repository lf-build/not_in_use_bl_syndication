namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAssociateBusiness
	{
		 string CompanyName { get; set; }
		 IAddress Address { get; set; }
		 string Role { get; set; }
		 IBusinessIdentity BusinessIds { get; set; }
	}
}
