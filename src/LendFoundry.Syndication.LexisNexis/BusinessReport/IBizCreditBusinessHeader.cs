namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditBusinessHeader
	{
		 string BusinessId { get; set; }
		 IDate DateFirstSeen { get; set; }
		 IDate DateLastSeen { get; set; }
		 IDate ExtractDate { get; set; }
		 IDate LastUpdatedDate { get; set; }
		 IDate FileEstablishDate { get; set; }
		 IDate LastInquiryDate { get; set; }
		 string YearsInFile { get; set; }
		 string RecordType { get; set; }
		 string CompanyName { get; set; }
		 string DbaName { get; set; }
		 string BusinessDescription { get; set; }
		 IAddress OrigAddress { get; set; }
		 string StateName { get; set; }
		 string MsaDescription { get; set; }
		 IAddress Address { get; set; }
		 string PhoneNumber { get; set; }
		 string TimeZone { get; set; }
		 string SicCode { get; set; }
		 string DisputeIndicator { get; set; }
		 string RecentUpdateDescription { get; set; }
		 string LastActivityDescription { get; set; }
		 string Url { get; set; }
	}
}
