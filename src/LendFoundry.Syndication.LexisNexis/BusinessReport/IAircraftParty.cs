namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAircraftParty
	{
		 string SerialNumber { get; set; }
		 IDate RegistrationDate { get; set; }
		 string CompanyName { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
	}
}
