using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class DnbRecord : IDnbRecord
    {
        public DnbRecord()
        { }
        public DnbRecord(ServiceReference.TopBusinessDnbRecord dnbRecord)
        {
            if (dnbRecord == null)
                return;
            DateFirstSeen = new Date(dnbRecord.DateFirstSeen);
            DateLastSeen = new Date(dnbRecord.DateLastSeen);
            DunsNumber = dnbRecord.DunsNumber;
            BusinessName = dnbRecord.BusinessName;
            TradeStyle = dnbRecord.TradeStyle;
            OrigAddress = new Address(dnbRecord.OrigAddress);
            OrigMailingAddress = new Address(dnbRecord.OrigMailingAddress);
            Phone10 = dnbRecord.Phone10;
            MsaCode = dnbRecord.MsaCode;
            MsaName = dnbRecord.MsaName;
            LineOfBusinessDescription = dnbRecord.LineOfBusinessDescription;
            if (dnbRecord.SICs != null)
            {
                var sics = new List<IIndustrySic>();
                foreach (var sic in dnbRecord.SICs)
                {
                    sics.Add(new IndustrySic(sic));
                }
                Sics = sics;
            }
            IndustryGroup = dnbRecord.IndustryGroup;
            YearStarted = dnbRecord.YearStarted;
            DateOfIncorporation = new Date(dnbRecord.DateOfIncorporation);
            StateOfIncorporation = dnbRecord.StateOfIncorporation;
            AnnualSalesVolumeSign = dnbRecord.AnnualSalesVolumeSign;
            AnnualSalesVolume = dnbRecord.AnnualSalesVolume;
            AnnualSalesCode = dnbRecord.AnnualSalesCode;
            EmployeesHereSign = dnbRecord.EmployeesHereSign;
            EmployeesHere = dnbRecord.EmployeesHere;
            EmployeesTotalSign = dnbRecord.EmployeesTotalSign;
            EmployeesTotal = dnbRecord.EmployeesTotal;
            EmployeesHereCode = dnbRecord.EmployeesHereCode;
            AnnualSalesRevisionDate = new Date(dnbRecord.AnnualSalesRevisionDate);
            NetWorthSign = dnbRecord.NetWorthSign;
            NetWorth = dnbRecord.NetWorth;
            TrendSalesSign = dnbRecord.TrendSalesSign;
            TrendSales = dnbRecord.TrendSales;
            TrendEmploymentTotalSign = dnbRecord.TrendEmploymentTotalSign;
            TrendEmploymentTotal = dnbRecord.TrendEmploymentTotal;
            BaseSalesSign = dnbRecord.BaseSalesSign;
            BaseSales = dnbRecord.BaseSales;
            BaseEmploymentTotalSign = dnbRecord.BaseEmploymentTotalSign;
            BaseEmploymentTotal = dnbRecord.BaseEmploymentTotal;
            PercentageSalesGrowthSign = dnbRecord.PercentageSalesGrowthSign;
            PercentageSalesGrowth = dnbRecord.PercentageSalesGrowth;
            PercentageEmploymentGrowthSign = dnbRecord.PercentageEmploymentGrowthSign;
            PercentageEmploymentGrowth = dnbRecord.PercentageEmploymentGrowth;
            SquareFootage = dnbRecord.SquareFootage;
            SalesTerritory = dnbRecord.SalesTerritory;
            OwnsRents = dnbRecord.OwnsRents;
            NumberOfAccounts = dnbRecord.NumberOfAccounts;
            BankDunsNumber = dnbRecord.BankDunsNumber;
            BankName = dnbRecord.BankName;
            AccountingFirmName = dnbRecord.AccountingFirmName;
            SmallBusinessIndicator = dnbRecord.SmallBusinessIndicator;
            MinorityOwned = dnbRecord.MinorityOwned;
            CottageIndicator = dnbRecord.CottageIndicator;
            ForeignOwned = dnbRecord.ForeignOwned;
            ManufacturingHereIndicator = dnbRecord.ManufacturingHereIndicator;
            PublicIndicator = dnbRecord.PublicIndicator;
            ImporterExporterIndicator = dnbRecord.ImporterExporterIndicator;
            StructureType = dnbRecord.StructureType;
            TypeOfEstablishment = dnbRecord.TypeOfEstablishment;
            ParentDunsNumber = dnbRecord.ParentDunsNumber;
            UltimateDunsNumber = dnbRecord.UltimateDunsNumber;
            HeadquartersDunsNumber = dnbRecord.HeadquartersDunsNumber;
            ParentCompanyName = dnbRecord.ParentCompanyName;
            UltimateCompanyName = dnbRecord.UltimateCompanyName;
            DiasCode = dnbRecord.DiasCode;
            HierarchyCode = dnbRecord.HierarchyCode;
            UltimateIndicator = dnbRecord.UltimateIndicator;
            HotList = new DnbHotList(dnbRecord.HotList);
            ReportDate = new Date(dnbRecord.ReportDate);
            DeleteRecordIndicator = dnbRecord.DeleteRecordIndicator;
            MailingAddress = new Address(dnbRecord.MailingAddress);
            Address = new Address(dnbRecord.Address);
            RecordType = dnbRecord.RecordType;
            ActiveDunsNumber = dnbRecord.ActiveDunsNumber;
            StructureTypeDecoded = dnbRecord.StructureTypeDecoded;
            TypeOfEstablishmentDecoded = dnbRecord.TypeOfEstablishmentDecoded;
            OwnsRentsDecoded = dnbRecord.OwnsRentsDecoded;
        }

        public IDate DateFirstSeen { get; set; }
        public IDate DateLastSeen { get; set; }
        public string DunsNumber { get; set; }
        public string BusinessName { get; set; }
        public string TradeStyle { get; set; }
        public IAddress OrigAddress { get; set; }
        public IAddress OrigMailingAddress { get; set; }
        public string Phone10 { get; set; }
        public string MsaCode { get; set; }
        public string MsaName { get; set; }
        public string LineOfBusinessDescription { get; set; }
        public List<IIndustrySic> Sics { get; set; }
        public string IndustryGroup { get; set; }
        public string YearStarted { get; set; }
        public IDate DateOfIncorporation { get; set; }
        public string StateOfIncorporation { get; set; }
        public string AnnualSalesVolumeSign { get; set; }
        public string AnnualSalesVolume { get; set; }
        public string AnnualSalesCode { get; set; }
        public string EmployeesHereSign { get; set; }
        public string EmployeesHere { get; set; }
        public string EmployeesTotalSign { get; set; }
        public string EmployeesTotal { get; set; }
        public string EmployeesHereCode { get; set; }
        public IDate AnnualSalesRevisionDate { get; set; }
        public string NetWorthSign { get; set; }
        public string NetWorth { get; set; }
        public string TrendSalesSign { get; set; }
        public string TrendSales { get; set; }
        public string TrendEmploymentTotalSign { get; set; }
        public string TrendEmploymentTotal { get; set; }
        public string BaseSalesSign { get; set; }
        public string BaseSales { get; set; }
        public string BaseEmploymentTotalSign { get; set; }
        public string BaseEmploymentTotal { get; set; }
        public string PercentageSalesGrowthSign { get; set; }
        public string PercentageSalesGrowth { get; set; }
        public string PercentageEmploymentGrowthSign { get; set; }
        public string PercentageEmploymentGrowth { get; set; }
        public string SquareFootage { get; set; }
        public string SalesTerritory { get; set; }
        public string OwnsRents { get; set; }
        public string NumberOfAccounts { get; set; }
        public string BankDunsNumber { get; set; }
        public string BankName { get; set; }
        public string AccountingFirmName { get; set; }
        public string SmallBusinessIndicator { get; set; }
        public string MinorityOwned { get; set; }
        public string CottageIndicator { get; set; }
        public string ForeignOwned { get; set; }
        public string ManufacturingHereIndicator { get; set; }
        public string PublicIndicator { get; set; }
        public string ImporterExporterIndicator { get; set; }
        public string StructureType { get; set; }
        public string TypeOfEstablishment { get; set; }
        public string ParentDunsNumber { get; set; }
        public string UltimateDunsNumber { get; set; }
        public string HeadquartersDunsNumber { get; set; }
        public string ParentCompanyName { get; set; }
        public string UltimateCompanyName { get; set; }
        public string DiasCode { get; set; }
        public string HierarchyCode { get; set; }
        public string UltimateIndicator { get; set; }
        public IDnbHotList HotList { get; set; }
        public IDate ReportDate { get; set; }
        public string DeleteRecordIndicator { get; set; }
        public IAddress MailingAddress { get; set; }
        public IAddress Address { get; set; }
        public string RecordType { get; set; }
        public string ActiveDunsNumber { get; set; }
        public string StructureTypeDecoded { get; set; }
        public string TypeOfEstablishmentDecoded { get; set; }
        public string OwnsRentsDecoded { get; set; }
    }
}
