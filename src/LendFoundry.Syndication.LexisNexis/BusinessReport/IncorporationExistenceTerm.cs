namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IncorporationExistenceTerm : IIncorporationExistenceTerm
    {
        public IncorporationExistenceTerm()
        { }
        public IncorporationExistenceTerm(ServiceReference.TopBusinessIncorporationExistenceTerm incorporationExistenceTerm)
        {
            Type = incorporationExistenceTerm.Type;
            ExpirationDate = new Date(incorporationExistenceTerm.ExpirationDate);
            Years = incorporationExistenceTerm.Years;
        }
        public string Type { get; set; }
        public IDate ExpirationDate { get; set; }
        public uint Years { get; set; }
    }
}
