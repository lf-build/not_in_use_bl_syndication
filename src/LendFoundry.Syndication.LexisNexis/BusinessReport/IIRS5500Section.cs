using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIrs5500Section
    {
        uint Irs5500RecordCount { get; set; }
        List<IIrs5500Record> Irs5500S { get; set; }
    }
}
