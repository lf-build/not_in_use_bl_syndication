using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IOperationsSitesSection
    {
        int ReturnedRecordCount { get; set; }
        int TotalRecordCount { get; set; }
        List<IOperationSite> OperationsSites { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
