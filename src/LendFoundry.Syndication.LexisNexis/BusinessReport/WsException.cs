namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class WsException : IWsException
    {
        public WsException()
        { }
        public WsException(ServiceReference.WsException wsException)
        {
            if (wsException == null)
                return;
            Source = wsException.Source;
            Code = wsException.Code;
            Location = wsException.Location;
            Message = wsException.Message;
        }
        public string Source { get; set; }
        public int Code { get; set; }
        public string Location { get; set; }
        public string Message { get; set; }
    }
}
