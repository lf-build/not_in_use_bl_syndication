using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IRegisteredAgentSection
    {
        uint RegisteredAgentCount { get; set; }
        List<IRegisteredAgentEntity> RegisteredAgents { get; set; }
    }
}
