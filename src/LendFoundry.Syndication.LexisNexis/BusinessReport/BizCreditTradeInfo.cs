namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTradeInfo : IBizCreditTradeInfo
    {
        public BizCreditTradeInfo()
        { }
        public BizCreditTradeInfo(ServiceReference.BizCreditTradeInfo bizCreditTradeInfo)
        {
            if (bizCreditTradeInfo == null)
                return;
            HighestCredit = bizCreditTradeInfo.HighestCredit;
            MedianCredit = bizCreditTradeInfo.MedianCredit;
            NumberCombinedLines = bizCreditTradeInfo.NumberCombinedLines;
            CombinedDaysBeyondTermTotal = bizCreditTradeInfo.CombinedDaysBeyondTermTotal;
            CombinedBalance = bizCreditTradeInfo.CombinedBalance;
            NumberAgedLines = bizCreditTradeInfo.NumberAgedLines;
            TotalAccountBalance = bizCreditTradeInfo.TotalAccountBalance;
            CombinedAccountBalance = bizCreditTradeInfo.CombinedAccountBalance;
            CollectionCount = bizCreditTradeInfo.CollectionCount;
            NumberAgedTradesDelinquentCurrent = bizCreditTradeInfo.NumberAgedTradesDelinquentCurrent;
            NumberAgedTradesDelinquent1To30 = bizCreditTradeInfo.NumberAgedTradesDelinquent1_30;
            NumberAgedTradesDelinquent31To60 = bizCreditTradeInfo.NumberAgedTradesDelinquent31_60;
            NumberAgedTradesDelinquent61To90 = bizCreditTradeInfo.NumberAgedTradesDelinquent61_90;
            NumberAgedTradesDelinquent91Plus = bizCreditTradeInfo.NumberAgedTradesDelinquent91Plus;
            DaysBeyondTerm = new BizCreditDaysBeyondTerm(bizCreditTradeInfo.DaysBeyondTerm);
        }
        public string HighestCredit { get; set; }
        public string MedianCredit { get; set; }
        public string NumberCombinedLines { get; set; }
        public string CombinedDaysBeyondTermTotal { get; set; }
        public string CombinedBalance { get; set; }
        public string NumberAgedLines { get; set; }
        public string TotalAccountBalance { get; set; }
        public string CombinedAccountBalance { get; set; }
        public string CollectionCount { get; set; }
        public string NumberAgedTradesDelinquentCurrent { get; set; }
        public string NumberAgedTradesDelinquent1To30 { get; set; }
        public string NumberAgedTradesDelinquent31To60 { get; set; }
        public string NumberAgedTradesDelinquent61To90 { get; set; }
        public string NumberAgedTradesDelinquent91Plus { get; set; }
        public IBizCreditDaysBeyondTerm DaysBeyondTerm { get; set; }
    }
}
