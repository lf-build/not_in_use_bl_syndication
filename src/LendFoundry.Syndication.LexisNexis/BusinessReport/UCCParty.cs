namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UccParty : IUccParty
    {
        public UccParty()
        { }
        public UccParty(ServiceReference.TopBusinessUCCParty uccParty)
        {
            if (uccParty == null)
                return;
            OriginalName = uccParty.OriginalName;
            CompanyName = uccParty.CompanyName;
            TaxId = uccParty.TaxId;
            Ssn = uccParty.SSN;
            Name = new Name(uccParty.Name);
            Address = new Address(uccParty.Address);
        }
        public string OriginalName { get; set; }
        public string CompanyName { get; set; }
        public string TaxId { get; set; }
        public string Ssn { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
    }
}
