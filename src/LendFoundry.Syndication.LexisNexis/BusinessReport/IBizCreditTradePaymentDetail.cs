namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditTradePaymentDetail
	{
		 string BusinessCategory { get; set; }
		 IDate ReportedDate { get; set; }
		 IDate ActivityDate { get; set; }
		 string PaymentTerms { get; set; }
		 string HighCreditMask { get; set; }
		 int RecentHighCredit { get; set; }
		 string AccountBalanceMask { get; set; }
		 int MaskedAccountBalance { get; set; }
		 IDebtBeyondTermsPercent DbtPercentages { get; set; }
		 string Comments { get; set; }
		 string PaymentIndicator { get; set; }
		 string NewTradeFlag { get; set; }
		 string TradeTypeDesc { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
	}
}
