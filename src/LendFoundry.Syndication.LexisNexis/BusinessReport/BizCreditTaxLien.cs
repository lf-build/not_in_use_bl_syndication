namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTaxLien : IBizCreditTaxLien
    {
        public BizCreditTaxLien()
        { }
        public BizCreditTaxLien(ServiceReference.BizCreditTaxLien bizCreditTaxLien)
        {
            if (bizCreditTaxLien == null)
                return;
            Type = bizCreditTaxLien.Type;
            Action = bizCreditTaxLien.Action;
            DocumentNumber = bizCreditTaxLien.DocumentNumber;
            FilingLocation = bizCreditTaxLien.FilingLocation;
            LiabilityAmount = bizCreditTaxLien.LiabilityAmount;
            Description = bizCreditTaxLien.Description;
            DisputeIndicator = bizCreditTaxLien.DisputeIndicator;
            DisputeCode = bizCreditTaxLien.DisputeCode;
            DateFiled = new Date(bizCreditTaxLien.DateFiled);
        }
        public string Type { get; set; }
        public string Action { get; set; }
        public string DocumentNumber { get; set; }
        public string FilingLocation { get; set; }
        public int LiabilityAmount { get; set; }
        public string Description { get; set; }
        public string DisputeIndicator { get; set; }
        public string DisputeCode { get; set; }
        public IDate DateFiled { get; set; }
    }
}
