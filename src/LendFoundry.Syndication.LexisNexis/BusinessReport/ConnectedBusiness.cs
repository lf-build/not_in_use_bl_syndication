namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class ConnectedBusiness : IConnectedBusiness
	{
        public ConnectedBusiness()
        { }
        public ConnectedBusiness(ServiceReference.TopBusinessConnectedBusiness connectedBusiness)
        {
            if (connectedBusiness == null)
                return;
            CompanyName = connectedBusiness.CompanyName;
            Address = new Address(connectedBusiness.Address);
            BusinessIds = new BusinessIdentity(connectedBusiness.BusinessIds);
        }
        public string CompanyName { get; set; }
		public IAddress Address { get; set; }
		public IBusinessIdentity BusinessIds { get; set; }
	}
}
