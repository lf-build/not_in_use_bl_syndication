using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Irs5500Section : IIrs5500Section
    {
        public Irs5500Section()
        { }
        public Irs5500Section(ServiceReference.TopBusinessIRS5500Section iRs5500Section)
        {
            if (iRs5500Section == null)
                return;
            Irs5500RecordCount = iRs5500Section.IRS5500RecordCount;
            if (iRs5500Section.IRS5500s != null)
            {
                Irs5500S = new List<IIrs5500Record>(iRs5500Section.IRS5500s.Select(irs5500 => new Irs5500Record(irs5500)));
            }
        }
        public uint Irs5500RecordCount { get; set; }
        public List<IIrs5500Record> Irs5500S { get; set; }
    }
}
