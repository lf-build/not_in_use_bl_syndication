using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IConnectedBusinessSection
    {
        int CountConnectedBusinesses { get; set; }
        int TotalCountConnectedBusinesses { get; set; }
        List<IConnectedBusiness> ConnectedBusinessRecords { get; set; }
    }
}
