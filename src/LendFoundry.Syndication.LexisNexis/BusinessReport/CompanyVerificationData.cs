namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class CompanyVerificationData : ICompanyVerificationData
    {
        public CompanyVerificationData()
        { }
        public CompanyVerificationData(ServiceReference.CompanyVerificationData companyVerificationData)
        {
            if (companyVerificationData == null)
                return;
            CompanyName = companyVerificationData.CompanyName;
            Address = new Address(companyVerificationData.Address);
            Phone10 = companyVerificationData.Phone10;
            Fein = companyVerificationData.FEIN;
        }
        public string CompanyName { get; set; }
        public IAddress Address { get; set; }
        public string Phone10 { get; set; }
        public string Fein { get; set; }
    }
}
