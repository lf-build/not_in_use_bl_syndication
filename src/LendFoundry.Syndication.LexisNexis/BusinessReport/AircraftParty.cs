namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class AircraftParty : IAircraftParty
	{
	    public AircraftParty()
	    {
	        
	    }

        public AircraftParty(ServiceReference.TopBusinessAircraftParty aircraftParty)
        {
            if (aircraftParty == null)
                return;
            SerialNumber = aircraftParty.SerialNumber;
            RegistrationDate = new Date(aircraftParty.registrationDate);
            CompanyName = aircraftParty.CompanyName;
            Name = new Name(aircraftParty.Name);
            Address = new Address(aircraftParty.Address);
        }

        public string SerialNumber { get; set; }
		public IDate RegistrationDate { get; set; }
		public string CompanyName { get; set; }
		public IName Name { get; set; }
		public IAddress Address { get; set; }
	}
}
