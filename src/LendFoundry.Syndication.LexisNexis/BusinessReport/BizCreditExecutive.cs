namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditExecutive : IBizCreditExecutive
	{
        public BizCreditExecutive()
        { }
        public BizCreditExecutive(ServiceReference.BizCreditExecutive bizCreditExecutive)
        {
            if (bizCreditExecutive == null)
                return;
            Name = new Name(bizCreditExecutive.Name);
            Title = bizCreditExecutive.Title;
        }
        public IName Name { get; set; }
		public string Title { get; set; }
	}
}
