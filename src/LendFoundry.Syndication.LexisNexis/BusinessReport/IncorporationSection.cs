using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IncorporationSection : IIncorporationSection
    {
        public IncorporationSection()
        { }
        public IncorporationSection(ServiceReference.TopBusinessIncorporationSection incorporationSection)
        {
            if (incorporationSection == null)
                return;
            if (incorporationSection.CorpFilings != null)
            {
                CorpFilings = new List<IIncorporationInfo>(incorporationSection.CorpFilings.Select(corpFiling => new IncorporationInfo(corpFiling)));
            }
            ReturnedCorpFilings = incorporationSection.ReturnedCorpFilings;
            TotalCorpFilings = incorporationSection.TotalCorpFilings;
            if (incorporationSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(incorporationSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public List<IIncorporationInfo> CorpFilings { get; set; }
        public int ReturnedCorpFilings { get; set; }
        public int TotalCorpFilings { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
