namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IrsScheduleAttachment : IIrsScheduleAttachment
    {
        public IrsScheduleAttachment()
        { }
        public IrsScheduleAttachment(ServiceReference.TopBusinessIRSScheduleAttachment irsScheduleAttachment)
        {
            if (irsScheduleAttachment == null)
                return;
            SchRAttachedIndicator = irsScheduleAttachment.SchRAttachedIndicator;
            SchTAttachedIndicator = irsScheduleAttachment.SchTAttachedIndicator;
            SchTAttachedCount = irsScheduleAttachment.SchTAttachedCount;
            SchTPndgInfoPriorYearDate = irsScheduleAttachment.SchTPndgInfoPriorYearDate;
            SchBAttachedIndicator = irsScheduleAttachment.SchBAttachedIndicator;
            SchEAttachedIndicator = irsScheduleAttachment.SchEAttachedIndicator;
            SchSsaAttachedIndicator = irsScheduleAttachment.SchSsaAttachedIndicator;
            SchHAttachedIndicator = irsScheduleAttachment.SchHAttachedIndicator;
            SchIAttachedIndicator = irsScheduleAttachment.SchIAttachedIndicator;
            SchAAttachedIndicator = irsScheduleAttachment.SchAAttachedIndicator;
            SchAAttachedCount = irsScheduleAttachment.SchAAttachedCount;
            SchCAttachedIndicator = irsScheduleAttachment.SchCAttachedIndicator;
            SchDAttachedIndicator = irsScheduleAttachment.SchDAttachedIndicator;
            SchGAttachedIndicator = irsScheduleAttachment.SchGAttachedIndicator;
            SchPAttachedIndicator = irsScheduleAttachment.SchPAttachedIndicator;
            SchPAttachedCount = irsScheduleAttachment.SchPAttachedCount;
            SchFAttachedIndicator = irsScheduleAttachment.SchFAttachedIndicator;
        }
        public string SchRAttachedIndicator { get; set; }
        public string SchTAttachedIndicator { get; set; }
        public int SchTAttachedCount { get; set; }
        public string SchTPndgInfoPriorYearDate { get; set; }
        public string SchBAttachedIndicator { get; set; }
        public string SchEAttachedIndicator { get; set; }
        public string SchSsaAttachedIndicator { get; set; }
        public string SchHAttachedIndicator { get; set; }
        public string SchIAttachedIndicator { get; set; }
        public string SchAAttachedIndicator { get; set; }
        public int SchAAttachedCount { get; set; }
        public string SchCAttachedIndicator { get; set; }
        public string SchDAttachedIndicator { get; set; }
        public string SchGAttachedIndicator { get; set; }
        public string SchPAttachedIndicator { get; set; }
        public int SchPAttachedCount { get; set; }
        public string SchFAttachedIndicator { get; set; }
    }
}
