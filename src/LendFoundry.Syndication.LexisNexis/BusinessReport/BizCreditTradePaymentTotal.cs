namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTradePaymentTotal : IBizCreditTradePaymentTotal
    {
        public BizCreditTradePaymentTotal()
        { }
        public BizCreditTradePaymentTotal(ServiceReference.BizCreditTradePaymentTotal bizCreditTradePaymentTotal)
        {
            if (bizCreditTradePaymentTotal == null)
                return;
            RegularTrades = new BizCreditPayment(bizCreditTradePaymentTotal.RegularTrades);
            NewTrades = new BizCreditPayment(bizCreditTradePaymentTotal.NewTrades);
            CombinedTrades = new BizCreditPayment(bizCreditTradePaymentTotal.CombinedTrades);
            HighestCreditMedian = bizCreditTradePaymentTotal.HighestCreditMedian;
            AgedTradesCount = bizCreditTradePaymentTotal.AgedTradesCount;
            AccountBalanceRegular = bizCreditTradePaymentTotal.AccountBalanceRegular;
            AccountBalanceNew = bizCreditTradePaymentTotal.AccountBalanceNew;
            AccountBalanceCombined = bizCreditTradePaymentTotal.AccountBalanceCombined;
        }
        public IBizCreditPayment RegularTrades { get; set; }
        public IBizCreditPayment NewTrades { get; set; }
        public IBizCreditPayment CombinedTrades { get; set; }
        public int HighestCreditMedian { get; set; }
        public int AgedTradesCount { get; set; }
        public int AccountBalanceRegular { get; set; }
        public int AccountBalanceNew { get; set; }
        public int AccountBalanceCombined { get; set; }
    }
}
