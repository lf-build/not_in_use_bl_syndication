namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class GeoLocation : IGeoLocation
    {
        public GeoLocation()
        { }
        public GeoLocation(ServiceReference.GeoLocation geoLocation)
        {
            if (geoLocation == null)
                return;
            Latitude = geoLocation.Latitude;
            Longitude = geoLocation.Longitude;
        }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
