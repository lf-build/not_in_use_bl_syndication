namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCredSNP : IBizCredSnp
    {
        public BizCredSNP()
        { }
        public BizCredSNP(ServiceReference.BizCredSNP bizCredSnp)
        {
            if (bizCredSnp == null)
                return;
            DataPrintLine = bizCredSnp.DataPrintLine;
        }
        public string DataPrintLine { get; set; }
    }
}
