namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IJudgmentLienParty
	{
		 string CompanyName { get; set; }
		 string TaxId { get; set; }
		 string Ssn { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
	}
}
