namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IrsPreparer : IIrsPreparer
    {
        public IrsPreparer()
        { }
        public IrsPreparer(ServiceReference.TopBusinessIRSPreparer irsPreparer)
        {
            if (irsPreparer == null)
                return;
            Name = irsPreparer.Name;
            Address = new Address(irsPreparer.Address);
            Fein = irsPreparer.FEIN;
            Phone = irsPreparer.Phone;
        }
        public string Name { get; set; }
        public IAddress Address { get; set; }
        public string Fein { get; set; }
        public string Phone { get; set; }
    }
}
