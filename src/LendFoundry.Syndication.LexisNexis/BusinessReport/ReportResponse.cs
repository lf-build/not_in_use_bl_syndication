using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ReportResponse : IReportResponse
    {
        public ReportResponse()
        { }
        public ReportResponse(ServiceReference.TopBusinessReportResponse reportResponse)
        {
            if (reportResponse == null)
                return;
            Header = new ResponseHeader(reportResponse.Header);
            if (reportResponse.Businesses != null)
            {
                Businesses = new List<IReportRecord>(reportResponse.Businesses.Select(business => new ReportRecord(business)));
            }
        }
        public IResponseHeader Header { get; set; }
        public List<IReportRecord> Businesses { get; set; }
    }
}
