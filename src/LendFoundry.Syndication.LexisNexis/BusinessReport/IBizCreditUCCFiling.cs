using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditUccFiling
    {
        string Type { get; set; }
        string Action { get; set; }
        string DocumentNumber { get; set; }
        string FilingLocation { get; set; }
        List<ICodeMap> Collaterals { get; set; }
        string AdditionalCollateralCodes { get; set; }
        string SecuredParty { get; set; }
        string Assignee { get; set; }
        string FileState { get; set; }
        string FileNumber { get; set; }
        string DisputeIndicator { get; set; }
        string DisputeCode { get; set; }
        IDate DateFiled { get; set; }
    }
}
