namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditBankingDetail : IBizCreditBankingDetail
	{
        public BizCreditBankingDetail()
        { }
        public BizCreditBankingDetail(ServiceReference.BizCreditBankingDetail bizCreditBankingDetail)
        {
            if (bizCreditBankingDetail == null)
                return;
            Name = bizCreditBankingDetail.Name;
            OrigAddress = new Address(bizCreditBankingDetail.OrigAddress);
            StateName = bizCreditBankingDetail.StateName;
            Address = new Address(bizCreditBankingDetail.Address);
            PhoneNumber = bizCreditBankingDetail.PhoneNumber;
            TimeZone = bizCreditBankingDetail.TimeZone;
            Balance = bizCreditBankingDetail.Balance;
            Relationship = bizCreditBankingDetail.Relationship;
            DateOpened = new Date(bizCreditBankingDetail.DateOpened);
            DateClosed = new Date(bizCreditBankingDetail.DateClosed);
            DisputeIndicator = bizCreditBankingDetail.DisputeIndicator;
            DisputeCode = bizCreditBankingDetail.DisputeCode;
        }
		public string Name { get; set; }
		public IAddress OrigAddress { get; set; }
		public string StateName { get; set; }
		public IAddress Address { get; set; }
		public string PhoneNumber { get; set; }
		public string TimeZone { get; set; }
		public int Balance { get; set; }
		public string Relationship { get; set; }
		public IDate DateOpened { get; set; }
		public IDate DateClosed { get; set; }
		public string DisputeIndicator { get; set; }
		public string DisputeCode { get; set; }
	}
}
