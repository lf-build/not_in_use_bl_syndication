namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IrsSponsor : IIrsSponsor
    {
        public IrsSponsor()
        { }
        public IrsSponsor(ServiceReference.TopBusinessIRSSponsor irsSponsor)
        {
            if (irsSponsor == null)
                return;
            Address = new Address(irsSponsor.Address);
            Name = new Name(irsSponsor.Name);
            SponsSignNameScore = irsSponsor.SponsSignNameScore;
            FullName = irsSponsor.FullName;
            DoingBusinessAs = irsSponsor.DoingBusinessAs;
            Fein = irsSponsor.FEIN;
            Phone = irsSponsor.Phone;
            BusinessCode = irsSponsor.BusinessCode;
            LastReportedName = irsSponsor.LastReportedName;
            LastReportedFein = irsSponsor.LastReportedFEIN;
            LastReportedPlanNumber = irsSponsor.LastReportedPlanNumber;
            SignatureDate = new Date(irsSponsor.SignatureDate);
        }
        public IAddress Address { get; set; }
        public IName Name { get; set; }
        public uint SponsSignNameScore { get; set; }
        public string FullName { get; set; }
        public string DoingBusinessAs { get; set; }
        public string Fein { get; set; }
        public string Phone { get; set; }
        public string BusinessCode { get; set; }
        public string LastReportedName { get; set; }
        public string LastReportedFein { get; set; }
        public string LastReportedPlanNumber { get; set; }
        public IDate SignatureDate { get; set; }
    }
}
