namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IConnectedBusiness
	{
		 string CompanyName { get; set; }
		 IAddress Address { get; set; }
		 IBusinessIdentity BusinessIds { get; set; }
	}
}
