namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditInquiryCount : IBizCreditInquiryCount
	{
        public BizCreditInquiryCount()
        { }
        public BizCreditInquiryCount(ServiceReference.BizCreditInquiryCount bizCreditInquiryCount)
        {
            if (bizCreditInquiryCount == null)
                return;
            Date = new Date(bizCreditInquiryCount.Date);
            Count = bizCreditInquiryCount.Count;
        }
        public IDate Date { get; set; }
		public int Count { get; set; }
	}
}
