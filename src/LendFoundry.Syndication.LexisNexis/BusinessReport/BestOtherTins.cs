using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BestOtherTins : IBestOtherTins
    {
        public BestOtherTins()
        { }
        public BestOtherTins(ServiceReference.TopBusinessBestOtherTins bestOtherTins)
        {
            if (bestOtherTins == null)
                return;
            Tin = bestOtherTins.Tin;
            if (bestOtherTins.CompanyNames != null)
            {
                CompanyNames = new List<IBestCompanyNameInfo>(bestOtherTins.CompanyNames.Select(companyName => new BestCompanyNameInfo(companyName)));
            }
            if (bestOtherTins.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(bestOtherTins.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public string Tin { get; set; }
        public List<IBestCompanyNameInfo> CompanyNames { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
