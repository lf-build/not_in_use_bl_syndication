namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IAircraftSection
	{
		 int AircraftRecordCount { get; set; }
		 int TotalAircraftRecordCount { get; set; }
		 IAircraftSummary AircraftRecords { get; set; }
	}
}
