namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditPayment : IBizCreditPayment
    {
        public BizCreditPayment()
        { }
        public BizCreditPayment(ServiceReference.BizCreditPayment bizCreditPayment)
        {
            if (bizCreditPayment == null)
                return;
            TradeCount = bizCreditPayment.TradeCount;
            Debt = bizCreditPayment.Debt;
            HighCreditMask = bizCreditPayment.HighCreditMask;
            RecentHighCredit = bizCreditPayment.RecentHighCredit;
            AccountBalanceMask = bizCreditPayment.AccountBalanceMask;
            MaskedAccountBalance = bizCreditPayment.MaskedAccountBalance;
            DbtPercentages = new DebtBeyondTermsPercent(bizCreditPayment.DBTPercentages);
        }
        public int TradeCount { get; set; }
        public int Debt { get; set; }
        public string HighCreditMask { get; set; }
        public int RecentHighCredit { get; set; }
        public string AccountBalanceMask { get; set; }
        public int MaskedAccountBalance { get; set; }
        public IDebtBeyondTermsPercent DbtPercentages { get; set; }
    }
}
