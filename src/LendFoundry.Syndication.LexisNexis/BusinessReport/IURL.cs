namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IUrl
	{
		 string Url { get; set; }
	}
}
