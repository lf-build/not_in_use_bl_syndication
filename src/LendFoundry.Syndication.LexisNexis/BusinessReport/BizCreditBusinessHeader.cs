namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditBusinessHeader : IBizCreditBusinessHeader
    {
        public BizCreditBusinessHeader()
        { }
        public BizCreditBusinessHeader(ServiceReference.BizCreditBusinessHeader bizCreditBusinessHeader)
        {
            if (bizCreditBusinessHeader == null)
                return;
            BusinessId = bizCreditBusinessHeader.BusinessId;
            DateFirstSeen = new Date(bizCreditBusinessHeader.DateFirstSeen);
            DateLastSeen = new Date(bizCreditBusinessHeader.DateLastSeen);
            ExtractDate = new Date(bizCreditBusinessHeader.ExtractDate);
            LastUpdatedDate = new Date(bizCreditBusinessHeader.LastUpdatedDate);
            FileEstablishDate = new Date(bizCreditBusinessHeader.FileEstablishDate);
            LastInquiryDate = new Date(bizCreditBusinessHeader.LastInquiryDate);
            YearsInFile = bizCreditBusinessHeader.YearsInFile;
            RecordType = bizCreditBusinessHeader.RecordType;
            CompanyName = bizCreditBusinessHeader.CompanyName;
            DbaName = bizCreditBusinessHeader.DBAName;
            BusinessDescription = bizCreditBusinessHeader.BusinessDescription;
            OrigAddress = new Address(bizCreditBusinessHeader.OrigAddress);
            StateName = bizCreditBusinessHeader.StateName;
            MsaDescription = bizCreditBusinessHeader.MSADescription;
            Address = new Address(bizCreditBusinessHeader.Address);
            PhoneNumber = bizCreditBusinessHeader.PhoneNumber;
            TimeZone = bizCreditBusinessHeader.TimeZone;
            SicCode = bizCreditBusinessHeader.SICCode;
            DisputeIndicator = bizCreditBusinessHeader.DisputeIndicator;
            RecentUpdateDescription = bizCreditBusinessHeader.RecentUpdateDescription;
            LastActivityDescription = bizCreditBusinessHeader.LastActivityDescription;
            Url = bizCreditBusinessHeader.URL;
        }
        public string BusinessId { get; set; }
        public IDate DateFirstSeen { get; set; }
        public IDate DateLastSeen { get; set; }
        public IDate ExtractDate { get; set; }
        public IDate LastUpdatedDate { get; set; }
        public IDate FileEstablishDate { get; set; }
        public IDate LastInquiryDate { get; set; }
        public string YearsInFile { get; set; }
        public string RecordType { get; set; }
        public string CompanyName { get; set; }
        public string DbaName { get; set; }
        public string BusinessDescription { get; set; }
        public IAddress OrigAddress { get; set; }
        public string StateName { get; set; }
        public string MsaDescription { get; set; }
        public IAddress Address { get; set; }
        public string PhoneNumber { get; set; }
        public string TimeZone { get; set; }
        public string SicCode { get; set; }
        public string DisputeIndicator { get; set; }
        public string RecentUpdateDescription { get; set; }
        public string LastActivityDescription { get; set; }
        public string Url { get; set; }
    }
}
