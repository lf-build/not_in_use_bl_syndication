namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditGovernmentDebarredContractor
	{
		 string Action { get; set; }
		 string BusinessName { get; set; }
		 IAddress OrigAddress { get; set; }
		 string StateName { get; set; }
		 IAddress Address { get; set; }
		 string ExtentOfAction { get; set; }
		 string Agency { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
		 IDate DateFiled { get; set; }
		 IDate DateReported { get; set; }
	}
}
