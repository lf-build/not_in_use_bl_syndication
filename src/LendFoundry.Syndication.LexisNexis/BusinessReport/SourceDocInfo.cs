namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SourceDocInfo : ISourceDocInfo
    {
        public SourceDocInfo()
        { }
        public SourceDocInfo(ServiceReference.TopBusinessSourceDocInfo sourceDocInfo)
        {
            if (sourceDocInfo == null)
                return;
            BusinessIds = new BusinessIdentity(sourceDocInfo.BusinessIds);
            IdType = sourceDocInfo.IdType;
            IdValue = sourceDocInfo.IdValue;
            Section = sourceDocInfo.Section;
            Source = sourceDocInfo.Source;
            Address = new Address(sourceDocInfo.Address);
            Name = new NameAndCompany(sourceDocInfo.Name);
        }
        public IBusinessIdentity BusinessIds { get; set; }
        public string IdType { get; set; }
        public string IdValue { get; set; }
        public string Section { get; set; }
        public string Source { get; set; }
        public IAddress Address { get; set; }
        public INameAndCompany Name { get; set; }
    }
}
