namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIrsScheduleAttachment
	{
		 string SchRAttachedIndicator { get; set; }
		 string SchTAttachedIndicator { get; set; }
		 int SchTAttachedCount { get; set; }
		 string SchTPndgInfoPriorYearDate { get; set; }
		 string SchBAttachedIndicator { get; set; }
		 string SchEAttachedIndicator { get; set; }
		 string SchSsaAttachedIndicator { get; set; }
		 string SchHAttachedIndicator { get; set; }
		 string SchIAttachedIndicator { get; set; }
		 string SchAAttachedIndicator { get; set; }
		 int SchAAttachedCount { get; set; }
		 string SchCAttachedIndicator { get; set; }
		 string SchDAttachedIndicator { get; set; }
		 string SchGAttachedIndicator { get; set; }
		 string SchPAttachedIndicator { get; set; }
		 int SchPAttachedCount { get; set; }
		 string SchFAttachedIndicator { get; set; }
	}
}
