namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IrsAdmin : IIrsAdmin
    {
        public IrsAdmin()
        { }
        public IrsAdmin(ServiceReference.TopBusinessIRSAdmin irsAdmin)
        {
            if (irsAdmin == null)
                return;
            Name = irsAdmin.Name;
            CareOfName = irsAdmin.CareOfName;
            Address = new Address(irsAdmin.Address);
            Fein = irsAdmin.FEIN;
            Phone = irsAdmin.Phone;
            SignatureDate = new Date(irsAdmin.SignatureDate);
            Signature = irsAdmin.Signature;
        }
        public string Name { get; set; }
        public string CareOfName { get; set; }
        public IAddress Address { get; set; }
        public string Fein { get; set; }
        public string Phone { get; set; }
        public IDate SignatureDate { get; set; }
        public string Signature { get; set; }
    }
}
