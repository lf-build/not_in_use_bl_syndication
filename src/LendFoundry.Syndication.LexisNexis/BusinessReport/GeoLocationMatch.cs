namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class GeoLocationMatch : IGeoLocationMatch
    {
        public GeoLocationMatch()
        { }
        public GeoLocationMatch(ServiceReference.GeoLocationMatch geoLocationMatch)
        {
            if (geoLocationMatch == null)
                return;
            Latitude = geoLocationMatch.Latitude;
            Longitude = geoLocationMatch.Longitude;
            MatchCode = geoLocationMatch.MatchCode;
            MatchDesc = geoLocationMatch.MatchDesc;
        }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MatchCode { get; set; }
        public string MatchDesc { get; set; }
    }
}
