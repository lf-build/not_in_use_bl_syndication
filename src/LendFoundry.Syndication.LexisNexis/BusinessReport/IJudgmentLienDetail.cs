using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IJudgmentLienDetail
    {
        List<IJudgmentLienParty> Debtors { get; set; }
        List<IJudgmentLienParty> Creditors { get; set; }
        List<IJudgmentLienFilings> Filings { get; set; }
        string FilingJurisdiction { get; set; }
        string Amount { get; set; }
        string OrigFilingNumber { get; set; }
        string OrigFilingType { get; set; }
        IDate OrigFilingDate { get; set; }
        string Eviction { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
