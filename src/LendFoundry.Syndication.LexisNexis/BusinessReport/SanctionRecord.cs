namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SanctionRecord : ISanctionRecord
    {
        public SanctionRecord()
        { }
        public SanctionRecord(ServiceReference.TopBusinessSanctionRecord sanctionRecord)
        {
            if (sanctionRecord == null)
                return;
            SanctionId = sanctionRecord.SanctionId;
            UniqueId = sanctionRecord.UniqueId;
            ProviderDetail = new SanctionProvider(sanctionRecord.ProviderDetail);
            FilingDetail = new SanctionFiling(sanctionRecord.FilingDetail);
        }
        public string SanctionId { get; set; }
        public string UniqueId { get; set; }
        public ISanctionProvider ProviderDetail { get; set; }
        public ISanctionFiling FilingDetail { get; set; }
    }
}
