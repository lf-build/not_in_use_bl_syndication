namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface ICodeMap
	{
		 string Code { get; set; }
		 string Description { get; set; }
	}
}
