using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Individual : IIndividual
    {
        public Individual()
        { }
        public Individual(ServiceReference.TopBusinessIndividual individual)
        {
            if (individual == null)
                return;
            Name = new Name(individual.Name);
            Address = new Address(individual.Address);
            UniqueId = individual.UniqueId;
            Ssn = individual.SSN;
            IsDeceased = individual.IsDeceased;
            HasDerog = individual.HasDerog;
            ExecutiveElsewhere = individual.ExecutiveElsewhere;
            BestPosition = new Position(individual.BestPosition);
            if (individual.OtherCurrents != null)
            {
                OtherCurrents = individual.OtherCurrents.Select(otherCurrent => new Position(otherCurrent)).Cast<IPosition>().ToList();
            }
            if (individual.AllOthers != null)
            {
                AllOthers = individual.AllOthers.Select(allOther => new Position(allOther)).Cast<IPosition>().ToList();
            }
            if (individual.SourceDocs != null)
            {
                SourceDocs = individual.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)).Cast<ISourceDocInfo>().ToList();
            }
        }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public string UniqueId { get; set; }
        public string Ssn { get; set; }
        public bool IsDeceased { get; set; }
        public bool HasDerog { get; set; }
        public long ExecutiveElsewhere { get; set; }
        public IPosition BestPosition { get; set; }
        public List<IPosition> OtherCurrents { get; set; }
        public List<IPosition> AllOthers { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
