using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ParentSection : IParentSection
    {
        public ParentSection()
        { }
        public ParentSection(ServiceReference.TopBusinessParentSection parentSection)
        {
            if (parentSection == null)
                return;
            if (parentSection.Parents != null)
            {
                Parents = new List<IRelativeRec>(parentSection.Parents.Select(parent => new RelativeRec(parent)));
            }
            if (parentSection.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(parentSection.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public List<IRelativeRec> Parents { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
