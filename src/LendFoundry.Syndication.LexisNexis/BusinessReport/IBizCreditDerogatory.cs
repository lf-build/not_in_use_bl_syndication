namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditDerogatory
	{
		 string Indicator { get; set; }
		 IDate FiledDate { get; set; }
		 string LiabilityAmount { get; set; }
		 int TotalNumberDerogatoryItems { get; set; }
		 int TotalNumberLegalItems { get; set; }
		 string TotalLegalBalanceAmount { get; set; }
	}
}
