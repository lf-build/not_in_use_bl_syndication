namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class SanctionProvider : ISanctionProvider
    {
        public SanctionProvider()
        { }
        public SanctionProvider(ServiceReference.TopBusinessSanctionProvider sanctionProvider)
        {
            if (sanctionProvider == null)
                return;
            Name = new Name(sanctionProvider.Name);
            Address = new GeoAddress(sanctionProvider.Address);
        }
        public IName Name { get; set; }
        public IGeoAddress Address { get; set; }
    }
}
