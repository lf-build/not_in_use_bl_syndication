using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBankruptcy
    {
        string CourtCode { get; set; }
        string CourtLocation { get; set; }
        string Ssn { get; set; }
        string TaxId { get; set; }
        string UniqueId { get; set; }
        string Comment { get; set; }
        string FilingStatus { get; set; }
        string OriginalFilingType { get; set; }
        string StatusHistory { get; set; }
        string OriginalCaseNumber { get; set; }
        string Status { get; set; }
        string StatusRaw { get; set; }
        string FilingType { get; set; }
        string FilerType { get; set; }
        IDate StatusDate { get; set; }
        IDate OriginalFilingDate { get; set; }
        List<IBankruptcyParty> Debtors { get; set; }
        List<IBankruptcyParty> Attorneys { get; set; }
        List<IBankruptcyParty> Trustees { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
