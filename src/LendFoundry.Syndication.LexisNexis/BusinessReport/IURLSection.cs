using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IUrlSection
    {
        int UrlRecordCount { get; set; }
        List<IUrl> Urls { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
