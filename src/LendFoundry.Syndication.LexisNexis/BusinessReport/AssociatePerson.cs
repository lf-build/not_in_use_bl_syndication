namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class AssociatePerson : IAssociatePerson
    {
        public AssociatePerson()
        {
            
        }

        public AssociatePerson(ServiceReference.TopBusinessAssociatePerson associatePerson)
        {
            if (associatePerson == null)
                return;
            Name = new Name(associatePerson.Name);
            IsDeceased = associatePerson.IsDeceased;
            HasDerog = associatePerson.HasDerog;
            Address = new Address(associatePerson.Address);
            ToDate = new Date(associatePerson.ToDate);
            FromDate = new Date(associatePerson.FromDate);
            Role = associatePerson.Role;
            UniqueId = associatePerson.UniqueId;
            Ssn = associatePerson.SSN;
        }

        public IName Name { get; set; }
        public bool IsDeceased { get; set; }
        public bool HasDerog { get; set; }
        public IAddress Address { get; set; }
        public IDate ToDate { get; set; }
        public IDate FromDate { get; set; }
        public string Role { get; set; }
        public string UniqueId { get; set; }
        public string Ssn { get; set; }
    }
}
