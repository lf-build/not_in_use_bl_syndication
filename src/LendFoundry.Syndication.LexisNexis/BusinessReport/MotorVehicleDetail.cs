using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class MotorVehicleDetail : IMotorVehicleDetail
    {
        public MotorVehicleDetail()
        { }
        public MotorVehicleDetail(ServiceReference.TopBusinessMotorVehicleDetail motorVehicleDetail)
        {
            if (motorVehicleDetail == null)
                return;
            if (motorVehicleDetail.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(motorVehicleDetail.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }

            Vin = motorVehicleDetail.VIN;
            VehicleType = motorVehicleDetail.VehicleType;
            ModelYear = motorVehicleDetail.ModelYear;
            Make = motorVehicleDetail.Make;
            Series = motorVehicleDetail.Series;
            Style = motorVehicleDetail.Style;
            BasePrice = motorVehicleDetail.BasePrice;
            StateOfOrigin = motorVehicleDetail.StateOfOrigin;
            Color = motorVehicleDetail.Color;
            VehicleUse = motorVehicleDetail.VehicleUse;
            NumberOfCylinders = motorVehicleDetail.NumberOfCylinders;
            EngineSize = motorVehicleDetail.EngineSize;
            FuelTypeName = motorVehicleDetail.FuelTypeName;
            Restraints = motorVehicleDetail.Restraints;
            AntiLockBrakes = motorVehicleDetail.AntiLockBrakes;
            AirConditioning = motorVehicleDetail.AirConditioning;
            DaytimeRunningLights = motorVehicleDetail.DaytimeRunningLights;
            PowerSteering = motorVehicleDetail.PowerSteering;
            PowerBrakes = motorVehicleDetail.PowerBrakes;
            PowerWindows = motorVehicleDetail.PowerWindows;
            SecuritySystem = motorVehicleDetail.SecuritySystem;
            Roof = motorVehicleDetail.Roof;
            Radio = motorVehicleDetail.Radio;
            FrontWheelDrive = motorVehicleDetail.FrontWheelDrive;
            FourWheelDrive = motorVehicleDetail.FourWheelDrive;
            TiltWheel = motorVehicleDetail.TiltWheel;
            NonDmvSource = motorVehicleDetail.NonDMVSource;


            if (motorVehicleDetail.Parties != null)
            {
                Parties = new List<IMotorVehicleParty>(motorVehicleDetail.Parties.Select(party => new MotorVehicleParty(party)));
            }
        }
        public List<ISourceDocInfo> SourceDocs { get; set; }
        public string Vin { get; set; }
        public string VehicleType { get; set; }
        public int ModelYear { get; set; }
        public string Make { get; set; }
        public string Series { get; set; }
        public string Style { get; set; }
        public string BasePrice { get; set; }
        public string StateOfOrigin { get; set; }
        public string Color { get; set; }
        public string VehicleUse { get; set; }
        public string NumberOfCylinders { get; set; }
        public string EngineSize { get; set; }
        public string FuelTypeName { get; set; }
        public string Restraints { get; set; }
        public string AntiLockBrakes { get; set; }
        public string AirConditioning { get; set; }
        public string DaytimeRunningLights { get; set; }
        public string PowerSteering { get; set; }
        public string PowerBrakes { get; set; }
        public string PowerWindows { get; set; }
        public string SecuritySystem { get; set; }
        public string Roof { get; set; }
        public string Radio { get; set; }
        public string FrontWheelDrive { get; set; }
        public string FourWheelDrive { get; set; }
        public string TiltWheel { get; set; }
        public bool NonDmvSource { get; set; }
        public List<IMotorVehicleParty> Parties { get; set; }
    }
}
