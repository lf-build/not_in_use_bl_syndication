namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class UccSection : IUccSection
    {
        public UccSection()
        { }
        public UccSection(ServiceReference.TopBusinessUCCSection uccSection)
        {
            if (uccSection == null)
                return;
            DerogSummaryCntUcc = uccSection.DerogSummaryCntUCC;
            SecuredAssetsCntUcc = uccSection.SecuredAssetsCntUCC;
            ReturnedAsDebtorCount = uccSection.ReturnedAsDebtorCount;
            TotalAsDebtorCount = uccSection.TotalAsDebtorCount;
            ReturnedAsSecuredCount = uccSection.ReturnedAsSecuredCount;
            TotalAsSecuredCount = uccSection.TotalAsSecuredCount;
            AsDebtor = new UccRole(uccSection.AsDebtor);
            AsSecured = new UccRole(uccSection.AsSecured);
        }
        public int DerogSummaryCntUcc { get; set; }
        public int SecuredAssetsCntUcc { get; set; }
        public int ReturnedAsDebtorCount { get; set; }
        public int TotalAsDebtorCount { get; set; }
        public int ReturnedAsSecuredCount { get; set; }
        public int TotalAsSecuredCount { get; set; }
        public IUccRole AsDebtor { get; set; }
        public IUccRole AsSecured { get; set; }
    }
}
