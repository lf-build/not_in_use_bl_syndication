namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class DnbHotList : IDnbHotList
    {
        public DnbHotList()
        { }
        public DnbHotList(ServiceReference.TopBusinessDnbHotList dnbHotList)
        {
            if (dnbHotList == null)
                return;
            NewIndicator = dnbHotList.NewIndicator;
            OwnershipChangeIndicator = dnbHotList.OwnershipChangeIndicator;
            CeoChangeIndicator = dnbHotList.CeoChangeIndicator;
            CompanyNameChangeInd = dnbHotList.CompanyNameChangeInd;
            AddressChangeIndicator = dnbHotList.AddressChangeIndicator;
            TelephoneChangeIndicator = dnbHotList.TelephoneChangeIndicator;
            NewChangeDate = new Date(dnbHotList.NewChangeDate);
            OwnershipChangeDate = new Date(dnbHotList.OwnershipChangeDate);
            CeoChangeDate = new Date(dnbHotList.CeoChangeDate);
            CompanyNameChgDate = new Date(dnbHotList.CompanyNameChgDate);
            AddressChangeDate = new Date(dnbHotList.AddressChangeDate);
            TelephoneChangeDate = new Date(dnbHotList.TelephoneChangeDate);
        }
        public string NewIndicator { get; set; }
        public string OwnershipChangeIndicator { get; set; }
        public string CeoChangeIndicator { get; set; }
        public string CompanyNameChangeInd { get; set; }
        public string AddressChangeIndicator { get; set; }
        public string TelephoneChangeIndicator { get; set; }
        public IDate NewChangeDate { get; set; }
        public IDate OwnershipChangeDate { get; set; }
        public IDate CeoChangeDate { get; set; }
        public IDate CompanyNameChgDate { get; set; }
        public IDate AddressChangeDate { get; set; }
        public IDate TelephoneChangeDate { get; set; }
    }
}
