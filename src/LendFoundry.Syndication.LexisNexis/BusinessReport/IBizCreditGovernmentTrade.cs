namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditGovernmentTrade
	{
		 string PaymentIndicator { get; set; }
		 string BusinessCategory { get; set; }
		 string PaymentTerms { get; set; }
		 string HighCreditMask { get; set; }
		 int RecentHighCredit { get; set; }
		 string AccountBalanceMask { get; set; }
		 int MaskedAccountBalance { get; set; }
		 IDebtBeyondTermsPercent DbtPercentages { get; set; }
		 string Comments { get; set; }
		 string NewTradeFlag { get; set; }
		 string TradeType { get; set; }
		 IDate DateReported { get; set; }
		 IDate DateLastSale { get; set; }
		 string DisputeIndicator { get; set; }
		 string DisputeCode { get; set; }
	}
}
