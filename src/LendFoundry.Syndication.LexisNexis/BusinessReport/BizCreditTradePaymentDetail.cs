namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditTradePaymentDetail : IBizCreditTradePaymentDetail
    {
        public BizCreditTradePaymentDetail()
        { }
        public BizCreditTradePaymentDetail(ServiceReference.BizCreditTradePaymentDetail bizCreditTradePaymentDetail)
        {
            if (bizCreditTradePaymentDetail == null)
                return;
            BusinessCategory = bizCreditTradePaymentDetail.BusinessCategory;
            ReportedDate = new Date(bizCreditTradePaymentDetail.ReportedDate);
            ActivityDate = new Date(bizCreditTradePaymentDetail.ActivityDate);
            PaymentTerms = bizCreditTradePaymentDetail.PaymentTerms;
            HighCreditMask = bizCreditTradePaymentDetail.HighCreditMask;
            RecentHighCredit = bizCreditTradePaymentDetail.RecentHighCredit;
            AccountBalanceMask = bizCreditTradePaymentDetail.AccountBalanceMask;
            MaskedAccountBalance = bizCreditTradePaymentDetail.MaskedAccountBalance;
            DbtPercentages = new DebtBeyondTermsPercent(bizCreditTradePaymentDetail.DBTPercentages);
            Comments = bizCreditTradePaymentDetail.Comments;
            PaymentIndicator = bizCreditTradePaymentDetail.PaymentIndicator;
            NewTradeFlag = bizCreditTradePaymentDetail.NewTradeFlag;
            TradeTypeDesc = bizCreditTradePaymentDetail.TradeTypeDesc;
            DisputeIndicator = bizCreditTradePaymentDetail.DisputeIndicator;
            DisputeCode = bizCreditTradePaymentDetail.DisputeCode;
        }
        public string BusinessCategory { get; set; }
        public IDate ReportedDate { get; set; }
        public IDate ActivityDate { get; set; }
        public string PaymentTerms { get; set; }
        public string HighCreditMask { get; set; }
        public int RecentHighCredit { get; set; }
        public string AccountBalanceMask { get; set; }
        public int MaskedAccountBalance { get; set; }
        public IDebtBeyondTermsPercent DbtPercentages { get; set; }
        public string Comments { get; set; }
        public string PaymentIndicator { get; set; }
        public string NewTradeFlag { get; set; }
        public string TradeTypeDesc { get; set; }
        public string DisputeIndicator { get; set; }
        public string DisputeCode { get; set; }
    }
}
