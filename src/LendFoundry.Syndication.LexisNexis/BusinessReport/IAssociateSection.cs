using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IAssociateSection
    {
        int ReturnBusinessCount { get; set; }
        int TotalBusinessCount { get; set; }
        List<IAssociateBusiness> BusinessAssociates { get; set; }
        int ReturnPersonCount { get; set; }
        int TotalPersonCount { get; set; }
        List<IAssociatePerson> PersonAssociates { get; set; }
    }
}
