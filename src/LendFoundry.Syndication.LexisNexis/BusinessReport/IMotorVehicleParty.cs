namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IMotorVehicleParty
	{
		 string PartyTypeDescription { get; set; }
		 string CompanyName { get; set; }
		 IName Name { get; set; }
		 IAddress Address { get; set; }
		 string TitleNumber { get; set; }
		 IDate TitleDate { get; set; }
		 IDate OriginalRegistrationDate { get; set; }
		 IDate RegistrationDate { get; set; }
		 IDate RegistrationExpirationDate { get; set; }
		 string LicensePlate { get; set; }
		 string LicensePlateState { get; set; }
		 string LicensePlateType { get; set; }
		 string PreviousLicensePlate { get; set; }
		 string TitleStatus { get; set; }
		 string OdometerMileage { get; set; }
		 string DecalYear { get; set; }
		 IDate SourceDateFirstSeen { get; set; }
		 IDate SourceDateLastSeen { get; set; }
	}
}
