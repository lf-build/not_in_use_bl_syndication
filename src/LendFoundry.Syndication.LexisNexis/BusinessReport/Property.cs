using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Property : IProperty
    {
        public Property()
        { }
        public Property(ServiceReference.TopBusinessProperty property)
        {
            if (property == null)
                return;
            PropertyUniqueId = property.PropertyUniqueID;
            SourceObscure = property.SourceObscure;
            PropertyAddress = new Address(property.PropertyAddress);
            IsNoticeOfDefault = property.IsNoticeOfDefault;
            IsForeclosed = property.IsForeclosed;
            Apn = property.APN;
            AddressType = property.AddressType;
            PurchasePrice = property.PurchasePrice;
            SalesPrice = property.SalesPrice;
            DocumentType = property.DocumentType;
            AssessedValue = property.AssessedValue;
            MarketLandValue = property.MarketLandValue;
            TotalMarketValue = property.TotalMarketValue;
            AssessmentDate = new Date(property.AssessmentDate);
            ContractDate = new Date(property.ContractDate);
            SaleDate = new Date(property.SaleDate);
            RecordingDate = new Date(property.RecordingDate);
            CurrentRecord = property.CurrentRecord;
            if (property.PSourceDocs != null)
            {
                PSourceDocs = new List<ISourceDocInfo>(property.PSourceDocs.Select(pSourceDoc => new SourceDocInfo(pSourceDoc)));
            }
            if (property.FSourceDocs != null)
            {
                FSourceDocs = new List<ISourceDocInfo>(property.FSourceDocs.Select(fSourceDoc => new SourceDocInfo(fSourceDoc)));
            }
            if (property.Parties != null)
            {
                Parties = new List<IPropertyParty>(property.Parties.Select(party => new PropertyParty(party)));
            }
            FIdType = property.FIDType;
            FIdTypeDesc = property.FIDTypeDesc;
            SortByDate = property.SortByDate;
            VendorSourceGlag = property.VendorSourceGlag;
            VendorSourceDesc = property.VendorSourceDesc;
            AccurintCurrentRecord = property.AccurintCurrentRecord;
            Deeds = new Deeds(property.Deeds);
            Accessor = new Accessor(property.Accessor);
            FaresLivingSquareFeet = property.FaresLivingSquareFeet;
            PartyType = property.PartyType;
            PartyTypeName = property.PartyTypeName;
            Address = new GeoAddressMatch(property.Address);
            Msa = property.MSA;
            GeoBlk = property.GeoBlk;
            GeoMatch = property.GeoMatch;
            Phone10 = property.Phone10;
        }
        public string PropertyUniqueId { get; set; }
        public string SourceObscure { get; set; }
        public IAddress PropertyAddress { get; set; }
        public bool IsNoticeOfDefault { get; set; }
        public bool IsForeclosed { get; set; }
        public string Apn { get; set; }
        public string AddressType { get; set; }
        public string PurchasePrice { get; set; }
        public string SalesPrice { get; set; }
        public string DocumentType { get; set; }
        public string AssessedValue { get; set; }
        public string MarketLandValue { get; set; }
        public string TotalMarketValue { get; set; }
        public IDate AssessmentDate { get; set; }
        public IDate ContractDate { get; set; }
        public IDate SaleDate { get; set; }
        public IDate RecordingDate { get; set; }
        public string CurrentRecord { get; set; }
        public List<ISourceDocInfo> PSourceDocs { get; set; }
        public List<ISourceDocInfo> FSourceDocs { get; set; }
        public List<IPropertyParty> Parties { get; set; }
        public string FIdType { get; set; }
        public string FIdTypeDesc { get; set; }
        public string SortByDate { get; set; }
        public string VendorSourceGlag { get; set; }
        public string VendorSourceDesc { get; set; }
        public string AccurintCurrentRecord { get; set; }
        public IDeeds Deeds { get; set; }
        public IAccessor Accessor { get; set; }
        public string FaresLivingSquareFeet { get; set; }
        public string PartyType { get; set; }
        public string PartyTypeName { get; set; }
        public IGeoAddressMatch Address { get; set; }
        public string Msa { get; set; }
        public string GeoBlk { get; set; }
        public string GeoMatch { get; set; }
        public string Phone10 { get; set; }
    }
}
