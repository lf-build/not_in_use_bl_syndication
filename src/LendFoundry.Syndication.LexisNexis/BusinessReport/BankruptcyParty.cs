namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BankruptcyParty : IBankruptcyParty
    {
        public BankruptcyParty()
        { }
        public BankruptcyParty(ServiceReference.TopBusinessBankruptcyParty bankruptcyParty)
        {
            if (bankruptcyParty == null)
                return;
            CompanyName = bankruptcyParty.CompanyName;
            Name = new Name(bankruptcyParty.Name);
            Address = new Address(bankruptcyParty.Address);
            TaxId = bankruptcyParty.TaxID;
            Ssn = bankruptcyParty.SSN;
        }
        public string CompanyName { get; set; }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public string TaxId { get; set; }
        public string Ssn { get; set; }
    }
}
