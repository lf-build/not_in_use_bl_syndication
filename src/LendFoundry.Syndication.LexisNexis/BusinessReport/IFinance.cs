using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IFinance
    {
        List<ISourceDocInfo> SourceDocs { get; set; }
        string AnnualSales { get; set; }
        IDate SalesDate { get; set; }
    }
}
