namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IMotorVehicleSection
	{
		 int MotorVehicleRecordCount { get; set; }
		 int TotalMotorVehicleRecordCount { get; set; }
		 IMotorVehicleSummary MotorVehicleRecords { get; set; }
	}
}
