namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditLienTotal : IBizCreditLienTotal
    {
        public BizCreditLienTotal()
        { }
        public BizCreditLienTotal(ServiceReference.BizCreditLienTotal bizCreditLienTotal)
        {
            if (bizCreditLienTotal == null)
                return;
            Filed = bizCreditLienTotal.Filed;
            FiledLast24Months = bizCreditLienTotal.FiledLast24Months;
            LiabilityAmount = bizCreditLienTotal.LiabilityAmount;
            MonthsSinceLastFiling = bizCreditLienTotal.MonthsSinceLastFiling;
            TaxAmountLast24Months = bizCreditLienTotal.TaxAmountLast24Months;
            PercentTaxAmountToTradeBalance = bizCreditLienTotal.PercentTaxAmountToTradeBalance;
            PaymentReceived = bizCreditLienTotal.PaymentReceived;
        }
        public int Filed { get; set; }
        public int FiledLast24Months { get; set; }
        public string LiabilityAmount { get; set; }
        public int MonthsSinceLastFiling { get; set; }
        public string TaxAmountLast24Months { get; set; }
        public string PercentTaxAmountToTradeBalance { get; set; }
        public bool PaymentReceived { get; set; }
    }
}
