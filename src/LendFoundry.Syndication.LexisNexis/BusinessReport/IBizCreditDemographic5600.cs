using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBizCreditDemographic5600
    {
        List<ICodeMap> Sics { get; set; }
        List<ICodeMap> Naics { get; set; }
        string YearsInBusiness { get; set; }
        int YearsInBusinessActual { get; set; }
        string Sales { get; set; }
        int SalesAcutal { get; set; }
        string SalesEstimated { get; set; }
        string EmployeeSize { get; set; }
        int EmployeeSizeActual { get; set; }
        string BusinessType { get; set; }
        string OwnerType { get; set; }
        string Location { get; set; }
        string YearBusinessStarted { get; set; }
        int ExecutiveCount { get; set; }
        List<IBizCreditExecutive> Executives { get; set; }
        string CottageIndicator { get; set; }
        string NonprofitIndicator { get; set; }
    }
}
