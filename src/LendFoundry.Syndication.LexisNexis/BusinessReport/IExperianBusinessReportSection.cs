using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IExperianBusinessReportSection
    {
        List<IBizCreditReportRecord> ExperianBusinessReports { get; set; }
    }
}
