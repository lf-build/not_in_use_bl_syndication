namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IPropertyMortgageInfo
	{
		 string LoanAmount { get; set; }
		 string LoanAmount2 { get; set; }
		 string LoanType { get; set; }
		 string TransactionType { get; set; }
		 string AddressType { get; set; }
		 string Description { get; set; }
		 string LenderName { get; set; }
		 IDate LoanDate { get; set; }
		 IDate ContractDate { get; set; }
		 IDate SaleDate { get; set; }
		 IDate RecordingDate { get; set; }
		 string DocumentType { get; set; }
		 IDate AssessmentDate { get; set; }
	}
}
