namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class RelationshipRec : IRelationshipRec
    {
        public RelationshipRec()
        { }
        public RelationshipRec(ServiceReference.TopBusinessRelationshipRec relationshipRec)
        {
            if (relationshipRec == null)
                return;
            SubjectRole = relationshipRec.SubjectRole;
            RelativeRole = relationshipRec.RelativeRole;
        }
        public string SubjectRole { get; set; }
        public string RelativeRole { get; set; }
    }
}
