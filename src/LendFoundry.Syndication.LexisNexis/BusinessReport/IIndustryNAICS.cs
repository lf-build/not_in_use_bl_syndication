namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIndustryNaics
	{
		 string Naics { get; set; }
		 string NaicsDescription { get; set; }
	}
}
