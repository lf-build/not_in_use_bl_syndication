namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditExecutiveSummary
	{
		 int CurrentDaysBeyondTerms { get; set; }
		 int PredictedDaysBeyondTerms { get; set; }
		 IDate DateOfPrediction { get; set; }
		 int AverageIndustryDbt { get; set; }
		 string IndustryDescription { get; set; }
		 int AverageAllIndustryDbt { get; set; }
		 int ConfidencePercent { get; set; }
		 int ConfidenceSlope { get; set; }
		 int LowBalance { get; set; }
		 int HighBalance { get; set; }
		 int CurrentBalance { get; set; }
		 int HighestCreditExtended { get; set; }
		 int MedianOfCreditExtended { get; set; }
		 string PaymentPerformance { get; set; }
		 string PaymentTrend { get; set; }
	}
}
