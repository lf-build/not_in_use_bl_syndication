using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IWatercraft
    {
        int CurrentRecordCount { get; set; }
        int TotalCurrentRecordCount { get; set; }
        List<IWatercraftDetail> CurrentWatercrafts { get; set; }
        List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        int PriorRecordCount { get; set; }
        int TotalPriorRecordCount { get; set; }
        List<IWatercraftDetail> PriorWatercrafts { get; set; }
        List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
