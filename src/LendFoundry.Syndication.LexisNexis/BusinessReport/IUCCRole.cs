using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IUccRole
    {
        List<IUcc> ActiveUcCs { get; set; }
        List<ISourceDocInfo> ActiveSourceDocs { get; set; }
        List<IUcc> TerminatedUcCs { get; set; }
        List<ISourceDocInfo> TerminatedSourceDocs { get; set; }
    }
}
