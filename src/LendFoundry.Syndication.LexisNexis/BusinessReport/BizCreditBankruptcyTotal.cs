namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditBankruptcyTotal : IBizCreditBankruptcyTotal
	{
        public BizCreditBankruptcyTotal()
        { }
        public BizCreditBankruptcyTotal(ServiceReference.BizCreditBankruptcyTotal bizCreditBankruptcyTotal)
        {
            if (bizCreditBankruptcyTotal == null)
                return;
            FiledLast9Years9Months = bizCreditBankruptcyTotal.FiledLast9Years9Months;
            Filed = bizCreditBankruptcyTotal.Filed;
            FiledLast24Months = bizCreditBankruptcyTotal.FiledLast24Months;
            NonFiled = bizCreditBankruptcyTotal.NonFiled;
            NonSatisfiedLast24Months = bizCreditBankruptcyTotal.NonSatisfiedLast24Months;
            LiabilityBalanceFiled = bizCreditBankruptcyTotal.LiabilityBalanceFiled;
            LiabilityBalanceNonFiled = bizCreditBankruptcyTotal.LiabilityBalanceNonFiled;
            MonthsSinceLastFiling = bizCreditBankruptcyTotal.MonthsSinceLastFiling;
            MonthsSinceLastNonFiling = bizCreditBankruptcyTotal.MonthsSinceLastNonFiling;
            PaymentReceived = bizCreditBankruptcyTotal.PaymentReceived;
        }
		public string FiledLast9Years9Months { get; set; }
		public int Filed { get; set; }
		public int FiledLast24Months { get; set; }
		public int NonFiled { get; set; }
		public int NonSatisfiedLast24Months { get; set; }
		public string LiabilityBalanceFiled { get; set; }
		public string LiabilityBalanceNonFiled { get; set; }
		public int MonthsSinceLastFiling { get; set; }
		public int MonthsSinceLastNonFiling { get; set; }
		public bool PaymentReceived { get; set; }
	}
}
