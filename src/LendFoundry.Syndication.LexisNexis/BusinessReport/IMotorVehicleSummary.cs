using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IMotorVehicleSummary
    {
        int CurrentRecordCount { get; set; }
        int TotalCurrentRecordCount { get; set; }
        int PriorRecordCount { get; set; }
        int TotalPriorRecordCount { get; set; }
        List<IMotorVehicleDetail> CurrentVehicles { get; set; }
        List<ISourceDocInfo> CurrentSourceDocs { get; set; }
        List<IMotorVehicleDetail> PriorVehicles { get; set; }
        List<ISourceDocInfo> PriorSourceDocs { get; set; }
    }
}
