using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IWatercraftDetail
    {
        List<ISourceDocInfo> SourceDocs { get; set; }
        string StateJurisdiction { get; set; }
        string HullNumber { get; set; }
        int ModelYear { get; set; }
        string VesselMake { get; set; }
        string VesselModel { get; set; }
        string Propulsion { get; set; }
        string Length { get; set; }
        string Use { get; set; }
        string VesselName { get; set; }
        string RegistrationStatus { get; set; }
        string RegistrationNumber { get; set; }
        IDate RegistrationDate { get; set; }
        IDate ExpirationDate { get; set; }
        bool NonDmvSource { get; set; }
        List<IWatercraftParty> PriorParties { get; set; }
        List<IWatercraftParty> CurrentParties { get; set; }
    }
}
