namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditGovernmentDebarredContractor : IBizCreditGovernmentDebarredContractor
    {
        public BizCreditGovernmentDebarredContractor()
        { }
        public BizCreditGovernmentDebarredContractor(ServiceReference.BizCreditGovernmentDebarredContractor bizCreditGovernmentDebarredContractor)
        {
            if (bizCreditGovernmentDebarredContractor == null)
                return;
            Action = bizCreditGovernmentDebarredContractor.Action;
            BusinessName = bizCreditGovernmentDebarredContractor.BusinessName;
            OrigAddress = new Address(bizCreditGovernmentDebarredContractor.OrigAddress);
            StateName = bizCreditGovernmentDebarredContractor.StateName;
            Address = new Address(bizCreditGovernmentDebarredContractor.Address);
            ExtentOfAction = bizCreditGovernmentDebarredContractor.ExtentOfAction;
            Agency = bizCreditGovernmentDebarredContractor.Agency;
            DisputeIndicator = bizCreditGovernmentDebarredContractor.DisputeIndicator;
            DisputeCode = bizCreditGovernmentDebarredContractor.DisputeCode;
            DateFiled = new Date(bizCreditGovernmentDebarredContractor.DateFiled);
            DateReported = new Date(bizCreditGovernmentDebarredContractor.DateReported);
        }
        public string Action { get; set; }
        public string BusinessName { get; set; }
        public IAddress OrigAddress { get; set; }
        public string StateName { get; set; }
        public IAddress Address { get; set; }
        public string ExtentOfAction { get; set; }
        public string Agency { get; set; }
        public string DisputeIndicator { get; set; }
        public string DisputeCode { get; set; }
        public IDate DateFiled { get; set; }
        public IDate DateReported { get; set; }
    }
}
