namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class ReportRecord : IReportRecord
    {
        public ReportRecord()
        { }
        public ReportRecord(ServiceReference.TopBusinessReportRecord reportRecord)
        {
            if (reportRecord == null)
                return;
            Best = new BestSection(reportRecord.BestSection);
            Parent = new ParentSection(reportRecord.ParentSection);
            Finance = new FinanceSection(reportRecord.FinanceSection);
            Industry = new IndustrySection(reportRecord.IndustrySection);
            Incorporation = new IncorporationSection(reportRecord.IncorporationSection);
            OperationsSites = new OperationsSitesSection(reportRecord.OperationsSitesSection);
            Contact = new ContactSection(reportRecord.ContactSection);
            License = new LicenseSection(reportRecord.LicenseSection);
            Url = new UrlSection(reportRecord.URLSection);
            Bankruptcy = new BankruptcySection(reportRecord.BankruptcySection);
            Lien = new LienSection(reportRecord.LienSection);
            Ucc = new UccSection(reportRecord.UCCSection);
            Property = new PropertySection(reportRecord.PropertySection);
            MotorVehicle = new MotorVehicleSection(reportRecord.MotorVehicleSection);
            Watercraft = new WatercraftSection(reportRecord.WatercraftSection);
            Aircraft = new AircraftSection(reportRecord.AircraftSection);
            Associate = new AssociateSection(reportRecord.AssociateSection);
            RegisteredAgent = new RegisteredAgentSection(reportRecord.RegisteredAgentSection);
            Irs5500 = new Irs5500Section(reportRecord.IRS5500Section);
            DunBradStreet = new DunBradStreetSection(reportRecord.DunBradStreetSection);
            Sanction = new SanctionSection(reportRecord.SanctionSection);
            ConnectedBusiness = new ConnectedBusinessSection(reportRecord.ConnectedBusinessSection);
            ExperianBusinessReport = new ExperianBusinessReportSection(reportRecord.ExperianBusinessReportSection);
            CompanyVerification = new CompanyVerificationSection(reportRecord.CompanyVerificationSection);
            BusinessRegistration = new BusinessRegistrationSection(reportRecord.BusinessRegistrationSection);
            Source = new SourceSection(reportRecord.SourceSection);
        }
        public IBestSection Best { get; set; }
        public IParentSection Parent { get; set; }
        public IFinanceSection Finance { get; set; }
        public IIndustrySection Industry { get; set; }
        public IIncorporationSection Incorporation { get; set; }
        public IOperationsSitesSection OperationsSites { get; set; }
        public IContactSection Contact { get; set; }
        public ILicenseSection License { get; set; }
        public IUrlSection Url { get; set; }
        public IBankruptcySection Bankruptcy { get; set; }
        public ILienSection Lien { get; set; }
        public IUccSection Ucc { get; set; }
        public IPropertySection Property { get; set; }
        public IMotorVehicleSection MotorVehicle { get; set; }
        public IWatercraftSection Watercraft { get; set; }
        public IAircraftSection Aircraft { get; set; }
        public IAssociateSection Associate { get; set; }
        public IRegisteredAgentSection RegisteredAgent { get; set; }
        public IIrs5500Section Irs5500 { get; set; }
        public IDunBradStreetSection DunBradStreet { get; set; }
        public ISanctionSection Sanction { get; set; }
        public IConnectedBusinessSection ConnectedBusiness { get; set; }
        public IExperianBusinessReportSection ExperianBusinessReport { get; set; }
        public ICompanyVerificationSection CompanyVerification { get; set; }
        public IBusinessRegistrationSection BusinessRegistration { get; set; }
        public ISourceSection Source { get; set; }
    }
}
