namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class GeoAddressMatch : IGeoAddressMatch
    {
        public GeoAddressMatch()
        { }
        public GeoAddressMatch(ServiceReference.GeoAddressMatch geoAddressMatch)
        {
            if (geoAddressMatch == null)
                return;
            Address = new Address(geoAddressMatch.Address);
            GeoLocationMatch = new GeoLocationMatch(geoAddressMatch.GeoLocationMatch);
        }
        public IAddress Address { get; set; }
        public IGeoLocationMatch GeoLocationMatch { get; set; }
    }
}
