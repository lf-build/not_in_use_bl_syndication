namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIrsAdmin
	{
		 string Name { get; set; }
		 string CareOfName { get; set; }
		 IAddress Address { get; set; }
		 string Fein { get; set; }
		 string Phone { get; set; }
		 IDate SignatureDate { get; set; }
		 string Signature { get; set; }
	}
}
