using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class LicenseSummary : ILicenseSummary
    {
        public LicenseSummary()
        { }
        public LicenseSummary(ServiceReference.TopBusinessLicenseSummary licenseSummary)
        {
            if (licenseSummary == null)
                return;
            if (licenseSummary.Licenses != null)
            {
                Licenses = new List<ILicenseRecord>(licenseSummary.Licenses.Select(license => new LicenseRecord(license)));
            }
            if (licenseSummary.SourceDocs != null)
            {
                SourceDocs = new List<ISourceDocInfo>(licenseSummary.SourceDocs.Select(sourceDoc => new SourceDocInfo(sourceDoc)));
            }
        }
        public List<ILicenseRecord> Licenses { get; set; }
        public List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
