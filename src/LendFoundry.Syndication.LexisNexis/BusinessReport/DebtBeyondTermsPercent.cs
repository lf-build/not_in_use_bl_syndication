namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class DebtBeyondTermsPercent : IDebtBeyondTermsPercent
	{
        public DebtBeyondTermsPercent()
        { }
        public DebtBeyondTermsPercent(ServiceReference.DebtBeyondTermsPercent debtBeyondTermsPercent)
        {
            if (debtBeyondTermsPercent == null)
                return;
            Current = debtBeyondTermsPercent.Current;
            Day01To30 = debtBeyondTermsPercent.Day_01_30;
            Day31To60 = debtBeyondTermsPercent.Day_31_60;
            Day61To90 = debtBeyondTermsPercent.Day_61_90;
            Day91ToPlus = debtBeyondTermsPercent.Day_91_Plus;
        }
        public int Current { get; set; }
		public int Day01To30 { get; set; }
		public int Day31To60 { get; set; }
		public int Day61To90 { get; set; }
		public int Day91ToPlus { get; set; }
	}
}
