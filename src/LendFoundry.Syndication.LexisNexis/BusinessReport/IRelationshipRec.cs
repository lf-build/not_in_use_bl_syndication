namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IRelationshipRec
	{
		 string SubjectRole { get; set; }
		 string RelativeRole { get; set; }
	}
}
