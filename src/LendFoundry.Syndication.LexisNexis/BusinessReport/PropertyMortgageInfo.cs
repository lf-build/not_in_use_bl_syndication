namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class PropertyMortgageInfo : IPropertyMortgageInfo
    {
        public PropertyMortgageInfo()
        { }
        public PropertyMortgageInfo(ServiceReference.TopBusinessPropertyMortgageInfo propertyMortgageInfo)
        {
            if (propertyMortgageInfo == null)
                return;
            LoanAmount = propertyMortgageInfo.LoanAmount;
            LoanAmount2 = propertyMortgageInfo.LoanAmount2;
            LoanType = propertyMortgageInfo.LoanType;
            TransactionType = propertyMortgageInfo.TransactionType;
            AddressType = propertyMortgageInfo.AddressType;
            Description = propertyMortgageInfo.Description;
            LenderName = propertyMortgageInfo.LenderName;
            LoanDate = new Date(propertyMortgageInfo.LoanDate);
            ContractDate = new Date(propertyMortgageInfo.ContractDate);
            SaleDate = new Date(propertyMortgageInfo.SaleDate);
            RecordingDate = new Date(propertyMortgageInfo.RecordingDate);
            DocumentType = propertyMortgageInfo.DocumentType;
            AssessmentDate = new Date(propertyMortgageInfo.AssessmentDate);
        }
        public string LoanAmount { get; set; }
        public string LoanAmount2 { get; set; }
        public string LoanType { get; set; }
        public string TransactionType { get; set; }
        public string AddressType { get; set; }
        public string Description { get; set; }
        public string LenderName { get; set; }
        public IDate LoanDate { get; set; }
        public IDate ContractDate { get; set; }
        public IDate SaleDate { get; set; }
        public IDate RecordingDate { get; set; }
        public string DocumentType { get; set; }
        public IDate AssessmentDate { get; set; }
    }
}
