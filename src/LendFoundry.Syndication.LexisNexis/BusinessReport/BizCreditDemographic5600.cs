using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class BizCreditDemographic5600 : IBizCreditDemographic5600
    {
        public BizCreditDemographic5600()
        { }
        public BizCreditDemographic5600(ServiceReference.BizCreditDemographic5600 bizCreditDemographic5600)
        {
            if (bizCreditDemographic5600 == null)
                return;
            if (bizCreditDemographic5600.SICs != null)
            {
                Sics = new List<ICodeMap>(bizCreditDemographic5600.SICs.Select(sic => new CodeMap(sic)));
            }
            if (bizCreditDemographic5600.NAICs != null)
            {
                Naics = new List<ICodeMap>(bizCreditDemographic5600.NAICs.Select(naic => new CodeMap(naic)));
            }
            YearsInBusiness = bizCreditDemographic5600.YearsInBusiness;
            YearsInBusinessActual = bizCreditDemographic5600.YearsInBusinessActual;
            Sales = bizCreditDemographic5600.Sales;
            SalesAcutal = bizCreditDemographic5600.SalesAcutal;
            SalesEstimated = bizCreditDemographic5600.SalesEstimated;
            EmployeeSize = bizCreditDemographic5600.EmployeeSize;
            EmployeeSizeActual = bizCreditDemographic5600.EmployeeSizeActual;
            BusinessType = bizCreditDemographic5600.BusinessType;
            OwnerType = bizCreditDemographic5600.OwnerType;
            Location = bizCreditDemographic5600.Location;
            YearBusinessStarted = bizCreditDemographic5600.YearBusinessStarted;
            ExecutiveCount = bizCreditDemographic5600.ExecutiveCount;
            if (bizCreditDemographic5600.Executives != null)
            {
                Executives = new List<IBizCreditExecutive>(bizCreditDemographic5600.Executives.Select(executive => new BizCreditExecutive(executive)));
            }
            CottageIndicator = bizCreditDemographic5600.CottageIndicator;
            NonprofitIndicator = bizCreditDemographic5600.NonprofitIndicator;
        }
        public List<ICodeMap> Sics { get; set; }
        public List<ICodeMap> Naics { get; set; }
        public string YearsInBusiness { get; set; }
        public int YearsInBusinessActual { get; set; }
        public string Sales { get; set; }
        public int SalesAcutal { get; set; }
        public string SalesEstimated { get; set; }
        public string EmployeeSize { get; set; }
        public int EmployeeSizeActual { get; set; }
        public string BusinessType { get; set; }
        public string OwnerType { get; set; }
        public string Location { get; set; }
        public string YearBusinessStarted { get; set; }
        public int ExecutiveCount { get; set; }
        public List<IBizCreditExecutive> Executives { get; set; }
        public string CottageIndicator { get; set; }
        public string NonprofitIndicator { get; set; }
    }
}
