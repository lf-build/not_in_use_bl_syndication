namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIndustrySic
	{
		 string SicCode { get; set; }
		 string SicCodeDescription { get; set; }
	}
}
