namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IIncorporationExistenceTerm
	{
		 string Type { get; set; }
		 IDate ExpirationDate { get; set; }
		 uint Years { get; set; }
	}
}
