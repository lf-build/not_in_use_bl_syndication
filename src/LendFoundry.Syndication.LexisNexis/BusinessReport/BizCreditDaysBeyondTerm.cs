namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditDaysBeyondTerm : IBizCreditDaysBeyondTerm
    {
        public BizCreditDaysBeyondTerm()
        { }
        public BizCreditDaysBeyondTerm(ServiceReference.BizCreditDaysBeyondTerm bizCreditDaysBeyondTerm)
        {
            if (bizCreditDaysBeyondTerm == null)
                return;
            CreditRating = bizCreditDaysBeyondTerm.CreditRating;
            Quarter1AverageDbt = bizCreditDaysBeyondTerm.Quarter1AverageDBT;
            Quarter2AverageDbt = bizCreditDaysBeyondTerm.Quarter2AverageDBT;
            Quarter3AverageDbt = bizCreditDaysBeyondTerm.Quarter3AverageDBT;
            Quarter4AverageDbt = bizCreditDaysBeyondTerm.Quarter4AverageDBT;
            Quarter5AverageDbt = bizCreditDaysBeyondTerm.Quarter5AverageDBT;
            CombinedDbt = bizCreditDaysBeyondTerm.CombinedDBT;
        }
        public string CreditRating { get; set; }
		public string Quarter1AverageDbt { get; set; }
		public string Quarter2AverageDbt { get; set; }
		public string Quarter3AverageDbt { get; set; }
		public string Quarter4AverageDbt { get; set; }
		public string Quarter5AverageDbt { get; set; }
		public string CombinedDbt { get; set; }
	}
}
