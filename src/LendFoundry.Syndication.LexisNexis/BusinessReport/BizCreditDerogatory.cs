namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class BizCreditDerogatory : IBizCreditDerogatory
	{
        public BizCreditDerogatory()
        { }
        public BizCreditDerogatory(ServiceReference.BizCreditDerogatory bizCreditDerogatory)
        {
            if (bizCreditDerogatory == null)
                return;
            Indicator = bizCreditDerogatory.Indicator;
            FiledDate = new Date(bizCreditDerogatory.FiledDate);
            LiabilityAmount = bizCreditDerogatory.LiabilityAmount;
            TotalNumberDerogatoryItems = bizCreditDerogatory.TotalNumberDerogatoryItems;
            TotalNumberLegalItems = bizCreditDerogatory.TotalNumberLegalItems;
            TotalLegalBalanceAmount = bizCreditDerogatory.TotalLegalBalanceAmount;
        }
        public string Indicator { get; set; }
		public IDate FiledDate { get; set; }
		public string LiabilityAmount { get; set; }
		public int TotalNumberDerogatoryItems { get; set; }
		public int TotalNumberLegalItems { get; set; }
		public string TotalLegalBalanceAmount { get; set; }
	}
}
