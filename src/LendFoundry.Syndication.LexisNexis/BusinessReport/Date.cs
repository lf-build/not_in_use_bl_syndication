namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class Date : IDate
	{
        public Date()
        { }
        public Date(ServiceReference.Date date)
        {
            if (date == null)
                return;
            Year = date.Year;
            Month = date.Month;
            Day = date.Day;
        }
		public short Year { get; set; }
		public short Month { get; set; }
		public short Day { get; set; }
	}
}
