using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IPropertyParty
    {
        List<IPropertyTransaction> Owners { get; set; }
        List<IPropertyTransaction> Sellers { get; set; }
        List<IPropertyTransaction> Borrowers { get; set; }
        List<IPropertyMortgageInfo> Mortgages { get; set; }
        List<IPropertyForeclosure> Foreclosures { get; set; }
        List<IPropertyForeclosure> NoticeOfDefaults { get; set; }
    }
}
