using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IIndustrySection
    {
        List<IIndustryRecord> IndustryRecords { get; set; }
    }
}
