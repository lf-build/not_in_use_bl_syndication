using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class IndustrySection : IIndustrySection
    {
        public IndustrySection()
        { }
        public IndustrySection(ServiceReference.TopBusinessIndustrySection industrySection)
        {
            if (industrySection?.IndustryRecords != null)
            {
                IndustryRecords = new List<IIndustryRecord>(industrySection.IndustryRecords.Select(industryRecord => new IndustryRecord(industryRecord)));
            }
        }
        public List<IIndustryRecord> IndustryRecords { get; set; }
    }
}
