namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class MotorVehicleSection : IMotorVehicleSection
	{
        public MotorVehicleSection()
        { }
        public MotorVehicleSection(ServiceReference.TopBusinessMotorVehicleSection motorVehicleSection)
        {
            if (motorVehicleSection == null)
                return;
            MotorVehicleRecordCount = motorVehicleSection.MotorVehicleRecordCount;
            TotalMotorVehicleRecordCount = motorVehicleSection.TotalMotorVehicleRecordCount;
            MotorVehicleRecords = new MotorVehicleSummary(motorVehicleSection.MotorVehicleRecords);
        }
        public int MotorVehicleRecordCount { get; set; }
		public int TotalMotorVehicleRecordCount { get; set; }
		public IMotorVehicleSummary MotorVehicleRecords { get; set; }
	}
}
