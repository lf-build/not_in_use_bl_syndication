namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IBizCreditDaysBeyondTerm
	{
		 string CreditRating { get; set; }
		 string Quarter1AverageDbt { get; set; }
		 string Quarter2AverageDbt { get; set; }
		 string Quarter3AverageDbt { get; set; }
		 string Quarter4AverageDbt { get; set; }
		 string Quarter5AverageDbt { get; set; }
		 string CombinedDbt { get; set; }
	}
}
