using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface ILicenseRecord
    {
        string LicenseNumber { get; set; }
        string Description { get; set; }
        string Issuer { get; set; }
        IDate IssueDate { get; set; }
        IDate ExpirationDate { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
