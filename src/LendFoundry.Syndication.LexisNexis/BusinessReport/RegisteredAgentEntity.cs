namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class RegisteredAgentEntity : IRegisteredAgentEntity
    {
        public RegisteredAgentEntity()
        { }
        public RegisteredAgentEntity(ServiceReference.TopBusinessRegisteredAgentEntity registeredAgentEntity)
        {
            Name = new Name(registeredAgentEntity.Name);
            Address = new Address(registeredAgentEntity.Address);
            CompanyName = registeredAgentEntity.CompanyName;
            Phone = registeredAgentEntity.Phone;
            UniqueId = registeredAgentEntity.UniqueId;
            BusinessIds = new BusinessIdentity(registeredAgentEntity.BusinessIds);
            Title = registeredAgentEntity.Title;
            IsDeceased = registeredAgentEntity.IsDeceased;
            HasDerog = registeredAgentEntity.HasDerog;
            FromDate = new Date(registeredAgentEntity.FromDate);
            ToDate = new Date(registeredAgentEntity.ToDate);
        }
        public IName Name { get; set; }
        public IAddress Address { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string UniqueId { get; set; }
        public IBusinessIdentity BusinessIds { get; set; }
        public string Title { get; set; }
        public bool IsDeceased { get; set; }
        public bool HasDerog { get; set; }
        public IDate FromDate { get; set; }
        public IDate ToDate { get; set; }
    }
}
