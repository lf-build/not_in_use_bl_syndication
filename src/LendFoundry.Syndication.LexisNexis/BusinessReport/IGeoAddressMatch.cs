namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IGeoAddressMatch
	{
		 IAddress Address { get; set; }
		 IGeoLocationMatch GeoLocationMatch { get; set; }
	}
}
