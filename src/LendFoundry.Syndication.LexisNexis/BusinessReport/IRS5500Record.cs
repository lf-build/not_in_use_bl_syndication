namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public class Irs5500Record : IIrs5500Record
    {
        public Irs5500Record()
        { }
        public Irs5500Record(ServiceReference.TopBusinessIRS5500Record iRs5500Record)
        {
            if (iRs5500Record == null)
                return;
            FormNumber = iRs5500Record.FormNumber;
            FormDate = new Date(iRs5500Record.FormDate);
            Rsi = iRs5500Record.RSI;
            DocumentLocatorNumber = iRs5500Record.DocumentLocatorNumber;
            PlanName = iRs5500Record.PlanName;
            PlanNumber = iRs5500Record.PlanNumber;
            TransactionDate = new Date(iRs5500Record.TransactionDate);
            FormPlanYearBeginDate = new Date(iRs5500Record.FormPlanYearBeginDate);
            FormTaxPrd = iRs5500Record.FormTaxPRD;
            TypePlanEntityIndicator = iRs5500Record.TypePlanEntityIndicator;
            TypeDfePlanEntity = iRs5500Record.TypeDFEPlanEntity;
            TypePlanFilingIndicator = iRs5500Record.TypePlanFilingIndicator;
            CollectiveBargainIndicator = iRs5500Record.CollectiveBargainIndicator;
            ExtApplicationFiledIndicator = iRs5500Record.ExtApplicationFiledIndicator;
            PlanEffectiveDate = new Date(iRs5500Record.PlanEffectiveDate);
            Fein = iRs5500Record.FEIN;
            SponsorName = iRs5500Record.SponsorName;
            SponsorDfeDbaName = iRs5500Record.SponsorDfeDBAName;
            SponsorDfeCareOfName = iRs5500Record.SponsorDfeCareOfName;
            SponsorDfeMailStrAddress = iRs5500Record.SponsorDfeMailStrAddress;
            SponsorDfeLoc01Address = iRs5500Record.SponsorDfeLoc01Address;
            SponsorDfeLoc02Address = iRs5500Record.SponsorDfeLoc02Address;
            SponsorDfeForeignRouteCd = iRs5500Record.SponsorDfeForeignRouteCD;
            SponsorDfeForeignMailCountry = iRs5500Record.SponsorDfeForeignMailCountry;
            AdminStreetAddress = iRs5500Record.AdminStreetAddress;
            AdminForeignRouteCd = iRs5500Record.AdminForeignRouteCD;
            AdminForeignMailingCountry = iRs5500Record.AdminForeignMailingCountry;
            LastReportedSponsorName = iRs5500Record.LastReportedSponsorName;
            LastReportedSponsorFein = iRs5500Record.LastReportedSponsorFein;
            LastReportedPlanNumber = iRs5500Record.LastReportedPlanNumber;
            PreParerStrAddress = iRs5500Record.PreParerStrAddress;
            PreparerForeignRouteCd = iRs5500Record.PreparerForeignRouteCD;
            PreparerFrgnMailingCountry = iRs5500Record.PreparerFrgnMailingCountry;
            AdminSignatureInd = iRs5500Record.AdminSignatureInd;
            AdminSignedDate = iRs5500Record.AdminSignedDate;
            AdminSignedName = iRs5500Record.AdminSignedName;
            SponsorSignatureIndicator = iRs5500Record.SponsorSignatureIndicator;
            SponsorSignedDate = iRs5500Record.SponsorSignedDate;
            SponsorSignedName = iRs5500Record.SponsorSignedName;
            TotPartcpBoyCnt = iRs5500Record.TotPartcpBoyCnt;
            TotActivePartcpCnt = iRs5500Record.TotActivePartcpCnt;
            RtdSepPartcpRcvgCnt = iRs5500Record.RtdSepPartcpRcvgCnt;
            RtdSepPartcpFutCnt = iRs5500Record.RtdSepPartcpFutCnt;
            SubtActRtdSepCnt = iRs5500Record.SubtActRtdSepCnt;
            BenefRcvgBnftCnt = iRs5500Record.BenefRcvgBnftCnt;
            TotActRtdSepBenefCnt = iRs5500Record.TotActRtdSepBenefCnt;
            PartcpAccountBalCnt = iRs5500Record.PartcpAccountBalCnt;
            SepPartcpParttlVstCnt = iRs5500Record.SepPartcpParttlVstCnt;
            SsaPartCpPartlVstdCnt = iRs5500Record.SSAPartCpPartlVSTDCnt;
            PensionBenefitPlanId = iRs5500Record.PensionBenefitPlanID;
            TypePensionBnftCode = iRs5500Record.TypePensionBnftCode;
            WelfareBenefitPlanIndicator = iRs5500Record.WelfareBenefitPlanIndicator;
            TypeWelfareBnftCode = iRs5500Record.TypeWelfareBnftCode;
            FringeBenefitPlanIndicator = iRs5500Record.FringeBenefitPlanIndicator;
            FundingArrangementCode = iRs5500Record.FundingArrangementCode;
            BenefitCode = iRs5500Record.BenefitCode;
            ScheduleAttachments = new IrsScheduleAttachment(iRs5500Record.ScheduleAttachments);
            Name = new Name(iRs5500Record.Name);
            Score = iRs5500Record.Score;
            IsMailingAddr = iRs5500Record.IsMailingAddr;
            Address = new Address(iRs5500Record.Address);
            SponsorDfePhoneNumber = iRs5500Record.SponsorDFEPhoneNumber;
            BusinessCode = iRs5500Record.BusinessCode;
            Sponsor = new IrsSponsor(iRs5500Record.Sponsor);
            Administrator = new IrsAdmin(iRs5500Record.Administrator);
            Preparer = new IrsPreparer(iRs5500Record.Preparer);
        }
        public string FormNumber { get; set; }
        public IDate FormDate { get; set; }
        public string Rsi { get; set; }
        public string DocumentLocatorNumber { get; set; }
        public string PlanName { get; set; }
        public string PlanNumber { get; set; }
        public IDate TransactionDate { get; set; }
        public IDate FormPlanYearBeginDate { get; set; }
        public string FormTaxPrd { get; set; }
        public string TypePlanEntityIndicator { get; set; }
        public string TypeDfePlanEntity { get; set; }
        public string TypePlanFilingIndicator { get; set; }
        public string CollectiveBargainIndicator { get; set; }
        public string ExtApplicationFiledIndicator { get; set; }
        public IDate PlanEffectiveDate { get; set; }
        public string Fein { get; set; }
        public string SponsorName { get; set; }
        public string SponsorDfeDbaName { get; set; }
        public string SponsorDfeCareOfName { get; set; }
        public string SponsorDfeMailStrAddress { get; set; }
        public string SponsorDfeLoc01Address { get; set; }
        public string SponsorDfeLoc02Address { get; set; }
        public string SponsorDfeForeignRouteCd { get; set; }
        public string SponsorDfeForeignMailCountry { get; set; }
        public string AdminStreetAddress { get; set; }
        public string AdminForeignRouteCd { get; set; }
        public string AdminForeignMailingCountry { get; set; }
        public string LastReportedSponsorName { get; set; }
        public string LastReportedSponsorFein { get; set; }
        public string LastReportedPlanNumber { get; set; }
        public string PreParerStrAddress { get; set; }
        public string PreparerForeignRouteCd { get; set; }
        public string PreparerFrgnMailingCountry { get; set; }
        public string AdminSignatureInd { get; set; }
        public string AdminSignedDate { get; set; }
        public string AdminSignedName { get; set; }
        public string SponsorSignatureIndicator { get; set; }
        public string SponsorSignedDate { get; set; }
        public string SponsorSignedName { get; set; }
        public string TotPartcpBoyCnt { get; set; }
        public string TotActivePartcpCnt { get; set; }
        public string RtdSepPartcpRcvgCnt { get; set; }
        public string RtdSepPartcpFutCnt { get; set; }
        public string SubtActRtdSepCnt { get; set; }
        public string BenefRcvgBnftCnt { get; set; }
        public string TotActRtdSepBenefCnt { get; set; }
        public string PartcpAccountBalCnt { get; set; }
        public string SepPartcpParttlVstCnt { get; set; }
        public string SsaPartCpPartlVstdCnt { get; set; }
        public string PensionBenefitPlanId { get; set; }
        public string TypePensionBnftCode { get; set; }
        public string WelfareBenefitPlanIndicator { get; set; }
        public string TypeWelfareBnftCode { get; set; }
        public string FringeBenefitPlanIndicator { get; set; }
        public string FundingArrangementCode { get; set; }
        public string BenefitCode { get; set; }
        public IIrsScheduleAttachment ScheduleAttachments { get; set; }
        public IName Name { get; set; }
        public string Score { get; set; }
        public bool IsMailingAddr { get; set; }
        public IAddress Address { get; set; }
        public string SponsorDfePhoneNumber { get; set; }
        public string BusinessCode { get; set; }
        public IIrsSponsor Sponsor { get; set; }
        public IIrsAdmin Administrator { get; set; }
        public IIrsPreparer Preparer { get; set; }
    }
}
