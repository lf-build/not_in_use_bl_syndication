using System.Collections.Generic;
namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
    public interface IBestOtherCompany
    {
        string CompanyName { get; set; }
        IDate DateFirstSeen { get; set; }
        IDate DateLastSeen { get; set; }
        bool Status { get; set; }
        List<ISourceDocInfo> SourceDocs { get; set; }
    }
}
