namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public interface IDeeds
	{
		 string State { get; set; }
		 string CountyName { get; set; }
		 string LenderName { get; set; }
		 string LegalBriefDescription { get; set; }
		 string DocumentTypeCode { get; set; }
		 string DocumentTypeDesc { get; set; }
		 string ArmResetDate { get; set; }
		 string DocumentNumber { get; set; }
		 string RecorderBookNumber { get; set; }
		 string RecorderPageNumber { get; set; }
		 string LandLotSize { get; set; }
		 string CityTransferTax { get; set; }
		 string CountyTransferTax { get; set; }
		 string TotalTransferTax { get; set; }
		 string PropertyUseCode { get; set; }
		 string PropertyUseDesc { get; set; }
		 string FirstTdLoanAmount { get; set; }
		 string FirstTdLoanTypeCode { get; set; }
		 string FirstTdLoanTypeDesc { get; set; }
		 string TypeFinancing { get; set; }
		 string FirstTdInterestRate { get; set; }
		 string FirstTdDueDate { get; set; }
		 string TitleCompanyName { get; set; }
		 string FaresTransactionType { get; set; }
		 string FaresTransactionTypeDesc { get; set; }
		 string FaresMortgageDeedType { get; set; }
		 string FaresMortgageDeedTypeDesc { get; set; }
		 string FaresMortgageTermCode { get; set; }
		 string FaresMortgageTermCodeDesc { get; set; }
		 string FaresMortgageTerm { get; set; }
		 string FaresIrisApn { get; set; }
	}
}
