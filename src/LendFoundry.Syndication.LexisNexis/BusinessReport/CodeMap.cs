namespace LendFoundry.Syndication.LexisNexis.BusinessReport
{
	public class CodeMap : ICodeMap
	{
        public CodeMap()
        { }
        public CodeMap(ServiceReference.CodeMap codeMap)
        {
            if (codeMap == null)
                return;
            Code = codeMap.Code;
            Description = codeMap.Description;
        }
		public string Code { get; set; }
		public string Description { get; set; }
	}
}
