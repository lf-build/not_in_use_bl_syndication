namespace LendFoundry.Syndication.LexisNexis
{
	public interface IUser
	{
		 string ReferenceCode { get; set; }
		 string BillingCode { get; set; }
		 string QueryId { get; set; }
		 string GrammLeachBlileyActPurpose { get; set; }
		 string DriversPrivacyProtectionActPurpose { get; set; }
		 IEndUserInfo EndUser { get; set; }
		 int MaxWaitSeconds { get; set; }
		 string AccountNumber { get; set; }

        
	}
}
