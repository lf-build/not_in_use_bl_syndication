﻿using System;
using System.Net;
using LendFoundry.Syndication.LexisNexis.BusinessSearch.ServiceReference;

namespace LendFoundry.Syndication.LexisNexis.Proxy
{
    public class BusinessReportServiceProxy : IBusinessReportServiceProxy
    {
        public BusinessReportServiceProxy(IBusinessReportConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.EndUser == null)
                throw new ArgumentNullException(nameof(configuration.EndUser));

            if (configuration.ReportOption == null)
                throw new ArgumentNullException(nameof(configuration.ReportOption));

            if (configuration.UserName == null)
                throw new ArgumentNullException(nameof(configuration.UserName));

            if (configuration.Password == null)
                throw new ArgumentNullException(nameof(configuration.Password));

            if (configuration.SearchOption == null)
                throw new ArgumentNullException(nameof(configuration.SearchOption));

            Configuration = new BusinessReportConfiguration(configuration);
        }

        private IBusinessReportConfiguration Configuration { get; }

        public BusinessReport.ServiceReference.TopBusinessReportResponse TopBusinessReport(BusinessReport.ServiceReference.User user, BusinessReport.ServiceReference.TopBusinessReportOption options, BusinessReport.ServiceReference.TopBusinessReportBy reportBy)
        {
            if (reportBy == null)
                throw new ArgumentNullException(nameof(reportBy));

            var soapClient = new BusinessReport.ServiceReference.WsAccurint
            {
                Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password)
            };

            var soapResponse = soapClient.TopBusinessReport(user, options, reportBy);
            return soapResponse;
        }

        public TopBusinessSearchResponse TopBusinessSearch(BusinessSearch.ServiceReference.User user, TopBusinessSearchBy search, TopBusinessSearchOption options)
        {
            if (search == null)
                throw new ArgumentNullException(nameof(search));

            var soapClient = new WsAccurint
            {
                Credentials = new NetworkCredential(Configuration.UserName, Configuration.Password)
            };

            var soapResponse = soapClient.TopBusinessSearch(user, search, options);

            return soapResponse;
        }
    }
}
