﻿namespace LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference
{
    public partial class TopBusinessReportOption
    {
        public TopBusinessReportOption(IReportOption reportOption)
        {
            if (reportOption == null)
                return;
            IncludeAircrafts = reportOption.IncludeAircrafts;
            IncludeAssociatedBusinesses = reportOption.IncludeAssociatedBusinesses;
            IncludeBankruptcies = reportOption.IncludeBankruptcies;
            IncludeContacts = reportOption.IncludeContacts;
            IncludeFinances = reportOption.IncludeFinances;
            IncludeIndustries = reportOption.IncludeIndustries;
            IncludeProfessionalLicenses = reportOption.IncludeProfessionalLicenses;
            IncludeLiensJudgments = reportOption.IncludeLiensJudgments;
            IncludeMotorVehicles = reportOption.IncludeMotorVehicles;
            IncludeOpsSites = reportOption.IncludeOpsSites;
            IncludeIncorporation = reportOption.IncludeIncorporation;
            IncludeParents = reportOption.IncludeParents;
            IncludeProperties = reportOption.IncludeProperties;
            IncludeUCCFilings = reportOption.IncludeUccFilings;
            IncludeUCCFilingsSecureds = reportOption.IncludeUccFilingsSecureds;
            IncludeInternetDomains = reportOption.IncludeInternetDomains;
            IncludeWatercrafts = reportOption.IncludeWatercrafts;
            IncludeSourceCounts = reportOption.IncludeSourceCounts;
            IncludeRegisteredAgents = reportOption.IncludeRegisteredAgents;
            IncludeConnectedBusinesses = reportOption.IncludeConnectedBusinesses;
            IncludeNameVariations = reportOption.IncludeNameVariations;
            IncludeIRS5500 = reportOption.IncludeIrs5500;
            IncludeExperianBusinessReports = reportOption.IncludeExperianBusinessReports;
            IncludeCompanyVerification = reportOption.IncludeCompanyVerification;
            IncludeDunBradStreet = reportOption.IncludeDunBradStreet;
            IncludeSanctions = reportOption.IncludeSanctions;
            IncludeBusinessRegistrations = reportOption.IncludeBusinessRegistrations;
            BusinessReportFetchLevel = (TopBusinessLevelType)(int)reportOption.BusinessReportFetchLevel;
        }
    }
}
