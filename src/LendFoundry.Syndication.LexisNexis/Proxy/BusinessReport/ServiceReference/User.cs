﻿namespace LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference
{
    public partial class User
    {
        public User()
        { }
        public User(IUser user)
        {
            if (user == null)
                return;
            ReferenceCode = user.ReferenceCode;
            BillingCode = user.BillingCode;
            QueryId = user.QueryId;
            GLBPurpose = user.GrammLeachBlileyActPurpose;
            DLPurpose = user.DriversPrivacyProtectionActPurpose;
            EndUser = new EndUserInfo
            {
                City = user.EndUser.City,
                CompanyName = user.EndUser.CompanyName,
                State = user.EndUser.State,
                StreetAddress1 = user.EndUser.StreetAddress1,
                Zip5 = user.EndUser.Zip5
            };
            MaxWaitSeconds = user.MaxWaitSeconds;
            AccountNumber = user.AccountNumber;
        }
    }
}
