﻿namespace LendFoundry.Syndication.LexisNexis.BusinessReport.ServiceReference
{
    public partial class TopBusinessReportBy
    {
        public TopBusinessReportBy()
        { }
        public TopBusinessReportBy(IReportRequest reportBy)
        {
            if (reportBy == null)
                return;
            BusinessIds = new BusinessIdentity
            {
                DotID = reportBy.DotId,
                EmpID = reportBy.EmpId,
                POWID = reportBy.PowId,
                ProxID = reportBy.ProxId,
                SeleID = reportBy.SeleId,
                OrgID = reportBy.OrgId,
                UltID = reportBy.UltId,
            };
            if (reportBy.DotId > 0)
                BusinessIds.DotIDSpecified = true;
            if (reportBy.EmpId > 0)
                BusinessIds.EmpIDSpecified = true;
            if (reportBy.OrgId > 0)
                BusinessIds.OrgIDSpecified = true;
            if (reportBy.PowId > 0)
                BusinessIds.POWIDSpecified = true;
            if (reportBy.ProxId > 0)
                BusinessIds.ProxIDSpecified = true;
            if (reportBy.SeleId > 0)
                BusinessIds.SeleIDSpecified = true;
            if (reportBy.UltId > 0)
                BusinessIds.UltIDSpecified = true;
        }
    }
}
