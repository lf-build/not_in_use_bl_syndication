﻿namespace LendFoundry.Syndication.LexisNexis.Proxy
{
    public interface IBusinessReportServiceProxy
    {
        BusinessReport.ServiceReference.TopBusinessReportResponse TopBusinessReport(BusinessReport.ServiceReference.User user, BusinessReport.ServiceReference.TopBusinessReportOption options, BusinessReport.ServiceReference.TopBusinessReportBy reportBy);
        BusinessSearch.ServiceReference.TopBusinessSearchResponse TopBusinessSearch(BusinessSearch.ServiceReference.User user, BusinessSearch.ServiceReference.TopBusinessSearchBy request, BusinessSearch.ServiceReference.TopBusinessSearchOption options);
    }
}
