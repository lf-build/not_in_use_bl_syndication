﻿namespace LendFoundry.Syndication.LexisNexis.BusinessSearch.ServiceReference
{
    public partial class TopBusinessSearchOption
    {
        public TopBusinessSearchOption(ISearchOption searchOption)
        {
            if (searchOption == null)
                return;
            ReturnCount = searchOption.ReturnCount;
            StartingRecord = searchOption.StartingRecord;
            Allow7DigitMatch = searchOption.Allow7DigitMatch;
            HSort = searchOption.HSort;
        }
    }
}
