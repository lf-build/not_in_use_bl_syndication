﻿namespace LendFoundry.Syndication.LexisNexis.BusinessSearch.ServiceReference
{
    public partial class TopBusinessSearchBy
    {
        public TopBusinessSearchBy()
        { }
        public TopBusinessSearchBy(ISearchRequest searchBy)
        {
            if (searchBy == null)
                return;
            CompanyName = searchBy.CompanyName;
            Address = new Address
            {
                City = searchBy.Address.City,
                County = searchBy.Address.County,
                PostalCode = searchBy.Address.PostalCode,
                State = searchBy.Address.State,
                StateCityZip = searchBy.Address.StateCityZip,
                StreetAddress1 = searchBy.Address.StreetAddress1,
                StreetAddress2 = searchBy.Address.StreetAddress2,
                StreetName = searchBy.Address.StreetName,
                StreetNumber = searchBy.Address.StreetNumber,
                StreetPostDirection = searchBy.Address.StreetPostDirection,
                StreetPreDirection = searchBy.Address.StreetPreDirection,
                StreetSuffix = searchBy.Address.StreetSuffix,
                UnitDesignation = searchBy.Address.UnitDesignation,
                UnitNumber = searchBy.Address.UnitNumber,
                Zip4 = searchBy.Address.Zip4,
                Zip5 = searchBy.Address.Zip5
            };
            
            Radius = searchBy.Radius;
            Phone10 = searchBy.Phone10;
            TIN = searchBy.Tin;
            SSN = searchBy.Ssn;
            URL = searchBy.Url;
            Email = searchBy.Email;
            Name = new Name
            {
                First = searchBy.Name.First,
                Full = searchBy.Name.Full,
                Last = searchBy.Name.Last,
                Middle = searchBy.Name.Middle,
                Prefix = searchBy.Name.Prefix,
                Suffix = searchBy.Name.Suffix
            };
            SIC = searchBy.Sic;
            SeleID = searchBy.SeleId;
        }
    }
}
