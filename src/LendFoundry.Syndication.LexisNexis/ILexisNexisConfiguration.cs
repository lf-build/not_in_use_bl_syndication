﻿namespace LendFoundry.Syndication.LexisNexis
{
    public interface ILexisNexisConfiguration
    {
        FlexIdServiceConfiguration FlexId { get; set; }
        FraudPointServiceConfiguration FraudPoint { get; set; }
        InstantIdServiceConfiguration InstantId { get; set; }
        KbaServiceConfiguration InstantIdKba { get; set; }
        BankruptcyConfiguration Bankruptcy { get; set; }
    }
}