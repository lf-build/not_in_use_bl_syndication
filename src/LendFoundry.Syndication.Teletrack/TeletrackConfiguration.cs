﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Teletrack
{
    public class TeletrackConfiguration : ITeletrackConfiguration
    {
        public TeletrackConfiguration()
        {

            RequestTypes = new List<IRequestTypeConfiguration>()
            {
                new RequestTypeConfiguration()
                {
                    RequestType ="Inquiry"
                },
                new RequestTypeConfiguration()
                {
                    RequestType ="Third Party Data",
                    RequestOptions = new List<IRequestOption>()
                    {
                       new RequestOption(){ Name= "Vendor",Value = "ID Analytics" },
                       new RequestOption()  { Name = "Product",Value = "ID Verify" },
                       new RequestOption()   { Name = "SelectOption",Value= "IDScore" },
                         new RequestOption()  { Name =  "SelectOption",Value= "IDScoreAction" }
                     }
                 }
                ,
                new RequestTypeConfiguration()
                {
                    RequestType ="OFAC"
                },

            };
        }

        public string inquiryurl { get; set; } = "https://xmltest.teletrack.com/inquiry.asmx?wsdl";
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UsernameTokenId { get; set; } = "SecurityToken-ea4855e0-675b-4ac5-a1c1-e170bac5f1eb";
        public string MustUnderstand { get; set; } = "1";
        public string Wsu { get; set; } = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wsswssecurity-utility-1.0.xsd";
        public string Tns { get; set; } = "http://xml.teletrack.com/transaction";
        public string Soapenc { get; set; } = "http://schemas.xmlsoap.org/soap/encoding/";
        public string Xsd { get; set; } = "http://www.w3.org/2001/XMLSchema";
        public string Xsi { get; set; } = "http://www.w3.org/2001/XMLSchema-instance";
        public string Wsse { get; set; } = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
        public string s0 { get; set; } = "http://xml.teletrack.com/globals";
        public string TeletrackXMLVersion { get; set; } = "1.3.0";
        public string VendorSoftwareID { get; set; } = "YourSoftwareName";
        public string VersionNumber { get; set; } = "2.3.0";
        public string SubscriberID { get; set; } = "44458";
        [JsonConverter(typeof(InterfaceListConverter<IRequestTypeConfiguration, RequestTypeConfiguration>))]
        public List<IRequestTypeConfiguration> RequestTypes { get; set; }
        public string Soapenv { get; set; } = "http://schemas.xmlsoap.org/soap/envelope/";
        public string SoapAction { get; set; } = "http://xml.teletrack.com/transaction/GetData";
    }
}
