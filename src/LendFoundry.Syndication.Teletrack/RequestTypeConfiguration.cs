﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Teletrack
{
    public class RequestTypeConfiguration :IRequestTypeConfiguration
    {
        public string RequestType { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IRequestOption, RequestOption>))]
        public List<IRequestOption> RequestOptions { get; set; }
    }

}
