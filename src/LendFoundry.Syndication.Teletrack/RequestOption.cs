﻿namespace LendFoundry.Syndication.Teletrack
{
    public class RequestOption :IRequestOption
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
