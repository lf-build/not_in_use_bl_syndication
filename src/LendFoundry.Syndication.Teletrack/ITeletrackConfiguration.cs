﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Teletrack
{
    public interface ITeletrackConfiguration
    {
        string inquiryurl { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string UsernameTokenId { get; set; }
        string MustUnderstand { get; set; }

        string Tns { get; set; }
        string Wsu { get; set; }
        string Soapenc { get; set; }
        string Xsd { get; set; }
        string Xsi { get; set; }
        string Wsse { get; set; }

        string s0 { get; set; }
        string TeletrackXMLVersion { get; set; }
        string VendorSoftwareID { get; set; }
        string VersionNumber { get; set; }
        string SubscriberID { get; set; }
       
        string Soapenv { get; set; }
        List<IRequestTypeConfiguration> RequestTypes { get; set; }

        string SoapAction { get; set; } 
    }
}
