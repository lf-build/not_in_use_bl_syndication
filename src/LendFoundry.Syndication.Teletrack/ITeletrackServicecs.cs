﻿using LendFoundry.Syndication.Teletrack.Inquiry;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Teletrack
{
    public interface ITeletrackService
    {
        Task<GetDataResponse> GetData(string entityType, string entityId, IInquiryRequest request);
    }
}
