﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Teletrack
{
   public interface IRequestTypeConfiguration
    {
         string RequestType { get; set; }
        List<IRequestOption> RequestOptions { get; set; }
    }
}
