﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Teletrack;
using LendFoundry.Syndication.Teletrack.Inquiry;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Teletrack
{
    public class TeletrackService :ITeletrackService
    {
        public TeletrackService(ITeletrackConfiguration configuration, IInquiryProxy proxy, ILookupService lookup, IEventHubClient eventHub, ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            Configuration = configuration;
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
        }

        private ITeletrackConfiguration Configuration { get; }
        private IInquiryProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public static string ServiceName { get; } = "Teletrack";
        public ILogger Logger { get; set; }

    
        public async Task<GetDataResponse> GetData(string entityType, string entityId, IInquiryRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
               
                var response = await Proxy.GetData(request);
                await EventHub.Publish(new TeletrackInquirySent
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = request,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return response;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new TeletrackInquiryFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception,
                    Request = request,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
