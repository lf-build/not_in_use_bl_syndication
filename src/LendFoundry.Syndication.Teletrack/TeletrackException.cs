﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Teletrack
{

    [Serializable]
    public class TeletrackException : Exception
    {
        public TeletrackException()
        {
        }

        public TeletrackException(string message) : base(message)
        {
        }

        public TeletrackException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TeletrackException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
