﻿namespace LendFoundry.Syndication.Teletrack
{
    public  interface IRequestOption
    {
         string Name { get; set; }

         string Value { get; set; }
    }
}
