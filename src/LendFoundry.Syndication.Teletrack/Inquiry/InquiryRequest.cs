﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Teletrack.Inquiry
{
    public class InquiryRequest :IInquiryRequest
    {
        
        public string Uin { get; set; }
    
        public string FirstName { get; set; }
     
        public string MiddleName { get; set; }
     
        public string LastName { get; set; }

        public string BirthDate { get; set; }
        
        public string DriversLicenseState { get; set; }

        public string DriversLicenseNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }
        public string City { get; set; }

        public string State { get;set;}

        public string PostalCode { get; set; }



        public string Country { get; set; }
        
        public string  MobilePhone { get; set; }

        public string HomePhone { get; set; }

        public string WorkPhone { get; set; }

        public string EmployerName { get; set; }
        public string EmployerMobilePhone { get; set; }

        public string EmployerHomePhone { get; set; }

        public string EmployerWorkPhone { get; set; }

        public string EmployerAddressLine1 { get; set; }

        public string EmployerAddressLine2 { get; set; }
        public string EmployerCity { get; set; }

        public string EmployerState { get; set; }

        public string EmployerPostalCode { get; set; }

        public List<string> EmailAddresses { get; set; }

        public string SalaryAmount { get; set; }

        public string PaymentFrequency { get; set; }


        public string NumberOfTransactions { get; set; }

        public string NumberOfNSFs { get; set; }
        public string Balance { get; set; }
        public string BankName { get; set; }
        public string BankPhoneNumber { get; set; }

        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string LengthOfResidency { get; set; }

        public string OwnOrRent { get; set; }
        public string PreviousCustomer { get; set; }
        public string PayrollGarnishment { get; set; }
        public string CurrentBankruptcy { get; set; }
    }
}
