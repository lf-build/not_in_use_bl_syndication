﻿
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Request
{
    [XmlRoot(ElementName = "UsernameToken", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
    public partial class UsernameToken
    {
        [XmlElement(ElementName = "Username", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
        public string Username { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
        public string Password { get; set; }
        [XmlAttribute(AttributeName = "Id", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wsswssecurity-utility-1.0.xsd")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "Security", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
    public partial class Security
    {
        [XmlElement(ElementName = "UsernameToken", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
        public UsernameToken UsernameToken { get; set; }
        [XmlAttribute(AttributeName = "mustUnderstand", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string MustUnderstand { get; set; }
        [XmlAttribute(AttributeName = "wsu", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Wsu { get; set; }
        [XmlAttribute(AttributeName = "soapenc", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenc { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "wsse", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Wsse { get; set; }
    }

    [XmlRoot(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Header
    {
        [XmlElement(ElementName = "Security", Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")]
        public Security Security { get; set; }
    }

    [XmlRoot(ElementName = "Software", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Software
    {
        [XmlAttribute(AttributeName = "VendorSoftwareID")]
        public string VendorSoftwareID { get; set; }
        [XmlAttribute(AttributeName = "VersionNumber")]
        public string VersionNumber { get; set; }
    }

    [XmlRoot(ElementName = "Subscriber", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Subscriber
    {
        [XmlAttribute(AttributeName = "SubscriberID")]
        public string SubscriberID { get; set; }
        [XmlAttribute(AttributeName = "UserName")]
        public string UserName { get; set; }
    }

    [XmlRoot(ElementName = "Request", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Request
    {
        [XmlAttribute(AttributeName = "RequestType")]
        public string RequestType { get; set; }
        [XmlElement(ElementName = "RequestOption", Namespace = "http://xml.teletrack.com/globals")]
        public List<RequestOption> RequestOption { get; set; }
    }

    [XmlRoot(ElementName = "RequestOption", Namespace = "http://xml.teletrack.com/globals")]
    public partial class RequestOption
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "RequestDetails", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class RequestDetails
    {
        [XmlElement(ElementName = "Request", Namespace = "http://xml.teletrack.com/transaction")]
        public List<Request> Request { get; set; }
    }

    [XmlRoot(ElementName = "Uin", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Uin
    {
        [XmlAttribute(AttributeName = "Uin")]
        public string _Uin { get; set; }
    }

    [XmlRoot(ElementName = "Name", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Name
    {
        [XmlAttribute(AttributeName = "FirstName")]
        public string FirstName { get; set; }
        [XmlAttribute(AttributeName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlAttribute(AttributeName = "LastName")]
        public string LastName { get; set; }
    }

    [XmlRoot(ElementName = "DriversLicense", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class DriversLicense
    {
        [XmlAttribute(AttributeName = "State")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
    }

    [XmlRoot(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
    public partial class SimpleAddress
    {
        [XmlAttribute(AttributeName = "Line1")]
        public string Line1 { get; set; }
        [XmlAttribute(AttributeName = "City")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "State")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlAttribute(AttributeName = "Line2")]
        public string Line2 { get; set; }
    }

    [XmlRoot(ElementName = "Address", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Address
    {
        [XmlElement(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
        public SimpleAddress SimpleAddress { get; set; }
        [XmlAttribute(AttributeName = "Country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "Phone", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Phone
    {
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Phones", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Phones
    {
        [XmlElement(ElementName = "Phone", Namespace = "http://xml.teletrack.com/transaction")]
        public List<Phone> Phone { get; set; }
    }

    [XmlRoot(ElementName = "EmployerAddress", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class EmployerAddress
    {
        [XmlElement(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
        public SimpleAddress SimpleAddress { get; set; }
    }

    [XmlRoot(ElementName = "EmployerPhone", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class EmployerPhone
    {
        [XmlAttribute(AttributeName = "Number")]
        public string Number { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Salary", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Salary
    {
        [XmlAttribute(AttributeName = "Amount")]
        public string Amount { get; set; }
        [XmlAttribute(AttributeName = "PaymentFrequency")]
        public string PaymentFrequency { get; set; }
    }

    [XmlRoot(ElementName = "Employer", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Employer
    {
        [XmlElement(ElementName = "EmployerAddress", Namespace = "http://xml.teletrack.com/transaction")]
        public EmployerAddress EmployerAddress { get; set; }
        [XmlElement(ElementName = "EmployerPhone", Namespace = "http://xml.teletrack.com/transaction")]
        public List<EmployerPhone> EmployerPhone { get; set; }
        [XmlElement(ElementName = "Salary", Namespace = "http://xml.teletrack.com/transaction")]
        public Salary Salary { get; set; }
        [XmlAttribute(AttributeName = "EmployerName")]
        public string EmployerName { get; set; }
    }

    [XmlRoot(ElementName = "Statement", Namespace = "http://xml.teletrack.com/globals")]
    public partial class Statement
    {
        [XmlAttribute(AttributeName = "NumberOfTransactions")]
        public string NumberOfTransactions { get; set; }
        [XmlAttribute(AttributeName = "NumberOfNSFs")]
        public string NumberOfNSFs { get; set; }
        [XmlAttribute(AttributeName = "Balance")]
        public string Balance { get; set; }
    }

    [XmlRoot(ElementName = "BankAccount", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class BankAccount
    {
        [XmlElement(ElementName = "Statement", Namespace = "http://xml.teletrack.com/globals")]
        public Statement Statement { get; set; }
        [XmlAttribute(AttributeName = "BankName")]
        public string BankName { get; set; }
        [XmlAttribute(AttributeName = "BankPhoneNumber")]
        public string BankPhoneNumber { get; set; }
        [XmlAttribute(AttributeName = "RoutingNumber")]
        public string RoutingNumber { get; set; }
        [XmlAttribute(AttributeName = "AccountNumber")]
        public string AccountNumber { get; set; }
        [XmlAttribute(AttributeName = "AccountType")]
        public string AccountType { get; set; }
    }

    [XmlRoot(ElementName = "BankAccounts", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class BankAccounts
    {
        [XmlElement(ElementName = "BankAccount", Namespace = "http://xml.teletrack.com/transaction")]
        public BankAccount BankAccount { get; set; }
    }

    [XmlRoot(ElementName = "EmailAddress", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class EmailAddress
    {
        [XmlAttribute(AttributeName = "EmailAddress")]
        public string _EmailAddress { get; set; }
    }

    [XmlRoot(ElementName = "EmailAddresses", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class EmailAddresses
    {
        [XmlElement(ElementName = "EmailAddress", Namespace = "http://xml.teletrack.com/transaction")]
        public List<EmailAddress> EmailAddress { get; set; }
    }

    [XmlRoot(ElementName = "ApplicantCredit", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class ApplicantCredit
    {
        [XmlAttribute(AttributeName = "LengthOfResidency")]
        public string LengthOfResidency { get; set; }
        [XmlAttribute(AttributeName = "OwnOrRent")]
        public string OwnOrRent { get; set; }
        [XmlAttribute(AttributeName = "PreviousCustomer")]
        public string PreviousCustomer { get; set; }
        [XmlAttribute(AttributeName = "PayrollGarnishment")]
        public string PayrollGarnishment { get; set; }
        [XmlAttribute(AttributeName = "CurrentBankruptcy")]
        public string CurrentBankruptcy { get; set; }
    }

    [XmlRoot(ElementName = "Applicant", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class Applicant
    {
        [XmlElement(ElementName = "Uin", Namespace = "http://xml.teletrack.com/transaction")]
        public Uin Uin { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "http://xml.teletrack.com/transaction")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "BirthDate", Namespace = "http://xml.teletrack.com/transaction")]
        public string BirthDate { get; set; }
        [XmlElement(ElementName = "DriversLicense", Namespace = "http://xml.teletrack.com/transaction")]
        public DriversLicense DriversLicense { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://xml.teletrack.com/transaction")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "Phones", Namespace = "http://xml.teletrack.com/transaction")]
        public Phones Phones { get; set; }
        [XmlElement(ElementName = "Employer", Namespace = "http://xml.teletrack.com/transaction")]
        public Employer Employer { get; set; }
        [XmlElement(ElementName = "BankAccounts", Namespace = "http://xml.teletrack.com/transaction")]
        public BankAccounts BankAccounts { get; set; }
        [XmlElement(ElementName = "EmailAddresses", Namespace = "http://xml.teletrack.com/transaction")]
        public EmailAddresses EmailAddresses { get; set; }
        [XmlElement(ElementName = "ApplicantCredit", Namespace = "http://xml.teletrack.com/transaction")]
        public ApplicantCredit ApplicantCredit { get; set; }
    }

    [XmlRoot(ElementName = "TransactionRequest", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class TransactionRequest
    {
        [XmlElement(ElementName = "Software", Namespace = "http://xml.teletrack.com/transaction")]
        public Software Software { get; set; }
        [XmlElement(ElementName = "Subscriber", Namespace = "http://xml.teletrack.com/transaction")]
        public Subscriber Subscriber { get; set; }
        [XmlElement(ElementName = "RequestDetails", Namespace = "http://xml.teletrack.com/transaction")]
        public RequestDetails RequestDetails { get; set; }
        [XmlElement(ElementName = "Applicant", Namespace = "http://xml.teletrack.com/transaction")]
        public Applicant Applicant { get; set; }
        [XmlAttribute(AttributeName = "Timestamp")]
        public string Timestamp { get; set; }
        [XmlAttribute(AttributeName = "TeletrackXMLVersion")]
        public string TeletrackXMLVersion { get; set; }
    }

    [XmlRoot(ElementName = "GetData", Namespace = "http://xml.teletrack.com/transaction")]
    public partial class GetData
    {
        [XmlElement(ElementName = "TransactionRequest", Namespace = "http://xml.teletrack.com/transaction")]
        public TransactionRequest TransactionRequest { get; set; }
        [XmlAttribute(AttributeName = "tns", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tns { get; set; }
        [XmlAttribute(AttributeName = "soapenc", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenc { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "s0", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S0 { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Body
    {
        [XmlElement(ElementName = "GetData", Namespace = "http://xml.teletrack.com/transaction")]
        public GetData GetData { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Envelope
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Header Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
    }
}
