﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Request
{
    public partial class Envelope
    {
        public Envelope()
        {

        }
        public Envelope(ITeletrackConfiguration configuration, IInquiryRequest request)
        {
            Header = new Header(configuration);
            Body = new Body(configuration, request);
            Soapenv = configuration.Soapenv;
        }
    }
    public partial class ApplicantCredit
    {
        public ApplicantCredit()
        {

        }
        public ApplicantCredit(IInquiryRequest request)
        {
            LengthOfResidency = request.LengthOfResidency;
            OwnOrRent = request.OwnOrRent;
            PreviousCustomer = request.PreviousCustomer;
            PayrollGarnishment = request.PayrollGarnishment;
            CurrentBankruptcy = request.CurrentBankruptcy;
        }
    }

    public partial class Salary
    {
        public Salary()
        {

        }
        public Salary(IInquiryRequest request)
        {
            Amount = request.SalaryAmount;
            PaymentFrequency = request.PaymentFrequency;
        }
    }
    public partial class Body
    {
        public Body()
        {

        }
        public Body(ITeletrackConfiguration configuration, IInquiryRequest request)
        {
            GetData = new GetData(configuration, request);
        }
    }
    public partial class GetData
    {
        public GetData()
        {

        }
        public GetData(ITeletrackConfiguration configuration, IInquiryRequest request)
        {
            TransactionRequest = new TransactionRequest(configuration, request);
            Tns = configuration.Tns;
            Soapenc = configuration.Soapenc;
            Xsd = configuration.Xsd;
            Xsi = configuration.Xsi;
            S0 = configuration.s0;
        }
    }
    public partial class TransactionRequest
    {
        public TransactionRequest()
        {

        }
        public TransactionRequest(ITeletrackConfiguration configuration,IInquiryRequest  request)
        {
            Software = new Software(configuration);
            Subscriber = new Subscriber(configuration);
            RequestDetails = new RequestDetails(configuration);
            Applicant = new Applicant(request);
            TeletrackXMLVersion = configuration.TeletrackXMLVersion;
        }
    }
    public partial class Name
    {
        public Name()
        {

        }
        public Name(string firstName,string middleName,string lastName)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
        }
    }
    public partial class Employer
    {
        public Employer()
        {

        }
        public Employer(IInquiryRequest request)
        {

            EmployerName = request.EmployerName;
            if(!string.IsNullOrEmpty(request.EmployerPostalCode))
            EmployerAddress = new EmployerAddress(request);
            
            var employerphone = new List<EmployerPhone>();
            if (!string.IsNullOrEmpty(request.EmployerMobilePhone))
                employerphone.Add(new EmployerPhone(request.EmployerMobilePhone, "Mobile"));
            if (!string.IsNullOrEmpty(request.EmployerHomePhone))
                employerphone.Add(new EmployerPhone(request.EmployerHomePhone, "Home"));
            if (!string.IsNullOrEmpty(request.EmployerWorkPhone))
                employerphone.Add(new EmployerPhone(request.EmployerWorkPhone, "Work"));
            if(!string.IsNullOrEmpty(request.PaymentFrequency))
            Salary = new Salary(request);
            if(employerphone.Count>0)
            EmployerPhone = employerphone;

        }
    }
    public partial class BankAccounts
    {
        public BankAccounts()
        {

        }
        public BankAccounts(IInquiryRequest request)
        {
            BankAccount = new BankAccount(request);
        }
    }
    public partial class BankAccount
    {
        public BankAccount()
        {

        }
        public BankAccount(IInquiryRequest request)
        {
            Statement = new Statement(request.NumberOfTransactions,request.NumberOfNSFs,request.Balance);
            BankName = request.BankName;
            BankPhoneNumber = request.BankPhoneNumber;
            RoutingNumber = request.RoutingNumber;
            AccountNumber = request.AccountNumber;
            AccountType = request.AccountType;
        }
    }
    public partial class Statement
    {
        public Statement()
        {

        }
        public Statement(string numberOfTransactions,string numberOfNSFs, string balance)
        {
            NumberOfTransactions = numberOfTransactions;
            NumberOfNSFs = numberOfNSFs;
            Balance = balance;
        }
    }
    public partial class EmployerPhone
    {
        public EmployerPhone()
        {

        }
        public EmployerPhone(string number,string type)
        {
            Number = number;
            Type = type;
        }
    }
    public partial class Applicant
    {
        public Applicant()
        {

        }
        public Applicant(IInquiryRequest request)
        {
            Uin = new Uin(request.Uin);
            Name = new Name(request.FirstName, request.MiddleName, request.LastName);
            BirthDate = request.BirthDate;
            DriversLicense = new DriversLicense(request.DriversLicenseState, request.DriversLicenseNumber);
            Address = new Address(request);
            Phones = new Phones(request.MobilePhone, request.HomePhone, request.WorkPhone);
           Employer = new Employer(request);
            EmailAddresses = new EmailAddresses(request);

        }
    }
    public partial class EmailAddresses
    {
        public EmailAddresses()
        {

        }
        public EmailAddresses(IInquiryRequest request)
        {
            EmailAddress = request?.EmailAddresses?.Select(i=>new EmailAddress(i)).ToList();
        }
    }
    public partial class EmailAddress
    {
        public EmailAddress()
        {

        }
        public EmailAddress(string emailAddress)
        {
            _EmailAddress = emailAddress;
        }
    }
    public partial class DriversLicense
    {
        public DriversLicense()
        {

        }
        public DriversLicense(string state,string number)
        {
            State = state;
            Number = number;
        }
    }
    public partial class Uin
    {
        public Uin()
        {

        }
        public Uin(string uin)
        {
            _Uin = uin;
        }
    }
    public partial class Phone
    {
        public Phone()
        {

        }
        public Phone(string number, string type)
        {
            Type = type;
            Number = number;
        }
    }
    public partial class Phones
    {
        public Phones()
        {

        }
        public Phones(string mobilephone,string homephone,string workphone)
        {
            var phones = new List<Phone>();
            if (!string.IsNullOrEmpty(mobilephone))
                phones.Add(new Phone(mobilephone, "Mobile"));
            if (!string.IsNullOrEmpty(homephone))
                phones.Add(new Phone(homephone, "Home"));
            if (!string.IsNullOrEmpty(workphone))
                phones.Add(new Phone(workphone, "Work"));
            Phone = phones;
        }
    }
    public partial class Address
    {
        public Address()
        {

        }
        public Address(IInquiryRequest request)
        {
            SimpleAddress = new SimpleAddress(request.AddressLine1,request.AddressLine2,request.City,request.State,request.PostalCode);
            Country = request.Country;
        }
    }
    public partial class EmployerAddress
    {
        public EmployerAddress()
        {

        }
        public EmployerAddress(IInquiryRequest request)
        {
            SimpleAddress = new SimpleAddress(request.EmployerAddressLine1, request.EmployerAddressLine2, request.EmployerCity, request.EmployerState, request.EmployerPostalCode);
        }
    }
    public partial class SimpleAddress
    {
        public SimpleAddress()
        {

        }
        public SimpleAddress(string line1, string line2, string city, string state, string postalCode)
        {
            Line1 = line1;
            Line2 = line2;
            City = city;
            State = state;
            PostalCode = postalCode;
        }
    }
    public partial class RequestDetails
    {
        public RequestDetails()
        {

        }
        public RequestDetails(ITeletrackConfiguration configuration)
        {
            Request = configuration.RequestTypes.Select(i => new Request(i)).ToList();
        }
    }
    public partial class Request
    {
        public Request()
        {

        }
        public Request(IRequestTypeConfiguration configuration)
        {
            RequestType = configuration.RequestType;
           if(configuration.RequestOptions!=null)
            RequestOption = configuration.RequestOptions.Select(i => new RequestOption(i.Name, i.Value)).ToList();
        }
    }
    public partial class RequestOption
    {
        public RequestOption()
        {

        }
        public RequestOption(string name,string value )
        {
            Name = name;
            Value = value;
        }
    }
    public partial class Subscriber
    {
        public Subscriber()
        {

        }
        public Subscriber(ITeletrackConfiguration configuration)
        {
            SubscriberID = configuration.SubscriberID;
            UserName = configuration.UserName;
        }
    }
    public partial class Software
    {
        public Software()
        {

        }
        public Software(ITeletrackConfiguration configuration)
        {

            VendorSoftwareID = configuration.VendorSoftwareID;
            VersionNumber = configuration.VersionNumber;
        }
    }
    public partial class Header
    {
        public Header()
        {

        }
        public Header(ITeletrackConfiguration configuration)
        {
            Security = new Security(configuration);
        }
    }
    public partial class Security
    {
        public Security()
        {

        }
        public Security(ITeletrackConfiguration configuration)
        {
            UsernameToken = new UsernameToken(configuration);
            MustUnderstand = configuration.MustUnderstand;
            Wsu = configuration.Wsu;
            Soapenc = configuration.Soapenc;
            Xsd = configuration.Xsd;
            Xsi = configuration.Xsi;
            Wsse = configuration.Wsse;
         }
    }
    public partial class UsernameToken
    {
        public UsernameToken()
        {

        }
        public UsernameToken(ITeletrackConfiguration configuration)
        {
            Username = configuration.UserName;
            Password = configuration.Password;
            Id = configuration.UsernameTokenId;
        }
    }
}
