﻿using System.Threading.Tasks;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response;
using RestSharp;
using System;
using System.Xml.Linq;
using System.Linq;
using System.IO;
using System.Net;

namespace LendFoundry.Syndication.Teletrack.Inquiry.Proxy
{
    public class InquiryProxy :IInquiryProxy
    {
        public InquiryProxy(ITeletrackConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            TeletrackConfiguration = configuration;
        }
        private ITeletrackConfiguration TeletrackConfiguration { get; }
        public async Task<GetDataResponse> GetData(IInquiryRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            var client = new RestClient(TeletrackConfiguration.inquiryurl);
            var proxyrequest = new Request.Envelope(TeletrackConfiguration, request);
            var restrequest = new RestRequest(Method.POST);
            restrequest.AddHeader("Accept", "application/json");
            restrequest.AddHeader("SOAPAction", TeletrackConfiguration.SoapAction);
            restrequest.AddParameter("text/xml", XmlSerialization.Serialize(proxyrequest), ParameterType.RequestBody);
            var objectResponse = await client.ExecuteTaskAsync(restrequest);

            if (objectResponse.ResponseStatus != ResponseStatus.Completed || objectResponse.StatusCode != HttpStatusCode.OK)
                throw new Exception(objectResponse.StatusDescription);

            if (objectResponse == null)
                throw new ArgumentNullException(nameof(objectResponse));

            XDocument xDoc = XDocument.Load(new StringReader(objectResponse.Content));

            var unwrappedResponse = xDoc.Descendants((XNamespace)"http://schemas.xmlsoap.org/soap/envelope/" + "Body")
                .First()
                .FirstNode;
          
            return XmlSerialization.Deserialize<GetDataResponse>(unwrappedResponse.ToString());
        }
      
     
    }
}
