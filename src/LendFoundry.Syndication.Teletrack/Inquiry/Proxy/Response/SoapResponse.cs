﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response
{
    [XmlRoot(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
    public class SimpleAddress
    {
        [XmlAttribute(AttributeName = "Line1")]
        public string Line1 { get; set; }
        [XmlAttribute(AttributeName = "Line2")]
        public string Line2 { get; set; }
        [XmlAttribute(AttributeName = "City")]
        public string City { get; set; }
        [XmlAttribute(AttributeName = "State")]
        public string State { get; set; }
        [XmlAttribute(AttributeName = "PostalCode")]
        public string PostalCode { get; set; }
    }

    [XmlRoot(ElementName = "SubscriberAddress", Namespace = "http://xml.teletrack.com/globals")]
    public class SubscriberAddress
    {
        [XmlElement(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
        public SimpleAddress SimpleAddress { get; set; }
    }

    [XmlRoot(ElementName = "Subscriber", Namespace = "http://xml.teletrack.com/transaction")]
    public class Subscriber
    {
        [XmlElement(ElementName = "SubscriberAddress", Namespace = "http://xml.teletrack.com/globals")]
        public SubscriberAddress SubscriberAddress { get; set; }
        [XmlAttribute(AttributeName = "SubscriberID")]
        public string SubscriberID { get; set; }
        [XmlAttribute(AttributeName = "SubscriberName")]
        public string SubscriberName { get; set; }
        [XmlAttribute(AttributeName = "ContactName")]
        public string ContactName { get; set; }
        [XmlAttribute(AttributeName = "UserName")]
        public string UserName { get; set; }
        [XmlAttribute(AttributeName = "tt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tt { get; set; }
    }

    [XmlRoot(ElementName = "Request", Namespace = "http://xml.teletrack.com/transaction")]
    public class Request
    {
        [XmlAttribute(AttributeName = "RequestType")]
        public string RequestType { get; set; }
        [XmlElement(ElementName = "RequestOption", Namespace = "http://xml.teletrack.com/globals")]
        public List<RequestOption> RequestOption { get; set; }
    }

    [XmlRoot(ElementName = "RequestOption", Namespace = "http://xml.teletrack.com/globals")]
    public class RequestOption
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "RequestDetails", Namespace = "http://xml.teletrack.com/transaction")]
    public class RequestDetails
    {
        [XmlElement(ElementName = "Request", Namespace = "http://xml.teletrack.com/transaction")]
        public List<Request> Request { get; set; }
        [XmlAttribute(AttributeName = "tt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tt { get; set; }
    }

    [XmlRoot(ElementName = "Uin", Namespace = "http://xml.teletrack.com/transaction")]
    public class Uin
    {
        [XmlAttribute(AttributeName = "Uin")]
        public string _Uin { get; set; }
    }

    [XmlRoot(ElementName = "Name", Namespace = "http://xml.teletrack.com/transaction")]
    public class Name
    {
        [XmlAttribute(AttributeName = "FirstName")]
        public string FirstName { get; set; }
        [XmlAttribute(AttributeName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlAttribute(AttributeName = "LastName")]
        public string LastName { get; set; }
    }

    [XmlRoot(ElementName = "Address", Namespace = "http://xml.teletrack.com/transaction")]
    public class Address
    {
        [XmlElement(ElementName = "SimpleAddress", Namespace = "http://xml.teletrack.com/globals")]
        public SimpleAddress SimpleAddress { get; set; }
        [XmlAttribute(AttributeName = "Country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "AddressList", Namespace = "http://xml.teletrack.com/transaction")]
    public class AddressList
    {
        [XmlElement(ElementName = "Address", Namespace = "http://xml.teletrack.com/transaction")]
        public Address Address { get; set; }
    }

    [XmlRoot(ElementName = "Employer", Namespace = "http://xml.teletrack.com/transaction")]
    public class Employer
    {
        [XmlAttribute(AttributeName = "EmployerName")]
        public string EmployerName { get; set; }
    }

    [XmlRoot(ElementName = "EmploymentHistory", Namespace = "http://xml.teletrack.com/transaction")]
    public class EmploymentHistory
    {
        [XmlElement(ElementName = "Employer", Namespace = "http://xml.teletrack.com/transaction")]
        public Employer Employer { get; set; }
    }

    [XmlRoot(ElementName = "Inquiry", Namespace = "http://xml.teletrack.com/transaction")]
    public class Inquiry
    {
        [XmlAttribute(AttributeName = "SubscriberID")]
        public string SubscriberID { get; set; }
        [XmlAttribute(AttributeName = "SubscriberName")]
        public string SubscriberName { get; set; }
        [XmlAttribute(AttributeName = "SubscriberCity")]
        public string SubscriberCity { get; set; }
        [XmlAttribute(AttributeName = "SubscriberState")]
        public string SubscriberState { get; set; }
        [XmlAttribute(AttributeName = "SubscriberZip")]
        public string SubscriberZip { get; set; }
        [XmlAttribute(AttributeName = "SubscriberPhone")]
        public string SubscriberPhone { get; set; }
        [XmlAttribute(AttributeName = "InquiryDateTime")]
        public string InquiryDateTime { get; set; }
        [XmlAttribute(AttributeName = "IndustryCode")]
        public string IndustryCode { get; set; }
        [XmlAttribute(AttributeName = "ChildIndustryCode")]
        public string ChildIndustryCode { get; set; }
        [XmlAttribute(AttributeName = "IsInDispute")]
        public string IsInDispute { get; set; }
    }

    [XmlRoot(ElementName = "PreviousInquiries", Namespace = "http://xml.teletrack.com/transaction")]
    public class PreviousInquiries
    {
        [XmlElement(ElementName = "Inquiry", Namespace = "http://xml.teletrack.com/transaction")]
        public List<Inquiry> Inquiry { get; set; }
    }

    [XmlRoot(ElementName = "PublicRecordData", Namespace = "http://xml.teletrack.com/transaction")]
    public class PublicRecordData
    {
        [XmlElement(ElementName = "Evictions", Namespace = "http://xml.teletrack.com/transaction")]
        public string Evictions { get; set; }
        [XmlElement(ElementName = "Bankruptcies", Namespace = "http://xml.teletrack.com/transaction")]
        public string Bankruptcies { get; set; }
    }

    [XmlRoot(ElementName = "UINValidation", Namespace = "http://xml.teletrack.com/transaction")]
    public class UINValidation
    {
        [XmlAttribute(AttributeName = "RecentIssue")]
        public string RecentIssue { get; set; }
        [XmlAttribute(AttributeName = "Deceased")]
        public string Deceased { get; set; }
    }

    [XmlRoot(ElementName = "ScoreReasonCode", Namespace = "http://xml.teletrack.com/globals")]
    public class ScoreReasonCode
    {
        [XmlAttribute(AttributeName = "ScoreCode")]
        public string ScoreCode { get; set; }
        [XmlAttribute(AttributeName = "Description")]
        public string Description { get; set; }
    }

    [XmlRoot(ElementName = "TeletrackScore", Namespace = "http://xml.teletrack.com/transaction")]
    public class TeletrackScore
    {
        [XmlElement(ElementName = "ScoreReasonCode", Namespace = "http://xml.teletrack.com/globals")]
        public List<ScoreReasonCode> ScoreReasonCode { get; set; }
        [XmlAttribute(AttributeName = "Range")]
        public string Range { get; set; }
        [XmlAttribute(AttributeName = "Score")]
        public string Score { get; set; }
        [XmlAttribute(AttributeName = "RecordType")]
        public string RecordType { get; set; }
    }

    [XmlRoot(ElementName = "Scores", Namespace = "http://xml.teletrack.com/transaction")]
    public class Scores
    {
        [XmlElement(ElementName = "TeletrackScore", Namespace = "http://xml.teletrack.com/transaction")]
        public TeletrackScore TeletrackScore { get; set; }
    }

    [XmlRoot(ElementName = "ConsumerCreditReport", Namespace = "http://xml.teletrack.com/transaction")]
    public class ConsumerCreditReport
    {
        [XmlElement(ElementName = "BirthDate", Namespace = "http://xml.teletrack.com/transaction")]
        public string BirthDate { get; set; }
        [XmlElement(ElementName = "Uin", Namespace = "http://xml.teletrack.com/transaction")]
        public Uin Uin { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "http://xml.teletrack.com/transaction")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "AddressList", Namespace = "http://xml.teletrack.com/transaction")]
        public AddressList AddressList { get; set; }
        [XmlElement(ElementName = "EmploymentHistory", Namespace = "http://xml.teletrack.com/transaction")]
        public EmploymentHistory EmploymentHistory { get; set; }
        [XmlElement(ElementName = "ChargeOffs", Namespace = "http://xml.teletrack.com/transaction")]
        public string ChargeOffs { get; set; }
        [XmlElement(ElementName = "PreviousInquiries", Namespace = "http://xml.teletrack.com/transaction")]
        public PreviousInquiries PreviousInquiries { get; set; }
        [XmlElement(ElementName = "SkipGuards", Namespace = "http://xml.teletrack.com/transaction")]
        public string SkipGuards { get; set; }
        [XmlElement(ElementName = "PublicRecordData", Namespace = "http://xml.teletrack.com/transaction")]
        public PublicRecordData PublicRecordData { get; set; }
        [XmlElement(ElementName = "UINValidation", Namespace = "http://xml.teletrack.com/transaction")]
        public UINValidation UINValidation { get; set; }
        [XmlElement(ElementName = "Scores", Namespace = "http://xml.teletrack.com/transaction")]
        public Scores Scores { get; set; }
        [XmlElement(ElementName = "ConsumerStatements", Namespace = "http://xml.teletrack.com/transaction")]
        public string ConsumerStatements { get; set; }
        [XmlAttribute(AttributeName = "TransactionCode")]
        public string TransactionCode { get; set; }
        [XmlAttribute(AttributeName = "tt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tt { get; set; }
    }

    [XmlRoot(ElementName = "OutputRecord", Namespace = "http://idanalytics.com/products/idscore/result")]
    public class OutputRecord
    {
        [XmlElement(ElementName = "IDAStatus", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDAStatus { get; set; }
        [XmlElement(ElementName = "AppID", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string AppID { get; set; }
        [XmlElement(ElementName = "Designation", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string Designation { get; set; }
        [XmlElement(ElementName = "IDASequence", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDASequence { get; set; }
        [XmlElement(ElementName = "IDATimeStamp", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDATimeStamp { get; set; }
        [XmlElement(ElementName = "IDScore", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScore { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode1", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode1 { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode2", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode2 { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode3", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode3 { get; set; }
        [XmlAttribute(AttributeName = "schemaVersion")]
        public string SchemaVersion { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode5", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode5 { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
    public class Item
    {
        [XmlElement(ElementName = "OutputRecord", Namespace = "http://idanalytics.com/products/idscore/result")]
        public OutputRecord OutputRecord { get; set; }
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
    public class Body
    {
        [XmlElement(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "Response", Namespace = "http://idanalytics.com/core/api")]
    public class Response
    {
        [XmlElement(ElementName = "Solution", Namespace = "http://idanalytics.com/core/api")]
        public string Solution { get; set; }
        [XmlElement(ElementName = "RequestID", Namespace = "http://idanalytics.com/core/api")]
        public string RequestID { get; set; }
        [XmlElement(ElementName = "View", Namespace = "http://idanalytics.com/core/api")]
        public string View { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body2
    {
        [XmlElement(ElementName = "Response", Namespace = "http://idanalytics.com/core/api")]
        public Response Response { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body2 Body2 { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Response", Namespace = "http://xml.teletrack.com/transaction")]
    public class Response2
    {
        [XmlElement(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Envelope Envelope { get; set; }
        [XmlAttribute(AttributeName = "ErrorCode")]
        public string ErrorCode { get; set; }
    }

    [XmlRoot(ElementName = "VendorResponse", Namespace = "http://xml.teletrack.com/transaction")]
    public class VendorResponse
    {
        [XmlElement(ElementName = "Request", Namespace = "http://xml.teletrack.com/transaction")]
        public Request Request { get; set; }
        [XmlElement(ElementName = "Response", Namespace = "http://xml.teletrack.com/transaction")]
        public Response2 Response2 { get; set; }
        [XmlAttribute(AttributeName = "Vendor")]
        public string Vendor { get; set; }
    }

    [XmlRoot(ElementName = "ThirdPartyData", Namespace = "http://xml.teletrack.com/transaction")]
    public class ThirdPartyData
    {
        [XmlElement(ElementName = "VendorResponse", Namespace = "http://xml.teletrack.com/transaction")]
        public VendorResponse VendorResponse { get; set; }
        [XmlAttribute(AttributeName = "tt", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tt { get; set; }
    }

    [XmlRoot(ElementName = "TransactionResponse", Namespace = "http://xml.teletrack.com/transaction")]
    public class TransactionResponse
    {
        [XmlElement(ElementName = "Subscriber", Namespace = "http://xml.teletrack.com/transaction")]
        public Subscriber Subscriber { get; set; }
        [XmlElement(ElementName = "RequestDetails", Namespace = "http://xml.teletrack.com/transaction")]
        public RequestDetails RequestDetails { get; set; }
        [XmlElement(ElementName = "ConsumerCreditReport", Namespace = "http://xml.teletrack.com/transaction")]
        public ConsumerCreditReport ConsumerCreditReport { get; set; }
        [XmlElement(ElementName = "ThirdPartyData", Namespace = "http://xml.teletrack.com/transaction")]
        public ThirdPartyData ThirdPartyData { get; set; }
        [XmlAttribute(AttributeName = "TransactionDateTime")]
        public string TransactionDateTime { get; set; }
        [XmlAttribute(AttributeName = "TeletrackXMLVersion")]
        public string TeletrackXMLVersion { get; set; }
    }

    [XmlRoot(ElementName = "GetDataResponse", Namespace = "http://xml.teletrack.com/transaction")]
    public class GetDataResponse
    {
        [XmlElement(ElementName = "TransactionResponse", Namespace = "http://xml.teletrack.com/transaction")]
        public TransactionResponse TransactionResponse { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

}
