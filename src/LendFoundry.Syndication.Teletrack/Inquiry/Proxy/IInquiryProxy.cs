﻿using System.Threading.Tasks;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy.Response;
namespace LendFoundry.Syndication.Teletrack.Inquiry.Proxy
{
    public interface IInquiryProxy
    {
        Task<GetDataResponse> GetData(IInquiryRequest request);
    }
}
