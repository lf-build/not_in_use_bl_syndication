﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Teletrack.Inquiry
{
    public interface IInquiryRequest
    {
        string Uin { get; set; }

        string FirstName { get; set; }

        string MiddleName { get; set; }

        string LastName { get; set; }

        string BirthDate { get; set; }

        string DriversLicenseState { get; set; }

        string DriversLicenseNumber { get; set; }

        string AddressLine1 { get; set; }

         string AddressLine2 { get; set; }
         string City { get; set; }

         string State { get; set; }

         string PostalCode { get; set; }

        string Country { get; set; }

        string MobilePhone { get; set; }

        string HomePhone { get; set; }

        string WorkPhone { get; set; }

        string EmployerName { get; set; }
        string EmployerMobilePhone { get; set; }

        string EmployerHomePhone { get; set; }

        string EmployerWorkPhone { get; set; }

        string EmployerAddressLine1 { get; set; }

         string EmployerAddressLine2 { get; set; }
         string EmployerCity { get; set; }

         string EmployerState { get; set; }

         string EmployerPostalCode { get; set; }

        List<string> EmailAddresses { get; set; }
         string SalaryAmount { get; set; }

         string PaymentFrequency { get; set; }


         string NumberOfTransactions { get; set; }

         string NumberOfNSFs { get; set; }
         string Balance { get; set; }
         string BankName { get; set; }
         string BankPhoneNumber { get; set; }

         string RoutingNumber { get; set; }
         string AccountNumber { get; set; }
         string AccountType { get; set; }
         string LengthOfResidency { get; set; }

         string OwnOrRent { get; set; }
         string PreviousCustomer { get; set; }
         string PayrollGarnishment { get; set; }
         string CurrentBankruptcy { get; set; }
    }
}
