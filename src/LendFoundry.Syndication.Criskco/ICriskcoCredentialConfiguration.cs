﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco
{
   public interface ICriskcoCredentialConfiguration
    {
        string appId { get; set; }
         string appToken { get; set; }
    }
}
