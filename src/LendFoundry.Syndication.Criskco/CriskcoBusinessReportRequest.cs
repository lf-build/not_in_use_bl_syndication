﻿
namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoBusinessReportRequest
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string BusinessId { get; set; }      
    }
}
