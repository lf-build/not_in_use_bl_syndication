﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Criskco.Events;
using LendFoundry.Syndication.Criskco.Proxy;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoService : ICriskcoService
    {
        #region Public Constructors
        public CriskcoService(ICriskcoProxy proxy, IEventHubClient eventHub, ILogger logger, IDecisionEngineService decisionEngineService, ICriskcoConfiguration configuration)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (decisionEngineService == null)
                throw new ArgumentNullException(nameof(decisionEngineService));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            Proxy = proxy;
            EventHub = eventHub;
            DecisionEngineService = decisionEngineService;
            Configuration = configuration;
            Logger = logger;
        }

        #endregion Public Constructors

        #region Private Properties

        private IEventHubClient EventHub { get; }

        private ICriskcoProxy Proxy { get; }
        private IDecisionEngineService DecisionEngineService { get; }
        private ICriskcoConfiguration Configuration { get; }
        private ILogger Logger { get; }

        #endregion Private Properties

        #region Public Methods


        public async Task<Response.ICriskcoResponse> GetApproveBusiness(string entityType, string entityId, string bussinessId)
        {
            try
            {
                if (bussinessId == null)
                    throw new ArgumentNullException(nameof(bussinessId));
                if (entityType == null)
                    throw new ArgumentNullException(nameof(entityType));
                if (entityId == null)
                    throw new ArgumentNullException(nameof(entityId));

                var referencenumber = Guid.NewGuid().ToString("N");
                var response = await Task.Run(() => Proxy.GetApproveBusiness(bussinessId));
                var result = new Response.CriskcoResponse(response);
                await EventHub.Publish(new GetApproveBusinessRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = bussinessId,
                    ReferenceNumber = referencenumber
                });
                return result;
            }
            catch (Exception Ex)
            {
                await EventHub.Publish(new GetApproveBusinessRequestedFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = Ex.Message,
                    Request = bussinessId,
                    ReferenceNumber = null
                });
                throw ;
            }
        }

        public async Task ProcessEvent(EventInfo eventInfo)
        {
            Logger.Info($"Started processing for #{eventInfo.Name} event");
            var eventConfiguration = Configuration.events.Where(e => e.Name.Equals(eventInfo.Name)).First();
            if (eventConfiguration == null)
                throw new ArgumentException($"{nameof(eventConfiguration)}");
            string responseData = eventConfiguration.Response.FormatWith(eventInfo);
            object data = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseData);
            var ruleResult = DecisionEngineService.Execute<dynamic, CriskcoBusinessReportRequest>(eventConfiguration.Rule, new { input = data });
            var result = await GetApproveBusiness(ruleResult.EntityType, ruleResult.EntityId, ruleResult.BusinessId);
            if (eventConfiguration.CompletionEvents != null && eventConfiguration.CompletionEvents.Count > 0)
            {
                foreach (var eventname in eventConfiguration.CompletionEvents)
                {
                    await EventHub.Publish(eventname, new
                    {
                        EntityId = ruleResult?.EntityId,
                        EntityType = ruleResult?.EntityType,
                        Response = result,
                        Request = data,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    Logger.Info($"Event #{eventname} published for #{ruleResult.EntityId}");
                }
            }
            Logger.Info($"Processing event #{eventInfo.Name} completed for {ruleResult?.EntityId} ");
        }
        #endregion Public Methods

        #region Execute Request
        private T ExecuteRequest<T>(string response) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(response);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + nameof(response), exception);
            }
        }
        #endregion

    }
}
