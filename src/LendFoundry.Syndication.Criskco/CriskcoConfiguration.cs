﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoConfiguration : ICriskcoConfiguration
    {
        [JsonConverter(typeof(InterfaceConverter<ICriskcoCredentialConfiguration, CriskcoCredentialConfiguration>))]
        public ICriskcoCredentialConfiguration CriskcoCredential { get; set; }
        public string ApiBaseUrl { get; set; }
        public List<EventConfiguration> events { get; set; }
    }

}
