﻿using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Syndication.Criskco
{
    public static class CriskcoListenerExtensions
    {
        public static void UseCriskcoListener(this IApplicationBuilder application)
        {
            application.ApplicationServices.GetRequiredService<ICriskcoListener>().Start();
        }
    }
}