﻿

namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoCredentialConfiguration : ICriskcoCredentialConfiguration
    {
        public string appId { get; set; }
        public string appToken { get; set; }

    }
}
