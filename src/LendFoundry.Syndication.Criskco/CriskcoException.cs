﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoException : Exception
    {

        public CriskcoException()
        {
        }

        public CriskcoException(string message) : base(message)
        {
        }

        public CriskcoException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
