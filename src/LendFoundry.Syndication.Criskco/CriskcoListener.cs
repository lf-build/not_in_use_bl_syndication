﻿using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reflection;

namespace LendFoundry.Syndication.Criskco
{

    public class CriskcoListener : ICriskcoListener
    {
        #region Constructor
        public CriskcoListener
        (
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ICriskcoServiceFactory criskcoServiceFactory,
            IConfigurationServiceFactory configurationFactory)
        {

            EnsureParameter(nameof(tokenHandler), tokenHandler);
            EnsureParameter(nameof(loggerFactory), loggerFactory);
            EnsureParameter(nameof(eventHubFactory), eventHubFactory);
            EnsureParameter(nameof(tenantServiceFactory), tenantServiceFactory);
            EnsureParameter(nameof(configurationFactory), configurationFactory);
            EnsureParameter(nameof(criskcoServiceFactory), criskcoServiceFactory);


            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ConfigurationFactory = configurationFactory;
            CriskcoServiceFactory = criskcoServiceFactory;
        }
        #endregion

        #region Private Properties

        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ICriskcoServiceFactory CriskcoServiceFactory { get; }

        #endregion

        #region Public Methods

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info("Starting Listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName, null, "system", null);
                    var reader = new StaticTokenReader(token.Value);
                    var eventHub = EventHubFactory.Create(reader);
                    var configuration = ConfigurationFactory.Create<CriskcoConfiguration>(Settings.ServiceName, reader).Get();
                    if (configuration != null && configuration.events != null)
                    {
                        var criskcoService = CriskcoServiceFactory.Create(reader, logger);
                        configuration
                       .events
                       .ToList().ForEach(events =>
                       {
                           eventHub.On(events.Name, ProcessEvent(logger, criskcoService));
                           logger.Info($"It was made subscription to EventHub with the Event: #{events.Name}");
                       });

                        logger.Info("-------------------------------------------");
                    }
                    else
                    {
                        logger.Error($"The configuration for service #{Settings.ServiceName} could not be found, please verify");
                    }
                    eventHub.StartAsync();
                });

                logger.Info("Criskco listener started");
            }
            catch (Exception ex)
            {
                logger.Error("Error while listening eventhub to process Criskco listener", ex);
                logger.Info("\n Criskco listener  is working yet and waiting new event\n");
                Start();
            }
        }

        #endregion

        #region Private Methods

        #region Process Event
        private Action<EventHub.Client.EventInfo> ProcessEvent(ILogger logger,ICriskcoService criskcoService)
        {
            return async @event =>
            {
                try
                {
                    await criskcoService.ProcessEvent(@event);
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name}", ex);
                }
            };
        }
        #endregion

        #region Ensure Parameter
        private static void EnsureParameter(string name, object value)
        {
            if (value == null) throw new ArgumentNullException($"{name} cannot be null.");
        }
        #endregion
       
        #endregion
    }
}
