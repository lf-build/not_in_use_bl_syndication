﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Criskco
{
    public interface ICriskcoConfiguration
    {
        ICriskcoCredentialConfiguration CriskcoCredential { get; set; }
     
        string ApiBaseUrl { get; set; }
        List<EventConfiguration> events { get; set; }
    }
}
