﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Criskco.Proxy
{
    public interface ICriskcoProxyFactory
    {
        ICriskcoProxy Create(ITokenReader reader, ICriskcoConfiguration configuration);
    }
}