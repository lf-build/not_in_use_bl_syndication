﻿using System;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public interface IInvoice
    {
         double? Amount { get; set; }
         string BusinessId { get; set; }
         string CustomerId { get; set; }
         string Description { get; set; }
         string ErpId1 { get; set; }
         string ErpId2 { get; set; }
         string ErpId3 { get; set; }
         int Id { get; set; }
         DateTime? IssueDate { get; set; }
         string Uid { get; set; }
         double? BadDebt { get; set; }
         double? Balance { get; set; }
         DateTime? DueDate { get; set; }
         DateTime? PredictedPayDate { get; set; }
    }
}