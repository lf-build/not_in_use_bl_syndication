﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class Customer : ICustomer
    {
        public int Id { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAccountingData, AccountingData>))]

        public IAccountingData AccountingData { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICommunityData, CommunityData>))]

        public ICommunityData CommunityData { get; set; }
        public string Country { get; set; }
        public long? CreditLine { get; set; }
        public string Currency { get; set; }
        public int? Defaulted { get; set; }
        public int? FinScore { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IHistoricalMetric, HistoricalMetric>))]

        public List<IHistoricalMetric> HistoricalMetrics { get; set; }
       
        public decimal? LateInvoicePercent { get; set; }
        public string Name { get; set; }
        public string NameOfficial { get; set; }
        public string NameOfficialAlt { get; set; }
        public int? NetTerms { get; set; }
        public int? PaymentOffsetAverageDays { get; set; }
        public string Phone { get; set; }
        public string RegistrationId { get; set; }
        public string State { get; set; }
        public string TaxId { get; set; }
        public string Zip { get; set; }
    }

   

   
 

    

}
