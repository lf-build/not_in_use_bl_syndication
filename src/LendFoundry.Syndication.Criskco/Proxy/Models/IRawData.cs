﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
   public interface IRawData
    {
         List<IBadDebts> BadDebts { get; set; }
         List<ICreditMemo> CreditMemoErrors { get; set; }
         List<ICreditMemo> CreditMemos { get; set; }
         List<IInvoice> InvoiceErrors { get; set; }
         List<IInvoice> Invoices { get; set; }
         List<IPayment> PaymentErrors { get; set; }
         List<IPayment> Payments { get; set; }
         List<ITransaction> Transactions { get; set; }
    }
}
