﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class AccountingData : IAccountingData
    {
        public int? Add { get; set; }
        public double? BadDebts { get; set; }
        public double? Cei { get; set; }
        public long? CreditLine { get; set; }
        public double? CurrentReceivables { get; set; }
        public double? DelinquentReceivables { get; set; }
        public int? Dso { get; set; }
        public int? TrueDso { get; set; }
    }
}
