﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class BadDebts : IBadDebts
    {
        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Type { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
    }
}
