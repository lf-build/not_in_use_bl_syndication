﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class CommunityData : ICommunityData
    {
        public int? Followers { get; set; }
        public int? MarketCreditLineLowerBound { get; set; }
        public int? MarketCreditLineUpperBound { get; set; }
        public int? MarketCurrentReceivablesLowerBound { get; set; }
        public int? MarketCurrentReceivablesUpperBound { get; set; }
        public int? MarketDelinquentReceivablesLowerBound { get; set; }
        public int? MarketDelinquentReceivablesUpperBound { get; set; }
    }

}
