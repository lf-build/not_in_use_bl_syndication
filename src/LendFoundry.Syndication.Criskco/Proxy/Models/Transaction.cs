﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class Transaction : ITransaction
    {
        public double? Amount { get; set; }
        public string BadDebtUid { get; set; }
        public string BusinessId { get; set; }
        public string CreditMemoUid { get; set; }
        public string CustomerId { get; set; }
        public int Id { get; set; }
        public string InvoiceUid { get; set; }
        public string PaymentUid { get; set; }
    }

}
