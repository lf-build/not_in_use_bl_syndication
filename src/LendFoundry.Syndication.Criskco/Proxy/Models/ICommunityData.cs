﻿namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public interface ICommunityData
    {
        int? Followers { get; set; }

        int? MarketCreditLineLowerBound { get; set; }

        int? MarketCreditLineUpperBound { get; set; }

        int? MarketCurrentReceivablesLowerBound { get; set; }

        int? MarketCurrentReceivablesUpperBound { get; set; }

        int? MarketDelinquentReceivablesLowerBound { get; set; }

        int? MarketDelinquentReceivablesUpperBound { get; set; }
    }
}