﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class HistoricalMetric : IHistoricalMetric
    {

      
        public double? Ar { get; set; }
        public double? BadDebts { get; set; }
        public double? Invoices { get; set; }
        public int? Month { get; set; }
        public double? Payments { get; set; }
        public int? Year { get; set; }
    }
}
