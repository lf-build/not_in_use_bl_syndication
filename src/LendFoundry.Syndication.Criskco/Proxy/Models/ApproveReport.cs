﻿
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class ApproveReport: IApproveReport
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICustomer, Customer>))]
        public List<ICustomer> Customers { get; set; }
        public string ErpType { get; set; }     
        public string Name { get; set; }
        public string NameAlternative { get; set; }
        public string Phone { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRawData, RawData>))]
        public IRawData RawData { get; set; }
        public string RegistrationId { get; set; }
        public string State { get; set; }
        public string StateRegistrered { get; set; }
        public string TaxId { get; set; }
        public string Zip { get; set; }
        public int FinScore { get; set; }
        
    }
}
