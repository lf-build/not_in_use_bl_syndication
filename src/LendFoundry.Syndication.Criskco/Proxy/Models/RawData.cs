﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class RawData : IRawData
    {
        [JsonConverter(typeof(InterfaceListConverter<IBadDebts, BadDebts>))]
        public List<IBadDebts> BadDebts { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICreditMemo, CreditMemo>))]
        public List<ICreditMemo> CreditMemoErrors { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICreditMemo, CreditMemo>))]
        public List<ICreditMemo> CreditMemos { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IInvoice, Invoice>))]
        public List<IInvoice> InvoiceErrors { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IInvoice, Invoice>))]
        public List<IInvoice> Invoices { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPayment, Payment>))]
        public List<IPayment> PaymentErrors { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPayment, Payment>))]
        public List<IPayment> Payments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITransaction, Transaction>))]
        public List<ITransaction> Transactions { get; set; }
    }
}
