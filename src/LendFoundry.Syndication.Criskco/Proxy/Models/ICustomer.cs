﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public interface ICustomer
    {
        int Id { get; set; }
        IAccountingData AccountingData { get; set; }
        string Address { get; set; }
        string City { get; set; }
        ICommunityData CommunityData { get; set; }
        string Country { get; set; }
        long? CreditLine { get; set; }
        string Currency { get; set; }
        int? Defaulted { get; set; }
        int? FinScore { get; set; }
        List<IHistoricalMetric> HistoricalMetrics { get; set; }
        decimal? LateInvoicePercent { get; set; }
        string Name { get; set; }
        string NameOfficial { get; set; }
        string NameOfficialAlt { get; set; }
        int? NetTerms { get; set; }
        int? PaymentOffsetAverageDays { get; set; }
        string Phone { get; set; }
        string RegistrationId { get; set; }
        string State { get; set; }
        string TaxId { get; set; }
        string Zip { get; set; }
    }
}
