﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Models
{
    public class Invoice : IInvoice
    {
        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string ErpId1 { get; set; }
        public string ErpId2 { get; set; }
        public string ErpId3 { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
        public double? BadDebt { get; set; }
        public double? Balance { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? PredictedPayDate { get; set; }
    }
}
