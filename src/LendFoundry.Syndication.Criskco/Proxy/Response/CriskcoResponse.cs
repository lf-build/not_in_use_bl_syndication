﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Criskco.Proxy.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy.Response
{
    public class CriskcoResponse : ICriskcoResponse
    {
        [JsonConverter(typeof(InterfaceConverter<IApproveReport, ApproveReport>))]
        public IApproveReport Data { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
