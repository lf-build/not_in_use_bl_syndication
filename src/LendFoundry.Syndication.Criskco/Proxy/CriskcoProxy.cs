﻿using System;
using RestSharp;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Criskco.Proxy.Response;

namespace LendFoundry.Syndication.Criskco.Proxy
{
    public class CriskcoProxy : ICriskcoProxy
    {
        public CriskcoProxy(ICriskcoConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (string.IsNullOrWhiteSpace(configuration.ApiBaseUrl))
                throw new ArgumentNullException(nameof(configuration.ApiBaseUrl));

            Configuration = configuration;
        }
        private ICriskcoConfiguration Configuration { get; }

        public ICriskcoResponse GetApproveBusiness(string bussinessId)
        {
            if (string.IsNullOrWhiteSpace(bussinessId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(bussinessId));
            var client = new RestClient(Configuration.ApiBaseUrl.TrimEnd('/'));
            var request = new RestRequest(Method.POST);
            request.AddQueryParameter("id", bussinessId);           
            request.AddParameter("text/plain", JsonConvert.SerializeObject(Configuration.CriskcoCredential), ParameterType.RequestBody);
         
            return ExecuteRequest<CriskcoResponse>(client, request);
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new CriskcoException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new CriskcoException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.ErrorMessage ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new CriskcoException($"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new NotFoundException(response.Content);

            if (response.StatusCode != HttpStatusCode.OK)
                throw new CriskcoException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.ErrorMessage ?? ""}");

            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.ErrorMessage, exception);
            }
        }
    }
}
