﻿using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Syndication.Criskco.Proxy
{
    public class CriskcoProxyFactory : ICriskcoProxyFactory
    {
        public CriskcoProxyFactory(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentException($"{nameof(provider)} cannot be null");

            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public ICriskcoProxy Create(ITokenReader reader, ICriskcoConfiguration configuration)
        {   
            return new CriskcoProxy(configuration);
        }
    }
}