﻿using LendFoundry.Syndication.Criskco.Proxy.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Proxy
{
    public interface ICriskcoProxy
    {
        ICriskcoResponse GetApproveBusiness(string bussinessId);
    }
}
