﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class AccountingData : IAccountingData
    {

        public AccountingData()
        {

        }

        public AccountingData(Proxy.Models.IAccountingData AccountingData)
        {
            Add = AccountingData.Add;
            BadDebts = AccountingData.BadDebts;
            Cei = AccountingData.Cei;
            CreditLine = AccountingData.CreditLine;
            CurrentReceivables = AccountingData.CurrentReceivables;
            DelinquentReceivables = AccountingData.DelinquentReceivables;
            Dso = AccountingData.Dso;
            TrueDso = AccountingData.TrueDso;

        }
        public int? Add { get; set; }
        public double? BadDebts { get; set; }
        public double? Cei { get; set; }
        public long? CreditLine { get; set; }
        public double? CurrentReceivables { get; set; }
        public double? DelinquentReceivables { get; set; }
        public int? Dso { get; set; }
        public int? TrueDso { get; set; }
    }
}
