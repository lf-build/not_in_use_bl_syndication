﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class Transaction : ITransaction
    {
        public Transaction() { }

        public Transaction(Proxy.Models.ITransaction Transaction)
        {
            Amount = Transaction.Amount;
            BadDebtUid = Transaction.BadDebtUid;
            BusinessId = Transaction.BusinessId;
            CreditMemoUid = Transaction.CreditMemoUid;
            CustomerId = Transaction.CustomerId;
            Id = Transaction.Id;
            InvoiceUid = Transaction.InvoiceUid;
            PaymentUid = Transaction.PaymentUid;

        }
        public double? Amount { get; set; }
        public string BadDebtUid { get; set; }
        public string BusinessId { get; set; }
        public string CreditMemoUid { get; set; }
        public string CustomerId { get; set; }
        public int Id { get; set; }
        public string InvoiceUid { get; set; }
        public string PaymentUid { get; set; }
    }

}
