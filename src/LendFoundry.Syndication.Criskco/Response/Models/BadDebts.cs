﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class BadDebts : IBadDebts
    {
        public BadDebts() { }
        public BadDebts(Proxy.Models.IBadDebts BadDebts)
        {
            Amount = BadDebts.Amount;
            BusinessId = BadDebts.BusinessId;
            CustomerId = BadDebts.CustomerId;
            Type = BadDebts.Type;
            Id = BadDebts.Id;
            IssueDate = BadDebts.IssueDate;
            Uid = BadDebts.Uid;
        }

        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Type { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
    }
}
