﻿
using System.Collections.Generic;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class ApproveReport: IApproveReport
    {
        public ApproveReport(Proxy.Models.IApproveReport ApproveReport)
        {
            if(ApproveReport != null)
            {
                Id = ApproveReport.Id;
                Address = ApproveReport.Address;
                City = ApproveReport.City;
                Country = ApproveReport.Country;
                ErpType = ApproveReport.ErpType;
                Name = ApproveReport.Name;
                NameAlternative = ApproveReport.NameAlternative;
                Phone = ApproveReport.Phone;               
                RawData = ApproveReport.RawData!= null? new RawData(ApproveReport.RawData):null ;
                RegistrationId = ApproveReport.RegistrationId;
                State = ApproveReport.State;
                StateRegistrered = ApproveReport.StateRegistrered;
                TaxId = ApproveReport.TaxId;
                Zip = ApproveReport.Zip;
                FinScore = ApproveReport.FinScore;
                if(ApproveReport.Customers!= null && ApproveReport.Customers?.Count > 0)
                {
                    CopyCustomersList(ApproveReport.Customers);
                }
            }
          
        }

        public int Id { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public List<ICustomer> Customers { get; set; }
        public string ErpType { get; set; }     
        public string Name { get; set; }
        public string NameAlternative { get; set; }
        public string Phone { get; set; }
        public IRawData RawData { get; set; }
        public string RegistrationId { get; set; }
        public string State { get; set; }
        public string StateRegistrered { get; set; }
        public string TaxId { get; set; }
        public string Zip { get; set; }
        public int FinScore { get; set; }
        private void CopyCustomersList(IList<Proxy.Models.ICustomer> customers)
        {
            Customers = new List<ICustomer>();
            foreach (var customer in customers)
            {
                Customers.Add(new Customer(customer));
            }
        }
    }

   
}
