﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class Payment: IPayment
    {

        public Payment() { }

        public Payment(Proxy.Models.IPayment Payment)
        {
            Amount = Payment.Amount;
            BusinessId = Payment.BusinessId;
            CustomerId = Payment.CustomerId;
            Description = Payment.Description;
            ErpId1 = Payment.ErpId1;
            ErpId2 = Payment.ErpId2;
            ErpId3 = Payment.ErpId3;
            Id = Payment.Id;
            IssueDate = Payment.IssueDate;
            Uid = Payment.Uid;

        }
        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string ErpId1 { get; set; }
        public string ErpId2 { get; set; }
        public string ErpId3 { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
    }

}
