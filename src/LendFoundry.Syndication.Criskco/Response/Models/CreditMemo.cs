﻿using System;


namespace LendFoundry.Syndication.Criskco.Response
{
    public class CreditMemo : ICreditMemo
    {
        public CreditMemo() { }

        public CreditMemo(Proxy.Models.ICreditMemo CreditMemo)
        {
            Amount = CreditMemo.Amount;
            BusinessId = CreditMemo.BusinessId;
            CustomerId = CreditMemo.CustomerId;
            Description = CreditMemo.Description;
            ErpId1 = CreditMemo.ErpId1;
            ErpId2 = CreditMemo.ErpId2;
            ErpId3 = CreditMemo.ErpId3;
            Id = CreditMemo.Id;
            IssueDate = CreditMemo.IssueDate;
            Uid = CreditMemo.Uid;

        }
        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string ErpId1 { get; set; }
        public string ErpId2 { get; set; }
        public string ErpId3 { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
    }
}
