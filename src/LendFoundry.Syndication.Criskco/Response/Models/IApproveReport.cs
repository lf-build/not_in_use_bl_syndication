﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public interface IApproveReport
    {
        int Id { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string Country { get; set; }
        string Currency { get; set; }
        List<ICustomer> Customers { get; set; }
        string ErpType { get; set; }

        string Name { get; set; }
        string NameAlternative { get; set; }
        string Phone { get; set; }
        IRawData RawData { get; set; }
        string RegistrationId { get; set; }
        string State { get; set; }
        string StateRegistrered { get; set; }
        string TaxId { get; set; }
        string Zip { get; set; }

        int FinScore { get; set; }
    }
}
