﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class Invoice : IInvoice
    {
        public Invoice() { }

        public Invoice(Proxy.Models.IInvoice Invoice)
        {
            Amount = Invoice.Amount;
            BusinessId = Invoice.BusinessId;
            CustomerId = Invoice.CustomerId;
            Description = Invoice.Description;
            ErpId1 = Invoice.ErpId1;
            ErpId2 = Invoice.ErpId2;
            ErpId3 = Invoice.ErpId3;
            Id = Invoice.Id;
            IssueDate = Invoice.IssueDate;
            Uid = Invoice.Uid;
            BadDebt = Invoice.BadDebt;
            Balance = Invoice.Balance;
            DueDate = Invoice.DueDate;
            PredictedPayDate = Invoice.PredictedPayDate;
        }
        public double? Amount { get; set; }
        public string BusinessId { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string ErpId1 { get; set; }
        public string ErpId2 { get; set; }
        public string ErpId3 { get; set; }
        public int Id { get; set; }
        public DateTime? IssueDate { get; set; }
        public string Uid { get; set; }
        public double? BadDebt { get; set; }
        public double? Balance { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? PredictedPayDate { get; set; }
    }
}
