﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class CommunityData : ICommunityData
    {
        public CommunityData() { }

        public CommunityData(Proxy.Models.ICommunityData CommunityData)
        {
            Followers = CommunityData.Followers;
            MarketCreditLineLowerBound = CommunityData.MarketCreditLineLowerBound;
            MarketCreditLineUpperBound = CommunityData.MarketCreditLineUpperBound;
            MarketCurrentReceivablesLowerBound = CommunityData.MarketCurrentReceivablesLowerBound;
            MarketCurrentReceivablesUpperBound = CommunityData.MarketCurrentReceivablesUpperBound;
            MarketDelinquentReceivablesLowerBound = CommunityData.MarketDelinquentReceivablesLowerBound;
            MarketDelinquentReceivablesUpperBound = CommunityData.MarketDelinquentReceivablesUpperBound;
        }
        public int? Followers { get; set; }
        public int? MarketCreditLineLowerBound { get; set; }
        public int? MarketCreditLineUpperBound { get; set; }
        public int? MarketCurrentReceivablesLowerBound { get; set; }
        public int? MarketCurrentReceivablesUpperBound { get; set; }
        public int? MarketDelinquentReceivablesLowerBound { get; set; }
        public int? MarketDelinquentReceivablesUpperBound { get; set; }
    }

}
