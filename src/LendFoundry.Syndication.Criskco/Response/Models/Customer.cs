﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class Customer : ICustomer
    {

        public Customer() { }

        public Customer(Proxy.Models.ICustomer Customer)
        {
            Id = Customer.Id;
            AccountingData = new AccountingData(Customer.AccountingData);
            Address = Customer.Address;
            City = Customer.City;
            CommunityData =new CommunityData(Customer.CommunityData);
            Country = Customer.Country;
            CreditLine = Customer.CreditLine;
            Currency = Customer.Currency;
            Defaulted = Customer.Defaulted;
            FinScore = Customer.FinScore;
            LateInvoicePercent = Customer.LateInvoicePercent;
            Name = Customer.Name;
            NameOfficial = Customer.NameOfficial;
            NameOfficialAlt = Customer.NameOfficialAlt;
            NetTerms = Customer.NetTerms;
            PaymentOffsetAverageDays = Customer.PaymentOffsetAverageDays;
            Phone = Customer.Phone;
            RegistrationId = Customer.RegistrationId;
            State = Customer.State;
            TaxId = Customer.TaxId;
            Zip = Customer.Zip;

            if(Customer.HistoricalMetrics!= null && Customer.HistoricalMetrics?.Count >0)
            {
                CopyHistoricalMetricsList(Customer.HistoricalMetrics);
            }

        }
        public int Id { get; set; }
        public IAccountingData AccountingData { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public ICommunityData CommunityData { get; set; }
        public string Country { get; set; }
        public long? CreditLine { get; set; }
        public string Currency { get; set; }
        public int? Defaulted { get; set; }
        public int? FinScore { get; set; }
        public List<IHistoricalMetric> HistoricalMetrics { get; set; }
       
        public decimal? LateInvoicePercent { get; set; }
        public string Name { get; set; }
        public string NameOfficial { get; set; }
        public string NameOfficialAlt { get; set; }
        public int? NetTerms { get; set; }
        public int? PaymentOffsetAverageDays { get; set; }
        public string Phone { get; set; }
        public string RegistrationId { get; set; }
        public string State { get; set; }
        public string TaxId { get; set; }
        public string Zip { get; set; }


        private void CopyHistoricalMetricsList(IList<Proxy.Models.IHistoricalMetric> historicalList)
        {
            HistoricalMetrics = new List<IHistoricalMetric>();
            foreach (var historicalmetric in historicalList)
            {
                HistoricalMetrics.Add(new HistoricalMetric(historicalmetric));
            }
        }
    }
}
