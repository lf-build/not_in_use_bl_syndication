﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class HistoricalMetric : IHistoricalMetric
    {
        public HistoricalMetric() { }

        public HistoricalMetric(Proxy.Models.IHistoricalMetric HistoricalMetric)
        {
            Ar = HistoricalMetric.Ar;
            BadDebts = HistoricalMetric.BadDebts;
            Invoices = HistoricalMetric.Invoices;
            Month = HistoricalMetric.Month;
            Payments = HistoricalMetric.Payments;
            Year = HistoricalMetric.Year;
        }
        public double? Ar { get; set; }
        public double? BadDebts { get; set; }
        public double? Invoices { get; set; }
        public int? Month { get; set; }
        public double? Payments { get; set; }
        public int? Year { get; set; }
    }
}
