﻿namespace LendFoundry.Syndication.Criskco.Response
{
    public interface IHistoricalMetric
    {
        double? Ar { get; set; }
        double? BadDebts { get; set; }
        double? Invoices { get; set; }
        int? Month { get; set; }
        double? Payments { get; set; }
        int? Year { get; set; }
    }
}