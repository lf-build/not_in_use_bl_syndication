﻿namespace LendFoundry.Syndication.Criskco.Response
{
    public interface IAccountingData
    {
        int? Add { get; set; }
        double? BadDebts { get; set; }
        double? Cei { get; set; }
        long? CreditLine { get; set; }
        double? CurrentReceivables { get; set; }
        double? DelinquentReceivables { get; set; }
        int? Dso { get; set; }
        int? TrueDso { get; set; }
    }
}