﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class RawData :IRawData
    {
        public RawData()
        {

        }

        public RawData(Proxy.Models.IRawData RawData)
        {
            if(RawData != null)
            {
                if(RawData.BadDebts != null && RawData.BadDebts?.Count >0)
                {
                    CopyIBadDebtsList(RawData.BadDebts);
                }
                if (RawData.CreditMemoErrors != null && RawData.CreditMemoErrors?.Count > 0)
                {
                    CopyCreditMemoList(RawData.CreditMemoErrors , CreditMemoErrors);
                }
                if (RawData.CreditMemos != null && RawData.CreditMemos?.Count > 0)
                {                    
                    CopyCreditMemoList(RawData.CreditMemos, CreditMemos);
                }
                if (RawData.InvoiceErrors != null && RawData.InvoiceErrors?.Count > 0)
                {
                    CopyInvoiceList(RawData.InvoiceErrors , InvoiceErrors);
                }
                if (RawData.Invoices != null && RawData.Invoices?.Count > 0)
                {
                    CopyInvoiceList(RawData.Invoices, Invoices);
                }
                if (RawData.PaymentErrors != null && RawData.PaymentErrors?.Count > 0)
                {
                    CopyPaymentsList(RawData.PaymentErrors , PaymentErrors);
                }
                if (RawData.Payments != null && RawData.Payments?.Count > 0)
                {
                    CopyPaymentsList(RawData.Payments, Payments);
                }
                if (RawData.Transactions != null && RawData.Transactions?.Count > 0)
                {
                    CopyTransactionsList(RawData.Transactions);
                }
            }
        }
        public List<IBadDebts> BadDebts { get; set; }
        public List<ICreditMemo> CreditMemoErrors { get; set; }
        public List<ICreditMemo> CreditMemos { get; set; }       
        public List<IInvoice> InvoiceErrors { get; set; }
        public List<IInvoice> Invoices { get; set; }
        public List<IPayment> PaymentErrors { get; set; }
        public List<IPayment> Payments { get; set; }
        public List<ITransaction> Transactions { get; set; }

        private void CopyIBadDebtsList(IList<Proxy.Models.IBadDebts> BadDebt)
        {
            BadDebts = new List<IBadDebts>();
            foreach (var badDebt in BadDebt)
            {
                BadDebts.Add(new BadDebts(badDebt));
            }
        }
        private void CopyCreditMemoList(IList<Proxy.Models.ICreditMemo> creditMemos, IList<ICreditMemo> creditMemoList)
        {
            creditMemoList = new List<ICreditMemo>();
            foreach (var creditMemo in creditMemos)
            {
                creditMemoList.Add(new CreditMemo(creditMemo));
            }
        }
        private void CopyInvoiceList(IList<Proxy.Models.IInvoice> invoices  , IList<IInvoice> invoiceList)
        {
            invoiceList = new List<IInvoice>();
            foreach (var invoice in invoices)
            {
                invoiceList.Add(new Invoice(invoice));
            }
        }
        private void CopyPaymentsList(IList<Proxy.Models.IPayment> payments , IList<IPayment> paymentLists)
        {
            paymentLists = new List<IPayment>();
            foreach (var payment in payments)
            {
                paymentLists.Add(new Payment(payment));
            }
        }
        private void CopyTransactionsList(IList<Proxy.Models.ITransaction> transactions)
        {
            Transactions = new List<ITransaction>();
            foreach (var transaction in transactions)
            {
                Transactions.Add(new Transaction(transaction));
            }
        }
    }
}
