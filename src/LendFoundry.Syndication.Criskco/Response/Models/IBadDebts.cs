﻿using System;

namespace LendFoundry.Syndication.Criskco.Response
{
    public interface IBadDebts
    {
        double? Amount { get; set; }
        string BusinessId { get; set; }
        string CustomerId { get; set; }
        string Type { get; set; }
        int Id { get; set; }
        DateTime? IssueDate { get; set; }
    }
}