﻿namespace LendFoundry.Syndication.Criskco.Response
{
    public interface ITransaction
    {
        double? Amount { get; set; }
        string BadDebtUid { get; set; }
        string BusinessId { get; set; }
        string CreditMemoUid { get; set; }
        string CustomerId { get; set; }
        int Id { get; set; }
        string InvoiceUid { get; set; }
        string PaymentUid { get; set; }
    }
}