﻿using LendFoundry.Syndication.Criskco.Proxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public interface ICriskcoResponse
    {
        IApproveReport ApproveReport { get; set; }
        string ErrorCode { get; set; }
        string ErrorMessage { get; set; }
        string Message { get; set; }
        bool Success { get; set; }
    }
}
