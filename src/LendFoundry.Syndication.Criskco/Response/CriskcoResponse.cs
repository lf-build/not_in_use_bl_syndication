﻿using LendFoundry.Syndication.Criskco.Proxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco.Response
{
    public class CriskcoResponse : ICriskcoResponse
    {
        public CriskcoResponse()
        {

        }

        public CriskcoResponse(Proxy.Response.ICriskcoResponse CriskcoResponse)
        {
            ApproveReport = new ApproveReport(CriskcoResponse.Data);
            ErrorCode = CriskcoResponse.ErrorCode;
            ErrorMessage = CriskcoResponse.ErrorMessage;
            Message = CriskcoResponse.Message;
            Success = CriskcoResponse.Success;
        }

        public IApproveReport ApproveReport { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
