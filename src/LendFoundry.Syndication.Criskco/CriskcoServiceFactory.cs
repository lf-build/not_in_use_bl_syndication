﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Criskco.Proxy;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Syndication.Criskco
{
    public class CriskcoServiceFactory : ICriskcoServiceFactory
    {
        public CriskcoServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ICriskcoService Create(ITokenReader reader,ILogger logger)
        {
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var criskcoConfigurationService = configurationServiceFactory.Create<CriskcoConfiguration>(Settings.ServiceName, reader);
            var criskcoConfiguration = criskcoConfigurationService.Get();

            var proxyFactory = Provider.GetService<ICriskcoProxyFactory>();
            var proxyService = proxyFactory.Create(reader,criskcoConfiguration);

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineFactory.Create(reader);

            return new CriskcoService(proxyService, eventHubService,logger,decisionEngine,criskcoConfiguration);
        }
    }
}
