﻿
using LendFoundry.EventHub.Client;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Criskco
{
    public interface ICriskcoService
    {
        Task<Response.ICriskcoResponse> GetApproveBusiness(string entityType, string entityId, string bussinessId);

        Task ProcessEvent(EventInfo eventInfo);
    }
}