﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Criskco
{
    public interface ICriskcoServiceFactory
    {
        ICriskcoService Create(ITokenReader reader, ILogger logger);
    }
}