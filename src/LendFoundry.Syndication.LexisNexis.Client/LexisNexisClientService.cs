﻿using System.Threading.Tasks;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Foundation.Services;
using RestSharp;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.FlexId;
using LendFoundry.Syndication.LexisNexis.FraudPoint;
using LendFoundry.Syndication.LexisNexis.InstantId;
using LendFoundry.Syndication.LexisNexis.InstantIdKba;
using System;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    public class LexisNexisClientService : ILexisNexisService
    {
        public LexisNexisClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IBankruptcyReportResponse> FcraBankruptcyReport(string entityType, string entityId, IBankruptcyFcraReportRequest reportRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-fcra-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(request);
        }

        public async Task<IBankruptcySearchResponse> FcraBankruptcySearch(string entityType, string entityId, ISearchBankruptcyRequest searchRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-search", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(searchRequest);
            return await Client.ExecuteAsync<BankruptcySearchResponse>(request);
        }

        public async Task<ICriminalReportResponse> CriminalRecordReport(string entityType, string entityId, ICriminalRecordReportRequest criminalRecordRequest)
        {
            var request = new RestRequest("/criminalrecord/{entityType}/{entityId}/criminalrecord-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(criminalRecordRequest);
            return await Client.ExecuteAsync<CriminalReportResponse>(request);
        }

        public async Task<ICriminalRecordSearchResponse> CriminalSearch(string entityType, string entityId, ICriminalRecordSearchRequest criminalSearchRequest)
        {
            var request = new RestRequest("/criminalrecord/{entityType}/{entityId}/criminalrecord-search", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(criminalSearchRequest);
            return await Client.ExecuteAsync<CriminalRecordSearchResponse>(request);
        }


        public async Task<IBankruptcyReportResponse> BankruptcyReport(string entityType, string entityId, IBankruptcyReportRequest reportRequest)
        {
            var request = new RestRequest("/bankruptcy/{entityType}/{entityId}/bankruptcy-report", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<BankruptcyReportResponse>(request);
        }
  
        public IFlexIdResponse GetFlexIdVerificationResult(IFlexIdRequest request)
        {
            throw new NotImplementedException();
        }

        public FraudPoint.IFraudPointResponse GetFraudPointVerificationResult(IFraudPointRequest request)
        {
            throw new NotImplementedException();
        }

        public ITransactionResponse GetInstantIdKbaContinuationVerificationResult(IInstantIdContinuationRequest request)
        {
            throw new NotImplementedException();
        }

        public ITransactionResponse GetInstantIdKbaIdentityVerificationResult(IInstantIdIdentityRequest request)
        {
            throw new NotImplementedException();
        }

      
        public IInstantIdResponse GetInstantIdVerificationResult(IInstantIdRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
