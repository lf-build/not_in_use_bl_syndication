﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    public class LexisNexisClientServiceFactory : ILexisNexisClientServiceFactory
    {
        #region Constructors
        public LexisNexisClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        #endregion

        #region Public Methods
        public ILexisNexisService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new LexisNexisClientService(client);
        }

        #endregion
    }
}

