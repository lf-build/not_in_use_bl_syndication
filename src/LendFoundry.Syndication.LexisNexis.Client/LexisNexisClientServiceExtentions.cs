﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    public static class LexisNexisClientServiceExtentions
    {
        public static IServiceCollection AddLexisNexisService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ILexisNexisClientServiceFactory>(p => new LexisNexisClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILexisNexisClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
