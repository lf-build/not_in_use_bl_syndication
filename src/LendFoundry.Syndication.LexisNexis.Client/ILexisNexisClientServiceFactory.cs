﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.LexisNexis.Client
{
    public interface ILexisNexisClientServiceFactory
    {
        ILexisNexisService Create(ITokenReader reader);
    }
}

