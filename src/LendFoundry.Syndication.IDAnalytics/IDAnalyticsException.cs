﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
    [Serializable]
    public class IDAnalyticsException : Exception
    {
        public IDAnalyticsException()
        {
        }

        public IDAnalyticsException(string message) : base(message)
        {
        }

        public IDAnalyticsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected IDAnalyticsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
