﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.IDAnalytics.Proxy;
using LendFoundry.Syndication.IDAnalytics.Proxy.Response;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
    public class IDAnalyticsService :IIDAnalyticsService
    { 
        public IDAnalyticsService(IIDAnalyticsConfiguration configuration, IIDAnalyticsProxy proxy, ILookupService lookup, IEventHubClient eventHub,ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            Configuration = configuration;
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            
        }
        private IIDAnalyticsConfiguration Configuration { get; }
        private IIDAnalyticsProxy  Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        public static string ServiceName { get; } = "IDAnalytics";
        public ILogger Logger { get; set; }
        public async Task<Envelope> GetReport(string entityType,string entityId,IDAnalyticsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
                request.LoanApplicationId = entityId;
                var response = await Proxy.GetReport(request);
                await EventHub.Publish(new IDAnalyticsReportPulled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = request,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return response;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new IDAnalyticsReportPulledFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw ;
            }
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
