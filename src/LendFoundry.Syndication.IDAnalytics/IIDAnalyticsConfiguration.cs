﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.IDAnalytics
{
    public interface IIDAnalyticsConfiguration
    {
        
        string Url { get; set; }
        string IdAnalyticsClient { get; set; }
        string IdAnalyticsUserName { get; set; }
        string IdAnalyticsPassword { get; set; }
        string IdAnalyticsSolution { get; set; }
        List<IProductTypeConfiguration> IDAnalyticsProducts { get; set; }
        string IdAnalyticsRequestType { get; set; }
        string IdAnalyticsIndustryType { get; set; }
        string IdAnalyticsEventType { get; set; }
        string IDAnalyticsCertificate { get; set; }
        string PassThru1 { get; set; } 
        string PassThru2 { get; set; } 
        string Designation { get; set; }
        decimal schemaVersion { get; set; }
         string key { get; set; }
        string ProxyUrl { get; set; }
        bool UseProxy { get; set; }
    }
}
