﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.IDAnalytics
{
    public class IDAnalyticsConfiguration : IIDAnalyticsConfiguration
    {
        public IDAnalyticsConfiguration()
        {
            IDAnalyticsProducts = new List<IProductTypeConfiguration>();
        }
         public string Url { get; set; }
        public string IdAnalyticsClient { get; set; }
        public string IdAnalyticsUserName { get; set; }
        public string IdAnalyticsPassword { get; set; }
        public string IdAnalyticsSolution { get; set; }
        
        public  string IdAnalyticsRequestType { get; set; }
        public string IdAnalyticsIndustryType { get; set; }
        public string IdAnalyticsEventType { get; set; }
        public string IDAnalyticsCertificate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IProductTypeConfiguration, ProductTypeConfiguration>))]
        public List<IProductTypeConfiguration> IDAnalyticsProducts { get; set; }

        public string PassThru1 { get; set; } = "passthru1";
        public string PassThru2 { get; set; } = "passthru2";
        public string Designation { get; set; } = "A1";
        public decimal schemaVersion { get; set; } = 3.0m;
        public string key { get; set; } = "standardRequest";
        public string ProxyUrl { get; set; }
        public bool UseProxy { get; set; } = true;
    }
}
