﻿using LendFoundry.Syndication.IDAnalytics.Proxy.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
    public interface IIDAnalyticsService
    {
        Task<Envelope> GetReport(string entityType, string entityId, IDAnalyticsRequest request);
    }
}
