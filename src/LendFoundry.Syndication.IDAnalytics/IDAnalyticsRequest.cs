﻿namespace LendFoundry.Syndication.IDAnalytics
{
    public class IDAnalyticsRequest :IIDAnalyticsRequest
    {
      
        public string ApplicationDate { get; set; }

        public string LoanNumber { get; set; }
        public string LoanApplicationId { get; set; }

        public string SSN { get; set; }
        public string SSNLast4 { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
     
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string PlaceOfBirth { get; set; }
        public string MothersMaiden { get; set; }
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
        public string Country { get; set; }
        public string AddressSince { get; set; }
        public string Occupancy { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string WorkPhone { get; set; }

        public string Email { get; set; }

        public string IDType { get; set; }

        public string IDNumber { get; set; }
        public string EmployerType { get; set; }
        public string EmploymentType { get; set; }
        public string Name { get; set; }
       
        public string TitleAtEmployment { get; set; }
       
        public string EmploymentAddressLine1 { get; set; }
       
        public string EmploymentAddressLine2 { get; set; }
       
        public string EmploymentCity { get; set; }
       
        public string EmploymentState { get; set; }
       
        public string EmploymentZip { get; set; }
       
        public string TimeAtEmployer { get; set; }
       
        public string StartDate { get; set; }
       
        public string Salary { get; set; }
       
        public string PrevEmployerType { get; set; }
       
        public string PrevEmploymentType { get; set; }
       
        public string PrevName { get; set; }
       
        public string PrevCity { get; set; }
       
        public string PrevState { get; set; }
       
        public string PrevZip { get; set; }
       
        public string PrevTimeAtEmployer { get; set; }
       
        public string PrevStartDate { get; set; }
       
        public string PrevTitle { get; set; }
       
        public string PrevSalary { get; set; }

     
       
        public string Channel { get; set; }
       
        public string AcquisitionMethod { get; set; }
       
        public string AgentLoc { get; set; }
       
        public string SourceIP { get; set; }
       
        public string PrimaryDecisionCode { get; set; }
       
        public string SecondaryDecisionCode { get; set; }
       
        public string PrimaryPortfolio { get; set; }
       
        public string SecondaryPortfolio { get; set; }
       
        public string SecondaryFraudCode { get; set; }
       
        public string PrimaryIncome { get; set; }
       
        public string InferredIncome { get; set; }
       
        public string RecommendedCreditLine { get; set; }

        public string AcctLinkKey { get; set; }
    }
}
