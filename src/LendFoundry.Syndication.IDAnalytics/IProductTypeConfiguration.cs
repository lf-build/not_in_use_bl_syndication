﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
  public  interface IProductTypeConfiguration
   {
        string Name { get; set; }
        string ProductID { get; set; }
    }
}
