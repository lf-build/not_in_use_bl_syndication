﻿using LendFoundry.Syndication.IDAnalytics;
using System;

namespace idscore.request
{

    public partial class IDScoreRequest
    {
        public IDScoreRequest()
        {

        }
        public IDScoreRequest(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            if (configuration.key.Equals("standardRequest"))
            {
                schemaVersion = configuration.schemaVersion;
                Origination = new Origination(configuration, request);
                Identity = new Identity(request);
                Application = new Application(request);
            }
        }
    }
    public partial class Origination
    {
        public Origination()
        {

        }
        public Origination(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            AppID = request.LoanApplicationId.ToString();
            RequestType = configuration.IdAnalyticsRequestType;
            ApplicationDate = Convert.ToDateTime(request.ApplicationDate);
            IndustryType = configuration.IdAnalyticsIndustryType;
            EventType = configuration.IdAnalyticsEventType;
            PassThru1 = configuration.PassThru1;
            PassThru2 = configuration.PassThru2;
            Designation = (Designation)Enum.Parse(typeof(Designation), configuration.Designation);
        }
    }
    public partial class Address
    {
        public Address()
        {

        }
        public Address(string line1,string line2)
        {
            Line1 = line1;
            Line2 = Line2;
        }
    }
    public partial class Employment
    {
        public Employment()
        {

        }
        public Employment(IIDAnalyticsRequest request)
        {
            EmploymentType = request.EmploymentType;
            EmployerType = request.EmployerType;
            Name = request.Name;
            City = request.City;
            State = request.EmploymentState;
            Zip = request.EmploymentZip;
            TimeAtEmployer =  request.TimeAtEmployer;
            StartDate = request?.StartDate!=null ? Convert.ToDateTime(request.StartDate).ToString("yyyy-MM-dd"):null;
            Title =  request.TitleAtEmployment;
            Salary = request.Salary;
            PrevEmployerType = request.PrevEmployerType;
            PrevEmploymentType =  request.PrevEmploymentType;
            PrevName = request.PrevName;
            PrevCity =  request.PrevCity;
            PrevState = request.PrevState;
            PrevZip =  request.PrevZip;
            PrevTimeAtEmployer =  request.PrevTimeAtEmployer;
            PrevStartDate = request?.PrevStartDate!=null?Convert.ToDateTime(request.PrevStartDate).ToString("yyyy-MM-dd"):null;
            PrevTitle =  request.PrevTitle;
            PrevSalary =  request.PrevSalary;
            Item = new Address(request.EmploymentAddressLine1, request.EmploymentAddressLine2);
        }
    }
    public partial class Identity
    {
        public Identity()
        {

        }
        public Identity(IIDAnalyticsRequest request)
        {
            SSN =  request.SSN;
            Title = request.Title;
            FirstName = request.FirstName;
            MiddleName =  request.MiddleName;
            LastName =  request.LastName;
            Suffix =  request.Suffix;
            DOB =  request.DOB;
            PlaceOfBirth =  request.PlaceOfBirth;
            MothersMaiden = request.MothersMaiden;
            City =  request.City;
            State = request.State;
            Zip = request.Zip;
            AddressSince =  request.AddressSince;
            PrevCity =  request.PrevCity;
            PrevState =  request.PrevState;
            HomePhone =  request.HomePhone;
            MobilePhone = request.MobilePhone;
            WorkPhone =  request.WorkPhone;
            WorkPhone =  request.IDType;
            IDType =  request.IDType;
            IDOrigin =  request.AddressSince;
            IDNumber =  request.AddressSince; ;
            Email =  request.Email;

            Item = new Address(request.AddressLine1, request.AddressLine2);
            Employment = new Employment(request);

        }

    }
    public partial class Application
    {
        public Application()
        {

        }
        public Application(IIDAnalyticsRequest request)
        {
            Channel =  request.Channel;
            AcquisitionMethod = request.AcquisitionMethod;
            AgentLoc =  request.AgentLoc;
            SourceIP =  request.SourceIP;
            PrimaryDecisionCode =  request.PrimaryDecisionCode;
            SecondaryDecisionCode =  request.SecondaryDecisionCode;
            PrimaryPortfolio =  request.PrimaryPortfolio;
            SecondaryPortfolio =  request.SecondaryPortfolio;
            SecondaryFraudCode =  request.SecondaryFraudCode;
            
            InferredIncome = request.InferredIncome;
            RecommendedCreditLine =  request.RecommendedCreditLine;
        }
    }
}
