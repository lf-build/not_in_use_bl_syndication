﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.IDAnalytics.Proxy
{
    [XmlType(Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class SOAPEnvelope
    {
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string soapenv { get; set; }

        //[XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        //public ResponseBody<CommandType> body { get; set; }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();

        public SOAPEnvelope()
        {
            xmlns.Add("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            xmlns.Add("api", "http://idanalytics.com/core/api");
            xmlns.Add("", "http://www.w3.org/2001/XMLSchema-instance");
            xmlns.Add("", "http://www.w3.org/2001/XMLSchema");
            xmlns.Add("", "http://idanalytics.com/products/idscore/request");
        }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class ResponseBody<T>
    {
       // [XmlElement(Namespace = "http://idanalytics.com/core/api")]
        public T Command { get; set; }
    }


}
