﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=4.0.30319.33440.
// 
namespace idscore
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class ComplianceRecord
    {

        private string appIDField;

        private string iDASequenceField;

        private System.DateTime iDATimeStampField;

        private string passThru1Field;

        private string passThru2Field;

        private string passThru3Field;

        private string passThru4Field;

        private RiskSummary riskSummaryField;

        private Group[] indicatorsField;

        private ActionSummary actionSummaryField;

        private decimal schemaVersionField;

        private bool schemaVersionFieldSpecified;

        /// <remarks/>
        public string AppID
        {
            get
            {
                return this.appIDField;
            }
            set
            {
                this.appIDField = value;
            }
        }

        /// <remarks/>
        public string IDASequence
        {
            get
            {
                return this.iDASequenceField;
            }
            set
            {
                this.iDASequenceField = value;
            }
        }

        /// <remarks/>
        public System.DateTime IDATimeStamp
        {
            get
            {
                return this.iDATimeStampField;
            }
            set
            {
                this.iDATimeStampField = value;
            }
        }

        /// <remarks/>
        public string PassThru1
        {
            get
            {
                return this.passThru1Field;
            }
            set
            {
                this.passThru1Field = value;
            }
        }

        /// <remarks/>
        public string PassThru2
        {
            get
            {
                return this.passThru2Field;
            }
            set
            {
                this.passThru2Field = value;
            }
        }

        /// <remarks/>
        public string PassThru3
        {
            get
            {
                return this.passThru3Field;
            }
            set
            {
                this.passThru3Field = value;
            }
        }

        /// <remarks/>
        public string PassThru4
        {
            get
            {
                return this.passThru4Field;
            }
            set
            {
                this.passThru4Field = value;
            }
        }

        /// <remarks/>
        public RiskSummary RiskSummary
        {
            get
            {
                return this.riskSummaryField;
            }
            set
            {
                this.riskSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Group", IsNullable = false)]
        public Group[] Indicators
        {
            get
            {
                return this.indicatorsField;
            }
            set
            {
                this.indicatorsField = value;
            }
        }

        /// <remarks/>
        public ActionSummary ActionSummary
        {
            get
            {
                return this.actionSummaryField;
            }
            set
            {
                this.actionSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal schemaVersion
        {
            get
            {
                return this.schemaVersionField;
            }
            set
            {
                this.schemaVersionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool schemaVersionSpecified
        {
            get
            {
                return this.schemaVersionFieldSpecified;
            }
            set
            {
                this.schemaVersionFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class RiskSummary
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class Group
    {

        private string statusField;

        private Indicator[] indicatorField;

        private string nameField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Indicator")]
        public Indicator[] Indicator
        {
            get
            {
                return this.indicatorField;
            }
            set
            {
                this.indicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class Indicator
    {

        private string descriptionField;

        private string valueField;

        private string statusField;

        private string nameField;

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class ActionSummary
    {

        private RecommendedAction[] recommendedActionField;

        private ActionResult[] actionResultField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RecommendedAction")]
        public RecommendedAction[] RecommendedAction
        {
            get
            {
                return this.recommendedActionField;
            }
            set
            {
                this.recommendedActionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ActionResult")]
        public ActionResult[] ActionResult
        {
            get
            {
                return this.actionResultField;
            }
            set
            {
                this.actionResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class RecommendedAction
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class ActionResult
    {

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://idanalytics.com/products/compliance/result")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://idanalytics.com/products/compliance/result", IsNullable = false)]
    public partial class Indicators
    {

        private Group[] groupField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Group")]
        public Group[] Group
        {
            get
            {
                return this.groupField;
            }
            set
            {
                this.groupField = value;
            }
        }
    }
}