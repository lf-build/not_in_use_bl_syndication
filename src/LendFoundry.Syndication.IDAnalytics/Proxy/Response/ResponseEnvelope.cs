﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.IDAnalytics.Proxy.Response
{
    [XmlRoot(ElementName = "OutputRecord", Namespace = "http://idanalytics.com/products/idscore/result")]
    public class OutputRecord
    {
        [XmlElement(ElementName = "IDAStatus", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDAStatus { get; set; }
        [XmlElement(ElementName = "AppID", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string AppID { get; set; }
        [XmlElement(ElementName = "Designation", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string Designation { get; set; }
        [XmlElement(ElementName = "IDASequence", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDASequence { get; set; }
        [XmlElement(ElementName = "IDATimeStamp", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDATimeStamp { get; set; }
        [XmlElement(ElementName = "IDScore", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScore { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode1", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode1 { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode2", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode2 { get; set; }
        [XmlElement(ElementName = "IDScoreResultCode3", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string IDScoreResultCode3 { get; set; }
        [XmlElement(ElementName = "PassThru1", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string PassThru1 { get; set; }
        [XmlElement(ElementName = "PassThru2", Namespace = "http://idanalytics.com/products/idscore/result")]
        public string PassThru2 { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "internal", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Internal { get; set; }
        [XmlAttribute(AttributeName = "schemaVersion")]
        public string SchemaVersion { get; set; }
        [XmlElement(ElementName = "Indicators", Namespace = "http://idanalytics.com/products/idscore/result")]
        public Indicators Indicators { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
    public class Item
    {
        [XmlElement(ElementName = "OutputRecord", Namespace = "http://idanalytics.com/products/idscore/result")]
        public OutputRecord OutputRecord { get; set; }
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
    }

    [XmlRoot(ElementName = "Indicator", Namespace = "http://idanalytics.com/products/idscore/result")]
    public class Indicator
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Group", Namespace = "http://idanalytics.com/products/idscore/result")]
    public class Group
    {
        [XmlElement(ElementName = "Indicator", Namespace = "http://idanalytics.com/products/idscore/result")]
        public List<Indicator> Indicator { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "Indicators", Namespace = "http://idanalytics.com/products/idscore/result")]
    public class Indicators
    {
        [XmlElement(ElementName = "Group", Namespace = "http://idanalytics.com/products/idscore/result")]
        public Group Group { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
    public class Body
    {
        [XmlElement(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "Response", Namespace = "http://idanalytics.com/core/api")]
    public class Response
    {
        [XmlElement(ElementName = "Solution", Namespace = "http://idanalytics.com/core/api")]
        public string Solution { get; set; }
        [XmlElement(ElementName = "RequestID", Namespace = "http://idanalytics.com/core/api")]
        public string RequestID { get; set; }
        [XmlElement(ElementName = "View", Namespace = "http://idanalytics.com/core/api")]
        public string View { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "api", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Api { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body2
    {
        [XmlElement(ElementName = "Response", Namespace = "http://idanalytics.com/core/api")]
        public Response Response { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body2 Body2 { get; set; }
        [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }
}
