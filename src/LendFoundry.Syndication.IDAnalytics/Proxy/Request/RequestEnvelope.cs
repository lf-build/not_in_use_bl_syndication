﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.IDAnalytics.Proxy.Request
{
    [XmlRoot(ElementName = "Credentials", Namespace = "http://idanalytics.com/core/api")]
    public partial class Credentials
    {
        [XmlElement(ElementName = "Username", Namespace = "http://idanalytics.com/core/api")]
        public string Username { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "http://idanalytics.com/core/api")]
        public string Password { get; set; }
    }

    [XmlRoot(ElementName = "Product", Namespace = "http://idanalytics.com/core/api")]
    public partial class Product
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "productID")]
        public string ProductID { get; set; }
    }

    [XmlRoot(ElementName = "ProductSelection", Namespace = "http://idanalytics.com/core/api")]
    public partial class ProductSelection
    {
        [XmlElement(ElementName = "Product", Namespace = "http://idanalytics.com/core/api")]
        public List<Product> Product { get; set; }
    }

    [XmlRoot(ElementName = "Origination", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class Origination
    {
        [XmlElement(ElementName = "RequestType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string RequestType { get; set; }
        [XmlElement(ElementName = "ApplicationDate", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string ApplicationDate { get; set; }
        [XmlElement(ElementName = "AppID", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string AppID { get; set; }
        [XmlElement(ElementName = "Designation", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Designation { get; set; }
        [XmlElement(ElementName = "EventType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string EventType { get; set; }
        [XmlElement(ElementName = "IndustryType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string IndustryType { get; set; }
        [XmlElement(ElementName = "PassThru1", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PassThru1 { get; set; }
        [XmlElement(ElementName = "PassThru2", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PassThru2 { get; set; }
    }

    [XmlRoot(ElementName = "Address", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class Address
    {
        [XmlElement(ElementName = "Line1", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Line1 { get; set; }
        [XmlElement(ElementName = "Line2", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Line2 { get; set; }
    }

    [XmlRoot(ElementName = "PrevAddress", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class PrevAddress
    {
        [XmlElement(ElementName = "Line1", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Line1 { get; set; }
    }

    [XmlRoot(ElementName = "Employment", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class Employment
    {
        [XmlElement(ElementName = "EmployerType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string EmployerType { get; set; }
        [XmlElement(ElementName = "EmploymentType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string EmploymentType { get; set; }
        [XmlElement(ElementName = "Name", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "City", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string City { get; set; }
        [XmlElement(ElementName = "State", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string State { get; set; }
        [XmlElement(ElementName = "Zip", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "TimeAtEmployer", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string TimeAtEmployer { get; set; }
        [XmlElement(ElementName = "Title", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Title { get; set; }
        [XmlElement(ElementName = "Salary", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Salary { get; set; }
        [XmlElement(ElementName = "PrevEmployerType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevEmployerType { get; set; }
        [XmlElement(ElementName = "PrevEmploymentType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevEmploymentType { get; set; }
        [XmlElement(ElementName = "PrevName", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevName { get; set; }
        [XmlElement(ElementName = "PrevAddress", Namespace = "http://idanalytics.com/products/idscore/request")]
        public PrevAddress PrevAddress { get; set; }
        [XmlElement(ElementName = "PrevCity", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevCity { get; set; }
        [XmlElement(ElementName = "PrevState", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevState { get; set; }
        [XmlElement(ElementName = "PrevZip", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevZip { get; set; }
        [XmlElement(ElementName = "PrevTimeAtEmployer", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevTimeAtEmployer { get; set; }
        [XmlElement(ElementName = "PrevTitle", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevTitle { get; set; }
        [XmlElement(ElementName = "PrevSalary", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevSalary { get; set; }
        [XmlElement(ElementName = "StartDate", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string StartDate { get; set; }
        [XmlElement(ElementName = "PrevStartDate", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevStartDate { get; set; }
        

    }

    [XmlRoot(ElementName = "Identity", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class Identity
    {
        [XmlElement(ElementName = "SSN", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string SSN { get; set; }
        [XmlElement(ElementName = "Title", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Title { get; set; }
        [XmlElement(ElementName = "FirstName", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleName", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "LastName", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "Suffix", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Suffix { get; set; }
        [XmlElement(ElementName = "DOB", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string DOB { get; set; }
        [XmlElement(ElementName = "PlaceOfBirth", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PlaceOfBirth { get; set; }
        [XmlElement(ElementName = "MothersMaiden", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string MothersMaiden { get; set; }
        [XmlElement(ElementName = "Address", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "City", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string City { get; set; }
        [XmlElement(ElementName = "State", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string State { get; set; }
        [XmlElement(ElementName = "Zip", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "AddressSince", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string AddressSince { get; set; }
        [XmlElement(ElementName = "PrevCity", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevCity { get; set; }
        [XmlElement(ElementName = "PrevState", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrevState { get; set; }
        [XmlElement(ElementName = "HomePhone", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string HomePhone { get; set; }
        [XmlElement(ElementName = "MobilePhone", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string MobilePhone { get; set; }
        [XmlElement(ElementName = "WorkPhone", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string WorkPhone { get; set; }
        [XmlElement(ElementName = "Email", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Email { get; set; }
        [XmlElement(ElementName = "IDType", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string IDType { get; set; }
        [XmlElement(ElementName = "IDOrigin", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string IDOrigin { get; set; }
        [XmlElement(ElementName = "IDNumber", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string IDNumber { get; set; }
        [XmlElement(ElementName = "Employment", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Employment Employment { get; set; }
    }

    [XmlRoot(ElementName = "Application", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class Application
    {
        [XmlElement(ElementName = "Channel", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string Channel { get; set; }
        [XmlElement(ElementName = "AcquisitionMethod", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string AcquisitionMethod { get; set; }
        [XmlElement(ElementName = "AgentLoc", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string AgentLoc { get; set; }
        [XmlElement(ElementName = "SourceIP", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string SourceIP { get; set; }
        [XmlElement(ElementName = "PrimaryDecisionCode", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrimaryDecisionCode { get; set; }
        [XmlElement(ElementName = "SecondaryDecisionCode", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string SecondaryDecisionCode { get; set; }
        [XmlElement(ElementName = "PrimaryPortfolio", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrimaryPortfolio { get; set; }
        [XmlElement(ElementName = "SecondaryPortfolio", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string SecondaryPortfolio { get; set; }
        [XmlElement(ElementName = "SecondaryFraudCode", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string SecondaryFraudCode { get; set; }
        [XmlElement(ElementName = "PrimaryIncome", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string PrimaryIncome { get; set; }
        [XmlElement(ElementName = "InferredIncome", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string InferredIncome { get; set; }
        [XmlElement(ElementName = "RecommendedCreditLine", Namespace = "http://idanalytics.com/products/idscore/request")]
        public string RecommendedCreditLine { get; set; }
    }

    [XmlRoot(ElementName = "IDScoreRequest", Namespace = "http://idanalytics.com/products/idscore/request")]
    public partial class IDScoreRequest
    {
        [XmlElement(ElementName = "Origination", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Origination Origination { get; set; }
        [XmlElement(ElementName = "Identity", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Identity Identity { get; set; }
        [XmlElement(ElementName = "Application", Namespace = "http://idanalytics.com/products/idscore/request")]
        public Application Application { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
    public partial class Item
    {
        [XmlElement(ElementName = "IDScoreRequest", Namespace = "http://idanalytics.com/products/idscore/request")]
        public IDScoreRequest IDScoreRequest { get; set; }
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
    public partial class Body
    {
        [XmlElement(ElementName = "Item", Namespace = "http://idanalytics.com/core/api")]
        public Item Item { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Command", Namespace = "http://idanalytics.com/core/api")]
    public partial class Command
    {
        [XmlElement(ElementName = "Client", Namespace = "http://idanalytics.com/core/api")]
        public string Client { get; set; }
        [XmlElement(ElementName = "GLBACode", Namespace = "http://idanalytics.com/core/api")]
        public string GLBACode { get; set; }
        [XmlElement(ElementName = "Solution", Namespace = "http://idanalytics.com/core/api")]
        public string Solution { get; set; }
        [XmlElement(ElementName = "Credentials", Namespace = "http://idanalytics.com/core/api")]
        public Credentials Credentials { get; set; }
        [XmlElement(ElementName = "ProductSelection", Namespace = "http://idanalytics.com/core/api")]
        public ProductSelection ProductSelection { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://idanalytics.com/core/api")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Body2
    {
        [XmlElement(ElementName = "Command", Namespace = "http://idanalytics.com/core/api")]
        public Command Command { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body2 Body2 { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

}
