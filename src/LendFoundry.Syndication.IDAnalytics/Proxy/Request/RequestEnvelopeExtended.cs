﻿using System;
using System.Linq;

namespace LendFoundry.Syndication.IDAnalytics.Proxy.Request
{
    public partial class Envelope
    {
        public Envelope()
        {

        }
        public Envelope(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            Body2 = new Body2(configuration, request);
        }

       
    }
    public partial class Body2
    {
        public Body2()
        {

        }
        public Body2(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            Command = new Command(configuration, request);
        }
    }
    
    public partial class Product
    {
        public Product()
        {

        }
        public Product(IProductTypeConfiguration productTypeConfiguration)
        {
            Name = productTypeConfiguration.Name;
            ProductID = productTypeConfiguration.ProductID;
        }
    }
    public partial class ProductSelection
    {
        public ProductSelection()
        {

        }
        public ProductSelection(IIDAnalyticsConfiguration configuration)
        {
           Product = configuration.IDAnalyticsProducts.Select(p => new Product(p)).ToList();
 
        }
    }
    public partial class Command
    {
        public Command()
        {

        }
        public Command(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            Credentials = new Credentials(configuration.IdAnalyticsUserName, configuration.IdAnalyticsPassword);
            ProductSelection = new ProductSelection(configuration);
            Solution = configuration.IdAnalyticsSolution;
            Body = new Body(configuration, request);
        }
    }
    public partial class Body
    {
        public Body()
        {

        }
        public Body(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            //var itemtype = new List<ItemType>();
            //itemtype.Add(new ItemType(configuration, request));
            Item = new Item(configuration, request);

        }

    }
    public partial class Item
    {
        public Item()
        {

        }
        public Item(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            Key = configuration.key;
            if (Key.Equals("standardRequest"))
            {
                IDScoreRequest = new IDScoreRequest(configuration, request);
            }
        }
    }
    public partial class IDScoreRequest
    {
        public IDScoreRequest()
        {

        }
        public IDScoreRequest(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            if (configuration.key.Equals("standardRequest"))
            {
               
                Origination = new Origination(configuration, request);
                Identity = new Identity(request);
                Application = new Application(request);
            }
        }
    }
    public partial class Application
    {
        public Application()
        {

        }
        public Application(IIDAnalyticsRequest request)
        {
            Channel = string.IsNullOrEmpty(request.Channel) ? "" : request.Channel;
            AcquisitionMethod = string.IsNullOrEmpty(request.AcquisitionMethod) ? "" : request.AcquisitionMethod;
            AgentLoc = request.AgentLoc;
            SourceIP = request.SourceIP;
            PrimaryDecisionCode = request.PrimaryDecisionCode;
            SecondaryDecisionCode = request.SecondaryDecisionCode;
            PrimaryPortfolio = request.PrimaryPortfolio;
            SecondaryPortfolio = request.SecondaryPortfolio;
            SecondaryFraudCode = request.SecondaryFraudCode;

            InferredIncome = request.InferredIncome;
            RecommendedCreditLine = request.RecommendedCreditLine;
        }
    }
    public partial class Identity
    {
        public Identity()
        {

        }
        public Identity(IIDAnalyticsRequest request)
        {
            SSN = request.SSN;
            Title = request.Title;
            FirstName = request.FirstName;
            MiddleName = request.MiddleName;
            LastName = request.LastName;
            Suffix = request.Suffix;
            DOB = request.DOB;
            PlaceOfBirth = request.PlaceOfBirth;
            MothersMaiden = request.MothersMaiden;
            City = request.City;
            State = request.State;
            Zip = request.Zip;
            AddressSince = request.AddressSince;
            PrevCity = request.PrevCity;
            PrevState = request.PrevState;
            HomePhone = request.HomePhone;
            MobilePhone = request.MobilePhone;
            WorkPhone = request.WorkPhone;
            WorkPhone = request.IDType;
            IDType = request.IDType;
            IDOrigin = request.AddressSince;
            IDNumber = request.AddressSince; ;
            Email = request.Email;
            Address = new Address(request.AddressLine1, request.AddressLine2);
            Employment = new Employment(request);

        }

    }
    public partial class Address
    {
        public Address()
        {

        }
        public Address(string line1, string line2)
        {
            Line1 = line1;
            Line2 = Line2;
        }
    }
    public partial class Employment
    {
        public Employment()
        {

        }
        public Employment(IIDAnalyticsRequest request)
        {
            EmploymentType = request.EmploymentType;
            EmployerType = request.EmployerType;
            Name = request.Name;
            City = request.City;
            State = request.EmploymentState;
            Zip = request.EmploymentZip;
            TimeAtEmployer = request.TimeAtEmployer;
            StartDate = request?.StartDate != null ? Convert.ToDateTime(request.StartDate).ToString("yyyy-MM-dd") : null;
            Title = request.TitleAtEmployment;
            Salary = request.Salary;
            PrevEmployerType = request.PrevEmployerType;
            PrevEmploymentType = request.PrevEmploymentType;
            PrevName = request.PrevName;
            PrevCity = request.PrevCity;
            PrevState = request.PrevState;
            PrevZip = request.PrevZip;
            PrevTimeAtEmployer = request.PrevTimeAtEmployer;
            PrevStartDate = request?.PrevStartDate != null ? Convert.ToDateTime(request.PrevStartDate).ToString("yyyy-MM-dd") : null;
            PrevTitle = request.PrevTitle;
            PrevSalary = request.PrevSalary;
            Address = new Address(request.EmploymentAddressLine1, request.EmploymentAddressLine2);
        }
    }
    public partial class Origination
    {
        public Origination()
        {

        }
        public Origination(IIDAnalyticsConfiguration configuration, IIDAnalyticsRequest request)
        {
            AppID = request.LoanApplicationId.ToString();
           
            RequestType = configuration.IdAnalyticsRequestType;
            ApplicationDate = request.ApplicationDate;
            IndustryType = configuration.IdAnalyticsIndustryType;
            EventType = configuration.IdAnalyticsEventType;
            PassThru1 = configuration.PassThru1;
            PassThru2 = configuration.PassThru2;
            Designation = configuration.Designation;
        }
    }
    public partial class Credentials
    {
        public Credentials()
        {

        }
        public Credentials(string IdAnalyticsUserName, string IdAnalyticsPassword)
        {
            Username = IdAnalyticsUserName;
            Password = IdAnalyticsPassword;
        }
    }
}
