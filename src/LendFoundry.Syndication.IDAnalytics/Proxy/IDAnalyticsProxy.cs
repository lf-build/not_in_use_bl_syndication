﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.IDAnalytics.Proxy.Response;
using RestSharp;
using System;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics.Proxy
{
    public class IDAnalyticsProxy :IIDAnalyticsProxy
    {
        public IDAnalyticsProxy(IIDAnalyticsConfiguration configuration , ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            Configuration = configuration;
        }
        private ILogger Logger { get; }
        private IIDAnalyticsConfiguration Configuration { get; }

       public async Task<Envelope> GetReport(IIDAnalyticsRequest request)
       {

            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            var soapRequest = new Request.Envelope(Configuration, request);
            var baseUri = new Uri(Configuration.Url);
            RestClient client;
            var restRequest = new RestRequest(Method.POST) { RequestFormat = DataFormat.Xml };
            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl) && Configuration.UseProxy)
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
               
                client = new RestClient(uri);
                restRequest.AddHeader("Host", baseUri.Host);
                restRequest.AddHeader("X-Proxy-Certificate", Configuration.IDAnalyticsCertificate);
             }
            else
            {
                client = new RestClient(baseUri);
            }
           
            restRequest.AddParameter("text/xml", XmlSerialization.Serialize<Request.Envelope>(soapRequest), ParameterType.RequestBody);
           // ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var response = await client.ExecuteTaskAsync(restRequest);
            if (response.ErrorException != null)
            {
                throw new IDAnalyticsException(response.ErrorException.StackTrace);
            }
            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new IDAnalyticsException(response.ErrorMessage);

            return XmlSerialization.Deserialize<Response.Envelope>(response.Content);
        }
    }
}
