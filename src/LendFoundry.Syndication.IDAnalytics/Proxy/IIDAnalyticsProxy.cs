﻿using LendFoundry.Syndication.IDAnalytics.Proxy.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics.Proxy
{
   public interface IIDAnalyticsProxy
    {
        Task<Envelope> GetReport(IIDAnalyticsRequest request);
    }
}
