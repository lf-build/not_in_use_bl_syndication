﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.IDAnalytics
{
    public class IDAnalyticsReportPulledFail : SyndicationCalledEvent
    {
    }
}
