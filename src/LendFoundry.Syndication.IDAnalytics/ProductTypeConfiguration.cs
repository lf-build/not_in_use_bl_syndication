﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
    
    public class ProductTypeConfiguration :IProductTypeConfiguration
    {
        public string Name { get; set; }
        public string ProductID { get; set; }
    }
}
