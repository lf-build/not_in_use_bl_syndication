﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.IDAnalytics
{
  public  interface IIDAnalyticsRequest
    {
         string ApplicationDate { get; set; }
         string LoanNumber { get; set; }
         string LoanApplicationId { get; set; }
         string SSN { get; set; }
         string SSNLast4 { get; set; }
         string Title { get; set; }
         string FirstName { get; set; }
         string MiddleName { get; set; }
         string LastName { get; set; }
         string Suffix { get; set; }
        //Optional, Tag Required, YYYY-MM-DD format
         string DOB { get; set; }
         string Gender { get; set; }
         string PlaceOfBirth { get; set; }
         string MothersMaiden { get; set; }
         string AddressLine1 { get; set; }
         string AddressLine2 { get; set; }
         string City { get; set; }
         string State { get; set; }
         string Zip { get; set; }
         string Country { get; set; }
         string AddressSince { get; set; }
         string Occupancy { get; set; }
         string HomePhone { get; set; }
         string MobilePhone { get; set; }
         string WorkPhone { get; set; }
         string Email { get; set; }
         string IDType { get; set; }
         string IDNumber { get; set; }
         string EmployerType { get; set; }
         string EmploymentType { get; set; }
         string Name { get; set; }
         string TitleAtEmployment { get; set; }
         string EmploymentAddressLine1 { get; set; }
         string EmploymentAddressLine2 { get; set; }
         string EmploymentCity { get; set; }
         string EmploymentState { get; set; }
         string EmploymentZip { get; set; }
         string TimeAtEmployer { get; set; }
         string StartDate { get; set; }
         string Salary { get; set; }

         string PrevEmployerType { get; set; }

         string PrevEmploymentType { get; set; }

         string PrevName { get; set; }

         string PrevCity { get; set; }

         string PrevState { get; set; }

         string PrevZip { get; set; }

         string PrevTimeAtEmployer { get; set; }

         string PrevStartDate { get; set; }

         string PrevTitle { get; set; }

         string PrevSalary { get; set; }

        //Application Object

         string Channel { get; set; }

         string AcquisitionMethod { get; set; }

         string AgentLoc { get; set; }

         string SourceIP { get; set; }

         string PrimaryDecisionCode { get; set; }

         string SecondaryDecisionCode { get; set; }

         string PrimaryPortfolio { get; set; }

         string SecondaryPortfolio { get; set; }

         string SecondaryFraudCode { get; set; }

         string PrimaryIncome { get; set; }

         string InferredIncome { get; set; }

         string RecommendedCreditLine { get; set; }

        string AcctLinkKey { get; set; }
    }
}
