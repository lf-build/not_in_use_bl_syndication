﻿using CreditExchange.Syndication.ThirdVista;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace CreditExchange.ThirdVista.Api.Controller
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IThirdVistaService service)
        {
            Service = service;
        }
        private IThirdVistaService Service { get; }
       
         [HttpPost("{entitytype}/{entityid}/thirdvista/registeruser/{customerSessionId}/{email}/{password}")]
        public async Task<IActionResult> RegisterUser(string entityType, string entityId,string customerSessionId, string email, string password)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.RegisterUser(entityType,entityId,customerSessionId, email, password)));
            });
        }
        [HttpPost("{entitytype}/{entityid}/thirdvista/authenticateuser/{email}/{password}")]
        public async Task<IActionResult> AuthenticateUser(string entityType, string entityId,string email, string password)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.AuthenticateUser(entityType,entityId,email, password)));
            });
        }
        [HttpPost("{entitytype}/{entityid}/thirdvista/getaccounts/{sessionId}")]
        public async Task<IActionResult> GetAccounts(string entityType, string entityId,string sessionId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetAccounts(entityType,entityId,sessionId)));
            });
        }
        [HttpPost("{entitytype}/{entityid}/thirdvista/gettransactions/{sessionId}")]
        public async Task<IActionResult> GetTransactions(string entityType, string entityId,string sessionId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetTransactions(entityType,entityId,sessionId)));
            });
        }
        [HttpPost("{entitytype}/{entityid}/thirdvista/GetImageTransactions/{sessionId}")]
        public async Task<IActionResult> GetImageTransactions(string entityType, string entityId,string sessionId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetImageTransactions(entityType,entityId, sessionId)));
            });
        }
        [HttpPost("{entitytype}/{entityid}/thirdvista/getstatementfiles/{sessionId}")]
        public async Task<IActionResult> GetStatementFiles(string entityType, string entityId,string sessionId)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetStatementFiles(entityType,entityId,sessionId)));
            });
        }
    }
}
