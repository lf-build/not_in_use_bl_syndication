﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using LendFoundry.Foundation.Logging;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Services;
using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Syndication.Teletrack;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy;

namespace LendFoundry.Teletrack.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // interface implements
            services.AddConfigurationService<TeletrackConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddTransient<ITeletrackConfiguration>(p =>
            {
                var configuration = p.GetService<IConfigurationService<TeletrackConfiguration>>().Get();
                return configuration;
            });
            services.AddLookupService(Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IInquiryProxy, InquiryProxy>();
            services.AddTransient<ITeletrackService, TeletrackService>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseHealthCheck();

        }
    }
}
