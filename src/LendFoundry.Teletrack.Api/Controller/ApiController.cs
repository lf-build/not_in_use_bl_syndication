﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Teletrack;
using LendFoundry.Syndication.Teletrack.Inquiry;
using LendFoundry.Syndication.Teletrack.Inquiry.Proxy;
using Microsoft.AspNet.Mvc;
using System;

using System.Threading.Tasks;

namespace LendFoundry.Teletrack.Api
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(ITeletrackService teletrackservice)
        {
            TeletrackService = teletrackservice;
        }

        private ITeletrackService TeletrackService { get; }

        [HttpPost("{entityType}/{entityId}/report")]
        public async Task<IActionResult> Getdata(string entityType,string entityId,[FromBody]InquiryRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => TeletrackService.GetData(entityType, entityId, request)));
                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}