﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IBusinessSameAddressRecord
    {
        IAddress address { get; set; }
        List<string> companyNames { get; set; }
        IDunBradstreetRecord dunBradstreetRecord { get; set; }
        IPhoneRecord phoneRecord { get; set; }
    }
}