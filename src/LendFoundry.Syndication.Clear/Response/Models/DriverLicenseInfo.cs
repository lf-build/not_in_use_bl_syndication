﻿
namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class DriverLicenseInfo :IDriverLicenseInfo
    {
        public DriverLicenseInfo()
        {

        }
        public string[] driverLicenseNumber { get; set; }

        public string[] driverLicenseState { get; set; }

        public string[] driverLicenseCounty { get; set; }
    }
}
