﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public   interface IPersonInfo
    {
        IPersonProfile personProfile {get; set;}

        IPersonName personName {get; set;}

        IAddress personAddress {get; set;}

        ISSNInfo sSNInfo {get; set;}

        IPhoneInfo phone {get; set;}

        string biography {get; set;}
    }
}
