﻿

using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class DominantValues:IDominantValues
    {
        public DominantValues()
        {

        }
        public DominantValues(Proxy.Response.CourtResultsPageResultGroupDominantValues dominantValues)
        {
            if (dominantValues !=null)
            {
                CourtDominantValues = new CourtDominantValues(dominantValues.CourtDominantValues);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<ICourtDominantValues, CourtDominantValues>))]
        public ICourtDominantValues CourtDominantValues { get; set; }
    }
}
