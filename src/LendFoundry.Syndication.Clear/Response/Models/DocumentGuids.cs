﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class DocumentGuids: IDocumentGuids
    {
        public DocumentGuids()
        {

        }
        public DocumentGuids(Proxy.Response.CourtResultsPageResultGroupRecordDetailsCourtResponseDetailDocumentGuids documentGuids)
        {
            if (documentGuids!=null)
            {
                SourceName = documentGuids.SourceName;
                SourceDocumentGuids = documentGuids.SourceDocumentGuids;
            }
        }
        public string SourceName { get; set; }
        public string SourceDocumentGuids { get; set; }
    }
}
