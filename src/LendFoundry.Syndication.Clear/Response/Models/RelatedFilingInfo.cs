﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class RelatedFilingInfo 
    {
        public RelatedFilingInfo()
        {

        }
        public string relatedFilingDateField;

        public string relatedFilingDocumentNumberField;

        public string relatedFilingNumberField;

        public string relatedFilingPageCountField;

        public string relatedFilingTimeField;

        public string relatedFilingTypeField;

    }
}
