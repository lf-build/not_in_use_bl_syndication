﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IKnownAddresses
    {
        IAddress Address { get; set; }
        ISourceInfo SourceInfo { get; set; }
    }
}