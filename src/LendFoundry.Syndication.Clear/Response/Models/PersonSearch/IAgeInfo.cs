﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IAgeInfo
    {
        string PersonBirthDate { get; set; }
    }
}