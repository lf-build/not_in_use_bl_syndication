﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class AKANames : IAKANames
    {
        public AKANames(Proxy.Response.PersonSearch.AKANames aKANames)
        {
            if (aKANames != null)
            {
                FirstName = aKANames.FirstName;
                LastName = aKANames.LastName;
                FullName = aKANames.FullName;
            }
        }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }
    }
}
