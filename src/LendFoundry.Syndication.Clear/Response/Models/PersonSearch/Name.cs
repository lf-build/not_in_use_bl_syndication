﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class Name : IName
    {
        public Name(Proxy.Response.PersonSearch.Name name)
        {
            if (name != null)
            {
                FirstName = name.FirstName;
                LastName = name.LastName;
                FullName = name.FullName;
            }
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }
    }
}
