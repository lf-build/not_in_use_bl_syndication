﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IPersonDominantValues
    {
        IAddress Address { get; set; }
        IAgeInfo AgeInfo { get; set; }
        IName Name { get; set; }
        string SSN { get; set; }
    }
}