﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class PersonResponseDetail : IPersonResponseDetail
    {
        public PersonResponseDetail(Proxy.Response.PersonSearch.PersonResponseDetail personResponseDetail)
        {
            if (personResponseDetail != null)
            {
                Name = new Name(personResponseDetail.Name);
                SSN = personResponseDetail.SSN;
                PersonProfile = new PersonProfile(personResponseDetail.PersonProfile);
                AKANames = personResponseDetail.AKANames.Select(a => new AKANames(a)).ToList<IAKANames>();
                EmailAddress = personResponseDetail.EmailAddress;
                KnownAddresses = personResponseDetail.KnownAddresses.Select(a => new KnownAddresses(a)).ToList<IKnownAddresses>();
                AdditionalPhoneNumbers = new AdditionalPhoneNumbers(personResponseDetail.AdditionalPhoneNumbers);
                PersonEntityId = personResponseDetail.PersonEntityId;
                AllSourceDocuments = new AllSourceDocuments(personResponseDetail.AllSourceDocuments);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        public string SSN { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile PersonProfile { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAKANames, AKANames>))]
        public List<IAKANames> AKANames { get; set; }
        public List<string> EmailAddress { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKnownAddresses, KnownAddresses>))]
        public List<IKnownAddresses> KnownAddresses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAdditionalPhoneNumbers, AdditionalPhoneNumbers>))]
        public IAdditionalPhoneNumbers AdditionalPhoneNumbers { get; set; }
        public string PersonEntityId { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAllSourceDocuments, AllSourceDocuments>))]
        public IAllSourceDocuments AllSourceDocuments { get; set; }
    }
}
