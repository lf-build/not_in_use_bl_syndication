﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IDominantValues
    {
        IPersonDominantValues PersonDominantValues { get; set; }
    }
}