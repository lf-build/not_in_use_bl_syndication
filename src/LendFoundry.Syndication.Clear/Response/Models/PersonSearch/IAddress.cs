﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IAddress
    {
        string City { get; set; }
        string Country { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        string ReportedDate { get; set; }
        string State { get; set; }
        string Street { get; set; }
        string ZipCode { get; set; }
    }
}