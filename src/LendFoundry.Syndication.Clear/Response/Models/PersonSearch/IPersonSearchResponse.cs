﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IPersonSearchResponse
    {
        string EndIndex { get; set; }
        IResultGroup ResultGroup { get; set; }
        string StartIndex { get; set; }
        IStatus Status { get; set; }
    }
}