﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class AgeInfo : IAgeInfo
    {
        public AgeInfo(Proxy.Response.PersonSearch.AgeInfo ageInfo)
        {
            if (ageInfo != null)
            {
                PersonBirthDate = ageInfo.PersonBirthDate;
            }
        }
        public string PersonBirthDate { get; set; }
    }

}
