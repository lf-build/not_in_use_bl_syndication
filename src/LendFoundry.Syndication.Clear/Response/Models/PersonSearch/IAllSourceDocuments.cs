﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IAllSourceDocuments
    {
        string SourceDocumentGuid { get; set; }
        string SourceName { get; set; }
    }
}