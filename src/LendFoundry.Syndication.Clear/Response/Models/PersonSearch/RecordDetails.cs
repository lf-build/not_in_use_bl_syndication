﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class RecordDetails : IRecordDetails
    {
        public RecordDetails(Proxy.Response.PersonSearch.RecordDetails recordDetails)
        {
            if (recordDetails != null)
            {
                PersonResponseDetail = new PersonResponseDetail(recordDetails.PersonResponseDetail);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPersonResponseDetail,PersonResponseDetail>))]
        public IPersonResponseDetail PersonResponseDetail { get; set; }
    }
}
