﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{

    public class SourceInfo : ISourceInfo
    {
        public SourceInfo(Proxy.Response.PersonSearch.SourceInfo sourceInfo)
        {
            if (sourceInfo != null)
            {
                SourceName = sourceInfo.SourceName;
                SourceDocumentGuid = sourceInfo.SourceDocumentGuid;
            }
        }
        public string SourceName { get; set; }

        public string SourceDocumentGuid { get; set; }
    }
}
