﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IAdditionalPhoneNumbers
    {
        string PhoneNumber { get; set; }
        ISourceInfo SourceInfo { get; set; }
    }
}