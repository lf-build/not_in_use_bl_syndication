﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IName
    {
        string FirstName { get; set; }
        string FullName { get; set; }
        string LastName { get; set; }
    }
}