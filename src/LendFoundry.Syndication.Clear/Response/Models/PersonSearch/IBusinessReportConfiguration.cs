﻿namespace LendFoundry.Syndication.Clear
{
    public  interface IBusinessReportConfiguration
    {
         bool BusinessOverviewSection { get; set; }
         bool PhoneNumberSection { get; set; }
         bool BusinessSameAddressSection { get; set; }
         bool PeopleSameAddressSection { get; set; }
         bool PeopleSamePhoneSection { get; set; }
         bool BusinessSamePhoneSection { get; set; }
         bool FEINSection { get; set; }
         bool BusinessFinderSection { get; set; }
         bool FictitiousBusinessNameSection { get; set; }
         bool ExecutiveAffiliationSection { get; set; }
         bool ExecutiveProfileSection { get; set; }
         bool CompanyProfileSection { get; set; }
         bool CurrentStockSection { get; set; }
         bool StockPerformanceSection { get; set; }
         bool AnnualFinancialSection { get; set; }
         bool SupplementaryDataSection { get; set; }
         bool GrowthRateSection { get; set; }
         bool FundamentalRatioSection { get; set; }
         bool MoneyServiceBusinessSection { get; set; }
         bool ForeignBusinessStatSection { get; set; }
         bool ExchangeRateSection { get; set; }
         bool DunBradstreetSection { get; set; }
         bool DunBradstreetPCISection { get; set; }
         bool WorldbaseSection { get; set; }
         bool CorporateSection { get; set; }
         bool ExecutiveOfficerSection { get; set; }
         bool BusinessProfileSection { get; set; }
         bool CurrentOfficerSection { get; set; }
         bool PreviousOfficerSection { get; set; }
         bool GlobalSanctionSection { get; set; }
         bool ArrestSection { get; set; }
         bool CriminalSection { get; set; }
         bool ProfessionalLicenseSection { get; set; }
         bool WorldCheckSection { get; set; }
         bool InfractionSection { get; set; }
         bool LawsuitSection { get; set; }
         bool LienJudgmentSection { get; set; }
         bool DocketSection { get; set; }
         bool FederalCaseLawSection { get; set; }
         bool StateCaseLawSection { get; set; }
         bool BankruptcySection { get; set; }
         bool RealPropertySection { get; set; }
         bool PreForeclosureSection { get; set; }
         bool BusinessContactSection { get; set; }
         bool UCCSection { get; set; }
         bool SECFilingSection { get; set; }
         bool RelatedSECFilingRecordSection { get; set; }
         bool OtherSecurityFilingRecordSection { get; set; }
         bool AircraftSection { get; set; }
         bool WatercraftSection { get; set; }
         bool NPISection { get; set; }
         bool HealthcareSanctionSection { get; set; }
         bool ExcludedPartySection { get; set; }
         bool AssociateAnalyticsChartSection { get; set; }
        string ReportChoice { get; set; }
        string Reference { get; set; }
         bool QuickAnalysisFlagSection { get; set; }
        

    }
}
