﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class Status : IStatus
    {
        public Status(Proxy.Response.PersonSearch.Status status)
        {
            if (status != null)
            {
                StatusCode = status.StatusCode;
                SubStatusCode = status.SubStatusCode;
            }
        }
        public string StatusCode { get; set; }

        public string SubStatusCode { get; set; }
    }
}
