﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class AdditionalPhoneNumbers : IAdditionalPhoneNumbers
    {
        public AdditionalPhoneNumbers(Proxy.Response.PersonSearch.AdditionalPhoneNumbers additionalPhoneNumbers)
        {
            if (additionalPhoneNumbers != null)
            {
                PhoneNumber = additionalPhoneNumbers.PhoneNumber;
                SourceInfo = new SourceInfo(additionalPhoneNumbers.SourceInfo);
            }
        }
        public string PhoneNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISourceInfo, SourceInfo>))]
        public ISourceInfo SourceInfo { get; set; }
    }

}
