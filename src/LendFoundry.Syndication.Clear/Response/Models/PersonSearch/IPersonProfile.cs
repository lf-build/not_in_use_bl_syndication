﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IPersonProfile
    {
        List<string> PersonBirthDate { get; set; }
        string PersonDeathDate { get; set; }
        string PersonHeight { get; set; }
        string PersonSex { get; set; }
        string PersonWeight { get; set; }
    }
}