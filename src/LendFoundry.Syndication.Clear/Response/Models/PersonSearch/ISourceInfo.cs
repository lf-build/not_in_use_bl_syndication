﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface ISourceInfo
    {
        string SourceDocumentGuid { get; set; }
        string SourceName { get; set; }
    }
}