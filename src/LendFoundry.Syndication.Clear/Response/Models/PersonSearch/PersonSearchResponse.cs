﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{


    public class PersonSearchResponse : IPersonSearchResponse
    {
        public PersonSearchResponse(Proxy.Response.PersonSearch.PersonSearchResultsPage personSearchResultsPage)
        {
            if (personSearchResultsPage != null)
            {
                Status = new Status(personSearchResultsPage.Status);
                StartIndex = personSearchResultsPage.StartIndex;
                EndIndex = personSearchResultsPage.EndIndex;
                ResultGroup = new ResultGroup(personSearchResultsPage.ResultGroup);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IStatus, Status>))]
        public IStatus Status { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IResultGroup, ResultGroup>))]
        public IResultGroup ResultGroup { get; set; }
      
    }



}
