﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IRecordDetails
    {
        IPersonResponseDetail PersonResponseDetail { get; set; }
    }
}