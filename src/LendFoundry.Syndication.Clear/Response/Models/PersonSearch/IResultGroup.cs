﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IResultGroup
    {
        IDominantValues DominantValues { get; set; }
        string GroupId { get; set; }
        string RecordCount { get; set; }
        IRecordDetails RecordDetails { get; set; }
        string Relevance { get; set; }
    }
}