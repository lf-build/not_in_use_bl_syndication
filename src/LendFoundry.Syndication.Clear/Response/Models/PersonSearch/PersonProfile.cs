﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class PersonProfile: IPersonProfile
    {
        public PersonProfile(Proxy.Response.PersonSearch.PersonProfile personProfile)
        {
            if (personProfile != null)
            {
                PersonBirthDate = personProfile.PersonBirthDate;
                PersonDeathDate = personProfile.PersonDeathDate;
                PersonHeight = personProfile.PersonHeight;
                PersonSex = personProfile.PersonSex;
                PersonWeight = personProfile.PersonWeight;
            }
        }
        public List<string> PersonBirthDate { get; set; }

        public string PersonDeathDate { get; set; }

        public string PersonHeight { get; set; }

        public string PersonSex { get; set; }

        public string PersonWeight { get; set; }
    }
}
