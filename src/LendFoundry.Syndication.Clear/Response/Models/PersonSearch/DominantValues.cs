﻿using LendFoundry.Foundation.Services;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class DominantValues : IDominantValues
    {
        public DominantValues(Proxy.Response.PersonSearch.DominantValues dominantValues)
        {
            if (dominantValues != null)
            {
                PersonDominantValues = new PersonDominantValues(dominantValues.PersonDominantValues);
            }
        }
        [Newtonsoft.Json.JsonConverter(typeof(InterfaceConverter<IPersonDominantValues, PersonDominantValues>))]
        public IPersonDominantValues PersonDominantValues { get; set; }
    }

}
