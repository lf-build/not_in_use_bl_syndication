﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IStatus
    {
        string StatusCode { get; set; }
        string SubStatusCode { get; set; }
    }
}