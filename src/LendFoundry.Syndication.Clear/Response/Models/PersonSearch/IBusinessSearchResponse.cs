﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IBusinessSearchResponse
    {
        string EndIndex { get; set; }
        List<IResultGroup> ResultGroup { get; set; }
        string StartIndex { get; set; }
        IStatus Status { get; set; }
    }
}