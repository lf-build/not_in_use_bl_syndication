﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class PersonDominantValues : IPersonDominantValues
    {
        public PersonDominantValues(Proxy.Response.PersonSearch.PersonDominantValues personDominantValues)
        {
            if (personDominantValues != null)
            {
                Name = new Name(personDominantValues.Name);
                SSN = personDominantValues.SSN;
                AgeInfo = new AgeInfo(personDominantValues.AgeInfo);
                Address = new Address(personDominantValues.Address);


            }
        }
        [JsonConverter(typeof(InterfaceConverter<IName, Name>))]
        public IName Name { get; set; }

        public string SSN { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAgeInfo, AgeInfo>))]
        public IAgeInfo AgeInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

    }
}
