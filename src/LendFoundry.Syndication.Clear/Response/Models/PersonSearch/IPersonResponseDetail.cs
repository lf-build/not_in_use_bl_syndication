﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public interface IPersonResponseDetail
    {
        IAdditionalPhoneNumbers AdditionalPhoneNumbers { get; set; }
        List<IAKANames> AKANames { get; set; }
        IAllSourceDocuments AllSourceDocuments { get; set; }
        List<string> EmailAddress { get; set; }
        List<IKnownAddresses> KnownAddresses { get; set; }
        IName Name { get; set; }
        string PersonEntityId { get; set; }
        IPersonProfile PersonProfile { get; set; }
        string SSN { get; set; }
    }
}