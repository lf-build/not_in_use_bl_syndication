﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{

    public class AllSourceDocuments : IAllSourceDocuments
    {
        public AllSourceDocuments(Proxy.Response.PersonSearch.AllSourceDocuments allSourceDocuments)
        {
            if (allSourceDocuments != null)
            {
                SourceName = allSourceDocuments.SourceName;
                SourceDocumentGuid = allSourceDocuments.SourceDocumentGuid;
            }
        }

        public string SourceName { get; set; }

        public string SourceDocumentGuid { get; set; }
    }
}
