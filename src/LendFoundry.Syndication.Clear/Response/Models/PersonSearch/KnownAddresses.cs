﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonSearch
{
    public class KnownAddresses : IKnownAddresses
    {
        public KnownAddresses(Proxy.Response.PersonSearch.KnownAddresses knownAddresses)
        {
            if (knownAddresses != null)
            {
                Address = new Address(knownAddresses.Address);
                SourceInfo = new SourceInfo(knownAddresses.SourceInfo);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISourceInfo, SourceInfo>))]
        public ISourceInfo SourceInfo { get; set; }
    }
}
