﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IPeopleSameAddressRecord
    {
        IAddress address { get; set; }
        List<IPersonName> personNames { get; set; }
        ISourcePeopleAddressInfo sourcePeopleAddress { get; set; }
    }
}