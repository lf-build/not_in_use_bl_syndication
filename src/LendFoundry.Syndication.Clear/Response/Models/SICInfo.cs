﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class SICInfo : ISICInfo
    {
        public SICInfo()
        {

        }
        public string sICCode { get; set; }

        public string sICDesc { get; set; }

        public string sICExt { get; set; }

        public string nAICSCode { get; set; }

        public string nAICSDesc { get; set; }

    }
}
