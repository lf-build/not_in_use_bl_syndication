﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public  interface IRecordDetails
    {
        ICourtResponseDetail CourtResponseDetail { get; set; }
    }
}
