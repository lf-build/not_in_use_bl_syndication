﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class RecordDetails:IRecordDetails
    {
        public RecordDetails()
        {

        }
        public RecordDetails(Proxy.Response.CourtResultsPageResultGroupRecordDetails recordDetails)
        {
            if (recordDetails.CourtResponseDetail!=null)
            {
                CourtResponseDetail = new CourtResponseDetail(recordDetails.CourtResponseDetail);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<ICourtResponseDetail, CourtResponseDetail>))]
        public ICourtResponseDetail CourtResponseDetail { get; set; }
    }
}
