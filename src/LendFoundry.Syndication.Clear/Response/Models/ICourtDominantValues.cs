﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public  interface ICourtDominantValues
    {
         string Name { get; set; }
         IAddress Address { get; set; }
         string CaseState { get; set; }
         string CaseDate { get; set; }
    }
}
