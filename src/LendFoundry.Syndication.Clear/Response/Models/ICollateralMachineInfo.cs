﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface ICollateralMachineInfo
    {
         string typeOfMachine { get; set; }

         string typeOfSecondaryMachine { get; set; }

         string manufacturer { get; set; }

         string manufacturerYear { get; set; }

         string modelDescription { get; set; }

         string model { get; set; }

         string modelYear { get; set; }

         string modelStatus { get; set; }

         string quantity { get; set; }

         string serialNumber { get; set; }
    }
}