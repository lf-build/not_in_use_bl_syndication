﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class PersonProfile :IPersonProfile
    {
        public PersonProfile()
        {
                
        }
        public string personBirthDate { get; set; }

        public string yearOfBirth { get; set; }

        public string monthOfBirth { get; set; }

        public string dayOfBirth { get; set; }

        public string personAgeTo { get; set; }

        public string personAgeFrom { get; set; }

        public string personDeathIndicator { get; set; }

        public string personDeathDate { get; set; }

        public string personDeathPlace { get; set; }

        public string personBirthPlace { get; set; }

        public string personBirthState { get; set; }

        public string numberofDependents { get; set; }

        public string employeeOccupation { get; set; }

        public string professionalTitle { get; set; }

        public string personAge { get; set; }

        public string personBuild { get; set; }

        public string personCitizenship { get; set; }

        public string personEducationLevel { get; set; }

        public string personEthnicity { get; set; }

        public string personEyeColor { get; set; }

        public string personHairColor { get; set; }

        public string personHeight { get; set; }

        public string personRace { get; set; }

        public string personSex { get; set; }

        public string personSkinTone { get; set; }

        public string personMaritalStatus { get; set; }

        public string personSpouseName { get; set; }

        public string personWeight { get; set; }

        public string personMarkings { get; set; }

        public string personMarkingsLocation { get; set; }

        public string personMarkingsType { get; set; }

        public string personContactEmail { get; set; }
    }
}
