﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDebtorSignerInfo
    {
         string signer { get; set; }

         string signerBusiness { get; set; }

         string signerDescription { get; set; }

         string signerTitle { get; set; }
    }
}
