﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class FilingInfo : IFilingInfo
    {
      //  public IFilingNumberInfo[] filingNumberInfo { get; set; }

       // public IFilingOffice[] filingOffice { get; set; }

        public string[] typeofFiling { get; set; }

        public string[] unlawfulDetainer { get; set; }

        public string[] fileDate { get; set; }

        public IAddress[] filingOfficeAddress { get; set; }

        public string[] releaseDate { get; set; }

        public string[] certificateNumber { get; set; }

        public string[] hiddenFilingNumber { get; set; }

        public string[] iRSSerialNumber { get; set; }

        public string[] orginalFilingNumber { get; set; }

        public string[] originalBook { get; set; }

        public string[] originalPage { get; set; }

        public string court { get; set; }

        public string controlNumber { get; set; }

        public string courtCounty { get; set; }

        public string collectionDate { get; set; }

        public string complaintDate { get; set; }

        public string federalLienNumber { get; set; }

        public string filingOfficeDUNSNumber { get; set; }

        public string maturityDate { get; set; }

        public string multipleDebtors { get; set; }

        public string originalFilingDate { get; set; }

        public string paragraph { get; set; }

        public string perfectedDate { get; set; }

        public string vendorNumber { get; set; }

     //   public ICaseDisposition dispositionInfo { get; set; }

       // public IFilingTypeInfo[] filingTypeInfo { get; set; }

        public string caseDetails { get; set; }

        public string convertDate { get; set; }

        public string demandAmount { get; set; }

        public string dischargeDate { get; set; }

        public string dismissalDate { get; set; }

        public string filingChapter { get; set; }

        public string filingDistrict { get; set; }

        public string filingState { get; set; }

        public string filingStatusFlag { get; set; }

        public string filingStatusTime { get; set; }

        public string finalDecreeDate { get; set; }

        public string keyNatureOfSuit { get; set; }

        public string keyNatureOfSuitCode { get; set; }

        public string otherDockets { get; set; }

        public string otherDocketsTitle { get; set; }

        public string reopenedDate { get; set; }

        public string reterminatedDate { get; set; }

        public string statusSetBy { get; set; }

        public string terminatedDate { get; set; }

        public string caseDispositionFinalDate { get; set; }

        public string documentDescription { get; set; }

        public string documentFormat { get; set; }

        public string documentFiledDate { get; set; }

        public string documentID { get; set; }

        public string documentStatus { get; set; }

        public string statusDate { get; set; }
    }
}
