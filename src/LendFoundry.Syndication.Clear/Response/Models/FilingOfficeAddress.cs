﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class FilingOfficeAddress : IFilingOfficeAddress
    {
        public FilingOfficeAddress(Proxy.Response.FilingOfficeAddress filingOfficeAddress)
        {
            if (filingOfficeAddress!=null)
            {

            }
        }
        public IAddress address { get; set; }

        public string filingOfficeName { get; set; }

        public string filingOfficeLocation { get; set; }
    }
}
