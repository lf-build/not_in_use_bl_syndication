﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class FilingStmtInfo
    {
        public FilingStmtInfo()
        {

        }
        public IBusinessInfo businessInfoField;

     //   public Comments[] commentsField;

        public string contractTypeField;

        public string expireDateField;

        public string fileNumberFullField;

        public string filePagesField;

        public string fileStatusField;

        public string fileTimeField;

        public string filingActionField;

        public string filingMethodField;

        public string filingTerminationField;

        public string filmNumberField;

        public string originalFileNumberFullField;

        public string pageCountField;

        public string pageNumberField;

        public string referenceFileNumberField;

        public string referenceIDField;

        public string relatedFileDateField;

        public string volumeNumberField;
    }
}
