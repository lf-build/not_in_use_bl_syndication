﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IPhoneRecord
    {
        IAddress address { get; set; }
        string addressValidationDate { get; set; }
        string directIndialNumber { get; set; }
        string firstReportedDate { get; set; }
        string lastReportedDate { get; set; }
        string listedInDirectoryAssist { get; set; }
        string mailDeliverable { get; set; }
        string name { get; set; }
        string originalServiceProvider { get; set; }
        string phoneConfidenceScore { get; set; }
        string phoneNumber { get; set; }
        string phoneType { get; set; }
        string recordType { get; set; }
        string source { get; set; }
    }
}