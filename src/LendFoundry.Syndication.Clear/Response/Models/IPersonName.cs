﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IPersonName
    {
         string prefix { get; set; }

         string firstName { get; set; }

        string middleName { get; set; }

        string maidenName { get; set; }

        string lastName { get; set; }

        string suffix { get; set; }

        string secondaryLastName { get; set; }

        string[] fullName { get; set; }
    }
}
