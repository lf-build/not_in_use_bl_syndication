﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class JudgmentInfo
    {
        public JudgmentInfo()
        {
                
        }
        public string awardAmount { get; set; }

        public string judgementPrincipalAmount { get; set; }

        public string paidOnCreditAmount { get; set; }

        public string judgmentTotalAmount { get; set; }

        //public IObligationInfo[] obligationInfo { get; set; }

       // public IStatusInfo statusInfo { get; set; }

        //public ISubjudgmentInfo subjudgmentInfo { get; set; }

    }
}
