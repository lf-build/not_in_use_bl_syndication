﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class PersonInfo : IPersonInfo
    {
        public PersonInfo()
        {
            if (true)
            {

            }
        }
        public IPersonProfile personProfile {get; set;}

        public IPersonName personName {get; set;}

        public IAddress personAddress {get; set;}

        public ISSNInfo sSNInfo {get; set;}

        public IPhoneInfo phone {get; set;}

        public string biography {get; set;}
    }
}
