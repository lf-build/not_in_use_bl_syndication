﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class UserTermRecord : IUserTermRecord
    {
        public UserTermRecord(Proxy.Response.BusinessReportResult.UserTermRecord userTermRecord)
        {
            if (userTermRecord!=null)
            {
                personSearch = new UserSearchInfo(userTermRecord.PersonSearch);
                businessSearch = new UserSearchInfo(userTermRecord.BusinessSearch);
                phoneSearch = new UserSearchInfo(userTermRecord.PhoneSearch);
                licenseSearch = new UserSearchInfo(userTermRecord.LicenseSearch);
                vehicleSearch = new UserSearchInfo(userTermRecord.VehicleSearch);
                realPropertySearch = new UserSearchInfo(userTermRecord.RealPropertySearch);
                courtSearch = new UserSearchInfo(userTermRecord.CourtSearch);
                watercraftSearch = new UserSearchInfo(userTermRecord.WatercraftSearch);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo personSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo businessSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo phoneSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo licenseSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo vehicleSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo realPropertySearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo courtSearch { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUserSearchInfo, UserSearchInfo>))]
        public IUserSearchInfo watercraftSearch { get; set; }


    }
}
