﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public partial class AKAInfo : IAKAInfo
    {
        public AKAInfo(Proxy.Response.BusinessReportResult.AKAInfo aKAInfo)
        {
            if (aKAInfo!=null)
            {
                aKAName = new PersonName(aKAInfo.AKAName);
                nameFirstReported = aKAInfo.NameFirstReported;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName aKAName { get; set; }

        public string nameFirstReported { get; set; }


    }
}
