﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class PeopleSameAddressRecord : IPeopleSameAddressRecord
    {
        public PeopleSameAddressRecord(Proxy.Response.BusinessReportResult.PeopleSameAddressRecord peopleSameAddressRecord)
        {
            if (peopleSameAddressRecord!=null)
            {
                address = new Address(peopleSameAddressRecord.Address);
                personNames = peopleSameAddressRecord.PersonNames.Select(a => new PersonName(a)).ToList<IPersonName>(); ;
                sourcePeopleAddress = new SourcePeopleAddressInfo(peopleSameAddressRecord.SourcePeopleAddress);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> personNames { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISourcePeopleAddressInfo, SourcePeopleAddressInfo>))]
        public ISourcePeopleAddressInfo sourcePeopleAddress { get; set; }

    }
}
