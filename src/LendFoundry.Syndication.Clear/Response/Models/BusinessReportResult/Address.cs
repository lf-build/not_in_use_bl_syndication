﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class Address :IAddress
    {
        public Address(Proxy.Response.BusinessReportResult.Address address)
        {
            if (address!=null)
            {
                street = address.Street;


                streetLine = address.StreetLine;

                streetLine1 = address.StreetLine1;

                streetLine2 = address.StreetLine2;

                streetLine3 = address.StreetLine3;

                pOBox = address.POBox;

                careOf = address.CareOf;

                city = address.City;

                state = address.State;

                stateAbbreviation = address.StateAbbreviation;

                province = address.Province;

                zipCode = address.ZipCode;

                zipCodeExtension = address.ZipCodeExtension;


                country = address.Country;

                continent = address.Continent;

                region = address.Region;

                fullAddress = address.FullAddress;

                typeOfAddress = address.TypeOfAddress;

                additionalInfo = address.AdditionalInfo;

                lastVerifiedDate = address.LastVerifiedDate;

                reportedDate = address.ReportedDate;

                latitude = address.Latitude;

                longitude = address.Longitude;
            }
        }

        public string street { get; set; }

        public List<string> streetLine { get; set; }

        public string streetLine1 { get; set; }

        public string streetLine2 { get; set; }

        public string streetLine3 { get; set; }

        public string pOBox { get; set; }

        public string careOf { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string stateAbbreviation { get; set; }

        public string province { get; set; }

        public string zipCode { get; set; }

        public string zipCodeExtension { get; set; }

        public string country { get; set; }

        public string continent { get; set; }

        public string region { get; set; }

        public string fullAddress { get; set; }

        public string typeOfAddress { get; set; }

        public string additionalInfo { get; set; }

        public string lastVerifiedDate { get; set; }

        public string reportedDate { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }


    }
}
