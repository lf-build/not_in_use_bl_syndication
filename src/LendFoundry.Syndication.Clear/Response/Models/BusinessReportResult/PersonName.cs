﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class PersonName : IPersonName
    {
        public PersonName(Proxy.Response.BusinessReportResult.PersonName personName)
        {
            if (personName!=null)
            {
                prefix = personName.Prefix;

                firstName = personName.FirstName;

                middleName = personName.MaidenName;

                maidenName = personName.MaidenName;

                lastName = personName.LastName;

                suffix = personName.Suffix;

                secondaryLastName = personName.SecondaryLastName;

                fullName = personName.FullName;
            }
        }
        public string prefix { get; set; }

        public string firstName { get; set; }

        public string middleName { get; set; }

        public string maidenName { get; set; }

        public string lastName { get; set; }

        public string suffix { get; set; }

        public string secondaryLastName { get; set; }

        public List<string> fullName { get; set; }
    }
}
