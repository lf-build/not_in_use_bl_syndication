﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class BusinessReportDetailsStatus: IBusinessReportDetailsStatus
    {
        public BusinessReportDetailsStatus(Proxy.Response.BusinessReportResult.Status businessReportDetailsStatus)
        {
            if (businessReportDetailsStatus!=null)
            {
                statusCode = businessReportDetailsStatus.StatusCode;
                subStatusCode = businessReportDetailsStatus.SubStatusCode;
                reference = businessReportDetailsStatus.Reference;
            }
        }

        public int statusCode { get; set; }

        public int subStatusCode { get; set; }

        public string reference { get; set; }
    }
}
