﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class UserSearchInfo : IUserSearchInfo
    {
        public UserSearchInfo(Proxy.Response.BusinessReportResult.UserSearchInfo userSearchInfo)
        {
            if (userSearchInfo!=null)
            {
                 address = new Address(userSearchInfo.Address);
                businessName = userSearchInfo.BusinessName;
                cIK = userSearchInfo.CIK;
                corporationID = userSearchInfo.CorporationID;
                criminalID = userSearchInfo.CriminalID;
                 driverLicense = new DriverLicenseInfo(userSearchInfo.DriverLicense);
                dUNS = userSearchInfo.DUNS;
                federalEmpID = userSearchInfo.FederalEmpID;
                fileTypeCN = userSearchInfo.FileTypeCN;
                fileTypeCN3 = userSearchInfo.FileTypeCN3;
                fileTypeCN5 = userSearchInfo.FileTypeCN5;
                filingDate = userSearchInfo.FilingDate;
                filingNumber = userSearchInfo.FilingNumber;
                hullID = userSearchInfo.HullID;
                licensePlate = userSearchInfo.LicensePlate;
                make = userSearchInfo.Make;
                model = userSearchInfo.Model;
                 name = new PersonName(userSearchInfo.Name);
                nPINumber = userSearchInfo.NPINumber;
                parcelNumber = userSearchInfo.ParcelNumber;
                 personProfile = new PersonProfile(userSearchInfo.PersonProfile);
                 phone = new PhoneInfo(userSearchInfo.Phone);
                profession = userSearchInfo.Profession;
                professionalLicenseNumber = userSearchInfo.ProfessionalLicenseNumber;
                 sSN = new SSNInfo(userSearchInfo.SSN);
                tickerSymbol = userSearchInfo.TickerSymbol;
                titleNumber = userSearchInfo.TitleNumber;
                vesselID = userSearchInfo.VesselID;
                vesselName = userSearchInfo.VesselName;
                vINNumber = userSearchInfo.VINNumber;
                year = userSearchInfo.Year;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName name { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile personProfile { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISSNInfo, SSNInfo>))]
        public ISSNInfo sSN { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public IDriverLicenseInfo driverLicense { get; set; }

        public string nPINumber { get; set; }

        public string businessName { get; set; }

        public string corporationID { get; set; }

        public string filingNumber { get; set; }

        public string filingDate { get; set; }

        public string dUNS { get; set; }

        public string federalEmpID { get; set; }

        public string fileTypeCN { get; set; }

        public string fileTypeCN3 { get; set; }

        public string fileTypeCN5 { get; set; }

        public string cIK { get; set; }

        public string tickerSymbol { get; set; }

        public string profession { get; set; }

        public string professionalLicenseNumber { get; set; }

        public string vINNumber { get; set; }

        public string titleNumber { get; set; }

        public string year { get; set; }

        public string make { get; set; }

        public string model { get; set; }

        public string licensePlate { get; set; }

        public string parcelNumber { get; set; }

        public string criminalID { get; set; }

        public string vesselName { get; set; }

        public string vesselID { get; set; }

        public string hullID { get; set; }

    }
}
