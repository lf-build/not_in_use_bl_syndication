﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ISourcePeopleAddressInfo
    {
        List<IPersonName> additionalHouseholdMembers { get; set; }
        List<string> additionalPhoneNumbers { get; set; }
        List<IAKAInfo> aKAInfo { get; set; }
        IDriverLicenseInfo driverLicenseInfo { get; set; }
        string name { get; set; }
        List<IPeopleAddress> peopleAddress { get; set; }
        IPersonProfile personProfile { get; set; }
        string spouseName { get; set; }
        string sSN { get; set; }
    }
}