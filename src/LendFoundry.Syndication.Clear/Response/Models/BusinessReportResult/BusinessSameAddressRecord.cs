﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public partial class BusinessSameAddressRecord : IBusinessSameAddressRecord
    { 
        public BusinessSameAddressRecord(Proxy.Response.BusinessReportResult.BusinessSameAddressRecord businessSameAddressRecord)
        {
            if (businessSameAddressRecord!=null)
            {
                address = new Address(businessSameAddressRecord.Address);
                companyNames = businessSameAddressRecord.CompanyNames;
                dunBradstreetRecord = new DunBradstreetRecord(businessSameAddressRecord.DunBradstreetRecord);
                phoneRecord = new PhoneRecord(businessSameAddressRecord.PhoneRecord);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        public List<string> companyNames { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDunBradstreetRecord, DunBradstreetRecord>))]
        public IDunBradstreetRecord dunBradstreetRecord { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneRecord, PhoneRecord>))]
        public IPhoneRecord phoneRecord { get; set; }

    }
}
