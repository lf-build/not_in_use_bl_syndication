﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IUserTermsSection
    {
        IUserTermRecord userTermRecord { get; set; }
    }
}