﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IAKAInfo
    {
        IPersonName aKAName { get; set; }
        string nameFirstReported { get; set; }
    }
}