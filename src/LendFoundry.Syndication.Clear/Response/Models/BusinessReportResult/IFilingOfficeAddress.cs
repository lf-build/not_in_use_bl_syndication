﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IFilingOfficeAddress
    {
         IAddress address { get; set; }

         string filingOfficeName { get; set; }

         string filingOfficeLocation { get; set; }
    }
}