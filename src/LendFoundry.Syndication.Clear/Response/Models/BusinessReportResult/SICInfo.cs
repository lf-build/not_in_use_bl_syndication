﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class SICInfo : ISICInfo
    {
        public SICInfo(Proxy.Response.BusinessReportResult.SICInfo sICInfo)
        {
            if (sICInfo!=null)
            {
                sICCode = sICInfo.SICCode;

                sICDesc = sICInfo.SICDesc;

                sICExt = sICInfo.SICExt;

                nAICSCode = sICInfo.NAICSCode;

                nAICSDesc = sICInfo.NAICSDesc;
            }
        }
        public string sICCode { get; set; }

        public string sICDesc { get; set; }

        public string sICExt { get; set; }

        public string nAICSCode { get; set; }

        public string nAICSDesc { get; set; }

    }
}
