﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class BusinessReportDetails: IBusinessReportDetails
    {
        public BusinessReportDetails(Proxy.Response.BusinessReportResult.BusinessReportDetails businessReportDetails)
        {
            if (businessReportDetails != null)
            {
                Status = new BusinessReportDetailsStatus(businessReportDetails.Status);
                SectionResults = businessReportDetails.SectionResults.Select(a => new BusinessReportDetailsSectionResults(a)).ToList<IBusinessReportDetailsSectionResults>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IBusinessReportDetailsStatus, BusinessReportDetailsStatus>))]
        public IBusinessReportDetailsStatus Status { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBusinessReportDetailsSectionResults, BusinessReportDetailsSectionResults>))]
        public List<IBusinessReportDetailsSectionResults> SectionResults { get; set; }

    }
}
