﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class PhoneRecord : IPhoneRecord
    {
        public PhoneRecord(Proxy.Response.BusinessReportResult.PhoneRecord phoneRecord)
        {
            if (phoneRecord!=null)
            {
                name = phoneRecord.Name;

                phoneNumber = phoneRecord.PhoneNumber;

                phoneType = phoneRecord.PhoneType;

                directIndialNumber = phoneRecord.DirectIndialNumber;

                recordType = phoneRecord.RecordType;

                firstReportedDate = phoneRecord.FirstReportedDate;

                lastReportedDate = phoneRecord.LastReportedDate;

                originalServiceProvider = phoneRecord.OriginalServiceProvider;

                listedInDirectoryAssist = phoneRecord.ListedInDirectoryAssist;

                phoneConfidenceScore = phoneRecord.PhoneConfidenceScore;

                 address = new Address(phoneRecord.Address);

                mailDeliverable = phoneRecord.MailDeliverable;

                addressValidationDate = phoneRecord.AddressValidationDate;

                source = phoneRecord.Source;
            }
        }

        public string name { get; set; }

        public string phoneNumber { get; set; }

        public string phoneType { get; set; }

        public string directIndialNumber { get; set; }

        public string recordType { get; set; }

        public string firstReportedDate { get; set; }

        public string lastReportedDate { get; set; }

        public string originalServiceProvider { get; set; }

        public string listedInDirectoryAssist { get; set; }

        public string phoneConfidenceScore { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        public string mailDeliverable { get; set; }

        public string addressValidationDate { get; set; }

        public string source { get; set; }

    }
}
