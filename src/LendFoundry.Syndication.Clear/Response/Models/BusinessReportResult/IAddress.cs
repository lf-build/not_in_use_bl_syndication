﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IAddress
    {
        string additionalInfo { get; set; }
        string careOf { get; set; }
        string city { get; set; }
        string continent { get; set; }
        string country { get; set; }
        string fullAddress { get; set; }
        string lastVerifiedDate { get; set; }
        string latitude { get; set; }
        string longitude { get; set; }
        string pOBox { get; set; }
        string province { get; set; }
        string region { get; set; }
        string reportedDate { get; set; }
        string state { get; set; }
        string stateAbbreviation { get; set; }
        string street { get; set; }
        List<string> streetLine { get; set; }
        string streetLine1 { get; set; }
        string streetLine2 { get; set; }
        string streetLine3 { get; set; }
        string typeOfAddress { get; set; }
        string zipCode { get; set; }
        string zipCodeExtension { get; set; }
    }
}