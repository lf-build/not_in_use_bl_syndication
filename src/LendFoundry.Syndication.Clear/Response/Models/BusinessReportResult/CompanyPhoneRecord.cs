﻿
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class CompanyPhoneRecord: ICompanyPhoneRecord
    {
        public CompanyPhoneRecord(Proxy.Response.BusinessReportResult.CompanyPhoneRecord companyPhoneRecord)
        {
            if (companyPhoneRecord!=null)
            {
                companyPhoneInfo= companyPhoneRecord.CompanyPhoneInfo.Select(c=> new CompanyPhoneInfo(c)).ToList<ICompanyPhoneInfo>();
                sourcePhoneInfo = new SourcePhoneInfo(companyPhoneRecord.SourcePhoneInfo);
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<ICompanyPhoneInfo, CompanyPhoneInfo>))]
        public List<ICompanyPhoneInfo> companyPhoneInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISourcePhoneInfo, SourcePhoneInfo>))]
        public ISourcePhoneInfo sourcePhoneInfo { get; set; }

    }

}
