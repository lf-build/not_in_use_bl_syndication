﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ICompanyPhoneRecord
    {
        List<ICompanyPhoneInfo> companyPhoneInfo { get; set; }
        ISourcePhoneInfo sourcePhoneInfo { get; set; }
    }
}