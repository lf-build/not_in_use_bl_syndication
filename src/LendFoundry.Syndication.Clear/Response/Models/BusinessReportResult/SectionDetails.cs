﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class SectionDetails :ISectionDetails
    {
        public SectionDetails(Proxy.Response.BusinessReportResult.SectionDetails sectionDetails)
        {
            if (sectionDetails!=null)
            {
                userTermsSection = new UserTermsSection(sectionDetails.UserTermsSection);
                businessOverviewSection = sectionDetails.BusinessOverviewSection.Select(s => new BusinessOverviewRecord(s)).ToList<IBusinessOverviewRecord>();
                phoneNumberSection = sectionDetails.PhoneNumberSection.Select(s => new CompanyPhoneRecord(s)).ToList<ICompanyPhoneRecord>();
                businessSameAddressSection = sectionDetails.BusinessSameAddressSection.Select(s => new BusinessSameAddressRecord(s)).ToList<IBusinessSameAddressRecord>();
                peopleSameAddressSection = sectionDetails.PeopleSameAddressSection.Select(s => new PeopleSameAddressRecord(s)).ToList<IPeopleSameAddressRecord>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IUserTermsSection, UserTermsSection>))]
        public IUserTermsSection userTermsSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessOverviewRecord, BusinessOverviewRecord>))]
        public List<IBusinessOverviewRecord> businessOverviewSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICompanyPhoneRecord, CompanyPhoneRecord>))]
        public List<ICompanyPhoneRecord> phoneNumberSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessSameAddressRecord, BusinessSameAddressRecord>))]
        public List<IBusinessSameAddressRecord> businessSameAddressSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPeopleSameAddressRecord, PeopleSameAddressRecord>))]
        public List<IPeopleSameAddressRecord> peopleSameAddressSection { get; set; }


    }
}
