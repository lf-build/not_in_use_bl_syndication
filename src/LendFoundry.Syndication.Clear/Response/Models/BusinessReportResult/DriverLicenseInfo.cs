﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class DriverLicenseInfo: IDriverLicenseInfo
    {
        public DriverLicenseInfo(Proxy.Response.BusinessReportResult.DriverLicenseInfo driverLicenseInfo)
        {
            if (driverLicenseInfo!=null)
            {
                driverLicenseNumber = driverLicenseInfo.DriverLicenseNumber;
                driverLicenseState = driverLicenseInfo.DriverLicenseState;
                driverLicenseCounty = driverLicenseInfo.DriverLicenseCountry;
            }
        }
        public List<string> driverLicenseNumber { get; set; }

        public List<string> driverLicenseState { get; set; }

        public List<string> driverLicenseCounty { get; set; }

    }
}
