﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IPersonProfile
    {
        List<string> dayOfBirth { get; set; }
        List<string> employeeOccupation { get; set; }
        List<string> monthOfBirth { get; set; }
        List<string> numberofDependents { get; set; }
        List<string> personAge { get; set; }
        List<string> personAgeFrom { get; set; }
        List<string> personAgeTo { get; set; }
        List<string> personBirthDate { get; set; }
        List<string> personBirthPlace { get; set; }
        List<string> personBirthState { get; set; }
        List<string> personBuild { get; set; }
        List<string> personCitizenship { get; set; }
        List<string> personContactEmail { get; set; }
        List<string> personDeathDate { get; set; }
        List<string> personDeathIndicator { get; set; }
        List<string> personDeathPlace { get; set; }
        List<string> personEducationLevel { get; set; }
        List<string> personEthnicity { get; set; }
        List<string> personEyeColor { get; set; }
        List<string> personHairColor { get; set; }
        List<string> personHeight { get; set; }
        List<string> personMaritalStatus { get; set; }
        List<string> personMarkings { get; set; }
        List<string> personMarkingsLocation { get; set; }
        List<string> personMarkingsType { get; set; }
        List<string> personRace { get; set; }
        List<string> personSex { get; set; }
        List<string> personSkinTone { get; set; }
        List<string> personSpouseName { get; set; }
        List<string> personWeight { get; set; }
        List<string> professionalTitle { get; set; }
        List<string> yearOfBirth { get; set; }
    }
}