﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class FilingOfficeAddress: IFilingOfficeAddress
    {
        public FilingOfficeAddress(Proxy.Response.BusinessReportResult.FilingOfficeAddress filingOfficeAddress)
        {
            if (filingOfficeAddress!=null)
            {
                address = new Address(filingOfficeAddress.Address);
                filingOfficeName = filingOfficeAddress.FilingOfficeLocation;
                filingOfficeLocation = filingOfficeAddress.FilingOfficeLocation;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        public string filingOfficeName { get; set; }

        public string filingOfficeLocation { get; set; }

    }
}
