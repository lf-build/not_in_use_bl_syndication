﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class PeopleAddress : IPeopleAddress
    {
        public PeopleAddress(Proxy.Response.BusinessReportResult.PeopleAddress peopleAddress)
        {
            if (peopleAddress!=null)
            {
                street = peopleAddress.Street;
                cityStateZip = peopleAddress.CityStateZip;
                phone = peopleAddress.Phone;
                reportedDate = peopleAddress.ReportedDate;
            }
              

        }
        public string street { get; set; }

        public string cityStateZip { get; set; }

        public List<string> phone { get; set; }

        public string reportedDate { get; set; }

    }
}
