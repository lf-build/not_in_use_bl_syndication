﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IPeopleAddress
    {
        string cityStateZip { get; set; }
        List<string> phone { get; set; }
        string reportedDate { get; set; }
        string street { get; set; }
    }
}