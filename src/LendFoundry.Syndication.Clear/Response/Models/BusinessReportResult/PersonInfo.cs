﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class PersonInfo: IPersonInfo
    {
        public PersonInfo(Proxy.Response.BusinessReportResult.PersonInfo personInfo)
        {
            if (personInfo!=null)
            {
                address = new Address(personInfo.Address);
                nameType = personInfo.NameType;
                personName=new PersonName(personInfo.PersonName);
                phoneNumber = new PhoneInfo(personInfo.PhoneNumber);
                personProfile = new PersonProfile(personInfo.PersonProfile);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        public string nameType { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phoneNumber { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile personProfile { get; set; }

    }
}
