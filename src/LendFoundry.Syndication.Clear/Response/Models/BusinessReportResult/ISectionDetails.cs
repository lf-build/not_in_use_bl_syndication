﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ISectionDetails
    {
        List<IBusinessOverviewRecord> businessOverviewSection { get; set; }
        List<IBusinessSameAddressRecord> businessSameAddressSection { get; set; }
        List<IPeopleSameAddressRecord> peopleSameAddressSection { get; set; }
        List<ICompanyPhoneRecord> phoneNumberSection { get; set; }
        IUserTermsSection userTermsSection { get; set; }
    }
}