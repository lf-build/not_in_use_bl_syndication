﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class DunBradstreetRecord : IDunBradstreetRecord
    {
        public DunBradstreetRecord(Proxy.Response.BusinessReportResult.DunBradstreetRecord dunBradstreetRecord)
        {
            if (dunBradstreetRecord != null)
            {
                businessInfo = new BusinessInfo(dunBradstreetRecord.BusinessInfo);

                establishedManufacturingIndicator = dunBradstreetRecord.EstablishedManufacturingIndicator;

                personInfo = dunBradstreetRecord.PersonInfo.Select(p => new PersonInfo(p)).ToList<IPersonInfo>();

                foreignOwned = dunBradstreetRecord.ForeignOwned;

                homeOrCottageBusiness = dunBradstreetRecord.HomeOrCottageBusiness;

                incorporationDate = dunBradstreetRecord.IncorporationDate;

                latestSales = dunBradstreetRecord.LatestSales;

                manufacturingLocation = dunBradstreetRecord.ManufacturingLocation;

                minorityOwned = dunBradstreetRecord.MinorityOwned;

                numberOfEmployeesHere = dunBradstreetRecord.NumberOfEmployeesHere;

                occupancyType = dunBradstreetRecord.OccupancyType;

                publicallyOwned = dunBradstreetRecord.PublicallyOwned;

                revisionDate = dunBradstreetRecord.RevisionDate;

               additionalSIC = new AdditionalSIC(dunBradstreetRecord.AdditionalSIC);

                secondaryCompanyName = dunBradstreetRecord.SecondaryCompanyName;

                smallBusiness = dunBradstreetRecord.SmallBusiness;

                squareFootageOfBuilding = dunBradstreetRecord.SquareFootageOfBuilding;

                stateOfIncorporation = dunBradstreetRecord.StateOfIncorporation;

                yearStarted = dunBradstreetRecord.YearStarted;

                lineOfBusiness = dunBradstreetRecord.LineOfBusiness;

                industryGroup = dunBradstreetRecord.IndustryGroup;

                sales1YrAgo = dunBradstreetRecord.Sales1YrAgo;

                sales3YrAgo = dunBradstreetRecord.Sales3YrAgo;

                salesGrowth = dunBradstreetRecord.SalesGrowth;

                salesTerritory = dunBradstreetRecord.SalesTerritory;

                numberOfAccounts = dunBradstreetRecord.NumberOfAccounts;

                netWorth = dunBradstreetRecord.NetWorth;

                employeeTotal = dunBradstreetRecord.EmployeeTotal;

                empTotal3YrAgo = dunBradstreetRecord.EmpTotal3YrAgo;

                empTotal1YrAgo = dunBradstreetRecord.EmpTotal1YrAgo;

                employmentGrowth = dunBradstreetRecord.EmploymentGrowth;

                mSACode = dunBradstreetRecord.MSACode;

                mSAName = dunBradstreetRecord.MSAName;

                bankName = dunBradstreetRecord.BankName;

                bankDUNSNumber = dunBradstreetRecord.BankDUNSNumber;

                accountingFirm = dunBradstreetRecord.AccountingFirm;

                parentCompanyName = dunBradstreetRecord.ParentCompanyName;

                parentCompanyDUNS = dunBradstreetRecord.ParentCompanyDUNS;

                ultimateCompanyName = dunBradstreetRecord.UltimateCompanyName;

                ultimateCompanyDUNS = dunBradstreetRecord.UltimateCompanyDUNS;

                headquartersName = dunBradstreetRecord.HeadquartersName;

                headquartersDUNS = dunBradstreetRecord.HeadquartersDUNS;

                otherDUNS = dunBradstreetRecord.OtherDUNS;

                recordUpdateDate = dunBradstreetRecord.RecordUpdateDate;

                source = dunBradstreetRecord.Source;
    }
}


        [JsonConverter(typeof(InterfaceConverter<IBusinessInfo, BusinessInfo>))]
        public IBusinessInfo businessInfo { get; set; }

        public string establishedManufacturingIndicator { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonInfo, PersonInfo>))]
        public List<IPersonInfo> personInfo { get; set; }

        public string foreignOwned { get; set; }

        public string homeOrCottageBusiness { get; set; }

        public string incorporationDate { get; set; }

        public string latestSales { get; set; }

        public string manufacturingLocation { get; set; }

        public string minorityOwned { get; set; }

        public string numberOfEmployeesHere { get; set; }

        public string occupancyType { get; set; }

        public string publicallyOwned { get; set; }

        public string revisionDate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAdditionalSIC, AdditionalSIC>))]
        public IAdditionalSIC additionalSIC { get; set; }

        public string secondaryCompanyName { get; set; }

        public string smallBusiness { get; set; }

        public string squareFootageOfBuilding { get; set; }

        public string stateOfIncorporation { get; set; }

        public string yearStarted { get; set; }

        public string lineOfBusiness { get; set; }

        public string industryGroup { get; set; }

        public string sales1YrAgo { get; set; }

        public string sales3YrAgo { get; set; }

        public string salesGrowth { get; set; }

        public string salesTerritory { get; set; }

        public string numberOfAccounts { get; set; }

        public string netWorth { get; set; }

        public string employeeTotal { get; set; }

        public string empTotal3YrAgo { get; set; }

        public string empTotal1YrAgo { get; set; }

        public string employmentGrowth { get; set; }

        public string mSACode { get; set; }

        public string mSAName { get; set; }

        public string bankName { get; set; }

        public string bankDUNSNumber { get; set; }

        public string accountingFirm { get; set; }

        public string parentCompanyName { get; set; }

        public string parentCompanyDUNS { get; set; }

        public string ultimateCompanyName { get; set; }

        public string ultimateCompanyDUNS { get; set; }

        public string headquartersName { get; set; }

        public string headquartersDUNS { get; set; }

        public string otherDUNS { get; set; }

        public string recordUpdateDate { get; set; }

        public string source { get; set; }

    }
}
