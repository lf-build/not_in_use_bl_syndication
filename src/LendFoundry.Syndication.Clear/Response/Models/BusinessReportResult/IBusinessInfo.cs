﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
  public  interface IBusinessInfo
    {
         IAddress address { get; set; }

         IAddress primaryAddress { get; set; }

         IAddress mailingAddress { get; set; }

         string corpBusinessStatus { get; set; }

         string dUNSNumber { get; set; }

         string filingType { get; set; }

         string filingDate { get; set; }

         string filingNumber { get; set; }

         string filingState { get; set; }

         string origFilingDate { get; set; }

         string origFilingNumber { get; set; }

         string taxId { get; set; }

         string federalEmpID { get; set; }

         List<ISICInfo> primarySIC { get; set; }

         string sICCode { get; set; }

         string sICDesc { get; set; }

         string sICExt { get; set; }

         string businessName { get; set; }

         IPhoneInfo phoneInfo { get; set; }

         IFilingOfficeAddress filingOfficeAddress { get; set; }

         string businessLocationType { get; set; }

         string businessEmail { get; set; }

         string uRL { get; set; }

         string businessNameShort { get; set; }

         string businessNumber { get; set; }

         string businessDescription { get; set; }

         string summaryBusinessDescription { get; set; }

         string numberOfEmployees { get; set; }

         string operatingStatus { get; set; }

         string yearStarted { get; set; }

         string nationalID { get; set; }

         string secondaryBusinessName { get; set; }
    }
}