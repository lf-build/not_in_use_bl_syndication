﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IUserTermRecord
    {
        IUserSearchInfo businessSearch { get; set; }
        IUserSearchInfo courtSearch { get; set; }
        IUserSearchInfo licenseSearch { get; set; }
        IUserSearchInfo personSearch { get; set; }
        IUserSearchInfo phoneSearch { get; set; }
        IUserSearchInfo realPropertySearch { get; set; }
        IUserSearchInfo vehicleSearch { get; set; }
        IUserSearchInfo watercraftSearch { get; set; }
    }
}