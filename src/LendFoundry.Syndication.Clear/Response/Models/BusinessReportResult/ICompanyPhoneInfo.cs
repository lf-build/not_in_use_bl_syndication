﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ICompanyPhoneInfo
    {
        string companyName { get; set; }
        string phoneNumber { get; set; }
    }
}