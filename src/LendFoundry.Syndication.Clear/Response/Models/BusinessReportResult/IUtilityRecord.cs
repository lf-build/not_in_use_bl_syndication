﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IUtilityRecord
    {
        IAddress billingAddress { get; set; }
        IDriverLicenseInfo driverLicenseInfo { get; set; }
        IPersonName personName { get; set; }
        IPhoneInfo phoneInfo { get; set; }
        string serviceConnectDate { get; set; }
        string source { get; set; }
        ISSNInfo sSNInfo { get; set; }
        IAddress utilityAddress { get; set; }
        string utilityReportedDate { get; set; }
        string[] utilityService { get; set; }
    }
}