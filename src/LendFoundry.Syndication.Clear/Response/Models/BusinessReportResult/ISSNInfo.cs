﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ISSNInfo
    {
        List<string> partialSSN { get; set; }
        List<string> SSN { get; set; }
        string SSNExpirationDate { get; set; }
        string SSNIssuanceText { get; set; }
        string SSNIssueDate { get; set; }
        string SSNIssueState { get; set; }
    }
}