﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IDriverLicenseInfo
    {
        List<string> driverLicenseCounty { get; set; }
        List<string> driverLicenseNumber { get; set; }
        List<string> driverLicenseState { get; set; }
    }
}