﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IBusinessReportDetails
    {
        IBusinessReportDetailsStatus Status { get; set; }
         List<IBusinessReportDetailsSectionResults> SectionResults { get; set; }

    }
}