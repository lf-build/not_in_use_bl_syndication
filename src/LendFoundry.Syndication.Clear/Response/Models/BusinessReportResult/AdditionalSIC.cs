﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public class AdditionalSIC: IAdditionalSIC
    {
        public AdditionalSIC(Proxy.Response.BusinessReportResult.AdditionalSIC additionalSIC)
        {
            if (additionalSIC!=null)
            {
                sICInfo2 = additionalSIC.SICInfo2.Select(s => new SICInfo(s)).ToList<ISICInfo>();
                sICInfo3 = additionalSIC.SICInfo2.Select(s => new SICInfo(s)).ToList<ISICInfo>();
                sICInfo4 = additionalSIC.SICInfo2.Select(s => new SICInfo(s)).ToList<ISICInfo>();
                sICInfo5 = additionalSIC.SICInfo2.Select(s => new SICInfo(s)).ToList<ISICInfo>();
                sICInfo6 = additionalSIC.SICInfo2.Select(s => new SICInfo(s)).ToList<ISICInfo>();
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> sICInfo2 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> sICInfo3 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> sICInfo4 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> sICInfo5 { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> sICInfo6 { get; set; }

    }
}
