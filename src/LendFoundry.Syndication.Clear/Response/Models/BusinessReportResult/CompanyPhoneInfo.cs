﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class CompanyPhoneInfo : ICompanyPhoneInfo
    {
        public CompanyPhoneInfo(Proxy.Response.BusinessReportResult.CompanyPhoneInfo companyProfileInfo)
        {
            if (companyProfileInfo!=null)
            {
                companyName = companyProfileInfo.CompanyName;
                phoneNumber = companyProfileInfo.PhoneNumber;
            }
        }
        public string companyName { get; set; }

        public string phoneNumber { get; set; }

    }
}
