﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public partial class SourcePeopleAddressInfo : ISourcePeopleAddressInfo
    {
        public SourcePeopleAddressInfo(Proxy.Response.BusinessReportResult.SourcePeopleAddressInfo sourcePeopleAddressInfo)
        {
            if (sourcePeopleAddressInfo!=null)
            {
                name = sourcePeopleAddressInfo.Name;
                sSN = sourcePeopleAddressInfo.SSN;
                personProfile =new PersonProfile (sourcePeopleAddressInfo.PersonProfile);
                driverLicenseInfo = new DriverLicenseInfo(sourcePeopleAddressInfo.DriverLicenseInfo);
                aKAInfo = sourcePeopleAddressInfo.AKAInfo.Select(a => new AKAInfo(a)).ToList<IAKAInfo>();
                spouseName = sourcePeopleAddressInfo.SpouseName;
                peopleAddress = sourcePeopleAddressInfo.PeopleAddress.Select(a => new PeopleAddress(a)).ToList<IPeopleAddress>();
                additionalPhoneNumbers = sourcePeopleAddressInfo.AdditionalPhoneNumbers;
                additionalHouseholdMembers = sourcePeopleAddressInfo.AdditionalHouseholdMembers.Select(a=>new PersonName(a)).ToList<IPersonName>();
            }
        }
        public string name { get; set; }

        public string sSN { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonProfile,PersonProfile>))]
        public IPersonProfile personProfile { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public IDriverLicenseInfo driverLicenseInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAKAInfo, AKAInfo>))]
        public List<IAKAInfo> aKAInfo { get; set; }

        public string spouseName { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPeopleAddress, PeopleAddress>))]
        public List<IPeopleAddress> peopleAddress { get; set; }

        public List<string> additionalPhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> additionalHouseholdMembers { get; set; }


    }
}
