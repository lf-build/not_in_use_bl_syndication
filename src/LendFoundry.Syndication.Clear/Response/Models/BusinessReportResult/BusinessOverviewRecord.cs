﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class BusinessOverviewRecord: IBusinessOverviewRecord
    {
        public BusinessOverviewRecord(Proxy.Response.BusinessReportResult.BusinessOverviewRecord businessOverviewRecord)
        {
            if (businessOverviewRecord!=null)
            {
                address =businessOverviewRecord.Address.Select(a=>new Address(a)).ToList<IAddress>();
                alternateCompanyName = businessOverviewRecord.AlternateCompanyName;
                businessDescription = businessOverviewRecord.BusinessDescription;
                companyContact = businessOverviewRecord.CompanyContact;
                companyName = businessOverviewRecord.CompanyName;
                contactTitle = businessOverviewRecord.ContactTitle;
                corporationStatus = businessOverviewRecord.CorporationStatus;
                currentOutstandingShares = businessOverviewRecord.CurrentOutstandingShares;
                dateofIncorporation = businessOverviewRecord.DateofIncorporation;
                dUNS = businessOverviewRecord.DUNS;
                fEIN = businessOverviewRecord.FEIN;
                fiscalYearEnd = businessOverviewRecord.FiscalYearEnd;
                lastAnnualFinancialDate = businessOverviewRecord.LastAnnualFinancialDate;
                legalImmediateParent = businessOverviewRecord.LegalImmediateParent;
                legalImmediateParentIndicator = businessOverviewRecord.LegalImmediateParentIndicator;
                legalUltimateParent = businessOverviewRecord.LegalImmediateParent;
                legalUltimateParentIndicator = businessOverviewRecord.LegalUltimateParentIndicator;
                numberofEmployees = businessOverviewRecord.NumberofEmployees;
                primarySICCode = businessOverviewRecord.PrimarySICCode;
                registeredAgent = businessOverviewRecord.RegisteredAgent;
                shareholders = businessOverviewRecord.Shareholders;
                stateofIncorporation = businessOverviewRecord.StateofIncorporation;
                tickerSymbol = businessOverviewRecord.TickerSymbol;
                webAddress = businessOverviewRecord.WebAddress;
                yearEstablished = businessOverviewRecord.YearEstablished;
            }
        }
        public string companyName { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address { get; set; }

        public List<string> alternateCompanyName { get; set; }

        public string tickerSymbol { get; set; }

        public List<string> stateofIncorporation { get; set; }

        public List<string> dateofIncorporation { get; set; }

        public string yearEstablished { get; set; }

        public string corporationStatus { get; set; }

        public string primarySICCode { get; set; }

        public List<string> dUNS { get; set; }

        public List<string> fEIN { get; set; }

        public string currentOutstandingShares { get; set; }

        public string shareholders { get; set; }

        public string numberofEmployees { get; set; }

        public string fiscalYearEnd { get; set; }

        public string lastAnnualFinancialDate { get; set; }

        public List<string> registeredAgent { get; set; }

        public string webAddress { get; set; }

        public string businessDescription { get; set; }

        public string companyContact { get; set; }

        public string contactTitle { get; set; }

        public string legalUltimateParentIndicator { get; set; }

        public string legalImmediateParentIndicator { get; set; }

        public string legalUltimateParent { get; set; }

        public string legalImmediateParent { get; set; }


    }
}
