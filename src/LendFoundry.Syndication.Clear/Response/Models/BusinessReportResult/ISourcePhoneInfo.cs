﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface ISourcePhoneInfo
    {
        IAddress address { get; set; }
        string businessContact { get; set; }
        string contactForLocation { get; set; }
        string name { get; set; }
        string officerName { get; set; }
        string phoneNumber { get; set; }
        string source { get; set; }
        string title { get; set; }
    }
}