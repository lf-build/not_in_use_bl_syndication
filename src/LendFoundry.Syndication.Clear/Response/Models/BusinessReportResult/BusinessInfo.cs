﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public partial class BusinessInfo : IBusinessInfo
    {
        public BusinessInfo(Proxy.Response.BusinessReportResult.BusinessInfo businessInfo)
        {
            if (businessInfo!=null)
            {
                address = new Address(businessInfo.Address);

                 primaryAddress = new Address(businessInfo.PrimaryAddress);

                mailingAddress = new Address(businessInfo.MailingAddress);

                corpBusinessStatus = businessInfo.CorpBusinessStatus;

                dUNSNumber = businessInfo.DUNSNumber;

                filingType = businessInfo.FilingType;

                filingDate = businessInfo.FilingDate;

                filingNumber = businessInfo.FilingNumber;

                filingState = businessInfo.FilingState;

                origFilingDate = businessInfo.OrigFilingDate;

                origFilingNumber = businessInfo.OrigFilingNumber;

                taxId = businessInfo.TaxId;

                federalEmpID = businessInfo.FederalEmpID;

                primarySIC = businessInfo.PrimarySIC.Select(s=>new SICInfo(s)).ToList<ISICInfo>();

                sICCode = businessInfo.SICCode;

                sICDesc = businessInfo.SICDesc;

                sICExt = businessInfo.SICExt;

                businessName = businessInfo.BusinessName;

                phoneInfo =new PhoneInfo (businessInfo.PhoneInfo);

                filingOfficeAddress = new FilingOfficeAddress(businessInfo.FilingOfficeAddress);

                businessLocationType = businessInfo.BusinessLocationType;

                businessEmail = businessInfo.BusinessEmail;

                uRL = businessInfo.URL;

                businessNameShort = businessInfo.BusinessNameShort;

                businessNumber = businessInfo.BusinessNumber;

                businessDescription = businessInfo.BusinessDescription;

                summaryBusinessDescription = businessInfo.SummaryBusinessDescription;

                numberOfEmployees = businessInfo.NumberOfEmployees;

                operatingStatus = businessInfo.OperatingStatus;

                yearStarted = businessInfo.YearStarted;

                nationalID = businessInfo.NationalID;

                secondaryBusinessName = businessInfo.SecondaryBusinessName;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress primaryAddress { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress mailingAddress { get; set; }

        public string corpBusinessStatus { get; set; }

        public string dUNSNumber { get; set; }

        public string filingType { get; set; }

        public string filingDate { get; set; }

        public string filingNumber { get; set; }

        public string filingState { get; set; }

        public string origFilingDate { get; set; }

        public string origFilingNumber { get; set; }

        public string taxId { get; set; }

        public string federalEmpID { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISICInfo, SICInfo>))]
        public List<ISICInfo> primarySIC { get; set; }

        public string sICCode { get; set; }

        public string sICDesc { get; set; }

        public string sICExt { get; set; }

        public string businessName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phoneInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IFilingOfficeAddress, FilingOfficeAddress>))]
        public IFilingOfficeAddress filingOfficeAddress { get; set; }

        public string businessLocationType { get; set; }

        public string businessEmail { get; set; }

        public string uRL { get; set; }

        public string businessNameShort { get; set; }

        public string businessNumber { get; set; }

        public string businessDescription { get; set; }

        public string summaryBusinessDescription { get; set; }

        public string numberOfEmployees { get; set; }

        public string operatingStatus { get; set; }

        public string yearStarted { get; set; }

        public string nationalID { get; set; }

        public string secondaryBusinessName { get; set; }

    }
}
