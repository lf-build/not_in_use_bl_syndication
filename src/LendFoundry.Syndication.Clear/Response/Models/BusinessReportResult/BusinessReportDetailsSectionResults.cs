﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{



    public partial class BusinessReportDetailsSectionResults: IBusinessReportDetailsSectionResults
    {
        public BusinessReportDetailsSectionResults(Proxy.Response.BusinessReportResult.SectionResults businessReportDetailsSectionResults)
        {
            if (businessReportDetailsSectionResults!=null)
            {
                sectionName = businessReportDetailsSectionResults.SectionName;
                ClearReportDescription = businessReportDetailsSectionResults.ClearReportDescription;
                sectionStatus = businessReportDetailsSectionResults.SectionStatus;
                sectionRecordCount = businessReportDetailsSectionResults.SectionRecordCount;
                sectionDetails = new SectionDetails(businessReportDetailsSectionResults.SectionDetails);
            }
        }

        public string sectionName { get; set; }

        public string ClearReportDescription { get; set; }

        public string sectionStatus { get; set; }

        public int sectionRecordCount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISectionDetails, SectionDetails>))]
        public ISectionDetails sectionDetails { get; set; }


    }

}

    


   


   


  


   


   





  


   


    


   


  


   

    


    


   


   

   


  


   


    

   


  


    


