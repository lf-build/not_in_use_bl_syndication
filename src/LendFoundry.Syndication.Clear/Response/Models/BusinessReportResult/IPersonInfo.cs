﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public   interface IPersonInfo
    {
         IAddress address { get; set; }

         string nameType { get; set; }

         IPersonName personName { get; set; }

         IPhoneInfo phoneNumber { get; set; }

         IPersonProfile personProfile { get; set; }
    }
}
