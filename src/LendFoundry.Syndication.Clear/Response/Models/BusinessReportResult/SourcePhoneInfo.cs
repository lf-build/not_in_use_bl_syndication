﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public  class SourcePhoneInfo : ISourcePhoneInfo
    {
        public SourcePhoneInfo(Proxy.Response.BusinessReportResult.SourcePhoneInfo sourcePhoneInfo)
        {
            if (sourcePhoneInfo!=null)
            {
                name = sourcePhoneInfo.Name;
                address = new Address(sourcePhoneInfo.Address);
                phoneNumber = sourcePhoneInfo.PhoneNumber;
                businessContact = sourcePhoneInfo.BusinessContact;
                officerName = sourcePhoneInfo.OfficerName;
                contactForLocation = sourcePhoneInfo.ContactForLocation;
                title = sourcePhoneInfo.Title;
                source = sourcePhoneInfo.Source;
            }
          
        }
        public string name { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        public string phoneNumber { get; set; }

        public string businessContact { get; set; }

        public string officerName { get; set; }

        public string contactForLocation { get; set; }

        public string title { get; set; }

        public string source { get; set; }

    }
}
