﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IBusinessReportDetailsSectionResults
    {
        string ClearReportDescription { get; set; }
        ISectionDetails sectionDetails { get; set; }
        string sectionName { get; set; }
        int sectionRecordCount { get; set; }
        string sectionStatus { get; set; }
    }
}