﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IAdditionalSIC
    {
        List<ISICInfo> sICInfo2 { get; set; }
        List<ISICInfo> sICInfo3 { get; set; }
        List<ISICInfo> sICInfo4 { get; set; }
        List<ISICInfo> sICInfo5 { get; set; }
        List<ISICInfo> sICInfo6 { get; set; }
    }
}