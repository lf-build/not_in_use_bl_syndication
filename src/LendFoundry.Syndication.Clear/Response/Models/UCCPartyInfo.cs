﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class UCCPartyInfo 
    {
        public UCCPartyInfo()
        {

        }
        public string[] partyName { get; set; }

        public IPersonName[] personName { get; set; }

        public string[] businessName { get; set; }

        public string businessDUNSNumber { get; set; }

        public string headqtrDUNSNumber { get; set; }

        public string typeOfParty { get; set; }

        public string taxID { get; set; }

        public IAddress address { get; set; }
    }
}
