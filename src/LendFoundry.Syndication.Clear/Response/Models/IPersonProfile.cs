﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
  public  interface IPersonProfile
    {
         string personBirthDate { get; set; }

         string yearOfBirth { get; set; }

         string monthOfBirth { get; set; }

         string dayOfBirth { get; set; }

         string personAgeTo { get; set; }

         string personAgeFrom { get; set; }

         string personDeathIndicator { get; set; }

         string personDeathDate { get; set; }

         string personDeathPlace { get; set; }

         string personBirthPlace { get; set; }

         string personBirthState { get; set; }

         string numberofDependents { get; set; }

         string employeeOccupation { get; set; }

         string professionalTitle { get; set; }

         string personAge { get; set; }

         string personBuild { get; set; }

         string personCitizenship { get; set; }

         string personEducationLevel { get; set; }

         string personEthnicity { get; set; }

         string personEyeColor { get; set; }

         string personHairColor { get; set; }

         string personHeight { get; set; }

         string personRace { get; set; }

         string personSex { get; set; }

         string personSkinTone { get; set; }

         string personMaritalStatus { get; set; }

         string personSpouseName { get; set; }

         string personWeight { get; set; }

         string personMarkings { get; set; }

         string personMarkingsLocation { get; set; }

         string personMarkingsType { get; set; }

         string personContactEmail { get; set; }
    }
}
