﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class UCCRecord: IUCCRecord
    {
        public UCCRecord()
        {
                
        }
        public UCCRecord(Proxy.Response.UCCRecord uccRecord)
        {
            if (uccRecord!=null)
            {

            }
        }
        public UCCFilingInfo uCCFilingInfo { get; set; }

        public UCCFilingHistory[] uCCFilingHistory { get; set; }

        public string source { get; set; }

    }
}
