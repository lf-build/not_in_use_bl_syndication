﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class ProfLicenseInfo : IProfLicenseInfo
    {
        public ProfLicenseInfo()
        {

        }
        public string boardCertification {get; set;}

        public string certificationBoard {get; set;}

        public string licenseAgency {get; set;}

        public string licenseDescription {get; set;}

        public string[] licenseNumber { get; set; }

        public string[] licenseSpecialty {get; set;}

        public string licenseState {get; set;}

        public string[] licenseStatus {get; set;}

        public string[] typeofLicense {get; set;}

    }
}
