﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;


namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class CourtDominantValues:ICourtDominantValues
    {
        public CourtDominantValues()
        {

        }
        public CourtDominantValues(Proxy.Response.CourtResultsPageResultGroupDominantValuesCourtDominantValues courtDominantValues)
        {
            if (courtDominantValues!=null)
            {
                Name = courtDominantValues.Name;
                if (courtDominantValues.Address!=null)
                {
                    Address = new Address(courtDominantValues.Address);
                }
                CaseState = courtDominantValues.CaseState;
                CaseDate = courtDominantValues.CaseDate;
            }
        }
        public string Name { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }
        public string CaseState { get; set; }
        public string CaseDate { get; set; }
    }
}
