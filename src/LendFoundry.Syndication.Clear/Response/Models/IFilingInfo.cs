﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
   public  interface IFilingInfo
    {
       
        //     IFilingNumberInfo[] filingNumberInfo { get; set; }

           //  IFilingOffice[] filingOffice { get; set; }

             string[] typeofFiling { get; set; }

             string[] unlawfulDetainer { get; set; }

             string[] fileDate { get; set; }

             IAddress[] filingOfficeAddress { get; set; }

             string[] releaseDate { get; set; }

             string[] certificateNumber { get; set; }

             string[] hiddenFilingNumber { get; set; }

             string[] iRSSerialNumber { get; set; }

             string[] orginalFilingNumber { get; set; }

             string[] originalBook { get; set; }

             string[] originalPage { get; set; }

             string court { get; set; }

             string controlNumber { get; set; }

             string courtCounty { get; set; }

             string collectionDate { get; set; }

             string complaintDate { get; set; }

             string federalLienNumber { get; set; }

             string filingOfficeDUNSNumber { get; set; }

             string maturityDate { get; set; }

             string multipleDebtors { get; set; }

             string originalFilingDate { get; set; }

             string paragraph { get; set; }

             string perfectedDate { get; set; }

             string vendorNumber { get; set; }

             //ICaseDisposition dispositionInfo { get; set; }

           //  IFilingTypeInfo[] filingTypeInfo { get; set; }

             string caseDetails { get; set; }

             string convertDate { get; set; }

             string demandAmount { get; set; }

             string dischargeDate { get; set; }

             string dismissalDate { get; set; }

             string filingChapter { get; set; }

             string filingDistrict { get; set; }

             string filingState { get; set; }

             string filingStatusFlag { get; set; }

             string filingStatusTime { get; set; }

             string finalDecreeDate { get; set; }

             string keyNatureOfSuit { get; set; }

             string keyNatureOfSuitCode { get; set; }

             string otherDockets { get; set; }

             string otherDocketsTitle { get; set; }

             string reopenedDate { get; set; }

             string reterminatedDate { get; set; }

             string statusSetBy { get; set; }

             string terminatedDate { get; set; }

             string caseDispositionFinalDate { get; set; }

             string documentDescription { get; set; }

             string documentFormat { get; set; }

             string documentFiledDate { get; set; }

             string documentID { get; set; }

             string documentStatus { get; set; }

             string statusDate { get; set; }
       
    }
}