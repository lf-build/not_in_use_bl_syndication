﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class PersonName : IPersonName
    {
        public PersonName()
        {

        }
        public string prefix { get; set; }

        public string firstName { get; set; }

        public string middleName { get; set; }

        public string maidenName { get; set; }

        public string lastName { get; set; }

        public string suffix { get; set; }

        public string secondaryLastName { get; set; }

        public string[] fullName { get; set; }
    }
}
