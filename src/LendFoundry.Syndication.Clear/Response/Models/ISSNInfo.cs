﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public  interface ISSNInfo
    {
         string[] sSN { get; set; }

         string[] partialSSN { get; set; }

         string sSNIssueDate { get; set; }

         string sSNExpirationDate { get; set; }

         string sSNIssuanceText { get; set; }

         string sSNIssueState { get; set; }
    }
}
