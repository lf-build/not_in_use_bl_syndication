﻿
namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class Status : IStatus
    {
        public Status()
        {

        }
        public Status(Proxy.Response.CourtResultsPageStatus status)
        {
            if (status!=null)
            {
                StatusCode = status.StatusCode;
                SubStatusCode = status.SubStatusCode;
            }
        }
        public string StatusCode { get; set; }
        public string SubStatusCode { get; set; }
    }
}
