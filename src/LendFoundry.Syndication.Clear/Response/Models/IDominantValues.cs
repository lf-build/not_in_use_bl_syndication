﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IDominantValues
    {
        ICourtDominantValues CourtDominantValues { get; set; }
    }
}
