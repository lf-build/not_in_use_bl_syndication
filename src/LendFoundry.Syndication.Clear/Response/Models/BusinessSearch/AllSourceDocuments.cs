﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class AllSourceDocuments: IAllSourceDocuments
    {
        public AllSourceDocuments(Proxy.Response.BusinessSearch.AllSourceDocuments allSourceDocuments)
        {
            if (allSourceDocuments!=null)
            {
                SourceName = allSourceDocuments.SourceName;
                SourceDocumentGuid = allSourceDocuments.SourceDocumentGuid;
            }
        }
        public string SourceName { get; set; }
        public string SourceDocumentGuid { get; set; }
    }
}
