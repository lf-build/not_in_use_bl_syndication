﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface ICorporationInfo
    {
        string CorporationNumber { get; set; }
        string DateIncorporated { get; set; }
        string StateOfIncorporation { get; set; }
    }
}