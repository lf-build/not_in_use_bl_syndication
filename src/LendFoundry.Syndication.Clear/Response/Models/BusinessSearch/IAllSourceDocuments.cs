﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IAllSourceDocuments
    {
        string SourceDocumentGuid { get; set; }
        string SourceName { get; set; }
    }
}