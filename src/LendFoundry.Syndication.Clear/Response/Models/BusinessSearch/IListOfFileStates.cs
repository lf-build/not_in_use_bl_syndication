﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IListOfFileStates
    {
        string FileState { get; set; }
    }
}