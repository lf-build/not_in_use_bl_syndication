﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IListOfFEINs
    {
        List<string> FEIN { get; set; }
    }
}