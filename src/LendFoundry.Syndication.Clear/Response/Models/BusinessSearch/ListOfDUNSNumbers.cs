﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class ListOfDUNSNumbers: IListOfDUNSNumbers
    {
        public ListOfDUNSNumbers(Proxy.Response.BusinessSearch.ListOfDUNSNumbers listOfDUNSNumbers)
        {
            if (listOfDUNSNumbers!=null)
            {
                DUNSNumber = listOfDUNSNumbers.DUNSNumber;
            }
        }
        public List<string> DUNSNumber { get; set; }
    }
}
