﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IRecordDetails
    {
        IBusinessResponseDetail BusinessResponseDetail { get; set; }
    }
}