﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class Address : IAddress
    {
        public Address(Proxy.Response.BusinessSearch.Address address)
        {
            if (address != null)
            {
                Street = address.Street;
                City = address.City;
                State = address.State;
                ZipCode = address.ZipCode;
                Country = address.Country;
                Latitude = address.Latitude;
                Longitude = address.Longitude;
            }
        }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
