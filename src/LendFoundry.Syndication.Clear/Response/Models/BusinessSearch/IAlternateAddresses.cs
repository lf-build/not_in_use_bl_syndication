﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IAlternateAddresses
    {
        string City { get; set; }
        string Country { get; set; }
        string County { get; set; }
        string Latitude { get; set; }
        string Longitude { get; set; }
        string State { get; set; }
        string Street { get; set; }
        string ZipCode { get; set; }
    }
}