﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class BusinessExecutives: IBusinessExecutives
    {
        public BusinessExecutives(Proxy.Response.BusinessSearch.BusinessExecutives businessExecutives)
        {
            if (businessExecutives!=null)
            {
                Name = businessExecutives.Name;
                Title = businessExecutives.Title;
            }
        }
        public string Name { get; set; }
        public string Title { get; set; }
    }
}
