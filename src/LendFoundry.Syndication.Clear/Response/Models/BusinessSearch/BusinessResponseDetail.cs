﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class BusinessResponseDetail: IBusinessResponseDetail
    {
        public BusinessResponseDetail(Proxy.Response.BusinessSearch.BusinessResponseDetail businessResponseDetail)
        {
            if (businessResponseDetail!=null)
            {
                BusinessName = businessResponseDetail.BusinessName;
                Address = new Address(businessResponseDetail.Address);
             //   AlternateBusinessNames = new AlternateBusinessNames(businessResponseDetail.AlternateBusinessNames);
              //  AlternateAddresses = new AlternateAddresses(businessResponseDetail.AlternateAddresses);
                Phones = new Phones(businessResponseDetail.Phones);
                BusinessExecutives = businessResponseDetail.BusinessExecutives.Select(b => new BusinessExecutives(b)).ToList<IBusinessExecutives>();
              //  ListOfFEINs = new ListOfFEINs(businessResponseDetail.ListOfFEINs);
                ListOfDUNSNumbers = new ListOfDUNSNumbers(businessResponseDetail.ListOfDUNSNumbers);
                ListOfFileStates = new ListOfFileStates(businessResponseDetail.ListOfFileStates);
                CorporationInfo = new CorporationInfo(businessResponseDetail.CorporationInfo);
                LegalUltimateParent = businessResponseDetail.LegalUltimateParent;
                LegalImmediateParent = businessResponseDetail.LegalImmediateParent;
                CompanyEntityId = businessResponseDetail.CompanyEntityId;
                AllSourceDocuments = businessResponseDetail.AllSourceDocuments.Select(a=>new AllSourceDocuments(a)).ToList<IAllSourceDocuments>();
            }
        }
        public string BusinessName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<IAlternateBusinessNames, AlternateBusinessNames>))]
        //public IAlternateBusinessNames AlternateBusinessNames { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<IAlternateAddresses, AlternateAddresses>))]
        //public IAlternateAddresses AlternateAddresses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPhones, Phones>))]
        public IPhones Phones { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBusinessExecutives, BusinessExecutives>))]
        public List<IBusinessExecutives> BusinessExecutives { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<IListOfFEINs, ListOfFEINs>))]
        //public IListOfFEINs ListOfFEINs { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IListOfDUNSNumbers, ListOfDUNSNumbers>))]
        public IListOfDUNSNumbers ListOfDUNSNumbers { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IListOfFileStates, ListOfFileStates>))]
        public IListOfFileStates ListOfFileStates { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICorporationInfo, CorporationInfo>))]
        public ICorporationInfo CorporationInfo { get; set; }
        public string LegalUltimateParent { get; set; }
        public string LegalImmediateParent { get; set; }
        public string CompanyEntityId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAllSourceDocuments, AllSourceDocuments>))]
        public List<IAllSourceDocuments> AllSourceDocuments { get; set; }
    }
}
