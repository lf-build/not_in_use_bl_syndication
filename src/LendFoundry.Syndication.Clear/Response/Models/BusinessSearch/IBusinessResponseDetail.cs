﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IBusinessResponseDetail
    {
        IAddress Address { get; set; }
        List<IAllSourceDocuments> AllSourceDocuments { get; set; }
      //  IAlternateAddresses AlternateAddresses { get; set; }
     //   IAlternateBusinessNames AlternateBusinessNames { get; set; }
        List<IBusinessExecutives> BusinessExecutives { get; set; }
        string BusinessName { get; set; }
        string CompanyEntityId { get; set; }
        ICorporationInfo CorporationInfo { get; set; }
        string LegalImmediateParent { get; set; }
        string LegalUltimateParent { get; set; }
        IListOfDUNSNumbers ListOfDUNSNumbers { get; set; }
     //   IListOfFEINs ListOfFEINs { get; set; }
        IListOfFileStates ListOfFileStates { get; set; }
        IPhones Phones { get; set; }
    }
}