﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class DominantValues: IDominantValues
    {
        public DominantValues(Proxy.Response.BusinessSearch.DominantValues dominantValues)
        {
            if (dominantValues!=null)
            {
                BusinessDominantValues = new BusinessDominantValues(dominantValues.BusinessDominantValues);

            }
        }
        [JsonConverter(typeof(InterfaceConverter<IBusinessDominantValues, BusinessDominantValues>))]
        public IBusinessDominantValues BusinessDominantValues { get; set; }
    }
}
