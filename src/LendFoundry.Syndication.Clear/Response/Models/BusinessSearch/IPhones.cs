﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IPhones
    {
        string PhoneNumber { get; set; }
    }
}