﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IAlternateBusinessNames
    {
        string BusinessName { get; set; }
    }
}