﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class ListOfFileStates: IListOfFileStates
    {
        public ListOfFileStates(Proxy.Response.BusinessSearch.ListOfFileStates listOfFileStates)
        {
            if (listOfFileStates!=null)
            {
                FileState = listOfFileStates.FileState;
            }
        }
        public string FileState { get; set; }
    }
}
