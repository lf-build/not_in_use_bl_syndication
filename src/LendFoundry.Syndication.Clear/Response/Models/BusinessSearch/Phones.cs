﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class Phones: IPhones
    {
        public Phones(Proxy.Response.BusinessSearch.Phones phones)
        {
            if (phones!=null)
            {
                PhoneNumber = phones.PhoneNumber;
            }
        }
        public string PhoneNumber { get; set; }
    }
}
