﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class RecordDetails: IRecordDetails
    {
        public RecordDetails(Proxy.Response.BusinessSearch.RecordDetails recordDetails)
        {
            if (recordDetails!=null)
            {
                BusinessResponseDetail = new BusinessResponseDetail(recordDetails.BusinessResponseDetail);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IBusinessResponseDetail, BusinessResponseDetail>))]
        public IBusinessResponseDetail BusinessResponseDetail { get; set; }
    }
}
