﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class Status : IStatus
    {
        public Status(Proxy.Response.BusinessSearch.Status status)
        {
            if (status != null)
            {
                StatusCode = status.StatusCode;
                SubStatusCode = status.SubStatusCode;
            }
        }
        public string StatusCode { get; set; }
        public string SubStatusCode { get; set; }
    }
}
