﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class BusinessDominantValues: IBusinessDominantValues
    {
        public BusinessDominantValues(Proxy.Response.BusinessSearch.BusinessDominantValues businessDominantValues)
        {
            if (businessDominantValues!=null)
            {
                Name = businessDominantValues.Name;
              //  FEIN = businessDominantValues.FEIN;
                Address = new Address(businessDominantValues.Address);
                FileState = businessDominantValues.FileState;
            }
        }
        public string Name { get; set; }
        public string FEIN { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }
        public string FileState { get; set; }
    }
}
