﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public class CorporationInfo: ICorporationInfo
    {
        public CorporationInfo(Proxy.Response.BusinessSearch.CorporationInfo corporationInfo)
        {
            if (corporationInfo!=null)
            {
                CorporationNumber = corporationInfo.CorporationNumber;
                StateOfIncorporation = corporationInfo.StateOfIncorporation;
                DateIncorporated = corporationInfo.DateIncorporated;
            }
        }
        public string CorporationNumber { get; set; }
        public string StateOfIncorporation { get; set; }
        public string DateIncorporated { get; set; }
    }
}
