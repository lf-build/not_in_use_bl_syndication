﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IBusinessExecutives
    {
        string Name { get; set; }
        string Title { get; set; }
    }
}