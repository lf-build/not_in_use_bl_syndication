﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{

    public class BusinessSearchResponse: IBusinessSearchResponse
    {
        public BusinessSearchResponse(Proxy.Response.BusinessSearch.BusinessSearchResponse businessSearchResponse)
        {
            if (businessSearchResponse!=null)
            {
                Status = new Status(businessSearchResponse.Status);
                StartIndex = businessSearchResponse.StartIndex;
                EndIndex = businessSearchResponse.EndIndex;
                ResultGroup = businessSearchResponse.ResultGroup.Select(a=> new ResultGroup(a)).ToList<IResultGroup>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IStatus, Status>))]
        public IStatus Status { get; set; }
        public string StartIndex { get; set; }
        public string EndIndex { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IResultGroup, ResultGroup>))]
        public List<IResultGroup> ResultGroup { get; set; }
      
    }


}
