﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IBusinessDominantValues
    {
        IAddress Address { get; set; }
        string FEIN { get; set; }
        string FileState { get; set; }
        string Name { get; set; }
    }
}