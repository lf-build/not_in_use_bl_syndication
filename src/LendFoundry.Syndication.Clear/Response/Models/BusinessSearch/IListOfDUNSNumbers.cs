﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IListOfDUNSNumbers
    {
        List<string> DUNSNumber { get; set; }
    }
}