﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessSearch
{
    public interface IDominantValues
    {
        IBusinessDominantValues BusinessDominantValues { get; set; }
    }
}