﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public  interface IDriverLicenseInfo
    {
        string[] driverLicenseNumber { get; set; }

        string[] driverLicenseState { get; set; }

        string[] driverLicenseCounty { get; set; }
    }
}
