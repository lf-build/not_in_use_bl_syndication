﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IDunBradstreetRecord
    {
        string accountingFirm { get; set; }
        IAdditionalSIC additionalSIC { get; set; }
        string bankDUNSNumber { get; set; }
        string bankName { get; set; }
        IBusinessInfo businessInfo { get; set; }
        string employeeTotal { get; set; }
        string employmentGrowth { get; set; }
        string empTotal1YrAgo { get; set; }
        string empTotal3YrAgo { get; set; }
        string establishedManufacturingIndicator { get; set; }
        string foreignOwned { get; set; }
        string headquartersDUNS { get; set; }
        string headquartersName { get; set; }
        string homeOrCottageBusiness { get; set; }
        string incorporationDate { get; set; }
        string industryGroup { get; set; }
        string latestSales { get; set; }
        string lineOfBusiness { get; set; }
        string manufacturingLocation { get; set; }
        string minorityOwned { get; set; }
        string mSACode { get; set; }
        string mSAName { get; set; }
        string netWorth { get; set; }
        string numberOfAccounts { get; set; }
        string numberOfEmployeesHere { get; set; }
        string occupancyType { get; set; }
        string otherDUNS { get; set; }
        string parentCompanyDUNS { get; set; }
        string parentCompanyName { get; set; }
        List<IPersonInfo> personInfo { get; set; }
        string publicallyOwned { get; set; }
        string recordUpdateDate { get; set; }
        string revisionDate { get; set; }
        string sales1YrAgo { get; set; }
        string sales3YrAgo { get; set; }
        string salesGrowth { get; set; }
        string salesTerritory { get; set; }
        string secondaryCompanyName { get; set; }
        string smallBusiness { get; set; }
        string source { get; set; }
        string squareFootageOfBuilding { get; set; }
        string stateOfIncorporation { get; set; }
        string ultimateCompanyDUNS { get; set; }
        string ultimateCompanyName { get; set; }
        string yearStarted { get; set; }
    }
}