﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IStatus
    {
         string StatusCode { get; set; }
         string SubStatusCode { get; set; }
    }
}
