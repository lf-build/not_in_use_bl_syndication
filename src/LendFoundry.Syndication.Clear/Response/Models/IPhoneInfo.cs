﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IPhoneInfo
    {
         string phoneNumber { get; set; }

         string phoneNumberSuffix { get; set; }

         string workPhoneNumber { get; set; }

         string tollFreePhoneNumber { get; set; }

         string otherPhoneNumber { get; set; }

         string faxNumber { get; set; }

         string phoneNumberType { get; set; }

         string otherPhoneNumberType { get; set; }
    }
}
