﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class CourtResponseDetail: ICourtResponseDetail
    {
        public CourtResponseDetail()
        {

        }
        public CourtResponseDetail(Proxy.Response.CourtResultsPageResultGroupRecordDetailsCourtResponseDetail courtResponseDetail)
        {
            if (courtResponseDetail!=null)
            {
                //if (courtResponseDetail.LienJudgeMultipleRecord != null)
                //{
                //    LienJudgeMultipleRecord = courtResponseDetail.LienJudgeMultipleRecord.Select(c => new LienJudgeMultipleRecord(c)).ToList<ILienJudgeMultipleRecord>();
                //}
                    if (courtResponseDetail.DocumentGuids!=null)
                {
                    DocumentGuids = new DocumentGuids(courtResponseDetail.DocumentGuids);
                }
                if (courtResponseDetail.UCCRecord != null)
                {
                    UCCRecord = courtResponseDetail.UCCRecord.Select(c => new UCCRecord(c)).ToList<IUCCRecord>();
                }
            }
        }
        //[JsonConverter(typeof(InterfaceListConverter<ILienJudgeMultipleRecord, LienJudgeMultipleRecord>))]
        //public List<ILienJudgeMultipleRecord> LienJudgeMultipleRecord { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDocumentGuids, DocumentGuids>))]
        public IDocumentGuids DocumentGuids { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IUCCRecord, UCCRecord>))]
        public List<IUCCRecord> UCCRecord { get; set; }
    }
}
