﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class RealEstateInfo 
    {
        public string descriptionField{ get; set; } 

        public string designationField{ get; set; } 

        public string[] businessNameField{ get; set; } 

        public string[] taxIDField{ get; set; } 

        public IPersonName[] personNameField{ get; set; } 

        public IAddress realEstateAddressField{ get; set; } 
    }
}
