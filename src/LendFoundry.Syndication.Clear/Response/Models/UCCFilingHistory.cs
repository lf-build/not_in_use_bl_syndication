﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class UCCFilingHistory
    {
        public IUCCFilingInfo uCCFilingInfoField { get; set; }
    }
}
