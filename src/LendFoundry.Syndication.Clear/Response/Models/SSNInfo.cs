﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class SSNInfo : ISSNInfo
    {
        public SSNInfo()
        {

        }
        public string[] sSN { get; set; }

        public string[] partialSSN { get; set; }

        public string sSNIssueDate { get; set; }

        public string sSNExpirationDate { get; set; }

        public string sSNIssuanceText { get; set; }

        public string sSNIssueState { get; set; }
    }
}
