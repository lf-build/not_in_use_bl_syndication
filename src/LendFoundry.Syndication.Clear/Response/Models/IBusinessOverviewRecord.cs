﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IBusinessOverviewRecord
    {
        List<IAddress> address { get; set; }
        List<string> alternateCompanyName { get; set; }
        string businessDescription { get; set; }
        string companyContact { get; set; }
        string companyName { get; set; }
        string contactTitle { get; set; }
        string corporationStatus { get; set; }
        string currentOutstandingShares { get; set; }
        List<string> dateofIncorporation { get; set; }
        List<string> dUNS { get; set; }
        List<string> fEIN { get; set; }
        string fiscalYearEnd { get; set; }
        string lastAnnualFinancialDate { get; set; }
        string legalImmediateParent { get; set; }
        string legalImmediateParentIndicator { get; set; }
        string legalUltimateParent { get; set; }
        string legalUltimateParentIndicator { get; set; }
        string numberofEmployees { get; set; }
        string primarySICCode { get; set; }
        List<string> registeredAgent { get; set; }
        string shareholders { get; set; }
        List<string> stateofIncorporation { get; set; }
        string tickerSymbol { get; set; }
        string webAddress { get; set; }
        string yearEstablished { get; set; }
    }
}