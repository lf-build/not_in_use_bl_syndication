﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IUCCFilingInfo
    {
         FilingStmtInfo[] filingStmtInfo { get; set; }

         UCCPartyInfo[] debtor { get; set; }

         UCCPartyInfo[] securedParty { get; set; }

         UCCPartyInfo[] assignee { get; set; }

         UCCPartyInfo[] assignor { get; set; }

         RealEstateInfo realEstateInfo { get; set; }

         //ICollateralInfo[] collateralInfo { get; set; }

        FilingOfficeStmt filingOfficeStmt { get; set; }

         RelatedFilingInfo[] relatedFilingInfo { get; set; }
    }
}