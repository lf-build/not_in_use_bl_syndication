﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class PhoneInfo :IPhoneInfo
    {
        public PhoneInfo(Proxy.Response.PhoneInfo phoneInfo)
        {
            if (phoneInfo != null)
            {
                phoneNumber = phoneInfo.PhoneNumber;

                phoneNumberSuffix = phoneInfo.PhoneNumberSuffix;

                workPhoneNumber = phoneInfo.WorkPhoneNumber;

                tollFreePhoneNumber = phoneInfo.TollFreePhoneNumber;
                otherPhoneNumber = phoneInfo.OtherPhoneNumber;

                faxNumber = phoneInfo.FaxNumber;

                phoneNumberType = phoneInfo.PhoneNumberType;
                otherPhoneNumberType = phoneInfo.OtherPhoneNumber;

                      }                  
                                
        }
        public string phoneNumber{get;set;}

        public string phoneNumberSuffix{get;set;}

        public string workPhoneNumber{get;set;}

        public string tollFreePhoneNumber{get;set;}

        public string otherPhoneNumber{get;set;}

        public string faxNumber{get;set;}

        public string phoneNumberType{get;set;}

        public string otherPhoneNumberType{get;set;}
    }
}
