﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IDocumentGuids
    {
         string SourceName { get; set; }
         string SourceDocumentGuids { get; set; }
    }
}
