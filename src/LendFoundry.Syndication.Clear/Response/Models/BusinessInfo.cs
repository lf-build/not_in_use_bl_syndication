﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class BusinessInfo : IBusinessInfo
    {
        public BusinessInfo()
        {
                
        }
        public IAddress address { get; set; }

        public IAddress primaryAddress { get; set; }

        public IAddress mailingAddress { get; set; }

        public string corpBusinessStatus { get; set; }

        public string dUNSNumber { get; set; }

        public string filingType { get; set; }

        public string filingDate { get; set; }

        public string filingNumber { get; set; }

        public string filingState { get; set; }

        public string origFilingDate { get; set; }

        public string origFilingNumber { get; set; }

        public string taxId { get; set; }

        public string federalEmpID { get; set; }

        public ISICInfo[] primarySIC { get; set; }

        public string sICCode { get; set; }

        public string sICDesc { get; set; }

        public string sICExt { get; set; }

        public string businessName { get; set; }

        public IPhoneInfo phoneInfo { get; set; }

        public IFilingOfficeAddress filingOfficeAddress { get; set; }

        public string businessLocationType { get; set; }

        public string businessEmail { get; set; }

        public string uRL { get; set; }

        public string businessNameShort { get; set; }

        public string businessNumber { get; set; }

        public string businessDescription { get; set; }

        public string summaryBusinessDescription { get; set; }

        public string numberOfEmployees { get; set; }

        public string operatingStatus { get; set; }

        public string yearStarted { get; set; }

        public string nationalID { get; set; }

        public string secondaryBusinessName { get; set; }
    }
}
