﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class ResultGroup:IResultGroup
    {
        public ResultGroup()
        {

        }
        public ResultGroup(Proxy.Response.CourtResultsPageResultGroup resultGroup)
        {
            if (resultGroup!=null)
            {
                GroupId = resultGroup.GroupId;
                RecordCount = resultGroup.RecordCount;
                Relevance = resultGroup.Relevance;
                DominantValues =  new DominantValues(resultGroup.DominantValues);
                RecordDetails = new RecordDetails(resultGroup.RecordDetails);
            }
        }
        public string GroupId { get; set; }
        public string RecordCount { get; set; }
        public string Relevance { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDominantValues,DominantValues>))]
        public IDominantValues DominantValues { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRecordDetails, RecordDetails>))]
        public IRecordDetails RecordDetails { get; set; }
    }
}
