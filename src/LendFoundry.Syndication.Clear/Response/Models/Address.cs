﻿

namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class Address: IAddress
    {
        public Address()
        {

        }
        public Address(Proxy.Response.CourtResultsPageResultGroupDominantValuesCourtDominantValuesAddress address)
        {
            if (address!=null)
            {
                Street = address.Street;
                City = address.City;
                State = address.State;
                ZipCode = address.ZipCode;
            }
        }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }
}
