namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IChartDetailsWithEntityId
    {
         string entityID { get; set; }

         IPersonName personName { get; set; }

         IRiskFlagsWithDocguids riskFlagsWithDocguids { get; set; }
    }
}
