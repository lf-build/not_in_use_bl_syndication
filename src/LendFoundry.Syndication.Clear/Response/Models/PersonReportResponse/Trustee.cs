using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Trustee : ITrustee
    {
        public Trustee(Proxy.Response.PersonReport.Trustee trustee)
        {
            if(trustee!=null)
            {
                partyInfo = new PartyInfo(trustee.PartyInfo);
                businessName = trustee.BusinessName;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPartyInfo, PartyInfo>))]
        public IPartyInfo partyInfo { get; set; }
        public string businessName { get; set; }
    }
}
