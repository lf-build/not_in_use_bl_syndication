using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAssociateAnalyticsChartRecord
    {
         List<IChartDetails> associates { get; set; }

         List<IChartDetails> relatives { get; set; }

         List<IChartDetailsWithEntityId> associatesWithEntityId { get; set; }

         List<IChartDetailsWithEntityId> relativeWithEntityId { get; set; }
    }
}
