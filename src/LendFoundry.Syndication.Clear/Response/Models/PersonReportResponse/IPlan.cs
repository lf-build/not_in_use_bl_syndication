namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPlan
    {
         string authorizedBy { get; set; }

         string authorizedTitle { get; set; }

         string planFilingDate { get; set; }

         string planStatus { get; set; }

         string planStatusDate { get; set; }

         string typeOfAuthorizer { get; set; }
    }
}
