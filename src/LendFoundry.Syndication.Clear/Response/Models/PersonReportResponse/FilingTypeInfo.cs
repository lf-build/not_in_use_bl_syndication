﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FilingTypeInfo : IFilingTypeInfo
    {
        public FilingTypeInfo(Proxy.Response.PersonReport.FilingTypeInfo filingTypeInfo)
        {
            if (filingTypeInfo != null)
            {
                actionDescription   = filingTypeInfo.ActionDescription;
                fileTypeDescription = filingTypeInfo.FileTypeDescription;
                taxDescription      = filingTypeInfo.TaxDescription;
                typeofTax           = filingTypeInfo.TypeofTax;
                fileType            = filingTypeInfo.FileType;
                typeOfAction        = filingTypeInfo.TypeOfAction;
            }
        }

        public string actionDescription{ get; set; }

        public string fileTypeDescription{ get; set; }

        public string taxDescription{ get; set; }

        public string typeofTax{ get; set; }

        public string fileType{ get; set; }

        public List<string> typeOfAction{ get; set; }
    }
}
