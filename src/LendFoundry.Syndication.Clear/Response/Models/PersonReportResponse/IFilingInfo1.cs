using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFilingInfo1
    {
         List<IFilingNumberInfo> filingNumberInfo{ get; set; }

         List<IFilingOffice> filingOffice{ get; set; }

         List<string> typeofFiling{ get; set; }

         List<string> unlawfulDetainer{ get; set; }

         List<string> fileDate{ get; set; }

         List<IAddress> filingOfficeAddress{ get; set; }

         List<string> releaseDate{ get; set; }

         List<string> certificateNumber{ get; set; }

         List<string> hiddenFilingNumber{ get; set; }

         List<string> iRSSerialNumber{ get; set; }

         List<string> orginalFilingNumber{ get; set; }

         List<string> originalBook{ get; set; }

         List<string> originalPage{ get; set; }

         string court{ get; set; }

         string controlNumber{ get; set; }

         string courtCounty{ get; set; }

         string collectionDate{ get; set; }

         string complaintDate{ get; set; }

         string federalLienNumber{ get; set; }

         string filingOfficeDUNSNumber{ get; set; }

         string maturityDate{ get; set; }

         string multipleDebtors{ get; set; }

         string originalFilingDate{ get; set; }

         string paragraph{ get; set; }

         string perfectedDate{ get; set; }

         string vendorNumber{ get; set; }

         ICaseDisposition dispositionInfo{ get; set; }

         List<IFilingTypeInfo> filingTypeInfo{ get; set; }

         string caseDetails{ get; set; }

         string convertDate{ get; set; }

         string demandAmount{ get; set; }

         string dischargeDate{ get; set; }

         string dismissalDate{ get; set; }

         string filingChapter{ get; set; }

         string filingDistrict{ get; set; }

         string filingState{ get; set; }

         string filingStatusFlag{ get; set; }

         string filingStatusTime{ get; set; }

         string finalDecreeDate{ get; set; }

         string keyNatureOfSuit{ get; set; }

         string keyNatureOfSuitCode{ get; set; }

         string otherDockets{ get; set; }

         string otherDocketsTitle{ get; set; }

         string reopenedDate{ get; set; }

         string reterminatedDate{ get; set; }

         string statusSetBy{ get; set; }

         string terminatedDate{ get; set; }

         string caseDispositionFinalDate{ get; set; }

         string documentDescription{ get; set; }

         string documentFormat{ get; set; }

         string documentFiledDate{ get; set; }

         string documentID{ get; set; }

         string documentStatus{ get; set; }

         string statusDate{ get; set; }
    }
}
