﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IBankruptcySection
    {
         List<IFedDocketBankruptcyRecord> fedDocketBankruptcyRecord { get; set; }

         List<IPublicBankruptcyRecord> publicBankruptcyRecord { get; set; }

         List<ISuperiorBankruptcyRecord> superiorBankruptcyRecord { get; set; }

    }
}
