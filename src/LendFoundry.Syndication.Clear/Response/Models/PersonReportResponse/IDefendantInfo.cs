﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDefendantInfo
    {
        IAddress address { get; set; }
        IAdmissionInfo admissionInfo { get; set; }
        List<IAddress> historicalAddress { get; set; }
        IIDInfo iDInfo { get; set; }
        List<string> miscellaneousInfo { get; set; }
        IPersonInfo3 personInfo { get; set; }
        IPhoneInfo phone { get; set; }
        List<IRelativeInfo> relativeInfo { get; set; }
    }
}
