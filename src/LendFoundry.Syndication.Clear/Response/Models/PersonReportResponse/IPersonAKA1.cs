using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonAKA1
    {

         string aKADateOfBirth { get; set; }

         IPersonName aKAPersonName { get; set; }

         List<string> aKANickName { get; set; }
    }
}
