namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISources
    {
        string sourceName { get; set; }

        string sourceDocumentGuid { get; set; }

        string sourceCollection { get; set; }

        string sourceGenericName { get; set; }
    }
}
