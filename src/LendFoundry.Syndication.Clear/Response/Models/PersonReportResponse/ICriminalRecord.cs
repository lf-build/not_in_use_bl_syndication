﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICriminalRecord
    {
        List<IAdditionalNotes> additionalNotes { get; set; }
        List<IAddress> address { get; set; }
        List<IArrestInfo> arrestInfo { get; set; }
        List<IBailInfo> bailInfo { get; set; }
        List<ICourtInfo> courtInfo { get; set; }
        List<ICriminalCharges> criminalCharges { get; set; }
        IIDInfo iDInfo { get; set; }
        List<IInmateDisciplineInfo> inmateDisciplineInfo { get; set; }
        List<IInmateReleaseInfo> inmateReleaseInfo { get; set; }
        List<IInstitutionInfo> institutionInfo { get; set; }
        IOfficeForeignAssetsControl officeForeignAssetsControl { get; set; }
        List<IParoleInfo> paroleInfo { get; set; }
        List<IPersonInfo3> personInfo { get; set; }
        List<IPleaInfo> pleaInfo { get; set; }
        List<IPriorChargesInfo> priorChargesInfo { get; set; }
        List<IProbationInfo> probationInfo { get; set; }
        List<ISentencingInfo> sentencingInfo { get; set; }
        string sourceState { get; set; }
        string typeOfCriminal { get; set; }
        string typeOfRecord { get; set; }
        List<IVehicleInfo> vehicleInfo { get; set; }
        string source { get; set; }
        List<IPhotoImages1> photoImages { get; set; }
    }
}
