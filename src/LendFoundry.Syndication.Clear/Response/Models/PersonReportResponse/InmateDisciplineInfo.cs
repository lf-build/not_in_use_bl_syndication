namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class InmateDisciplineInfo : IInmateDisciplineInfo
    {
        public InmateDisciplineInfo(Proxy.Response.PersonReport.InmateDisciplineInfo inmateDisciplineInfo)
        {
            if (inmateDisciplineInfo != null)
            {
                decisionOnAppeal          = inmateDisciplineInfo.DecisionOnAppeal;
                infractionDate            = inmateDisciplineInfo.InfractionDate;
                infractionDescription     = inmateDisciplineInfo.InfractionDescription;
                infractionHearing         = inmateDisciplineInfo.InfractionHearing;
                infractionPlea            = inmateDisciplineInfo.InfractionPlea;
                infractionTimeLost        = inmateDisciplineInfo.InfractionTimeLost;
                infractionVerdict         = inmateDisciplineInfo.InfractionVerdict;
                infractionViolationStatus = inmateDisciplineInfo.InfractionViolationStatus;
            }
        }

        public string decisionOnAppeal { get; set; }
        public string infractionDate { get; set; }
        public string infractionDescription { get; set; }
        public string infractionHearing { get; set; }
        public string infractionPlea { get; set; }
        public string infractionTimeLost { get; set; }
        public string infractionVerdict { get; set; }
        public string infractionViolationStatus { get; set; }
    }
}
