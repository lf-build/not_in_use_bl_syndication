using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ProbationInfo : IProbationInfo
    {
        public ProbationInfo(Proxy.Response.PersonReport.ProbationInfo probationInfo)
        {
            if(probationInfo!=null)
            {


                placementAfterViolation = probationInfo.PlacementAfterViolation;
                probationActualEndDate = probationInfo.ProbationActualEndDate;
                probationAfterPrison = probationInfo.ProbationAfterPrison;
                probationAgency = probationInfo.ProbationAgency;
                probationBeginDate = probationInfo.ProbationBeginDate;
                probationScheduledEndDate = probationInfo.ProbationScheduledEndDate;
                probationTime = probationInfo.ProbationTime.Select(a=>new DurationOfTime(a)).ToList<IDurationOfTime>();
                probationMinimumTime = new DurationOfTime(probationInfo.ProbationMinimumTime);
                probationViolation = probationInfo.ProbationViolation;
                probationViolationDate = probationInfo.ProbationViolationDate;
                probConsecutiveConcurrent = probationInfo.ProbConsecutiveConcurrent;
                returnedToCustodyDate = probationInfo.ReturnedToCustodyDate;
                typeOfProbation = probationInfo.TypeOfProbation;
            }
        }
        public string placementAfterViolation { get; set; }
        public string probationActualEndDate { get; set; }
        public string probationAfterPrison { get; set; }
        public string probationAgency { get; set; }
        public string probationBeginDate { get; set; }
        public string probationScheduledEndDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDurationOfTime, DurationOfTime>))]
        public List<IDurationOfTime> probationTime { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime probationMinimumTime { get; set; }
        public string probationViolation { get; set; }
        public string probationViolationDate { get; set; }
        public string probConsecutiveConcurrent { get; set; }
        public string returnedToCustodyDate { get; set; }
        public string typeOfProbation { get; set; }
    }
}
