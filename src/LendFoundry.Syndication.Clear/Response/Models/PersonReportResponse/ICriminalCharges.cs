using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICriminalCharges
    {
         string amendedCriminalOffense{ get; set; }

         string amendedStatuteViolated{ get; set; }

         string chargesTypeOfDocument{ get; set; }

         string classOfCrime{ get; set; }

         string convictionLocation{ get; set; }

         string countyOfCrime{ get; set; }

         string crimeDate{ get; set; }

         ICrimeIndicators crimeIndicators{ get; set; }

         string criminalCourt{ get; set; }

         string criminalOffense{ get; set; }

         string dispositionOfCrime{ get; set; }

         string highestFelonyClass{ get; set; }

         string numberOfCounts{ get; set; }

         string offenderStatus{ get; set; }

         string statuteViolated{ get; set; }

         string totalConvictedUniqueOffenses{ get; set; }

         string townOfCrime{ get; set; }

         string bailAmount{ get; set; }

         ICaseInfo caseInfo{ get; set; }

         ICourtDateInfo courtDateInfo{ get; set; }

         IInmateReleaseInfo inmateReleaseInfo{ get; set; }

         List<string> miscellaneousInfo{ get; set; }

         string warrantNumber{ get; set; }
    }
}
