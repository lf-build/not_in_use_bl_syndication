namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class DurationOfTime : IDurationOfTime
    {
        public DurationOfTime(Proxy.Response.PersonReport.DurationOfTime durationOfTime)
        {
            if(durationOfTime!=null)
            {
                lengthAndUnits = durationOfTime.LengthAndUnits;
                numberOfHours = durationOfTime.NumberOfHours;
                numberOfDays = durationOfTime.NumberOfDays;
                numberOfMonths = durationOfTime.NumberOfMonths;
                numberOfYears = durationOfTime.NumberOfYears;
            }
        }
        public string lengthAndUnits { get; set; }
        public string numberOfHours { get; set; }
        public string numberOfDays { get; set; }
        public string numberOfMonths { get; set; }
        public string numberOfYears { get; set; }
    }
}
