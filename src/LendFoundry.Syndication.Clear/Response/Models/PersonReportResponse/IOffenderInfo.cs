using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IOffenderInfo
    {
         string amendedCharges { get; set; }

         string amendedChargesDate { get; set; }

         string amendedOffense { get; set; }

         string amendedOffenseDate { get; set; }

         string appealStatus { get; set; }

         string appealDate { get; set; }

         string arraignmentDate { get; set; }

         string arrestAgency { get; set; }

         string arrestDate { get; set; }

         string arrestWarrantFlag { get; set; }

         IBailInfo bailInfo { get; set; }

         IBookingInfo bookingInfo { get; set; }

         string caseDispositionFinalDate { get; set; }

         string caseDispositionDecisionCategoryText { get; set; }

         ICaseInfo caseInfo { get; set; }

         string citationNumber { get; set; }

         string crimeDate { get; set; }

         ICrimeIndicators crimeIndicators { get; set; }

         string crimeSeverity { get; set; }

         string criminalOffense { get; set; }

         string convictDate { get; set; }

         string courtCounty { get; set; }

         string courtName { get; set; }

         string custodyDate { get; set; }

         string custodyLocation { get; set; }

         string dispositionCharges { get; set; }

         string dispositionChargesDate { get; set; }

         string documentFiledDate { get; set; }

         IFineAmountInfo fineAmountInfo { get; set; }

         string initialCharges { get; set; }

         string initialChargesDate { get; set; }

         IInmateReleaseInfo inmateReleaseInfo { get; set; }

         List<string> judgeName { get; set; }

         IParoleInfo paroleInfo { get; set; }

         IPleaInfo pleaInfo { get; set; }

         IProbationInfo probationInfo { get; set; }

         IOffenderPriorInfo offenderPriorInfo { get; set; }

         string offenderSequenceNumber { get; set; }

         string offenseLocation { get; set; }

         string offenseStatus { get; set; }

         string offenseStatusDate { get; set; }

         string priorOffenseIndicator { get; set; }

         ISentencingInfo sentencingInfo { get; set; }

         string statuteViolated { get; set; }

         string typeOfTrial { get; set; }

         string warrantDate { get; set; }

         string warrrantDescription { get; set; }

         string warrantIssueDate { get; set; }

         string verdict { get; set; }

         List<IAdditionalInfo> additionalInfo { get; set; }
    }
}
