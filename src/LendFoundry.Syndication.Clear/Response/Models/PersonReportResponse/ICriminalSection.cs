﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICriminalSection
    {
        List<ICriminalRecord> CriminalRecord { get; set; }
        List<ICriminalExpansionRecord> CriminalExpansionRecord { get; set; }
    }
}
