using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CommunityService : ICommunityService
    {
        public CommunityService(Proxy.Response.PersonReport.CommunityService communityService)
        {
            if(communityService!=null)
            {
                communityServiceCounty = communityService.CommunityServiceCounty;
                durationOfTime = new DurationOfTime(communityService.DurationOfTime);
            }
        }
        public string communityServiceCounty { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime durationOfTime { get; set; }
    }
}
