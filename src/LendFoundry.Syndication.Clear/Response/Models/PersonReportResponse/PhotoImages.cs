﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PhotoImages : IPhotoImages
    {
        public PhotoImages(Proxy.Response.PersonReport.PhotoImages photoImages)
        {
            if (photoImages!=null)
            {
                photoURL = photoImages.PhotoURL;
                images = photoImages.Images.Select(i => new ImageInfo(i)).ToList<IImageInfo>();
            }
        }
        public List<string> photoURL { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IImageInfo, ImageInfo>))]
        public List<IImageInfo> images { get; set; }


    }
}
