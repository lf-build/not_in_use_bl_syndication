namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IValueAndLabelSet
    {
         string fieldDateLabel { get; set; }

         string fieldDateValue { get; set; }

         string fieldStringLabel { get; set; }

         string fieldStringValue { get; set; }
    }
}
