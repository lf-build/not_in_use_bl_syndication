namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CaseFiling : ICaseFiling
    {
        public CaseFiling(Proxy.Response.PersonReport.CaseFiling caseFiling)
        {
            if(caseFiling!=null)
            {
                fileNumber = caseFiling.FileNumber;
                filedDate = caseFiling.FiledDate;
            }
        }
        public string fileNumber { get; set; }
        public string filedDate { get; set; }
    }
}
