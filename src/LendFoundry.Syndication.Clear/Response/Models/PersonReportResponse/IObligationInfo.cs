﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public  interface IObligationInfo
    {
         string obligation { get; set; }

         string obligationDescription { get; set; }

         string owedTotalAmount { get; set; }

         string totalObligation { get; set; }
    }
}
