﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   
    public class CriminalExpansionRecord : ICriminalExpansionRecord
    {
        public CriminalExpansionRecord(Proxy.Response.PersonReport.CriminalExpansionRecord criminalExpansionRecord)
        {
            if (criminalExpansionRecord != null)
            {
                defendantInfo   = new DefendantInfo(criminalExpansionRecord.DefendantInfo);
                offenderInfo    = criminalExpansionRecord.OffenderInfo.Select(a => new OffenderInfo(a)).ToList<IOffenderInfo>();
                additionalInfo = new AdditionalInfo(criminalExpansionRecord.AdditionalInfo);
                publicationDate = criminalExpansionRecord.PublicationDate;
                sourceName      = criminalExpansionRecord.SourceName;
                sourceCounty    = criminalExpansionRecord.SourceCounty;
                sourceState     = criminalExpansionRecord.SourceState;
                typeOfCriminal  = criminalExpansionRecord.TypeOfCriminal;
                source          = criminalExpansionRecord.Source;
                photoImages     = criminalExpansionRecord.PhotoImages.Select(a=>new PhotoImages1(a)).ToList<IPhotoImages1>();
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IDefendantInfo, DefendantInfo>))]
        public IDefendantInfo defendantInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOffenderInfo, OffenderInfo>))]
        public List<IOffenderInfo> offenderInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAdditionalInfo, AdditionalInfo>))]
        public IAdditionalInfo additionalInfo { get; set; }

        public string publicationDate { get; set; }

        public string sourceName { get; set; }

        public string sourceCounty { get; set; }

        public string sourceState { get; set; }

        public string typeOfCriminal { get; set; }

        public string source { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhotoImages1, PhotoImages1>))]
        public List<IPhotoImages1> photoImages { get; set; }
    }
}
