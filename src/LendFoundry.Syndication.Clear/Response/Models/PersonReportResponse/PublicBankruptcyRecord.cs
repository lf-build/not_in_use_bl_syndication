using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PublicBankruptcyRecord : IPublicBankruptcyRecord
    {
        public PublicBankruptcyRecord(Proxy.Response.PersonReport.PublicBankruptcyRecord publicBankruptcyRecord)
        {
            if (publicBankruptcyRecord != null)
            {
                assetLiabilityInfo  = publicBankruptcyRecord.AssetLiabilityInfo.Select(a=>new AssetLiabilityInfo(a)).ToList<IAssetLiabilityInfo>();
                comments            = publicBankruptcyRecord.Comments.Select(a => new CommentInfo(a)).ToList<ICommentInfo>();
                creditor            = publicBankruptcyRecord.Creditor.Select(a => new Creditor(a)).ToList<ICreditor>();
                debtor              = publicBankruptcyRecord.Debtor.Select(a => new Debtor(a)).ToList<IDebtor>();
                eventHistory        = publicBankruptcyRecord.EventHistory.Select(a => new EventHistory(a)).ToList<IEventHistory>();
                filingInfo          = new FilingInfo1(publicBankruptcyRecord.FilingInfo);
                filingOfficeAddress = new Address(publicBankruptcyRecord.FilingOfficeAddress);
                judge               = publicBankruptcyRecord.Judge;
                planInfo            = new PlanInfo(publicBankruptcyRecord.PlanInfo);
                referee             = new PartyInfo(publicBankruptcyRecord.Referee);
                scheduled341        =new  Scheduled341(publicBankruptcyRecord.Scheduled341);
                trustee             = new Trustee(publicBankruptcyRecord.Trustee);
                source              = publicBankruptcyRecord.Source;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IAssetLiabilityInfo, AssetLiabilityInfo>))]
        public List<IAssetLiabilityInfo> assetLiabilityInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICommentInfo, CommentInfo>))]
        public List<ICommentInfo> comments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICreditor, Creditor>))]
        public List<ICreditor> creditor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IEventHistory, EventHistory>))]
        public List<IEventHistory> eventHistory { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFilingInfo1, FilingInfo1>))]
        public IFilingInfo1 filingInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress filingOfficeAddress { get; set; }
        public string judge { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPlanInfo, PlanInfo>))]
        public IPlanInfo planInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPartyInfo, PartyInfo>))]
        public IPartyInfo referee { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IScheduled341, Scheduled341>))]
        public IScheduled341 scheduled341 { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ITrustee, Trustee>))]
        public ITrustee trustee { get; set; }
        public string source { get; set; }
    }
}
