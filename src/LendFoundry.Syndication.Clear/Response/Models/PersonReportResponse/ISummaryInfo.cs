﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISummaryInfo
    {
        string classSummaryInfo { get; set; }
        string date { get; set; }
        string name { get; set; }
        string state { get; set; }
        string type { get; set; }
    }
}