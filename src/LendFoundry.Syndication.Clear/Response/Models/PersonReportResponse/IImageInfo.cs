﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IImageInfo
    {
        string binaryImage { get; set; }
        string contentType { get; set; }
    }
}