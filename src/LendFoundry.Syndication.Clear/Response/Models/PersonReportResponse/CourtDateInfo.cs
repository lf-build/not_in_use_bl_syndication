namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CourtDateInfo : ICourtDateInfo
    {
        public CourtDateInfo(Proxy.Response.PersonReport.CourtDateInfo courtDateInfo)
        {
            if (courtDateInfo != null)
            {
                nextCourtDate = courtDateInfo.NextCourtDate;
                nextCourtTime = courtDateInfo.NextCourtTime;
            }
        }

        public string nextCourtDate { get; set; }
        public string nextCourtTime { get; set; }
    }
}
