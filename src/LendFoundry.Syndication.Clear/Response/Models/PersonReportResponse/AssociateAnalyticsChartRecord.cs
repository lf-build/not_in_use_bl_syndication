using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AssociateAnalyticsChartRecord : IAssociateAnalyticsChartRecord
    {
        public AssociateAnalyticsChartRecord(Proxy.Response.PersonReport.AssociateAnalyticsChartRecord associateAnalyticsChartRecord)
        {
            if (associateAnalyticsChartRecord != null)
            {
                associates             = associateAnalyticsChartRecord.Associates.Select(s => new ChartDetails(s)).ToList<IChartDetails>();
                relatives              = associateAnalyticsChartRecord.Relatives.Select(s => new ChartDetails(s)).ToList<IChartDetails>();
                associatesWithEntityId = associateAnalyticsChartRecord.AssociatesWithEntityId.Select(s => new ChartDetailsWithEntityId(s)).ToList<IChartDetailsWithEntityId>();
                relativeWithEntityId   = associateAnalyticsChartRecord.RelativeWithEntityId.Select(s => new ChartDetailsWithEntityId(s)).ToList<IChartDetailsWithEntityId>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IChartDetails, ChartDetails>))]
        public List<IChartDetails> associates { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IChartDetails, ChartDetails>))]
        public List<IChartDetails> relatives { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IChartDetailsWithEntityId, ChartDetailsWithEntityId>))]
        public List<IChartDetailsWithEntityId> associatesWithEntityId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IChartDetailsWithEntityId, ChartDetailsWithEntityId>))]
        public List<IChartDetailsWithEntityId> relativeWithEntityId { get; set; }
    }
}
