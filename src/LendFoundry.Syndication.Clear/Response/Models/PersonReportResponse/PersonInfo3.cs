using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonInfo3 : IPersonInfo3
    {
        public PersonInfo3(Proxy.Response.PersonReport.PersonInfo3 personInfo)
        {
            if (personInfo != null)
            {
                personName            = personInfo.PersonName.Select(a=>new PersonName(a)).ToList<IPersonName>();
                personProfile         =new PersonProfile(personInfo.PersonProfile);
                personAKA             = personInfo.PersonAKA.Select(a=>new PersonAKA1(a)).ToList<IPersonAKA1>();
                personSSN             =new SSNInfo(personInfo.PersonSSN);
                personMilitarySummary = personInfo.PersonMilitarySummary.Select(a=>new PersonMilitarySummary1(a)).ToList<IPersonMilitarySummary1>();
                personEducation       = personInfo.PersonEducation.Select(a=>new PersonEducation(a)).ToList<IPersonEducation>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> personName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile personProfile { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPersonAKA1, PersonAKA1>))]
        public List<IPersonAKA1> personAKA { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISSNInfo, SSNInfo>))]
        public ISSNInfo personSSN { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPersonMilitarySummary1, PersonMilitarySummary1>))]
        public List<IPersonMilitarySummary1> personMilitarySummary { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPersonEducation, PersonEducation>))]
        public List<IPersonEducation> personEducation { get; set; }
    }
}
