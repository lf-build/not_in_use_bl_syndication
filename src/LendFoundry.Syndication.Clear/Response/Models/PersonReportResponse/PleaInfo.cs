namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PleaInfo : IPleaInfo
    {
        public PleaInfo(Proxy.Response.PersonReport.PleaInfo pleaInfo)
        {
            if (pleaInfo != null)
            {
                changedPlea      = pleaInfo.ChangedPlea;
                originalPlea     = pleaInfo.OriginalPlea;
                pleaDate         = pleaInfo.PleaDate;
                pleaWithdrawDate = pleaInfo.PleaWithdrawDate;
            }
        }

        public string changedPlea { get; set; }
        public string originalPlea { get; set; }
        public string pleaDate { get; set; }
        public string pleaWithdrawDate { get; set; }
    }
}
