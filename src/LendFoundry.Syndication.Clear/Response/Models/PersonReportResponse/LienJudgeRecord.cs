using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LienJudgeRecord : ILienJudgeRecord
    {
        public LienJudgeRecord(Proxy.Response.PersonReport.LienJudgeRecord lienJudgeRecord)
        {
            if (lienJudgeRecord != null)
            {
                creditor   = lienJudgeRecord.Creditor.Select(s => new Creditor(s)).ToList<ICreditor>(); ;
                debtor     = lienJudgeRecord.Debtor.Select(s => new Debtor(s)).ToList<IDebtor>(); ;
                filingInfo = lienJudgeRecord.FilingInfo.Select(s => new FilingInfo1(s)).ToList<IFilingInfo1>();
                source     = lienJudgeRecord.Source;
            }
        }

        public List<ICreditor> creditor { get; set; }
        public List<IDebtor> debtor { get; set; }
        public List<IFilingInfo1> filingInfo { get; set; }
        public string source { get; set; }
    }
}
