using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICourtInfo
    {
         List<IAddress> address { get; set; }

         string arraignmentDate { get; set; }

         ICaseInfo caseInfo { get; set; }

         string caseTypeAmended { get; set; }

         string courtDivision { get; set; }

         string courtName { get; set; }

         IAttorneyInfo defenseAttorney { get; set; }

         List<IDefenseAttorneyList> defenseAttorneyList { get; set; }

         string finalPlea { get; set; }

         IFineAmountInfo fineAmountInfo { get; set; }

         List<IPersonName> judgeName { get; set; }

         string judicialDistrictOrCircuit { get; set; }

         string normalizedCourtName { get; set; }

         string originalCourtName { get; set; }

         string preHearingFlag { get; set; }

         IAttorneyInfo prosecutingAttorney { get; set; }

         List<IProsecutingAttorneyList> prosecutingAttorneyList { get; set; }

         string typeOfTrial { get; set; }

         string verdict { get; set; }

         string verdictDate { get; set; }

         string dispositionDecision { get; set; }

         string dispositionFinalDate { get; set; }

         string dispositionCategory { get; set; }
    }
}
