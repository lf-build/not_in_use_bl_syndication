namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPriorChargesInfo
    {
         string priorIncarcerationCount { get; set; }

         string priorIncarcerationDate { get; set; }

         string priorLocation { get; set; }

         string priorStatus { get; set; }

         string priorOffense { get; set; }
    }
}
