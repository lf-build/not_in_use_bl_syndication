﻿

using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Debtor: IDebtor
    {
        public Debtor(Proxy.Response.PersonReport.Debtor debtor)
        {
            if (debtor != null)
            {
                debtorOwedAmount  = debtor.DebtorOwedAmount;
                debtorOccupation  = debtor.DebtorOccupation;
                numberOfDebtors   = debtor.NumberOfDebtors;
                partyInfo         = new PartyInfo(debtor.PartyInfo);
                debtorDescription = debtor.DebtorDescription;
                debtorAmount      = debtor.DebtorAmount;
                obligationInfo    = debtor.ObligationInfo.Select(a=>new ObligationInfo(a)).ToList<IObligationInfo>();
                dispositionInfo   = new CaseDisposition(debtor.DispositionInfo);
                demandAmount      = debtor.DemandAmount;
                status            = debtor.Status;
                debtorSignerInfo  = new DebtorSignerInfo(debtor.DebtorSignerInfo);
                dUNSNumber        = debtor.DUNSNumber;
                taxID             = debtor.TaxID;
                typeOfDebtor      = debtor.TypeOfDebtor;
                typeOfDebtorName  = debtor.TypeOfDebtorName;
                sSNInfo           = debtor.SSNInfo.Select(a=>new SSNInfo(a)).ToList<ISSNInfo>();
            }
        }

        public string debtorOwedAmount { get; set; }

        public string debtorOccupation {get; set; }

        public string numberOfDebtors {get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPartyInfo, PartyInfo>))]
        public IPartyInfo partyInfo {get; set; }

        public string debtorDescription {get; set; }

        public List<string> debtorAmount {get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IObligationInfo, ObligationInfo>))]
        public List<IObligationInfo> obligationInfo {get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICaseDisposition, CaseDisposition>))]
        public ICaseDisposition dispositionInfo {get; set; }

        public List<string> demandAmount {get; set; }

        public string status {get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDebtorSignerInfo, DebtorSignerInfo>))]
        public IDebtorSignerInfo debtorSignerInfo {get; set; }

        public string dUNSNumber {get; set; }

        public string taxID {get; set; }

        public List<string> typeOfDebtor {get; set; }

        public List<string> typeOfDebtorName {get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISSNInfo, SSNInfo>))]
        public List<ISSNInfo> sSNInfo {get; set; }
    }
}
