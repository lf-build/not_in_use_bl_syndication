using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ILienJudgmentSection
    {
         List<ILienJudgeRecord> lienJudgeRecord{ get; set; }

         List<ILienJudgeNYRecord> lienJudgeNYRecord{ get; set; }

         List<ILienJudgeMultipleRecord> lienJudgeMultipleRecord{ get; set; }
    }
}
