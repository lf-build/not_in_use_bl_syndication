﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LienInfo :ILienInfo
    {
        public LienInfo(Proxy.Response.PersonReport.LienInfo lienInfo)
        {
            if (lienInfo != null)
            {
                debtLot                   = lienInfo.DebtLot;
                debtNumber                = lienInfo.DebtNumber;
                judgmentAmount            = lienInfo.JudgmentAmount;
                satisfactionDate          = lienInfo.SatisfactionDate;
                typeofSatisfaction        = lienInfo.TypeofSatisfaction;
                sheriffExecutionRequired  = lienInfo.SheriffExecutionRequired;
                expirationDate            = lienInfo.ExpirationDate;
                creditorAmount            = lienInfo.CreditorAmount;
                lienAmount                = lienInfo.LienAmount;
                lienPrincipalAmount       = lienInfo.LienPrincipalAmount;
                taxAmount                 = lienInfo.TaxAmount;
                lienTotalAmount           = lienInfo.LienTotalAmount;
                totalAmountPaid           = lienInfo.TotalAmountPaid;
                totalCalculatedAmount     = lienInfo.TotalCalculatedAmount;
                totalCalculatedAmountPaid = lienInfo.TotalCalculatedAmountPaid;
                statusInfo                = new StatusInfo(lienInfo.StatusInfo);
                subjudgmentInfo           = new SubjudgmentInfo(lienInfo.SubjudgmentInfo);
            }
        }

        public string debtLot { get; set; }

        public string debtNumber { get; set; }

        public string judgmentAmount { get; set; }

        public string satisfactionDate { get; set; }

        public string typeofSatisfaction { get; set; }

        public string sheriffExecutionRequired { get; set; }

        public List<string> expirationDate { get; set; }

        public string creditorAmount { get; set; }

        public string lienAmount { get; set; }

        public string lienPrincipalAmount { get; set; }

        public string taxAmount { get; set; }

        public string lienTotalAmount { get; set; }

        public string totalAmountPaid { get; set; }

        public string totalCalculatedAmount { get; set; }

        public string totalCalculatedAmountPaid { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatusInfo, StatusInfo>))]
        public IStatusInfo statusInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISubjudgmentInfo, SubjudgmentInfo>))]
        public ISubjudgmentInfo subjudgmentInfo { get; set; }
    }
}
