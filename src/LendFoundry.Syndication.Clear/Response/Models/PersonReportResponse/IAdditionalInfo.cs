using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAdditionalInfo
    {
         List<IDefenseAttorneyList> defenseAttorneyList { get; set; }

         List<string> judgeName { get; set; }

         List<IOfficerInfo> officerInfo { get; set; }
    }
}
