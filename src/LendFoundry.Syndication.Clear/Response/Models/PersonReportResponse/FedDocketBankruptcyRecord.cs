using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FedDocketBankruptcyRecord : IFedDocketBankruptcyRecord
    {
        public FedDocketBankruptcyRecord(Proxy.Response.PersonReport.FedDocketBankruptcyRecord fedDocketBankruptcyRecord)
        {
            if (fedDocketBankruptcyRecord != null)
            {
                creditor     = fedDocketBankruptcyRecord.Creditor.Select(a=>new Creditor(a)).ToList<ICreditor>();
                debtor       = fedDocketBankruptcyRecord.Debtor.Select(a => new Debtor(a)).ToList<IDebtor>();
                filingInfo   = new FilingInfo1(fedDocketBankruptcyRecord.FilingInfo);
                judge        = fedDocketBankruptcyRecord.Judge;
                otherParty   = fedDocketBankruptcyRecord.OtherParty.Select(a=> new PartyInfo(a)).ToList<IPartyInfo>();
                scheduled341 = fedDocketBankruptcyRecord.Scheduled341.Select(a => new Scheduled341(a)).ToList<IScheduled341>();
                trustee      = fedDocketBankruptcyRecord.Trustee.Select(a => new Trustee(a)).ToList<ITrustee>();
                caseCategory = fedDocketBankruptcyRecord.CaseCategory;
                caseDocketID = fedDocketBankruptcyRecord.CaseDocketID;
                caseTitle    = fedDocketBankruptcyRecord.CaseTitle;
                source       = fedDocketBankruptcyRecord.Source;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ICreditor, Creditor>))]
        public List<ICreditor> creditor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFilingInfo1, FilingInfo1>))]
        public IFilingInfo1 filingInfo { get; set; }
        public List<string> judge { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPartyInfo, PartyInfo>))]
        public List<IPartyInfo> otherParty { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IScheduled341, Scheduled341>))]
        public List<IScheduled341> scheduled341 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITrustee, Trustee>))]
        public List<ITrustee> trustee { get; set; }
        public List<string> caseCategory { get; set; }
        public List<string> caseDocketID { get; set; }
        public List<string> caseTitle { get; set; }
        public string source { get; set; }
    }
}
