﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDeathSummary
    {
        IPersonProfile personDeath { get; set; }
        ISourceInfo sourceInfo { get; set; }
    }
}