using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public  interface IArrestRecord
    {
         List<IAddress> address { get; set; }

         List<IArrestInfo> arrestInfo { get; set; }

         List<ICourtInfo> courtInfo { get; set; }

         List<ICriminalCharges> criminalCharges { get; set; }

         IIDInfo iDInfo { get; set; }

         List<IInstitutionInfo> institutionInfo { get; set; }

         IPersonInfo3 personInfo { get; set; }

         string source { get; set; }

         List<IPhotoImages1> photoImages { get; set; }
    }
}
