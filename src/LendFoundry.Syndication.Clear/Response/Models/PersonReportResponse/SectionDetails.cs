﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SectionDetails : ISectionDetails
    {
        public SectionDetails(Proxy.Response.PersonReport.SectionDetails sectionDetails,string sectionName)
        {
            if (sectionDetails!=null && !string.IsNullOrWhiteSpace(sectionName))
            {
                if(sectionName== "UserTermsSection")
                userTermsSection = new UserTermsSection(sectionDetails.UserTermsSection);

                if (sectionName == "SubjectSection")
                    subjectSection = sectionDetails.SubjectSection.Select(s => new SubjectRecord(s)).ToList<ISubjectRecord>();
                if (sectionName == "AddressInfo")
                    addressSection = sectionDetails.AddressSection.Select(s => new AddressInfo(s)).ToList<IAddressInfo>();
                if (sectionName == "UtilityRecord")
                    utilitySection = sectionDetails.UtilitySection.Select(s => new UtilityRecord(s)).ToList<IUtilityRecord>();
                if (sectionName == "AddressInfo")
                    phoneNumberSection = sectionDetails.PhoneNumberSection.Select(s => new AddressInfo(s)).ToList<IAddressInfo>();
                if (sectionName == "CriminalSection")
                    criminalSection = new CriminalSection(sectionDetails.CriminalSection);
                if (sectionName == "BankruptcySection")
                    bankruptcySection = new BankruptcySection(sectionDetails.BankruptcySection);
                if (sectionName == "LienJudgmentSection")
                    lienJudgmentSection = new LienJudgmentSection(sectionDetails.LienJudgmentSection);
                if (sectionName == "AssociateAnalyticsChartSection")
                    associateAnalyticsChartSection = sectionDetails.AssociateAnalyticsChartSection.Select(s => new AssociateAnalyticsChartRecord(s)).ToList<IAssociateAnalyticsChartRecord>();
                if (sectionName == "ArrestSection")
                    arrestSection = sectionDetails.ArrestSection.Select(s => new ArrestRecord(s)).ToList<IArrestRecord>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IUserTermsSection, UserTermsSection>))]
        public IUserTermsSection userTermsSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISubjectRecord, SubjectRecord>))]
        public List<ISubjectRecord> subjectSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddressInfo,AddressInfo>))]
        public List<IAddressInfo> addressSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IUtilityRecord, UtilityRecord>))]
        public List<IUtilityRecord> utilitySection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddressInfo,AddressInfo>))]
        public List<IAddressInfo> phoneNumberSection { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICriminalSection, CriminalSection>))]
        public ICriminalSection criminalSection { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBankruptcySection, BankruptcySection>))]
        public IBankruptcySection bankruptcySection { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILienJudgmentSection, LienJudgmentSection>))]
        public ILienJudgmentSection lienJudgmentSection { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAssociateAnalyticsChartRecord, AssociateAnalyticsChartRecord>))]
        public List<IAssociateAnalyticsChartRecord> associateAnalyticsChartSection { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IArrestRecord, ArrestRecord>))]
        public List<IArrestRecord> arrestSection { get; set; }

    }
}
