﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICreditor
    {
         IPartyInfo partyInfo { get; set; }

         string creditHolderLevelDesc { get; set; }

         string creditHolderSerialNumber { get; set; }

         string creditHolder { get; set; }

         string creditHolderLevel { get; set; }

         string dUNSNumber { get; set; }

         string foreignRegion { get; set; }

         string claimDescription { get; set; }

         string otherClaim { get; set; }

         string securedClaim { get; set; }

         string unsecuredClaim { get; set; }
    }
}
