namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ITrustee
    {
         IPartyInfo partyInfo { get; set; }

         string businessName { get; set; }
    }
}
