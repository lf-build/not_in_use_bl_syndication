﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Comments : IComments
    {
        public Comments(Proxy.Response.PersonReport.Comments comments)
        {
            if (comments != null)
            {
                commentDate     = comments.CommentDate;
                commentSource   = comments.CommentSource;
                commentText     = comments.CommentText;
                commentText2    = comments.CommentText2;
                sourceBusiness  = comments.SourceBusiness;
                sourceTitle     = comments.SourceTitle;
                sourceTitle2    = comments.SourceTitle2;
                sourceTitle3    = comments.SourceTitle3;
                sourceTitleDesc = comments.SourceTitleDesc;
            }
        }

        public string commentDate { get; set; }

        public string commentSource { get; set; }

        public string commentText { get; set; }

        public string commentText2 { get; set; }

        public string sourceBusiness { get; set; }

        public string sourceTitle { get; set; }

        public string sourceTitle2 { get; set; }

        public string sourceTitle3 { get; set; }

        public string sourceTitleDesc { get; set; }
    }
}
