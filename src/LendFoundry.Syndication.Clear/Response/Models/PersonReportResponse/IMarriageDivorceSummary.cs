﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IMarriageDivorceSummary
    {
        System.Collections.Generic.List<ISummaryInfo> divorceInfo { get; set; }
        System.Collections.Generic.List<ISummaryInfo> marriageInfo { get; set; }
    }
}