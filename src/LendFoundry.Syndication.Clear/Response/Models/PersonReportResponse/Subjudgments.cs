﻿
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Subjudgments :ISubjudgments
    {
        public Subjudgments(Proxy.Response.PersonReport.Subjudgments subjudgments)
        {
            if (subjudgments != null)
            {
                lienInfo     = subjudgments.LienInfo.Select(a=>new LienInfo(a)).ToList<ILienInfo>();
                judgmentInfo = subjudgments.JudgmentInfo.Select(a => new JudgmentInfo(a)).ToList<IJudgmentInfo>();
                commentInfo  = subjudgments.CommentInfo.Select(a => new CommentInfo(a)).ToList<ICommentInfo>();
                debtor       = subjudgments.Debtor.Select(a => new Debtor(a)).ToList<IDebtor>();
                creditor     = subjudgments.Creditor.Select(a => new Creditor(a)).ToList<ICreditor>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ILienInfo, LienInfo>))]
        public List<ILienInfo> lienInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IJudgmentInfo, JudgmentInfo>))]
        public List<IJudgmentInfo> judgmentInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICommentInfo, CommentInfo>))]
        public List<ICommentInfo> commentInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICreditor, Creditor>))]
        public List<ICreditor> creditor { get; set; }
    }
}
