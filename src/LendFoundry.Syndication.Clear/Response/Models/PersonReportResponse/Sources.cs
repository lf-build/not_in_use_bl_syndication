namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Sources : ISources
    {
        
        public Sources(Proxy.Response.PersonReport.Sources sources)
        {
            if (sources != null)
            {
                sourceName         = sources.SourceName;
                sourceDocumentGuid = sources.SourceDocumentGuid;
                sourceCollection   = sources.SourceCollection;
                sourceGenericName  = sources.SourceGenericName;
            }
        }

        public string sourceName { get; set; }
        public string sourceDocumentGuid { get; set; }
        public string sourceCollection { get; set; }
        public string sourceGenericName { get; set; }
    }
}
