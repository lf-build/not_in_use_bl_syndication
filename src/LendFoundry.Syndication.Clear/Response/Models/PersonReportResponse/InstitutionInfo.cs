using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class InstitutionInfo : IInstitutionInfo
    {
        public InstitutionInfo(Proxy.Response.PersonReport.InstitutionInfo institutionInfo)
           
        {
            if (institutionInfo!=null)
            {
                additionalInstitution  = new AdditionalInstitution(institutionInfo.AdditionalInstitution);
                address                = new Address(institutionInfo.Address);
                admissionInfo          = new AdmissionInfo(institutionInfo.AdmissionInfo);
                classAtEscape          = institutionInfo.ClassAtEscape;
                escapeDate             = institutionInfo.EscapeDate;
                escapedFacility        = institutionInfo.EscapedFacility;
                escapeHistoryDesc      = institutionInfo.EscapeHistoryDesc;
                escapeRecaptureDate    = institutionInfo.EscapeRecaptureDate;
                gainTime               = institutionInfo.GainTime;
                gainTimeEffectiveDate  = institutionInfo.GainTimeEffectiveDate;
                inmateCellNumber       = institutionInfo.InmateCellNumber;
                inmateNumber           = institutionInfo.InmateNumber;
                inmateStatus           = institutionInfo.InmateStatus;
                inmateCustodyClass     = institutionInfo.InmateCustodyClass;
                lastMoveOrTransfer     = institutionInfo.LastMoveOrTransfer;
                lastMoveOrTransferDate = institutionInfo.LastMoveOrTransferDate;
                phone                  = new PhoneInfo(institutionInfo.Phone);
                specialProvision       = institutionInfo.SpecialProvision;
                specialProvisionDate   = institutionInfo.SpecialProvisionDate;
                timeServedCredit       = institutionInfo.TimeServedCredit;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IAdditionalInstitution, AdditionalInstitution>))]
        public IAdditionalInstitution additionalInstitution { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAdmissionInfo, AdmissionInfo>))]
        public IAdmissionInfo admissionInfo { get; set; }
        public string classAtEscape { get; set; }
        public string escapeDate { get; set; }
        public string escapedFacility { get; set; }
        public string escapeHistoryDesc { get; set; }
        public string escapeRecaptureDate { get; set; }
        public string gainTime { get; set; }
        public string gainTimeEffectiveDate { get; set; }
        public string inmateCellNumber { get; set; }
        public string inmateNumber { get; set; }
        public string inmateStatus { get; set; }
        public string inmateCustodyClass { get; set; }
        public string lastMoveOrTransfer { get; set; }
        public string lastMoveOrTransferDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phone { get; set; }
        public string specialProvision { get; set; }
        public string specialProvisionDate { get; set; }
        public string timeServedCredit { get; set; }
    }
}
