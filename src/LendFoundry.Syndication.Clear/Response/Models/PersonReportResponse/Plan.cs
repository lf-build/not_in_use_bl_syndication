namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Plan : IPlan
    {
        public Plan(Proxy.Response.PersonReport.Plan plan)
        {
            if (plan != null)
            {
                authorizedBy     = plan.AuthorizedBy;
                authorizedTitle  = plan.AuthorizedTitle;
                planFilingDate   = plan.PlanFilingDate;
                planStatus       = plan.PlanStatus;
                planStatusDate   = plan.PlanStatusDate;
                typeOfAuthorizer = plan.TypeOfAuthorizer;
            }
        }

        public string authorizedBy { get; set; }
        public string authorizedTitle { get; set; }
        public string planFilingDate { get; set; }
        public string planStatus { get; set; }
        public string planStatusDate { get; set; }
        public string typeOfAuthorizer { get; set; }
    }
}
