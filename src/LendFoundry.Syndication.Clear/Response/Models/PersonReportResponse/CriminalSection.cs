﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   
    public class CriminalSection : ICriminalSection
    {
        public CriminalSection(Proxy.Response.PersonReport.CriminalSection CriminalSection)
        {
            if (CriminalSection != null)
            {
                CriminalRecord = CriminalSection.CriminalRecord.Select(a=>new CriminalRecord(a)).ToList<ICriminalRecord>();
                CriminalExpansionRecord = CriminalSection.CriminalExpansionRecord.Select(a => new CriminalExpansionRecord(a)).ToList<ICriminalExpansionRecord>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalRecord, CriminalRecord>))]
        public List<ICriminalRecord> CriminalRecord { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICriminalExpansionRecord, CriminalExpansionRecord>))]
        public List<ICriminalExpansionRecord> CriminalExpansionRecord { get; set; }

    }
}
