using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFedDocketBankruptcyRecord
    {
         List<ICreditor> creditor { get; set; }

         List<IDebtor> debtor { get; set; }

         IFilingInfo1 filingInfo { get; set; }

         List<string> judge { get; set; }

         List<IPartyInfo> otherParty { get; set; }

         List<IScheduled341> scheduled341 { get; set; }

         List<ITrustee> trustee { get; set; }

         List<string> caseCategory { get; set; }

         List<string> caseDocketID { get; set; }

         List<string> caseTitle { get; set; }

         string source { get; set; }
    }
}
