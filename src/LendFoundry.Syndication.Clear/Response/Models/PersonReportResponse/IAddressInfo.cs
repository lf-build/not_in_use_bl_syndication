﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAddressInfo
    {
        IAddress address { get; set; }
        List<IPersonName> personName { get; set; }
        List<IPhoneInfo> phone { get; set; }
    List<ISourceInfo> sourceInfo { get; set; }
    }
}