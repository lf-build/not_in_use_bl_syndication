using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IIDInfo
    {
         List<IDriverLicenseInfo> driverLicenseInfo { get; set; }

         List<string> fBICriminalID { get; set; }

         List<IUndocumentedImmigrantInfo> undocumentedImmigrantInfo { get; set; }

         string sexOffenderID { get; set; }

         List<IIDDetails> stateIDInfo { get; set; }

         List<IIDDetails> otherIDInfo { get; set; }

         List<string> nationalID { get; set; }

         List<string> passportID { get; set; }

         string inmateID { get; set; }

         List<ISSNInfo> personSSN { get; set; }

         List<IPersonProfile> personProfile { get; set; }
    }
}
