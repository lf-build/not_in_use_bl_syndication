﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IStatusInfo
    {
         string status { get; set; }

         string statusDate { get; set; }
    }
}
