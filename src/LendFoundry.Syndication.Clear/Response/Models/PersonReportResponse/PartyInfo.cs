﻿

using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PartyInfo: IPartyInfo
    {
        public PartyInfo(Proxy.Response.PersonReport.PartyInfo partyInfo)
        {
            if (partyInfo != null)
            {
               personName            = new PersonName(partyInfo.PersonName);
               businessName          = partyInfo.BusinessName;
               typeOfParty           = partyInfo.TypeOfParty;
               address               = partyInfo.Address.Select(a=> new Address(a)).ToList<IAddress>();
               phoneInfo             = new PhoneInfo(partyInfo.PhoneInfo);
               attorneyInfo          = partyInfo.AttorneyInfo.Select(a=>new AttorneyInfo(a)).ToList<IAttorneyInfo>();
               comment               = partyInfo.Comment;
               dUNSNumber            = partyInfo.DUNSNumber;
               headquarterDUNSNumber = partyInfo.HeadquarterDUNSNumber;
               foreignReg            = partyInfo.ForeignReg;
               sSNInfo               = partyInfo.SSNInfo.Select(a=>new SSNInfo(a)).ToList<ISSNInfo>();
               driverLicenseInfo     = partyInfo.DriverLicenseInfo.Select(a=>new DriverLicenseInfo(a)).ToList<IDriverLicenseInfo>();
               personProfile         = partyInfo.PersonProfile.Select(a=>new PersonProfile(a)).ToList<IPersonProfile>();
               fEIN                  = partyInfo.FEIN;
               aKAName               = partyInfo.AKAName.Select(a=>new PersonName(a)).ToList<IPersonName>();
               reportedDate          = partyInfo.ReportedDate;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }

        public string businessName { get; set; }

        public List<string> typeOfParty { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phoneInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAttorneyInfo, AttorneyInfo>))]
        public List<IAttorneyInfo> attorneyInfo { get; set; }

        public List<string> comment { get; set; }

        public List<string> dUNSNumber { get; set; }

        public List<string> headquarterDUNSNumber { get; set; }

        public string foreignReg { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISSNInfo, SSNInfo>))]
        public List<ISSNInfo> sSNInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public List<IDriverLicenseInfo> driverLicenseInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonProfile, PersonProfile>))]
        public List<IPersonProfile> personProfile { get; set; }

        public List<string> fEIN { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> aKAName { get; set; }

        public string reportedDate { get; set; }

    }
}
