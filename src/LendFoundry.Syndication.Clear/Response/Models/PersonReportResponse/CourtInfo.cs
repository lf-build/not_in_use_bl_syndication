using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CourtInfo : ICourtInfo
    {
        
        public CourtInfo(Proxy.Response.PersonReport.CourtInfo courtInfo)
        {
            if (courtInfo != null)
            {
                address                   = courtInfo.Address.Select(a=> new Address(a)).ToList<IAddress>();
                arraignmentDate           = courtInfo.ArraignmentDate;
                caseInfo                  = new CaseInfo(courtInfo.CaseInfo);
                caseTypeAmended           = courtInfo.CaseTypeAmended;
                courtDivision             = courtInfo.CourtDivision;
                courtName                 = courtInfo.CourtName;
                defenseAttorney           = new AttorneyInfo(courtInfo.DefenseAttorney);
                defenseAttorneyList       = courtInfo.DefenseAttorneyList.Select(a=>new DefenseAttorneyList(a)).ToList<IDefenseAttorneyList>();
                finalPlea                 = courtInfo.FinalPlea;
                fineAmountInfo            = new FineAmountInfo(courtInfo.FineAmountInfo);
                judgeName                 = courtInfo.JudgeName.Select(a=>new PersonName(a)).ToList<IPersonName>();
                judicialDistrictOrCircuit = courtInfo.JudicialDistrictOrCircuit;
                normalizedCourtName       = courtInfo.NormalizedCourtName;
                originalCourtName         = courtInfo.OriginalCourtName;
                preHearingFlag            = courtInfo.PreHearingFlag;
                prosecutingAttorney       = new AttorneyInfo(courtInfo.ProsecutingAttorney);
                prosecutingAttorneyList   = courtInfo.ProsecutingAttorneyList.Select(a=>new ProsecutingAttorneyList(a)).ToList<IProsecutingAttorneyList>();
                typeOfTrial               = courtInfo.TypeOfTrial;
                verdict                   = courtInfo.Verdict;
                verdictDate               = courtInfo.VerdictDate;
                dispositionDecision       = courtInfo.DispositionDecision;
                dispositionFinalDate      = courtInfo.DispositionFinalDate;
                dispositionCategory       = courtInfo.DispositionCategory;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address { get; set; }
        public string arraignmentDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICaseInfo, CaseInfo>))]
        public ICaseInfo caseInfo { get; set; }
        public string caseTypeAmended { get; set; }
        public string courtDivision { get; set; }
        public string courtName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAttorneyInfo, AttorneyInfo>))]
        public IAttorneyInfo defenseAttorney { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDefenseAttorneyList, DefenseAttorneyList>))]
        public List<IDefenseAttorneyList> defenseAttorneyList { get; set; }
        public string finalPlea { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFineAmountInfo, FineAmountInfo>))]
        public IFineAmountInfo fineAmountInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> judgeName { get; set; }
        public string judicialDistrictOrCircuit { get; set; }
        public string normalizedCourtName { get; set; }
        public string originalCourtName { get; set; }
        public string preHearingFlag { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAttorneyInfo, AttorneyInfo>))]
        public IAttorneyInfo prosecutingAttorney { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IProsecutingAttorneyList, ProsecutingAttorneyList>))]
        public List<IProsecutingAttorneyList> prosecutingAttorneyList { get; set; }
        public string typeOfTrial { get; set; }
        public string verdict { get; set; }
        public string verdictDate { get; set; }
        public string dispositionDecision { get; set; }
        public string dispositionFinalDate { get; set; }
        public string dispositionCategory { get; set; }
    }
}
