﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISectionDetails
    {
        List<IAddressInfo> addressSection { get; set; }
        List<IAddressInfo> phoneNumberSection { get; set; }
        List<ISubjectRecord> subjectSection { get; set; }
        IUserTermsSection userTermsSection { get; set; }
        List<IUtilityRecord> utilitySection { get; set; }
        IBankruptcySection bankruptcySection { get; set; }

    }
}