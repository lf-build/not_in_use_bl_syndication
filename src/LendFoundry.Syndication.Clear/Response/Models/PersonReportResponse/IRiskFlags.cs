namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IRiskFlags
    {
         string assocWithOFACGlobalPEP { get; set; }

         string oFAC { get; set; }

         string worldCheck { get; set; }

         string globalSanctions { get; set; }

         string residentialUsedAsBusiness { get; set; }

         string prisonAddress { get; set; }

         string pOBoxAddress { get; set; }

         string bankruptcy { get; set; }

         string assocRelativeWithResidentialUsedAsBusiness { get; set; }

         string assocRelativeWithPrisonAddress { get; set; }

         string assocRelativeWithPOBoxAddress { get; set; }

         string criminal { get; set; }

         string multipleSSN { get; set; }

         string sSNMultipleIndividuals { get; set; }

         string recordedAsDeceased { get; set; }

         string ageYoungerThanSSN { get; set; }

         string addressReportedLessNinetyDays { get; set; }

         string sSNFormatInvalid { get; set; }

         string healthcareSanction { get; set; }

         string phoneNumberInconsistentAddress { get; set; }

         string arrest { get; set; }
    }
}
