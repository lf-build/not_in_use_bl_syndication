﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IWorkAffiliations
    {
        string businessName { get; set; }
        string emailAddress { get; set; }
        string title { get; set; }
    }
}