﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CommentSource: ICommentSource
    {
        public CommentSource(Proxy.Response.PersonReport.CommentSource commentSource)
        {
            if (commentSource != null)
            {
                sourceBusinessName     = commentSource.SourceBusinessName;
                sourceName             = commentSource.SourceName;
                sourceTitle            = commentSource.SourceTitle;
                sourceTitle2           = commentSource.SourceTitle2;
                sourceTitle3           = commentSource.SourceTitle3;
                statusInfo             = commentSource.StatusInfo;
                sourceTitleDescription = commentSource.SourceTitleDescription;
            }
        }

        public string sourceBusinessName { get; set; }

        public string sourceName { get; set; }

        public string sourceTitle { get; set; }

        public string sourceTitle2 { get; set; }

        public string sourceTitle3 { get; set; }

        public string statusInfo { get; set; }

        public string sourceTitleDescription { get; set; }

    }
}
