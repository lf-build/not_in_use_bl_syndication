namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class IDDetails : IIDDetails
    {
        public IDDetails(Proxy.Response.PersonReport.IDDetails iDDetails)
        {
            if(iDDetails!=null)
            {
                stateID = iDDetails.StateID;
                fullStateID = iDDetails.FullStateID;
                iDType = iDDetails.IDType;
                iDNumber = iDDetails.IDNumber;
                iDIssuer = iDDetails.IDIssuer;
            }
        }
        public string stateID { get; set; }
        public string fullStateID { get; set; }
        public string iDType { get; set; }
        public string iDNumber { get; set; }
        public string iDIssuer { get; set; }
    }
}
