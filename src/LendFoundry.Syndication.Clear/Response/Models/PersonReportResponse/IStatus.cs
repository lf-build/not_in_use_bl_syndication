﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IStatus
    {
        int StatusCode { get; set; }
        int SubStatusCode { get; set; }
    }
}