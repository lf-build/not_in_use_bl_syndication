namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAdditionalInstitution
    {
         string assignmentTypeDesc { get; set; }

         string custodyReviewDate { get; set; }

         string custodyReviewDesc { get; set; }

         IAdmissionInfo admissionInfo { get; set; }
    }
}
