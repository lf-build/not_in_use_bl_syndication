namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IRiskFlagsWithDocguids
    {
        IRiskFlagInfo assocWithOFACGlobalPEP { get; set; }

        IRiskFlagInfo oFAC { get; set; }

        IRiskFlagInfo worldCheck { get; set; }

        IRiskFlagInfo globalSanctions { get; set; }

        IRiskFlagInfo residentialUsedAsBusiness { get; set; }

        IRiskFlagInfo prisonAddress { get; set; }

        IRiskFlagInfo pOBoxAddress { get; set; }

        IRiskFlagInfo bankruptcy { get; set; }

        IRiskFlagInfo assocRelativeWithResidentialUsedAsBusiness { get; set; }

        IRiskFlagInfo assocRelativeWithPrisonAddress { get; set; }

        IRiskFlagInfo assocRelativeWithPOBoxAddress { get; set; }

        IRiskFlagInfo criminal { get; set; }

        IRiskFlagInfo multipleSSN { get; set; }

        IRiskFlagInfo sSNMultipleIndividuals { get; set; }

        IRiskFlagInfo recordedAsDeceased { get; set; }

        IRiskFlagInfo ageYoungerThanSSN { get; set; }

        IRiskFlagInfo addressReportedLessNinetyDays { get; set; }

        IRiskFlagInfo sSNFormatInvalid { get; set; }

        IRiskFlagInfo healthcareSanction { get; set; }

        IRiskFlagInfo phoneNumberInconsistentAddress { get; set; }

        IRiskFlagInfo arrest { get; set; }
    }
}
