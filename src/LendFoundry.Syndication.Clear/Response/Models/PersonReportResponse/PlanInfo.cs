using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PlanInfo : IPlanInfo
    {
        public PlanInfo(Proxy.Response.PersonReport.PlanInfo planInfo)
        {
            if(planInfo!=null)
            {
                plan = planInfo.Plan.Select(a => new Plan(a)).ToList<IPlan>();
                planDetails = planInfo.PlanDetails;
            }
        }
        public List<IPlan> plan { get; set; }
        public List<string> planDetails { get; set; }
    }
}
