﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LicenseSummary : ILicenseSummary
    {
        public LicenseSummary(Proxy.Response.PersonReport.LicenseSummary licenseSummary)
        {
            if (licenseSummary!=null)
            {
                professionalLicense = licenseSummary.ProfessionalLicense.Select(s=>new SummaryInfo(s)).ToList<ISummaryInfo>();
                recreationalLicense = licenseSummary.RecreationalLicense.Select(s=>new SummaryInfo(s)).ToList<ISummaryInfo>();
                healthcareLicense = licenseSummary.HealthcareLicense.Select(s=>new SummaryInfo(s)).ToList<ISummaryInfo>();
            }
        }
        [JsonConverter(typeof(InterfaceListConverter<ISummaryInfo, SummaryInfo>))]
        public List<ISummaryInfo> professionalLicense { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryInfo, SummaryInfo>))]
        public List<ISummaryInfo> recreationalLicense { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryInfo, SummaryInfo>))]
        public List<ISummaryInfo> healthcareLicense { get; set; }
    }
}
