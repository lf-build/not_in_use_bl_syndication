﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFilingTypeInfo
    {
         string actionDescription { get; set; }

         string fileTypeDescription { get; set; }

         string taxDescription { get; set; }

         string typeofTax { get; set; }

         string fileType { get; set; }

         List<string> typeOfAction { get; set; }
    }
}