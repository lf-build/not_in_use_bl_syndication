using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class IDInfo : IIDInfo
    {

        public IDInfo(Proxy.Response.PersonReport.IDInfo iDInfo)
        {
            if(iDInfo!=null)
            {
                driverLicenseInfo = iDInfo.DriverLicenseInfo.Select(a => new DriverLicenseInfo(a)).ToList<IDriverLicenseInfo>();
                fBICriminalID = iDInfo.FBICriminalID;
                undocumentedImmigrantInfo = iDInfo.UndocumentedImmigrantInfo.Select(a => new UndocumentedImmigrantInfo(a)).ToList<IUndocumentedImmigrantInfo>();
                sexOffenderID = iDInfo.SexOffenderID;
                stateIDInfo = iDInfo.StateIDInfo.Select(a => new IDDetails(a)).ToList<IIDDetails>();
                otherIDInfo = iDInfo.StateIDInfo.Select(a => new IDDetails(a)).ToList<IIDDetails>();
                otherIDInfo = iDInfo.StateIDInfo.Select(a => new IDDetails(a)).ToList<IIDDetails>();
                nationalID = iDInfo.NationalID;
                passportID = iDInfo.PassportID;
                inmateID = iDInfo.InmateID;
                personSSN = iDInfo.PersonSSN.Select(a => new SSNInfo(a)).ToList<ISSNInfo>();
                personProfile = iDInfo.PersonProfile.Select(a => new PersonProfile(a)).ToList<IPersonProfile>();



            }
        }
        [JsonConverter(typeof(InterfaceListConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public List<IDriverLicenseInfo> driverLicenseInfo{ get; set; }

        public List<string> fBICriminalID{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IUndocumentedImmigrantInfo, UndocumentedImmigrantInfo>))]
        public List<IUndocumentedImmigrantInfo> undocumentedImmigrantInfo{ get; set; }

        public string sexOffenderID{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIDDetails, IDDetails>))]
        public List<IIDDetails> stateIDInfo{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIDDetails, IDDetails>))]
        public List<IIDDetails> otherIDInfo{ get; set; }

        public List<string> nationalID{ get; set; }

        public List<string> passportID{ get; set; }

        public string inmateID{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISSNInfo, SSNInfo>))]
        public List<ISSNInfo> personSSN{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonProfile, PersonProfile>))]
        public List<IPersonProfile> personProfile{ get; set; }
    }
}
