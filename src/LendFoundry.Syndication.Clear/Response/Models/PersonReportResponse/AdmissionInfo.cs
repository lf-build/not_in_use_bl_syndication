namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AdmissionInfo: IAdmissionInfo
    {
        public AdmissionInfo(Proxy.Response.PersonReport.AdmissionInfo admissionInfo)
        {
            if (admissionInfo!=null)
            {
                admissionDate = admissionInfo.AdmissionDate;
                institutionLocation = admissionInfo.InstitutionLocation;
                typeOfAdmission = admissionInfo.TypeOfAdmission;
            }
        }
        public string admissionDate{ get; set; }

        public string institutionLocation{ get; set; }

        public string typeOfAdmission{ get; set; }
    }
}
