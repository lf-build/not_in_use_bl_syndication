namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IOffenderPriorInfo
    {
         IInmateReleaseInfo inmateReleaseInfo { get; set; }

         IPriorChargesInfo priorChargesInfo { get; set; }

         ISentencingInfo sentencingInfo { get; set; }

         string priorCaseNumber { get; set; }

         string priorCrimeDate { get; set; }

         string priorCrimeCategory { get; set; }

         string priorCrimeClass { get; set; }

         string priorDisposition { get; set; }

         string priorDispositionDate { get; set; }
    }
}
