namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IInmateDisciplineInfo
    {

         string decisionOnAppeal { get; set; }

         string infractionDate { get; set; }

         string infractionDescription { get; set; }

         string infractionHearing { get; set; }

         string infractionPlea { get; set; }

         string infractionTimeLost { get; set; }

         string infractionVerdict { get; set; }

         string infractionViolationStatus { get; set; }
    }
}
