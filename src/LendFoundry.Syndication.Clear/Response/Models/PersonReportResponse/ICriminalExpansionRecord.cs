﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICriminalExpansionRecord
    {
        IDefendantInfo defendantInfo { get; set; }
        List<IOffenderInfo> offenderInfo { get; set; }
        IAdditionalInfo additionalInfo { get; set; }
        string publicationDate { get; set; }
        string sourceName { get; set; }
        string sourceCounty { get; set; }
        string sourceState { get; set; }
        string typeOfCriminal { get; set; }
        string source { get; set; }
        List<IPhotoImages1> photoImages { get; set; }
    }
}
