namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class OfficerInfo : IOfficerInfo
    {
        public OfficerInfo(Proxy.Response.PersonReport.OfficerInfo officerInfo)
        {
            if (officerInfo != null)
            {
                officerBadge = officerInfo.OfficerBadge;
                officerName = officerInfo.OfficerName;
            }
        }

        public string officerBadge { get; set; }
        public string officerName { get; set; }
    }
}
