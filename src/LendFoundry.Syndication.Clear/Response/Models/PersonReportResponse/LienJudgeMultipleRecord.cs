using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LienJudgeMultipleRecord : ILienJudgeMultipleRecord
    {
        public LienJudgeMultipleRecord(Proxy.Response.PersonReport.LienJudgeMultipleRecord lienJudgeMultipleRecord)
        {
            if (lienJudgeMultipleRecord != null)
            {
                creditor             = lienJudgeMultipleRecord.Creditor.Select(a=>new Creditor(a)).ToList<ICreditor>();
                debtor               = lienJudgeMultipleRecord.Debtor.Select(a => new Debtor(a)).ToList<IDebtor>();
                subjudgments         = lienJudgeMultipleRecord.Subjudgments.Select(a => new Subjudgments(a)).ToList<ISubjudgments>();
                judgmentInfo         = lienJudgeMultipleRecord.JudgmentInfo.Select(a => new JudgmentInfo(a)).ToList<IJudgmentInfo>();
                lienInfo             = lienJudgeMultipleRecord.LienInfo.Select(a => new LienInfo(a)).ToList<ILienInfo>();
                commentInfo          = lienJudgeMultipleRecord.CommentInfo.Select(a => new CommentInfo(a)).ToList<ICommentInfo>();
                numberofSubjudgments = lienJudgeMultipleRecord.NumberofSubjudgments;
                lienJudgeFilingInfo  = lienJudgeMultipleRecord.LienJudgeFilingInfo.Select(a => new FilingInfo1(a)).ToList<IFilingInfo1>();
                source               = lienJudgeMultipleRecord.Source;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ICreditor, Creditor>))]
        public List<ICreditor> creditor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubjudgments, Subjudgments>))]
        public List<ISubjudgments> subjudgments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IJudgmentInfo, JudgmentInfo>))]
        public List<IJudgmentInfo> judgmentInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILienInfo, LienInfo>))]
        public List<ILienInfo> lienInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICommentInfo, CommentInfo>))]
        public List<ICommentInfo> commentInfo { get; set; }
        public string numberofSubjudgments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFilingInfo1, FilingInfo1>))]
        public List<IFilingInfo1> lienJudgeFilingInfo { get; set; }
        public string source { get; set; }
    }
}
