﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SubjectRecord : ISubjectRecord
    {
        public SubjectRecord(Proxy.Response.PersonReport.SubjectRecord subjectRecord)
        {
            if (subjectRecord != null)
            {
                personInfo = new PersonInfo(subjectRecord.PersonInfo);
                deathSummary = new DeathSummary(subjectRecord.DeathSummary);
                driverLicenseInfo = subjectRecord.DriverLicenseInfo.Select(d => new DriverLicenseInfo(d)).ToList<IDriverLicenseInfo>();
                marriageDivorceSummary = new MarriageDivorceSummary(subjectRecord.MarriageDivorceSummary);
                licenseSummary = new LicenseSummary(subjectRecord.LicenseSummary);
                workAffiliations = subjectRecord.WorkAffiliations.Select(s => new WorkAffiliations(s)).ToList<IWorkAffiliations>();
                photoImages = new PhotoImages(subjectRecord.PhotoImages);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPersonInfo, PersonInfo>))]
        public IPersonInfo personInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonInfo, PersonInfo>))]
        public List<IPersonInfo> personAKAInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDeathSummary, DeathSummary>))]
        public IDeathSummary deathSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public List<IDriverLicenseInfo> driverLicenseInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IMarriageDivorceSummary, MarriageDivorceSummary>))]
        public IMarriageDivorceSummary marriageDivorceSummary { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILicenseSummary, LicenseSummary>))]
        public ILicenseSummary licenseSummary { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IWorkAffiliations, WorkAffiliations>))]
        public List<IWorkAffiliations> workAffiliations { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhotoImages, PhotoImages>))]
        public IPhotoImages photoImages { get; set; }


    }
}
