using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class RelativeInfo: IRelativeInfo
    {
        public RelativeInfo(Proxy.Response.PersonReport.RelativeInfo relativeInfo)
        {
            if (relativeInfo!=null)
            {
                address = new Address(relativeInfo.Address);
                personName = new PersonName(relativeInfo.PersonName);
                phone = new PhoneInfo(relativeInfo.Phone);
                typeOfRelative = relativeInfo.TypeOfRelative;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phone { get; set; }

        public string typeOfRelative { get; set; }
    }
}
