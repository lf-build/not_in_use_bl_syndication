namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IBookingInfo
    {
         string bookingDate { get; set; }

         string bookingLocation { get; set; }

         string bookingLocationText { get; set; }

         string bookingNumber { get; set; }

         string bookingTime { get; set; }

         IAgencyInfo arrestAgencyInfo { get; set; }

         string arrestDateTime { get; set; }

         string offenseDateTime { get; set; }

         IAgencyInfo holdingAgencyInfo { get; set; }

         string paroleClass { get; set; }

         string releaseDateTime { get; set; }

         string releasedFromSupervision { get; set; }

         string releaseReason { get; set; }

         string sentenceExpirationDate { get; set; }

         string scheduledReleaseDate { get; set; }

         string status { get; set; }
    }
}
