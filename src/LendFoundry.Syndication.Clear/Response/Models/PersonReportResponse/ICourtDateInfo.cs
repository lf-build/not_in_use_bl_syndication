namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICourtDateInfo
    {
         string nextCourtDate { get; set; }

         string nextCourtTime { get; set; }
    }
}
