﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{

    public class DefendantInfo : IDefendantInfo
    {
        public DefendantInfo(Proxy.Response.PersonReport.DefendantInfo defendantInfo)
        {
            if (defendantInfo != null)
            {
                address = new Address(defendantInfo.Address);
                admissionInfo = new AdmissionInfo(defendantInfo.AdmissionInfo);
                historicalAddress = defendantInfo.HistoricalAddress.Select(a => new Address(a)).ToList<IAddress>();
                iDInfo = new IDInfo(defendantInfo.IDInfo);
                miscellaneousInfo = defendantInfo.MiscellaneousInfo;
                personInfo = new PersonInfo3(defendantInfo.PersonInfo);
                phone = new PhoneInfo(defendantInfo.Phone);
                relativeInfo = defendantInfo.RelativeInfo.Select(a => new RelativeInfo(a)).ToList<IRelativeInfo>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address{ get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAdmissionInfo, AdmissionInfo>))]
        public IAdmissionInfo admissionInfo{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> historicalAddress{ get; set; }

        [JsonConverter(typeof(InterfaceConverter<IIDInfo, IDInfo>))]
        public IIDInfo iDInfo{ get; set; }

        public List<string> miscellaneousInfo{ get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonInfo3, PersonInfo3>))]
        public IPersonInfo3 personInfo{ get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phone{ get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRelativeInfo, RelativeInfo>))]
        public List<IRelativeInfo> relativeInfo{ get; set; }
    }
}
