namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPhotoImages1
    {
         string binaryImage { get; set; }

         string contentType { get; set; }
    }
}
