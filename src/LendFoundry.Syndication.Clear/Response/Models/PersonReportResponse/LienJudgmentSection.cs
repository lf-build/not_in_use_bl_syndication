﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LienJudgmentSection: ILienJudgmentSection
    {
        public LienJudgmentSection(Proxy.Response.PersonReport.LienJudgmentSection lienJudgmentSection)
        {
            if (lienJudgmentSection != null)
            {
                lienJudgeRecord = lienJudgmentSection.LienJudgeRecord.Select(s => new LienJudgeRecord(s)).ToList<ILienJudgeRecord>();
                lienJudgeNYRecord = lienJudgmentSection.LienJudgeNYRecord.Select(s => new LienJudgeNYRecord(s)).ToList<ILienJudgeNYRecord>();
                lienJudgeMultipleRecord = lienJudgmentSection.LienJudgeMultipleRecord.Select(s => new LienJudgeMultipleRecord(s)).ToList<ILienJudgeMultipleRecord>();

            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ILienJudgeRecord, LienJudgeRecord>))]
        public List<ILienJudgeRecord> lienJudgeRecord { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILienJudgeNYRecord, LienJudgeNYRecord>))]
        public List<ILienJudgeNYRecord> lienJudgeNYRecord { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ILienJudgeMultipleRecord, LienJudgeMultipleRecord>))]
        public List<ILienJudgeMultipleRecord> lienJudgeMultipleRecord { get; set; }
    }
}
