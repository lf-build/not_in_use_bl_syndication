﻿

using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Creditor: ICreditor
    {
        public Creditor(Proxy.Response.PersonReport.Creditor creditor)
        {
            if (creditor != null)
            {
                partyInfo                = new PartyInfo(creditor.PartyInfo);
                creditHolderLevelDesc    = creditor.CreditHolderLevelDesc;
                creditHolderSerialNumber = creditor.CreditHolderSerialNumber;
                creditHolder             = creditor.CreditHolder;
                creditHolderLevel        = creditor.CreditHolderLevel;
                dUNSNumber               = creditor.DUNSNumber;
                foreignRegion            = creditor.ForeignRegion;
                claimDescription         = creditor.ClaimDescription;
                otherClaim               = creditor.OtherClaim;
                securedClaim             = creditor.SecuredClaim;
                unsecuredClaim           = creditor.UnsecuredClaim;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IPartyInfo, PartyInfo>))]
        public IPartyInfo partyInfo { get; set; }

        public string creditHolderLevelDesc { get; set; }

        public string creditHolderSerialNumber { get; set; }

        public string creditHolder { get; set; }

        public string creditHolderLevel { get; set; }

        public string dUNSNumber { get; set; }

        public string foreignRegion { get; set; }

        public string claimDescription { get; set; }

        public string otherClaim { get; set; }

        public string securedClaim { get; set; }

        public string unsecuredClaim { get; set; }
    }
}
