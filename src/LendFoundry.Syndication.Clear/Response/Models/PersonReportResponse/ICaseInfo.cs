using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICaseInfo
    {
         string districtCourtDocketNumber { get; set; }

         string docketNumber { get; set; }

         string otherCaseNumber { get; set; }

         List<string> caseNumber { get; set; }

         ICaseFiling caseFiling { get; set; }

         string caseGeneralCategory { get; set; }

         string caseTitle { get; set; }

         string caseCategory { get; set; }

         string caseInformation { get; set; }

         string caseStatus { get; set; }

         string caseStatusDate { get; set; }

         List<string> caseComments { get; set; }
    }
}
