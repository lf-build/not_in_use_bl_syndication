using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class OffenderInfo : IOffenderInfo
    {
        public OffenderInfo(Proxy.Response.PersonReport.OffenderInfo offenderInfo)
        {
            if (offenderInfo != null)
            {
                amendedChargesDate = offenderInfo.AmendedCharges;
                amendedCharges = offenderInfo.AmendedChargesDate;
                amendedOffense = offenderInfo.AmendedOffense;
                amendedOffenseDate = offenderInfo.AmendedOffenseDate;
                appealStatus = offenderInfo.AppealStatus;
                appealDate = offenderInfo.AppealDate;
                arraignmentDate = offenderInfo.ArraignmentDate;
                arrestAgency = offenderInfo.ArrestAgency;
                arrestDate = offenderInfo.ArrestDate;
                arrestWarrantFlag = offenderInfo.ArrestWarrantFlag;
                bailInfo = new BailInfo(offenderInfo.BailInfo);
                bookingInfo = new BookingInfo(offenderInfo.BookingInfo);
                caseDispositionFinalDate = offenderInfo.CaseDispositionFinalDate;
                caseDispositionDecisionCategoryText = offenderInfo.CaseDispositionDecisionCategoryText;
                caseInfo = new CaseInfo(offenderInfo.CaseInfo);
                citationNumber = offenderInfo.CitationNumber;
                crimeDate = offenderInfo.CrimeDate;
                crimeIndicators = new CrimeIndicators(offenderInfo.CrimeIndicators);
                crimeSeverity = offenderInfo.CrimeSeverity;
                criminalOffense = offenderInfo.CriminalOffense;
                convictDate = offenderInfo.ConvictDate;
                courtCounty = offenderInfo.CourtCounty;
                courtName = offenderInfo.CourtName;
                custodyDate = offenderInfo.CustodyDate;
                custodyLocation = offenderInfo.CustodyLocation;
                dispositionCharges = offenderInfo.DispositionCharges;
                dispositionChargesDate = offenderInfo.DispositionChargesDate;
                documentFiledDate = offenderInfo.DocumentFiledDate;
                fineAmountInfo = new FineAmountInfo(offenderInfo.FineAmountInfo);
                initialCharges = offenderInfo.InitialCharges;
                initialChargesDate = offenderInfo.InitialChargesDate;
                inmateReleaseInfo = new InmateReleaseInfo(offenderInfo.InmateReleaseInfo);
                judgeName = offenderInfo.JudgeName;
                paroleInfo = new ParoleInfo(offenderInfo.ParoleInfo);
                pleaInfo = new PleaInfo(offenderInfo.PleaInfo);
                probationInfo = new ProbationInfo(offenderInfo.ProbationInfo);
                offenderPriorInfo = new OffenderPriorInfo(offenderInfo.OffenderPriorInfo);
                offenderSequenceNumber = offenderInfo.OffenderSequenceNumber;
                offenseLocation = offenderInfo.OffenseLocation;
                offenseStatus = offenderInfo.OffenseStatus;
                offenseStatusDate = offenderInfo.OffenseStatusDate;
                priorOffenseIndicator = offenderInfo.PriorOffenseIndicator;
                sentencingInfo = new SentencingInfo(offenderInfo.SentencingInfo);
                statuteViolated = offenderInfo.StatuteViolated;
                typeOfTrial = offenderInfo.TypeOfTrial;
                warrantDate = offenderInfo.WarrantDate;
                warrrantDescription = offenderInfo.WarrrantDescription;
                warrantIssueDate = offenderInfo.WarrantIssueDate;
                verdict = offenderInfo.Verdict;
                additionalInfo = offenderInfo.AdditionalInfo.Select(a => new AdditionalInfo(a)).ToList<IAdditionalInfo>();
            }
                                                        
        }
        public string amendedCharges { get; set; }
        public string amendedChargesDate { get; set; }
        public string amendedOffense { get; set; }
        public string amendedOffenseDate { get; set; }
        public string appealStatus { get; set; }
        public string appealDate { get; set; }
        public string arraignmentDate { get; set; }
        public string arrestAgency { get; set; }
        public string arrestDate { get; set; }
        public string arrestWarrantFlag { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBailInfo, BailInfo>))]
        public IBailInfo bailInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBookingInfo, BookingInfo>))]
        public IBookingInfo bookingInfo { get; set; }
        public string caseDispositionFinalDate { get; set; }
        public string caseDispositionDecisionCategoryText { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICaseInfo, CaseInfo>))]
        public ICaseInfo caseInfo { get; set; }
        public string citationNumber { get; set; }
        public string crimeDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICrimeIndicators, CrimeIndicators>))]
        public ICrimeIndicators crimeIndicators { get; set; }
        public string crimeSeverity { get; set; }
        public string criminalOffense { get; set; }
        public string convictDate { get; set; }
        public string courtCounty { get; set; }
        public string courtName { get; set; }
        public string custodyDate { get; set; }
        public string custodyLocation { get; set; }
        public string dispositionCharges { get; set; }
        public string dispositionChargesDate { get; set; }
        public string documentFiledDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFineAmountInfo, FineAmountInfo>))]
        public IFineAmountInfo fineAmountInfo { get; set; }
        public string initialCharges { get; set; }
        public string initialChargesDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IInmateReleaseInfo, InmateReleaseInfo>))]
        public IInmateReleaseInfo inmateReleaseInfo { get; set; }
        public List<string> judgeName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IParoleInfo, ParoleInfo>))]
        public IParoleInfo paroleInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPleaInfo, PleaInfo>))]
        public IPleaInfo pleaInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IProbationInfo, ProbationInfo>))]
        public IProbationInfo probationInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IOffenderPriorInfo, OffenderPriorInfo>))]
        public IOffenderPriorInfo offenderPriorInfo { get; set; }
        public string offenderSequenceNumber { get; set; }
        public string offenseLocation { get; set; }
        public string offenseStatus { get; set; }
        public string offenseStatusDate { get; set; }
        public string priorOffenseIndicator { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISentencingInfo, SentencingInfo>))]
        public ISentencingInfo sentencingInfo { get; set; }
        public string statuteViolated { get; set; }
        public string typeOfTrial { get; set; }
        public string warrantDate { get; set; }
        public string warrrantDescription { get; set; }
        public string warrantIssueDate { get; set; }
        public string verdict { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAdditionalInfo, AdditionalInfo>))]
        public List<IAdditionalInfo> additionalInfo { get; set; }
    }
}
