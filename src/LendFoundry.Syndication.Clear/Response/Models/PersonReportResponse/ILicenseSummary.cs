﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ILicenseSummary
    {
        List<ISummaryInfo> healthcareLicense { get; set; }
        List<ISummaryInfo> professionalLicense { get; set; }
        List<ISummaryInfo> recreationalLicense { get; set; }
    }
}