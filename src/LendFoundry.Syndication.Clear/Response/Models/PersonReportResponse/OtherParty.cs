using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class OtherParty : IOtherParty
    {
        public OtherParty(Proxy.Response.PersonReport.OtherParty otherParty)
        {
            if(otherParty!=null)
            {
                partyInfo=new  PartyInfo(otherParty.PartyInfo);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPartyInfo, PartyInfo>))]
        public IPartyInfo partyInfo { get; set; }
    }
}
