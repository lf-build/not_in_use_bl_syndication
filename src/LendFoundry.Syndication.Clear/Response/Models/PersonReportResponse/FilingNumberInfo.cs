﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FilingNumberInfo : IFilingNumberInfo
    {
        public FilingNumberInfo(Proxy.Response.PersonReport.FilingNumberInfo filingNumberInfo)
        {
            if (filingNumberInfo != null)
            {
                filingNumber = filingNumberInfo.FilingNumber;
                pageNumber   = filingNumberInfo.PageNumber;
                volumeNumber = filingNumberInfo.VolumeNumber;
            }
        }

        public List<string> filingNumber {get; set;}

        public List<string> pageNumber {get; set;}

        public List<string> volumeNumber {get; set;}
    }
}
