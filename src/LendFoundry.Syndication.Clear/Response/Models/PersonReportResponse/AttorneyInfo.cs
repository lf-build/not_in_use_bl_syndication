﻿

using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AttorneyInfo : IAttorneyInfo
    {
      

        public AttorneyInfo(Proxy.Response.PersonReport.AttorneyInfo attorneyInfo)
        {
            if (attorneyInfo != null)
            {
                attorneyBarNumber          = attorneyInfo.AttorneyBarNumber;
                attorneyCOR                = attorneyInfo.AttorneyCOR;
                attorneyEmail              = attorneyInfo.AttorneyEmail;
                attornyForeignRegistration = attorneyInfo.AttornyForeignRegistration;
                attorneyFTS                = attorneyInfo.AttorneyFTS;
                attorneyID                 = attorneyInfo.AttorneyID;
                attorneyLicense            = attorneyInfo.AttorneyLicense;
                attorneyName               = attorneyInfo.AttorneyName;
                attorneyPhone              = attorneyInfo.AttorneyPhone;
                attorneyFax                = attorneyInfo.AttorneyFax;
                attorneyAddress = attorneyInfo.AttorneyAddress.Select(a => new Address(a)).ToList<IAddress>();
                attorneyRepresents         = attorneyInfo.AttorneyRepresents;
                attorneyRole               = attorneyInfo.AttorneyRole;
                attorneyStatus             = attorneyInfo.AttorneyStatus;
                attorneyTerminatedDate     = attorneyInfo.AttorneyTerminatedDate;
                typeofAttorney             = attorneyInfo.TypeofAttorney;
                firmName                   = attorneyInfo.FirmName;
                firmStatus                 = attorneyInfo.FirmStatus;
            }
        }

        public string attorneyBarNumber { get; set; }

        public string attorneyCOR { get; set; }

        public string attorneyEmail { get; set; }

        public string attornyForeignRegistration { get; set; }

        public string attorneyFTS { get; set; }

        public string attorneyID { get; set; }

        public string attorneyLicense { get; set; }

        public List<string> attorneyName { get; set; }

        public string attorneyPhone { get; set; }

        public string attorneyFax { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> attorneyAddress { get; set; }

        public string attorneyRepresents { get; set; }

        public string attorneyRole { get; set; }

        public string attorneyStatus { get; set; }

        public string attorneyTerminatedDate { get; set; }

        public string typeofAttorney { get; set; }

        public string firmName { get; set; }

        public string firmStatus { get; set; }
    }
}
