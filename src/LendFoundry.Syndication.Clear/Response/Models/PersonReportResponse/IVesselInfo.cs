using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IVesselInfo
    {
         string typeOfVessel { get; set; }

         string callSign { get; set; }

         string country { get; set; }

         string grossTonnage { get; set; }

         List<string> owner { get; set; }

         string tonnage { get; set; }
    }
}
