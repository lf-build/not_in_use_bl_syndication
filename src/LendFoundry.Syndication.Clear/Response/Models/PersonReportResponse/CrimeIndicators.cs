namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CrimeIndicators : ICrimeIndicators
    {
        public CrimeIndicators(Proxy.Response.PersonReport.CrimeIndicators crimeIndicators)
        {
            if(crimeIndicators!=null)
            {
                attemptedCrime = crimeIndicators.AttemptedCrime;
                drugCrime = crimeIndicators.DrugCrime;
                guiltyButMentallyIll = crimeIndicators.GuiltyButMentallyIll;
                hateCrime = crimeIndicators.HateCrime;
                inchoateCrime = crimeIndicators.InchoateCrime;
                otherCrimeInfo = crimeIndicators.OtherCrimeInfo;
                propertyCrime = crimeIndicators.PropertyCrime;
                propertySeized = crimeIndicators.PropertySeized;
                riskLevel = crimeIndicators.RiskLevel;
                secondFelony = crimeIndicators.SecondFelony;
                sexCrime = crimeIndicators.SexCrime;
                victimAgeDesc = crimeIndicators.VictimAgeDesc;
                victimGender = crimeIndicators.VictimGender;
                victimIsaMinor = crimeIndicators.VictimIsaMinor;
                weaponCrime = crimeIndicators.WeaponCrime;
            }
        }
        public string attemptedCrime { get; set; }
        public string drugCrime { get; set; }
        public string guiltyButMentallyIll { get; set; }
        public string hateCrime { get; set; }
        public string inchoateCrime { get; set; }
        public string otherCrimeInfo { get; set; }
        public string propertyCrime { get; set; }
        public string propertySeized { get; set; }
        public string riskLevel { get; set; }
        public string secondFelony { get; set; }
        public string sexCrime { get; set; }
        public string victimAgeDesc { get; set; }
        public string victimGender { get; set; }
        public string victimIsaMinor { get; set; }
        public string weaponCrime { get; set; }
    }
}
