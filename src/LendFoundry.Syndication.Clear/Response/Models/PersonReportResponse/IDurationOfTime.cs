namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDurationOfTime
    {
         string lengthAndUnits  { get; set; }

         string numberOfHours  { get; set; }

         string numberOfDays  { get; set; }

         string numberOfMonths  { get; set; }

         string numberOfYears  { get; set; }
    }
}
