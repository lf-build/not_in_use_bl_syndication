using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class JudgmentInfo : IJudgmentInfo
    {
        public JudgmentInfo(Proxy.Response.PersonReport.JudgmentInfo judgmentInfo)
        {
            if (judgmentInfo != null)
            {
                awardAmount              = judgmentInfo.AwardAmount;
                judgementPrincipalAmount = judgmentInfo.JudgementPrincipalAmount;
                paidOnCreditAmount       = judgmentInfo.PaidOnCreditAmount;
                judgmentTotalAmount      = judgmentInfo.JudgmentTotalAmount;
                obligationInfo           = judgmentInfo.ObligationInfo.Select(a=>new ObligationInfo(a)).ToList<IObligationInfo>();
                statusInfo               = new StatusInfo(judgmentInfo.StatusInfo);
                subjudgmentInfo          = new SubjudgmentInfo(judgmentInfo.SubjudgmentInfo);
            }
        }

        public string awardAmount { get; set; }
        public string judgementPrincipalAmount { get; set; }
        public string paidOnCreditAmount { get; set; }
        public string judgmentTotalAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IObligationInfo, ObligationInfo>))]
        public List<IObligationInfo> obligationInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IStatusInfo, StatusInfo>))]
        public IStatusInfo statusInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISubjudgmentInfo, SubjudgmentInfo>))]
        public ISubjudgmentInfo subjudgmentInfo { get; set; }
    }
}
