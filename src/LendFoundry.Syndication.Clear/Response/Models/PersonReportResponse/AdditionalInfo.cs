using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AdditionalInfo : IAdditionalInfo
    {
       

        public AdditionalInfo(Proxy.Response.PersonReport.AdditionalInfo additionalInfo)
        {
            if (additionalInfo != null)
            {
                defenseAttorneyList = additionalInfo.DefenseAttorneyList.Select(a => new DefenseAttorneyList(a)).ToList<IDefenseAttorneyList>();
                judgeName = judgeName;
                officerInfo = additionalInfo.OfficerInfo.Select(a=> new OfficerInfo(a)).ToList<IOfficerInfo>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IDefenseAttorneyList, DefenseAttorneyList>))]
        public List<IDefenseAttorneyList> defenseAttorneyList { get; set; }
        public List<string> judgeName { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOfficerInfo, OfficerInfo>))]
        public List<IOfficerInfo> officerInfo { get; set; }
    }
}
