using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class BankruptcySection : IBankruptcySection
    {
        public BankruptcySection(Proxy.Response.PersonReport.BankruptcySection bankruptcySection)
        {
            if (bankruptcySection != null)
            {
                fedDocketBankruptcyRecord = bankruptcySection.FedDocketBankruptcyRecord.Select(a=>new FedDocketBankruptcyRecord(a)).ToList<IFedDocketBankruptcyRecord>();
                publicBankruptcyRecord    = bankruptcySection.PublicBankruptcyRecord.Select(a => new PublicBankruptcyRecord(a)).ToList<IPublicBankruptcyRecord>();
                superiorBankruptcyRecord  = bankruptcySection.SuperiorBankruptcyRecord.Select(a => new SuperiorBankruptcyRecord(a)).ToList<ISuperiorBankruptcyRecord>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IFedDocketBankruptcyRecord, FedDocketBankruptcyRecord>))]
        public List<IFedDocketBankruptcyRecord> fedDocketBankruptcyRecord { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPublicBankruptcyRecord, PublicBankruptcyRecord>))]
        public List<IPublicBankruptcyRecord> publicBankruptcyRecord { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISuperiorBankruptcyRecord, SuperiorBankruptcyRecord>))]
        public List<ISuperiorBankruptcyRecord> superiorBankruptcyRecord { get; set; }
    }
}
