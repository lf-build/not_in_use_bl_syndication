using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class RiskFlagsWithDocguids : IRiskFlagsWithDocguids
    {
        public RiskFlagsWithDocguids(Proxy.Response.PersonReport.RiskFlagsWithDocguids riskFlagsWithDocguids)
        {
            if (riskFlagsWithDocguids != null)
            {
                assocWithOFACGlobalPEP                     = new RiskFlagInfo(riskFlagsWithDocguids.AssocWithOFACGlobalPEP);
                oFAC                                       = new RiskFlagInfo(riskFlagsWithDocguids.OFAC);
                worldCheck                                 = new RiskFlagInfo(riskFlagsWithDocguids.WorldCheck);
                globalSanctions                            = new RiskFlagInfo(riskFlagsWithDocguids.GlobalSanctions);
                residentialUsedAsBusiness                  = new RiskFlagInfo(riskFlagsWithDocguids.ResidentialUsedAsBusiness);
                prisonAddress                              = new RiskFlagInfo(riskFlagsWithDocguids.PrisonAddress);
                pOBoxAddress                               = new RiskFlagInfo(riskFlagsWithDocguids.POBoxAddress);
                bankruptcy                                 = new RiskFlagInfo(riskFlagsWithDocguids.Bankruptcy);
                assocRelativeWithResidentialUsedAsBusiness = new RiskFlagInfo(riskFlagsWithDocguids.AssocRelativeWithResidentialUsedAsBusiness);
                assocRelativeWithPrisonAddress             = new RiskFlagInfo(riskFlagsWithDocguids.AssocRelativeWithPrisonAddress);
                assocRelativeWithPOBoxAddress              = new RiskFlagInfo(riskFlagsWithDocguids.AssocRelativeWithPOBoxAddress);
                criminal                                   = new RiskFlagInfo(riskFlagsWithDocguids.Criminal);
                multipleSSN                                = new RiskFlagInfo(riskFlagsWithDocguids.MultipleSSN);
                sSNMultipleIndividuals                     = new RiskFlagInfo(riskFlagsWithDocguids.SSNMultipleIndividuals);
                recordedAsDeceased                         = new RiskFlagInfo(riskFlagsWithDocguids.RecordedAsDeceased);
                ageYoungerThanSSN                          = new RiskFlagInfo(riskFlagsWithDocguids.AgeYoungerThanSSN);
                addressReportedLessNinetyDays              = new RiskFlagInfo(riskFlagsWithDocguids.AddressReportedLessNinetyDays);
                sSNFormatInvalid                           = new RiskFlagInfo(riskFlagsWithDocguids.SSNFormatInvalid);
                healthcareSanction                         = new RiskFlagInfo(riskFlagsWithDocguids.HealthcareSanction);
                phoneNumberInconsistentAddress             = new RiskFlagInfo(riskFlagsWithDocguids.PhoneNumberInconsistentAddress);
                arrest                                     = new RiskFlagInfo(riskFlagsWithDocguids.Arrest);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo assocWithOFACGlobalPEP { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo oFAC { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo worldCheck { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo globalSanctions { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo residentialUsedAsBusiness { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo prisonAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo pOBoxAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo bankruptcy { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo assocRelativeWithResidentialUsedAsBusiness { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo assocRelativeWithPrisonAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo assocRelativeWithPOBoxAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo criminal { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo multipleSSN { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo sSNMultipleIndividuals { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo recordedAsDeceased { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo ageYoungerThanSSN { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo addressReportedLessNinetyDays { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo sSNFormatInvalid { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo healthcareSanction { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo phoneNumberInconsistentAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagInfo, RiskFlagInfo>))]
        public IRiskFlagInfo arrest { get; set; }
    }
}
