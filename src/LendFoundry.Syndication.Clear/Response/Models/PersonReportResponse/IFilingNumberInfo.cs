﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFilingNumberInfo
    {
         List<string> filingNumber {get; set;}

         List<string> pageNumber {get; set;}

         List<string> volumeNumber {get; set;}
    }
}
