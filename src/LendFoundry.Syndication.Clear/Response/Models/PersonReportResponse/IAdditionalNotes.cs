namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAdditionalNotes
    {
         string dataLabel { get; set; }

         string dataValue { get; set; }
    }
}
