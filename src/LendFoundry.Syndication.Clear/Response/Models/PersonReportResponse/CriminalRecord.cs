﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{

    public class CriminalRecord : ICriminalRecord
    {
        public CriminalRecord(Proxy.Response.PersonReport.CriminalRecord criminalRecord)
        {
            if (criminalRecord != null)
            {
               additionalNotes            = criminalRecord.AdditionalNotes.Select(a=>new AdditionalNotes(a)).ToList<IAdditionalNotes>();
               address                    = criminalRecord.Address.Select(a => new Address(a)).ToList<IAddress>(); 
               arrestInfo                 = criminalRecord.ArrestInfo.Select(a => new ArrestInfo(a)).ToList<IArrestInfo>();
               bailInfo                   = criminalRecord.BailInfo.Select(a => new BailInfo(a)).ToList<IBailInfo>();
               courtInfo                  = criminalRecord.CourtInfo.Select(a => new CourtInfo(a)).ToList<ICourtInfo>();
               criminalCharges            = criminalRecord.CriminalCharges.Select(a => new CriminalCharges(a)).ToList<ICriminalCharges>();
               iDInfo                     = new IDInfo(criminalRecord.IDInfo);
               inmateDisciplineInfo       = criminalRecord.InmateDisciplineInfo.Select(a => new InmateDisciplineInfo(a)).ToList<IInmateDisciplineInfo>();
               inmateReleaseInfo          = criminalRecord.InmateReleaseInfo.Select(a => new InmateReleaseInfo(a)).ToList<IInmateReleaseInfo>();
               institutionInfo            = criminalRecord.InstitutionInfo.Select(a => new InstitutionInfo(a)).ToList<IInstitutionInfo>();
               officeForeignAssetsControl = new OfficeForeignAssetsControl(criminalRecord.OfficeForeignAssetsControl);
               paroleInfo                 = criminalRecord.ParoleInfo.Select(a => new ParoleInfo(a)).ToList<IParoleInfo>();
               personInfo                 = criminalRecord.PersonInfo.Select(a => new PersonInfo3(a)).ToList<IPersonInfo3>();
               pleaInfo                   = criminalRecord.PleaInfo.Select(a => new PleaInfo(a)).ToList<IPleaInfo>();
               priorChargesInfo           = criminalRecord.PriorChargesInfo.Select(a => new PriorChargesInfo(a)).ToList<IPriorChargesInfo>();
               probationInfo              = criminalRecord.ProbationInfo.Select(a => new ProbationInfo(a)).ToList<IProbationInfo>();
               sentencingInfo             = criminalRecord.SentencingInfo.Select(a => new SentencingInfo(a)).ToList<ISentencingInfo>();
               sourceState                = criminalRecord.SourceState;
               typeOfCriminal             = criminalRecord.TypeOfCriminal;
               typeOfRecord               = criminalRecord.TypeOfRecord;
               vehicleInfo                = criminalRecord.VehicleInfo.Select(a => new VehicleInfo(a)).ToList<IVehicleInfo>();
               source                     = criminalRecord.Source;
               photoImages                = criminalRecord.PhotoImages.Select(a => new PhotoImages1(a)).ToList<IPhotoImages1>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IAdditionalNotes, AdditionalNotes>))]
        public List<IAdditionalNotes> additionalNotes { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IArrestInfo, ArrestInfo>))]
        public List<IArrestInfo> arrestInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBailInfo, BailInfo>))]
        public List<IBailInfo> bailInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICourtInfo, CourtInfo>))]
        public List<ICourtInfo> courtInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICriminalCharges, CriminalCharges>))]
        public List<ICriminalCharges> criminalCharges { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IIDInfo,IIDInfo>))]
        public IIDInfo iDInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInmateDisciplineInfo, InmateDisciplineInfo>))]
        public List<IInmateDisciplineInfo> inmateDisciplineInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInmateReleaseInfo, InmateReleaseInfo>))]
        public List<IInmateReleaseInfo> inmateReleaseInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInstitutionInfo, InstitutionInfo>))]
        public List<IInstitutionInfo> institutionInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IOfficeForeignAssetsControl, OfficeForeignAssetsControl>))]
        public IOfficeForeignAssetsControl officeForeignAssetsControl { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IParoleInfo, ParoleInfo>))]
        public List<IParoleInfo> paroleInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonInfo3, PersonInfo3>))]
        public List<IPersonInfo3> personInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPleaInfo, PleaInfo>))]
        public List<IPleaInfo> pleaInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPriorChargesInfo, PriorChargesInfo>))]
        public List<IPriorChargesInfo> priorChargesInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IProbationInfo, ProbationInfo>))]
        public List<IProbationInfo> probationInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISentencingInfo, SentencingInfo>))]
        public List<ISentencingInfo> sentencingInfo { get; set; }

        public string sourceState { get; set; }

        public string typeOfCriminal { get; set; }

        public string typeOfRecord { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IVehicleInfo, VehicleInfo>))]
        public List<IVehicleInfo> vehicleInfo { get; set; }

        public string source { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhotoImages1, PhotoImages1>))]
        public List<IPhotoImages1> photoImages { get; set; }
    }
}
