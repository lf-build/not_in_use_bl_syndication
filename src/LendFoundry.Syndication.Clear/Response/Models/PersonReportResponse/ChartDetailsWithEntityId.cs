using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ChartDetailsWithEntityId : IChartDetailsWithEntityId
    {
        public ChartDetailsWithEntityId(Proxy.Response.PersonReport.ChartDetailsWithEntityId chartDetailsWithEntityId)
        {
            if (chartDetailsWithEntityId != null)
            {
                entityID              = chartDetailsWithEntityId.EntityID;
                personName            =new PersonName(chartDetailsWithEntityId.PersonName);
                riskFlagsWithDocguids =new RiskFlagsWithDocguids(chartDetailsWithEntityId.RiskFlagsWithDocguids);
            }
        }

        public string entityID { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlagsWithDocguids, RiskFlagsWithDocguids>))]
        public IRiskFlagsWithDocguids riskFlagsWithDocguids { get; set; }
    }
}
