﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public  class SSNInfo : ISSNInfo
    {
        public SSNInfo(Proxy.Response.PersonReport.SSNInfo sSNInfo)
        {
            if (sSNInfo!=null)
            {
                SSN = sSNInfo.SSN;

                partialSSN = sSNInfo.PartialSSN;

                SSNIssueDate = sSNInfo.SSNIssueDate;

                SSNExpirationDate = sSNInfo.SSNExpirationDate;

                SSNIssuanceText = sSNInfo.SSNIssuanceText;

                SSNIssueState = sSNInfo.SSNIssueState;
            }
        }
        public List<string> SSN { get; set; }

        public List<string> partialSSN { get; set; }

        public string SSNIssueDate { get; set; }

        public string SSNExpirationDate { get; set; }

        public string SSNIssuanceText { get; set; }

        public string SSNIssueState { get; set; }

    }
}
