using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CriminalCharges : ICriminalCharges
    {
       
        public CriminalCharges(Proxy.Response.PersonReport.CriminalCharges criminalCharges)
        {
            if (criminalCharges != null)
            {
               amendedCriminalOffense       = criminalCharges.AmendedCriminalOffense;
               amendedStatuteViolated       = criminalCharges.AmendedStatuteViolated;
               chargesTypeOfDocument        = criminalCharges.ChargesTypeOfDocument;
               classOfCrime                 = criminalCharges.ClassOfCrime;
               convictionLocation           = criminalCharges.ConvictionLocation;
               countyOfCrime                = criminalCharges.CountyOfCrime;
               crimeDate                    = criminalCharges.CrimeDate;
               crimeIndicators              = new CrimeIndicators(criminalCharges.CrimeIndicators);
               criminalCourt                = criminalCharges.CriminalCourt;
               criminalOffense              = criminalCharges.CriminalOffense;
               dispositionOfCrime           = criminalCharges.DispositionOfCrime;
               highestFelonyClass           = criminalCharges.HighestFelonyClass;
               numberOfCounts               = criminalCharges.NumberOfCounts;
               offenderStatus               = criminalCharges.OffenderStatus;
               statuteViolated              = criminalCharges.StatuteViolated;
               totalConvictedUniqueOffenses = criminalCharges.TotalConvictedUniqueOffenses;
               townOfCrime                  = criminalCharges.TownOfCrime;
               bailAmount                   = criminalCharges.BailAmount;
               caseInfo                     = new CaseInfo(criminalCharges.CaseInfo);
               courtDateInfo                = new CourtDateInfo(criminalCharges.CourtDateInfo);
               inmateReleaseInfo            = new InmateReleaseInfo(criminalCharges.InmateReleaseInfo);
               miscellaneousInfo            = criminalCharges.MiscellaneousInfo;
               warrantNumber                = criminalCharges.WarrantNumber;
            }
        }

        public string amendedCriminalOffense { get; set; }
        public string amendedStatuteViolated { get; set; }
        public string chargesTypeOfDocument { get; set; }
        public string classOfCrime { get; set; }
        public string convictionLocation { get; set; }
        public string countyOfCrime { get; set; }
        public string crimeDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICrimeIndicators, CrimeIndicators>))]
        public ICrimeIndicators crimeIndicators { get; set; }
        public string criminalCourt { get; set; }
        public string criminalOffense { get; set; }
        public string dispositionOfCrime { get; set; }
        public string highestFelonyClass { get; set; }
        public string numberOfCounts { get; set; }
        public string offenderStatus { get; set; }
        public string statuteViolated { get; set; }
        public string totalConvictedUniqueOffenses { get; set; }
        public string townOfCrime { get; set; }
        public string bailAmount { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICaseInfo, CaseInfo>))]
        public ICaseInfo caseInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICourtDateInfo, CourtDateInfo>))]
        public ICourtDateInfo courtDateInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IInmateReleaseInfo, InmateReleaseInfo>))]
        public IInmateReleaseInfo inmateReleaseInfo { get; set; }
        public List<string> miscellaneousInfo { get; set; }
        public string warrantNumber { get; set; }
    }
}
