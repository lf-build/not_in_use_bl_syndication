namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAssetLiabilityInfo
    {
         string otherAssets { get; set; }

         string otherLiability { get; set; }

         string scheduledDate { get; set; }

         string securedExemptAssets { get; set; }

         string securedLiability { get; set; }

         string totalAssets { get; set; }

         string totalLiability { get; set; }

         string unsecuredLiability { get; set; }
    }
}
