﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SourceInfo:ISourceInfo
    {
        public SourceInfo(Proxy.Response.PersonReport.SourceInfo sourceInfo)
        {
            if (sourceInfo!=null)
            {
                sourceName = sourceInfo.SourceName;
                firstReportedDate = sourceInfo.FirstReportedDate;
                lastReportedDate = sourceInfo.LastReportedDate;
            }
        }
        public List<string> sourceName { get; set; }

        public List<string> firstReportedDate { get; set; }

        public List<string> lastReportedDate { get; set; }
    }
}
