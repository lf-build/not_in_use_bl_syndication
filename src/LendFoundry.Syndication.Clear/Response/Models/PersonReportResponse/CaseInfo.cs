using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CaseInfo : ICaseInfo
    {
        public CaseInfo(Proxy.Response.PersonReport.CaseInfo caseInfo)
        {
            if(caseInfo!=null)
            {
                districtCourtDocketNumber = caseInfo.DistrictCourtDocketNumber;
                docketNumber = caseInfo.DocketNumber;
                otherCaseNumber = caseInfo.OtherCaseNumber;
                caseNumber = caseInfo.CaseNumber;
                caseFiling = new CaseFiling(caseInfo.CaseFiling);
                caseGeneralCategory = caseInfo.CaseGeneralCategory;
                caseTitle = caseInfo.CaseTitle;
                caseCategory = caseInfo.CaseCategory;
                caseInformation = caseInfo.CaseInformation;
                caseStatus = caseInfo.CaseStatus;
                caseStatusDate = caseInfo.CaseStatusDate;
                caseComments = caseInfo.CaseComments;
            }
        }
        public string districtCourtDocketNumber { get; set; }
        public string docketNumber { get; set; }
        public string otherCaseNumber { get; set; }
        public List<string> caseNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICaseFiling, CaseFiling>))]
        public ICaseFiling caseFiling { get; set; }
        public string caseGeneralCategory { get; set; }
        public string caseTitle { get; set; }
        public string caseCategory { get; set; }
        public string caseInformation { get; set; }
        public string caseStatus { get; set; }
        public string caseStatusDate { get; set; }
        public List<string> caseComments { get; set; }
    }
}
