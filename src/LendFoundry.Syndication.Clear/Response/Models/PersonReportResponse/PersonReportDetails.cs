﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonReportDetails: IPersonReportDetails
    {
        public PersonReportDetails(Proxy.Response.PersonReport.PersonReportDetails personReportDetails)
        {
            if (personReportDetails!=null)
            {
                Status =new Status(personReportDetails.Status);
                SectionResults = personReportDetails.SectionResults.Select(a=> new PersonReportDetailsSectionResults(a)).ToList<IPersonReportDetailsSectionResults>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IStatus, Status>))]
        public IStatus Status { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPersonReportDetailsSectionResults, PersonReportDetailsSectionResults>))]
        public List<IPersonReportDetailsSectionResults> SectionResults { get; set; }
    }
}
