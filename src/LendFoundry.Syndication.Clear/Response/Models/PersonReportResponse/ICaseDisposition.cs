﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICaseDisposition
    {
         string decisionCategory { get; set; }

         string decision { get; set; }

         string finalDate { get; set; }

         string disposition { get; set; }

         string dispositionDate { get; set; }
    }
}
