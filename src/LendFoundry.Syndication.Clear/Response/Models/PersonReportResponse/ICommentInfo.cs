﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
  public   interface ICommentInfo
    {
         List<string> comment { get; set; }

         string commentLine2 { get; set; }

         List<string> commentDate { get; set; }

         List<string> commentDescription { get; set; }

         List<ICommentSource> commentSource { get; set; }

         string commentCode { get; set; }

         string commentTitle { get; set; }
    }
}