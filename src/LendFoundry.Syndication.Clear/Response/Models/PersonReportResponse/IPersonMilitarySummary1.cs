namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonMilitarySummary1
    {
         string branchName { get; set; }

         string dischargeCategory { get; set; }

         string dischargeDate { get; set; }
    }
}
