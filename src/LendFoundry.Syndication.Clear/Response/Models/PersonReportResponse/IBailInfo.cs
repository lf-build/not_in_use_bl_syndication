namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IBailInfo
    {
         string bailerName { get; set; }

         string bailPosted { get; set; }

         string bailSet { get; set; }

         string dateBailPosted { get; set; }

         string dateBailSet { get; set; }
    }
}
