namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFineAmountInfo
    {
         string assessmentAmount{ get; set; }

         string courtCostAmount{ get; set; }

         string courtOrderedPayment{ get; set; }

         string feeAmount{ get; set; }

         string finedAmount{ get; set; }

         string restitutionAmount{ get; set; }

         string stayedFine{ get; set; }
    }
}
