﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISourceInfo
    {
        List<string> firstReportedDate { get; set; }
        List<string> lastReportedDate { get; set; }
        List<string> sourceName { get; set; }
    }
}