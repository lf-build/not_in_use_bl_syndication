namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IUndocumentedImmigrantInfo
    {
         string undocumentedImmigrantName { get; set; }

         string undocumentedImmigrantID { get; set; }
    }
}
