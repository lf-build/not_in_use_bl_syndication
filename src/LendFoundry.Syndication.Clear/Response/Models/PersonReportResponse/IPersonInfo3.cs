using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonInfo3
    {
         List<IPersonName> personName { get; set; }

         IPersonProfile personProfile { get; set; }

         List<IPersonAKA1> personAKA { get; set; }

         ISSNInfo personSSN { get; set; }

         List<IPersonMilitarySummary1> personMilitarySummary { get; set; }

         List<IPersonEducation> personEducation { get; set; }
    }
}
