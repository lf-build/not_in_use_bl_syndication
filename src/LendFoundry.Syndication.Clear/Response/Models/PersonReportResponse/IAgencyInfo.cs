namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAgencyInfo
    {
         string agencyName { get; set; }

         string arrestReason { get; set; }

         string typeOfAgency { get; set; }

         string reportedDate { get; set; }

         IAddress address { get; set; }

         IPhoneInfo phone { get; set; }
    }
}
