namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonMilitarySummary1 : IPersonMilitarySummary1
    {
        public PersonMilitarySummary1(Proxy.Response.PersonReport.PersonMilitarySummary1 personMilitarySummary)
        {
            if (personMilitarySummary != null)
            {
                branchName        = personMilitarySummary.BranchName;
                dischargeCategory = personMilitarySummary.DischargeCategory;
                dischargeDate     = personMilitarySummary.DischargeDate;
            }
        }

        public string branchName { get; set; }
        public string dischargeCategory { get; set; }
        public string dischargeDate { get; set; }
    }
}
