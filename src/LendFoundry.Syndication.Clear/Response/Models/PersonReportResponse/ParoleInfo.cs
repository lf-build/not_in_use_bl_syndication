using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ParoleInfo : IParoleInfo
    {

        public ParoleInfo(Proxy.Response.PersonReport.ParoleInfo paroleInfo)
        {
            if (paroleInfo != null)
            {
                nextParoleHearingDate   = paroleInfo.NextParoleHearingDate;
                paroleAmendedDate       = paroleInfo.ParoleAmendedDate;
                paroleBeginDate         = paroleInfo.ParoleBeginDate;
                paroleEligibilityDate   = paroleInfo.ParoleEligibilityDate;
                paroleHearing           = paroleInfo.ParoleHearing.Select(a=> new ParoleHearing(a)).ToList<IParoleHearing>();
                paroleTerm              = new DurationOfTime(paroleInfo.ParoleTerm);
                paroleLocation          = paroleInfo.ParoleLocation;
                paroleOfficer           = paroleInfo.ParoleOfficer;
                paroleProjectedDate     = paroleInfo.ParoleProjectedDate;
                paroleReleaseDate       = paroleInfo.ParoleReleaseDate;
                paroleTerminationReason = paroleInfo.ParoleTerminationReason;
                probationStatus         = paroleInfo.ProbationStatus;
                supervisionCounty       = paroleInfo.SupervisionCounty;
                paroleStatus            = paroleInfo.ParoleStatus;
                paroleOfficerPhone      = paroleInfo.ParoleOfficerPhone;
            }
        }

        public string nextParoleHearingDate { get; set; }
        public string paroleAmendedDate { get; set; }
        public string paroleBeginDate { get; set; }
        public string paroleEligibilityDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IParoleHearing, ParoleHearing>))]
        public List<IParoleHearing> paroleHearing { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime paroleTerm { get; set; }
        public string paroleLocation { get; set; }
        public string paroleOfficer { get; set; }
        public string paroleProjectedDate { get; set; }
        public string paroleReleaseDate { get; set; }
        public string paroleTerminationReason { get; set; }
        public string probationStatus { get; set; }
        public string supervisionCounty { get; set; }
        public string paroleStatus { get; set; }
        public string paroleOfficerPhone { get; set; }
    }
}
