﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonProfile: IPersonProfile
    {
        public PersonProfile(Proxy.Response.PersonReport.PersonProfile personProfile)
        {
            if (personProfile!=null)
            {
                personBirthDate = personProfile.PersonBirthDate;

                yearOfBirth = personProfile.YearOfBirth;

                monthOfBirth = personProfile.MonthOfBirth;

                dayOfBirth = personProfile.DayOfBirth;

                personAgeTo = personProfile.PersonAgeTo;

                personAgeFrom = personProfile.PersonAgeFrom;

                personDeathIndicator = personProfile.PersonDeathIndicator;

                personDeathDate = personProfile.PersonDeathDate;

                personDeathPlace = personProfile.PersonDeathPlace;

                personBirthPlace = personProfile.PersonBirthPlace;

                personBirthState = personProfile.PersonBirthState;

                numberofDependents = personProfile.NumberofDependents;

                employeeOccupation = personProfile.EmployeeOccupation;

                professionalTitle = personProfile.ProfessionalTitle;

                personAge = personProfile.PersonAge;

                personBuild = personProfile.PersonBuild;

                personCitizenship = personProfile.PersonCitizenship;

                personEducationLevel = personProfile.PersonEducationLevel;

                personEthnicity = personProfile.PersonEthnicity;

                personEyeColor = personProfile.PersonEyeColor;

                personHairColor = personProfile.PersonHeight;

                personHeight = personProfile.PersonHeight;

                personRace = personProfile.PersonRace;

                personSex = personProfile.PersonSex;

                personSkinTone = personProfile.PersonSkinTone;

                personMaritalStatus = personProfile.PersonMaritalStatus;

                personSpouseName = personProfile.PersonSpouseName;

                personWeight = personProfile.PersonWeight;

                personMarkings = personProfile.PersonMarkings;

                personMarkingsLocation = personProfile.PersonMarkingsLocation;

                personMarkingsType = personProfile.PersonMarkingsType;

                personContactEmail = personProfile.PersonContactEmail;
            }
        }

        public List<string> personBirthDate { get; set; }

        public List<string> yearOfBirth { get; set; }

        public List<string> monthOfBirth { get; set; }

        public List<string> dayOfBirth { get; set; }

        public List<string> personAgeTo { get; set; }

        public List<string> personAgeFrom { get; set; }

        public List<string> personDeathIndicator { get; set; }

        public List<string> personDeathDate { get; set; }

        public List<string> personDeathPlace { get; set; }

        public List<string> personBirthPlace { get; set; }

        public List<string> personBirthState { get; set; }

        public List<string> numberofDependents { get; set; }

        public List<string> employeeOccupation { get; set; }

        public List<string> professionalTitle { get; set; }

        public List<string> personAge { get; set; }

        public List<string> personBuild { get; set; }

        public List<string> personCitizenship { get; set; }

        public List<string> personEducationLevel { get; set; }

        public List<string> personEthnicity { get; set; }

        public List<string> personEyeColor { get; set; }

        public List<string> personHairColor { get; set; }

        public List<string> personHeight { get; set; }

        public List<string> personRace { get; set; }

        public List<string> personSex { get; set; }

        public List<string> personSkinTone { get; set; }

        public List<string> personMaritalStatus { get; set; }

        public List<string> personSpouseName { get; set; }

        public List<string> personWeight { get; set; }

        public List<string> personMarkings { get; set; }

        public List<string> personMarkingsLocation { get; set; }

        public List<string> personMarkingsType { get; set; }

        public List<string> personContactEmail { get; set; }

    }
}
