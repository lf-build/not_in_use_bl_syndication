﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CommentInfo : ICommentInfo
    {
        public CommentInfo(Proxy.Response.PersonReport.CommentInfo commentInfo)
        {
            if (commentInfo != null)
            {
                comment            = commentInfo.Comment;
                commentLine2       = commentInfo.CommentLine2;
                commentDate        = commentInfo.CommentDate;
                commentDescription = commentInfo.CommentDescription;
                commentSource      = commentInfo.CommentSource.Select(a=>new CommentSource(a)).ToList<ICommentSource>();
                commentCode        = commentInfo.CommentCode;
                commentTitle       = commentInfo.CommentTitle;
            }
        }

        public List<string> comment { get; set; }

        public string commentLine2 { get; set; }

        public List<string> commentDate { get; set; }

        public List<string> commentDescription { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ICommentSource, CommentSource>))]
        public List<ICommentSource> commentSource { get; set; }

        public string commentCode { get; set; }

        public string commentTitle { get; set; }
    }
}
