﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   public  interface ICommentSource
    {
         string sourceBusinessName { get; set; }

         string sourceName { get; set; }

         string sourceTitle { get; set; }

         string sourceTitle2 { get; set; }

         string sourceTitle3 { get; set; }

         string statusInfo { get; set; }

         string sourceTitleDescription { get; set; }
    }
}