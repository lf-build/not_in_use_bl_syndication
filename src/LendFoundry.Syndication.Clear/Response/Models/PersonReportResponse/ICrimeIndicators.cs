namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICrimeIndicators
    {
         string attemptedCrime { get; set; }

         string drugCrime { get; set; }

         string guiltyButMentallyIll { get; set; }

         string hateCrime { get; set; }

         string inchoateCrime { get; set; }

         string otherCrimeInfo { get; set; }

         string propertyCrime { get; set; }

         string propertySeized { get; set; }

         string riskLevel { get; set; }

         string secondFelony { get; set; }

         string sexCrime { get; set; }

         string victimAgeDesc { get; set; }

         string victimGender { get; set; }

         string victimIsaMinor { get; set; }

         string weaponCrime { get; set; }
    }
}
