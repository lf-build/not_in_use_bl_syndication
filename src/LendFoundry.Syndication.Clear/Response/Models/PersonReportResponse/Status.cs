﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Status: IStatus
    {
        public Status(Proxy.Response.PersonReport.Status status)
        {
            if (status!=null)
            {
                StatusCode = status.StatusCode;
                SubStatusCode = status.SubStatusCode;
            }
        }
        public  int StatusCode { get; set; }
        public int SubStatusCode { get; set; }
    }
}