using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IProbationInfo
    {
         string placementAfterViolation { get; set; }

         string probationActualEndDate { get; set; }

         string probationAfterPrison { get; set; }

         string probationAgency { get; set; }

         string probationBeginDate { get; set; }

         string probationScheduledEndDate { get; set; }

         List<IDurationOfTime> probationTime { get; set; }

         IDurationOfTime probationMinimumTime { get; set; }

         string probationViolation { get; set; }

         string probationViolationDate { get; set; }

         string probConsecutiveConcurrent { get; set; }

         string returnedToCustodyDate { get; set; }

         string typeOfProbation { get; set; }
    }
}
