﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ImageInfo: IImageInfo
    {
        public ImageInfo(Proxy.Response.PersonReport.ImageInfo imageInfo)
        {
            if (imageInfo!=null)
            {
                binaryImage = imageInfo.BinaryImage;
                contentType = imageInfo.ContentType;
            }
        }
        public string binaryImage { get; set; }

        public string contentType { get; set; }

    }
}
