using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class LienJudgeNYRecord : ILienJudgeNYRecord
    {
        public LienJudgeNYRecord(Proxy.Response.PersonReport.LienJudgeNYRecord lienJudgeNYRecord)
        {
            if (lienJudgeNYRecord != null)
            {
                creditor    = lienJudgeNYRecord.Creditor.Select(a=>new Creditor(a)).ToList<ICreditor>();
                debtor      = lienJudgeNYRecord.Debtor.Select(a => new Debtor(a)).ToList<IDebtor>();
                filingInfo  = new FilingInfo1(lienJudgeNYRecord.FilingInfo);
                otherParty  = lienJudgeNYRecord.OtherParty.Select(a => new OtherParty(a)).ToList<IOtherParty>();
                lienInfo    = new LienInfo(lienJudgeNYRecord.LienInfo);
                comment     = lienJudgeNYRecord.Comment;
                commentDate = lienJudgeNYRecord.CommentDate;
                court       = lienJudgeNYRecord.Court;
                source      = lienJudgeNYRecord.Source;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<ICreditor, Creditor>))]
        public List<ICreditor> creditor { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFilingInfo1, FilingInfo1>))]
        public IFilingInfo1 filingInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOtherParty, OtherParty>))]
        public List<IOtherParty> otherParty { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ILienInfo, LienInfo>))]
        public ILienInfo lienInfo { get; set; }
        public List<string> comment { get; set; }
        public List<string> commentDate { get; set; }
        public string court { get; set; }
        public string source { get; set; }
    }
}
