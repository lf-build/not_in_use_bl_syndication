﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class StatusInfo : IStatusInfo
    {
        public StatusInfo(Proxy.Response.PersonReport.StatusInfo statusInfo)
        {
            if (statusInfo != null)
            {
                status = statusInfo.Status;
                statusDate = statusInfo.StatusDate;
            }
        }

        public string status { get; set; }

        public string statusDate { get; set; }
    }
}
