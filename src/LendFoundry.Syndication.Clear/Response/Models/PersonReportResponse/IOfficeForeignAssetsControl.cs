namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IOfficeForeignAssetsControl
    {
         string miscellaneousInfo { get; set; }

         string sanctionProgram { get; set; }

         IVesselInfo vesselInfo { get; set; }
    }
}
