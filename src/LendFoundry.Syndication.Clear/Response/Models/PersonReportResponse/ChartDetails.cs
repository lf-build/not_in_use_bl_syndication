using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ChartDetails : IChartDetails
    {
        public ChartDetails(Proxy.Response.PersonReport.ChartDetails chartDetails)
        {
            if (chartDetails != null)
            {
                this.personName = new PersonName(chartDetails.PersonName);
                this.riskFlags = new RiskFlags(chartDetails.RiskFlags);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IRiskFlags, RiskFlags>))]
        public IRiskFlags riskFlags { get; set; }
    }
}
