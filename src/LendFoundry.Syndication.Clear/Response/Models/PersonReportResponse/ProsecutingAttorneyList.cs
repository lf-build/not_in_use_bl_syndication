using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ProsecutingAttorneyList : IProsecutingAttorneyList
    {
      
        public ProsecutingAttorneyList(Proxy.Response.PersonReport.ProsecutingAttorneyList prosecutingAttorneyList)
        {
            if (prosecutingAttorneyList != null)
            {
                prosecutingAgency = prosecutingAttorneyList.ProsecutingAgency;
                attorneyInfo = new AttorneyInfo(prosecutingAttorneyList.AttorneyInfo);
                attorneyAddress =new Address(prosecutingAttorneyList.AttorneyAddress);
            }
        }

        public string prosecutingAgency { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAttorneyInfo, AttorneyInfo>))]
        public IAttorneyInfo attorneyInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress attorneyAddress { get; set; }
    }
}
