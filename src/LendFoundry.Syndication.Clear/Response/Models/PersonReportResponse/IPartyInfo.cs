﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPartyInfo
    {
         IPersonName personName { get; set; }

        string businessName { get; set; }

         List<string> typeOfParty { get; set; }

        List<IAddress> address { get; set; }

         IPhoneInfo phoneInfo { get; set; }

        List<IAttorneyInfo> attorneyInfo { get; set; }

        List<string> comment { get; set; }

         List<string> dUNSNumber { get; set; }

         List<string> headquarterDUNSNumber { get; set; }

         string foreignReg { get; set; }

         List<ISSNInfo> sSNInfo { get; set; }

        List<IDriverLicenseInfo> driverLicenseInfo { get; set; }

         List<IPersonProfile> personProfile { get; set; }

         List<string> fEIN { get; set; }

        List<IPersonName> aKAName { get; set; }

         string reportedDate { get; set; }
    }
}
