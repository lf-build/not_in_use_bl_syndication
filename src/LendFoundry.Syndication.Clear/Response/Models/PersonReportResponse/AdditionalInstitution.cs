using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AdditionalInstitution : IAdditionalInstitution
    {
        public AdditionalInstitution(Proxy.Response.PersonReport.AdditionalInstitution additionalInstitution)
        {
            if (additionalInstitution != null)
            {
                assignmentTypeDesc = additionalInstitution.AssignmentTypeDesc;
                custodyReviewDate = additionalInstitution.CustodyReviewDate;
                custodyReviewDesc = additionalInstitution.CustodyReviewDesc;
                admissionInfo = new AdmissionInfo(additionalInstitution.AdmissionInfo);
            }
        }

        public string assignmentTypeDesc { get; set; }
        public string custodyReviewDate { get; set; }
        public string custodyReviewDesc { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAdmissionInfo, AdmissionInfo>))]
        public IAdmissionInfo admissionInfo { get; set; }
    }
}
