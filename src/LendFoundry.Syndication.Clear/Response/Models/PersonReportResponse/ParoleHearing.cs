namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ParoleHearing : IParoleHearing
    {
        public ParoleHearing(Proxy.Response.PersonReport.ParoleHearing paroleHearing)
        {
            if (paroleHearing != null)
            {
                hearingDate = paroleHearing.HearingDate;
                hearingLocation = paroleHearing.HearingLocation;
                typeOfHearing = paroleHearing.TypeOfHearing;
                numMonthsDeferred = paroleHearing.NumMonthsDeferred;
                paroleBoardAction = paroleHearing.ParoleBoardAction;
           }
        }
        public string hearingDate { get; set; }
        public string hearingLocation { get; set; }
        public string typeOfHearing { get; set; }
        public string numMonthsDeferred { get; set; }
        public string paroleBoardAction { get; set; }
    }
}
