using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPublicBankruptcyRecord
    {
         List<IAssetLiabilityInfo> assetLiabilityInfo { get; set; }

         List<ICommentInfo> comments { get; set; }

         List<ICreditor> creditor { get; set; }

         List<IDebtor> debtor { get; set; }

         List<IEventHistory> eventHistory { get; set; }

         IFilingInfo1 filingInfo { get; set; }

         IAddress filingOfficeAddress { get; set; }

         string judge { get; set; }

         IPlanInfo planInfo { get; set; }

         IPartyInfo referee { get; set; }

         IScheduled341 scheduled341 { get; set; }

         ITrustee trustee { get; set; }

         string source { get; set; }
    }
}
