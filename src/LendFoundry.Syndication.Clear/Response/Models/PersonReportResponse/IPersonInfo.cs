﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public   interface IPersonInfo
    {
         IPersonName personName { get; set; }


         IPersonProfile personProfile { get; set; }
         ISSNInfo personSSN { get; set; }
    }
}
