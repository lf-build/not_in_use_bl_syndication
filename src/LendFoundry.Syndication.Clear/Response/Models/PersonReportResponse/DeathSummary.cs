﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class DeathSummary: IDeathSummary
    {
        public DeathSummary(Proxy.Response.PersonReport.DeathSummary deathSummary)
        {
            if (deathSummary!=null)
            {
                personDeath=new PersonProfile(deathSummary.PersonDeath);
                sourceInfo = new SourceInfo(deathSummary.SourceInfo);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile personDeath { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISourceInfo, SourceInfo>))]
        public ISourceInfo sourceInfo { get; set; }
    }
}
