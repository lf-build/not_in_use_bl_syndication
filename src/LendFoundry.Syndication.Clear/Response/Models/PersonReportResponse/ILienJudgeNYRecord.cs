using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ILienJudgeNYRecord
    {
         List<ICreditor> creditor { get; set; }

         List<IDebtor> debtor { get; set; }

         IFilingInfo1 filingInfo { get; set; }

         List<IOtherParty> otherParty { get; set; }

         ILienInfo lienInfo { get; set; }

         List<string> comment { get; set; }

         List<string> commentDate { get; set; }

         string court { get; set; }

         string source { get; set; }
    }
}
