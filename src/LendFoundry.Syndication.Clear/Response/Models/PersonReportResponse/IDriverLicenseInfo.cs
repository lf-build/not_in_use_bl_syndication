﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDriverLicenseInfo
    {
        List<string> driverLicenseCountry { get; set; }
        List<string> driverLicenseNumber { get; set; }
        List<string> driverLicenseState { get; set; }
    }
}