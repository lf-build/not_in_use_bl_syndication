using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class OfficeForeignAssetsControl : IOfficeForeignAssetsControl
    {
        public OfficeForeignAssetsControl(Proxy.Response.PersonReport.OfficeForeignAssetsControl officeForeignAssetsControl)
        {
            if (officeForeignAssetsControl != null)
            {
                miscellaneousInfo = officeForeignAssetsControl.MiscellaneousInfo;
                sanctionProgram   = officeForeignAssetsControl.SanctionProgram;
                vesselInfo        = new VesselInfo(officeForeignAssetsControl.VesselInfo);
            }
        }

        public string miscellaneousInfo { get; set; }
        public string sanctionProgram { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IVesselInfo, VesselInfo>))]
        public IVesselInfo vesselInfo { get; set; }
    }
}
