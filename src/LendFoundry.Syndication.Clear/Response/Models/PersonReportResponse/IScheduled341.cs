namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IScheduled341
    {
         string scheduled341Date{ get; set; }

         string scheduled341Description{ get; set; }

         string scheduled341Location{ get; set; }

         string scheduled341Time{ get; set; }
    }
}
