using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IRiskFlagInfo
    {
         string riskFlag { get; set; }

         List<ISources> documentGuids { get; set; }
    }
}
