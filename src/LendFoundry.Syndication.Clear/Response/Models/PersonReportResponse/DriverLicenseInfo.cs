﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class DriverLicenseInfo: IDriverLicenseInfo
    {
        public DriverLicenseInfo(Proxy.Response.PersonReport.DriverLicenseInfo driverLicenseInfo)
        {
            if (driverLicenseInfo!=null)
            {
                driverLicenseNumber = driverLicenseInfo.DriverLicenseNumber;
                driverLicenseState = driverLicenseInfo.DriverLicenseState;
                driverLicenseCountry = driverLicenseInfo.DriverLicenseCounty;
            }
        }
        public List<string> driverLicenseNumber { get; set; }

        public List<string> driverLicenseState { get; set; }

        public List<string> driverLicenseCountry { get; set; }

    }
}
