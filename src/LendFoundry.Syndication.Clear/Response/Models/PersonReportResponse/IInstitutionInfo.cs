namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IInstitutionInfo
    {
         IAdditionalInstitution additionalInstitution { get; set; }

         IAddress address { get; set; }

         IAdmissionInfo admissionInfo { get; set; }

         string classAtEscape { get; set; }

         string escapeDate { get; set; }

         string escapedFacility { get; set; }

         string escapeHistoryDesc { get; set; }

         string escapeRecaptureDate { get; set; }

         string gainTime { get; set; }

         string gainTimeEffectiveDate { get; set; }

         string inmateCellNumber { get; set; }

         string inmateNumber { get; set; }

         string inmateStatus { get; set; }

         string inmateCustodyClass { get; set; }

         string lastMoveOrTransfer { get; set; }

         string lastMoveOrTransferDate { get; set; }

         IPhoneInfo phone { get; set; }

         string specialProvision { get; set; }

         string specialProvisionDate { get; set; }

         string timeServedCredit { get; set; }
    }
}
