namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ValueAndLabelSet : IValueAndLabelSet
    {
        public ValueAndLabelSet(Proxy.Response.PersonReport.ValueAndLabelSet valueAndLabelSet)
        {
            if (valueAndLabelSet != null)
            {
                fieldDateLabel   = valueAndLabelSet.FieldDateLabel;
                fieldDateValue   = valueAndLabelSet.FieldDateValue;
                fieldStringLabel = valueAndLabelSet.FieldStringLabel;
                fieldStringValue = valueAndLabelSet.FieldStringValue;
            }
        }

        public string fieldDateLabel { get; set; }
        public string fieldDateValue { get; set; }
        public string fieldStringLabel { get; set; }
        public string fieldStringValue { get; set; }
    }
}
