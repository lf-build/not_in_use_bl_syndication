namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AssetLiabilityInfo : IAssetLiabilityInfo
    {
        public AssetLiabilityInfo(Proxy.Response.PersonReport.AssetLiabilityInfo assetLiabilityInfo)
        {
            if (assetLiabilityInfo != null)
            {
                otherAssets         = assetLiabilityInfo.OtherAssets;
                otherLiability      = assetLiabilityInfo.OtherLiability;
                scheduledDate       = assetLiabilityInfo.ScheduledDate;
                securedExemptAssets = assetLiabilityInfo.SecuredExemptAssets;
                securedLiability    = assetLiabilityInfo.SecuredLiability;
                totalAssets         = assetLiabilityInfo.TotalAssets;
                totalLiability      = assetLiabilityInfo.TotalLiability;
                unsecuredLiability  = assetLiabilityInfo.UnsecuredLiability;
            }
        }

        public string otherAssets { get; set; }
        public string otherLiability { get; set; }
        public string scheduledDate { get; set; }
        public string securedExemptAssets { get; set; }
        public string securedLiability { get; set; }
        public string totalAssets { get; set; }
        public string totalLiability { get; set; }
        public string unsecuredLiability { get; set; }
    }
}
