using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IParoleInfo
    {
         string nextParoleHearingDate { get; set; }

         string paroleAmendedDate { get; set; }

         string paroleBeginDate { get; set; }

         string paroleEligibilityDate { get; set; }

         List<IParoleHearing> paroleHearing { get; set; }

         IDurationOfTime paroleTerm { get; set; }

         string paroleLocation { get; set; }

         string paroleOfficer { get; set; }

         string paroleProjectedDate { get; set; }

         string paroleReleaseDate { get; set; }

         string paroleTerminationReason { get; set; }

         string probationStatus { get; set; }

         string supervisionCounty { get; set; }

         string paroleStatus { get; set; }

         string paroleOfficerPhone { get; set; }
    }
}
