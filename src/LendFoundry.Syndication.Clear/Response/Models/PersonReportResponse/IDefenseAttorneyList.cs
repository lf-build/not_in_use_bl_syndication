namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IDefenseAttorneyList
    {
         string typeOfDefenseUsed { get; set; }

         IAttorneyInfo attorneyInfo { get; set; }

         IAddress attorneyAddress { get; set; }
    }
}
