﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IUserSearchInfo
    {
        IAddress address { get; set; }
        string businessName { get; set; }
        string cIK { get; set; }
        string corporationID { get; set; }
        string criminalID { get; set; }
        IDriverLicenseInfo driverLicense { get; set; }
        string dUNS { get; set; }
        string federalEmpID { get; set; }
        string fileTypeCN { get; set; }
        string fileTypeCN3 { get; set; }
        string fileTypeCN5 { get; set; }
        string filingDate { get; set; }
        string filingNumber { get; set; }
        string hullID { get; set; }
        string licensePlate { get; set; }
        string make { get; set; }
        string model { get; set; }
        IPersonName name { get; set; }
        string nPINumber { get; set; }
        string parcelNumber { get; set; }
        IPersonProfile personProfile { get; set; }
        IPhoneInfo phone { get; set; }
        string profession { get; set; }
        string professionalLicenseNumber { get; set; }
        ISSNInfo sSN { get; set; }
        string tickerSymbol { get; set; }
        string titleNumber { get; set; }
        string vesselID { get; set; }
        string vesselName { get; set; }
        string vINNumber { get; set; }
        string year { get; set; }
    }
}