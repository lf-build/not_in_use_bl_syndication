namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IProsecutingAttorneyList
    {
         string prosecutingAgency{ get; set; }

         IAttorneyInfo attorneyInfo{ get; set; }

         IAddress attorneyAddress{ get; set; }
    }
}
