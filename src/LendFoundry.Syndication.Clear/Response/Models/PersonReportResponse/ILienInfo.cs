﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public   interface ILienInfo
    {
         string debtLot { get; set; }

         string debtNumber { get; set; }

         string judgmentAmount { get; set; }

         string satisfactionDate { get; set; }

         string typeofSatisfaction { get; set; }

         string sheriffExecutionRequired { get; set; }

         List<string> expirationDate { get; set; }

         string creditorAmount { get; set; }

         string lienAmount { get; set; }

         string lienPrincipalAmount { get; set; }

         string taxAmount { get; set; }

         string lienTotalAmount { get; set; }

         string totalAmountPaid { get; set; }

         string totalCalculatedAmount { get; set; }

         string totalCalculatedAmountPaid { get; set; }

         IStatusInfo statusInfo { get; set; }

         ISubjudgmentInfo subjudgmentInfo { get; set; }
    }
}
