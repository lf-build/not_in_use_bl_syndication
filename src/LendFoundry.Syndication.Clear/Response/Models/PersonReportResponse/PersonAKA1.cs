using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonAKA1 : IPersonAKA1
    {
        public PersonAKA1(Proxy.Response.PersonReport.PersonAKA1 personAKA)
        {
            if (personAKA != null)
            {
                aKADateOfBirth = personAKA.AKADateOfBirth;
                aKAPersonName = new PersonName(personAKA.AKAPersonName);
                aKANickName = personAKA.AKANickName;
            }
        }

        public string aKADateOfBirth { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName aKAPersonName { get; set; }
        public List<string> aKANickName { get; set; }
    }
}
