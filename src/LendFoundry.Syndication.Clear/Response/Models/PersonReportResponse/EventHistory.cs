namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class EventHistory : IEventHistory
    {
        public EventHistory(Proxy.Response.PersonReport.EventHistory eventHistory)
        {
            if (eventHistory != null)
            {
                historyEventDate        = eventHistory.HistoryEventDate;
                historyEventDescription = eventHistory.HistoryEventDescription;
                typeOfHistoryEvent      = eventHistory.TypeOfHistoryEvent;
            }
        }

        public string historyEventDate { get; set; }
        public string historyEventDescription { get; set; }
        public string typeOfHistoryEvent { get; set; }
    }
}
