namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Scheduled341 : IScheduled341
    {
     
        public Scheduled341(Proxy.Response.PersonReport.Scheduled341 scheduled341)
        {
            if (scheduled341 != null)
            {
                scheduled341Date        = scheduled341.Scheduled341Date;
                scheduled341Description = scheduled341.Scheduled341Description;
                scheduled341Location    = scheduled341.Scheduled341Location;
                scheduled341Time        = scheduled341.Scheduled341Time;
            }
        }

        public string scheduled341Date { get; set; }
        public string scheduled341Description { get; set; }
        public string scheduled341Location { get; set; }
        public string scheduled341Time { get; set; }
    }
}
