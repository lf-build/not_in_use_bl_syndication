namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class RiskFlags : IRiskFlags
    {
        public RiskFlags(Proxy.Response.PersonReport.RiskFlags riskFlags)
        {
            if (riskFlags != null)
            {
                assocWithOFACGlobalPEP                     = riskFlags.AssocWithOFACGlobalPEP;
                oFAC                                       = riskFlags.OFAC;
                worldCheck                                 = riskFlags.WorldCheck;
                globalSanctions                            = riskFlags.GlobalSanctions;
                residentialUsedAsBusiness                  = riskFlags.ResidentialUsedAsBusiness;
                prisonAddress                              = riskFlags.PrisonAddress;
                pOBoxAddress                               = riskFlags.POBoxAddress;
                bankruptcy                                 = riskFlags.Bankruptcy;
                assocRelativeWithResidentialUsedAsBusiness = riskFlags.AssocRelativeWithResidentialUsedAsBusiness;
                assocRelativeWithPrisonAddress             = riskFlags.AssocRelativeWithPrisonAddress;
                assocRelativeWithPOBoxAddress              = riskFlags.AssocRelativeWithPOBoxAddress;
                criminal                                   = riskFlags.Criminal;
                multipleSSN                                = riskFlags.MultipleSSN;
                sSNMultipleIndividuals                     = riskFlags.SSNMultipleIndividuals;
                recordedAsDeceased                         = riskFlags.RecordedAsDeceased;
                ageYoungerThanSSN                          = riskFlags.AgeYoungerThanSSN;
                addressReportedLessNinetyDays              = riskFlags.AddressReportedLessNinetyDays;
                sSNFormatInvalid                           = riskFlags.SSNFormatInvalid;
                healthcareSanction                         = riskFlags.HealthcareSanction;
                phoneNumberInconsistentAddress             = riskFlags.PhoneNumberInconsistentAddress;
                arrest                                     = riskFlags.Arrest;
            }
        }

        public string assocWithOFACGlobalPEP { get; set; }
        public string oFAC { get; set; }
        public string worldCheck { get; set; }
        public string globalSanctions { get; set; }
        public string residentialUsedAsBusiness { get; set; }
        public string prisonAddress { get; set; }
        public string pOBoxAddress { get; set; }
        public string bankruptcy { get; set; }
        public string assocRelativeWithResidentialUsedAsBusiness { get; set; }
        public string assocRelativeWithPrisonAddress { get; set; }
        public string assocRelativeWithPOBoxAddress { get; set; }
        public string criminal { get; set; }
        public string multipleSSN { get; set; }
        public string sSNMultipleIndividuals { get; set; }
        public string recordedAsDeceased { get; set; }
        public string ageYoungerThanSSN { get; set; }
        public string addressReportedLessNinetyDays { get; set; }
        public string sSNFormatInvalid { get; set; }
        public string healthcareSanction { get; set; }
        public string phoneNumberInconsistentAddress { get; set; }
        public string arrest { get; set; }
    }
}
