namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AdditionalNotes : IAdditionalNotes
    {
        public AdditionalNotes(Proxy.Response.PersonReport.AdditionalNotes additionalNotes)
        {
            if (additionalNotes != null)
            {
                dataLabel = additionalNotes.DataLabel;
                dataValue = additionalNotes.DataValue;
            }
        }

        public string dataLabel { get; set; }
        public string dataValue { get; set; }
    }
}
