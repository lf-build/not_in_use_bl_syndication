using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class VesselInfo : IVesselInfo
    {
        public VesselInfo(Proxy.Response.PersonReport.VesselInfo vesselInfo)
        {
            if (vesselInfo != null)
            {
                typeOfVessel = vesselInfo.TypeOfVessel;
                callSign     = vesselInfo.CallSign;
                country      = vesselInfo.Country;
                grossTonnage = vesselInfo.GrossTonnage;
                owner        = vesselInfo.Owner;
                tonnage      = vesselInfo.Tonnage;
            }
        }

        public string typeOfVessel { get; set; }
        public string callSign { get; set; }
        public string country { get; set; }
        public string grossTonnage { get; set; }
        public List<string> owner { get; set; }
        public string tonnage { get; set; }
    }
}
