﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public  class UserTermsSection : IUserTermsSection
    {
        public UserTermsSection(Proxy.Response.PersonReport.UserTermsSection userTermsSection)
        {
            if (userTermsSection!=null)
            {
                userTermRecord = new UserTermRecord(userTermsSection.UserTermRecord);
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IUserTermRecord, UserTermRecord>))]
        public IUserTermRecord userTermRecord { get; set; }


    }
}
