using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IJudgmentInfo
    {
         string awardAmount{ get; set; }

         string judgementPrincipalAmount{ get; set; }

         string paidOnCreditAmount{ get; set; }

         string judgmentTotalAmount{ get; set; }

         List<IObligationInfo> obligationInfo{ get; set; }

         IStatusInfo statusInfo{ get; set; }

         ISubjudgmentInfo subjudgmentInfo{ get; set; }
    }
}
