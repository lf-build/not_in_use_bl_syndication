﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   public  interface ISubjudgmentInfo
    {
         string subjudgmentFilingDate { get; set; }

         List<string> comment { get; set; }

         ICaseDisposition dispositionInfo { get; set; }

         IStatusInfo statusInfo { get; set; }
    }
}
