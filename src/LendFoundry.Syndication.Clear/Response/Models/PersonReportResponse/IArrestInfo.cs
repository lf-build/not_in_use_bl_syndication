using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IArrestInfo
    {

         string agreementDate { get; set; }

         List<IAgencyInfo> arrestAgencyInfo { get; set; }

         string arrestCancelDate { get; set; }

         List<string> arrestDate { get; set; }

         string arrestLocation { get; set; }

         string arrestDisposition { get; set; }

         string arrestNum { get; set; }

         string arrestWarrantFlag { get; set; }

         IBookingInfo bookingInfo { get; set; }

         string countyOfCrime { get; set; }

         string timeOfArrest { get; set; }

         string nameUsedAtArrest { get; set; }

         string arrestCounty { get; set; }
    }
}
