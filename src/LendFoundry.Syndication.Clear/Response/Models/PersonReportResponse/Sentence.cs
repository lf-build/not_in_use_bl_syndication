using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class Sentence : ISentence
    {
        public Sentence(Proxy.Response.PersonReport.Sentence sentence)
        {
            if(sentence!=null)
            {
                additionalSentenceInformation = sentence.AdditionalSentenceInformation;
                durationOfTime = new DurationOfTime(sentence.DurationOfTime);

            }
        }
        public string additionalSentenceInformation { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime durationOfTime { get; set; }
    }
}
