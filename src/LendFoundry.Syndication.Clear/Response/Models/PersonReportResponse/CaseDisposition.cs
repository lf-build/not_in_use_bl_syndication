﻿
namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class CaseDisposition :ICaseDisposition
    {
        public CaseDisposition(Proxy.Response.PersonReport.CaseDisposition caseDisposition)
        {
            if (caseDisposition != null)
            {
                decisionCategory = caseDisposition.DecisionCategory;
                decision         = caseDisposition.Decision;
                finalDate        = caseDisposition.FinalDate;
                disposition      = caseDisposition.Disposition;
                dispositionDate  = caseDisposition.DispositionDate;
            }
        }

        public string decisionCategory  { get; set; }

        public string decision { get; set; }

        public string finalDate { get; set; }

        public string disposition { get; set; }

        public string dispositionDate { get; set; }
    }
}
