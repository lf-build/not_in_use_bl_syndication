namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICommunityService
    {
         string communityServiceCounty { get; set; }

        IDurationOfTime durationOfTime { get; set; }
    }
}
