﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   public  interface IComments
    {
         string commentDate { get; set; }

         string commentSource { get; set; }

         string commentText { get; set; }

         string commentText2 { get; set; }

         string sourceBusiness { get; set; }

         string sourceTitle { get; set; }

         string sourceTitle2 { get; set; }

         string sourceTitle3 { get; set; }

         string sourceTitleDesc { get; set; }
    }
}