namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FineAmountInfo : IFineAmountInfo
    {
        public FineAmountInfo(Proxy.Response.PersonReport.FineAmountInfo fineAmountInfo)
        {
            if(fineAmountInfo!=null)
            {
                assessmentAmount = fineAmountInfo.AssessmentAmount;
                courtCostAmount = fineAmountInfo.CourtCostAmount;
                courtOrderedPayment = fineAmountInfo.CourtOrderedPayment;
                feeAmount = fineAmountInfo.FeeAmount;
                finedAmount = fineAmountInfo.FinedAmount;
                restitutionAmount = fineAmountInfo.RestitutionAmount;
                stayedFine = fineAmountInfo.StayedFine;
            }
        }
        public string assessmentAmount { get; set; }
        public string courtCostAmount { get; set; }
        public string courtOrderedPayment { get; set; }
        public string feeAmount { get; set; }
        public string finedAmount { get; set; }
        public string restitutionAmount { get; set; }
        public string stayedFine { get; set; }
    }
}
