namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISentence
    {
         string additionalSentenceInformation { get; set; }

         IDurationOfTime durationOfTime { get; set; }
    }
}
