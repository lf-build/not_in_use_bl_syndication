using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class OffenderPriorInfo : IOffenderPriorInfo
    {
        public OffenderPriorInfo(Proxy.Response.PersonReport.OffenderPriorInfo offenderPriorInfo)
        {
            if (offenderPriorInfo != null)
            {
                inmateReleaseInfo    = new InmateReleaseInfo(offenderPriorInfo.InmateReleaseInfo);
                priorChargesInfo     = new PriorChargesInfo(offenderPriorInfo.PriorChargesInfo);
                sentencingInfo       =new SentencingInfo(offenderPriorInfo.SentencingInfo);
                priorCaseNumber      = offenderPriorInfo.PriorCaseNumber;
                priorCrimeDate       = offenderPriorInfo.PriorCrimeDate;
                priorCrimeCategory   = offenderPriorInfo.PriorCrimeCategory;
                priorCrimeClass      = offenderPriorInfo.PriorCrimeClass;
                priorDisposition     = offenderPriorInfo.PriorDisposition;
                priorDispositionDate = offenderPriorInfo.PriorDispositionDate;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IInmateReleaseInfo, InmateReleaseInfo>))]
        public IInmateReleaseInfo inmateReleaseInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPriorChargesInfo, PriorChargesInfo>))]
        public IPriorChargesInfo priorChargesInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISentencingInfo, SentencingInfo>))]
        public ISentencingInfo sentencingInfo { get; set; }
        public string priorCaseNumber { get; set; }
        public string priorCrimeDate { get; set; }
        public string priorCrimeCategory { get; set; }
        public string priorCrimeClass { get; set; }
        public string priorDisposition { get; set; }
        public string priorDispositionDate { get; set; }
    }
}
