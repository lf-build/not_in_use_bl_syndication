namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IEventHistory
    {
         string historyEventDate { get; set; }

         string historyEventDescription { get; set; }

         string typeOfHistoryEvent { get; set; }
    }
}
