using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ILienJudgeRecord
    {
         List<ICreditor> creditor { get; set; }

         List<IDebtor> debtor { get; set; }

         List<IFilingInfo1> filingInfo { get; set; }

         string source { get; set; }
    }
}
