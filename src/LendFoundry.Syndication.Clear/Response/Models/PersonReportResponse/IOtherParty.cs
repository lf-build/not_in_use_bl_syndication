namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IOtherParty
    {
             IPartyInfo partyInfo { get; set; }

    }
}
