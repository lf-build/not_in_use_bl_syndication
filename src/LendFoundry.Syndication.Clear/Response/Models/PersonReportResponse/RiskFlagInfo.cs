using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class RiskFlagInfo : IRiskFlagInfo
    {
        public RiskFlagInfo(Proxy.Response.PersonReport.RiskFlagInfo riskFlagInfo)
        {
            if (riskFlagInfo != null)
            {
               riskFlag = riskFlagInfo.RiskFlag;
                documentGuids = riskFlagInfo.DocumentGuids.Select(a=>new Sources(a)).ToList<ISources>();
            }
        }

        public string riskFlag { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISources, Sources>))]
        public List<ISources> documentGuids { get; set; }
    }
}
