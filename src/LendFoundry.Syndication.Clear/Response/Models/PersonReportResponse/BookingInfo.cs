using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class BookingInfo : IBookingInfo
    {
        public BookingInfo(Proxy.Response.PersonReport.BookingInfo bookingInfo)
        {
            if(bookingInfo!=null)
            {
                bookingDate = bookingInfo.BookingDate;
                bookingLocation = bookingInfo.BookingLocation;
                bookingLocationText = bookingInfo.BookingLocationText;
                bookingNumber = bookingInfo.BookingNumber;
                bookingTime = bookingInfo.BookingTime;
                arrestAgencyInfo = new AgencyInfo(bookingInfo.ArrestAgencyInfo);
                arrestDateTime = bookingInfo.ArrestDateTime;
                offenseDateTime = bookingInfo.OffenseDateTime;
                holdingAgencyInfo = new AgencyInfo(bookingInfo.HoldingAgencyInfo);
                paroleClass = bookingInfo.ParoleClass;
                releaseDateTime = bookingInfo.ReleaseDateTime;
                releasedFromSupervision = bookingInfo.ReleasedFromSupervision;
                releaseReason = bookingInfo.ReleaseReason;
                sentenceExpirationDate = bookingInfo.SentenceExpirationDate;
                scheduledReleaseDate = bookingInfo.ScheduledReleaseDate;
                status = bookingInfo.Status;
            }
        }
        public string bookingDate { get; set; }
        public string bookingLocation { get; set; }
        public string bookingLocationText { get; set; }
        public string bookingNumber { get; set; }
        public string bookingTime { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAgencyInfo, AgencyInfo>))]
        public IAgencyInfo arrestAgencyInfo { get; set; }
        public string arrestDateTime { get; set; }
        public string offenseDateTime { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAgencyInfo, AgencyInfo>))]
        public IAgencyInfo holdingAgencyInfo { get; set; }
        public string paroleClass { get; set; }
        public string releaseDateTime { get; set; }
        public string releasedFromSupervision { get; set; }
        public string releaseReason { get; set; }
        public string sentenceExpirationDate { get; set; }
        public string scheduledReleaseDate { get; set; }
        public string status { get; set; }
    }
}
