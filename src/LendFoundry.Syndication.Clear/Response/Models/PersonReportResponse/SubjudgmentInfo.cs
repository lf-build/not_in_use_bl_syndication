﻿
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SubjudgmentInfo : ISubjudgmentInfo
    {
        public SubjudgmentInfo(Proxy.Response.PersonReport.SubjudgmentInfo subjudgmentInfo)
        {
            if (subjudgmentInfo != null)
            {
                subjudgmentFilingDate = subjudgmentInfo.SubjudgmentFilingDate;
                comment               = subjudgmentInfo.Comment;
                dispositionInfo       = new CaseDisposition(subjudgmentInfo.DispositionInfo);
                statusInfo            = new StatusInfo(subjudgmentInfo.StatusInfo);
            }
        }

        public string subjudgmentFilingDate { get; set; }

        public List<string> comment { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICaseDisposition, CaseDisposition>))]
        public ICaseDisposition dispositionInfo { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IStatusInfo, StatusInfo>))]
        public IStatusInfo statusInfo { get; set; }
    }
}
