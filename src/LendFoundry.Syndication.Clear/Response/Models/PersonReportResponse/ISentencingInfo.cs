namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISentencingInfo
    {

         ICommunityService communityService { get; set; }

         string consecutiveConcurrent { get; set; }

         IDurationOfTime licenseSuspension { get; set; }

         IDurationOfTime maximumSentencing { get; set; }

         IDurationOfTime minimumSentencing { get; set; }

         ISentence sentence { get; set; }

         string sentenceBeginDate { get; set; }

         string sentenceCompleted { get; set; }

         string sentenceCounty { get; set; }

         string sentenceDescription { get; set; }

         string sentenceEffectiveDate { get; set; }

         string sentenceExpirationDate { get; set; }

         string sentenceImposedDate { get; set; }

         string sentenceModifiedDate { get; set; }

         string sentencePardoned { get; set; }

         string sentenceStatus { get; set; }

         IDurationOfTime sentenceSuspension { get; set; }

    }
}
