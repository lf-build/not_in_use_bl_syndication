using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class DefenseAttorneyList : IDefenseAttorneyList
    {
        public DefenseAttorneyList(Proxy.Response.PersonReport.DefenseAttorneyList defenseAttorneyList)
        {
            if(defenseAttorneyList!=null)
            {
                typeOfDefenseUsed = defenseAttorneyList.TypeOfDefenseUsed;
                attorneyInfo      =new AttorneyInfo(defenseAttorneyList.AttorneyInfo);
                attorneyAddress   =new Address(defenseAttorneyList.AttorneyAddress);
            }
           
        }

        public string typeOfDefenseUsed { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAttorneyInfo, AttorneyInfo>))]
        public IAttorneyInfo attorneyInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress attorneyAddress { get; set; }
    }
}
