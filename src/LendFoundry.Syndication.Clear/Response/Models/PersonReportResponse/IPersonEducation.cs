namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonEducation
    {
         string institution { get; set; }

         string graduationDate { get; set; }
    }
}
