namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class BailInfo : IBailInfo
    {
        public BailInfo(Proxy.Response.PersonReport.BailInfo bailInfo)
        {
            if(bailInfo!=null)
            {
                bailPosted = bailInfo.BailPosted;
                bailerName = bailInfo.BailerName;
                bailSet = bailInfo.BailSet;
                dateBailPosted = bailInfo.DateBailPosted;
                dateBailSet = bailInfo.DateBailSet;
            }
        }


        public string bailerName{ get; set; }

        public string bailPosted{ get; set; }

        public string bailSet{ get; set; }

        public string dateBailPosted{ get; set; }

        public string dateBailSet{ get; set; }
    }
}
