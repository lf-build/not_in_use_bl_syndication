namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PriorChargesInfo : IPriorChargesInfo
    {
        public PriorChargesInfo(Proxy.Response.PersonReport.PriorChargesInfo priorChargesInfo)
        {
            if (priorChargesInfo != null)
            {
                priorIncarcerationCount = priorChargesInfo.PriorIncarcerationCount;
                priorIncarcerationDate  = priorChargesInfo.PriorIncarcerationDate;
                priorLocation           = priorChargesInfo.PriorLocation;
                priorStatus             = priorChargesInfo.PriorStatus;
                priorOffense            = priorChargesInfo.PriorOffense;
            }
        }

        public string priorIncarcerationCount { get; set; }
        public string priorIncarcerationDate { get; set; }
        public string priorLocation { get; set; }
        public string priorStatus { get; set; }
        public string priorOffense { get; set; }
    }
}
