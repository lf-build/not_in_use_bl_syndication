using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SentencingInfo : ISentencingInfo
    {
        public SentencingInfo(Proxy.Response.PersonReport.SentencingInfo sentencingInfo)
        {
            if(sentencingInfo!=null)
            {
                communityService = new CommunityService(sentencingInfo.CommunityService);
                consecutiveConcurrent = sentencingInfo.ConsecutiveConcurrent;
                licenseSuspension = new DurationOfTime(sentencingInfo.LicenseSuspension);
                maximumSentencing = new DurationOfTime(sentencingInfo.MaximumSentencing);
                minimumSentencing = new DurationOfTime(sentencingInfo.MinimumSentencing);
                sentence = new Sentence(sentencingInfo.Sentence);
                sentenceBeginDate = sentencingInfo.SentenceBeginDate;
                sentenceCompleted = sentencingInfo.SentenceCompleted;
                sentenceCounty = sentencingInfo.SentenceCounty;
                sentenceDescription = sentencingInfo.SentenceDescription;
                sentenceEffectiveDate = sentencingInfo.SentenceEffectiveDate;
                sentenceExpirationDate = sentencingInfo.SentenceExpirationDate;
                sentenceImposedDate = sentencingInfo.SentenceImposedDate;
                sentenceModifiedDate = sentencingInfo.SentenceModifiedDate;
                sentencePardoned = sentencingInfo.SentencePardoned;
                sentenceStatus = sentencingInfo.SentenceStatus;
                sentenceSuspension = new DurationOfTime(sentencingInfo.SentenceSuspension);
            }
        }
        [JsonConverter(typeof(InterfaceConverter<ICommunityService, CommunityService>))]
        public ICommunityService communityService { get; set; }
        public string consecutiveConcurrent { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime licenseSuspension { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime maximumSentencing { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime minimumSentencing { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISentence, Sentence>))]
        public ISentence sentence { get; set; }
        public string sentenceBeginDate { get; set; }
        public string sentenceCompleted { get; set; }
        public string sentenceCounty { get; set; }
        public string sentenceDescription { get; set; }
        public string sentenceEffectiveDate { get; set; }
        public string sentenceExpirationDate { get; set; }
        public string sentenceImposedDate { get; set; }
        public string sentenceModifiedDate { get; set; }
        public string sentencePardoned { get; set; }
        public string sentenceStatus { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDurationOfTime, DurationOfTime>))]
        public IDurationOfTime sentenceSuspension { get; set; }
    }
}
