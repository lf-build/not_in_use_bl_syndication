﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public  interface IDebtor
    {
         string debtorOwedAmount { get; set; }

         string debtorOccupation { get; set; }

         string numberOfDebtors { get; set; }

         IPartyInfo partyInfo { get; set; }

         string debtorDescription { get; set; }

        List<string> debtorAmount { get; set; }

        List<IObligationInfo> obligationInfo { get; set; }

         ICaseDisposition dispositionInfo { get; set; }

        List<string> demandAmount { get; set; }

         string status { get; set; }

         IDebtorSignerInfo debtorSignerInfo { get; set; }

         string dUNSNumber { get; set; }

         string taxID { get; set; }

        List<string> typeOfDebtor { get; set; }

        List<string> typeOfDebtorName { get; set; }

        List<ISSNInfo> sSNInfo { get; set; }
    }
}
