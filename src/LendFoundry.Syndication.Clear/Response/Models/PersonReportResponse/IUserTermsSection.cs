﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IUserTermsSection
    {
        IUserTermRecord userTermRecord { get; set; }
    }
}