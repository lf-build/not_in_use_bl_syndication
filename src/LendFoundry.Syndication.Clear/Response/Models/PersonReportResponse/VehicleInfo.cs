namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class VehicleInfo : IVehicleInfo
    {
        public VehicleInfo(Proxy.Response.PersonReport.VehicleInfo vehicleInfo)
        {
            if (vehicleInfo != null)
            {
                make         = vehicleInfo.Make;
                model        = vehicleInfo.Model;
                modelYear    = vehicleInfo.ModelYear;
                plateNumber  = vehicleInfo.PlateNumber;
                plateState   = vehicleInfo.PlateState;
                vehicleColor = vehicleInfo.VehicleColor;
            }
        }

        public string make { get; set; }
        public string model { get; set; }
        public string modelYear { get; set; }
        public string plateNumber { get; set; }
        public string plateState { get; set; }
        public string vehicleColor { get; set; }
    }
}
