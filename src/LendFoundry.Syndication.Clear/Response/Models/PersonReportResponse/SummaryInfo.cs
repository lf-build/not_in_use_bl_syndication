﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SummaryInfo: ISummaryInfo
    {
        public SummaryInfo(Proxy.Response.PersonReport.SummaryInfo summaryInfo)
        {
            if (summaryInfo!=null)
            {
                name = summaryInfo.Name;

                state = summaryInfo.State;

                date = summaryInfo.Date;

                type = summaryInfo.Type;

                classSummaryInfo = summaryInfo.Class;
             }
        }
        public string name { get; set; }

        public string state { get; set; }

        public string date { get; set; }

        public string type { get; set; }

        public string classSummaryInfo { get; set; }
}
}
