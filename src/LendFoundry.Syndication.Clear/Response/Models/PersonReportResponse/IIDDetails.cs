namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IIDDetails
    {
         string stateID { get; set; }

         string fullStateID { get; set; }

         string iDType { get; set; }

         string iDNumber { get; set; }

         string iDIssuer { get; set; }
    }
}
