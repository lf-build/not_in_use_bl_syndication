﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AddressInfo : IAddressInfo
    {
        public AddressInfo(Proxy.Response.PersonReport.AddressInfo addressInfo)
        {
            if (addressInfo!=null)
            {
                address = new Address(addressInfo.Address);
                phone = addressInfo.Phone.Select(a=>new PhoneInfo(a)).ToList<IPhoneInfo>();
                sourceInfo = addressInfo.SourceInfo.Select(a=>new SourceInfo(a)).ToList<ISourceInfo>();
                personName = addressInfo.PersonName.Select(a=>new PersonName(a)).ToList<IPersonName>();
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneInfo, PhoneInfo>))]
        public List<IPhoneInfo> phone { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISourceInfo, SourceInfo>))]
        public List<ISourceInfo> sourceInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPersonName, PersonName>))]
        public List<IPersonName> personName { get; set; }
    }
}
