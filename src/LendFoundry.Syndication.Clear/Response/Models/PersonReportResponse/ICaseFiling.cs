namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ICaseFiling
    {
         string fileNumber { get; set; }

         string filedDate { get; set; }
    }
}
