﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonReportDetailsSectionResults: IPersonReportDetailsSectionResults
    {
        public PersonReportDetailsSectionResults(Proxy.Response.PersonReport.SectionResults personReportDetailsSectionResults)
        {
            if (personReportDetailsSectionResults!=null)
            {
                sectionName = personReportDetailsSectionResults.SectionName;
                clearReportDescription = personReportDetailsSectionResults.ClearReportDescription;
                sectionStatus = personReportDetailsSectionResults.SectionStatus;
                sectionRecordCount = personReportDetailsSectionResults.SectionRecordCount;
                sectionDetails = new SectionDetails(personReportDetailsSectionResults.SectionDetails, sectionName);

            }
        }
        public string sectionName { get; set; }

        public string clearReportDescription { get; set; }

        public string sectionStatus { get; set; }

        public int sectionRecordCount { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISectionDetails, SectionDetails>))]
        public ISectionDetails sectionDetails { get; set; }
    }
}
