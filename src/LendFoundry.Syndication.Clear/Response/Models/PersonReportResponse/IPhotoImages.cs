﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPhotoImages
    {
        List<IImageInfo> images { get; set; }
        List<string> photoURL { get; set; }
    }
}