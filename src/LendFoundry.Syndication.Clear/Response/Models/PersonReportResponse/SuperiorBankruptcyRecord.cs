using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class SuperiorBankruptcyRecord : ISuperiorBankruptcyRecord
    {
        public SuperiorBankruptcyRecord(Proxy.Response.PersonReport.SuperiorBankruptcyRecord superiorBankruptcyRecord)
        {
            if (superiorBankruptcyRecord != null)
            {
                debtor       = superiorBankruptcyRecord.Debtor.Select(a=>new Debtor(a)).ToList<IDebtor>();
                filingInfo   = new FilingInfo1(superiorBankruptcyRecord.FilingInfo);
                judge        = superiorBankruptcyRecord.Judge;
                otherParty   = superiorBankruptcyRecord.OtherParty.Select(a => new PartyInfo(a)).ToList<IPartyInfo>();
                scheduled341 = superiorBankruptcyRecord.Scheduled341.Select(a => new Scheduled341(a)).ToList<IScheduled341>();
                trustee      = superiorBankruptcyRecord.Trustee.Select(a => new Trustee(a)).ToList<ITrustee>();
                caseCategory = superiorBankruptcyRecord.CaseCategory;
                caseTitle    = superiorBankruptcyRecord.CaseTitle;
                source       = superiorBankruptcyRecord.Source;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IDebtor, Debtor>))]
        public List<IDebtor> debtor { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IFilingInfo1, FilingInfo1>))]
        public IFilingInfo1 filingInfo { get; set; }
        public List<string> judge { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPartyInfo, PartyInfo>))]
        public List<IPartyInfo> otherParty { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IScheduled341, Scheduled341>))]
        public List<IScheduled341> scheduled341 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ITrustee, Trustee>))]
        public List<ITrustee> trustee { get; set; }
        public List<string> caseCategory { get; set; }
        public string caseTitle { get; set; }
        public string source { get; set; }
    }
}
