using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class AgencyInfo : IAgencyInfo
    {
        public AgencyInfo(Proxy.Response.PersonReport.AgencyInfo agencyInfo)
        {
            if(agencyInfo!=null)
            {
                agencyName = agencyInfo.AgencyName;
                arrestReason = agencyInfo.ArrestReason;
                typeOfAgency = agencyInfo.TypeOfAgency;
                reportedDate = agencyInfo.ReportedDate;
                address = new Address(agencyInfo.Address);
                phone = new PhoneInfo(agencyInfo.Phone);
            }
        }
        public string agencyName { get; set; }
        public string arrestReason { get; set; }
        public string typeOfAgency { get; set; }
        public string reportedDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress address { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phone { get; set; }
    }
}
