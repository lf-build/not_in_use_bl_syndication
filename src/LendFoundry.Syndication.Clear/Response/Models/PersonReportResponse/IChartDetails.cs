namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IChartDetails
    {
         IPersonName personName { get; set; }

         IRiskFlags riskFlags { get; set; }
    }
}
