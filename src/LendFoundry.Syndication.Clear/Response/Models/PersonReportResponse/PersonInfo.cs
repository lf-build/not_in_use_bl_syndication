﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonInfo : IPersonInfo
    {
        public PersonInfo(Proxy.Response.PersonReport.PersonInfo personInfo)
        {
            if (personInfo != null)
            {
                 personName = new PersonName(personInfo.PersonName);
                personProfile = new PersonProfile(personInfo.PersonProfile);
                personSSN = new SSNInfo(personInfo.PersonSSN);
            }
        }




        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName { get; set; }

     
        [JsonConverter(typeof(InterfaceConverter<IPersonProfile, PersonProfile>))]
        public IPersonProfile personProfile { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISSNInfo, SSNInfo>))]
        public ISSNInfo personSSN { get; set; }

    }
}
