using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ArrestRecord : IArrestRecord
    {

        public ArrestRecord(Proxy.Response.PersonReport.ArrestRecord arrestRecord)
        {
            if (arrestRecord != null)
            {
                address = arrestRecord.Address.Select(a => new Address(a)).ToList<IAddress>();
                arrestInfo = arrestRecord.ArrestInfo.Select(a => new ArrestInfo(a)).ToList<IArrestInfo>();
                courtInfo = arrestRecord.CourtInfo.Select(a => new CourtInfo(a)).ToList<ICourtInfo>();
                criminalCharges = arrestRecord.CriminalCharges.Select(a => new CriminalCharges(a)).ToList<ICriminalCharges>();
                iDInfo = new IDInfo(arrestRecord.IDInfo);
                institutionInfo = arrestRecord.InstitutionInfo.Select(a => new InstitutionInfo(a)).ToList<IInstitutionInfo>();
                personInfo = new PersonInfo3(arrestRecord.PersonInfo);
                source = arrestRecord.Source;
                photoImages = arrestRecord.PhotoImages.Select(a => new PhotoImages1(a)).ToList<IPhotoImages1>();
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IArrestInfo, ArrestInfo>))]
        public List<IArrestInfo> arrestInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICourtInfo, CourtInfo>))]
        public List<ICourtInfo> courtInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ICriminalCharges, CriminalCharges>))]
        public List<ICriminalCharges> criminalCharges { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IIDInfo, IDInfo>))]
        public IIDInfo iDInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IInstitutionInfo, InstitutionInfo>))]
        public List<IInstitutionInfo> institutionInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonInfo3, PersonInfo3>))]
        public IPersonInfo3 personInfo { get; set; }
        public string source { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhotoImages1, PhotoImages1>))]
        public List<IPhotoImages1> photoImages { get; set; }
    }
}
