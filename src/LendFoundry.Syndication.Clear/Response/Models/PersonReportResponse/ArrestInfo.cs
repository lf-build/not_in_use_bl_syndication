using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ArrestInfo : IArrestInfo
    {
        public ArrestInfo(Proxy.Response.PersonReport.ArrestInfo arrestInfo)
        {
            if (arrestInfo != null)
            {
                agreementDate     = arrestInfo.AgreementDate;
                arrestAgencyInfo = arrestInfo.ArrestAgencyInfo.Select(a => new AgencyInfo(a)).ToList<IAgencyInfo>();
                arrestCancelDate  = arrestInfo.ArrestCancelDate;
                arrestDate        = arrestInfo.ArrestDate;
                arrestLocation    = arrestInfo.ArrestLocation;
                arrestDisposition = arrestInfo.ArrestDisposition;
                arrestNum         = arrestInfo.ArrestNum;
                arrestWarrantFlag = arrestInfo.ArrestWarrantFlag;
                bookingInfo       = new BookingInfo(arrestInfo.BookingInfo);
                countyOfCrime     = arrestInfo.CountyOfCrime;
                timeOfArrest      = arrestInfo.TimeOfArrest;
                nameUsedAtArrest  = arrestInfo.NameUsedAtArrest;
                arrestCounty      = arrestInfo.ArrestCounty;
            }
        }

        public string agreementDate { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAgencyInfo, AgencyInfo>))]
        public List<IAgencyInfo> arrestAgencyInfo { get; set; }

        public string arrestCancelDate { get; set; }

        public List<string> arrestDate { get; set; }

        public string arrestLocation { get; set; }

        public string arrestDisposition { get; set; }

        public string arrestNum { get; set; }

        public string arrestWarrantFlag { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBookingInfo, BookingInfo>))]
        public IBookingInfo bookingInfo { get; set; }

        public string countyOfCrime { get; set; }

        public string timeOfArrest { get; set; }

        public string nameUsedAtArrest { get; set; }

        public string arrestCounty { get; set; }
    }
}
