namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class InmateReleaseInfo : IInmateReleaseInfo
    {
        public InmateReleaseInfo(Proxy.Response.PersonReport.InmateReleaseInfo inmateReleaseInfo)
        {
            if(inmateReleaseInfo!=null)
            {
                actualReleaseDate = inmateReleaseInfo.ActualReleaseDate;
                firearmRestriction = inmateReleaseInfo.FirearmRestriction;
                maximumReleaseDate = inmateReleaseInfo.MaximumReleaseDate;
                projectedReleaseDate = inmateReleaseInfo.ProjectedReleaseDate;
                inmateReleaseReason = inmateReleaseInfo.InmateReleaseReason;
                inmateReleaseTime = inmateReleaseInfo.InmateReleaseTime;
                tentativeReleaseDate = inmateReleaseInfo.TentativeReleaseDate;
            }
        }
        public string actualReleaseDate { get; set; }
        public string firearmRestriction { get; set; }
        public string maximumReleaseDate { get; set; }
        public string projectedReleaseDate { get; set; }
        public string inmateReleaseReason { get; set; }
        public string inmateReleaseTime { get; set; }
        public string tentativeReleaseDate { get; set; }
    }
}
