namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IOfficerInfo
    {
         string officerBadge { get; set; }

         string officerName { get; set; }
    }
}
