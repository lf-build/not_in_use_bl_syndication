﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FilingOffice: IFilingOffice
    {
        public FilingOffice(Proxy.Response.PersonReport.FilingOffice filingOffice)
        {
            if (filingOffice != null)
            {
                filingOfficeName = filingOffice.FilingOfficeName;
                address = filingOffice.Address.Select(a=>new Address(a)).ToList<IAddress>();
            }
        }

        public List<string> filingOfficeName {get; set;}

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> address {get; set;}
    }
}
