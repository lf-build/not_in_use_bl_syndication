﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class MarriageDivorceSummary : IMarriageDivorceSummary
    {
        public MarriageDivorceSummary(Proxy.Response.PersonReport.MarriageDivorceSummary marriageDivorceSummary)
        {
            if (marriageDivorceSummary!=null)
            {
                marriageInfo = marriageDivorceSummary.MarriageInfo.Select(m => new SummaryInfo(m)).ToList<ISummaryInfo>();
                divorceInfo = marriageDivorceSummary.DivorceInfo.Select(m => new SummaryInfo(m)).ToList<ISummaryInfo>();
            }
         

        }
        [JsonConverter(typeof(InterfaceListConverter<ISummaryInfo, ISummaryInfo>))]
        public List<ISummaryInfo> marriageInfo { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISummaryInfo, SummaryInfo>))]
        public List<ISummaryInfo> divorceInfo { get; set; }
    }
}
