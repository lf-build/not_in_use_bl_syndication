﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonReportDetailsSectionResults
    {
        string clearReportDescription { get; set; }
        ISectionDetails sectionDetails { get; set; }
        string sectionName { get; set; }
        int sectionRecordCount { get; set; }
        string sectionStatus { get; set; }
    }
}