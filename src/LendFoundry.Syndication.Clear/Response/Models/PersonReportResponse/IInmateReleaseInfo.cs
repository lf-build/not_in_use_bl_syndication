namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IInmateReleaseInfo
    {
         string actualReleaseDate{ get; set; }

         string firearmRestriction{ get; set; }

         string maximumReleaseDate{ get; set; }

         string projectedReleaseDate{ get; set; }

         string inmateReleaseReason{ get; set; }

         string inmateReleaseTime{ get; set; }

         string tentativeReleaseDate{ get; set; }
    }
}
