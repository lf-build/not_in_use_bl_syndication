namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IVehicleInfo
    {
         string make { get; set; }

         string model { get; set; }

         string modelYear { get; set; }

         string plateNumber { get; set; }

         string plateState { get; set; }

         string vehicleColor { get; set; }
    }
}
