﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
   public interface IAttorneyInfo
    {
         string attorneyBarNumber { get; set; }

         string attorneyCOR { get; set; }

         string attorneyEmail { get; set; }

         string attornyForeignRegistration { get; set; }

         string attorneyFTS { get; set; }

         string attorneyID { get; set; }

         string attorneyLicense { get; set; }

         List<string> attorneyName { get; set; }

         string attorneyPhone { get; set; }

         string attorneyFax { get; set; }

         List<IAddress> attorneyAddress { get; set; }

         string attorneyRepresents { get; set; }

         string attorneyRole { get; set; }

         string attorneyStatus { get; set; }

         string attorneyTerminatedDate { get; set; }

         string typeofAttorney { get; set; }

         string firmName { get; set; }

         string firmStatus { get; set; }
    }
}
