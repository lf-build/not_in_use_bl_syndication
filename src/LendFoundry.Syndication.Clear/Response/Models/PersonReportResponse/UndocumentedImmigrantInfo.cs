namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class UndocumentedImmigrantInfo : IUndocumentedImmigrantInfo
    {
        public UndocumentedImmigrantInfo(Proxy.Response.PersonReport.UndocumentedImmigrantInfo undocumentedImmigrantInfo)
        {
            if(undocumentedImmigrantInfo!=null)
            {
                undocumentedImmigrantName = undocumentedImmigrantInfo.UndocumentedImmigrantName;
                undocumentedImmigrantID = undocumentedImmigrantInfo.UndocumentedImmigrantID;
            }
        }
        public string undocumentedImmigrantName { get; set; }
        public string undocumentedImmigrantID { get; set; }
    }
}
