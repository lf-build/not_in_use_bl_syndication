using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class FilingInfo1 : IFilingInfo1
    {
        public FilingInfo1(Proxy.Response.PersonReport.FilingInfo1 filingInfo)
        {
            if (filingInfo != null)
            {
                filingNumberInfo         = filingInfo.FilingNumberInfo.Select(a=> new FilingNumberInfo(a)).ToList<IFilingNumberInfo>();
                filingOffice             = filingInfo.FilingOffice.Select(a=> new FilingOffice(a)).ToList<IFilingOffice>();
                typeofFiling             = filingInfo.TypeofFiling;
                unlawfulDetainer         = filingInfo.UnlawfulDetainer;
                fileDate                 = filingInfo.FileDate;
                filingOfficeAddress      = filingInfo.FilingOfficeAddress.Select(a=>new Address(a)).ToList<IAddress>();
                releaseDate              = filingInfo.ReleaseDate;
                certificateNumber        = filingInfo.CertificateNumber;
                hiddenFilingNumber       = filingInfo.HiddenFilingNumber;
                iRSSerialNumber          = filingInfo.IRSSerialNumber;
                orginalFilingNumber      = filingInfo.OrginalFilingNumber;
                originalBook             = filingInfo.OriginalBook;
                originalPage             = filingInfo.OriginalPage;
                court                    = filingInfo.Court;
                controlNumber            = filingInfo.ControlNumber;
                courtCounty              = filingInfo.CourtCounty;
                collectionDate           = filingInfo.CollectionDate;
                complaintDate            = filingInfo.ComplaintDate;
                federalLienNumber        = filingInfo.FederalLienNumber;
                filingOfficeDUNSNumber   = filingInfo.FilingOfficeDUNSNumber;
                maturityDate             = filingInfo.MaturityDate;
                multipleDebtors          = filingInfo.MultipleDebtors;
                originalFilingDate       = filingInfo.OriginalFilingDate;
                paragraph                = filingInfo.Paragraph;
                perfectedDate            = filingInfo.PerfectedDate;
                vendorNumber             = filingInfo.VendorNumber;
                dispositionInfo          = new CaseDisposition(filingInfo.DispositionInfo);
                filingTypeInfo           = filingInfo.FilingTypeInfo.Select(a=>new FilingTypeInfo(a)).ToList<IFilingTypeInfo>();
                caseDetails              = filingInfo.CaseDetails;
                convertDate              = filingInfo.ConvertDate;
                demandAmount             = filingInfo.DemandAmount;
                dischargeDate            = filingInfo.DischargeDate;
                dismissalDate            = filingInfo.DismissalDate;
                filingChapter            = filingInfo.FilingChapter;
                filingDistrict           = filingInfo.FilingDistrict;
                filingState              = filingInfo.FilingState;
                filingStatusFlag         = filingInfo.FilingStatusFlag;
                filingStatusTime         = filingInfo.FilingStatusTime;
                finalDecreeDate          = filingInfo.FinalDecreeDate;
                keyNatureOfSuit          = filingInfo.KeyNatureOfSuit;
                keyNatureOfSuitCode      = filingInfo.KeyNatureOfSuitCode;
                otherDockets             = filingInfo.OtherDockets;
                otherDocketsTitle        = filingInfo.OtherDocketsTitle;
                reopenedDate             = filingInfo.ReopenedDate;
                reterminatedDate         = filingInfo.ReterminatedDate;
                statusSetBy              = filingInfo.StatusSetBy;
                terminatedDate           = filingInfo.TerminatedDate;
                caseDispositionFinalDate = filingInfo.CaseDispositionFinalDate;
                documentDescription      = filingInfo.DocumentDescription;
                documentFormat           = filingInfo.DocumentFormat;
                documentFiledDate        = filingInfo.DocumentFiledDate;
                documentID               = filingInfo.DocumentID;
                documentStatus           = filingInfo.DocumentStatus;
                statusDate               = filingInfo.StatusDate;
            }
        }

        [JsonConverter(typeof(InterfaceListConverter<IFilingNumberInfo, FilingNumberInfo>))]
        public List<IFilingNumberInfo> filingNumberInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFilingOffice, FilingOffice>))]
        public List<IFilingOffice> filingOffice { get; set; }
        public List<string> typeofFiling { get; set; }
        public List<string> unlawfulDetainer { get; set; }
        public List<string> fileDate { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> filingOfficeAddress { get; set; }
        public List<string> releaseDate { get; set; }
        public List<string> certificateNumber { get; set; }
        public List<string> hiddenFilingNumber { get; set; }
        public List<string> iRSSerialNumber { get; set; }
        public List<string> orginalFilingNumber { get; set; }
        public List<string> originalBook { get; set; }
        public List<string> originalPage { get; set; }
        public string court { get; set; }
        public string controlNumber { get; set; }
        public string courtCounty { get; set; }
        public string collectionDate { get; set; }
        public string complaintDate { get; set; }
        public string federalLienNumber { get; set; }
        public string filingOfficeDUNSNumber { get; set; }
        public string maturityDate { get; set; }
        public string multipleDebtors { get; set; }
        public string originalFilingDate { get; set; }
        public string paragraph { get; set; }
        public string perfectedDate { get; set; }
        public string vendorNumber { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ICaseDisposition, CaseDisposition>))]
        public ICaseDisposition dispositionInfo { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFilingTypeInfo, FilingTypeInfo>))]
        public List<IFilingTypeInfo> filingTypeInfo { get; set; }
        public string caseDetails { get; set; }
        public string convertDate { get; set; }
        public string demandAmount { get; set; }
        public string dischargeDate { get; set; }
        public string dismissalDate { get; set; }
        public string filingChapter { get; set; }
        public string filingDistrict { get; set; }
        public string filingState { get; set; }
        public string filingStatusFlag { get; set; }
        public string filingStatusTime { get; set; }
        public string finalDecreeDate { get; set; }
        public string keyNatureOfSuit { get; set; }
        public string keyNatureOfSuitCode { get; set; }
        public string otherDockets { get; set; }
        public string otherDocketsTitle { get; set; }
        public string reopenedDate { get; set; }
        public string reterminatedDate { get; set; }
        public string statusSetBy { get; set; }
        public string terminatedDate { get; set; }
        public string caseDispositionFinalDate { get; set; }
        public string documentDescription { get; set; }
        public string documentFormat { get; set; }
        public string documentFiledDate { get; set; }
        public string documentID { get; set; }
        public string documentStatus { get; set; }
        public string statusDate { get; set; }
    }
}
