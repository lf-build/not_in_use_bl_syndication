namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PersonEducation : IPersonEducation
    {
        public PersonEducation(Proxy.Response.PersonReport.PersonEducation personEducation)
        {
            if (personEducation != null)
            {
                institution = personEducation.Institution;
                graduationDate = personEducation.GraduationDate;
            }
        }

        public string institution { get; set; }
        public string graduationDate { get; set; }
    }
}
