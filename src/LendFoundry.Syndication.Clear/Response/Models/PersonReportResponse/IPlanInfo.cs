using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPlanInfo
    {
         List<IPlan> plan { get; set; }

         List<string> planDetails { get; set; }
    }
}
