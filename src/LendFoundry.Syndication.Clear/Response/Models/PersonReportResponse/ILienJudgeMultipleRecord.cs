﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ILienJudgeMultipleRecord
    {
         List<ICreditor> creditor{ get; set; }

         List<IDebtor> debtor{ get; set; }

         List<ISubjudgments> subjudgments{ get; set; }

         List<IJudgmentInfo> judgmentInfo{ get; set; }

         List<ILienInfo> lienInfo{ get; set; }

         List<ICommentInfo> commentInfo{ get; set; }

         string numberofSubjudgments{ get; set; }

         List<IFilingInfo1> lienJudgeFilingInfo{ get; set; }

         string source{ get; set; }
    }
}
