﻿

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class ObligationInfo:IObligationInfo
    {
        public ObligationInfo(Proxy.Response.PersonReport.ObligationInfo obligationInfo)
        {
            if (obligationInfo != null)
            {
                obligation            = obligationInfo.Obligation;
                obligationDescription = obligationInfo.ObligationDescription;
                owedTotalAmount       = obligationInfo.OwedTotalAmount;
                totalObligation       = obligationInfo.TotalObligation;
            }
        }

        public string obligation { get; set; }

        public string obligationDescription { get; set; }

        public string owedTotalAmount { get; set; }

        public string totalObligation { get; set; }

    }
}
