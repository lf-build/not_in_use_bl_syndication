﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class WorkAffiliations : IWorkAffiliations
    {
        public WorkAffiliations(Proxy.Response.PersonReport.WorkAffiliations workAffiliations)
        {
            if (workAffiliations!=null)
            {
                businessName = workAffiliations.BusinessName;
                title = workAffiliations.Title;
                emailAddress = workAffiliations.EmailAddress;
            }
            
        }
        public string businessName { get; set; }

        public string title { get; set; }

        public string emailAddress { get; set; }

    }
}
