﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISubjectRecord
    {
        IDeathSummary deathSummary { get; set; }
        List<IDriverLicenseInfo> driverLicenseInfo { get; set; }
        ILicenseSummary licenseSummary { get; set; }
        IMarriageDivorceSummary marriageDivorceSummary { get; set; }
        List<IPersonInfo> personAKAInfo { get; set; }
        IPersonInfo personInfo { get; set; }
        IPhotoImages photoImages { get; set; }
        List<IWorkAffiliations> workAffiliations { get; set; }
    }
}