﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class UtilityRecord: IUtilityRecord
    {
        public UtilityRecord(Proxy.Response.PersonReport.UtilityRecord utilityRecord)
        {
            if (utilityRecord!=null)
            {
                personName = new PersonName(utilityRecord.PersonName);
                billingAddress = new Address(utilityRecord.BillingAddress);
                driverLicenseInfo = new DriverLicenseInfo(utilityRecord.DriverLicenseInfo);
                sSNInfo = new SSNInfo(utilityRecord.SSNInfo);
                phoneInfo = new PhoneInfo(utilityRecord.PhoneInfo);
                serviceConnectDate = utilityRecord.ServiceConnectDate;
                utilityAddress = new Address(utilityRecord.UtilityAddress);
                utilityReportedDate = utilityRecord.UtilityReportedDate;
                utilityService = utilityRecord.UtilityService;
                source = utilityRecord.Source;
            }
        }
        [JsonConverter(typeof(InterfaceConverter<IPersonName, PersonName>))]
        public IPersonName personName {get;set;}

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress billingAddress {get;set;}

        [JsonConverter(typeof(InterfaceConverter<IDriverLicenseInfo, DriverLicenseInfo>))]
        public IDriverLicenseInfo driverLicenseInfo {get;set;}

        [JsonConverter(typeof(InterfaceConverter<ISSNInfo, SSNInfo>))]
        public ISSNInfo sSNInfo {get;set;}

        [JsonConverter(typeof(InterfaceConverter<IPhoneInfo, PhoneInfo>))]
        public IPhoneInfo phoneInfo {get;set;}

        public string serviceConnectDate {get;set;}

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress utilityAddress {get;set;}

        public string utilityReportedDate {get;set;}

        public List<string> utilityService {get;set;}

        public string source {get;set;}
    }
}
