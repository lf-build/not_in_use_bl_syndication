namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPleaInfo
    {
         string changedPlea { get; set; }

         string originalPlea { get; set; }

         string pleaDate { get; set; }

         string pleaWithdrawDate { get; set; }
    }
}
