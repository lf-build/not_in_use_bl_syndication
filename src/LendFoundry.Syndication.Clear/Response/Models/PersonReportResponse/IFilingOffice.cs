﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IFilingOffice
    {
         List<string> filingOfficeName {get; set;}

         List<IAddress> address {get; set;}
    }
}
