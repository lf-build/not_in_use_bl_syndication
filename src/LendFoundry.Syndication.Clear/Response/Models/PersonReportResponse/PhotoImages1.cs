namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class PhotoImages1 : IPhotoImages1
    {
        public PhotoImages1(Proxy.Response.PersonReport.PhotoImages1 photoImages1)
        {
            binaryImage = photoImages1.BinaryImage;
            contentType = photoImages1.ContentType;
        }

        public string binaryImage { get; set; }
        public string contentType { get; set; }
    }
}
