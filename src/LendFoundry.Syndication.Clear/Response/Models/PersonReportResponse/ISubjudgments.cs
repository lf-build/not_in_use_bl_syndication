﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface ISubjudgments
    {
         List<ILienInfo> lienInfo{ get; set; }

         List<IJudgmentInfo> judgmentInfo{ get; set; }

         List<ICommentInfo> commentInfo{ get; set; }

         List<IDebtor> debtor{ get; set; }

         List<ICreditor> creditor{ get; set; }
    }
}
