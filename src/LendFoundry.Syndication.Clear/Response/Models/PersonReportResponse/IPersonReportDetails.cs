﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IPersonReportDetails
    {
         IStatus Status { get; set; }
         List<IPersonReportDetailsSectionResults> SectionResults { get; set; }
    }
}