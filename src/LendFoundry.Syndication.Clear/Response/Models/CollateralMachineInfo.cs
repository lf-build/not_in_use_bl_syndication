﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class CollateralMachineInfo : ICollateralMachineInfo
    {
        public CollateralMachineInfo()
        {

        }
        public string typeOfMachine { get; set; }

        public string typeOfSecondaryMachine { get; set; }

        public string manufacturer { get; set; }

        public string manufacturerYear { get; set; }

        public string modelDescription { get; set; }

        public string model { get; set; }

        public string modelYear { get; set; }

        public string modelStatus { get; set; }

        public string quantity { get; set; }

        public string serialNumber { get; set; }
    }
}
