﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonDocumentSearch
{
    public interface IAddressesPhones
    {
        IAddress address { get; set; }
        string addressType { get; set; }
        string country { get; set; }
        string firstReportedDate { get; set; }
        string latitude { get; set; }
        string longitude { get; set; }
        string phoneNumber1 { get; set; }
        string phoneNumber2 { get; set; }
        string reportedDate { get; set; }
    }
}