﻿

namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public class DebtorSignerInfo: IDebtorSignerInfo
    {
        public DebtorSignerInfo(Proxy.Response.PersonReport.DebtorSignerInfo debtorSignerInfo)
        {
            if (debtorSignerInfo != null)
            {
                signer            = debtorSignerInfo.Signer;
                signerBusiness    = debtorSignerInfo.SignerBusiness;
                signerDescription = debtorSignerInfo.SignerDescription;
                signerTitle       = debtorSignerInfo.SignerTitle;
            }
        }

        public string signer {get; set; }

        public string signerBusiness {get; set; }

        public string signerDescription {get; set; }

        public string signerTitle {get; set; }
    }
}
