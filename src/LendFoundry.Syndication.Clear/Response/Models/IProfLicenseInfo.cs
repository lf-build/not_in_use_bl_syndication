﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IProfLicenseInfo
    {
        string boardCertification { get; set; }
        string certificationBoard { get; set; }
        string licenseAgency { get; set; }
        string licenseDescription { get; set; }
        string[] licenseNumber { get; set; }
        string[] licenseSpecialty { get; set; }
        string licenseState { get; set; }
        string[] licenseStatus { get; set; }
        string[] typeofLicense { get; set; }
    }
}