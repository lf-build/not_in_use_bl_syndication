﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface ISICInfo
    {
        string nAICSCode { get; set; }
        string nAICSDesc { get; set; }
        string sICCode { get; set; }
        string sICDesc { get; set; }
        string sICExt { get; set; }
    }
}