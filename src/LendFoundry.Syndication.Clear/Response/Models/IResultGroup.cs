﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public interface IResultGroup
    {
         string GroupId { get; set; }
         string RecordCount { get; set; }
         string Relevance { get; set; }
        IDominantValues DominantValues { get; set; }
        IRecordDetails RecordDetails { get; set; }
    }
}
