﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class FilingOfficeStmt 
    {
        public FilingOfficeStmt()
        {

        }
        public string accountStmt {get; set;}

        public string crossRefFileNumber {get; set;}

        public string crossReFileNumberFull {get; set;}

        public string inaccuracyStmt {get; set;}
    }
}
