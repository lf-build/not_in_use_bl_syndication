﻿namespace LendFoundry.Syndication.Clear.Response.Models
{
    public class UCCFilingInfo : IUCCFilingInfo
    {
        public FilingStmtInfo[] filingStmtInfo { get; set; }

        public UCCPartyInfo[] debtor { get; set; }

        public UCCPartyInfo[] securedParty { get; set; }

        public UCCPartyInfo[] assignee { get; set; }

        public UCCPartyInfo[] assignor { get; set; }

        public RealEstateInfo realEstateInfo { get; set; }

      //  public ICollateralInfo[] collateralInfo { get; set; }

        public FilingOfficeStmt filingOfficeStmt { get; set; }

        public RelatedFilingInfo[] relatedFilingInfo { get; set; }
    }
}
