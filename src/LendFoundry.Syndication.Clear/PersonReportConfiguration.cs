﻿namespace LendFoundry.Syndication.Clear
{
    public class PersonReportConfiguration : IPersonReportConfiguration
    {

        public string AssociatesSectionAddressOption { get; set; } = "3";
        public string AssociatesSectionTimeframeOption { get; set; } = "2";
        public string AssociateSectionDisplayOption { get; set; } = "F";
        public string NeighborsTypeOption { get; set; } = "3";
        public string NeighborsTimeframeOption { get; set; } = "2";
        public string RelativesSectionDegreeOption { get; set; } = "1";
        public string RelativesSectionDisplayOption { get; set; } = "F";
        public bool VehiclesSectionLimitAssocOption { get; set; } = false;
        public bool VehiclesSectionLimitYearOption { get; set; } = false;
        public string AddressSectionAddressOption { get; set; } = "A";
        public string BusinessAtSubjectAddressSectionAddressOption { get; set; } = "3";
        public string LicenseSectionAddressOption { get; set; } = "3";
        public string LicenseSectionTimeframeOption { get; set; } = "0";
        public string PropertyOwnerSectionAddressOption { get; set; } = "3";
        public string VehiclesAtSubjectAddressSectionAddressOption { get; set; } = "3";
        public string VehiclesAtSubjectAddressSectionTimeframeOption { get; set; } = "0";
        public bool PropertyOwnerSectionLimitAssocOption { get; set; } = false;


            public bool AddressSection { get; set; } = true;
            public bool DeathSection { get; set; } = true;
        public bool WorkAffiliationSection { get; set; } = true;
        public bool UtilitySection { get; set; } = true;
        public bool SSNAddressFraudSection { get; set; } = true;
        public bool OtherSSNSection { get; set; } = true;
        public bool OtherNamesforSSNSection { get; set; } = true;
        public bool PhoneNumberSection { get; set; } = true;
        public bool PhoneListingSection { get; set; } = true;
        public bool CanadianPhoneSection { get; set; } = true;
        public bool EmailSection { get; set; } = true;
        public bool DriverLicenseSection { get; set; } = true;
        public bool DivorceSection { get; set; } = true;
        public bool LicenseSection { get; set; } = true;
        public bool HealthcareLicenseSection { get; set; } = true;
        public bool NPISection { get; set; } = true;
        public bool MilitarySection { get; set; } = true;
        public bool PoliticalDonorSection { get; set; } = true;
        public bool VoterRegistrationSection { get; set; } = true;
        public bool DriversAtSubjectAddressSection { get; set; } = true;
        public bool GlobalSanctionSection { get; set; } = true;
        public bool HealthcareSanctionSection { get; set; } = true;
        public bool ExcludedPartySection { get; set; } = true;
        public bool WorldCheckSection { get; set; } = true;
        public bool InfractionSection { get; set; } = true;
        public bool CriminalSection { get; set; } = true;
        public bool RealTimeArrestSection { get; set; } = true;
        public bool ArrestSection { get; set; } = true;
        public bool ExecutiveAffiliationSection { get; set; } = true;
        public bool DunBradstreetSection { get; set; } = true;
        public bool ShareholderSection { get; set; } = true;
        public bool BusinessAtSubjectAddressSection { get; set; } = true;
        public bool LienJudgmentSection { get; set; } = true;
        public bool BankruptcySection { get; set; } = true;
        public bool LawsuitSection { get; set; } = true;
        public bool DocketSection { get; set; } = true;
        public bool CorporateSection { get; set; } = true;
        public bool UCCSection { get; set; } = true;
        public bool RealPropertySection { get; set; } = true;
        public bool PropertyOwnerSection { get; set; } = true;
        public bool PreForeclosureSection { get; set; } = true;
        public bool RealTimeVehicleSection { get; set; } = true;
        public bool VehicleSection { get; set; } = true;
        public bool VehiclesAtSubjectAddressSection { get; set; } = true;
        public bool WatercraftSection { get; set; } = true;
        public bool AircraftSection { get; set; } = true;
        public bool UnclaimedAssetSection { get; set; } = true;
        public bool RelativeSection { get; set; } = true;
        public bool AssociateSection { get; set; } = true;
        public bool NeighborSection { get; set; } = true;
        public bool QuickAnalysisFlagSection { get; set; } = true;
        public bool AssociateAnalyticsChartSection { get; set; } = true;


        public string ReportChoice { get; set; } = "Individual";

        public string Reference { get; set; }

    }


}

