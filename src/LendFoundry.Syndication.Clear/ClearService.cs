﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Clear.Events;
using LendFoundry.Syndication.Clear.Proxy;
using LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse;
using LendFoundry.Syndication.Clear.Request;
using LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.Clear.Response.Models.PersonSearch;
using LendFoundry.Syndication.Clear.Response.Models.BusinessSearch;

namespace LendFoundry.Syndication.Clear
{
    public class ClearService : IClearService
    { 
        public ClearService(IClearConfiguration configuration, IClearProxy proxy, ILookupService lookup, IEventHubClient eventHub,ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            Configuration = configuration;
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            
        }
        public IClearConfiguration Configuration { get; }
        public IClearProxy  Proxy { get; }
        public IEventHubClient EventHub { get; }
        public ILookupService Lookup { get; }
        public ILogger Logger { get; set; }
        
        public async Task<IPersonSearchResponse> SearchPerson(string entityType, string entityId, IPersonSearchRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            
            entityType = EnsureEntityType(entityType);

            try
            {
                var response = await Proxy.SearchPerson(request);
                var result = new PersonSearchResponse(response);
                var referenceNumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new ClearPersonSearchPerformed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = request,
                    ReferenceNumber = referenceNumber
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new ClearPersonSearchFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw new Exception(exception.Message);
            }
        }

        public async Task<IPersonReportDetails> GetPersonReport(string entityType, string entityId,string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
                var response = await Proxy.GetPersonReport(groupId);
                var result = new PersonReportDetails(response);
                var referenceNumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new ClearPersonReportFetched
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = groupId,
                    ReferenceNumber = referenceNumber
                });
                return result;

            }
            catch (Exception exception)
            {
                await EventHub.Publish(new ClearPersonReportFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = groupId,
                    ReferenceNumber = null
                });
                throw new Exception(exception.Message);
            }
        }


        public async Task<IBusinessSearchResponse> SearchBusiness(string entityType, string entityId, IBusinessSearchRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(entityType))
                throw new ArgumentException("EnityType is require", nameof(entityType));
            if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
                var response = await Proxy.SearchBusiness(request);
                var result = new BusinessSearchResponse(response);
                var referenceNumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new ClearBusinessSearchPerformed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = response,
                    Request = request,
                    ReferenceNumber = referenceNumber
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new ClearBusinessSearchFailed
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = request,
                    ReferenceNumber = null
                });
                throw new Exception(exception.Message);
            }
        }
        public async Task<IBusinessReportDetails> GetBusinessReport(string entityType, string entityId, string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));
                if (string.IsNullOrEmpty(entityId))
                throw new ArgumentException("EntityId is require", nameof(entityId));
            entityType = EnsureEntityType(entityType);
            try
            {
                var response = await Proxy.GetBusinessReport(groupId);
                var result = new BusinessReportDetails(response);
                var referenceNumber = Guid.NewGuid().ToString("N");
                await EventHub.Publish(new ClearBusinessReportFetched
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = groupId,
                    ReferenceNumber = referenceNumber
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new ClearBusinessReportFailled
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = groupId,
                    ReferenceNumber = null
                });
                throw new Exception(exception.Message);
            }
        }

        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
