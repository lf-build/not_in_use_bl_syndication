﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Clear.Events
{
    public class ClearPersonSearchFailed : SyndicationCalledEvent
    {
    }
}
