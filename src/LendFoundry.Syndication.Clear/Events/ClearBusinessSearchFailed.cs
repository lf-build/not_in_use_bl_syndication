﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Clear.Events
{
    public class ClearBusinessSearchFailed : SyndicationCalledEvent
    {
    }
}
