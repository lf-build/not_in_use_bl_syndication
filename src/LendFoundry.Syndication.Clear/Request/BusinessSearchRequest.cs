﻿namespace LendFoundry.Syndication.Clear.Request
{
    public class BusinessSearchRequest: IBusinessSearchRequest
    {
        public string Reference { get; set; }
        public string BusinessName { get; set; }
        public string CorporationId { get; set; }
        public string FEIN { get; set; }
        public string DUNSNumber { get; set; }
        public string LastSecondaryNameSoundSimilarOption { get; set; }
        public string SecondaryLastNameOption { get; set; }
        public string FirstNameSoundSimilarOption { get; set; }
        public string FirstNameVariationsOption { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string StreetNamesSoundSimilarOption { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FilingNumber { get; set; }
        public string FilingDate { get; set; }
        public string MiddleInitial { get; set; }
        public string SecondaryLastName { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string NPINumber { get; set; }

    }
}
