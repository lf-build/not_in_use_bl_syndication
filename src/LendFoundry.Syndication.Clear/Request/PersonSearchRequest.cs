﻿namespace LendFoundry.Syndication.Clear.Request
{
    public class PersonSearchRequest: IPersonSearchRequest
    {
        public string Reference { get; set; }
        public string LastSecondaryNameSoundSimilarOption { get; set; }
        public string SecondaryLastNameOption { get; set; }
        public string FirstNameBeginsWithOption { get; set; }
        public string FirstNameSoundSimilarOption { get; set; }
        public string FirstNameExactMatchOption { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondaryLastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string SSN { get; set; }
        public string PhoneNumber { get; set; }
        public string PersonBirthDate { get; set; }
        public string DriverLicenseNumber { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Province { get; set; }
        public string PersonAgeTo { get; set; }
        public string PersonAgeFrom { get; set; }
        public string EmailAddress { get; set; }
        public string NPINumber { get; set; }
        public string MiddleInitial { get; set; }
        public string PersonEntityId { get; set; }

    }
}
