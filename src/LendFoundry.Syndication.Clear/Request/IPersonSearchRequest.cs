﻿namespace LendFoundry.Syndication.Clear.Request
{
    public interface IPersonSearchRequest
    {
        string City { get; set; }
        string DriverLicenseNumber { get; set; }
        string FirstName { get; set; }
        string FirstNameBeginsWithOption { get; set; }
        string FirstNameExactMatchOption { get; set; }
        string FirstNameSoundSimilarOption { get; set; }
        string LastName { get; set; }
        string LastSecondaryNameSoundSimilarOption { get; set; }
        string PersonBirthDate { get; set; }
        string PhoneNumber { get; set; }
        string Reference { get; set; }
        string SecondaryLastName { get; set; }
        string SecondaryLastNameOption { get; set; }
        string SSN { get; set; }
        string State { get; set; }
        string Street { get; set; }
        string Country { get; set; }
        string ZipCode { get; set; }
        string Province { get; set; }
        string PersonAgeTo { get; set; }
        string PersonAgeFrom { get; set; }
        string EmailAddress { get; set; }
        string NPINumber { get; set; }
        string MiddleInitial { get; set; }
        string PersonEntityId { get; set; }

    }
}