﻿namespace LendFoundry.Syndication.Clear.Request
{
    public interface IBusinessSearchRequest
    {
         string Reference { get; set; }
         string BusinessName { get; set; }
         string CorporationId { get; set; }
         string FEIN { get; set; }
         string DUNSNumber { get; set; }
         string LastSecondaryNameSoundSimilarOption { get; set; }
         string SecondaryLastNameOption { get; set; }
         string FirstNameSoundSimilarOption { get; set; }
         string FirstNameVariationsOption { get; set; }
         string LastName { get; set; }
         string FirstName { get; set; }
         string StreetNamesSoundSimilarOption { get; set; }
         string Street { get; set; }
         string City { get; set; }
         string State { get; set; }
         string ZipCode { get; set; }
         string PhoneNumber { get; set; }
         string FilingNumber { get; set; }
        string FilingDate { get; set; }
         string MiddleInitial { get; set; }
        string SecondaryLastName { get; set; }
        string Province { get; set; }
        string Country { get; set; }
         string NPINumber { get; set; }


    }
}