﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IAdmissionInfo
    {
        string admissionDate { get; set; }
        string institutionLocation { get; set; }
        string typeOfAdmission { get; set; }
    }
}