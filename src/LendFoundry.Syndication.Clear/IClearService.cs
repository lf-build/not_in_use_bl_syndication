﻿using LendFoundry.Syndication.Clear.Request;
using System.Threading.Tasks;
using LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult;
using LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse;
using LendFoundry.Syndication.Clear.Response.Models.PersonSearch;
using LendFoundry.Syndication.Clear.Response.Models.BusinessSearch;

namespace LendFoundry.Syndication.Clear
{
    public interface IClearService
    {
        Task<IPersonReportDetails> GetPersonReport(string entityType, string entityId,string groupId);
        Task<IPersonSearchResponse> SearchPerson(string entityType, string entityId, IPersonSearchRequest request);
        Task<IBusinessSearchResponse> SearchBusiness(string entityType, string entityId, IBusinessSearchRequest request);
        Task<IBusinessReportDetails> GetBusinessReport(string entityType, string entityId, string groupId);


    }
}
