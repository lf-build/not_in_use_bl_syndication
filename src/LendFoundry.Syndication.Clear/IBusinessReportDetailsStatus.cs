﻿namespace LendFoundry.Syndication.Clear.Response.Models.BusinessReportResult
{
    public interface IBusinessReportDetailsStatus
    {
        string reference { get; set; }
        int statusCode { get; set; }
        int subStatusCode { get; set; }
    }
}