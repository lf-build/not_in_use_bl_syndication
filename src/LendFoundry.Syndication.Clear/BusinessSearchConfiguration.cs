﻿namespace LendFoundry.Syndication.Clear
{
    public class BusinessSearchConfiguration: IBusinessSearchConfiguration
    {
        public bool PublicRecordBusiness { get; set; } = true;
        public bool NPIRecord { get; set; } = true;
        public bool PublicRecordUCCFilings { get; set; } = true;
        public bool WorldCheckRiskIntelligence { get; set; } = true;
    }
}
