﻿namespace LendFoundry.Syndication.Clear
{
    public class PersonSearchConfiguration: IPersonSearchConfiguration
    {
        public bool NPIRecord { get; set; }
        public bool PublicRecordPeople { get; set; }
        public bool RealTimeIncarcerationAndArrests { get; set; }
        public bool WorldCheckRiskIntelligence { get; set; }
    }
}
