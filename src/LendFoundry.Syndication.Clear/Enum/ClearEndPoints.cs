﻿namespace LendFoundry.Syndication.Clear.Enum
{
    public enum ClearEndPoints
    {
        PersonSearch = 1,
        PersonReport = 2,
        BusinessSearch = 3,
        BusinessReport = 4
    }
}
