﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Clear.Proxy.Response
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0", IsNullable = false)]
    public partial class CourtResultsPage
    {

        public string startIndexField;

        public string endIndexField;

        public CourtResultsPageStatus statusField;

        public List<CourtResultsPageResultGroup> resultGroupField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StartIndex
        {
            get
            {
                return this.startIndexField;
            }
            set
            {
                this.startIndexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EndIndex
        {
            get
            {
                return this.endIndexField;
            }
            set
            {
                this.endIndexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CourtResultsPageStatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public List<CourtResultsPageResultGroup> ResultGroup
        {
            get
            {
                return this.resultGroupField;
            }
            set
            {
                this.resultGroupField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CourtResultsPageStatus
    {

        public string statusCodeField;

        public string subStatusCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string SubStatusCode
        {
            get
            {
                return this.subStatusCodeField;
            }
            set
            {
                this.subStatusCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingTypeInfo
    {

        public string actionDescriptionField;

        public string fileTypeDescriptionField;

        public string taxDescriptionField;

        public string typeofTaxField;

        public string fileTypeField;

        public string[] typeOfActionField;

        /// <remarks/>
        public string ActionDescription
        {
            get
            {
                return this.actionDescriptionField;
            }
            set
            {
                this.actionDescriptionField = value;
            }
        }

        /// <remarks/>
        public string FileTypeDescription
        {
            get
            {
                return this.fileTypeDescriptionField;
            }
            set
            {
                this.fileTypeDescriptionField = value;
            }
        }

        /// <remarks/>
        public string TaxDescription
        {
            get
            {
                return this.taxDescriptionField;
            }
            set
            {
                this.taxDescriptionField = value;
            }
        }

        /// <remarks/>
        public string TypeofTax
        {
            get
            {
                return this.typeofTaxField;
            }
            set
            {
                this.typeofTaxField = value;
            }
        }

        /// <remarks/>
        public string FileType
        {
            get
            {
                return this.fileTypeField;
            }
            set
            {
                this.fileTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TypeOfAction")]
        public string[] TypeOfAction
        {
            get
            {
                return this.typeOfActionField;
            }
            set
            {
                this.typeOfActionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingOffice
    {

        public string[] filingOfficeNameField;

        public Address[] addressField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingOfficeName")]
        public string[] FilingOfficeName
        {
            get
            {
                return this.filingOfficeNameField;
            }
            set
            {
                this.filingOfficeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Address")]
        public Address[] Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class Address
    {

        public string streetField;

        public string[] streetLineField;

        public string streetLine1Field;

        public string streetLine2Field;

        public string streetLine3Field;

        public string pOBoxField;

        public string careOfField;

        public string cityField;

        public string stateField;

        public string stateAbbreviationField;

        public string provinceField;

        public string zipCodeField;

        public string zipCodeExtensionField;

        public string countyField;

        public string countryField;

        public string continentField;

        public string regionField;

        public string fullAddressField;

        public string typeOfAddressField;

        public string additionalInfoField;

        public string lastVerifiedDateField;

        public string reportedDateField;

        public string latitudeField;

        public string longitudeField;

        /// <remarks/>
        public string Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("StreetLine")]
        public string[] StreetLine
        {
            get
            {
                return this.streetLineField;
            }
            set
            {
                this.streetLineField = value;
            }
        }

        /// <remarks/>
        public string StreetLine1
        {
            get
            {
                return this.streetLine1Field;
            }
            set
            {
                this.streetLine1Field = value;
            }
        }

        /// <remarks/>
        public string StreetLine2
        {
            get
            {
                return this.streetLine2Field;
            }
            set
            {
                this.streetLine2Field = value;
            }
        }

        /// <remarks/>
        public string StreetLine3
        {
            get
            {
                return this.streetLine3Field;
            }
            set
            {
                this.streetLine3Field = value;
            }
        }

        /// <remarks/>
        public string POBox
        {
            get
            {
                return this.pOBoxField;
            }
            set
            {
                this.pOBoxField = value;
            }
        }

        /// <remarks/>
        public string CareOf
        {
            get
            {
                return this.careOfField;
            }
            set
            {
                this.careOfField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        public string StateAbbreviation
        {
            get
            {
                return this.stateAbbreviationField;
            }
            set
            {
                this.stateAbbreviationField = value;
            }
        }

        /// <remarks/>
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>
        public string ZipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }

        /// <remarks/>
        public string ZipCodeExtension
        {
            get
            {
                return this.zipCodeExtensionField;
            }
            set
            {
                this.zipCodeExtensionField = value;
            }
        }

        /// <remarks/>
        public string County
        {
            get
            {
                return this.countyField;
            }
            set
            {
                this.countyField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string Continent
        {
            get
            {
                return this.continentField;
            }
            set
            {
                this.continentField = value;
            }
        }

        /// <remarks/>
        public string Region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }

        /// <remarks/>
        public string FullAddress
        {
            get
            {
                return this.fullAddressField;
            }
            set
            {
                this.fullAddressField = value;
            }
        }

        /// <remarks/>
        public string TypeOfAddress
        {
            get
            {
                return this.typeOfAddressField;
            }
            set
            {
                this.typeOfAddressField = value;
            }
        }

        /// <remarks/>
        public string AdditionalInfo
        {
            get
            {
                return this.additionalInfoField;
            }
            set
            {
                this.additionalInfoField = value;
            }
        }

        /// <remarks/>
        public string LastVerifiedDate
        {
            get
            {
                return this.lastVerifiedDateField;
            }
            set
            {
                this.lastVerifiedDateField = value;
            }
        }

        /// <remarks/>
        public string ReportedDate
        {
            get
            {
                return this.reportedDateField;
            }
            set
            {
                this.reportedDateField = value;
            }
        }

        /// <remarks/>
        public string Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public string Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingNumberInfo
    {

        public string[] filingNumberField;

        public string[] pageNumberField;

        public string[] volumeNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingNumber")]
        public string[] FilingNumber
        {
            get
            {
                return this.filingNumberField;
            }
            set
            {
                this.filingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PageNumber")]
        public string[] PageNumber
        {
            get
            {
                return this.pageNumberField;
            }
            set
            {
                this.pageNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("VolumeNumber")]
        public string[] VolumeNumber
        {
            get
            {
                return this.volumeNumberField;
            }
            set
            {
                this.volumeNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingInfo
    {

        public FilingNumberInfo[] filingNumberInfoField;

        public FilingOffice[] filingOfficeField;

        public string[] typeofFilingField;

        public string[] unlawfulDetainerField;

        public string[] fileDateField;

        public Address[] filingOfficeAddressField;

        public string[] releaseDateField;

        public string[] certificateNumberField;

        public string[] hiddenFilingNumberField;

        public string[] iRSSerialNumberField;

        public string[] orginalFilingNumberField;

        public string[] originalBookField;

        public string[] originalPageField;

        public string courtField;

        public string controlNumberField;

        public string courtCountyField;

        public string collectionDateField;

        public string complaintDateField;

        public string federalLienNumberField;

        public string filingOfficeDUNSNumberField;

        public string maturityDateField;

        public string multipleDebtorsField;

        public string originalFilingDateField;

        public string paragraphField;

        public string perfectedDateField;

        public string vendorNumberField;

        public CaseDisposition dispositionInfoField;

        public FilingTypeInfo[] filingTypeInfoField;

        public string caseDetailsField;

        public string convertDateField;

        public string demandAmountField;

        public string dischargeDateField;

        public string dismissalDateField;

        public string filingChapterField;

        public string filingDistrictField;

        public string filingStateField;

        public string filingStatusFlagField;

        public string filingStatusTimeField;

        public string finalDecreeDateField;

        public string keyNatureOfSuitField;

        public string keyNatureOfSuitCodeField;

        public string otherDocketsField;

        public string otherDocketsTitleField;

        public string reopenedDateField;

        public string reterminatedDateField;

        public string statusSetByField;

        public string terminatedDateField;

        public string caseDispositionFinalDateField;

        public string documentDescriptionField;

        public string documentFormatField;

        public string documentFiledDateField;

        public string documentIDField;

        public string documentStatusField;

        public string statusDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingNumberInfo")]
        public FilingNumberInfo[] FilingNumberInfo
        {
            get
            {
                return this.filingNumberInfoField;
            }
            set
            {
                this.filingNumberInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingOffice")]
        public FilingOffice[] FilingOffice
        {
            get
            {
                return this.filingOfficeField;
            }
            set
            {
                this.filingOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TypeofFiling")]
        public string[] TypeofFiling
        {
            get
            {
                return this.typeofFilingField;
            }
            set
            {
                this.typeofFilingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("UnlawfulDetainer")]
        public string[] UnlawfulDetainer
        {
            get
            {
                return this.unlawfulDetainerField;
            }
            set
            {
                this.unlawfulDetainerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FileDate")]
        public string[] FileDate
        {
            get
            {
                return this.fileDateField;
            }
            set
            {
                this.fileDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingOfficeAddress")]
        public Address[] FilingOfficeAddress
        {
            get
            {
                return this.filingOfficeAddressField;
            }
            set
            {
                this.filingOfficeAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ReleaseDate")]
        public string[] ReleaseDate
        {
            get
            {
                return this.releaseDateField;
            }
            set
            {
                this.releaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CertificateNumber")]
        public string[] CertificateNumber
        {
            get
            {
                return this.certificateNumberField;
            }
            set
            {
                this.certificateNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("HiddenFilingNumber")]
        public string[] HiddenFilingNumber
        {
            get
            {
                return this.hiddenFilingNumberField;
            }
            set
            {
                this.hiddenFilingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("IRSSerialNumber")]
        public string[] IRSSerialNumber
        {
            get
            {
                return this.iRSSerialNumberField;
            }
            set
            {
                this.iRSSerialNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OrginalFilingNumber")]
        public string[] OrginalFilingNumber
        {
            get
            {
                return this.orginalFilingNumberField;
            }
            set
            {
                this.orginalFilingNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OriginalBook")]
        public string[] OriginalBook
        {
            get
            {
                return this.originalBookField;
            }
            set
            {
                this.originalBookField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OriginalPage")]
        public string[] OriginalPage
        {
            get
            {
                return this.originalPageField;
            }
            set
            {
                this.originalPageField = value;
            }
        }

        /// <remarks/>
        public string Court
        {
            get
            {
                return this.courtField;
            }
            set
            {
                this.courtField = value;
            }
        }

        /// <remarks/>
        public string ControlNumber
        {
            get
            {
                return this.controlNumberField;
            }
            set
            {
                this.controlNumberField = value;
            }
        }

        /// <remarks/>
        public string CourtCounty
        {
            get
            {
                return this.courtCountyField;
            }
            set
            {
                this.courtCountyField = value;
            }
        }

        /// <remarks/>
        public string CollectionDate
        {
            get
            {
                return this.collectionDateField;
            }
            set
            {
                this.collectionDateField = value;
            }
        }

        /// <remarks/>
        public string ComplaintDate
        {
            get
            {
                return this.complaintDateField;
            }
            set
            {
                this.complaintDateField = value;
            }
        }

        /// <remarks/>
        public string FederalLienNumber
        {
            get
            {
                return this.federalLienNumberField;
            }
            set
            {
                this.federalLienNumberField = value;
            }
        }

        /// <remarks/>
        public string FilingOfficeDUNSNumber
        {
            get
            {
                return this.filingOfficeDUNSNumberField;
            }
            set
            {
                this.filingOfficeDUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string MaturityDate
        {
            get
            {
                return this.maturityDateField;
            }
            set
            {
                this.maturityDateField = value;
            }
        }

        /// <remarks/>
        public string MultipleDebtors
        {
            get
            {
                return this.multipleDebtorsField;
            }
            set
            {
                this.multipleDebtorsField = value;
            }
        }

        /// <remarks/>
        public string OriginalFilingDate
        {
            get
            {
                return this.originalFilingDateField;
            }
            set
            {
                this.originalFilingDateField = value;
            }
        }

        /// <remarks/>
        public string Paragraph
        {
            get
            {
                return this.paragraphField;
            }
            set
            {
                this.paragraphField = value;
            }
        }

        /// <remarks/>
        public string PerfectedDate
        {
            get
            {
                return this.perfectedDateField;
            }
            set
            {
                this.perfectedDateField = value;
            }
        }

        /// <remarks/>
        public string VendorNumber
        {
            get
            {
                return this.vendorNumberField;
            }
            set
            {
                this.vendorNumberField = value;
            }
        }

        /// <remarks/>
        public CaseDisposition DispositionInfo
        {
            get
            {
                return this.dispositionInfoField;
            }
            set
            {
                this.dispositionInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingTypeInfo")]
        public FilingTypeInfo[] FilingTypeInfo
        {
            get
            {
                return this.filingTypeInfoField;
            }
            set
            {
                this.filingTypeInfoField = value;
            }
        }

        /// <remarks/>
        public string CaseDetails
        {
            get
            {
                return this.caseDetailsField;
            }
            set
            {
                this.caseDetailsField = value;
            }
        }

        /// <remarks/>
        public string ConvertDate
        {
            get
            {
                return this.convertDateField;
            }
            set
            {
                this.convertDateField = value;
            }
        }

        /// <remarks/>
        public string DemandAmount
        {
            get
            {
                return this.demandAmountField;
            }
            set
            {
                this.demandAmountField = value;
            }
        }

        /// <remarks/>
        public string DischargeDate
        {
            get
            {
                return this.dischargeDateField;
            }
            set
            {
                this.dischargeDateField = value;
            }
        }

        /// <remarks/>
        public string DismissalDate
        {
            get
            {
                return this.dismissalDateField;
            }
            set
            {
                this.dismissalDateField = value;
            }
        }

        /// <remarks/>
        public string FilingChapter
        {
            get
            {
                return this.filingChapterField;
            }
            set
            {
                this.filingChapterField = value;
            }
        }

        /// <remarks/>
        public string FilingDistrict
        {
            get
            {
                return this.filingDistrictField;
            }
            set
            {
                this.filingDistrictField = value;
            }
        }

        /// <remarks/>
        public string FilingState
        {
            get
            {
                return this.filingStateField;
            }
            set
            {
                this.filingStateField = value;
            }
        }

        /// <remarks/>
        public string FilingStatusFlag
        {
            get
            {
                return this.filingStatusFlagField;
            }
            set
            {
                this.filingStatusFlagField = value;
            }
        }

        /// <remarks/>
        public string FilingStatusTime
        {
            get
            {
                return this.filingStatusTimeField;
            }
            set
            {
                this.filingStatusTimeField = value;
            }
        }

        /// <remarks/>
        public string FinalDecreeDate
        {
            get
            {
                return this.finalDecreeDateField;
            }
            set
            {
                this.finalDecreeDateField = value;
            }
        }

        /// <remarks/>
        public string KeyNatureOfSuit
        {
            get
            {
                return this.keyNatureOfSuitField;
            }
            set
            {
                this.keyNatureOfSuitField = value;
            }
        }

        /// <remarks/>
        public string KeyNatureOfSuitCode
        {
            get
            {
                return this.keyNatureOfSuitCodeField;
            }
            set
            {
                this.keyNatureOfSuitCodeField = value;
            }
        }

        /// <remarks/>
        public string OtherDockets
        {
            get
            {
                return this.otherDocketsField;
            }
            set
            {
                this.otherDocketsField = value;
            }
        }

        /// <remarks/>
        public string OtherDocketsTitle
        {
            get
            {
                return this.otherDocketsTitleField;
            }
            set
            {
                this.otherDocketsTitleField = value;
            }
        }

        /// <remarks/>
        public string ReopenedDate
        {
            get
            {
                return this.reopenedDateField;
            }
            set
            {
                this.reopenedDateField = value;
            }
        }

        /// <remarks/>
        public string ReterminatedDate
        {
            get
            {
                return this.reterminatedDateField;
            }
            set
            {
                this.reterminatedDateField = value;
            }
        }

        /// <remarks/>
        public string StatusSetBy
        {
            get
            {
                return this.statusSetByField;
            }
            set
            {
                this.statusSetByField = value;
            }
        }

        /// <remarks/>
        public string TerminatedDate
        {
            get
            {
                return this.terminatedDateField;
            }
            set
            {
                this.terminatedDateField = value;
            }
        }

        /// <remarks/>
        public string CaseDispositionFinalDate
        {
            get
            {
                return this.caseDispositionFinalDateField;
            }
            set
            {
                this.caseDispositionFinalDateField = value;
            }
        }

        /// <remarks/>
        public string DocumentDescription
        {
            get
            {
                return this.documentDescriptionField;
            }
            set
            {
                this.documentDescriptionField = value;
            }
        }

        /// <remarks/>
        public string DocumentFormat
        {
            get
            {
                return this.documentFormatField;
            }
            set
            {
                this.documentFormatField = value;
            }
        }

        /// <remarks/>
        public string DocumentFiledDate
        {
            get
            {
                return this.documentFiledDateField;
            }
            set
            {
                this.documentFiledDateField = value;
            }
        }

        /// <remarks/>
        public string DocumentID
        {
            get
            {
                return this.documentIDField;
            }
            set
            {
                this.documentIDField = value;
            }
        }

        /// <remarks/>
        public string DocumentStatus
        {
            get
            {
                return this.documentStatusField;
            }
            set
            {
                this.documentStatusField = value;
            }
        }

        /// <remarks/>
        public string StatusDate
        {
            get
            {
                return this.statusDateField;
            }
            set
            {
                this.statusDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CaseDisposition
    {

        public string decisionCategoryField;

        public string decisionField;

        public string finalDateField;

        public string dispositionField;

        public string dispositionDateField;

        /// <remarks/>
        public string DecisionCategory
        {
            get
            {
                return this.decisionCategoryField;
            }
            set
            {
                this.decisionCategoryField = value;
            }
        }

        /// <remarks/>
        public string Decision
        {
            get
            {
                return this.decisionField;
            }
            set
            {
                this.decisionField = value;
            }
        }

        /// <remarks/>
        public string FinalDate
        {
            get
            {
                return this.finalDateField;
            }
            set
            {
                this.finalDateField = value;
            }
        }

        /// <remarks/>
        public string Disposition
        {
            get
            {
                return this.dispositionField;
            }
            set
            {
                this.dispositionField = value;
            }
        }

        /// <remarks/>
        public string DispositionDate
        {
            get
            {
                return this.dispositionDateField;
            }
            set
            {
                this.dispositionDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CommentSource
    {

        public string sourceBusinessNameField;

        public string sourceNameField;

        public string sourceTitleField;

        public string sourceTitle2Field;

        public string sourceTitle3Field;

        public string statusInfoField;

        public string sourceTitleDescriptionField;

        /// <remarks/>
        public string SourceBusinessName
        {
            get
            {
                return this.sourceBusinessNameField;
            }
            set
            {
                this.sourceBusinessNameField = value;
            }
        }

        /// <remarks/>
        public string SourceName
        {
            get
            {
                return this.sourceNameField;
            }
            set
            {
                this.sourceNameField = value;
            }
        }

        /// <remarks/>
        public string SourceTitle
        {
            get
            {
                return this.sourceTitleField;
            }
            set
            {
                this.sourceTitleField = value;
            }
        }

        /// <remarks/>
        public string SourceTitle2
        {
            get
            {
                return this.sourceTitle2Field;
            }
            set
            {
                this.sourceTitle2Field = value;
            }
        }

        /// <remarks/>
        public string SourceTitle3
        {
            get
            {
                return this.sourceTitle3Field;
            }
            set
            {
                this.sourceTitle3Field = value;
            }
        }

        /// <remarks/>
        public string StatusInfo
        {
            get
            {
                return this.statusInfoField;
            }
            set
            {
                this.statusInfoField = value;
            }
        }

        /// <remarks/>
        public string SourceTitleDescription
        {
            get
            {
                return this.sourceTitleDescriptionField;
            }
            set
            {
                this.sourceTitleDescriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CommentInfo
    {

        public string[] commentField;

        public string commentLine2Field;

        public string[] commentDateField;

        public string[] commentDescriptionField;

        public CommentSource[] commentSourceField;

        public string commentCodeField;

        public string commentTitleField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comment")]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string CommentLine2
        {
            get
            {
                return this.commentLine2Field;
            }
            set
            {
                this.commentLine2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommentDate")]
        public string[] CommentDate
        {
            get
            {
                return this.commentDateField;
            }
            set
            {
                this.commentDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommentDescription")]
        public string[] CommentDescription
        {
            get
            {
                return this.commentDescriptionField;
            }
            set
            {
                this.commentDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommentSource")]
        public CommentSource[] CommentSource
        {
            get
            {
                return this.commentSourceField;
            }
            set
            {
                this.commentSourceField = value;
            }
        }

        /// <remarks/>
        public string CommentCode
        {
            get
            {
                return this.commentCodeField;
            }
            set
            {
                this.commentCodeField = value;
            }
        }

        /// <remarks/>
        public string CommentTitle
        {
            get
            {
                return this.commentTitleField;
            }
            set
            {
                this.commentTitleField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class JudgmentInfo
    {

        public string awardAmountField;

        public string judgementPrincipalAmountField;

        public string paidOnCreditAmountField;

        public string judgmentTotalAmountField;

        public ObligationInfo[] obligationInfoField;

        public StatusInfo statusInfoField;

        public SubjudgmentInfo subjudgmentInfoField;

        /// <remarks/>
        public string AwardAmount
        {
            get
            {
                return this.awardAmountField;
            }
            set
            {
                this.awardAmountField = value;
            }
        }

        /// <remarks/>
        public string JudgementPrincipalAmount
        {
            get
            {
                return this.judgementPrincipalAmountField;
            }
            set
            {
                this.judgementPrincipalAmountField = value;
            }
        }

        /// <remarks/>
        public string PaidOnCreditAmount
        {
            get
            {
                return this.paidOnCreditAmountField;
            }
            set
            {
                this.paidOnCreditAmountField = value;
            }
        }

        /// <remarks/>
        public string JudgmentTotalAmount
        {
            get
            {
                return this.judgmentTotalAmountField;
            }
            set
            {
                this.judgmentTotalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ObligationInfo")]
        public ObligationInfo[] ObligationInfo
        {
            get
            {
                return this.obligationInfoField;
            }
            set
            {
                this.obligationInfoField = value;
            }
        }

        /// <remarks/>
        public StatusInfo StatusInfo
        {
            get
            {
                return this.statusInfoField;
            }
            set
            {
                this.statusInfoField = value;
            }
        }

        /// <remarks/>
        public SubjudgmentInfo SubjudgmentInfo
        {
            get
            {
                return this.subjudgmentInfoField;
            }
            set
            {
                this.subjudgmentInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class ObligationInfo
    {

        public string obligationField;

        public string obligationDescriptionField;

        public string owedTotalAmountField;

        public string totalObligationField;

        /// <remarks/>
        public string Obligation
        {
            get
            {
                return this.obligationField;
            }
            set
            {
                this.obligationField = value;
            }
        }

        /// <remarks/>
        public string ObligationDescription
        {
            get
            {
                return this.obligationDescriptionField;
            }
            set
            {
                this.obligationDescriptionField = value;
            }
        }

        /// <remarks/>
        public string OwedTotalAmount
        {
            get
            {
                return this.owedTotalAmountField;
            }
            set
            {
                this.owedTotalAmountField = value;
            }
        }

        /// <remarks/>
        public string TotalObligation
        {
            get
            {
                return this.totalObligationField;
            }
            set
            {
                this.totalObligationField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class StatusInfo
    {

        public string statusField;

        public string statusDateField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string StatusDate
        {
            get
            {
                return this.statusDateField;
            }
            set
            {
                this.statusDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class SubjudgmentInfo
    {

        public string subjudgmentFilingDateField;

        public string[] commentField;

        public CaseDisposition dispositionInfoField;

        public StatusInfo statusInfoField;

        /// <remarks/>
        public string SubjudgmentFilingDate
        {
            get
            {
                return this.subjudgmentFilingDateField;
            }
            set
            {
                this.subjudgmentFilingDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comment")]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public CaseDisposition DispositionInfo
        {
            get
            {
                return this.dispositionInfoField;
            }
            set
            {
                this.dispositionInfoField = value;
            }
        }

        /// <remarks/>
        public StatusInfo StatusInfo
        {
            get
            {
                return this.statusInfoField;
            }
            set
            {
                this.statusInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class LienInfo
    {

        public string debtLotField;

        public string debtNumberField;

        public string judgmentAmountField;

        public string satisfactionDateField;

        public string typeofSatisfactionField;

        public string sheriffExecutionRequiredField;

        public string[] expirationDateField;

        public string creditorAmountField;

        public string lienAmountField;

        public string lienPrincipalAmountField;

        public string taxAmountField;

        public string lienTotalAmountField;

        public string totalAmountPaidField;

        public string totalCalculatedAmountField;

        public string totalCalculatedAmountPaidField;

        public StatusInfo statusInfoField;

        public SubjudgmentInfo subjudgmentInfoField;

        /// <remarks/>
        public string DebtLot
        {
            get
            {
                return this.debtLotField;
            }
            set
            {
                this.debtLotField = value;
            }
        }

        /// <remarks/>
        public string DebtNumber
        {
            get
            {
                return this.debtNumberField;
            }
            set
            {
                this.debtNumberField = value;
            }
        }

        /// <remarks/>
        public string JudgmentAmount
        {
            get
            {
                return this.judgmentAmountField;
            }
            set
            {
                this.judgmentAmountField = value;
            }
        }

        /// <remarks/>
        public string SatisfactionDate
        {
            get
            {
                return this.satisfactionDateField;
            }
            set
            {
                this.satisfactionDateField = value;
            }
        }

        /// <remarks/>
        public string TypeofSatisfaction
        {
            get
            {
                return this.typeofSatisfactionField;
            }
            set
            {
                this.typeofSatisfactionField = value;
            }
        }

        /// <remarks/>
        public string SheriffExecutionRequired
        {
            get
            {
                return this.sheriffExecutionRequiredField;
            }
            set
            {
                this.sheriffExecutionRequiredField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ExpirationDate")]
        public string[] ExpirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public string CreditorAmount
        {
            get
            {
                return this.creditorAmountField;
            }
            set
            {
                this.creditorAmountField = value;
            }
        }

        /// <remarks/>
        public string LienAmount
        {
            get
            {
                return this.lienAmountField;
            }
            set
            {
                this.lienAmountField = value;
            }
        }

        /// <remarks/>
        public string LienPrincipalAmount
        {
            get
            {
                return this.lienPrincipalAmountField;
            }
            set
            {
                this.lienPrincipalAmountField = value;
            }
        }

        /// <remarks/>
        public string TaxAmount
        {
            get
            {
                return this.taxAmountField;
            }
            set
            {
                this.taxAmountField = value;
            }
        }

        /// <remarks/>
        public string LienTotalAmount
        {
            get
            {
                return this.lienTotalAmountField;
            }
            set
            {
                this.lienTotalAmountField = value;
            }
        }

        /// <remarks/>
        public string TotalAmountPaid
        {
            get
            {
                return this.totalAmountPaidField;
            }
            set
            {
                this.totalAmountPaidField = value;
            }
        }

        /// <remarks/>
        public string TotalCalculatedAmount
        {
            get
            {
                return this.totalCalculatedAmountField;
            }
            set
            {
                this.totalCalculatedAmountField = value;
            }
        }

        /// <remarks/>
        public string TotalCalculatedAmountPaid
        {
            get
            {
                return this.totalCalculatedAmountPaidField;
            }
            set
            {
                this.totalCalculatedAmountPaidField = value;
            }
        }

        /// <remarks/>
        public StatusInfo StatusInfo
        {
            get
            {
                return this.statusInfoField;
            }
            set
            {
                this.statusInfoField = value;
            }
        }

        /// <remarks/>
        public SubjudgmentInfo SubjudgmentInfo
        {
            get
            {
                return this.subjudgmentInfoField;
            }
            set
            {
                this.subjudgmentInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class Subjudgments
    {

        public LienInfo[] lienInfoField;

        public JudgmentInfo[] judgmentInfoField;

        public CommentInfo[] commentInfoField;

        public Debtor[] debtorField;

        public Creditor[] creditorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LienInfo")]
        public LienInfo[] LienInfo
        {
            get
            {
                return this.lienInfoField;
            }
            set
            {
                this.lienInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("JudgmentInfo")]
        public JudgmentInfo[] JudgmentInfo
        {
            get
            {
                return this.judgmentInfoField;
            }
            set
            {
                this.judgmentInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommentInfo")]
        public CommentInfo[] CommentInfo
        {
            get
            {
                return this.commentInfoField;
            }
            set
            {
                this.commentInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Debtor")]
        public Debtor[] Debtor
        {
            get
            {
                return this.debtorField;
            }
            set
            {
                this.debtorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Creditor")]
        public Creditor[] Creditor
        {
            get
            {
                return this.creditorField;
            }
            set
            {
                this.creditorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class Debtor
    {

        public string debtorOwedAmountField;

        public string debtorOccupationField;

        public string numberOfDebtorsField;

        public PartyInfo partyInfoField;

        public string debtorDescriptionField;

        public string[] debtorAmountField;

        public ObligationInfo[] obligationInfoField;

        public CaseDisposition dispositionInfoField;

        public string[] demandAmountField;

        public string statusField;

        public DebtorSignerInfo debtorSignerInfoField;

        public string dUNSNumberField;

        public string taxIDField;

        public string[] typeOfDebtorField;

        public string[] typeOfDebtorNameField;

        public SSNInfo[] sSNInfoField;

        /// <remarks/>
        public string DebtorOwedAmount
        {
            get
            {
                return this.debtorOwedAmountField;
            }
            set
            {
                this.debtorOwedAmountField = value;
            }
        }

        /// <remarks/>
        public string DebtorOccupation
        {
            get
            {
                return this.debtorOccupationField;
            }
            set
            {
                this.debtorOccupationField = value;
            }
        }

        /// <remarks/>
        public string NumberOfDebtors
        {
            get
            {
                return this.numberOfDebtorsField;
            }
            set
            {
                this.numberOfDebtorsField = value;
            }
        }

        /// <remarks/>
        public PartyInfo PartyInfo
        {
            get
            {
                return this.partyInfoField;
            }
            set
            {
                this.partyInfoField = value;
            }
        }

        /// <remarks/>
        public string DebtorDescription
        {
            get
            {
                return this.debtorDescriptionField;
            }
            set
            {
                this.debtorDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DebtorAmount")]
        public string[] DebtorAmount
        {
            get
            {
                return this.debtorAmountField;
            }
            set
            {
                this.debtorAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ObligationInfo")]
        public ObligationInfo[] ObligationInfo
        {
            get
            {
                return this.obligationInfoField;
            }
            set
            {
                this.obligationInfoField = value;
            }
        }

        /// <remarks/>
        public CaseDisposition DispositionInfo
        {
            get
            {
                return this.dispositionInfoField;
            }
            set
            {
                this.dispositionInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DemandAmount")]
        public string[] DemandAmount
        {
            get
            {
                return this.demandAmountField;
            }
            set
            {
                this.demandAmountField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public DebtorSignerInfo DebtorSignerInfo
        {
            get
            {
                return this.debtorSignerInfoField;
            }
            set
            {
                this.debtorSignerInfoField = value;
            }
        }

        /// <remarks/>
        public string DUNSNumber
        {
            get
            {
                return this.dUNSNumberField;
            }
            set
            {
                this.dUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string TaxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TypeOfDebtor")]
        public string[] TypeOfDebtor
        {
            get
            {
                return this.typeOfDebtorField;
            }
            set
            {
                this.typeOfDebtorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TypeOfDebtorName")]
        public string[] TypeOfDebtorName
        {
            get
            {
                return this.typeOfDebtorNameField;
            }
            set
            {
                this.typeOfDebtorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SSNInfo")]
        public SSNInfo[] SSNInfo
        {
            get
            {
                return this.sSNInfoField;
            }
            set
            {
                this.sSNInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class PartyInfo
    {

        public PersonName personNameField;

        public string businessNameField;

        public string[] typeOfPartyField;

        public Address[] addressField;

        public PhoneInfo phoneInfoField;

        public AttorneyInfo[] attorneyInfoField;

        public string[] commentField;

        public string[] dUNSNumberField;

        public string[] headquarterDUNSNumberField;

        public string foreignRegField;

        public SSNInfo[] sSNInfoField;

        public DriverLicenseInfo[] driverLicenseInfoField;

        public PersonProfile[] personProfileField;

        public string[] fEINField;

        public PersonName[] aKANameField;

        public string reportedDateField;

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TypeOfParty")]
        public string[] TypeOfParty
        {
            get
            {
                return this.typeOfPartyField;
            }
            set
            {
                this.typeOfPartyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Address")]
        public Address[] Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public PhoneInfo PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AttorneyInfo")]
        public AttorneyInfo[] AttorneyInfo
        {
            get
            {
                return this.attorneyInfoField;
            }
            set
            {
                this.attorneyInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comment")]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DUNSNumber")]
        public string[] DUNSNumber
        {
            get
            {
                return this.dUNSNumberField;
            }
            set
            {
                this.dUNSNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("HeadquarterDUNSNumber")]
        public string[] HeadquarterDUNSNumber
        {
            get
            {
                return this.headquarterDUNSNumberField;
            }
            set
            {
                this.headquarterDUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string ForeignReg
        {
            get
            {
                return this.foreignRegField;
            }
            set
            {
                this.foreignRegField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SSNInfo")]
        public SSNInfo[] SSNInfo
        {
            get
            {
                return this.sSNInfoField;
            }
            set
            {
                this.sSNInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DriverLicenseInfo")]
        public DriverLicenseInfo[] DriverLicenseInfo
        {
            get
            {
                return this.driverLicenseInfoField;
            }
            set
            {
                this.driverLicenseInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonProfile")]
        public PersonProfile[] PersonProfile
        {
            get
            {
                return this.personProfileField;
            }
            set
            {
                this.personProfileField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FEIN")]
        public string[] FEIN
        {
            get
            {
                return this.fEINField;
            }
            set
            {
                this.fEINField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AKAName")]
        public PersonName[] AKAName
        {
            get
            {
                return this.aKANameField;
            }
            set
            {
                this.aKANameField = value;
            }
        }

        /// <remarks/>
        public string ReportedDate
        {
            get
            {
                return this.reportedDateField;
            }
            set
            {
                this.reportedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class PersonName
    {

        public string prefixField;

        public string firstNameField;

        public string middleNameField;

        public string maidenNameField;

        public string lastNameField;

        public string suffixField;

        public string secondaryLastNameField;

        public string[] fullNameField;

        /// <remarks/>
        public string Prefix
        {
            get
            {
                return this.prefixField;
            }
            set
            {
                this.prefixField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string MiddleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string MaidenName
        {
            get
            {
                return this.maidenNameField;
            }
            set
            {
                this.maidenNameField = value;
            }
        }

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string Suffix
        {
            get
            {
                return this.suffixField;
            }
            set
            {
                this.suffixField = value;
            }
        }

        /// <remarks/>
        public string SecondaryLastName
        {
            get
            {
                return this.secondaryLastNameField;
            }
            set
            {
                this.secondaryLastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FullName")]
        public string[] FullName
        {
            get
            {
                return this.fullNameField;
            }
            set
            {
                this.fullNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class PhoneInfo
    {

        public string phoneNumberField;

        public string phoneNumberSuffixField;

        public string workPhoneNumberField;

        public string tollFreePhoneNumberField;

        public string otherPhoneNumberField;

        public string faxNumberField;

        public string phoneNumberTypeField;

        public string otherPhoneNumberTypeField;

        /// <remarks/>
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        public string PhoneNumberSuffix
        {
            get
            {
                return this.phoneNumberSuffixField;
            }
            set
            {
                this.phoneNumberSuffixField = value;
            }
        }

        /// <remarks/>
        public string WorkPhoneNumber
        {
            get
            {
                return this.workPhoneNumberField;
            }
            set
            {
                this.workPhoneNumberField = value;
            }
        }

        /// <remarks/>
        public string TollFreePhoneNumber
        {
            get
            {
                return this.tollFreePhoneNumberField;
            }
            set
            {
                this.tollFreePhoneNumberField = value;
            }
        }

        /// <remarks/>
        public string OtherPhoneNumber
        {
            get
            {
                return this.otherPhoneNumberField;
            }
            set
            {
                this.otherPhoneNumberField = value;
            }
        }

        /// <remarks/>
        public string FaxNumber
        {
            get
            {
                return this.faxNumberField;
            }
            set
            {
                this.faxNumberField = value;
            }
        }

        /// <remarks/>
        public string PhoneNumberType
        {
            get
            {
                return this.phoneNumberTypeField;
            }
            set
            {
                this.phoneNumberTypeField = value;
            }
        }

        /// <remarks/>
        public string OtherPhoneNumberType
        {
            get
            {
                return this.otherPhoneNumberTypeField;
            }
            set
            {
                this.otherPhoneNumberTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class AttorneyInfo
    {

        public string attorneyBarNumberField;

        public string attorneyCORField;

        public string attorneyEmailField;

        public string attornyForeignRegistrationField;

        public string attorneyFTSField;

        public string attorneyIDField;

        public string attorneyLicenseField;

        public string[] attorneyNameField;

        public string attorneyPhoneField;

        public string attorneyFaxField;

        public Address[] attorneyAddressField;

        public string attorneyRepresentsField;

        public string attorneyRoleField;

        public string attorneyStatusField;

        public string attorneyTerminatedDateField;

        public string typeofAttorneyField;

        public string firmNameField;

        public string firmStatusField;

        /// <remarks/>
        public string AttorneyBarNumber
        {
            get
            {
                return this.attorneyBarNumberField;
            }
            set
            {
                this.attorneyBarNumberField = value;
            }
        }

        /// <remarks/>
        public string AttorneyCOR
        {
            get
            {
                return this.attorneyCORField;
            }
            set
            {
                this.attorneyCORField = value;
            }
        }

        /// <remarks/>
        public string AttorneyEmail
        {
            get
            {
                return this.attorneyEmailField;
            }
            set
            {
                this.attorneyEmailField = value;
            }
        }

        /// <remarks/>
        public string AttornyForeignRegistration
        {
            get
            {
                return this.attornyForeignRegistrationField;
            }
            set
            {
                this.attornyForeignRegistrationField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFTS
        {
            get
            {
                return this.attorneyFTSField;
            }
            set
            {
                this.attorneyFTSField = value;
            }
        }

        /// <remarks/>
        public string AttorneyID
        {
            get
            {
                return this.attorneyIDField;
            }
            set
            {
                this.attorneyIDField = value;
            }
        }

        /// <remarks/>
        public string AttorneyLicense
        {
            get
            {
                return this.attorneyLicenseField;
            }
            set
            {
                this.attorneyLicenseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AttorneyName")]
        public string[] AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>
        public string AttorneyFax
        {
            get
            {
                return this.attorneyFaxField;
            }
            set
            {
                this.attorneyFaxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AttorneyAddress")]
        public Address[] AttorneyAddress
        {
            get
            {
                return this.attorneyAddressField;
            }
            set
            {
                this.attorneyAddressField = value;
            }
        }

        /// <remarks/>
        public string AttorneyRepresents
        {
            get
            {
                return this.attorneyRepresentsField;
            }
            set
            {
                this.attorneyRepresentsField = value;
            }
        }

        /// <remarks/>
        public string AttorneyRole
        {
            get
            {
                return this.attorneyRoleField;
            }
            set
            {
                this.attorneyRoleField = value;
            }
        }

        /// <remarks/>
        public string AttorneyStatus
        {
            get
            {
                return this.attorneyStatusField;
            }
            set
            {
                this.attorneyStatusField = value;
            }
        }

        /// <remarks/>
        public string AttorneyTerminatedDate
        {
            get
            {
                return this.attorneyTerminatedDateField;
            }
            set
            {
                this.attorneyTerminatedDateField = value;
            }
        }

        /// <remarks/>
        public string TypeofAttorney
        {
            get
            {
                return this.typeofAttorneyField;
            }
            set
            {
                this.typeofAttorneyField = value;
            }
        }

        /// <remarks/>
        public string FirmName
        {
            get
            {
                return this.firmNameField;
            }
            set
            {
                this.firmNameField = value;
            }
        }

        /// <remarks/>
        public string FirmStatus
        {
            get
            {
                return this.firmStatusField;
            }
            set
            {
                this.firmStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class SSNInfo
    {

        public string[] sSNField;

        public string[] partialSSNField;

        public string sSNIssueDateField;

        public string sSNExpirationDateField;

        public string sSNIssuanceTextField;

        public string sSNIssueStateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SSN")]
        public string[] SSN
        {
            get
            {
                return this.sSNField;
            }
            set
            {
                this.sSNField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PartialSSN")]
        public string[] PartialSSN
        {
            get
            {
                return this.partialSSNField;
            }
            set
            {
                this.partialSSNField = value;
            }
        }

        /// <remarks/>
        public string SSNIssueDate
        {
            get
            {
                return this.sSNIssueDateField;
            }
            set
            {
                this.sSNIssueDateField = value;
            }
        }

        /// <remarks/>
        public string SSNExpirationDate
        {
            get
            {
                return this.sSNExpirationDateField;
            }
            set
            {
                this.sSNExpirationDateField = value;
            }
        }

        /// <remarks/>
        public string SSNIssuanceText
        {
            get
            {
                return this.sSNIssuanceTextField;
            }
            set
            {
                this.sSNIssuanceTextField = value;
            }
        }

        /// <remarks/>
        public string SSNIssueState
        {
            get
            {
                return this.sSNIssueStateField;
            }
            set
            {
                this.sSNIssueStateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class DriverLicenseInfo
    {

        public string[] driverLicenseNumberField;

        public string[] driverLicenseStateField;

        public string[] driverLicenseCountyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DriverLicenseNumber")]
        public string[] DriverLicenseNumber
        {
            get
            {
                return this.driverLicenseNumberField;
            }
            set
            {
                this.driverLicenseNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DriverLicenseState")]
        public string[] DriverLicenseState
        {
            get
            {
                return this.driverLicenseStateField;
            }
            set
            {
                this.driverLicenseStateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DriverLicenseCounty")]
        public string[] DriverLicenseCounty
        {
            get
            {
                return this.driverLicenseCountyField;
            }
            set
            {
                this.driverLicenseCountyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class PersonProfile
    {

        public string[] personBirthDateField;

        public string[] yearOfBirthField;

        public string[] monthOfBirthField;

        public string[] dayOfBirthField;

        public string[] personAgeToField;

        public string[] personAgeFromField;

        public string[] personDeathIndicatorField;

        public string[] personDeathDateField;

        public string[] personDeathPlaceField;

        public string[] personBirthPlaceField;

        public string[] personBirthStateField;

        public string[] numberofDependentsField;

        public string[] employeeOccupationField;

        public string[] professionalTitleField;

        public string[] personAgeField;

        public string[] personBuildField;

        public string[] personCitizenshipField;

        public string[] personEducationLevelField;

        public string[] personEthnicityField;

        public string[] personEyeColorField;

        public string[] personHairColorField;

        public string[] personHeightField;

        public string[] personRaceField;

        public string[] personSexField;

        public string[] personSkinToneField;

        public string[] personMaritalStatusField;

        public string[] personSpouseNameField;

        public string[] personWeightField;

        public string[] personMarkingsField;

        public string[] personMarkingsLocationField;

        public string[] personMarkingsTypeField;

        public string[] personContactEmailField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonBirthDate")]
        public string[] PersonBirthDate
        {
            get
            {
                return this.personBirthDateField;
            }
            set
            {
                this.personBirthDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("YearOfBirth")]
        public string[] YearOfBirth
        {
            get
            {
                return this.yearOfBirthField;
            }
            set
            {
                this.yearOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MonthOfBirth")]
        public string[] MonthOfBirth
        {
            get
            {
                return this.monthOfBirthField;
            }
            set
            {
                this.monthOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DayOfBirth")]
        public string[] DayOfBirth
        {
            get
            {
                return this.dayOfBirthField;
            }
            set
            {
                this.dayOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonAgeTo")]
        public string[] PersonAgeTo
        {
            get
            {
                return this.personAgeToField;
            }
            set
            {
                this.personAgeToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonAgeFrom")]
        public string[] PersonAgeFrom
        {
            get
            {
                return this.personAgeFromField;
            }
            set
            {
                this.personAgeFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonDeathIndicator")]
        public string[] PersonDeathIndicator
        {
            get
            {
                return this.personDeathIndicatorField;
            }
            set
            {
                this.personDeathIndicatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonDeathDate")]
        public string[] PersonDeathDate
        {
            get
            {
                return this.personDeathDateField;
            }
            set
            {
                this.personDeathDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonDeathPlace")]
        public string[] PersonDeathPlace
        {
            get
            {
                return this.personDeathPlaceField;
            }
            set
            {
                this.personDeathPlaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonBirthPlace")]
        public string[] PersonBirthPlace
        {
            get
            {
                return this.personBirthPlaceField;
            }
            set
            {
                this.personBirthPlaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonBirthState")]
        public string[] PersonBirthState
        {
            get
            {
                return this.personBirthStateField;
            }
            set
            {
                this.personBirthStateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("NumberofDependents")]
        public string[] NumberofDependents
        {
            get
            {
                return this.numberofDependentsField;
            }
            set
            {
                this.numberofDependentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("EmployeeOccupation")]
        public string[] EmployeeOccupation
        {
            get
            {
                return this.employeeOccupationField;
            }
            set
            {
                this.employeeOccupationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ProfessionalTitle")]
        public string[] ProfessionalTitle
        {
            get
            {
                return this.professionalTitleField;
            }
            set
            {
                this.professionalTitleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonAge")]
        public string[] PersonAge
        {
            get
            {
                return this.personAgeField;
            }
            set
            {
                this.personAgeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonBuild")]
        public string[] PersonBuild
        {
            get
            {
                return this.personBuildField;
            }
            set
            {
                this.personBuildField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonCitizenship")]
        public string[] PersonCitizenship
        {
            get
            {
                return this.personCitizenshipField;
            }
            set
            {
                this.personCitizenshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonEducationLevel")]
        public string[] PersonEducationLevel
        {
            get
            {
                return this.personEducationLevelField;
            }
            set
            {
                this.personEducationLevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonEthnicity")]
        public string[] PersonEthnicity
        {
            get
            {
                return this.personEthnicityField;
            }
            set
            {
                this.personEthnicityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonEyeColor")]
        public string[] PersonEyeColor
        {
            get
            {
                return this.personEyeColorField;
            }
            set
            {
                this.personEyeColorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonHairColor")]
        public string[] PersonHairColor
        {
            get
            {
                return this.personHairColorField;
            }
            set
            {
                this.personHairColorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonHeight")]
        public string[] PersonHeight
        {
            get
            {
                return this.personHeightField;
            }
            set
            {
                this.personHeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonRace")]
        public string[] PersonRace
        {
            get
            {
                return this.personRaceField;
            }
            set
            {
                this.personRaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonSex")]
        public string[] PersonSex
        {
            get
            {
                return this.personSexField;
            }
            set
            {
                this.personSexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonSkinTone")]
        public string[] PersonSkinTone
        {
            get
            {
                return this.personSkinToneField;
            }
            set
            {
                this.personSkinToneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonMaritalStatus")]
        public string[] PersonMaritalStatus
        {
            get
            {
                return this.personMaritalStatusField;
            }
            set
            {
                this.personMaritalStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonSpouseName")]
        public string[] PersonSpouseName
        {
            get
            {
                return this.personSpouseNameField;
            }
            set
            {
                this.personSpouseNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonWeight")]
        public string[] PersonWeight
        {
            get
            {
                return this.personWeightField;
            }
            set
            {
                this.personWeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonMarkings")]
        public string[] PersonMarkings
        {
            get
            {
                return this.personMarkingsField;
            }
            set
            {
                this.personMarkingsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonMarkingsLocation")]
        public string[] PersonMarkingsLocation
        {
            get
            {
                return this.personMarkingsLocationField;
            }
            set
            {
                this.personMarkingsLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonMarkingsType")]
        public string[] PersonMarkingsType
        {
            get
            {
                return this.personMarkingsTypeField;
            }
            set
            {
                this.personMarkingsTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonContactEmail")]
        public string[] PersonContactEmail
        {
            get
            {
                return this.personContactEmailField;
            }
            set
            {
                this.personContactEmailField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class DebtorSignerInfo
    {

        public string signerField;

        public string signerBusinessField;

        public string signerDescriptionField;

        public string signerTitleField;

        /// <remarks/>
        public string Signer
        {
            get
            {
                return this.signerField;
            }
            set
            {
                this.signerField = value;
            }
        }

        /// <remarks/>
        public string SignerBusiness
        {
            get
            {
                return this.signerBusinessField;
            }
            set
            {
                this.signerBusinessField = value;
            }
        }

        /// <remarks/>
        public string SignerDescription
        {
            get
            {
                return this.signerDescriptionField;
            }
            set
            {
                this.signerDescriptionField = value;
            }
        }

        /// <remarks/>
        public string SignerTitle
        {
            get
            {
                return this.signerTitleField;
            }
            set
            {
                this.signerTitleField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class Creditor
    {

        public PartyInfo partyInfoField;

        public string creditHolderLevelDescField;

        public string creditHolderSerialNumberField;

        public string creditHolderField;

        public string creditHolderLevelField;

        public string dUNSNumberField;

        public string foreignRegionField;

        public string claimDescriptionField;

        public string otherClaimField;

        public string securedClaimField;

        public string unsecuredClaimField;

        /// <remarks/>
        public PartyInfo PartyInfo
        {
            get
            {
                return this.partyInfoField;
            }
            set
            {
                this.partyInfoField = value;
            }
        }

        /// <remarks/>
        public string CreditHolderLevelDesc
        {
            get
            {
                return this.creditHolderLevelDescField;
            }
            set
            {
                this.creditHolderLevelDescField = value;
            }
        }

        /// <remarks/>
        public string CreditHolderSerialNumber
        {
            get
            {
                return this.creditHolderSerialNumberField;
            }
            set
            {
                this.creditHolderSerialNumberField = value;
            }
        }

        /// <remarks/>
        public string CreditHolder
        {
            get
            {
                return this.creditHolderField;
            }
            set
            {
                this.creditHolderField = value;
            }
        }

        /// <remarks/>
        public string CreditHolderLevel
        {
            get
            {
                return this.creditHolderLevelField;
            }
            set
            {
                this.creditHolderLevelField = value;
            }
        }

        /// <remarks/>
        public string DUNSNumber
        {
            get
            {
                return this.dUNSNumberField;
            }
            set
            {
                this.dUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string ForeignRegion
        {
            get
            {
                return this.foreignRegionField;
            }
            set
            {
                this.foreignRegionField = value;
            }
        }

        /// <remarks/>
        public string ClaimDescription
        {
            get
            {
                return this.claimDescriptionField;
            }
            set
            {
                this.claimDescriptionField = value;
            }
        }

        /// <remarks/>
        public string OtherClaim
        {
            get
            {
                return this.otherClaimField;
            }
            set
            {
                this.otherClaimField = value;
            }
        }

        /// <remarks/>
        public string SecuredClaim
        {
            get
            {
                return this.securedClaimField;
            }
            set
            {
                this.securedClaimField = value;
            }
        }

        /// <remarks/>
        public string UnsecuredClaim
        {
            get
            {
                return this.unsecuredClaimField;
            }
            set
            {
                this.unsecuredClaimField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class LienJudgeMultipleRecord
    {

        public Creditor[] creditorField;

        public Debtor[] debtorField;

        public Subjudgments[] subjudgmentsField;

        public JudgmentInfo[] judgmentInfoField;

        public LienInfo[] lienInfoField;

        public CommentInfo[] commentInfoField;

        public string numberofSubjudgmentsField;

        public FilingInfo[] lienJudgeFilingInfoField;

        public string sourceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Creditor")]
        public Creditor[] Creditor
        {
            get
            {
                return this.creditorField;
            }
            set
            {
                this.creditorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Debtor")]
        public Debtor[] Debtor
        {
            get
            {
                return this.debtorField;
            }
            set
            {
                this.debtorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Subjudgments")]
        public Subjudgments[] Subjudgments
        {
            get
            {
                return this.subjudgmentsField;
            }
            set
            {
                this.subjudgmentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("JudgmentInfo")]
        public JudgmentInfo[] JudgmentInfo
        {
            get
            {
                return this.judgmentInfoField;
            }
            set
            {
                this.judgmentInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LienInfo")]
        public LienInfo[] LienInfo
        {
            get
            {
                return this.lienInfoField;
            }
            set
            {
                this.lienInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CommentInfo")]
        public CommentInfo[] CommentInfo
        {
            get
            {
                return this.commentInfoField;
            }
            set
            {
                this.commentInfoField = value;
            }
        }

        /// <remarks/>
        public string NumberofSubjudgments
        {
            get
            {
                return this.numberofSubjudgmentsField;
            }
            set
            {
                this.numberofSubjudgmentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LienJudgeFilingInfo")]
        public FilingInfo[] LienJudgeFilingInfo
        {
            get
            {
                return this.lienJudgeFilingInfoField;
            }
            set
            {
                this.lienJudgeFilingInfoField = value;
            }
        }

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class UCCFilingHistory
    {

        public UCCFilingInfo uCCFilingInfoField;

        /// <remarks/>
        public UCCFilingInfo UCCFilingInfo
        {
            get
            {
                return this.uCCFilingInfoField;
            }
            set
            {
                this.uCCFilingInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class UCCFilingInfo
    {

        public FilingStmtInfo[] filingStmtInfoField;

        public UCCPartyInfo[] debtorField;

        public UCCPartyInfo[] securedPartyField;

        public UCCPartyInfo[] assigneeField;

        public UCCPartyInfo[] assignorField;

        public RealEstateInfo realEstateInfoField;

        public CollateralInfo[] collateralInfoField;

        public FilingOfficeStmt filingOfficeStmtField;

        public RelatedFilingInfo[] relatedFilingInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("FilingStmtInfo")]
        public FilingStmtInfo[] FilingStmtInfo
        {
            get
            {
                return this.filingStmtInfoField;
            }
            set
            {
                this.filingStmtInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Debtor")]
        public UCCPartyInfo[] Debtor
        {
            get
            {
                return this.debtorField;
            }
            set
            {
                this.debtorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SecuredParty")]
        public UCCPartyInfo[] SecuredParty
        {
            get
            {
                return this.securedPartyField;
            }
            set
            {
                this.securedPartyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Assignee")]
        public UCCPartyInfo[] Assignee
        {
            get
            {
                return this.assigneeField;
            }
            set
            {
                this.assigneeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Assignor")]
        public UCCPartyInfo[] Assignor
        {
            get
            {
                return this.assignorField;
            }
            set
            {
                this.assignorField = value;
            }
        }

        /// <remarks/>
        public RealEstateInfo RealEstateInfo
        {
            get
            {
                return this.realEstateInfoField;
            }
            set
            {
                this.realEstateInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CollateralInfo")]
        public CollateralInfo[] CollateralInfo
        {
            get
            {
                return this.collateralInfoField;
            }
            set
            {
                this.collateralInfoField = value;
            }
        }

        /// <remarks/>
        public FilingOfficeStmt FilingOfficeStmt
        {
            get
            {
                return this.filingOfficeStmtField;
            }
            set
            {
                this.filingOfficeStmtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RelatedFilingInfo")]
        public RelatedFilingInfo[] RelatedFilingInfo
        {
            get
            {
                return this.relatedFilingInfoField;
            }
            set
            {
                this.relatedFilingInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingStmtInfo
    {

        public BusinessInfo businessInfoField;

        public Comments[] commentsField;

        public string contractTypeField;

        public string expireDateField;

        public string fileNumberFullField;

        public string filePagesField;

        public string fileStatusField;

        public string fileTimeField;

        public string filingActionField;

        public string filingMethodField;

        public string filingTerminationField;

        public string filmNumberField;

        public string originalFileNumberFullField;

        public string pageCountField;

        public string pageNumberField;

        public string referenceFileNumberField;

        public string referenceIDField;

        public string relatedFileDateField;

        public string volumeNumberField;

        /// <remarks/>
        public BusinessInfo BusinessInfo
        {
            get
            {
                return this.businessInfoField;
            }
            set
            {
                this.businessInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Comments")]
        public Comments[] Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        public string ContractType
        {
            get
            {
                return this.contractTypeField;
            }
            set
            {
                this.contractTypeField = value;
            }
        }

        /// <remarks/>
        public string ExpireDate
        {
            get
            {
                return this.expireDateField;
            }
            set
            {
                this.expireDateField = value;
            }
        }

        /// <remarks/>
        public string FileNumberFull
        {
            get
            {
                return this.fileNumberFullField;
            }
            set
            {
                this.fileNumberFullField = value;
            }
        }

        /// <remarks/>
        public string FilePages
        {
            get
            {
                return this.filePagesField;
            }
            set
            {
                this.filePagesField = value;
            }
        }

        /// <remarks/>
        public string FileStatus
        {
            get
            {
                return this.fileStatusField;
            }
            set
            {
                this.fileStatusField = value;
            }
        }

        /// <remarks/>
        public string FileTime
        {
            get
            {
                return this.fileTimeField;
            }
            set
            {
                this.fileTimeField = value;
            }
        }

        /// <remarks/>
        public string FilingAction
        {
            get
            {
                return this.filingActionField;
            }
            set
            {
                this.filingActionField = value;
            }
        }

        /// <remarks/>
        public string FilingMethod
        {
            get
            {
                return this.filingMethodField;
            }
            set
            {
                this.filingMethodField = value;
            }
        }

        /// <remarks/>
        public string FilingTermination
        {
            get
            {
                return this.filingTerminationField;
            }
            set
            {
                this.filingTerminationField = value;
            }
        }

        /// <remarks/>
        public string FilmNumber
        {
            get
            {
                return this.filmNumberField;
            }
            set
            {
                this.filmNumberField = value;
            }
        }

        /// <remarks/>
        public string OriginalFileNumberFull
        {
            get
            {
                return this.originalFileNumberFullField;
            }
            set
            {
                this.originalFileNumberFullField = value;
            }
        }

        /// <remarks/>
        public string PageCount
        {
            get
            {
                return this.pageCountField;
            }
            set
            {
                this.pageCountField = value;
            }
        }

        /// <remarks/>
        public string PageNumber
        {
            get
            {
                return this.pageNumberField;
            }
            set
            {
                this.pageNumberField = value;
            }
        }

        /// <remarks/>
        public string ReferenceFileNumber
        {
            get
            {
                return this.referenceFileNumberField;
            }
            set
            {
                this.referenceFileNumberField = value;
            }
        }

        /// <remarks/>
        public string ReferenceID
        {
            get
            {
                return this.referenceIDField;
            }
            set
            {
                this.referenceIDField = value;
            }
        }

        /// <remarks/>
        public string RelatedFileDate
        {
            get
            {
                return this.relatedFileDateField;
            }
            set
            {
                this.relatedFileDateField = value;
            }
        }

        /// <remarks/>
        public string VolumeNumber
        {
            get
            {
                return this.volumeNumberField;
            }
            set
            {
                this.volumeNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class BusinessInfo
    {

        public Address addressField;

        public Address primaryAddressField;

        public Address mailingAddressField;

        public string corpBusinessStatusField;

        public string dUNSNumberField;

        public string filingTypeField;

        public string filingDateField;

        public string filingNumberField;

        public string filingStateField;

        public string origFilingDateField;

        public string origFilingNumberField;

        public string taxIdField;

        public string federalEmpIDField;

        public SICInfo[] primarySICField;

        public string sICCodeField;

        public string sICDescField;

        public string sICExtField;

        public string businessNameField;

        public PhoneInfo phoneInfoField;

        public FilingOfficeAddress filingOfficeAddressField;

        public string businessLocationTypeField;

        public string businessEmailField;

        public string uRLField;

        public string businessNameShortField;

        public string businessNumberField;

        public string businessDescriptionField;

        public string summaryBusinessDescriptionField;

        public string numberOfEmployeesField;

        public string operatingStatusField;

        public string yearStartedField;

        public string nationalIDField;

        public string secondaryBusinessNameField;

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public Address PrimaryAddress
        {
            get
            {
                return this.primaryAddressField;
            }
            set
            {
                this.primaryAddressField = value;
            }
        }

        /// <remarks/>
        public Address MailingAddress
        {
            get
            {
                return this.mailingAddressField;
            }
            set
            {
                this.mailingAddressField = value;
            }
        }

        /// <remarks/>
        public string CorpBusinessStatus
        {
            get
            {
                return this.corpBusinessStatusField;
            }
            set
            {
                this.corpBusinessStatusField = value;
            }
        }

        /// <remarks/>
        public string DUNSNumber
        {
            get
            {
                return this.dUNSNumberField;
            }
            set
            {
                this.dUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string FilingType
        {
            get
            {
                return this.filingTypeField;
            }
            set
            {
                this.filingTypeField = value;
            }
        }

        /// <remarks/>
        public string FilingDate
        {
            get
            {
                return this.filingDateField;
            }
            set
            {
                this.filingDateField = value;
            }
        }

        /// <remarks/>
        public string FilingNumber
        {
            get
            {
                return this.filingNumberField;
            }
            set
            {
                this.filingNumberField = value;
            }
        }

        /// <remarks/>
        public string FilingState
        {
            get
            {
                return this.filingStateField;
            }
            set
            {
                this.filingStateField = value;
            }
        }

        /// <remarks/>
        public string OrigFilingDate
        {
            get
            {
                return this.origFilingDateField;
            }
            set
            {
                this.origFilingDateField = value;
            }
        }

        /// <remarks/>
        public string OrigFilingNumber
        {
            get
            {
                return this.origFilingNumberField;
            }
            set
            {
                this.origFilingNumberField = value;
            }
        }

        /// <remarks/>
        public string TaxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        /// <remarks/>
        public string FederalEmpID
        {
            get
            {
                return this.federalEmpIDField;
            }
            set
            {
                this.federalEmpIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PrimarySIC")]
        public SICInfo[] PrimarySIC
        {
            get
            {
                return this.primarySICField;
            }
            set
            {
                this.primarySICField = value;
            }
        }

        /// <remarks/>
        public string SICCode
        {
            get
            {
                return this.sICCodeField;
            }
            set
            {
                this.sICCodeField = value;
            }
        }

        /// <remarks/>
        public string SICDesc
        {
            get
            {
                return this.sICDescField;
            }
            set
            {
                this.sICDescField = value;
            }
        }

        /// <remarks/>
        public string SICExt
        {
            get
            {
                return this.sICExtField;
            }
            set
            {
                this.sICExtField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public PhoneInfo PhoneInfo
        {
            get
            {
                return this.phoneInfoField;
            }
            set
            {
                this.phoneInfoField = value;
            }
        }

        /// <remarks/>
        public FilingOfficeAddress FilingOfficeAddress
        {
            get
            {
                return this.filingOfficeAddressField;
            }
            set
            {
                this.filingOfficeAddressField = value;
            }
        }

        /// <remarks/>
        public string BusinessLocationType
        {
            get
            {
                return this.businessLocationTypeField;
            }
            set
            {
                this.businessLocationTypeField = value;
            }
        }

        /// <remarks/>
        public string BusinessEmail
        {
            get
            {
                return this.businessEmailField;
            }
            set
            {
                this.businessEmailField = value;
            }
        }

        /// <remarks/>
        public string URL
        {
            get
            {
                return this.uRLField;
            }
            set
            {
                this.uRLField = value;
            }
        }

        /// <remarks/>
        public string BusinessNameShort
        {
            get
            {
                return this.businessNameShortField;
            }
            set
            {
                this.businessNameShortField = value;
            }
        }

        /// <remarks/>
        public string BusinessNumber
        {
            get
            {
                return this.businessNumberField;
            }
            set
            {
                this.businessNumberField = value;
            }
        }

        /// <remarks/>
        public string BusinessDescription
        {
            get
            {
                return this.businessDescriptionField;
            }
            set
            {
                this.businessDescriptionField = value;
            }
        }

        /// <remarks/>
        public string SummaryBusinessDescription
        {
            get
            {
                return this.summaryBusinessDescriptionField;
            }
            set
            {
                this.summaryBusinessDescriptionField = value;
            }
        }

        /// <remarks/>
        public string NumberOfEmployees
        {
            get
            {
                return this.numberOfEmployeesField;
            }
            set
            {
                this.numberOfEmployeesField = value;
            }
        }

        /// <remarks/>
        public string OperatingStatus
        {
            get
            {
                return this.operatingStatusField;
            }
            set
            {
                this.operatingStatusField = value;
            }
        }

        /// <remarks/>
        public string YearStarted
        {
            get
            {
                return this.yearStartedField;
            }
            set
            {
                this.yearStartedField = value;
            }
        }

        /// <remarks/>
        public string NationalID
        {
            get
            {
                return this.nationalIDField;
            }
            set
            {
                this.nationalIDField = value;
            }
        }

        /// <remarks/>
        public string SecondaryBusinessName
        {
            get
            {
                return this.secondaryBusinessNameField;
            }
            set
            {
                this.secondaryBusinessNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class SICInfo
    {

        public string sICCodeField;

        public string sICDescField;

        public string sICExtField;

        public string nAICSCodeField;

        public string nAICSDescField;

        /// <remarks/>
        public string SICCode
        {
            get
            {
                return this.sICCodeField;
            }
            set
            {
                this.sICCodeField = value;
            }
        }

        /// <remarks/>
        public string SICDesc
        {
            get
            {
                return this.sICDescField;
            }
            set
            {
                this.sICDescField = value;
            }
        }

        /// <remarks/>
        public string SICExt
        {
            get
            {
                return this.sICExtField;
            }
            set
            {
                this.sICExtField = value;
            }
        }

        /// <remarks/>
        public string NAICSCode
        {
            get
            {
                return this.nAICSCodeField;
            }
            set
            {
                this.nAICSCodeField = value;
            }
        }

        /// <remarks/>
        public string NAICSDesc
        {
            get
            {
                return this.nAICSDescField;
            }
            set
            {
                this.nAICSDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingOfficeAddress
    {

        public Address addressField;

        public string filingOfficeNameField;

        public string filingOfficeLocationField;

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string FilingOfficeName
        {
            get
            {
                return this.filingOfficeNameField;
            }
            set
            {
                this.filingOfficeNameField = value;
            }
        }

        /// <remarks/>
        public string FilingOfficeLocation
        {
            get
            {
                return this.filingOfficeLocationField;
            }
            set
            {
                this.filingOfficeLocationField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class Comments
    {

        public string commentDateField;

        public string commentSourceField;

        public string commentTextField;

        public string commentText2Field;

        public string sourceBusinessField;

        public string sourceTitleField;

        public string sourceTitle2Field;

        public string sourceTitle3Field;

        public string sourceTitleDescField;

        /// <remarks/>
        public string CommentDate
        {
            get
            {
                return this.commentDateField;
            }
            set
            {
                this.commentDateField = value;
            }
        }

        /// <remarks/>
        public string CommentSource
        {
            get
            {
                return this.commentSourceField;
            }
            set
            {
                this.commentSourceField = value;
            }
        }

        /// <remarks/>
        public string CommentText
        {
            get
            {
                return this.commentTextField;
            }
            set
            {
                this.commentTextField = value;
            }
        }

        /// <remarks/>
        public string CommentText2
        {
            get
            {
                return this.commentText2Field;
            }
            set
            {
                this.commentText2Field = value;
            }
        }

        /// <remarks/>
        public string SourceBusiness
        {
            get
            {
                return this.sourceBusinessField;
            }
            set
            {
                this.sourceBusinessField = value;
            }
        }

        /// <remarks/>
        public string SourceTitle
        {
            get
            {
                return this.sourceTitleField;
            }
            set
            {
                this.sourceTitleField = value;
            }
        }

        /// <remarks/>
        public string SourceTitle2
        {
            get
            {
                return this.sourceTitle2Field;
            }
            set
            {
                this.sourceTitle2Field = value;
            }
        }

        /// <remarks/>
        public string SourceTitle3
        {
            get
            {
                return this.sourceTitle3Field;
            }
            set
            {
                this.sourceTitle3Field = value;
            }
        }

        /// <remarks/>
        public string SourceTitleDesc
        {
            get
            {
                return this.sourceTitleDescField;
            }
            set
            {
                this.sourceTitleDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class UCCPartyInfo
    {

        public string[] partyNameField;

        public PersonName[] personNameField;

        public string[] businessNameField;

        public string businessDUNSNumberField;

        public string headqtrDUNSNumberField;

        public string typeOfPartyField;

        public string taxIDField;

        public Address addressField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PartyName")]
        public string[] PartyName
        {
            get
            {
                return this.partyNameField;
            }
            set
            {
                this.partyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonName")]
        public PersonName[] PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BusinessName")]
        public string[] BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string BusinessDUNSNumber
        {
            get
            {
                return this.businessDUNSNumberField;
            }
            set
            {
                this.businessDUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string HeadqtrDUNSNumber
        {
            get
            {
                return this.headqtrDUNSNumberField;
            }
            set
            {
                this.headqtrDUNSNumberField = value;
            }
        }

        /// <remarks/>
        public string TypeOfParty
        {
            get
            {
                return this.typeOfPartyField;
            }
            set
            {
                this.typeOfPartyField = value;
            }
        }

        /// <remarks/>
        public string TaxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class RealEstateInfo
    {

        public string descriptionField;

        public string designationField;

        public string[] businessNameField;

        public string[] taxIDField;

        public PersonName[] personNameField;

        public Address realEstateAddressField;

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BusinessName")]
        public string[] BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TaxID")]
        public string[] TaxID
        {
            get
            {
                return this.taxIDField;
            }
            set
            {
                this.taxIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PersonName")]
        public PersonName[] PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public Address RealEstateAddress
        {
            get
            {
                return this.realEstateAddressField;
            }
            set
            {
                this.realEstateAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CollateralInfo
    {

        public string typeOfCollateralField;

        public CollateralMachineInfo[] collateralMachineInfoField;

        /// <remarks/>
        public string TypeOfCollateral
        {
            get
            {
                return this.typeOfCollateralField;
            }
            set
            {
                this.typeOfCollateralField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CollateralMachineInfo")]
        public CollateralMachineInfo[] CollateralMachineInfo
        {
            get
            {
                return this.collateralMachineInfoField;
            }
            set
            {
                this.collateralMachineInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CollateralMachineInfo
    {

        public string typeOfMachineField;

        public string typeOfSecondaryMachineField;

        public string manufacturerField;

        public string manufacturerYearField;

        public string modelDescriptionField;

        public string modelField;

        public string modelYearField;

        public string modelStatusField;

        public string quantityField;

        public string serialNumberField;

        /// <remarks/>
        public string TypeOfMachine
        {
            get
            {
                return this.typeOfMachineField;
            }
            set
            {
                this.typeOfMachineField = value;
            }
        }

        /// <remarks/>
        public string TypeOfSecondaryMachine
        {
            get
            {
                return this.typeOfSecondaryMachineField;
            }
            set
            {
                this.typeOfSecondaryMachineField = value;
            }
        }

        /// <remarks/>
        public string Manufacturer
        {
            get
            {
                return this.manufacturerField;
            }
            set
            {
                this.manufacturerField = value;
            }
        }

        /// <remarks/>
        public string ManufacturerYear
        {
            get
            {
                return this.manufacturerYearField;
            }
            set
            {
                this.manufacturerYearField = value;
            }
        }

        /// <remarks/>
        public string ModelDescription
        {
            get
            {
                return this.modelDescriptionField;
            }
            set
            {
                this.modelDescriptionField = value;
            }
        }

        /// <remarks/>
        public string Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string ModelYear
        {
            get
            {
                return this.modelYearField;
            }
            set
            {
                this.modelYearField = value;
            }
        }

        /// <remarks/>
        public string ModelStatus
        {
            get
            {
                return this.modelStatusField;
            }
            set
            {
                this.modelStatusField = value;
            }
        }

        /// <remarks/>
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class FilingOfficeStmt
    {

        public string accountStmtField;

        public string crossRefFileNumberField;

        public string crossReFileNumberFullField;

        public string inaccuracyStmtField;

        /// <remarks/>
        public string AccountStmt
        {
            get
            {
                return this.accountStmtField;
            }
            set
            {
                this.accountStmtField = value;
            }
        }

        /// <remarks/>
        public string CrossRefFileNumber
        {
            get
            {
                return this.crossRefFileNumberField;
            }
            set
            {
                this.crossRefFileNumberField = value;
            }
        }

        /// <remarks/>
        public string CrossReFileNumberFull
        {
            get
            {
                return this.crossReFileNumberFullField;
            }
            set
            {
                this.crossReFileNumberFullField = value;
            }
        }

        /// <remarks/>
        public string InaccuracyStmt
        {
            get
            {
                return this.inaccuracyStmtField;
            }
            set
            {
                this.inaccuracyStmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class RelatedFilingInfo
    {

        public string relatedFilingDateField;

        public string relatedFilingDocumentNumberField;

        public string relatedFilingNumberField;

        public string relatedFilingPageCountField;

        public string relatedFilingTimeField;

        public string relatedFilingTypeField;

        /// <remarks/>
        public string RelatedFilingDate
        {
            get
            {
                return this.relatedFilingDateField;
            }
            set
            {
                this.relatedFilingDateField = value;
            }
        }

        /// <remarks/>
        public string RelatedFilingDocumentNumber
        {
            get
            {
                return this.relatedFilingDocumentNumberField;
            }
            set
            {
                this.relatedFilingDocumentNumberField = value;
            }
        }

        /// <remarks/>
        public string RelatedFilingNumber
        {
            get
            {
                return this.relatedFilingNumberField;
            }
            set
            {
                this.relatedFilingNumberField = value;
            }
        }

        /// <remarks/>
        public string RelatedFilingPageCount
        {
            get
            {
                return this.relatedFilingPageCountField;
            }
            set
            {
                this.relatedFilingPageCountField = value;
            }
        }

        /// <remarks/>
        public string RelatedFilingTime
        {
            get
            {
                return this.relatedFilingTimeField;
            }
            set
            {
                this.relatedFilingTimeField = value;
            }
        }

        /// <remarks/>
        public string RelatedFilingType
        {
            get
            {
                return this.relatedFilingTypeField;
            }
            set
            {
                this.relatedFilingTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class UCCRecord
    {

        public UCCFilingInfo uCCFilingInfoField;

        public UCCFilingHistory[] uCCFilingHistoryField;

        public string sourceField;

        /// <remarks/>
        public UCCFilingInfo UCCFilingInfo
        {
            get
            {
                return this.uCCFilingInfoField;
            }
            set
            {
                this.uCCFilingInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("UCCFilingHistory")]
        public UCCFilingHistory[] UCCFilingHistory
        {
            get
            {
                return this.uCCFilingHistoryField;
            }
            set
            {
                this.uCCFilingHistoryField = value;
            }
        }

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CourtResultsPageResultGroup
    {

        public string groupIdField;

        public string recordCountField;

        public string relevanceField;

        public CourtResultsPageResultGroupDominantValues dominantValuesField;

        public CourtResultsPageResultGroupRecordDetails recordDetailsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string GroupId
        {
            get
            {
                return this.groupIdField;
            }
            set
            {
                this.groupIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string RecordCount
        {
            get
            {
                return this.recordCountField;
            }
            set
            {
                this.recordCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Relevance
        {
            get
            {
                return this.relevanceField;
            }
            set
            {
                this.relevanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("DominantValues", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CourtResultsPageResultGroupDominantValues DominantValues
        {
            get
            {
                return this.dominantValuesField;
            }
            set
            {
                this.dominantValuesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RecordDetails", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CourtResultsPageResultGroupRecordDetails RecordDetails
        {
            get
            {
                return this.recordDetailsField;
            }
            set
            {
                this.recordDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://com/thomsonreuters/schemas/search")]
    public partial class CourtResultsPageResultGroupDominantValues
    {

        public CourtResultsPageResultGroupDominantValuesCourtDominantValues courtDominantValuesField;

        /// <remarks/>
        public CourtResultsPageResultGroupDominantValuesCourtDominantValues CourtDominantValues
        {
            get
            {
                return this.courtDominantValuesField;
            }
            set
            {
                this.courtDominantValuesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://com/thomsonreuters/schemas/search")]
    public partial class CourtResultsPageResultGroupDominantValuesCourtDominantValues
    {

        public string nameField;

        public string caseStateField;

        public string caseDateField;

        public CourtResultsPageResultGroupDominantValuesCourtDominantValuesAddress addressField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CaseState
        {
            get
            {
                return this.caseStateField;
            }
            set
            {
                this.caseStateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CaseDate
        {
            get
            {
                return this.caseDateField;
            }
            set
            {
                this.caseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CourtResultsPageResultGroupDominantValuesCourtDominantValuesAddress Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class CourtResultsPageResultGroupDominantValuesCourtDominantValuesAddress
    {

        public string streetField;

        public string cityField;

        public string stateField;

        public string zipCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ZipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://com/thomsonreuters/schemas/search")]
    public partial class CourtResultsPageResultGroupRecordDetails
    {

        public CourtResultsPageResultGroupRecordDetailsCourtResponseDetail courtResponseDetailField;

        /// <remarks/>
        public CourtResultsPageResultGroupRecordDetailsCourtResponseDetail CourtResponseDetail
        {
            get
            {
                return this.courtResponseDetailField;
            }
            set
            {
                this.courtResponseDetailField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://com/thomsonreuters/schemas/search")]
    public partial class CourtResultsPageResultGroupRecordDetailsCourtResponseDetail
    {

        public UCCRecord[] uCCRecordField;

        public LienJudgeMultipleRecord[] lienJudgeMultipleRecordField;

        public CourtResultsPageResultGroupRecordDetailsCourtResponseDetailDocumentGuids documentGuidsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("UCCRecord")]
        public UCCRecord[] UCCRecord
        {
            get
            {
                return this.uCCRecordField;
            }
            set
            {
                this.uCCRecordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("LienJudgeMultipleRecord")]
        public LienJudgeMultipleRecord[] LienJudgeMultipleRecord
        {
            get
            {
                return this.lienJudgeMultipleRecordField;
            }
            set
            {
                this.lienJudgeMultipleRecordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CourtResultsPageResultGroupRecordDetailsCourtResponseDetailDocumentGuids DocumentGuids
        {
            get
            {
                return this.documentGuidsField;
            }
            set
            {
                this.documentGuidsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://com/thomsonreuters/schemas/search")]
    public partial class CourtResultsPageResultGroupRecordDetailsCourtResponseDetailDocumentGuids
    {

        public string sourceNameField;

        public string sourceDocumentGuidsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string SourceName
        {
            get
            {
                return this.sourceNameField;
            }
            set
            {
                this.sourceNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string SourceDocumentGuids
        {
            get
            {
                return this.sourceDocumentGuidsField;
            }
            set
            {
                this.sourceDocumentGuidsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://clear.thomsonreuters.com/api/search/2.0", IsNullable = false)]
    public partial class NewDataSet
    {

        public CourtResultsPage[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CourtResultsPage")]
        public CourtResultsPage[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

}
