﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Response.PersonSearch
{

    [XmlRoot(ElementName = "Status")]
    public class Status
    {
        [XmlElement(ElementName = "StatusCode")]
        public string StatusCode { get; set; }
        [XmlElement(ElementName = "SubStatusCode")]
        public string SubStatusCode { get; set; }
    }

    [XmlRoot(ElementName = "Name")]
    public class Name
    {
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "AgeInfo")]
    public class AgeInfo
    {
        [XmlElement(ElementName = "PersonBirthDate")]
        public string PersonBirthDate { get; set; }
    }

    [XmlRoot(ElementName = "Address")]
    public class Address
    {
        [XmlElement(ElementName = "Street")]
        public string Street { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "ReportedDate")]
        public string ReportedDate { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "ZipCode")]
        public string ZipCode { get; set; }
        [XmlElement(ElementName = "County")]
        public string County { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "Latitude")]
        public string Latitude { get; set; }
        [XmlElement(ElementName = "Longitude")]
        public string Longitude { get; set; }
    }

    [XmlRoot(ElementName = "PersonDominantValues")]
    public class PersonDominantValues
    {
        [XmlElement(ElementName = "Name")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "SSN")]
        public string SSN { get; set; }
        [XmlElement(ElementName = "AgeInfo")]
        public AgeInfo AgeInfo { get; set; }
        [XmlElement(ElementName = "Address")]
        public Address Address { get; set; }
    }

    [XmlRoot(ElementName = "DominantValues")]
    public class DominantValues
    {
        [XmlElement(ElementName = "PersonDominantValues")]
        public PersonDominantValues PersonDominantValues { get; set; }
    }

    [XmlRoot(ElementName = "PersonProfile")]
    public class PersonProfile
    {
        [XmlElement(ElementName = "PersonBirthDate")]
        public List<string> PersonBirthDate { get; set; }
        [XmlElement(ElementName = "PersonDeathDate")]
        public string PersonDeathDate { get; set; }
        [XmlElement(ElementName = "PersonHeight")]
        public string PersonHeight { get; set; }
        [XmlElement(ElementName = "PersonSex")]
        public string PersonSex { get; set; }
        [XmlElement(ElementName = "PersonWeight")]
        public string PersonWeight { get; set; }
    }

    [XmlRoot(ElementName = "AKANames")]
    public class AKANames
    {
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }

    [XmlRoot(ElementName = "SourceInfo")]
    public class SourceInfo
    {
        [XmlElement(ElementName = "SourceName")]
        public string SourceName { get; set; }
        [XmlElement(ElementName = "SourceDocumentGuid")]
        public string SourceDocumentGuid { get; set; }
    }

    [XmlRoot(ElementName = "KnownAddresses")]
    public class KnownAddresses
    {
        [XmlElement(ElementName = "Address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "SourceInfo")]
        public SourceInfo SourceInfo { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalPhoneNumbers")]
    public class AdditionalPhoneNumbers
    {
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlElement(ElementName = "SourceInfo")]
        public SourceInfo SourceInfo { get; set; }
    }

    [XmlRoot(ElementName = "AllSourceDocuments")]
    public class AllSourceDocuments
    {
        [XmlElement(ElementName = "SourceName")]
        public string SourceName { get; set; }
        [XmlElement(ElementName = "SourceDocumentGuid")]
        public string SourceDocumentGuid { get; set; }
    }

    [XmlRoot(ElementName = "PersonResponseDetail")]
    public class PersonResponseDetail
    {
        [XmlElement(ElementName = "Name")]
        public Name Name { get; set; }
        [XmlElement(ElementName = "SSN")]
        public string SSN { get; set; }
        [XmlElement(ElementName = "PersonProfile")]
        public PersonProfile PersonProfile { get; set; }
        [XmlElement(ElementName = "AKANames")]
        public List<AKANames> AKANames { get; set; }
        [XmlElement(ElementName = "EmailAddress")]
        public List<string> EmailAddress { get; set; }
        [XmlElement(ElementName = "KnownAddresses")]
        public List<KnownAddresses> KnownAddresses { get; set; }
        [XmlElement(ElementName = "AdditionalPhoneNumbers")]
        public AdditionalPhoneNumbers AdditionalPhoneNumbers { get; set; }
        [XmlElement(ElementName = "PersonEntityId")]
        public string PersonEntityId { get; set; }
        [XmlElement(ElementName = "AllSourceDocuments")]
        public AllSourceDocuments AllSourceDocuments { get; set; }
    }

    [XmlRoot(ElementName = "RecordDetails")]
    public class RecordDetails
    {
        [XmlElement(ElementName = "PersonResponseDetail")]
        public PersonResponseDetail PersonResponseDetail { get; set; }
    }

    [XmlRoot(ElementName = "ResultGroup")]
    public class ResultGroup
    {
        [XmlElement(ElementName = "GroupId")]
        public string GroupId { get; set; }
        [XmlElement(ElementName = "RecordCount")]
        public string RecordCount { get; set; }
        [XmlElement(ElementName = "Relevance")]
        public string Relevance { get; set; }
        [XmlElement(ElementName = "DominantValues")]
        public DominantValues DominantValues { get; set; }
        [XmlElement(ElementName = "RecordDetails")]
        public RecordDetails RecordDetails { get; set; }
    }

    [XmlRoot(ElementName = "PersonResultsPage")]
    public class PersonSearchResultsPage
    {
        [XmlElement(ElementName = "Status")]
        public Status Status { get; set; }
        [XmlElement(ElementName = "StartIndex")]
        public string StartIndex { get; set; }
        [XmlElement(ElementName = "EndIndex")]
        public string EndIndex { get; set; }
        [XmlElement(ElementName = "ResultGroup")]
        public ResultGroup ResultGroup { get; set; }
    }



}
