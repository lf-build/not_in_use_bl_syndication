﻿using System.Xml.Serialization;
namespace LendFoundry.Syndication.Clear.Proxy.Response
{
    [XmlType(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    [XmlRoot(ElementName = "CourtResults", Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
        public partial class CourtSearchUrlResonse
    {
            [XmlElement(ElementName = "Status")]
            public StatusCodes Status { get; set; }
            [XmlElement(ElementName = "Uri")]
            public string Uri { get; set; }
            [XmlElement(ElementName = "GroupCount")]
            public string GroupCount { get; set; }
            [XmlAttribute(AttributeName = "ns2", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string ns2 { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    [XmlRoot(ElementName = "Status")]
    public partial class StatusCodes
    {
        [XmlElement(ElementName = "StatusCode")]
        public string StatusCode { get; set; }
        [XmlElement(ElementName = "SubStatusCode")]
        public string SubStatusCode { get; set; }
    }


}