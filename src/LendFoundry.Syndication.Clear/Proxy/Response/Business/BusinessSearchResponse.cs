﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Response.BusinessSearch
{

    [XmlRoot(ElementName = "Status")]
        public class Status
        {
            [XmlElement(ElementName = "StatusCode")]
            public string StatusCode { get; set; }
            [XmlElement(ElementName = "SubStatusCode")]
            public string SubStatusCode { get; set; }
        }

        [XmlRoot(ElementName = "Address")]
        public class Address
        {
            [XmlElement(ElementName = "Street")]
            public string Street { get; set; }
            [XmlElement(ElementName = "City")]
            public string City { get; set; }
            [XmlElement(ElementName = "State")]
            public string State { get; set; }
            [XmlElement(ElementName = "ZipCode")]
            public string ZipCode { get; set; }
            [XmlElement(ElementName = "County")]
            public string County { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "Latitude")]
            public string Latitude { get; set; }
            [XmlElement(ElementName = "Longitude")]
            public string Longitude { get; set; }
        }

        [XmlRoot(ElementName = "BusinessDominantValues")]
        public class BusinessDominantValues
        {
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "FileState")]
            public string FileState { get; set; }
        }

        [XmlRoot(ElementName = "DominantValues")]
        public class DominantValues
        {
            [XmlElement(ElementName = "BusinessDominantValues")]
            public BusinessDominantValues BusinessDominantValues { get; set; }
        }

        [XmlRoot(ElementName = "Phones")]
        public class Phones
        {
            [XmlElement(ElementName = "PhoneNumber")]
            public string PhoneNumber { get; set; }
        }

        [XmlRoot(ElementName = "BusinessExecutives")]
        public class BusinessExecutives
        {
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Title")]
            public string Title { get; set; }
        }

        [XmlRoot(ElementName = "ListOfDUNSNumbers")]
        public class ListOfDUNSNumbers
        {
            [XmlElement(ElementName = "DUNSNumber")]
            public List<string> DUNSNumber { get; set; }
        }

        [XmlRoot(ElementName = "AllSourceDocuments")]
        public class AllSourceDocuments
        {
            [XmlElement(ElementName = "SourceName")]
            public string SourceName { get; set; }
            [XmlElement(ElementName = "SourceDocumentGuid")]
            public string SourceDocumentGuid { get; set; }
        }

        [XmlRoot(ElementName = "BusinessResponseDetail")]
        public class BusinessResponseDetail
        {
            [XmlElement(ElementName = "BusinessName")]
            public string BusinessName { get; set; }
            [XmlElement(ElementName = "Address")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "Phones")]
            public Phones Phones { get; set; }
            [XmlElement(ElementName = "BusinessExecutives")]
            public List<BusinessExecutives> BusinessExecutives { get; set; }
            [XmlElement(ElementName = "ListOfDUNSNumbers")]
            public ListOfDUNSNumbers ListOfDUNSNumbers { get; set; }
            [XmlElement(ElementName = "LegalUltimateParent")]
            public string LegalUltimateParent { get; set; }
            [XmlElement(ElementName = "LegalImmediateParent")]
            public string LegalImmediateParent { get; set; }
            [XmlElement(ElementName = "CompanyEntityId")]
            public string CompanyEntityId { get; set; }
            [XmlElement(ElementName = "AllSourceDocuments")]
            public List<AllSourceDocuments> AllSourceDocuments { get; set; }
            [XmlAttribute(AttributeName = "ns2")]
            public string Ns2 { get; set; }
            [XmlElement(ElementName = "ListOfFileStates")]
            public ListOfFileStates ListOfFileStates { get; set; }
            [XmlElement(ElementName = "CorporationInfo")]
            public CorporationInfo CorporationInfo { get; set; }
            [XmlElement(ElementName = "UCCInfo")]
            public UCCInfo UCCInfo { get; set; }
        }

        [XmlRoot(ElementName = "RecordDetails")]
        public class RecordDetails
        {
            [XmlElement(ElementName = "BusinessResponseDetail")]
            public BusinessResponseDetail BusinessResponseDetail { get; set; }
        }

        [XmlRoot(ElementName = "ResultGroup")]
        public class ResultGroup
        {
            [XmlElement(ElementName = "GroupId")]
            public string GroupId { get; set; }
            [XmlElement(ElementName = "RecordCount")]
            public string RecordCount { get; set; }
            [XmlElement(ElementName = "Relevance")]
            public string Relevance { get; set; }
            [XmlElement(ElementName = "DominantValues")]
            public DominantValues DominantValues { get; set; }
            [XmlElement(ElementName = "RecordDetails")]
            public RecordDetails RecordDetails { get; set; }
        }

        [XmlRoot(ElementName = "ListOfFileStates")]
        public class ListOfFileStates
        {
            [XmlElement(ElementName = "FileState")]
            public string FileState { get; set; }
        }

        [XmlRoot(ElementName = "CorporationInfo")]
        public class CorporationInfo
        {
            [XmlElement(ElementName = "CorporationNumber")]
            public string CorporationNumber { get; set; }
            [XmlElement(ElementName = "StateOfIncorporation")]
            public string StateOfIncorporation { get; set; }
            [XmlElement(ElementName = "DateIncorporated")]
            public string DateIncorporated { get; set; }
        }

        [XmlRoot(ElementName = "UCCInfo")]
        public class UCCInfo
        {
            [XmlElement(ElementName = "UCCFilingDate")]
            public string UCCFilingDate { get; set; }
        }

        [XmlRoot(ElementName = "BusinessResultsPage")]
        public class BusinessSearchResponse
    {
            [XmlElement(ElementName = "Status")]
            public Status Status { get; set; }
            [XmlElement(ElementName = "StartIndex")]
            public string StartIndex { get; set; }
            [XmlElement(ElementName = "EndIndex")]
            public string EndIndex { get; set; }
            [XmlElement(ElementName = "ResultGroup")]
            public List<ResultGroup> ResultGroup { get; set; }
            [XmlAttribute(AttributeName = "ns2")]
            public string Ns2 { get; set; }
        }

    }

