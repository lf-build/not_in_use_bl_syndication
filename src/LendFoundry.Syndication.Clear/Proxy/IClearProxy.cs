﻿using LendFoundry.Syndication.Clear.Proxy.Response.BusinessReportResult;
using LendFoundry.Syndication.Clear.Proxy.Response.PersonReport;
using LendFoundry.Syndication.Clear.Request;
using LendFoundry.Syndication.Clear.Proxy.Response.PersonSearch;
using System.Threading.Tasks;
using LendFoundry.Syndication.Clear.Proxy.Response.BusinessSearch;

namespace LendFoundry.Syndication.Clear.Proxy
{
    public interface IClearProxy
    {
        Task<PersonReportDetails> GetPersonReport(string groupId);
        Task<PersonSearchResultsPage> SearchPerson(IPersonSearchRequest request);
        Task<BusinessSearchResponse> SearchBusiness(IBusinessSearchRequest request);
        Task<BusinessReportDetails> GetBusinessReport(string groupId);

    }
}
