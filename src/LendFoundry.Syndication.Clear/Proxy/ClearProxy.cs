﻿using LendFoundry.Syndication.Clear.Request;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Clear.Proxy.Response.BusinessReportResult;
using LendFoundry.Syndication.Clear.Proxy.Response.PersonReport;
using LendFoundry.Syndication.Clear.Proxy.Response.PersonSearch;
using LendFoundry.Syndication.Clear.Proxy.Response.BusinessSearch;
using LendFoundry.Syndication.Clear.Proxy.Response.PersonReportUrlResponse;
using LendFoundry.Syndication.Clear.Proxy.Response.BusinessReportUrlResponse;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using LendFoundry.Foundation.Logging;
namespace LendFoundry.Syndication.Clear.Proxy
{
    public class ClearProxy : IClearProxy
    {
        public ClearProxy(IClearConfiguration configuration, ILogger logger)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            Configuration = configuration;
            Logger = logger;
        }
        
        private IClearConfiguration Configuration { get; }
        private ILogger Logger { get; }

        public async Task<PersonSearchResultsPage> SearchPerson(IPersonSearchRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var personSearchRequest = new Request.PersonSearch.PersonSearchRequest(Configuration, request);
            
            Dictionary<string, string> namespaceList = new Dictionary<string, string>();
            namespaceList.Add("ps", "http://clear.thomsonreuters.com/api/search/2.0");
            string xmlRequest = XmlSerialization.Serialize(personSearchRequest, namespaceList);
            xmlRequest = xmlRequest.Replace("Xmlns", "xmlns");
            Logger.Info("Person serch xml request "+xmlRequest);
            var response = await ExecuteRequest<PersonResults>(Configuration.PersonSearchUrl, Method.POST , xmlRequest);
            Logger.Info("Person serch url response " + response);
            if (response!=null)
                return await ExecuteRequest<PersonSearchResultsPage>(response.Uri, Method.GET);
            throw new NotFoundException("Person not found");
        }

        public async Task<PersonReportDetails> GetPersonReport(string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));

            var personReportRequest = new Request.PersonReport.PersonReportRequest(Configuration, groupId);
            Dictionary<string, string> namespaceList = new Dictionary<string, string>();
            namespaceList.Add("pr", "http://clear.thomsonreuters.com/api/report/2.0");
            namespaceList.Add("rc", "com/thomsonreuters/schemas/person-report");
            string xmlRequest = XmlSerialization.Serialize(personReportRequest, namespaceList);
            Logger.Info("Person Report xml request " + xmlRequest);
            var response = await ExecuteRequest<PersonReportResults>(Configuration.PersonReportUrl, Method.POST, xmlRequest);
            Logger.Info("Person report url response " + response);
            if (response != null)
                return await ExecuteRequest<PersonReportDetails>(response.Uri, Method.GET, null, Configuration.PersonReportType);
            throw new NotFoundException("Person not found");
        }

        public async Task<BusinessSearchResponse> SearchBusiness(IBusinessSearchRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var businessSearchRequest = new Request.BusinessSearch.BusinessSearchRequest(Configuration, request);
            string xmlRequest = XmlSerialization.Serialize(businessSearchRequest,null);
            var response = await ExecuteRequest<BusinessResults>(Configuration.BusinessSearchUrl, Method.POST, xmlRequest);
            if (response != null)
                return await ExecuteRequest<BusinessSearchResponse>(response.Uri, Method.GET);
            throw new NotFoundException("Business not found");
        }

        public async Task<BusinessReportDetails> GetBusinessReport(string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));

            var businessReportRequest = new Request.BusinessReport.BusinessReportRequest(Configuration, groupId);
            string xmlRequest = XmlSerialization.Serialize(businessReportRequest,null);
            var response = await ExecuteRequest<BusinessReportResults>(Configuration.BusinessReportUrl, Method.POST, xmlRequest);
            if (response != null)
                return await ExecuteRequest<BusinessReportDetails>(response.Uri, Method.GET);
            throw new NotFoundException("Business not found");
        }

        private async Task<TResponse> ExecuteRequest<TResponse>(string uri,Method method, object request = null,string personReportType="")
        {
            var baseUri = new Uri(uri);

            var restRequest = new RestRequest(method) { RequestFormat = DataFormat.Xml };

            restRequest.Credentials = new NetworkCredential(Configuration.UserName,Configuration.Password);

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl) && Configuration.UseProxy)
                baseUri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");

            if (method == Method.POST)            
                restRequest.AddParameter("application/xml", request, ParameterType.RequestBody);

            if (!string.IsNullOrWhiteSpace(personReportType) && method == Method.GET)
                restRequest.AddQueryParameter("reportType",Configuration.PersonReportType);

            var client = new RestClient(baseUri);

            if (!string.IsNullOrWhiteSpace(Configuration.Certificate))
            {
                Logger.Info("Adding certificate");
                var certificateRowData = Convert.FromBase64String(Configuration.Certificate);
                var certificates = new X509Certificate2();
                certificates.Import(certificateRowData,
                    Configuration.CertificatePassword, X509KeyStorageFlags.DefaultKeySet);

                client.ClientCertificates = new X509Certificate2Collection
                    {
                        new X509Certificate2(certificateRowData,
                            Configuration.CertificatePassword)
                    };
                Logger.Info("Client Certificate added");
            }

            var response = await client.ExecuteTaskAsync(restRequest);
            Logger.Info("Getting response from service"+ response.Content,"-----"+response.ErrorMessage);
            if (response.ErrorException != null)
                throw new Exception(response.ErrorMessage);

            if (response.ResponseStatus != ResponseStatus.Completed || response.StatusCode != HttpStatusCode.OK)
                throw new Exception(response.ErrorMessage);

            Logger.Info("Clear Response"+response);

            return XmlSerialization.Deserialize<TResponse>(response.Content);
        }
    }
}
