﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Request.PersonReport
{

    [XmlRoot(ElementName = "PermissiblePurpose")]
        public partial class PermissiblePurpose
        {
            [XmlElement(ElementName = "GLB")]
            public string Glb { get; set; }
            [XmlElement(ElementName =  "DPPA")]
            public string Dppa { get; set; }
            [XmlElement(ElementName = "VOTER" )]
            public string Voter { get; set; }
        }

        [XmlRoot(ElementName = "ReportOptions")]
        public partial class ReportOptions
        {
            [XmlElement(ElementName = "AssociatesSectionAddressOption")]
            public string AssociatesSectionAddressOption { get; set; }
            [XmlElement(ElementName = "AssociatesSectionTimeframeOption")]
            public string AssociatesSectionTimeframeOption { get; set; }
            [XmlElement(ElementName = "AssociateSectionDisplayOption")]
            public string AssociateSectionDisplayOption { get; set; }
            [XmlElement(ElementName = "NeighborsTypeOption")]
            public string NeighborsTypeOption { get; set; }
            [XmlElement(ElementName = "NeighborsTimeframeOption")]
            public string NeighborsTimeframeOption { get; set; }
            [XmlElement(ElementName = "RelativesSectionDegreeOption")]
            public string RelativesSectionDegreeOption { get; set; }
            [XmlElement(ElementName = "RelativesSectionDisplayOption")]
            public string RelativesSectionDisplayOption { get; set; }
            [XmlElement(ElementName = "VehiclesSectionLimitAssocOption")]
            public bool VehiclesSectionLimitAssocOption { get; set; }
            [XmlElement(ElementName = "VehiclesSectionLimitYearOption")]
            public bool VehiclesSectionLimitYearOption { get; set; }
            [XmlElement(ElementName = "AddressSectionAddressOption")]
            public string AddressSectionAddressOption { get; set; }
            [XmlElement(ElementName = "BusinessAtSubjectAddressSectionAddressOption")]
            public string BusinessAtSubjectAddressSectionAddressOption { get; set; }
            [XmlElement(ElementName = "LicenseSectionAddressOption")]
            public string LicenseSectionAddressOption { get; set; }
            [XmlElement(ElementName = "LicenseSectionTimeframeOption")]
            public string LicenseSectionTimeframeOption { get; set; }
            [XmlElement(ElementName = "PropertyOwnerSectionAddressOption")]
            public string PropertyOwnerSectionAddressOption { get; set; }
            [XmlElement(ElementName = "VehiclesAtSubjectAddressSectionAddressOption")]
            public string VehiclesAtSubjectAddressSectionAddressOption { get; set; }
            [XmlElement(ElementName = "VehiclesAtSubjectAddressSectionTimeframeOption")]
            public string VehiclesAtSubjectAddressSectionTimeframeOption { get; set; }
            [XmlElement(ElementName = "PropertyOwnerSectionLimitAssocOption")]
            public bool PropertyOwnerSectionLimitAssocOption { get; set; }
        }

        [XmlRoot(ElementName = "ReportSections")]
    public partial class ReportSections
    {
        [XmlElement(ElementName = "AddressSection")]
        public bool AddressSection { get; set; }
        [XmlElement(ElementName = "DeathSection")]
        public bool DeathSection { get; set; }
        [XmlElement(ElementName = "WorkAffiliationSection")]
        public bool WorkAffiliationSection { get; set; }
        [XmlElement(ElementName = "UtilitySection")]
        public bool UtilitySection { get; set; }
        [XmlElement(ElementName = "SSNAddressFraudSection")]
        public bool SSNAddressFraudSection { get; set; }
        [XmlElement(ElementName = "OtherSSNSection")]
        public bool OtherSSNSection { get; set; }
        [XmlElement(ElementName = "OtherNamesforSSNSection")]
        public bool OtherNamesforSSNSection { get; set; }
        [XmlElement(ElementName = "PhoneNumberSection")]
        public bool PhoneNumberSection { get; set; }
        [XmlElement(ElementName = "PhoneListingSection")]
        public bool PhoneListingSection { get; set; }
        [XmlElement(ElementName = "CanadianPhoneSection")]
        public bool CanadianPhoneSection { get; set; }
        [XmlElement(ElementName = "EmailSection")]
        public bool EmailSection { get; set; }
        [XmlElement(ElementName = "DriverLicenseSection")]
        public bool DriverLicenseSection { get; set; }
        [XmlElement(ElementName = "DivorceSection")]
        public bool DivorceSection { get; set; }
        [XmlElement(ElementName = "LicenseSection")]
        public bool LicenseSection { get; set; }
        [XmlElement(ElementName = "HealthcareLicenseSection")]
        public bool HealthcareLicenseSection { get; set; }
        [XmlElement(ElementName = "NPISection")]
        public bool NPISection { get; set; }
        [XmlElement(ElementName = "MilitarySection")]
        public bool MilitarySection { get; set; }
        [XmlElement(ElementName = "PoliticalDonorSection")]
        public bool PoliticalDonorSection { get; set; }
        [XmlElement(ElementName = "VoterRegistrationSection")]
        public bool VoterRegistrationSection { get; set; }
        [XmlElement(ElementName = "DriversAtSubjectAddressSection")]
        public bool DriversAtSubjectAddressSection { get; set; }
        [XmlElement(ElementName = "GlobalSanctionSection")]
        public bool GlobalSanctionSection { get; set; }
        [XmlElement(ElementName = "HealthcareSanctionSection")]
        public bool HealthcareSanctionSection { get; set; }
        [XmlElement(ElementName = "ExcludedPartySection")]
        public bool ExcludedPartySection { get; set; }
        [XmlElement(ElementName = "WorldCheckSection")]
        public bool WorldCheckSection { get; set; }
        [XmlElement(ElementName = "InfractionSection")]
        public bool InfractionSection { get; set; }
        [XmlElement(ElementName = "CriminalSection")]
        public bool CriminalSection { get; set; }
        [XmlElement(ElementName = "RealTimeArrestSection")]
        public bool RealTimeArrestSection { get; set; }
        [XmlElement(ElementName = "ArrestSection")]
        public bool ArrestSection { get; set; }
        [XmlElement(ElementName = "ExecutiveAffiliationSection")]
        public bool ExecutiveAffiliationSection { get; set; }
        [XmlElement(ElementName = "DunBradstreetSection")]
        public bool DunBradstreetSection { get; set; }
        [XmlElement(ElementName = "ShareholderSection")]
        public bool ShareholderSection { get; set; }
        [XmlElement(ElementName = "BusinessAtSubjectAddressSection")]
        public bool BusinessAtSubjectAddressSection { get; set; }
        [XmlElement(ElementName = "LienJudgmentSection")]
        public bool LienJudgmentSection { get; set; }
        [XmlElement(ElementName = "BankruptcySection")]
        public bool BankruptcySection { get; set; }
        [XmlElement(ElementName = "LawsuitSection")]
        public bool LawsuitSection { get; set; }
        [XmlElement(ElementName = "DocketSection")]
        public bool DocketSection { get; set; }
        [XmlElement(ElementName = "CorporateSection")]
        public bool CorporateSection { get; set; }
        [XmlElement(ElementName = "UCCSection")]
        public bool UCCSection { get; set; }
        [XmlElement(ElementName = "RealPropertySection")]
        public bool RealPropertySection { get; set; }
        [XmlElement(ElementName = "PropertyOwnerSection")]
        public bool PropertyOwnerSection { get; set; }
        [XmlElement(ElementName = "PreForeclosureSection")]
        public bool PreForeclosureSection { get; set; }
        [XmlElement(ElementName = "RealTimeVehicleSection")]
        public bool RealTimeVehicleSection { get; set; }
        [XmlElement(ElementName = "VehicleSection")]
        public bool VehicleSection { get; set; }
        [XmlElement(ElementName = "VehiclesAtSubjectAddressSection")]
        public bool VehiclesAtSubjectAddressSection { get; set; }
        [XmlElement(ElementName = "WatercraftSection")]
        public bool WatercraftSection { get; set; }
        [XmlElement(ElementName = "AircraftSection")]
        public bool AircraftSection { get; set; }
        [XmlElement(ElementName = "UnclaimedAssetSection")]
        public bool UnclaimedAssetSection { get; set; }
        [XmlElement(ElementName = "RelativeSection")]
        public bool RelativeSection { get; set; }
        [XmlElement(ElementName = "AssociateSection")]
        public bool AssociateSection { get; set; }
        [XmlElement(ElementName = "NeighborSection")]
        public bool NeighborSection { get; set; }
        [XmlElement(ElementName = "QuickAnalysisFlagSection")]
        public bool QuickAnalysisFlagSection { get; set; }
        [XmlElement(ElementName = "AssociateAnalyticsChartSection")]
        public bool AssociateAnalyticsChartSection { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "")]
    [XmlRoot(ElementName = "ReportCriteria", Namespace = "com/thomsonreuters/schemas/person-report")]
        public partial class ReportCriteria
        {
            [XmlElement(ElementName = "GroupID")]
            public string GroupID { get; set; }
            [XmlElement(ElementName = "ReportChoice")]
            public string ReportChoice { get; set; }
            [XmlElement(ElementName = "ReportOptions")]
            public ReportOptions ReportOptions { get; set; }
            [XmlElement(ElementName = "ReportSections")]
            public ReportSections ReportSections { get; set; }
        [XmlAttribute(AttributeName = "rc", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Rc { get; set; } = "com/thomsonreuters/schemas/person-report";
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; } 
    }

        [XmlRoot(ElementName = "Criteria")]
        public partial class Criteria
        {
            [XmlElement(ElementName = "ReportCriteria", Namespace = "com/thomsonreuters/schemas/person-report")]
            public ReportCriteria ReportCriteria { get; set; }
        }

    [XmlType(AnonymousType = true, Namespace = "")]
    [XmlRoot(ElementName = "PersonReportRequest", Namespace = "http://clear.thomsonreuters.com/api/report/2.0")]
        public partial class PersonReportRequest
        {
            [XmlElement(ElementName = "PermissiblePurpose")]
            public PermissiblePurpose PermissiblePurpose { get; set; }
            [XmlElement(ElementName = "Reference")]
            public string Reference { get; set; }
            [XmlElement(ElementName = "Criteria")]
            public Criteria Criteria { get; set; }
        }

    }

