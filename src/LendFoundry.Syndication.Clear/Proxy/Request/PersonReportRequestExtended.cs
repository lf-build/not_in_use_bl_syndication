﻿using System;

namespace LendFoundry.Syndication.Clear.Proxy.Request.PersonReport
{
    public partial class PermissiblePurpose
    {
        public PermissiblePurpose()
        {

        }
        public PermissiblePurpose(IClearConfiguration configuration)
        {

            Glb = configuration.Glb;
            Dppa = configuration.Dppa;
            Voter = configuration.Voter;
        }
    }
        public partial class ReportOptions
        {
            public ReportOptions()
            {

            }
        public ReportOptions(IClearConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.PersonReportConfiguration != null)
            {
                AssociatesSectionAddressOption = configuration.PersonReportConfiguration.AssociatesSectionAddressOption;
                AssociatesSectionAddressOption = configuration.PersonReportConfiguration.AssociatesSectionAddressOption;
                AssociatesSectionTimeframeOption = configuration.PersonReportConfiguration.AssociatesSectionTimeframeOption;
                AssociateSectionDisplayOption = configuration.PersonReportConfiguration.AssociateSectionDisplayOption;
                NeighborsTypeOption = configuration.PersonReportConfiguration.NeighborsTypeOption;
                NeighborsTimeframeOption = configuration.PersonReportConfiguration.NeighborsTimeframeOption;
                RelativesSectionDegreeOption = configuration.PersonReportConfiguration.RelativesSectionDegreeOption;
                RelativesSectionDisplayOption = configuration.PersonReportConfiguration.RelativesSectionDisplayOption;
                VehiclesSectionLimitAssocOption = configuration.PersonReportConfiguration.VehiclesSectionLimitAssocOption;
                VehiclesSectionLimitYearOption = configuration.PersonReportConfiguration.VehiclesSectionLimitYearOption;
                AddressSectionAddressOption = configuration.PersonReportConfiguration.AddressSectionAddressOption;
                BusinessAtSubjectAddressSectionAddressOption = configuration.PersonReportConfiguration.BusinessAtSubjectAddressSectionAddressOption;
                LicenseSectionAddressOption = configuration.PersonReportConfiguration.LicenseSectionAddressOption;
                LicenseSectionTimeframeOption = configuration.PersonReportConfiguration.LicenseSectionTimeframeOption;
                PropertyOwnerSectionAddressOption = configuration.PersonReportConfiguration.PropertyOwnerSectionAddressOption;
                VehiclesAtSubjectAddressSectionAddressOption = configuration.PersonReportConfiguration.VehiclesAtSubjectAddressSectionAddressOption;
                VehiclesAtSubjectAddressSectionTimeframeOption = configuration.PersonReportConfiguration.VehiclesAtSubjectAddressSectionTimeframeOption;
                PropertyOwnerSectionLimitAssocOption = configuration.PersonReportConfiguration.PropertyOwnerSectionLimitAssocOption;
            }
        }
        }
    public partial class ReportSections
    {
        public ReportSections()
        {

        }
        public ReportSections(IClearConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.PersonReportConfiguration != null)
            {
                AddressSection = configuration.PersonReportConfiguration.AddressSection;
                DeathSection = configuration.PersonReportConfiguration.DeathSection;
                WorkAffiliationSection = configuration.PersonReportConfiguration.WorkAffiliationSection;
                UtilitySection = configuration.PersonReportConfiguration.UtilitySection;
                SSNAddressFraudSection = configuration.PersonReportConfiguration.SSNAddressFraudSection;
                OtherSSNSection = configuration.PersonReportConfiguration.OtherSSNSection;
                OtherNamesforSSNSection = configuration.PersonReportConfiguration.OtherSSNSection;
                PhoneNumberSection = configuration.PersonReportConfiguration.PhoneNumberSection;
                PhoneListingSection = configuration.PersonReportConfiguration.PhoneListingSection;
                CanadianPhoneSection = configuration.PersonReportConfiguration.CanadianPhoneSection;
                EmailSection = configuration.PersonReportConfiguration.EmailSection;
                DriverLicenseSection = configuration.PersonReportConfiguration.DriverLicenseSection;
                DivorceSection = configuration.PersonReportConfiguration.DivorceSection;
                LicenseSection = configuration.PersonReportConfiguration.LicenseSection;
                HealthcareLicenseSection = configuration.PersonReportConfiguration.HealthcareLicenseSection;
                NPISection = configuration.PersonReportConfiguration.NPISection;
                MilitarySection = configuration.PersonReportConfiguration.MilitarySection;
                         PoliticalDonorSection = configuration.PersonReportConfiguration.PoliticalDonorSection;
                         VoterRegistrationSection = configuration.PersonReportConfiguration.VoterRegistrationSection;
                         DriversAtSubjectAddressSection = configuration.PersonReportConfiguration.DriversAtSubjectAddressSection;
                         GlobalSanctionSection = configuration.PersonReportConfiguration.GlobalSanctionSection;
                         HealthcareSanctionSection = configuration.PersonReportConfiguration.HealthcareSanctionSection;
                         ExcludedPartySection = configuration.PersonReportConfiguration.ExcludedPartySection;
                         WorldCheckSection = configuration.PersonReportConfiguration.WorldCheckSection;
                         InfractionSection = configuration.PersonReportConfiguration.InfractionSection;
                         CriminalSection = configuration.PersonReportConfiguration.CriminalSection;
                         RealTimeArrestSection = configuration.PersonReportConfiguration.RealTimeArrestSection;
                         ArrestSection = configuration.PersonReportConfiguration.ArrestSection;
                         ExecutiveAffiliationSection = configuration.PersonReportConfiguration.ExecutiveAffiliationSection;
                         DunBradstreetSection = configuration.PersonReportConfiguration.DunBradstreetSection;
                         ShareholderSection = configuration.PersonReportConfiguration.ShareholderSection;
                         BusinessAtSubjectAddressSection = configuration.PersonReportConfiguration.BusinessAtSubjectAddressSection;
                         LienJudgmentSection = configuration.PersonReportConfiguration.LienJudgmentSection;
                         BankruptcySection = configuration.PersonReportConfiguration.BankruptcySection;
                         LawsuitSection = configuration.PersonReportConfiguration.LawsuitSection;
                         DocketSection = configuration.PersonReportConfiguration.DocketSection;
                         CorporateSection = configuration.PersonReportConfiguration.CorporateSection;
                         UCCSection = configuration.PersonReportConfiguration.UCCSection;
                         RealPropertySection = configuration.PersonReportConfiguration.RealPropertySection;
                         PropertyOwnerSection = configuration.PersonReportConfiguration.PropertyOwnerSection;
                         PreForeclosureSection = configuration.PersonReportConfiguration.PreForeclosureSection;
                         RealTimeVehicleSection = configuration.PersonReportConfiguration.RealTimeVehicleSection;
                         VehicleSection = configuration.PersonReportConfiguration.VehicleSection;
                         VehiclesAtSubjectAddressSection = configuration.PersonReportConfiguration.VehiclesAtSubjectAddressSection;
                         WatercraftSection = configuration.PersonReportConfiguration.WatercraftSection;
                         AircraftSection = configuration.PersonReportConfiguration.AircraftSection;
                         UnclaimedAssetSection = configuration.PersonReportConfiguration.UnclaimedAssetSection;
                RelativeSection = configuration.PersonReportConfiguration.RelativeSection;
                AssociateSection = configuration.PersonReportConfiguration.AssociateSection;
                NeighborSection = configuration.PersonReportConfiguration.NeighborSection;
                QuickAnalysisFlagSection = configuration.PersonReportConfiguration.QuickAnalysisFlagSection;
                AssociateAnalyticsChartSection = configuration.PersonReportConfiguration.AssociateAnalyticsChartSection;
            }
        }
    }
    public partial class ReportCriteria
    {
        public ReportCriteria()
        {
        }
        public ReportCriteria(IClearConfiguration configuration,string groupId)
        {
            if (!string.IsNullOrWhiteSpace(groupId))
                GroupID = groupId;

            if (configuration != null)
            {
                ReportChoice = configuration.PersonReportConfiguration.ReportChoice;
                ReportSections = new ReportSections(configuration);
                ReportOptions = new ReportOptions(configuration);
            }
           
        }
    }
    public partial class Criteria
    {
        public Criteria()
        {

        }
        public Criteria(IClearConfiguration configuration,string groupId)
        {
            if (configuration != null)
            {
                ReportCriteria = new ReportCriteria(configuration, groupId);
            }
        }
    }
    public partial class PersonReportRequest
    {
        public PersonReportRequest()
        {
            
        }
        public PersonReportRequest(IClearConfiguration configuration,string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));

            if (configuration!=null)
            {
                PermissiblePurpose = new PermissiblePurpose(configuration);
                Reference = configuration.PersonReportConfiguration.Reference;
                Criteria = new Criteria(configuration,groupId);
            }
        }

    }
}