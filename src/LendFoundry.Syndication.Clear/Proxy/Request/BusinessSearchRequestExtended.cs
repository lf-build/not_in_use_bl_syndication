﻿using LendFoundry.Syndication.Clear.Request;

namespace LendFoundry.Syndication.Clear.Proxy.Request.BusinessSearch
{

    public partial class PermissiblePurpose 
    {
        public PermissiblePurpose()
        {

        }
        public PermissiblePurpose(IClearConfiguration configuration)
        {
            if (configuration != null)
            {
                Glb = configuration.Glb;
                Dppa = configuration.Dppa;
                Voter = configuration.Voter;
            }

        }
    }

    public partial class CorporationInfo
    {
        public CorporationInfo()
        {

        }
        public CorporationInfo(IBusinessSearchRequest request)
        {
            if (request != null)
            {
                CorporationId = request.CorporationId;
                FilingNumber = request.FilingNumber;
                FilingDate = request.FilingDate;
                FEIN = request.FEIN;
                DUNSNumber = request.DUNSNumber;
            }
        }
       
    }

    public partial class AdvancedNameSearch
    {
        public AdvancedNameSearch()
        {

        }
        public AdvancedNameSearch(IBusinessSearchRequest request)
        {
            if (request!=null)
            {
                LastSecondaryNameSoundSimilarOption = request.LastSecondaryNameSoundSimilarOption;
                SecondaryLastNameOption = request.SecondaryLastNameOption;
                FirstNameSoundSimilarOption = request.FirstNameSoundSimilarOption;
                FirstNameVariationsOption = request.FirstNameVariationsOption;
            }
        }
        
    }

    public partial class NameInfo
    {
        public NameInfo()
        {
                
        }
        public NameInfo(IBusinessSearchRequest request)
        {
            if (request!=null)
            {
                AdvancedNameSearch = new AdvancedNameSearch(request);
                LastName = request.LastName;
                FirstName = request.FirstName;
                MiddleInitial = request.MiddleInitial;
                SecondaryLastName = request.SecondaryLastName;
            }
        }
       
    }

    public partial class AddressInfo
    {
        public AddressInfo()
        {

        }
        public AddressInfo(IBusinessSearchRequest request)
        {
            if (request!=null)
            {
                StreetNamesSoundSimilarOption = request.StreetNamesSoundSimilarOption;
                Street = request.Street;
                City = request.City;
                State = request.State;
                County = request.Country;
                ZipCode = request.ZipCode;
                Province = request.Province;
                Country = request.Country;
            }
        }
      
    }

    public partial class BusinessCriteria
    {
        public BusinessCriteria()
        {

        }
        public BusinessCriteria(IBusinessSearchRequest request)
        {
            if (request!=null)
            {
                NameInfo = new NameInfo(request);
                AddressInfo = new AddressInfo(request);
                BusinessName = request.BusinessName;
                CorporationInfo = new CorporationInfo(request);
                NPINumber =request.NPINumber;
                PhoneNumber = request.PhoneNumber;
            }

        }
        
    }

    public partial class Criteria
    {
        public Criteria()
        {

        }
        public Criteria(IBusinessSearchRequest request)
        {
            if (request!=null)
            {
                BusinessCriteria = new BusinessCriteria(request);
            }
        }
        
    }

    public partial class Datasources
    {
        public Datasources()
        {

        }
        public Datasources(IClearConfiguration configuration)
        {
            if (configuration!=null)
            {
                PublicRecordBusiness = configuration.BusinessSearchConfiguration.PublicRecordBusiness;
                NPIRecord = configuration.BusinessSearchConfiguration.NPIRecord;
                PublicRecordUCCFilings = configuration.BusinessSearchConfiguration.PublicRecordUCCFilings;
                WorldCheckRiskIntelligence = configuration.BusinessSearchConfiguration.WorldCheckRiskIntelligence;
            }
             
        }
       
    }

    public partial class BusinessSearchRequest
    {
        public BusinessSearchRequest()
        {

        }
        public BusinessSearchRequest(IClearConfiguration configuration,IBusinessSearchRequest request)
        {
            if (configuration!=null && request!=null)
            {
                PermissiblePurpose = new PermissiblePurpose(configuration);
                Reference = request.Reference;
                Criteria = new Criteria(request);
                Datasources = new Datasources(configuration);
            }
        }
   
    }

}
