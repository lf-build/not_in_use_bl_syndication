﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Request.PersonSearch
{
    [XmlRoot(ElementName = "PermissiblePurpose")]
    public partial class PermissiblePurpose
    {

        [XmlElement(ElementName = "GLB")]
        public string Glb { get; set; }
        [XmlElement(ElementName = "DPPA")]
        public string Dppa { get; set; }
        [XmlElement(ElementName = "VOTER")]
        public string Voter { get; set; }
    }

    [XmlRoot(ElementName = "AdvancedNameSearch")]
    public partial class AdvancedNameSearch
    {
        [XmlElement(ElementName = "LastSecondaryNameSoundSimilarOption")]
        public string LastSecondaryNameSoundSimilarOption { get; set; }
        [XmlElement(ElementName = "SecondaryLastNameOption")]
        public string SecondaryLastNameOption { get; set; }
        [XmlElement(ElementName = "FirstNameBeginsWithOption")]
        public string FirstNameBeginsWithOption { get; set; }
        [XmlElement(ElementName = "FirstNameSoundSimilarOption")]
        public string FirstNameSoundSimilarOption { get; set; }
        [XmlElement(ElementName = "FirstNameExactMatchOption")]
        public string FirstNameExactMatchOption { get; set; }
        [XmlAttribute(AttributeName = "xmlns", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xmlns { get; set; }

    }

    [XmlRoot(ElementName = "NameInfo")]
    public partial class NameInfo
    {
        [XmlElement(ElementName = "AdvancedNameSearch")]
        public AdvancedNameSearch AdvancedNameSearch { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleInitial")]
        public string MiddleInitial { get; set; }
        [XmlElement(ElementName = "SecondaryLastName")]
        public string SecondaryLastName { get; set; }
        [XmlAttribute]
        public string Xmlns { get; set; } = "";

    }

    [XmlRoot(ElementName = "AddressInfo")]
    public partial class AddressInfo
    {
        [XmlElement(ElementName = "Street")]
        public string Street { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "County")]
        public string County { get; set; }
        [XmlElement(ElementName = "ZipCode")]
        public string ZipCode { get; set; }
        [XmlElement(ElementName = "Province")]
        public string Province { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
        [XmlAttribute]
        public string Xmlns { get; set; } = "";

    }

    [XmlRoot(ElementName = "AgeInfo")]
    public partial class AgeInfo
    {
        [XmlElement(ElementName = "PersonBirthDate")]
        public string PersonBirthDate { get; set; }
        [XmlElement(ElementName = "PersonAgeTo")]
        public string PersonAgeTo { get; set; }
        [XmlElement(ElementName = "PersonAgeFrom")]
        public string PersonAgeFrom { get; set; }
        [XmlAttribute]
        public string Xmlns { get; set; } = "";

    }

    [XmlRoot(ElementName = "PersonCriteria")]
    public partial class PersonCriteria
    {
        [XmlElement(ElementName = "NameInfo")]
        public NameInfo NameInfo { get; set; }
        [XmlElement(ElementName = "AddressInfo")]
        public AddressInfo AddressInfo { get; set; }
        [XmlElement(ElementName = "EmailAddress")]
        public string EmailAddress { get; set; }
        [XmlElement(ElementName = "NPINumber")]
        public string NPINumber { get; set; }
        [XmlElement(ElementName = "SSN")]
        public string SSN { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlElement(ElementName = "AgeInfo", Namespace = "")]
        public AgeInfo AgeInfo { get; set; }
        [XmlElement(ElementName = "DriverLicenseNumber")]
        public string DriverLicenseNumber { get; set; }
      //  [XmlElement(ElementName = "PersonEntityId")]
      //  public PersonEntityId PersonEntityId { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; } = "http://www.w3.org/2001/XMLSchema-instance";
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; } = "http://www.w3.org/2001/XMLSchema";
        [XmlAttribute]
        public string Xmlns { get; set; } = "com/thomsonreuters/schemas/search";
    }

    [XmlRoot(ElementName = "Criteria")]
    public partial class Criteria
    {
        [XmlElement(ElementName = "PersonCriteria")]
        public PersonCriteria PersonCriteria { get; set; }
    }

    //[XmlRoot(ElementName = "PersonEntityId")]
    //public partial class PersonEntityId
    //{
    //    [XmlAttribute]
    //    public string Xmlns { get; set; } = "";

    //}

    [XmlRoot(ElementName = "Datasources")]
    public partial class Datasources
    {
        [XmlElement(ElementName = "PublicRecordPeople")]
        public bool PublicRecordPeople { get; set; }
        [XmlElement(ElementName = "NPIRecord")]
        public bool NPIRecord { get; set; }
        [XmlElement(ElementName = "RealTimeIncarcerationAndArrests")]
        public bool RealTimeIncarcerationAndArrests { get; set; }
        [XmlElement(ElementName = "WorldCheckRiskIntelligence")]
        public bool WorldCheckRiskIntelligence { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "")]
    [XmlRoot(ElementName = "PersonSearchRequest", Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class PersonSearchRequest
    {

        [XmlElement(ElementName = "PermissiblePurpose")]
        public PermissiblePurpose PermissiblePurpose { get; set; }
        [XmlElement(ElementName = "Reference")]
        public string Reference { get; set; }
        [XmlElement(ElementName = "Criteria")]
        public Criteria Criteria { get; set; }
        [XmlElement(ElementName = "Datasources")]
        public Datasources Datasources { get; set; }
        [XmlAttribute(AttributeName = "p1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string p1 { get; set; } = "com/thomsonreuters/schemas/search";
    }
}



