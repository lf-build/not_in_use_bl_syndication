﻿using LendFoundry.Syndication.Clear.Request;

namespace LendFoundry.Syndication.Clear.Proxy.Request.PersonSearch
{
    public partial class PermissiblePurpose 
    {
        public PermissiblePurpose()
        {

        }
        public PermissiblePurpose(IClearConfiguration configuration)
        {
            if (configuration != null)
            {
                Glb = configuration.Glb;
                Dppa = configuration.Dppa;
                Voter = configuration.Voter;
            }

        }
    }

    public partial class AdvancedNameSearch
    {
        public AdvancedNameSearch()
        {

        }
        public AdvancedNameSearch(IPersonSearchRequest request)
        {
            if (request != null)
            {
                LastSecondaryNameSoundSimilarOption = request.LastSecondaryNameSoundSimilarOption;
                SecondaryLastNameOption = request.SecondaryLastNameOption;
                FirstNameSoundSimilarOption = request.FirstNameSoundSimilarOption;
                FirstNameBeginsWithOption = request.FirstNameBeginsWithOption;
                FirstNameExactMatchOption = request.FirstNameExactMatchOption;
            }
        }
    }

    public partial class NameInfo
    {
        public NameInfo()
        {

        }
        public NameInfo(IPersonSearchRequest request)
        {
            if (request != null)
            {
                AdvancedNameSearch = new AdvancedNameSearch(request);
                LastName = request.LastName;
                FirstName = request.FirstName;
                MiddleInitial = request.MiddleInitial;
                SecondaryLastName = request.SecondaryLastName;
            }
        }
    }

    public partial class AddressInfo
    {
        public AddressInfo()
        {

        }
        public AddressInfo(IPersonSearchRequest request)
        {
            if (request != null)
            {
                Street = request.Street;
                City = request.City;
                State = request.State;
                ZipCode = request.ZipCode;
                Province = request.Province;
                Country = request.Country;
            }
        }
    }

    
    public partial class AgeInfo
    {
        public AgeInfo()
        {

        }
        public AgeInfo(IPersonSearchRequest request)
        {
            if (request!=null)
            {
                PersonBirthDate = request.PersonBirthDate;
                PersonAgeTo = request.PersonAgeTo;
                PersonAgeFrom = request.PersonAgeFrom;


            }
        }
      
    }

    public partial class PersonCriteria
    {
        public PersonCriteria()
        {

        }
        public PersonCriteria(IPersonSearchRequest request)
        {
            if (request!=null)
            {
                NameInfo = new NameInfo(request);
                AddressInfo = new AddressInfo(request);
                EmailAddress = request.EmailAddress;
                NPINumber = request.NPINumber;
                SSN = request.SSN;
                PhoneNumber = request.PhoneNumber;
                AgeInfo = new AgeInfo(request);
                DriverLicenseNumber = request.DriverLicenseNumber;
              //  PersonEntityId = new PersonEntityId(request);
            }
        }

      
    }
    public partial class Criteria
    {
        public Criteria()
        {

        }
        public Criteria(IPersonSearchRequest request)
        {
            if (request != null)
            {
                PersonCriteria = new PersonCriteria(request);
            }
        }
    }

    //public partial class PersonEntityId
    //{
    //    public PersonEntityId()
    //    {

    //    }
    //    public PersonEntityId(IPersonSearchRequest request)
    //    {
    //        if (request != null)
    //        {
    //        }
    //    }
    //}

    public partial class Datasources
    {
        public Datasources()
        {

        }
        public Datasources(IClearConfiguration configuration)
        {
            if (configuration != null)
            {
                PublicRecordPeople = configuration.PersonSearchConfiguration.PublicRecordPeople;
                NPIRecord = configuration.PersonSearchConfiguration.NPIRecord;
                RealTimeIncarcerationAndArrests = configuration.PersonSearchConfiguration.RealTimeIncarcerationAndArrests;
                WorldCheckRiskIntelligence = configuration.PersonSearchConfiguration.WorldCheckRiskIntelligence;
            }

        }
    }

    public partial class PersonSearchRequest
    {
        public PersonSearchRequest()
        {

        }
        public PersonSearchRequest(IClearConfiguration configuration, IPersonSearchRequest request)
        {
            if (configuration != null && request != null)
            {
                PermissiblePurpose = new PermissiblePurpose(configuration);
                Reference = request.Reference;
                Criteria = new Criteria(request);
                Datasources = new Datasources(configuration);
            }
        }
    }
}
