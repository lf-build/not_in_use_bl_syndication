﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Request.BusinessSearch
{

    [XmlRoot(ElementName = "PermissiblePurpose")]
    public partial class PermissiblePurpose
    {
        [XmlElement(ElementName = "GLB")]
        public string Glb { get; set; }
        [XmlElement(ElementName =  "DPPA")]
        public string Dppa { get; set; }
        [XmlElement(ElementName = "VOTER" )]
        public string Voter { get; set; }
    }

    [XmlRoot(ElementName = "CorporationInfo")]
    public partial class CorporationInfo
    {
        [XmlElement(ElementName = "CorporationId")]
        public string CorporationId { get; set; }
        [XmlElement(ElementName = "FilingNumber")]
        public string FilingNumber { get; set; }
        [XmlElement(ElementName = "FilingDate")]
        public string FilingDate { get; set; }
        [XmlElement(ElementName = "FEIN")]
        public string FEIN { get; set; }
        [XmlElement(ElementName = "DUNSNumber")]
        public string DUNSNumber { get; set; }
    }

    [XmlRoot(ElementName = "AdvancedNameSearch")]
    public partial class AdvancedNameSearch
    {
        [XmlElement(ElementName = "LastSecondaryNameSoundSimilarOption")]
        public string LastSecondaryNameSoundSimilarOption { get; set; }
        [XmlElement(ElementName = "SecondaryLastNameOption")]
        public string SecondaryLastNameOption { get; set; }
        [XmlElement(ElementName = "FirstNameSoundSimilarOption")]
        public string FirstNameSoundSimilarOption { get; set; }
        [XmlElement(ElementName = "FirstNameVariationsOption")]
        public string FirstNameVariationsOption { get; set; }
    }

    [XmlRoot(ElementName = "NameInfo")]
    public partial class NameInfo
    {
        [XmlElement(ElementName = "AdvancedNameSearch")]
        public AdvancedNameSearch AdvancedNameSearch { get; set; }
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }
        [XmlElement(ElementName = "MiddleInitial")]
        public string MiddleInitial { get; set; }
        [XmlElement(ElementName = "SecondaryLastName")]
        public string SecondaryLastName { get; set; }
    }

    [XmlRoot(ElementName = "AddressInfo")]
    public partial class AddressInfo
    {
        [XmlElement(ElementName = "StreetNamesSoundSimilarOption")]
        public string StreetNamesSoundSimilarOption { get; set; }
        [XmlElement(ElementName = "Street")]
        public string Street { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "State")]
        public string State { get; set; }
        [XmlElement(ElementName = "County")]
        public string County { get; set; }
        [XmlElement(ElementName = "ZipCode")]
        public string ZipCode { get; set; }
        [XmlElement(ElementName = "Province")]
        public string Province { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "BusinessCriteria", Namespace = "com/thomsonreuters/schemas/search")]
    public partial class BusinessCriteria
    {
        [XmlElement(ElementName = "BusinessName")]
        public string BusinessName { get; set; }
        [XmlElement(ElementName = "CorporationInfo")]
        public CorporationInfo CorporationInfo { get; set; }
        [XmlElement(ElementName = "NPINumber")]
        public string NPINumber { get; set; }
        [XmlElement(ElementName = "NameInfo")]
        public NameInfo NameInfo { get; set; }
        [XmlElement(ElementName = "AddressInfo")]
        public AddressInfo AddressInfo { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlAttribute(AttributeName = "b1", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string B1 { get; set; }
    }

    [XmlRoot(ElementName = "Criteria")]
    public partial class Criteria
    {
        [XmlElement(ElementName = "BusinessCriteria", Namespace = "com/thomsonreuters/schemas/search")]
        public BusinessCriteria BusinessCriteria { get; set; }
    }

    [XmlRoot(ElementName = "Datasources")]
    public partial class Datasources
    {
        [XmlElement(ElementName = "PublicRecordBusiness")]
        public bool PublicRecordBusiness { get; set; }
        [XmlElement(ElementName = "NPIRecord")]
        public bool NPIRecord { get; set; }
        [XmlElement(ElementName = "PublicRecordUCCFilings")]
        public bool PublicRecordUCCFilings { get; set; }
        [XmlElement(ElementName = "WorldCheckRiskIntelligence")]
        public bool WorldCheckRiskIntelligence { get; set; }
    }

    [XmlRoot(ElementName = "BusinessSearchRequest", Namespace = "http://clear.thomsonreuters.com/api/search/2.0")]
    public partial class BusinessSearchRequest
    {
        [XmlElement(ElementName = "PermissiblePurpose")]
        public PermissiblePurpose PermissiblePurpose { get; set; }
        [XmlElement(ElementName = "Reference")]
        public string Reference { get; set; }
        [XmlElement(ElementName = "Criteria")]
        public Criteria Criteria { get; set; }
        [XmlElement(ElementName = "Datasources")]
        public Datasources Datasources { get; set; }
        [XmlAttribute(AttributeName = "bs", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Bs { get; set; }
    }



}
