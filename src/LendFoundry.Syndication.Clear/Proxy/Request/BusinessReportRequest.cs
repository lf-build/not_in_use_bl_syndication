﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear.Proxy.Request.BusinessReport
{

    [XmlRoot(ElementName = "PermissiblePurpose")]
        public partial class PermissiblePurpose
        {
            [XmlElement(ElementName = "GLB")]
            public string Glb { get; set; }
            [XmlElement(ElementName =  "DPPA")]
            public string Dppa { get; set; }
            [XmlElement(ElementName = "VOTER" )]
            public string Voter { get; set; }
        }

        [XmlRoot(ElementName = "ReportSections")]
        public partial class ReportSections
        {
            [XmlElement(ElementName = "BusinessOverviewSection")]
            public bool BusinessOverviewSection { get; set; }
            [XmlElement(ElementName = "PhoneNumberSection")]
            public bool PhoneNumberSection { get; set; }
            [XmlElement(ElementName = "BusinessSameAddressSection")]
            public bool BusinessSameAddressSection { get; set; }
            [XmlElement(ElementName = "PeopleSameAddressSection")]
            public bool PeopleSameAddressSection { get; set; }
            [XmlElement(ElementName = "PeopleSamePhoneSection")]
            public bool PeopleSamePhoneSection { get; set; }
            [XmlElement(ElementName = "BusinessSamePhoneSection")]
            public bool BusinessSamePhoneSection { get; set; }
            [XmlElement(ElementName = "FEINSection")]
            public bool FEINSection { get; set; }
            [XmlElement(ElementName = "BusinessFinderSection")]
            public bool BusinessFinderSection { get; set; }
            [XmlElement(ElementName = "FictitiousBusinessNameSection")]
            public bool FictitiousBusinessNameSection { get; set; }
            [XmlElement(ElementName = "ExecutiveAffiliationSection")]
            public bool ExecutiveAffiliationSection { get; set; }
            [XmlElement(ElementName = "ExecutiveProfileSection")]
            public bool ExecutiveProfileSection { get; set; }
            [XmlElement(ElementName = "CompanyProfileSection")]
            public bool CompanyProfileSection { get; set; }
            [XmlElement(ElementName = "CurrentStockSection")]
            public bool CurrentStockSection { get; set; }
            [XmlElement(ElementName = "StockPerformanceSection")]
            public bool StockPerformanceSection { get; set; }
            [XmlElement(ElementName = "AnnualFinancialSection")]
            public bool AnnualFinancialSection { get; set; }
            [XmlElement(ElementName = "SupplementaryDataSection")]
            public bool SupplementaryDataSection { get; set; }
            [XmlElement(ElementName = "GrowthRateSection")]
            public bool GrowthRateSection { get; set; }
            [XmlElement(ElementName = "FundamentalRatioSection")]
            public bool FundamentalRatioSection { get; set; }
            [XmlElement(ElementName = "MoneyServiceBusinessSection")]
            public bool MoneyServiceBusinessSection { get; set; }
            [XmlElement(ElementName = "ForeignBusinessStatSection")]
            public bool ForeignBusinessStatSection { get; set; }
            [XmlElement(ElementName = "ExchangeRateSection")]
            public bool ExchangeRateSection { get; set; }
            [XmlElement(ElementName = "DunBradstreetSection")]
            public bool DunBradstreetSection { get; set; }
            [XmlElement(ElementName = "DunBradstreetPCISection")]
            public bool DunBradstreetPCISection { get; set; }
            [XmlElement(ElementName = "WorldbaseSection")]
            public bool WorldbaseSection { get; set; }
            [XmlElement(ElementName = "CorporateSection")]
            public bool CorporateSection { get; set; }
            [XmlElement(ElementName = "ExecutiveOfficerSection")]
            public bool ExecutiveOfficerSection { get; set; }
            [XmlElement(ElementName = "BusinessProfileSection")]
            public bool BusinessProfileSection { get; set; }
            [XmlElement(ElementName = "CurrentOfficerSection")]
            public bool CurrentOfficerSection { get; set; }
            [XmlElement(ElementName = "PreviousOfficerSection")]
            public bool PreviousOfficerSection { get; set; }
            [XmlElement(ElementName = "GlobalSanctionSection")]
            public bool GlobalSanctionSection { get; set; }
            [XmlElement(ElementName = "ArrestSection")]
            public bool ArrestSection { get; set; }
            [XmlElement(ElementName = "CriminalSection")]
            public bool CriminalSection { get; set; }
            [XmlElement(ElementName = "ProfessionalLicenseSection")]
            public bool ProfessionalLicenseSection { get; set; }
            [XmlElement(ElementName = "WorldCheckSection")]
            public bool WorldCheckSection { get; set; }
            [XmlElement(ElementName = "InfractionSection")]
            public bool InfractionSection { get; set; }
            [XmlElement(ElementName = "LawsuitSection")]
            public bool LawsuitSection { get; set; }
            [XmlElement(ElementName = "LienJudgmentSection")]
            public bool LienJudgmentSection { get; set; }
            [XmlElement(ElementName = "DocketSection")]
            public bool DocketSection { get; set; }
            [XmlElement(ElementName = "FederalCaseLawSection")]
            public bool FederalCaseLawSection { get; set; }
            [XmlElement(ElementName = "StateCaseLawSection")]
            public bool StateCaseLawSection { get; set; }
            [XmlElement(ElementName = "BankruptcySection")]
            public bool BankruptcySection { get; set; }
            [XmlElement(ElementName = "RealPropertySection")]
            public bool RealPropertySection { get; set; }
            [XmlElement(ElementName = "PreForeclosureSection")]
            public bool PreForeclosureSection { get; set; }
            [XmlElement(ElementName = "BusinessContactSection")]
            public bool BusinessContactSection { get; set; }
            [XmlElement(ElementName = "UCCSection")]
            public bool UCCSection { get; set; }
            [XmlElement(ElementName = "SECFilingSection")]
            public bool SECFilingSection { get; set; }
            [XmlElement(ElementName = "RelatedSECFilingRecordSection")]
            public bool RelatedSECFilingRecordSection { get; set; }
            [XmlElement(ElementName = "OtherSecurityFilingRecordSection")]
            public bool OtherSecurityFilingRecordSection { get; set; }
            [XmlElement(ElementName = "AircraftSection")]
            public bool AircraftSection { get; set; }
            [XmlElement(ElementName = "WatercraftSection")]
            public bool WatercraftSection { get; set; }
            [XmlElement(ElementName = "NPISection")]
            public bool NPISection { get; set; }
            [XmlElement(ElementName = "HealthcareSanctionSection")]
            public bool HealthcareSanctionSection { get; set; }
            [XmlElement(ElementName = "ExcludedPartySection")]
            public bool ExcludedPartySection { get; set; }
            [XmlElement(ElementName = "AssociateAnalyticsChartSection")]
            public bool AssociateAnalyticsChartSection { get; set; }
            [XmlElement(ElementName = "QuickAnalysisFlagSection")]
            public bool QuickAnalysisFlagSection { get; set; }
        }

        [XmlRoot(ElementName = "ReportCriteria", Namespace = "com/thomsonreuters/schemas/company-report")]
        public partial class ReportCriteria
        {
            [XmlElement(ElementName = "GroupID")]
            public string GroupID { get; set; }
            [XmlElement(ElementName = "ReportChoice")]
            public string ReportChoice { get; set; }
            [XmlElement(ElementName = "ReportSections")]
            public ReportSections ReportSections { get; set; }
            [XmlAttribute(AttributeName = "rc", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Rc { get; set; }
        }

        [XmlRoot(ElementName = "Criteria")]
        public partial class Criteria
        {
            [XmlElement(ElementName = "ReportCriteria", Namespace = "com/thomsonreuters/schemas/company-report")]
            public ReportCriteria ReportCriteria { get; set; }
        }

        [XmlRoot(ElementName = "BusinessReportRequest", Namespace = "http://clear.thomsonreuters.com/api/report/2.0")]
        public partial class BusinessReportRequest
        {
            [XmlElement(ElementName = "PermissiblePurpose")]
            public PermissiblePurpose PermissiblePurpose { get; set; }
            [XmlElement(ElementName = "Reference")]
            public string Reference { get; set; }
            [XmlElement(ElementName = "Criteria")]
            public Criteria Criteria { get; set; }
            [XmlAttribute(AttributeName = "br", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Br { get; set; }
        }

    }


