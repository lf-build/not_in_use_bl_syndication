﻿using System;

namespace LendFoundry.Syndication.Clear.Proxy.Request.BusinessReport
{
    public partial class BusinessReportRequest
    {
        public BusinessReportRequest()
        {

        }
        public BusinessReportRequest(IClearConfiguration configuration, string groupId)
        {
            
            if (configuration!=null)
            {
                PermissiblePurpose = new PermissiblePurpose();
                Reference = configuration.BusinessReportConfiguration.Reference;
                Criteria = new Criteria(configuration,groupId);

            }

        }
    }

        public partial class PermissiblePurpose
        {
            public PermissiblePurpose()
            {

            }
            public PermissiblePurpose(IClearConfiguration configuration)
            {

                Glb = configuration.Glb;
                Dppa = configuration.Dppa;
                Voter = configuration.Voter;
            }
        }

        public partial class ReportSections
        {
        public ReportSections()
        {

        }
        public ReportSections(IClearConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            if (configuration.BusinessReportConfiguration!=null)
            {
                BusinessOverviewSection = configuration.BusinessReportConfiguration.BusinessOverviewSection;
                PhoneNumberSection = configuration.BusinessReportConfiguration.PhoneNumberSection;
                BusinessSameAddressSection = configuration.BusinessReportConfiguration.BusinessSameAddressSection;
                PeopleSameAddressSection = configuration.BusinessReportConfiguration.PeopleSameAddressSection;
                PeopleSamePhoneSection = configuration.BusinessReportConfiguration.PeopleSamePhoneSection;
                BusinessSamePhoneSection = configuration.BusinessReportConfiguration.BusinessSamePhoneSection;
                FEINSection = configuration.BusinessReportConfiguration.FEINSection;
                BusinessFinderSection = configuration.BusinessReportConfiguration.BusinessFinderSection;
                FictitiousBusinessNameSection = configuration.BusinessReportConfiguration.FictitiousBusinessNameSection;
                ExecutiveAffiliationSection = configuration.BusinessReportConfiguration.ExecutiveAffiliationSection;
                ExecutiveProfileSection = configuration.BusinessReportConfiguration.ExecutiveProfileSection;
                CompanyProfileSection = configuration.BusinessReportConfiguration.CompanyProfileSection;
                CurrentStockSection = configuration.BusinessReportConfiguration.CurrentStockSection;
                StockPerformanceSection = configuration.BusinessReportConfiguration.StockPerformanceSection;
                AnnualFinancialSection = configuration.BusinessReportConfiguration.AnnualFinancialSection;
                SupplementaryDataSection = configuration.BusinessReportConfiguration.SupplementaryDataSection;
                GrowthRateSection = configuration.BusinessReportConfiguration.GrowthRateSection;
                FundamentalRatioSection = configuration.BusinessReportConfiguration.FundamentalRatioSection;
                MoneyServiceBusinessSection = configuration.BusinessReportConfiguration.MoneyServiceBusinessSection;
                ForeignBusinessStatSection = configuration.BusinessReportConfiguration.ForeignBusinessStatSection;
                ExchangeRateSection = configuration.BusinessReportConfiguration.ExchangeRateSection;
                DunBradstreetSection = configuration.BusinessReportConfiguration.DunBradstreetSection;
                DunBradstreetPCISection = configuration.BusinessReportConfiguration.DunBradstreetPCISection;
                WorldbaseSection = configuration.BusinessReportConfiguration.WorldbaseSection;
                CorporateSection = configuration.BusinessReportConfiguration.CorporateSection;
                ExecutiveOfficerSection = configuration.BusinessReportConfiguration.ExecutiveOfficerSection;
                BusinessProfileSection = configuration.BusinessReportConfiguration.BusinessProfileSection;
                CurrentOfficerSection = configuration.BusinessReportConfiguration.CurrentOfficerSection;
                PreviousOfficerSection = configuration.BusinessReportConfiguration.PreviousOfficerSection;
                GlobalSanctionSection = configuration.BusinessReportConfiguration.GlobalSanctionSection;
                ArrestSection = configuration.BusinessReportConfiguration.ArrestSection;
                CriminalSection = configuration.BusinessReportConfiguration.CriminalSection;
                ProfessionalLicenseSection = configuration.BusinessReportConfiguration.ProfessionalLicenseSection;
                WorldCheckSection = configuration.BusinessReportConfiguration.WorldCheckSection;
                InfractionSection = configuration.BusinessReportConfiguration.InfractionSection;
                LawsuitSection = configuration.BusinessReportConfiguration.LawsuitSection;
                LienJudgmentSection = configuration.BusinessReportConfiguration.LienJudgmentSection;
                DocketSection = configuration.BusinessReportConfiguration.DocketSection;
                FederalCaseLawSection = configuration.BusinessReportConfiguration.FederalCaseLawSection;
                StateCaseLawSection = configuration.BusinessReportConfiguration.StateCaseLawSection;
                BankruptcySection = configuration.BusinessReportConfiguration.BankruptcySection;
                RealPropertySection = configuration.BusinessReportConfiguration.RealPropertySection;
                PreForeclosureSection = configuration.BusinessReportConfiguration.PreForeclosureSection;
                BusinessContactSection = configuration.BusinessReportConfiguration.BusinessContactSection;
                UCCSection = configuration.BusinessReportConfiguration.UCCSection;
                SECFilingSection = configuration.BusinessReportConfiguration.SECFilingSection;
                RelatedSECFilingRecordSection = configuration.BusinessReportConfiguration.RelatedSECFilingRecordSection;
                OtherSecurityFilingRecordSection = configuration.BusinessReportConfiguration.OtherSecurityFilingRecordSection;
                AircraftSection = configuration.BusinessReportConfiguration.AircraftSection;
                WatercraftSection = configuration.BusinessReportConfiguration.WatercraftSection;
                NPISection = configuration.BusinessReportConfiguration.NPISection;
                HealthcareSanctionSection = configuration.BusinessReportConfiguration.HealthcareSanctionSection;
                ExcludedPartySection = configuration.BusinessReportConfiguration.ExcludedPartySection;
                AssociateAnalyticsChartSection = configuration.BusinessReportConfiguration.AssociateAnalyticsChartSection;
                QuickAnalysisFlagSection = configuration.BusinessReportConfiguration.QuickAnalysisFlagSection;
            }
        }
        }


        public partial class ReportCriteria
        {
        public ReportCriteria()
        {

        }
        public ReportCriteria(IClearConfiguration configuration, string groupId)
        {
            if (string.IsNullOrWhiteSpace(groupId))
                throw new ArgumentNullException(nameof(groupId));
            if (configuration != null)
            {
                GroupID = groupId;
                ReportChoice = configuration.BusinessReportConfiguration.ReportChoice;
                ReportSections = new ReportSections(configuration);
            }
        }
            


        }


        public partial class Criteria
        {
        public Criteria()
        {

        }
            public Criteria(IClearConfiguration configuration,string groupId)
            {
             
               ReportCriteria =new ReportCriteria(configuration,groupId);

            }

        }

      

}
