﻿namespace LendFoundry.Syndication.Clear
{
    public interface IClearConfiguration
    {
        string Url { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string ProxyUrl { get; set; }
        bool UseProxy { get; set; }
        string Glb { get; set; }
        string Dppa { get; set; }
        string Voter { get; set; }
        string PersonReportType { get; set; }
         string PersonSearchUrl { get; set; }
         string PersonReportUrl { get; set; }
         string BusinessSearchUrl { get; set; }
         string BusinessReportUrl { get; set; }
        string CertificatePassword { get; set; }
         string Certificate { get; set; }
        IPersonReportConfiguration PersonReportConfiguration { get; set; }
        IBusinessReportConfiguration BusinessReportConfiguration { get; set; }
         IPersonSearchConfiguration PersonSearchConfiguration { get; set; }
         IBusinessSearchConfiguration BusinessSearchConfiguration { get; set; }
    }
}
