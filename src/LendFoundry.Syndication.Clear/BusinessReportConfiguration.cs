﻿

namespace LendFoundry.Syndication.Clear.Request
{
    public class BusinessReportConfiguration: IBusinessReportConfiguration
    {
        public bool BusinessOverviewSection { get; set; } = true;
        public bool PhoneNumberSection { get; set; } = true;
        public bool BusinessSameAddressSection { get; set; } = true;
        public bool PeopleSameAddressSection { get; set; } = true;
        public bool PeopleSamePhoneSection { get; set; } = true;
        public bool BusinessSamePhoneSection { get; set; } = true;
        public bool FEINSection { get; set; } = true;
        public bool BusinessFinderSection { get; set; } = true;
        public bool FictitiousBusinessNameSection { get; set; } = true;
        public bool ExecutiveAffiliationSection { get; set; } = true;
        public bool ExecutiveProfileSection { get; set; } = true;
        public bool CompanyProfileSection { get; set; } = true;
        public bool CurrentStockSection { get; set; } = true;
        public bool StockPerformanceSection { get; set; } = true;
        public bool AnnualFinancialSection { get; set; } = true;
        public bool SupplementaryDataSection { get; set; } = true;
        public bool GrowthRateSection { get; set; } = true;
        public bool FundamentalRatioSection { get; set; } = true;
        public bool MoneyServiceBusinessSection { get; set; } = true;
        public bool ForeignBusinessStatSection { get; set; } = true;
        public bool ExchangeRateSection { get; set; } = true;
        public bool DunBradstreetSection { get; set; } = true;
        public bool DunBradstreetPCISection { get; set; } = true;
        public bool WorldbaseSection { get; set; } = true;
        public bool CorporateSection { get; set; } = true;
        public bool ExecutiveOfficerSection { get; set; } = true;
        public bool BusinessProfileSection { get; set; } = true;
        public bool CurrentOfficerSection { get; set; } = true;
        public bool PreviousOfficerSection { get; set; } = true;
        public bool GlobalSanctionSection { get; set; } = true;
        public bool ArrestSection { get; set; } = true;
        public bool CriminalSection { get; set; } = true;
        public bool ProfessionalLicenseSection { get; set; } = true;
        public bool WorldCheckSection { get; set; } = true;
        public bool InfractionSection { get; set; } = true;
        public bool LawsuitSection { get; set; } = true;
        public bool LienJudgmentSection { get; set; } = true;
        public bool DocketSection { get; set; } = true;
        public bool FederalCaseLawSection { get; set; } = true;
        public bool StateCaseLawSection { get; set; } = true;
        public bool BankruptcySection { get; set; } = true;
        public bool RealPropertySection { get; set; } = true;
        public bool PreForeclosureSection { get; set; } = true;
        public bool BusinessContactSection { get; set; } = true;
        public bool UCCSection { get; set; } = true;
        public bool SECFilingSection { get; set; } = true;
        public bool RelatedSECFilingRecordSection { get; set; } = true;
        public bool OtherSecurityFilingRecordSection { get; set; } = true;
        public bool AircraftSection { get; set; } = true;
        public bool WatercraftSection { get; set; } = true;
        public bool NPISection { get; set; } = true;
        public bool HealthcareSanctionSection { get; set; } = true;
        public bool ExcludedPartySection { get; set; } = true;
        public bool AssociateAnalyticsChartSection { get; set; } = true;

        public bool QuickAnalysisFlagSection { get; set; } = true;
        public string ReportChoice { get; set; } = "Business";
        public string Reference { get; set; }
    }
}
