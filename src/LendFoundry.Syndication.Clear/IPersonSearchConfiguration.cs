﻿namespace LendFoundry.Syndication.Clear
{
    public interface IPersonSearchConfiguration
    {
        bool NPIRecord { get; set; }
        bool PublicRecordPeople { get; set; }
        bool RealTimeIncarcerationAndArrests { get; set; }
        bool WorldCheckRiskIntelligence { get; set; }
    }
}