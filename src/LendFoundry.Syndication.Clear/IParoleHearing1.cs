﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IParoleHearing1
    {
        string hearingDate { get; set; }
        string hearingLocation { get; set; }
        string numMonthsDeferred { get; set; }
        string paroleBoardAction { get; set; }
        string typeOfHearing { get; set; }
    }
}