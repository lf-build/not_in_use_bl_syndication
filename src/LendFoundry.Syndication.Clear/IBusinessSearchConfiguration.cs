﻿namespace LendFoundry.Syndication.Clear
{
    public interface IBusinessSearchConfiguration
    {
        bool NPIRecord { get; set; }
        bool PublicRecordBusiness { get; set; }
        bool PublicRecordUCCFilings { get; set; }
        bool WorldCheckRiskIntelligence { get; set; }
    }
}