﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Clear
{
    [Serializable]
    public class ClearException : Exception
    {
        public ClearException()
        {
        }

        public ClearException(string message) : base(message)
        {
        }

        public ClearException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ClearException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
