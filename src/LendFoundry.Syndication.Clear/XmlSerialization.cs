﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Clear
{
    internal sealed class ExtentedStringWriter : StringWriter
    {
        public ExtentedStringWriter(Encoding desiredEncoding)
        {
            Encoding = desiredEncoding;
        }

        public override Encoding Encoding { get; }
    }

    public static class XmlSerialization
    {
        public static string Serialize<T>(T @object, Dictionary<string, string> namespaceList)
        {
            string xmlContent;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var sww = new ExtentedStringWriter(Encoding.UTF8))
            using (var writer = XmlWriter.Create(sww, new XmlWriterSettings() { NamespaceHandling = NamespaceHandling.OmitDuplicates, OmitXmlDeclaration = true }))
            {
                if (namespaceList!=null)
                {
                    var namespaces = new XmlSerializerNamespaces();
                    foreach (var item in namespaceList)
                    {
                        namespaces.Add(item.Key, item.Value);
                    }
                    xmlSerializer.Serialize(writer, @object, namespaces);
                    xmlContent = sww.ToString();
                }
                else
                {
                    xmlSerializer.Serialize(writer, @object);
                    xmlContent = sww.ToString();
                }
            }
           
            return xmlContent;
        }

       public static T Deserialize<T>(string xmlString)
        {
            try
            {
                Type objType = typeof(T);
                XmlSerializer serializer = new XmlSerializer(objType);
                var objectResult = (T)Activator.CreateInstance(objType);
                XElement xmlDocumentWithNs = XElement.Parse(xmlString);
                XElement xmlDocumentWithoutNs=  stripNS(xmlDocumentWithNs);
                var result = string.Concat(xmlDocumentWithoutNs);
                using (var stringReader = new StringReader(result))
                {
                    using (XmlReader reader = XmlReader.Create(stringReader))
                    {
                        objectResult = (T)serializer.Deserialize(reader);
                    }
                }
                return objectResult;
            }catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        static XElement stripNS(XElement root)
        {
            XElement result = new XElement(
                root.Name.LocalName,
                root.HasElements ?
                    root.Elements().Select(el => stripNS(el)) :
                    (object)root.Value
            );

            result.ReplaceAttributes(
                root.Attributes().Where(attr => (!attr.IsNamespaceDeclaration)));

            return result;
        }

       
    }
}