﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using LendFoundry.Syndication.Clear.Request;

namespace LendFoundry.Syndication.Clear
{
    public class ClearConfiguration : IClearConfiguration
    {
         public string Url { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ProxyUrl { get; set; }
        public bool UseProxy { get; set; } = true;
        public string Glb { get; set; }
        public string Dppa { get; set; }
        public string Voter { get; set; }
        public string PersonReportType { get; set; }
        public string PersonSearchUrl { get; set; }
        public string PersonReportUrl { get; set; }
        public string BusinessSearchUrl { get; set; }
        public string BusinessReportUrl { get; set; }
        public string CertificatePassword { get; set; }
        public string Certificate { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPersonReportConfiguration, PersonReportConfiguration>))]
        public IPersonReportConfiguration PersonReportConfiguration { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBusinessReportConfiguration, BusinessReportConfiguration>))]
        public IBusinessReportConfiguration BusinessReportConfiguration { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPersonSearchConfiguration, PersonSearchConfiguration>))]
        public IPersonSearchConfiguration PersonSearchConfiguration { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBusinessSearchConfiguration, BusinessSearchConfiguration>))]
        public IBusinessSearchConfiguration BusinessSearchConfiguration { get; set; }

    }
}
