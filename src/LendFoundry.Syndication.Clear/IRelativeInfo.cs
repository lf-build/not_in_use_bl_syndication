﻿namespace LendFoundry.Syndication.Clear.Response.Models.PersonReportResponse
{
    public interface IRelativeInfo
    {
        IAddress address { get; set; }
        IPersonName personName { get; set; }
        IPhoneInfo phone { get; set; }
        string typeOfRelative { get; set; }
    }
}