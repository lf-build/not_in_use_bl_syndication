﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;

namespace CreditExchange.YodleeFastLink.Client
{
    public class YodleeFastLinkServiceClientFactory : IYodleeFastLinkServiceClientFactory
    {
        public YodleeFastLinkServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }
        public Syndication.YodleeFastLink.IYodleeFastLinkService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new YodleeFastLinkService(client);
        }
    }
}
