﻿using CreditExchange.Syndication.YodleeFastLink;
using LendFoundry.Security.Tokens;

namespace CreditExchange.YodleeFastLink.Client
{
    public interface IYodleeFastLinkServiceClientFactory
    {
        IYodleeFastLinkService Create(ITokenReader reader);
    }
}
