﻿using System;
using System.Threading.Tasks;
using CreditExchange.Syndication.YodleeFastLink;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.YodleeFastLink.Request;
using RestSharp;
using CreditExchange.Syndication.YodleeFastLink.Response;

namespace CreditExchange.YodleeFastLink.Client
{
    public class YodleeFastLinkService : IYodleeFastLinkService
    {
        public YodleeFastLinkService(IServiceClient client)
        {
            Client = client;
        }
        IServiceClient Client { get; }
        public async Task<ICobrandResponse> GetCobrandToken(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/get_cobrand_token", Method.GET);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);

            return await Client.ExecuteAsync<CobrandResponse>(request);
        }
        public async Task<IUserAuthenticateResponse> RegisterUser(string entityType, string entityId, UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/register_user", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(userAuthenticateRequest);
            return await Client.ExecuteAsync<UserAuthenticateResponse>(request);
        }
        public async Task<IUserAuthenticateResponse> AuthenticateUser(string entityType, string entityId,UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/authenticate_user", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(userAuthenticateRequest);
            return await Client.ExecuteAsync<UserAuthenticateResponse>(request);
        }
        public async Task<IFastlinkAccessTokenResponse> GetFastlinkAccessToken(string entityType, string entityId,FastlinkAccessTokenRequest fastlinkAccessTokenRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/get_fastlink_accesstoken", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(fastlinkAccessTokenRequest);
            return await Client.ExecuteAsync<FastlinkAccessTokenResponse>(request);
        }
        public async Task<IRestResponse> GetFalstLinkUrl(string entityType, string entityId,FastLinkUrlRequest fastlinkReuest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/get_Fastlink_Url", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(fastlinkReuest);
            return await Client.ExecuteRequestAsync(request);
        }
        public async Task<IGetBankAccountResponse> GetUserBankAccounts(string entityType, string entityId,GetBankAccountsRequest getBankAccountsRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/get_user_bank_accounts", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(getBankAccountsRequest);
            return await Client.ExecuteAsync<GetBankAccountResponse>(request);
        }
        public async Task<ITransactionResponse> GetAccountTransactions(string entityType, string entityId,GetTransactionRequest getTransactionRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/yodleefastlink/get_account_transactions", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(getTransactionRequest);
            return await Client.ExecuteAsync<TransactionResponse>(request);
        }
    }
}
