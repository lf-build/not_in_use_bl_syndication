﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace CreditExchange.YodleeFastLink.Client
{
    public static class YodleeFastLinkClientExtensions
    {
        public static IServiceCollection AddYodleeFastLinkService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IYodleeFastLinkServiceClientFactory>(p => new YodleeFastLinkServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IYodleeFastLinkServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
