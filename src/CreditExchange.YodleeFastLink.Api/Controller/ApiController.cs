﻿using CreditExchange.Syndication.YodleeFastLink;
using CreditExchange.Syndication.YodleeFastLink.Request;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.YodleeFastLink;
using Microsoft.AspNet.Mvc;
using System.Threading.Tasks;

namespace CreditExchange.YodleeFastLink.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IYodleeFastLinkService service)
        {
            Service = service;
        }
        private IYodleeFastLinkService Service { get; }
        [HttpGet("{entitytype}/{entityid}/yodleefastlink/get_cobrand_token")]
        public async Task<IActionResult> GetCobrandToken(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.GetCobrandToken(entityType, entityId)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/yodleefastlink/register_user")]
        public async Task<IActionResult> RegisterUser(string entityType, string entityId, [FromBody]UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.RegisterUser(entityType, entityId, userAuthenticateRequest)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/yodleefastlink/authenticate_user")]
        public async Task<IActionResult> AuthenticateUser(string entityType, string entityId, [FromBody]UserRegisterNAuthenticateRequest userAuthenticateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.AuthenticateUser(entityType, entityId, userAuthenticateRequest)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/yodleefastlink/get_fastlink_accesstoken")]
        public async Task<IActionResult> GetFastlinkAccessToken(string entityType, string entityId, [FromBody]FastlinkAccessTokenRequest fastlinkAccessTokenRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.GetFastlinkAccessToken(entityType, entityId, fastlinkAccessTokenRequest)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }


        [HttpPost("{entitytype}/{entityid}/yodleefastlink/get_Fastlink_Url")]
        public async Task<IActionResult> GetFalstLinkUrl(string entityType, string entityId, [FromBody]FastLinkUrlRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.GetFalstLinkUrl(entityType, entityId, request)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/yodleefastlink/get_user_bank_accounts")]
        public async Task<IActionResult> GetUserBankAccounts(string entityType, string entityId, [FromBody]GetBankAccountsRequest getBankAccountsRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.GetUserBankAccounts(entityType, entityId, getBankAccountsRequest)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/yodleefastlink/get_account_transactions")]
        public async Task<IActionResult> GetAccountTransactions(string entityType, string entityId, [FromBody]GetTransactionRequest getTransactionRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.GetAccountTransactions(entityType, entityId, getTransactionRequest)));

                }
                catch (YodleeFastLinkException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
    }
}
