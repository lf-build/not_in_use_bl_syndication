﻿namespace CreditExchange.Syndication.ThirdVista
{
    public interface IThirdVistaConfiguration
    {
        string BaseUrl { get; set; }
        string AuthenticationApi { get; set; }
        string GetTransactionsApi { get; set; }
        string GetTransactionImageApi { get; set; }
        string DownLoadTransactionImageApi { get; set; }
        string GetStatementFilesApi { get; set; }
        string DownLoadStatementFileApi { get; set; }
        string RegisterUserApi { get; set; }
        string GetAccountsApi { get; set; }
    }
}
