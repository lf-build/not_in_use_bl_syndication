﻿namespace CreditExchange.Syndication.ThirdVista.Proxy.Request
{
    public interface IBaseRequest
    {
        string SessionId { get; set; }
    }
}
