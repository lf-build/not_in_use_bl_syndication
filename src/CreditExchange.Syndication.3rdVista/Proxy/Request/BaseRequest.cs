﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Request
{
    public class BaseRequest: IBaseRequest
    {
        [JsonProperty(PropertyName= "p_session_id")]
        public string SessionId { get; set; }
    }
}
