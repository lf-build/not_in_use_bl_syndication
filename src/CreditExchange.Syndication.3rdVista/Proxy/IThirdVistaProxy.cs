﻿using CreditExchange.Syndication.ThirdVista.Proxy.Response;

namespace CreditExchange.Syndication.ThirdVista.Proxy
{
    public interface IThirdVistaProxy
    {
        IRegisterUserResponse RegisterUser(string customerSessionId, string email, string password);
        IAuthenticationResponse AuthenticateUser(string email, string password);
        IListAccountsResponse GetAccounts(string sessionId);
        IGetTransactionsResponse GetTransactions(string sessionId);
        IGetImageTransactionsResponse GetImageTransactions(string sessionId);
        IListStatementFilesResponse GetStatementFiles(string sessionId);

    }
}
