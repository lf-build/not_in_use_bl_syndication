﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class Role: IRole
    {
        [JsonProperty("role_code")]
        public string RoleCode { get; set; }

        [JsonProperty("role_description")]
        public string RoleDescription { get; set; }
    }
}
