﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class AccountTransactions : IAccountTransactions
    {
        [JsonProperty("row_id")]
        public string RowId { get; set; }

        [JsonProperty("row_datetime")]
        public string RowDatetime { get; set; }

        [JsonProperty("row_epochtime")]
        public int RowEpochtime { get; set; }

        [JsonProperty("row_key")]
        public string RowKey { get; set; }

        [JsonProperty("container_type")]
        public string ContainerType { get; set; }

        [JsonProperty("container_desc")]
        public string ContainerDesc { get; set; }

        [JsonProperty("extractor_code")]
        public string ExtractorCode { get; set; }

        [JsonProperty("extractor_name")]
        public string ExtractorName { get; set; }

        [JsonProperty("extractor_url")]
        public string ExtractorUrl { get; set; }

        [JsonProperty("intuit_id")]
        public string IntuitId { get; set; }

        [JsonProperty("provider_code")]
        public string ProviderCode { get; set; }

        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("provider_alias")]
        public string ProviderAlias { get; set; }

        [JsonProperty("provider_url")]
        public string ProviderUrl { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("country_name")]
        public string CountryName { get; set; }

        [JsonProperty("account_code")]
        public string AccountCode { get; set; }

        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [JsonProperty("sub_acct_code")]
        public string SubAcctCode { get; set; }

        [JsonProperty("sub_acct_name")]
        public string SubAcctName { get; set; }

        [JsonProperty("user_email")]
        public string UserEmail { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("parent_customer_code")]
        public object ParentCustomerCode { get; set; }

        [JsonProperty("parent_customer_name")]
        public object ParentCustomerName { get; set; }

        [JsonProperty("trans_datetime")]
        public string TransDatetime { get; set; }

        [JsonProperty("trans_epochtime")]
        public int TransEpochtime { get; set; }

        [JsonProperty("trans_date")]
        public string TransDate { get; set; }

        [JsonProperty("trans_epochdate")]
        public int TransEpochdate { get; set; }

        [JsonProperty("trans_time")]
        public string TransTime { get; set; }

        [JsonProperty("trans_month_num")]
        public string TransMonthNum { get; set; }

        [JsonProperty("trans_month_name")]
        public string TransMonthName { get; set; }

        [JsonProperty("trans_month_year")]
        public string TransMonthYear { get; set; }

        [JsonProperty("trans_month_day")]
        public string TransMonthDay { get; set; }

        [JsonProperty("trans_week_day")]
        public string TransWeekDay { get; set; }

        [JsonProperty("trans_week_quarter")]
        public string TransWeekQuarter { get; set; }

        [JsonProperty("trans_week_iso_year")]
        public string TransWeekIsoYear { get; set; }

        [JsonProperty("trans_week_ansi_year")]
        public string TransWeekAnsiYear { get; set; }

        [JsonProperty("trans_seq")]
        public int TransSeq { get; set; }

        [JsonProperty("trans_code")]
        public string TransCode { get; set; }

        [JsonProperty("trans_desc")]
        public string TransDesc { get; set; }

        [JsonProperty("credit_value")]
        public double CreditValue { get; set; }

        [JsonProperty("debit_value")]
        public double DebitValue { get; set; }

        [JsonProperty("net_value")]
        public double NetValue { get; set; }

        [JsonProperty("abs_value")]
        public double AbsValue { get; set; }

        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty("currency_name")]
        public string CurrencyName { get; set; }

        [JsonProperty("currency_sign")]
        public string CurrencySign { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("category_group")]
        public string CategoryGroup { get; set; }

        [JsonProperty("sub_category_name")]
        public string SubCategoryName { get; set; }

        [JsonProperty("cust_category_name")]
        public string CustCategoryName { get; set; }

        [JsonProperty("cust_category_group")]
        public string CustCategoryGroup { get; set; }

        [JsonProperty("cust_sub_category_name")]
        public string CustSubCategoryName { get; set; }

        [JsonProperty("cust_class_type")]
        public string CustClassType { get; set; }

        [JsonProperty("cust_class_name")]
        public string CustClassName { get; set; }

        [JsonProperty("trans_category_name")]
        public string TransCategoryName { get; set; }

        [JsonProperty("trans_category_group")]
        public string TransCategoryGroup { get; set; }

        [JsonProperty("trans_sub_category_name")]
        public string TransSubCategoryName { get; set; }

        [JsonProperty("trans_class_type")]
        public string TransClassType { get; set; }

        [JsonProperty("trans_class_name")]
        public string TransClassName { get; set; }

        [JsonProperty("orig_trans_desc")]
        public string OrigTransDesc { get; set; }

        [JsonProperty("class_type")]
        public string ClassType { get; set; }

        [JsonProperty("class_name")]
        public string ClassName { get; set; }

        [JsonProperty("saved_timestamp")]
        public string SavedTimestamp { get; set; }

        [JsonProperty("saved_epochtime")]
        public object SavedEpochtime { get; set; }

        [JsonProperty("custom_flag")]
        public bool CustomFlag { get; set; }

        [JsonProperty("valid_flag")]
        public bool ValidFlag { get; set; }

        [JsonProperty("mime_type")]
        public object MimeType { get; set; }

        [JsonProperty("image_flag")]
        public bool ImageFlag { get; set; }

        [JsonProperty("fund_flag")]
        public bool FundFlag { get; set; }

        [JsonProperty("image_url")]
        public object ImageUrl { get; set; }

        [JsonProperty("check_number")]
        public object CheckNumber { get; set; }

        [JsonProperty("fund_code")]
        public object FundCode { get; set; }

        [JsonProperty("fund_name")]
        public object FundName { get; set; }

        [JsonProperty("fund_quant")]
        public object FundQuant { get; set; }

        [JsonProperty("fund_price")]
        public object FundPrice { get; set; }

        [JsonProperty("image_count")]
        public int ImageCount { get; set; }

        [JsonProperty("fund_count")]
        public int FundCount { get; set; }
    }
}
