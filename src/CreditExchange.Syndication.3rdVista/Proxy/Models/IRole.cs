﻿namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IRole
    {
       string RoleCode { get; set; }
       string RoleDescription { get; set; }
    }
}
