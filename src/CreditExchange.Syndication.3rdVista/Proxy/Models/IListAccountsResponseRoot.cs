﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IListAccountsResponseRoot: IRootBase
    {
         IList<IAccount> Accounts { get; set; }
         string StatusText { get; set; }
    }
}
