﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class ListAccountsResponseRoot:RootBase,IListAccountsResponseRoot
    {
        [JsonProperty("account")]
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Account>, IAccount>))]
        public IList<IAccount> Accounts { get; set; }

        [JsonProperty("status_text")]
        public string StatusText { get; set; }
    }
}
