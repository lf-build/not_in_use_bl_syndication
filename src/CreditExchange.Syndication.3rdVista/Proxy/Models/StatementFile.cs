﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class StatementFile: IStatementFile
    {
        [JsonProperty("row_id")]
        public string RowId { get; set; }

        [JsonProperty("row_datetime")]
        public string RowDatetime { get; set; }

        [JsonProperty("row_epochtime")]
        public int RowEpochtime { get; set; }

        [JsonProperty("row_key")]
        public string RowKey { get; set; }

        [JsonProperty("stmt_file_label")]
        public string StmtFileLabel { get; set; }

        [JsonProperty("container_type")]
        public string ContainerType { get; set; }

        [JsonProperty("container_desc")]
        public string ContainerDesc { get; set; }

        [JsonProperty("sub_acct_code")]
        public string SubAcctCode { get; set; }

        [JsonProperty("sub_acct_name")]
        public string SubAcctName { get; set; }

        [JsonProperty("user_email")]
        public string UserEmail { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("parent_customer_code")]
        public object ParentCustomerCode { get; set; }

        [JsonProperty("parent_customer_name")]
        public object ParentCustomerName { get; set; }

        [JsonProperty("file_date")]
        public string FileDate { get; set; }

        [JsonProperty("file_epochdate")]
        public int FileEpochdate { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }

        [JsonProperty("file_size")]
        public int FileSize { get; set; }

        [JsonProperty("file_url")]
        public string FileUrl { get; set; }

        [JsonProperty("mime_type")]
        public string MimeType { get; set; }

        [JsonProperty("account_code")]
        public string AccountCode { get; set; }

        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [JsonProperty("extractor_code")]
        public string ExtractorCode { get; set; }

        [JsonProperty("extractor_name")]
        public string ExtractorName { get; set; }

        [JsonProperty("extractor_url")]
        public string ExtractorUrl { get; set; }

        [JsonProperty("intuit_id")]
        public string IntuitId { get; set; }

        [JsonProperty("provider_code")]
        public string ProviderCode { get; set; }

        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("provider_alias")]
        public string ProviderAlias { get; set; }

        [JsonProperty("provider_url")]
        public string ProviderUrl { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("country_name")]
        public string CountryName { get; set; }

        [JsonProperty("file_timestamp")]
        public string FileTimestamp { get; set; }

        [JsonProperty("file_epochtime")]
        public object FileEpochtime { get; set; }

        [JsonProperty("icon_url")]
        public string IconUrl { get; set; }
    }
}
