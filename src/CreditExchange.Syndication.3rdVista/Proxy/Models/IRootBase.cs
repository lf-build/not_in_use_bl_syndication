﻿namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IRootBase
    {
        string ExceptionText { get; set; }
        string MessageText { get; set; }
        string RowCount { get; set; }
        string PageCount { get; set; }
        string PageNumber { get; set; }
        string ReturnCode { get; set; }
        string ReturnText { get; set; }
        string ReturnType { get; set; }
    }
}
