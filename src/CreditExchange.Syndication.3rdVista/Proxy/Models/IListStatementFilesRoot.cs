﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IListStatementFilesRoot
    {
        IList<IStatementFile> StatementFiles { get; set; }
        string StatusText { get; set; }
        string RowCount { get; set; }
        string PageCount { get; set; }
        string PageNumber { get; set; }
        string ReturnCode { get; set; }
        string ReturnText { get; set; }
        string ReturnType { get; set; }
    }
}
