﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class BankAccountTransactionImage: IBankAccountTransactionImage
    {
        [JsonProperty("row_id")]
        public string RowId { get; set; }

        [JsonProperty("row_datetime")]
        public string RowDateTime { get; set; }

        [JsonProperty("row_epochtime")]
        public int RowEpochtime { get; set; }

        [JsonProperty("row_key")]
        public string RowKey { get; set; }

        [JsonProperty("user_email")]
        public string UserEmail { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("parent_customer_code")]
        public object ParentCustomerCode { get; set; }

        [JsonProperty("parent_customer_name")]
        public object ParentCustomerName { get; set; }

        [JsonProperty("extractor_code")]
        public string ExtractorCode { get; set; }

        [JsonProperty("extractor_name")]
        public string ExtractorName { get; set; }

        [JsonProperty("extractor_url")]
        public string ExtractorUrl { get; set; }

        [JsonProperty("intuit_id")]
        public string IntuitId { get; set; }

        [JsonProperty("provider_code")]
        public string ProviderCode { get; set; }

        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("provider_alias")]
        public string ProviderAlias { get; set; }

        [JsonProperty("provider_url")]
        public string ProviderUrl { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("country_name")]
        public string CountryName { get; set; }

        [JsonProperty("account_code")]
        public string AccountCode { get; set; }

        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [JsonProperty("bank_acct_code")]
        public string BankAccountCode { get; set; }

        [JsonProperty("bank_acct_name")]
        public string BankAccountName { get; set; }

        [JsonProperty("trans_datetime")]
        public string TransactionDateTime { get; set; }

        [JsonProperty("trans_epochtime")]
        public int TransactionEpochTime { get; set; }

        [JsonProperty("trans_date")]
        public string TransactionDate { get; set; }

        [JsonProperty("trans_epochdate")]
        public int TransactionEpochDate { get; set; }

        [JsonProperty("trans_time")]
        public string TransactionTime { get; set; }

        [JsonProperty("trans_month_num")]
        public string TransactionMonth { get; set; }

        [JsonProperty("trans_month_name")]
        public string TransactionMonthName { get; set; }

        [JsonProperty("trans_month_year")]
        public string TransactionMonthYear { get; set; }

        [JsonProperty("trans_month_day")]
        public string TransactionMonthDay { get; set; }

        [JsonProperty("trans_week_day")]
        public string TransactionWeekDay { get; set; }

        [JsonProperty("trans_week_quarter")]
        public string TransactionWeekQuarter { get; set; }

        [JsonProperty("trans_week_iso_year")]
        public string TransactionWeekISOYear { get; set; }

        [JsonProperty("trans_week_ansi_year")]
        public string TransactionWeekANSIYear { get; set; }

        [JsonProperty("trans_seq")]
        public int TransactionSeq { get; set; }

        [JsonProperty("trans_code")]
        public string TransactionCode { get; set; }

        [JsonProperty("trans_desc")]
        public string TransactionDesc { get; set; }

        [JsonProperty("credit_value")]
        public double CreditValue { get; set; }

        [JsonProperty("debit_value")]
        public double DebitValue { get; set; }

        [JsonProperty("net_value")]
        public double NetValue { get; set; }

        [JsonProperty("abs_value")]
        public double AbsValue { get; set; }

        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty("currency_name")]
        public string CurrencyName { get; set; }

        [JsonProperty("currency_sign")]
        public string CurrencySign { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }

        [JsonProperty("category_group")]
        public string CategoryGroup { get; set; }

        [JsonProperty("sub_category_name")]
        public string SubCategoryName { get; set; }

        [JsonProperty("cust_category_name")]
        public string CustomerCategoryName { get; set; }

        [JsonProperty("cust_category_group")]
        public string CustomerCategoryGroup { get; set; }

        [JsonProperty("cust_sub_category_name")]
        public string CustomerSubCategoryName { get; set; }

        [JsonProperty("cust_class_type")]
        public string CustomerClassType { get; set; }

        [JsonProperty("cust_class_name")]
        public string CustomerClassName { get; set; }

        [JsonProperty("trans_category_name")]
        public string TransactionCategoryName { get; set; }

        [JsonProperty("trans_category_group")]
        public string TransactionCategoryGroup { get; set; }

        [JsonProperty("trans_sub_category_name")]
        public string TransactionSubCategoryName { get; set; }

        [JsonProperty("trans_class_type")]
        public string TransactionClassType { get; set; }

        [JsonProperty("trans_class_name")]
        public string TransactionClassName { get; set; }

        [JsonProperty("class_type")]
        public string ClassType { get; set; }

        [JsonProperty("class_name")]
        public string ClassName { get; set; }

        [JsonProperty("saved_timestamp")]
        public string SavedTimestamp { get; set; }

        [JsonProperty("saved_epochtime")]
        public object SavedEpochtime { get; set; }

        [JsonProperty("valid_flag")]
        public bool ValidFlag { get; set; }

        [JsonProperty("image_seq")]
        public int ImageSeq { get; set; }

        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty("mime_type")]
        public string MIMEType { get; set; }

        [JsonProperty("image_timestamp")]
        public string ImageTimestamp { get; set; }

        [JsonProperty("image_epochtime")]
        public object ImageEpochtime { get; set; }

        [JsonProperty("image_count")]
        public int ImageCount { get; set; }

        [JsonProperty("check_number")]
        public object CheckNumber { get; set; }

        [JsonProperty("payee")]
        public object Payee { get; set; }

        [JsonProperty("trans_row_id")]
        public string TransactionRowId { get; set; }

        [JsonProperty("trans_row_key")]
        public string TransactionRowKey { get; set; }
    }
}
