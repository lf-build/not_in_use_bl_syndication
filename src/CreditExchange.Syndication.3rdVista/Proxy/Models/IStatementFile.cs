﻿namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IStatementFile
    {
        string RowId { get; set; }
        string RowDatetime { get; set; }
        int RowEpochtime { get; set; }
        string RowKey { get; set; }
        string StmtFileLabel { get; set; }
        string ContainerType { get; set; }
        string ContainerDesc { get; set; }
        string SubAcctCode { get; set; }
        string SubAcctName { get; set; }
        string UserEmail { get; set; }
        string UserName { get; set; }
        string CustomerCode { get; set; }
        string CustomerName { get; set; }
        object ParentCustomerCode { get; set; }
        object ParentCustomerName { get; set; }
        string FileDate { get; set; }
        int FileEpochdate { get; set; }
        string FileName { get; set; }
        int FileSize { get; set; }
        string FileUrl { get; set; }
        string MimeType { get; set; }
        string AccountCode { get; set; }
        string AccountName { get; set; }
        string ExtractorCode { get; set; }
        string ExtractorName { get; set; }
        string ExtractorUrl { get; set; }
        string IntuitId { get; set; }
        string ProviderCode { get; set; }
        string ProviderName { get; set; }
        string ProviderAlias { get; set; }
        string ProviderUrl { get; set; }
        string CountryCode { get; set; }
        string CountryName { get; set; }
        string FileTimestamp { get; set; }
        object FileEpochtime { get; set; }
        string IconUrl { get; set; }
    }
}
