﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class GetImageTransactionsRoot: IGetImageTransactionsRoot
    {
        [JsonProperty("bank_acct_trans_image")]
        [JsonConverter(typeof(ConcreteListJsonConverter<List<BankAccountTransactionImage>, IBankAccountTransactionImage>))]
        public IList<IBankAccountTransactionImage> BankAccountTransactionImage { get; set; }

        [JsonProperty("status_text")]
        public string StatusText { get; set; }

        [JsonProperty("row_count")]
        public string RowCount { get; set; }

        [JsonProperty("page_count")]
        public string PageCount { get; set; }

        [JsonProperty("page_number")]
        public string PageNumber { get; set; }

        [JsonProperty("return_code")]
        public string ReturnCode { get; set; }

        [JsonProperty("return_text")]
        public string ReturnText { get; set; }

        [JsonProperty("return_type")]
        public string ReturnType { get; set; }
    }
}
