﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class RootBase: IRootBase
    {
        [JsonProperty("message_text")]
        public string MessageText { get; set; }
        [JsonProperty("exception_text")]
        public string ExceptionText { get; set; }
        [JsonProperty("row_count")]
        public string RowCount { get; set; }

        [JsonProperty("page_count")]
        public string PageCount { get; set; }

        [JsonProperty("page_number")]
        public string PageNumber { get; set; }

        [JsonProperty("return_code")]
        public string ReturnCode { get; set; }

        [JsonProperty("return_text")]
        public string ReturnText { get; set; }

        [JsonProperty("return_type")]
        public string ReturnType { get; set; }
    }
}
