﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class AuthenticationResponseRoot: RootBase,IAuthenticationResponseRoot
    {
        [JsonProperty("user_email")]
        public string UserEmail { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("password_expired")]
        public bool PasswordExpired { get; set; }

        [JsonProperty("expiration_days")]
        public int ExpirationDays { get; set; }

        [JsonProperty("session_cookie")]
        public string SessionCookie { get; set; }

        [JsonProperty("session_id")]
        public string SessionId { get; set; }

        [JsonProperty("session_timeout")]
        public int SessionTimeout { get; set; }

        [JsonProperty("language_code")]
        public string LanguageCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("numeric_chars")]
        public string NumericChars { get; set; }

        [JsonProperty("date_mask")]
        public string DateMask { get; set; }

        [JsonProperty("time_mask")]
        public string TimeMask { get; set; }

        [JsonProperty("datetime_mask")]
        public string DatetimeMask { get; set; }

        [JsonProperty("timestamp_mask")]
        public string TimestampMask { get; set; }

        [JsonProperty("currency_sign")]
        public string CurrencySign { get; set; }

        [JsonProperty("enable_lock")]
        public bool EnableLock { get; set; }

        [JsonProperty("enable_cache")]
        public bool EnableCache { get; set; }

        [JsonProperty("enable_compression")]
        public bool EnableCompression { get; set; }

        [JsonProperty("lock_timeout")]
        public int LockTimeout { get; set; }

        [JsonProperty("value_separator")]
        public string ValueSeparator { get; set; }

        [JsonProperty("type_separator")]
        public string TypeSeparator { get; set; }

        [JsonProperty("field_separator")]
        public string FieldSeparator { get; set; }

        [JsonProperty("list_separator")]
        public string ListSeparator { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("customer_email")]
        public string CustomerEmail { get; set; }

        [JsonProperty("account_limit")]
        public int AccountLimit { get; set; }

        [JsonProperty("update_interval")]
        public int UpdateInterval { get; set; }

        [JsonProperty("concurrency_limit")]
        public int ConcurrencyLimit { get; set; }

        [JsonProperty("user_limit")]
        public int UserLimit { get; set; }

        [JsonProperty("fu_timestamp")]
        public string FuTimestamp { get; set; }

        [JsonProperty("lu_timestamp")]
        public string LuTimestamp { get; set; }

        [JsonProperty("fu_epochtime")]
        public long FuEpochtime { get; set; }

        [JsonProperty("lu_epochtime")]
        public long LuEpochtime { get; set; }

        [JsonProperty("server_timezone")]
        public string ServerTimezone { get; set; }

        [JsonProperty("client_timezone")]
        public string ClientTimezone { get; set; }

        [JsonProperty("bank_flag")]
        public string BankFlag { get; set; }

        [JsonProperty("cc_flag")]
        public string CcFlag { get; set; }

        [JsonProperty("inv_flag")]
        public string InvFlag { get; set; }

        [JsonProperty("stmt_flag")]
        public string StmtFlag { get; set; }

        [JsonProperty("bill_flag")]
        public string BillFlag { get; set; }

        [JsonProperty("image_flag")]
        public string ImageFlag { get; set; }

        [JsonProperty("max_trans_history")]
        public int MaxTransHistory { get; set; }

        [JsonProperty("max_stmt_history")]
        public int MaxStmtHistory { get; set; }

        [JsonProperty("max_trans_count")]
        public int MaxTransCount { get; set; }

        [JsonProperty("max_stmt_count")]
        public int MaxStmtCount { get; set; }

        [JsonProperty("def_trans_history")]
        public int DefTransHistory { get; set; }

        [JsonProperty("def_stmt_history")]
        public int DefStmtHistory { get; set; }

        [JsonProperty("def_trans_count")]
        public int DefTransCount { get; set; }

        [JsonProperty("def_stmt_count")]
        public int DefStmtCount { get; set; }

        [JsonProperty("role")]
        [JsonConverter(typeof(ConcreteListJsonConverter<List<Role>, IRole>))]
        public IList<IRole> Role { get; set; }
    }
}
