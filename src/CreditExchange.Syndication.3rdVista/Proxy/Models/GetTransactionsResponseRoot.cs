﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class GetTransactionsResponseRoot: RootBase, IGetTransactionsResponseRoot
    {
        [JsonProperty("sub_acct_trans")]
        [JsonConverter(typeof(ConcreteListJsonConverter<List<AccountTransactions>, IAccountTransactions>))]
        public IList<IAccountTransactions> AccountTransactions { get; set; }

        [JsonProperty("status_text")]
        public string StatusText { get; set; }
    }
}
