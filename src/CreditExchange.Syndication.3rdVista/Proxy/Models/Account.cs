﻿using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public class Account: IAccount
    {
        [JsonProperty("row_id")]
        public string RowId { get; set; }

        [JsonProperty("row_datetime")]
        public string RowDatetime { get; set; }

        [JsonProperty("row_epochtime")]
        public int RowEpochtime { get; set; }

        [JsonProperty("row_key")]
        public string RowKey { get; set; }

        [JsonProperty("account_label")]
        public string AccountLabel { get; set; }

        [JsonProperty("account_code")]
        public string AccountCode { get; set; }

        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [JsonProperty("user_email")]
        public string UserEmail { get; set; }

        [JsonProperty("user_name")]
        public string UserName { get; set; }

        [JsonProperty("customer_code")]
        public string CustomerCode { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("customer_email")]
        public object CustomerEmail { get; set; }

        [JsonProperty("parent_customer_code")]
        public object ParentCustomerCode { get; set; }

        [JsonProperty("parent_customer_name")]
        public object ParentCustomerName { get; set; }

        [JsonProperty("extractor_code")]
        public string ExtractorCode { get; set; }

        [JsonProperty("extractor_name")]
        public string ExtractorName { get; set; }

        [JsonProperty("extractor_url")]
        public string ExtractorUrl { get; set; }

        [JsonProperty("intuit_id")]
        public string IntuitId { get; set; }

        [JsonProperty("uncategorized_name")]
        public string UncategorizedName { get; set; }

        [JsonProperty("provider_code")]
        public string ProviderCode { get; set; }

        [JsonProperty("provider_name")]
        public string ProviderName { get; set; }

        [JsonProperty("provider_alias")]
        public string ProviderAlias { get; set; }

        [JsonProperty("provider_url")]
        public string ProviderUrl { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("country_name")]
        public string CountryName { get; set; }

        [JsonProperty("icon_url")]
        public string IconUrl { get; set; }

        [JsonProperty("created_timestamp")]
        public string CreatedTimestamp { get; set; }

        [JsonProperty("updated_timestamp")]
        public string UpdatedTimestamp { get; set; }

        [JsonProperty("executed_timestamp")]
        public string ExecutedTimestamp { get; set; }

        [JsonProperty("created_epochtime")]
        public object CreatedEpochtime { get; set; }

        [JsonProperty("updated_epochtime")]
        public object UpdatedEpochtime { get; set; }

        [JsonProperty("executed_epochtime")]
        public object ExecutedEpochtime { get; set; }

        [JsonProperty("created_timelapse")]
        public string CreatedTimelapse { get; set; }

        [JsonProperty("updated_timelapse")]
        public string UpdatedTimelapse { get; set; }

        [JsonProperty("executed_timelapse")]
        public string ExecutedTimelapse { get; set; }

        [JsonProperty("status_type")]
        public string StatusType { get; set; }

        [JsonProperty("status_desc")]
        public string StatusDesc { get; set; }

        [JsonProperty("status_message")]
        public string StatusMessage { get; set; }

        [JsonProperty("trans_history")]
        public int TransHistory { get; set; }

        [JsonProperty("trans_count")]
        public int TransCount { get; set; }

        [JsonProperty("stmt_history")]
        public int StmtHistory { get; set; }

        [JsonProperty("stmt_count")]
        public int StmtCount { get; set; }

        [JsonProperty("sug_trans_history")]
        public int SugTransHistory { get; set; }

        [JsonProperty("sug_stmt_history")]
        public int SugStmtHistory { get; set; }

        [JsonProperty("bank_flag")]
        public bool BankFlag { get; set; }

        [JsonProperty("cc_flag")]
        public bool CcFlag { get; set; }

        [JsonProperty("inv_flag")]
        public bool InvFlag { get; set; }

        [JsonProperty("stmt_flag")]
        public bool StmtFlag { get; set; }

        [JsonProperty("bill_flag")]
        public bool BillFlag { get; set; }

        [JsonProperty("offline_flag")]
        public bool OfflineFlag { get; set; }

        [JsonProperty("image_flag")]
        public bool ImageFlag { get; set; }

        [JsonProperty("fund_flag")]
        public bool FundFlag { get; set; }

        [JsonProperty("trans_flag")]
        public bool TransFlag { get; set; }

        [JsonProperty("empty_flag")]
        public bool EmptyFlag { get; set; }

        [JsonProperty("executing_flag")]
        public bool ExecutingFlag { get; set; }

        [JsonProperty("retry_flag")]
        public bool RetryFlag { get; set; }

        [JsonProperty("retry_count")]
        public object RetryCount { get; set; }

        [JsonProperty("active_flag")]
        public bool ActiveFlag { get; set; }

        [JsonProperty("sel_bank_flag")]
        public bool SelBankFlag { get; set; }

        [JsonProperty("sel_cc_flag")]
        public bool SelCcFlag { get; set; }

        [JsonProperty("sel_inv_flag")]
        public bool SelInvFlag { get; set; }

        [JsonProperty("sel_stmt_flag")]
        public bool SelStmtFlag { get; set; }

        [JsonProperty("sel_bill_flag")]
        public bool SelBillFlag { get; set; }

        [JsonProperty("sel_image_flag")]
        public bool SelImageFlag { get; set; }

        [JsonProperty("ext_bank_flag")]
        public bool ExtBankFlag { get; set; }

        [JsonProperty("ext_cc_flag")]
        public bool ExtCcFlag { get; set; }

        [JsonProperty("ext_inv_flag")]
        public bool ExtInvFlag { get; set; }

        [JsonProperty("ext_stmt_flag")]
        public bool ExtStmtFlag { get; set; }

        [JsonProperty("ext_bill_flag")]
        public bool ExtBillFlag { get; set; }

        [JsonProperty("ext_image_flag")]
        public bool ExtImageFlag { get; set; }

        [JsonProperty("cst_bank_flag")]
        public bool CstBankFlag { get; set; }

        [JsonProperty("cst_cc_flag")]
        public bool CstCcFlag { get; set; }

        [JsonProperty("cst_inv_flag")]
        public bool CstInvFlag { get; set; }

        [JsonProperty("cst_stmt_flag")]
        public bool CstStmtFlag { get; set; }

        [JsonProperty("cst_bill_flag")]
        public bool CstBillFlag { get; set; }

        [JsonProperty("cst_image_flag")]
        public bool CstImageFlag { get; set; }

        [JsonProperty("operation_type")]
        public object OperationType { get; set; }

        [JsonProperty("operation_desc")]
        public object OperationDesc { get; set; }

        [JsonProperty("started_url")]
        public object StartedUrl { get; set; }

        [JsonProperty("ended_url")]
        public object EndedUrl { get; set; }

        [JsonProperty("cancelled_url")]
        public object CancelledUrl { get; set; }

        [JsonProperty("success_url")]
        public object SuccessUrl { get; set; }

        [JsonProperty("failure_url")]
        public object FailureUrl { get; set; }

        [JsonProperty("message_url")]
        public object MessageUrl { get; set; }

        [JsonProperty("warning_url")]
        public object WarningUrl { get; set; }

        [JsonProperty("error_url")]
        public object ErrorUrl { get; set; }

        [JsonProperty("added_url")]
        public object AddedUrl { get; set; }

        [JsonProperty("updated_url")]
        public object UpdatedUrl { get; set; }

        [JsonProperty("reloaded_url")]
        public object ReloadedUrl { get; set; }

        [JsonProperty("changed_url")]
        public object ChangedUrl { get; set; }
    }
}
