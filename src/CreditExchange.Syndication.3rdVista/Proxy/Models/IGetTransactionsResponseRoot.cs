﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Models
{
    public interface IGetTransactionsResponseRoot:IRootBase
    {
        IList<IAccountTransactions> AccountTransactions { get; set; }
        string StatusText { get; set; }
    }
}
