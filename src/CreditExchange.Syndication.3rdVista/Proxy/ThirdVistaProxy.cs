﻿using System;
using CreditExchange.Syndication.ThirdVista.Proxy.Response;
using RestSharp;
using Newtonsoft.Json;
using System.Net;

namespace CreditExchange.Syndication.ThirdVista.Proxy
{
    public class ThirdVistaProxy : IThirdVistaProxy
    {
        public ThirdVistaProxy(ThirdVistaConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.BaseUrl))
                throw new ArgumentNullException(nameof(configuration.BaseUrl));
            if (string.IsNullOrWhiteSpace(configuration.AuthenticationApi))
                throw new ArgumentNullException(nameof(configuration.AuthenticationApi));
            if (string.IsNullOrWhiteSpace(configuration.DownLoadStatementFileApi))
                throw new ArgumentNullException(nameof(configuration.DownLoadStatementFileApi));
            if (string.IsNullOrWhiteSpace(configuration.DownLoadTransactionImageApi))
                throw new ArgumentNullException(nameof(configuration.DownLoadTransactionImageApi));
            if (string.IsNullOrWhiteSpace(configuration.GetAccountsApi))
                throw new ArgumentNullException(nameof(configuration.GetAccountsApi));
            if (string.IsNullOrWhiteSpace(configuration.GetStatementFilesApi))
                throw new ArgumentNullException(nameof(configuration.GetStatementFilesApi));
            if (string.IsNullOrWhiteSpace(configuration.GetTransactionImageApi))
                throw new ArgumentNullException(nameof(configuration.GetTransactionImageApi));
            if (string.IsNullOrWhiteSpace(configuration.GetTransactionsApi))
                throw new ArgumentNullException(nameof(configuration.GetTransactionsApi));
            if (string.IsNullOrWhiteSpace(configuration.RegisterUserApi))
                throw new ArgumentNullException(nameof(configuration.RegisterUserApi));
            Configuration = configuration;
        }
        private IThirdVistaConfiguration Configuration { get; }
        #region Api's Implementation
        public IRegisterUserResponse RegisterUser(string customerSessionId, string email, string password)
        {
            if (string.IsNullOrWhiteSpace(customerSessionId))
                throw new ArgumentNullException(nameof(customerSessionId));
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.RegisterUserApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, customerSessionId);
            request.AddParameter("p_user_email", email);
            request.AddParameter("p_user_name", email);
            request.AddParameter("p_user_password", password);
            var response = ExecuteRequest<RegisterUserResponse>(client, request);
            return response;
        }
        public IAuthenticationResponse AuthenticateUser(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.AuthenticationApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, string.Empty);
            request.AddParameter("p_user_email", email);
            request.AddParameter("p_user_password", password);
            var response = ExecuteRequest<AuthenticationResponse>(client, request);
            return response;
        }
        public IListAccountsResponse GetAccounts(string sessionId)
        {
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.GetAccountsApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, sessionId);
            var response = ExecuteRequest<ListAccountsResponse>(client, request);
            return response;
        }
        public IGetTransactionsResponse GetTransactions(string sessionId)
        {
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.GetTransactionsApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, sessionId);
            var response = ExecuteRequest<GetTransactionsResponse>(client, request);
            return response;
        }
        public IGetImageTransactionsResponse GetImageTransactions(string sessionId)
        {
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.GetTransactionImageApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, sessionId);
            var response = ExecuteRequest<GetImageTransactionsResponse>(client, request);
            return response;
        }
        public IListStatementFilesResponse GetStatementFiles(string sessionId)
        {
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));

            var client = new RestClient(Configuration.BaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.GetStatementFilesApi.TrimEnd('/'), Method.POST);
            AddDefaultParam(ref request, sessionId);
            var response = ExecuteRequest<ListStatementFilesResponse>(client, request);
            return response;
        }
        #endregion
        #region Private Members
        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }
        private void AddDefaultParam(ref IRestRequest request, string sessionId)
        {
            if (!string.IsNullOrEmpty(sessionId))
                request.AddParameter("p_session_id", WebUtility.UrlEncode(sessionId));
            request.AddParameter("p_output_type", "JSON");
        }
        #endregion
    }
}
