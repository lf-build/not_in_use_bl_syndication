﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IListAccountsResponse
    {
        IListAccountsResponseRoot ListAccountsResponseRoot { get; set; }
    }
}
