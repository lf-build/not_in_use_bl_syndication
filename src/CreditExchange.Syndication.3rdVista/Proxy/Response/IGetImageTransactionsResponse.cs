﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IGetImageTransactionsResponse
    {
        IGetImageTransactionsRoot Root { get; set; }
    }
}
