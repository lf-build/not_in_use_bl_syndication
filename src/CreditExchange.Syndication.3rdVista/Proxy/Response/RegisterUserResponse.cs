﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class RegisterUserResponse: IRegisterUserResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<RegisterUserResponseRoot>))]
        public IRegisterUserResponseRoot Root { get; set; }
    }
}
