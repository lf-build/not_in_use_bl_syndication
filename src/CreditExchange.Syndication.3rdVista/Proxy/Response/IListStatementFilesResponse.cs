﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IListStatementFilesResponse
    {
        IListStatementFilesRoot Root { get; set; }
    }
}
