﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class AuthenticationResponse: IAuthenticationResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<AuthenticationResponseRoot>))]
        public IAuthenticationResponseRoot AuthenticationResponseRoot { get; set; }
    }
}
