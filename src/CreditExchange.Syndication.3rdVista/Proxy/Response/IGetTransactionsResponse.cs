﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IGetTransactionsResponse
    {
        IGetTransactionsResponseRoot GetTransactionsResponseRoot { get; set; }
    }
}
