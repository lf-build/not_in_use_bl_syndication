﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class GetTransactionsResponse: IGetTransactionsResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<GetTransactionsResponseRoot>))]
        public IGetTransactionsResponseRoot GetTransactionsResponseRoot { get; set; }
    }
}
