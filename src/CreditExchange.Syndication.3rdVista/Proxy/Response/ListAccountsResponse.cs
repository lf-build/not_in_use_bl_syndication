﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class ListAccountsResponse: IListAccountsResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<ListAccountsResponseRoot>))]
        public IListAccountsResponseRoot ListAccountsResponseRoot { get; set; }
    }
}
