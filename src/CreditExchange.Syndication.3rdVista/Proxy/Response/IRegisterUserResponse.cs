﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IRegisterUserResponse
    {
        IRegisterUserResponseRoot Root { get; set; }
    }
}
