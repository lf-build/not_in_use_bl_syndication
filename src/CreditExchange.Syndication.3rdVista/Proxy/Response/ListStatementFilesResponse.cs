﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class ListStatementFilesResponse: IListStatementFilesResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<ListStatementFilesRoot>))]
        public IListStatementFilesRoot Root { get; set; }
    }
}
