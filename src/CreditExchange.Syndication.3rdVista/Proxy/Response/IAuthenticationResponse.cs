﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public interface IAuthenticationResponse
    {
        IAuthenticationResponseRoot AuthenticationResponseRoot { get; set; }
    }
}
