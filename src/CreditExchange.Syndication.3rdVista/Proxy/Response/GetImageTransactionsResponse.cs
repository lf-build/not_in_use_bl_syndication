﻿using CreditExchange.Syndication.ThirdVista.Proxy.Models;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.ThirdVista.Proxy.Response
{
    public class GetImageTransactionsResponse: IGetImageTransactionsResponse
    {
        [JsonProperty("root")]
        [JsonConverter(typeof(ConcreteJsonConverter<GetImageTransactionsRoot>))]
        public IGetImageTransactionsRoot Root { get; set; }
    }
}
