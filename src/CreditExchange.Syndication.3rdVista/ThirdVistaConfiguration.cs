﻿namespace CreditExchange.Syndication.ThirdVista
{
    public class ThirdVistaConfiguration : IThirdVistaConfiguration
    {
        public string BaseUrl { get; set; } = "https://www.ledgersyncapp.com/apis/tst0001";
        public string AuthenticationApi { get; set; } = "fde.authenticate_user";
        public string GetTransactionsApi { get; set; } = "fde.list_sub_acct_trans";
        public string GetTransactionImageApi { get; set; } = "fde.list_bank_acct_trans_image";
        public string DownLoadTransactionImageApi { get; set; } = "fde.get_trans_image";
        public string GetStatementFilesApi { get; set; } = "fde.list_stmt_file";
        public string DownLoadStatementFileApi { get; set; } = "fde.download_stmt_file";
        public string RegisterUserApi { get; set; } = "fde.register_user";
        public string GetAccountsApi { get; set; } = "fde.list_account";
    }
}
