﻿using LendFoundry.SyndicationStore.Events;

namespace CreditExchange.Syndication.ThirdVista.Events
{
    public class ThirdVistaAuthenticateUserRequested : SyndicationCalledEvent
    {
    }
}