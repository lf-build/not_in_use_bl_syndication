﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class GetImageTransactionsResponse: IGetImageTransactionsResponse
    {
        public GetImageTransactionsResponse()
        {

        }
        public GetImageTransactionsResponse(Proxy.Response.IGetImageTransactionsResponse getImageTransactionsResponse)
        {
            if (getImageTransactionsResponse != null)
                Root = new GetImageTransactionsRoot(getImageTransactionsResponse.Root);
        }
        public IGetImageTransactionsRoot Root { get; set; }
    }
}
