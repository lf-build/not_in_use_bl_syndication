﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public interface IAuthenticationResponseRoot:IRootBase
    {
       string UserEmail { get; set; }
       string UserName { get; set; }
       bool PasswordExpired { get; set; }
       int ExpirationDays { get; set; }
       string SessionCookie { get; set; }
       string SessionId { get; set; }
       int SessionTimeout { get; set; }
       string LanguageCode { get; set; }
       string CountryCode { get; set; }
       string NumericChars { get; set; }
       string DateMask { get; set; }
       string TimeMask { get; set; }
       string DatetimeMask { get; set; }
       string TimestampMask { get; set; }
       string CurrencySign { get; set; }
       bool EnableLock { get; set; }
       bool EnableCache { get; set; }
       bool EnableCompression { get; set; }
       int LockTimeout { get; set; }
       string ValueSeparator { get; set; }
       string TypeSeparator { get; set; }
       string FieldSeparator { get; set; }
       string ListSeparator { get; set; }
       string CustomerCode { get; set; }
       string CustomerName { get; set; }
       string CustomerEmail { get; set; }
       int AccountLimit { get; set; }
       int UpdateInterval { get; set; }
       int ConcurrencyLimit { get; set; }
       int UserLimit { get; set; }
       string FuTimestamp { get; set; }
       string LuTimestamp { get; set; }
       long FuEpochtime { get; set; }
       long LuEpochtime { get; set; }
       string ServerTimezone { get; set; }
       string ClientTimezone { get; set; }
       string BankFlag { get; set; }
       string CcFlag { get; set; }
       string InvFlag { get; set; }
       string StmtFlag { get; set; }
       string BillFlag { get; set; }
       string ImageFlag { get; set; }
       int MaxTransHistory { get; set; }
       int MaxStmtHistory { get; set; }
       int MaxTransCount { get; set; }
       int MaxStmtCount { get; set; }
       int DefTransHistory { get; set; }
       int DefStmtHistory { get; set; }
       int DefTransCount { get; set; }
       int DefStmtCount { get; set; }
       IList<IRole> Roles { get; set; }
    }
}
