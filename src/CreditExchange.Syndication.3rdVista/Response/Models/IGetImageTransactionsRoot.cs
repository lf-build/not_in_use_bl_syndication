﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public interface IGetImageTransactionsRoot
    {
       IList<IBankAccountTransactionImage> BankAccountTransactionImage { get; set; }
       string StatusText { get; set; }
       string RowCount { get; set; }
       string PageCount { get; set; }
       string PageNumber { get; set; }
       string ReturnCode { get; set; }
       string ReturnText { get; set; }
       string ReturnType { get; set; }
    }
}
