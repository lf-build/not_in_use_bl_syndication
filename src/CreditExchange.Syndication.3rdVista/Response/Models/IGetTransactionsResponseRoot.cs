﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public interface IGetTransactionsResponseRoot:IRootBase
    {
        IList<IAccountTransactions> AccountTransactions { get; set; }
        string StatusText { get; set; }
    }
}
