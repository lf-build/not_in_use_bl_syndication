﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class Role: IRole
    {
        public Role() { }
        public Role(Proxy.Models.IRole role)
        {
            RoleCode = role?.RoleCode;
            RoleDescription = role?.RoleDescription;
        }
        public string RoleCode { get; set; }
        public string RoleDescription { get; set; }
    }
}
