﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class AccountTransactions : IAccountTransactions
    {
        public AccountTransactions() { }
        public AccountTransactions(Proxy.Models.IAccountTransactions accountTransactions)
        {
            if (accountTransactions != null)
            {
                RowId = accountTransactions.RowId;
                RowDatetime = accountTransactions.RowDatetime;
                RowEpochtime = accountTransactions.RowEpochtime;
                RowKey = accountTransactions.RowKey;
                ContainerType = accountTransactions.ContainerType;
                ContainerDesc = accountTransactions.ContainerDesc;
                ExtractorCode = accountTransactions.ExtractorCode;
                ExtractorName = accountTransactions.ExtractorName;
                ExtractorUrl = accountTransactions.ExtractorUrl;
                IntuitId = accountTransactions.IntuitId;
                ProviderCode = accountTransactions.ProviderCode;
                ProviderName = accountTransactions.ProviderName;
                ProviderAlias = accountTransactions.ProviderAlias;
                ProviderUrl = accountTransactions.ProviderUrl;
                CountryCode = accountTransactions.CountryCode;
                CountryName = accountTransactions.CountryName;
                AccountCode = accountTransactions.AccountCode;
                AccountName = accountTransactions.AccountName;
                SubAcctCode = accountTransactions.SubAcctCode;
                SubAcctName = accountTransactions.SubAcctName;
                UserEmail = accountTransactions.UserEmail;
                UserName = accountTransactions.UserName;
                CustomerCode = accountTransactions.CustomerCode;
                CustomerName = accountTransactions.CustomerName;
                ParentCustomerCode = accountTransactions.ParentCustomerCode;
                ParentCustomerName = accountTransactions.ParentCustomerName;
                TransDatetime = accountTransactions.TransDatetime;
                TransEpochtime = accountTransactions.TransEpochtime;
                TransDate = accountTransactions.TransDate;
                TransEpochdate = accountTransactions.TransEpochdate;
                TransTime = accountTransactions.TransTime;
                TransMonthNum = accountTransactions.TransMonthNum;
                TransMonthName = accountTransactions.TransMonthName;
                TransMonthYear = accountTransactions.TransMonthYear;
                TransMonthDay = accountTransactions.TransMonthDay;
                TransWeekDay = accountTransactions.TransWeekDay;
                TransWeekQuarter = accountTransactions.TransWeekQuarter;
                TransWeekIsoYear = accountTransactions.TransWeekIsoYear;
                TransWeekAnsiYear = accountTransactions.TransWeekAnsiYear;
                TransSeq = accountTransactions.TransSeq;
                TransCode = accountTransactions.TransCode;
                TransDesc = accountTransactions.TransDesc;
                CreditValue = accountTransactions.CreditValue;
                DebitValue = accountTransactions.DebitValue;
                NetValue = accountTransactions.NetValue;
                AbsValue = accountTransactions.AbsValue;
                CurrencyCode = accountTransactions.CurrencyCode;
                CurrencyName = accountTransactions.CurrencyName;
                CurrencySign = accountTransactions.CurrencySign;
                CategoryName = accountTransactions.CategoryName;
                CategoryGroup = accountTransactions.CategoryGroup;
                SubCategoryName = accountTransactions.SubCategoryName;
                CustCategoryName = accountTransactions.CustCategoryName;
                CustCategoryGroup = accountTransactions.CustCategoryGroup;
                CustSubCategoryName = accountTransactions.CustSubCategoryName;
                CustClassType = accountTransactions.CustClassType;
                CustClassName = accountTransactions.CustClassName;
                TransCategoryName = accountTransactions.TransCategoryName;
                TransCategoryGroup = accountTransactions.TransCategoryGroup;
                TransSubCategoryName = accountTransactions.TransSubCategoryName;
                TransClassType = accountTransactions.TransClassType;
                TransClassName = accountTransactions.TransClassName;
                OrigTransDesc = accountTransactions.OrigTransDesc;
                ClassType = accountTransactions.ClassType;
                ClassName = accountTransactions.ClassName;
                SavedTimestamp = accountTransactions.SavedTimestamp;
                SavedEpochtime = accountTransactions.SavedEpochtime;
                CustomFlag = accountTransactions.CustomFlag;
                ValidFlag = accountTransactions.ValidFlag;
                MimeType = accountTransactions.MimeType;
                ImageFlag = accountTransactions.ImageFlag;
                FundFlag = accountTransactions.FundFlag;
                ImageUrl = accountTransactions.ImageUrl;
                CheckNumber = accountTransactions.CheckNumber;
                FundCode = accountTransactions.FundCode;
                FundName = accountTransactions.FundName;
                FundQuant = accountTransactions.FundQuant;
                FundPrice = accountTransactions.FundPrice;
                ImageCount = accountTransactions.ImageCount;
                FundCount = accountTransactions.FundCount;
            }
        }
        public string RowId { get; set; }
        public string RowDatetime { get; set; }
        public int RowEpochtime { get; set; }
        public string RowKey { get; set; }
        public string ContainerType { get; set; }
        public string ContainerDesc { get; set; }
        public string ExtractorCode { get; set; }
        public string ExtractorName { get; set; }
        public string ExtractorUrl { get; set; }
        public string IntuitId { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAlias { get; set; }
        public string ProviderUrl { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string SubAcctCode { get; set; }
        public string SubAcctName { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public object ParentCustomerCode { get; set; }
        public object ParentCustomerName { get; set; }
        public string TransDatetime { get; set; }
        public int TransEpochtime { get; set; }
        public string TransDate { get; set; }
        public int TransEpochdate { get; set; }
        public string TransTime { get; set; }
        public string TransMonthNum { get; set; }
        public string TransMonthName { get; set; }
        public string TransMonthYear { get; set; }
        public string TransMonthDay { get; set; }
        public string TransWeekDay { get; set; }
        public string TransWeekQuarter { get; set; }
        public string TransWeekIsoYear { get; set; }
        public string TransWeekAnsiYear { get; set; }
        public int TransSeq { get; set; }
        public string TransCode { get; set; }
        public string TransDesc { get; set; }
        public double CreditValue { get; set; }
        public double DebitValue { get; set; }
        public double NetValue { get; set; }
        public double AbsValue { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySign { get; set; }
        public string CategoryName { get; set; }
        public string CategoryGroup { get; set; }
        public string SubCategoryName { get; set; }
        public string CustCategoryName { get; set; }
        public string CustCategoryGroup { get; set; }
        public string CustSubCategoryName { get; set; }
        public string CustClassType { get; set; }
        public string CustClassName { get; set; }
        public string TransCategoryName { get; set; }
        public string TransCategoryGroup { get; set; }
        public string TransSubCategoryName { get; set; }
        public string TransClassType { get; set; }
        public string TransClassName { get; set; }
        public string OrigTransDesc { get; set; }
        public string ClassType { get; set; }
        public string ClassName { get; set; }
        public string SavedTimestamp { get; set; }
        public object SavedEpochtime { get; set; }
        public bool CustomFlag { get; set; }
        public bool ValidFlag { get; set; }
        public object MimeType { get; set; }
        public bool ImageFlag { get; set; }
        public bool FundFlag { get; set; }
        public object ImageUrl { get; set; }
        public object CheckNumber { get; set; }
        public object FundCode { get; set; }
        public object FundName { get; set; }
        public object FundQuant { get; set; }
        public object FundPrice { get; set; }
        public int ImageCount { get; set; }
        public int FundCount { get; set; }
    }
}
