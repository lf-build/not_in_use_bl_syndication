﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class GetImageTransactionsRoot : IGetImageTransactionsRoot
    {
        public GetImageTransactionsRoot()
        {

        }
        public GetImageTransactionsRoot(Proxy.Models.IGetImageTransactionsRoot getImageTransactionsRoot)
        {
            if (getImageTransactionsRoot != null)
            {
                StatusText = getImageTransactionsRoot.StatusText;
                RowCount = getImageTransactionsRoot.RowCount;
                PageCount = getImageTransactionsRoot.PageCount;
                PageNumber = getImageTransactionsRoot.PageNumber;
                ReturnCode = getImageTransactionsRoot.ReturnCode;
                ReturnText = getImageTransactionsRoot.ReturnText;
                ReturnType = getImageTransactionsRoot.ReturnType;
                if (getImageTransactionsRoot.BankAccountTransactionImage != null)
                    CopyBankAccountTransactionImageList(getImageTransactionsRoot.BankAccountTransactionImage);
            }
        }
        public IList<IBankAccountTransactionImage> BankAccountTransactionImage { get; set; }
        public string StatusText { get; set; }
        public string RowCount { get; set; }
        public string PageCount { get; set; }
        public string PageNumber { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnText { get; set; }
        public string ReturnType { get; set; }
        private void CopyBankAccountTransactionImageList(IList<Proxy.Models.IBankAccountTransactionImage> BankAccountTransactionImagelist)
        {
            BankAccountTransactionImage = new List<IBankAccountTransactionImage>();
            foreach (var item in BankAccountTransactionImagelist)
            {
                BankAccountTransactionImage.Add(new BankAccountTransactionImage(item));
            }
        }
    }
}
