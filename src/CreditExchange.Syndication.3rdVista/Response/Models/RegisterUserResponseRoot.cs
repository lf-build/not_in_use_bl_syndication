﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class RegisterUserResponseRoot : RootBase, IRegisterUserResponseRoot
    {
        public RegisterUserResponseRoot() { }
        public RegisterUserResponseRoot(Proxy.Models.IRegisterUserResponseRoot registerUserResponseRoot) :base(registerUserResponseRoot)
        {

        }
    }
}
