﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class RootBase : IRootBase
    {
        public RootBase() { }
        public RootBase(Proxy.Models.IRootBase rootBase)
        {
            if (rootBase != null)
            {
                MessageText = rootBase.MessageText;
                ExceptionText = rootBase.ExceptionText;
                RowCount = rootBase.RowCount;
                PageCount = rootBase.PageCount;
                PageNumber = rootBase.PageNumber;
                ReturnCode = rootBase.ReturnCode;
                ReturnText = rootBase.ReturnText;
                ReturnType = rootBase.ReturnType;
            }
        }
        public string MessageText { get; set; }
        public string ExceptionText { get; set; }
        public string RowCount { get; set; }
        public string PageCount { get; set; }
        public string PageNumber { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnText { get; set; }
        public string ReturnType { get; set; }
    }
}
