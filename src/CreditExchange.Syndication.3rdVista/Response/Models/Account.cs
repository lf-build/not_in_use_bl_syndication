﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class Account : IAccount
    {
        public Account() { }
        public Account(Proxy.Models.IAccount account)
        {
            if (account != null)
            {
                RowId = account.RowId;
                RowDatetime = account.RowDatetime;
                RowEpochtime = account.RowEpochtime;
                RowKey = account.RowKey;
                AccountLabel = account.AccountLabel;
                AccountCode = account.AccountCode;
                AccountName = account.AccountName;
                UserEmail = account.UserEmail;
                UserName = account.UserName;
                CustomerCode = account.CustomerCode;
                CustomerName = account.CustomerName;
                CustomerEmail = account.CustomerEmail;
                ParentCustomerCode = account.ParentCustomerCode;
                ParentCustomerName = account.ParentCustomerName;
                ExtractorCode = account.ExtractorCode;
                ExtractorName = account.ExtractorName;
                ExtractorUrl = account.ExtractorUrl;
                IntuitId = account.IntuitId;
                UncategorizedName = account.UncategorizedName;
                ProviderCode = account.ProviderCode;
                ProviderName = account.ProviderName;
                ProviderAlias = account.ProviderAlias;
                ProviderUrl = account.ProviderUrl;
                CountryCode = account.CountryCode;
                CountryName = account.CountryName;
                IconUrl = account.IconUrl;
                CreatedTimestamp = account.CreatedTimestamp;
                UpdatedTimestamp = account.UpdatedTimestamp;
                ExecutedTimestamp = account.ExecutedTimestamp;
                CreatedEpochtime = account.CreatedEpochtime;
                UpdatedEpochtime = account.UpdatedEpochtime;
                ExecutedEpochtime = account.ExecutedEpochtime;
                CreatedTimelapse = account.CreatedTimelapse;
                UpdatedTimelapse = account.UpdatedTimelapse;
                ExecutedTimelapse = account.ExecutedTimelapse;
                StatusType = account.StatusType;
                StatusDesc = account.StatusDesc;
                StatusMessage = account.StatusMessage;
                TransHistory = account.TransHistory;
                TransCount = account.TransCount;
                StmtHistory = account.StmtHistory;
                StmtCount = account.StmtCount;
                SugTransHistory = account.SugTransHistory;
                SugStmtHistory = account.SugStmtHistory;
                BankFlag = account.BankFlag;
                CcFlag = account.CcFlag;
                InvFlag = account.InvFlag;
                StmtFlag = account.StmtFlag;
                BillFlag = account.BillFlag;
                OfflineFlag = account.OfflineFlag;
                ImageFlag = account.ImageFlag;
                FundFlag = account.FundFlag;
                TransFlag = account.TransFlag;
                EmptyFlag = account.EmptyFlag;
                ExecutingFlag = account.ExecutingFlag;
                RetryFlag = account.RetryFlag;
                RetryCount = account.RetryCount;
                ActiveFlag = account.ActiveFlag;
                SelBankFlag = account.SelBankFlag;
                SelCcFlag = account.SelCcFlag;
                SelInvFlag = account.SelInvFlag;
                SelStmtFlag = account.SelStmtFlag;
                SelBillFlag = account.SelBillFlag;
                SelImageFlag = account.SelImageFlag;
                ExtBankFlag = account.ExtBankFlag;
                ExtCcFlag = account.ExtCcFlag;
                ExtInvFlag = account.ExtInvFlag;
                ExtStmtFlag = account.ExtStmtFlag;
                ExtBillFlag = account.ExtBillFlag;
                ExtImageFlag = account.ExtImageFlag;
                CstBankFlag = account.CstBankFlag;
                CstCcFlag = account.CstCcFlag;
                CstInvFlag = account.CstInvFlag;
                CstStmtFlag = account.CstStmtFlag;
                CstBillFlag = account.CstBillFlag;
                CstImageFlag = account.CstImageFlag;
                OperationType = account.OperationType;
                OperationDesc = account.OperationDesc;
                StartedUrl = account.StartedUrl;
                EndedUrl = account.EndedUrl;
                CancelledUrl = account.CancelledUrl;
                SuccessUrl = account.SuccessUrl;
                FailureUrl = account.FailureUrl;
                MessageUrl = account.MessageUrl;
                WarningUrl = account.WarningUrl;
                ErrorUrl = account.ErrorUrl;
                AddedUrl = account.AddedUrl;
                UpdatedUrl = account.UpdatedUrl;
                ReloadedUrl = account.ReloadedUrl;
                ChangedUrl = account.ChangedUrl;
            }
        }
        public string RowId { get; set; }
        public string RowDatetime { get; set; }
        public int RowEpochtime { get; set; }
        public string RowKey { get; set; }
        public string AccountLabel { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public object CustomerEmail { get; set; }
        public object ParentCustomerCode { get; set; }
        public object ParentCustomerName { get; set; }
        public string ExtractorCode { get; set; }
        public string ExtractorName { get; set; }
        public string ExtractorUrl { get; set; }
        public string IntuitId { get; set; }
        public string UncategorizedName { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAlias { get; set; }
        public string ProviderUrl { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string IconUrl { get; set; }
        public string CreatedTimestamp { get; set; }
        public string UpdatedTimestamp { get; set; }
        public string ExecutedTimestamp { get; set; }
        public object CreatedEpochtime { get; set; }
        public object UpdatedEpochtime { get; set; }
        public object ExecutedEpochtime { get; set; }
        public string CreatedTimelapse { get; set; }
        public string UpdatedTimelapse { get; set; }
        public string ExecutedTimelapse { get; set; }
        public string StatusType { get; set; }
        public string StatusDesc { get; set; }
        public string StatusMessage { get; set; }
        public int TransHistory { get; set; }
        public int TransCount { get; set; }
        public int StmtHistory { get; set; }
        public int StmtCount { get; set; }
        public int SugTransHistory { get; set; }
        public int SugStmtHistory { get; set; }
        public bool BankFlag { get; set; }
        public bool CcFlag { get; set; }
        public bool InvFlag { get; set; }
        public bool StmtFlag { get; set; }
        public bool BillFlag { get; set; }
        public bool OfflineFlag { get; set; }
        public bool ImageFlag { get; set; }
        public bool FundFlag { get; set; }
        public bool TransFlag { get; set; }
        public bool EmptyFlag { get; set; }
        public bool ExecutingFlag { get; set; }
        public bool RetryFlag { get; set; }
        public object RetryCount { get; set; }
        public bool ActiveFlag { get; set; }
        public bool SelBankFlag { get; set; }
        public bool SelCcFlag { get; set; }
        public bool SelInvFlag { get; set; }
        public bool SelStmtFlag { get; set; }
        public bool SelBillFlag { get; set; }
        public bool SelImageFlag { get; set; }
        public bool ExtBankFlag { get; set; }
        public bool ExtCcFlag { get; set; }
        public bool ExtInvFlag { get; set; }
        public bool ExtStmtFlag { get; set; }
        public bool ExtBillFlag { get; set; }
        public bool ExtImageFlag { get; set; }
        public bool CstBankFlag { get; set; }
        public bool CstCcFlag { get; set; }
        public bool CstInvFlag { get; set; }
        public bool CstStmtFlag { get; set; }
        public bool CstBillFlag { get; set; }
        public bool CstImageFlag { get; set; }
        public object OperationType { get; set; }
        public object OperationDesc { get; set; }
        public object StartedUrl { get; set; }
        public object EndedUrl { get; set; }
        public object CancelledUrl { get; set; }
        public object SuccessUrl { get; set; }
        public object FailureUrl { get; set; }
        public object MessageUrl { get; set; }
        public object WarningUrl { get; set; }
        public object ErrorUrl { get; set; }
        public object AddedUrl { get; set; }
        public object UpdatedUrl { get; set; }
        public object ReloadedUrl { get; set; }
        public object ChangedUrl { get; set; }
    }
}
