﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class GetTransactionsResponseRoot : RootBase, IGetTransactionsResponseRoot
    {
        public GetTransactionsResponseRoot() { }
        public GetTransactionsResponseRoot(Proxy.Models.IGetTransactionsResponseRoot getTransactionsResponseRoot) : base(getTransactionsResponseRoot)
        {
            if (getTransactionsResponseRoot.AccountTransactions != null)
                CopyAccountTransactionList(getTransactionsResponseRoot.AccountTransactions);
            StatusText = getTransactionsResponseRoot?.StatusText;
        }
        public IList<IAccountTransactions> AccountTransactions { get; set; }

        public string StatusText { get; set; }
        private void CopyAccountTransactionList(IList<Proxy.Models.IAccountTransactions> accountTransactions)
        {
            AccountTransactions = new List<IAccountTransactions>();
            foreach (var accountTrans in accountTransactions)
            {
                AccountTransactions.Add(new AccountTransactions(accountTrans));
            }
        }
    }
}
