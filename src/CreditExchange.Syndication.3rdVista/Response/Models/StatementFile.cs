﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class StatementFile : IStatementFile
    {
        public StatementFile() { }
        public StatementFile(Proxy.Models.IStatementFile statementFile)
        {
            if (statementFile != null)
            {
                RowId = statementFile.RowId;
                RowDatetime = statementFile.RowDatetime;
                RowEpochtime = statementFile.RowEpochtime;
                RowKey = statementFile.RowKey;
                StmtFileLabel = statementFile.StmtFileLabel;
                ContainerType = statementFile.ContainerType;
                ContainerDesc = statementFile.ContainerDesc;
                SubAcctCode = statementFile.SubAcctCode;
                SubAcctName = statementFile.SubAcctName;
                UserEmail = statementFile.UserEmail;
                UserName = statementFile.UserName;
                CustomerCode = statementFile.CustomerCode;
                CustomerName = statementFile.CustomerName;
                ParentCustomerCode = statementFile.ParentCustomerCode;
                ParentCustomerName = statementFile.ParentCustomerName;
                FileDate = statementFile.FileDate;
                FileEpochdate = statementFile.FileEpochdate;
                FileName = statementFile.FileName;
                FileSize = statementFile.FileSize;
                FileUrl = statementFile.FileUrl;
                MimeType = statementFile.MimeType;
                AccountCode = statementFile.AccountCode;
                AccountName = statementFile.AccountName;
                ExtractorCode = statementFile.ExtractorCode;
                ExtractorName = statementFile.ExtractorName;
                ExtractorUrl = statementFile.ExtractorUrl;
                IntuitId = statementFile.IntuitId;
                ProviderCode = statementFile.ProviderCode;
                ProviderName = statementFile.ProviderName;
                ProviderAlias = statementFile.ProviderAlias;
                ProviderUrl = statementFile.ProviderUrl;
                CountryCode = statementFile.CountryCode;
                CountryName = statementFile.CountryName;
                FileTimestamp = statementFile.FileTimestamp;
                FileEpochtime = statementFile.FileEpochtime;
                IconUrl = statementFile.IconUrl;
            }
        }

        public string RowId { get; set; }
        public string RowDatetime { get; set; }
        public int RowEpochtime { get; set; }
        public string RowKey { get; set; }
        public string StmtFileLabel { get; set; }
        public string ContainerType { get; set; }
        public string ContainerDesc { get; set; }
        public string SubAcctCode { get; set; }
        public string SubAcctName { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public object ParentCustomerCode { get; set; }
        public object ParentCustomerName { get; set; }
        public string FileDate { get; set; }
        public int FileEpochdate { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public string FileUrl { get; set; }
        public string MimeType { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string ExtractorCode { get; set; }
        public string ExtractorName { get; set; }
        public string ExtractorUrl { get; set; }
        public string IntuitId { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAlias { get; set; }
        public string ProviderUrl { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string FileTimestamp { get; set; }
        public object FileEpochtime { get; set; }
        public string IconUrl { get; set; }
    }
}
