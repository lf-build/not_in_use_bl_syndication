﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class ListAccountsResponseRoot : RootBase, IListAccountsResponseRoot
    {
        public ListAccountsResponseRoot() { }
        public ListAccountsResponseRoot(Proxy.Models.IListAccountsResponseRoot listAccountsResponseRoot) : base(listAccountsResponseRoot)
        {
            if (listAccountsResponseRoot.Accounts != null)
                CopyAccountList(listAccountsResponseRoot.Accounts);
                StatusText = listAccountsResponseRoot?.StatusText;
        }
        public IList<IAccount> Accounts { get; set; }
        public string StatusText { get; set; }
        private void CopyAccountList(IList<Proxy.Models.IAccount> accounts)
        {
            Accounts = new List<IAccount>();
            foreach (var account in accounts)
            {
                Accounts.Add(new Account(account));
            }
        }
    }
}
