﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public interface IRole
    {
       string RoleCode { get; set; }
       string RoleDescription { get; set; }
    }
}
