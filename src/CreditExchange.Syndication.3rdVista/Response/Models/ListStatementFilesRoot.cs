﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class ListStatementFilesRoot : IListStatementFilesRoot
    {
        public ListStatementFilesRoot()
        {

        }
        public ListStatementFilesRoot(Proxy.Models.IListStatementFilesRoot listStatementFilesRoot)
        {
            if (listStatementFilesRoot != null)
            {
                StatusText = listStatementFilesRoot.StatusText;
                RowCount = listStatementFilesRoot.RowCount;
                PageCount = listStatementFilesRoot.PageCount;
                PageNumber = listStatementFilesRoot.PageNumber;
                ReturnCode = listStatementFilesRoot.ReturnCode;
                ReturnText = listStatementFilesRoot.ReturnText;
                ReturnType = listStatementFilesRoot.ReturnType;
                if (listStatementFilesRoot.StatementFiles != null)
                    CopyStatementFiles(listStatementFilesRoot.StatementFiles);
            }
        }
        public IList<IStatementFile> StatementFiles { get; set; }
        public string StatusText { get; set; }
        public string RowCount { get; set; }
        public string PageCount { get; set; }
        public string PageNumber { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnText { get; set; }
        public string ReturnType { get; set; }
        private void CopyStatementFiles(IList<Proxy.Models.IStatementFile> statementFiles)
        {
            StatementFiles = new List<IStatementFile>();
            foreach (var item in statementFiles)
            {
                StatementFiles.Add(new StatementFile(item));
            }
        }
    }
}
