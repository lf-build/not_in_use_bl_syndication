﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public interface IListAccountsResponseRoot: IRootBase
    {
         IList<IAccount> Accounts { get; set; }
         string StatusText { get; set; }
    }
}
