﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class AuthenticationResponseRoot : RootBase, IAuthenticationResponseRoot
    {
        public AuthenticationResponseRoot() { }
        public AuthenticationResponseRoot(Proxy.Models.IAuthenticationResponseRoot authenticationResponseRoot) : base(authenticationResponseRoot)
        {
            if (authenticationResponseRoot != null)
            {
                UserEmail = authenticationResponseRoot.UserEmail;
                UserName = authenticationResponseRoot.UserName;
                PasswordExpired = authenticationResponseRoot.PasswordExpired;
                ExpirationDays = authenticationResponseRoot.ExpirationDays;
                SessionCookie = authenticationResponseRoot.SessionCookie;
                SessionId = authenticationResponseRoot.SessionId;
                SessionTimeout = authenticationResponseRoot.SessionTimeout;
                LanguageCode = authenticationResponseRoot.LanguageCode;
                CountryCode = authenticationResponseRoot.CountryCode;
                NumericChars = authenticationResponseRoot.NumericChars;
                DateMask = authenticationResponseRoot.DateMask;
                TimeMask = authenticationResponseRoot.TimeMask;
                DatetimeMask = authenticationResponseRoot.DatetimeMask;
                TimestampMask = authenticationResponseRoot.TimestampMask;
                CurrencySign = authenticationResponseRoot.CurrencySign;
                EnableLock = authenticationResponseRoot.EnableLock;
                EnableCache = authenticationResponseRoot.EnableCache;
                EnableCompression = authenticationResponseRoot.EnableCompression;
                LockTimeout = authenticationResponseRoot.LockTimeout;
                ValueSeparator = authenticationResponseRoot.ValueSeparator;
                TypeSeparator = authenticationResponseRoot.TypeSeparator;
                FieldSeparator = authenticationResponseRoot.FieldSeparator;
                ListSeparator = authenticationResponseRoot.ListSeparator;
                CustomerCode = authenticationResponseRoot.CustomerCode;
                CustomerName = authenticationResponseRoot.CustomerName;
                CustomerEmail = authenticationResponseRoot.CustomerEmail;
                AccountLimit = authenticationResponseRoot.AccountLimit;
                UpdateInterval = authenticationResponseRoot.UpdateInterval;
                ConcurrencyLimit = authenticationResponseRoot.ConcurrencyLimit;
                UserLimit = authenticationResponseRoot.UserLimit;
                FuTimestamp = authenticationResponseRoot.FuTimestamp;
                LuTimestamp = authenticationResponseRoot.LuTimestamp;
                FuEpochtime = authenticationResponseRoot.FuEpochtime;
                LuEpochtime = authenticationResponseRoot.LuEpochtime;
                ServerTimezone = authenticationResponseRoot.ServerTimezone;
                ClientTimezone = authenticationResponseRoot.ClientTimezone;
                BankFlag = authenticationResponseRoot.BankFlag;
                CcFlag = authenticationResponseRoot.CcFlag;
                InvFlag = authenticationResponseRoot.InvFlag;
                StmtFlag = authenticationResponseRoot.StmtFlag;
                BillFlag = authenticationResponseRoot.BillFlag;
                ImageFlag = authenticationResponseRoot.ImageFlag;
                MaxTransHistory = authenticationResponseRoot.MaxTransHistory;
                MaxStmtHistory = authenticationResponseRoot.MaxStmtHistory;
                MaxTransCount = authenticationResponseRoot.MaxTransCount;
                MaxStmtCount = authenticationResponseRoot.MaxStmtCount;
                DefTransHistory = authenticationResponseRoot.DefTransHistory;
                DefStmtHistory = authenticationResponseRoot.DefStmtHistory;
                DefTransCount = authenticationResponseRoot.DefTransCount;
                DefStmtCount = authenticationResponseRoot.DefStmtCount;
                MessageText = authenticationResponseRoot.MessageText;
                RowCount = authenticationResponseRoot.RowCount;
                PageCount = authenticationResponseRoot.PageCount;
                PageNumber = authenticationResponseRoot.PageNumber;
                ReturnCode = authenticationResponseRoot.ReturnCode;
                ReturnText = authenticationResponseRoot.ReturnText;
                ReturnType = authenticationResponseRoot.ReturnType;
                if (authenticationResponseRoot.Role != null)
                    CopyRoleList(authenticationResponseRoot.Role);
            }
        }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public bool PasswordExpired { get; set; }
        public int ExpirationDays { get; set; }
        public string SessionCookie { get; set; }
        public string SessionId { get; set; }
        public int SessionTimeout { get; set; }
        public string LanguageCode { get; set; }
        public string CountryCode { get; set; }
        public string NumericChars { get; set; }
        public string DateMask { get; set; }
        public string TimeMask { get; set; }
        public string DatetimeMask { get; set; }
        public string TimestampMask { get; set; }
        public string CurrencySign { get; set; }
        public bool EnableLock { get; set; }
        public bool EnableCache { get; set; }
        public bool EnableCompression { get; set; }
        public int LockTimeout { get; set; }
        public string ValueSeparator { get; set; }
        public string TypeSeparator { get; set; }
        public string FieldSeparator { get; set; }
        public string ListSeparator { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public int AccountLimit { get; set; }
        public int UpdateInterval { get; set; }
        public int ConcurrencyLimit { get; set; }
        public int UserLimit { get; set; }
        public string FuTimestamp { get; set; }
        public string LuTimestamp { get; set; }
        public long FuEpochtime { get; set; }
        public long LuEpochtime { get; set; }
        public string ServerTimezone { get; set; }
        public string ClientTimezone { get; set; }
        public string BankFlag { get; set; }
        public string CcFlag { get; set; }
        public string InvFlag { get; set; }
        public string StmtFlag { get; set; }
        public string BillFlag { get; set; }
        public string ImageFlag { get; set; }
        public int MaxTransHistory { get; set; }
        public int MaxStmtHistory { get; set; }
        public int MaxTransCount { get; set; }
        public int MaxStmtCount { get; set; }
        public int DefTransHistory { get; set; }
        public int DefStmtHistory { get; set; }
        public int DefTransCount { get; set; }
        public int DefStmtCount { get; set; }
        public IList<IRole> Roles { get; set; }
        private void CopyRoleList(IList<Proxy.Models.IRole> roles)
        {
            Roles = new List<IRole>();
            foreach (var role in roles)
            {
                Roles.Add(new Role(role));
            }
        }
    }
}
