﻿namespace CreditExchange.Syndication.ThirdVista.Response.Models
{
    public class BankAccountTransactionImage : IBankAccountTransactionImage
    {
        public BankAccountTransactionImage() { }
        public BankAccountTransactionImage(Proxy.Models.IBankAccountTransactionImage bankAccountTransactionImage)
        {
            if (bankAccountTransactionImage != null)
            {
                RowId = bankAccountTransactionImage.RowId;
                RowDateTime = bankAccountTransactionImage.RowDateTime;
                RowEpochtime = bankAccountTransactionImage.RowEpochtime;
                RowKey = bankAccountTransactionImage.RowKey;
                UserEmail = bankAccountTransactionImage.UserEmail;
                UserName = bankAccountTransactionImage.UserName;
                CustomerCode = bankAccountTransactionImage.CustomerCode;
                CustomerName = bankAccountTransactionImage.CustomerName;
                ParentCustomerCode = bankAccountTransactionImage.ParentCustomerCode;
                ParentCustomerName = bankAccountTransactionImage.ParentCustomerName;
                ExtractorCode = bankAccountTransactionImage.ExtractorCode;
                ExtractorName = bankAccountTransactionImage.ExtractorName;
                ExtractorUrl = bankAccountTransactionImage.ExtractorUrl;
                IntuitId = bankAccountTransactionImage.IntuitId;
                ProviderCode = bankAccountTransactionImage.ProviderCode;
                ProviderName = bankAccountTransactionImage.ProviderName;
                ProviderAlias = bankAccountTransactionImage.ProviderAlias;
                ProviderUrl = bankAccountTransactionImage.ProviderUrl;
                CountryCode = bankAccountTransactionImage.CountryCode;
                CountryName = bankAccountTransactionImage.CountryName;
                AccountCode = bankAccountTransactionImage.AccountCode;
                AccountName = bankAccountTransactionImage.AccountName;
                BankAccountCode = bankAccountTransactionImage.BankAccountCode;
                BankAccountName = bankAccountTransactionImage.BankAccountName;
                TransactionDateTime = bankAccountTransactionImage.TransactionDateTime;
                TransactionEpochTime = bankAccountTransactionImage.TransactionEpochTime;
                TransactionDate = bankAccountTransactionImage.TransactionDate;
                TransactionEpochDate = bankAccountTransactionImage.TransactionEpochDate;
                TransactionTime = bankAccountTransactionImage.TransactionTime;
                TransactionMonth = bankAccountTransactionImage.TransactionMonth;
                TransactionMonthName = bankAccountTransactionImage.TransactionMonthName;
                TransactionMonthYear = bankAccountTransactionImage.TransactionMonthYear;
                TransactionMonthDay = bankAccountTransactionImage.TransactionMonthDay;
                TransactionWeekDay = bankAccountTransactionImage.TransactionWeekDay;
                TransactionWeekQuarter = bankAccountTransactionImage.TransactionWeekQuarter;
                TransactionWeekISOYear = bankAccountTransactionImage.TransactionWeekISOYear;
                TransactionWeekANSIYear = bankAccountTransactionImage.TransactionWeekANSIYear;
                TransactionSeq = bankAccountTransactionImage.TransactionSeq;
                TransactionCode = bankAccountTransactionImage.TransactionCode;
                TransactionDesc = bankAccountTransactionImage.TransactionDesc;
                CreditValue = bankAccountTransactionImage.CreditValue;
                DebitValue = bankAccountTransactionImage.DebitValue;
                NetValue = bankAccountTransactionImage.NetValue;
                AbsValue = bankAccountTransactionImage.AbsValue;
                CurrencyCode = bankAccountTransactionImage.CurrencyCode;
                CurrencyName = bankAccountTransactionImage.CurrencyName;
                CurrencySign = bankAccountTransactionImage.CurrencySign;
                CategoryName = bankAccountTransactionImage.CategoryName;
                CategoryGroup = bankAccountTransactionImage.CategoryGroup;
                SubCategoryName = bankAccountTransactionImage.SubCategoryName;
                CustomerCategoryName = bankAccountTransactionImage.CustomerCategoryName;
                CustomerCategoryGroup = bankAccountTransactionImage.CustomerCategoryGroup;
                CustomerSubCategoryName = bankAccountTransactionImage.CustomerSubCategoryName;
                CustomerClassType = bankAccountTransactionImage.CustomerClassType;
                CustomerClassName = bankAccountTransactionImage.CustomerClassName;
                TransactionCategoryName = bankAccountTransactionImage.TransactionCategoryName;
                TransactionCategoryGroup = bankAccountTransactionImage.TransactionCategoryGroup;
                TransactionSubCategoryName = bankAccountTransactionImage.TransactionSubCategoryName;
                TransactionClassType = bankAccountTransactionImage.TransactionClassType;
                TransactionClassName = bankAccountTransactionImage.TransactionClassName;
                ClassType = bankAccountTransactionImage.ClassType;
                ClassName = bankAccountTransactionImage.ClassName;
                SavedTimestamp = bankAccountTransactionImage.SavedTimestamp;
                SavedEpochtime = bankAccountTransactionImage.SavedEpochtime;
                ValidFlag = bankAccountTransactionImage.ValidFlag;
                ImageSeq = bankAccountTransactionImage.ImageSeq;
                ImageUrl = bankAccountTransactionImage.ImageUrl;
                MIMEType = bankAccountTransactionImage.MIMEType;
                ImageTimestamp = bankAccountTransactionImage.ImageTimestamp;
                ImageEpochtime = bankAccountTransactionImage.ImageEpochtime;
                ImageCount = bankAccountTransactionImage.ImageCount;
                CheckNumber = bankAccountTransactionImage.CheckNumber;
                Payee = bankAccountTransactionImage.Payee;
                TransactionRowId = bankAccountTransactionImage.TransactionRowId;
                TransactionRowKey = bankAccountTransactionImage.TransactionRowKey;
            }
        }
        public string RowId { get; set; }
        public string RowDateTime { get; set; }
        public int RowEpochtime { get; set; }
        public string RowKey { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public object ParentCustomerCode { get; set; }
        public object ParentCustomerName { get; set; }
        public string ExtractorCode { get; set; }
        public string ExtractorName { get; set; }
        public string ExtractorUrl { get; set; }
        public string IntuitId { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderAlias { get; set; }
        public string ProviderUrl { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public string BankAccountCode { get; set; }
        public string BankAccountName { get; set; }
        public string TransactionDateTime { get; set; }
        public int TransactionEpochTime { get; set; }
        public string TransactionDate { get; set; }
        public int TransactionEpochDate { get; set; }
        public string TransactionTime { get; set; }
        public string TransactionMonth { get; set; }
        public string TransactionMonthName { get; set; }
        public string TransactionMonthYear { get; set; }
        public string TransactionMonthDay { get; set; }
        public string TransactionWeekDay { get; set; }
        public string TransactionWeekQuarter { get; set; }
        public string TransactionWeekISOYear { get; set; }
        public string TransactionWeekANSIYear { get; set; }
        public int TransactionSeq { get; set; }
        public string TransactionCode { get; set; }
        public string TransactionDesc { get; set; }
        public double CreditValue { get; set; }
        public double DebitValue { get; set; }
        public double NetValue { get; set; }
        public double AbsValue { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySign { get; set; }
        public string CategoryName { get; set; }
        public string CategoryGroup { get; set; }
        public string SubCategoryName { get; set; }
        public string CustomerCategoryName { get; set; }
        public string CustomerCategoryGroup { get; set; }
        public string CustomerSubCategoryName { get; set; }
        public string CustomerClassType { get; set; }
        public string CustomerClassName { get; set; }
        public string TransactionCategoryName { get; set; }
        public string TransactionCategoryGroup { get; set; }
        public string TransactionSubCategoryName { get; set; }
        public string TransactionClassType { get; set; }
        public string TransactionClassName { get; set; }
        public string ClassType { get; set; }
        public string ClassName { get; set; }
        public string SavedTimestamp { get; set; }
        public object SavedEpochtime { get; set; }
        public bool ValidFlag { get; set; }
        public int ImageSeq { get; set; }
        public string ImageUrl { get; set; }
        public string MIMEType { get; set; }
        public string ImageTimestamp { get; set; }
        public object ImageEpochtime { get; set; }
        public int ImageCount { get; set; }
        public object CheckNumber { get; set; }
        public object Payee { get; set; }
        public string TransactionRowId { get; set; }
        public string TransactionRowKey { get; set; }
    }
}
