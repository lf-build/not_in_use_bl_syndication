﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class AuthenticationResponse : IAuthenticationResponse
    {
        public AuthenticationResponse() { }
        public AuthenticationResponse(Proxy.Response.IAuthenticationResponse authenticationResponse)
        {
            if (authenticationResponse != null)
                AuthenticationResponseRoot = new AuthenticationResponseRoot(authenticationResponse.AuthenticationResponseRoot);
        }
        public IAuthenticationResponseRoot AuthenticationResponseRoot { get; set; }
    }
}
