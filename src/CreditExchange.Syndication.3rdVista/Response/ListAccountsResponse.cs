﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class ListAccountsResponse : IListAccountsResponse
    {
        public ListAccountsResponse() { }
        public ListAccountsResponse(Proxy.Response.IListAccountsResponse listAccountsResponse)
        {
            if (listAccountsResponse != null)
                ListAccountsResponseRoot = new ListAccountsResponseRoot(listAccountsResponse.ListAccountsResponseRoot);
        }
        public IListAccountsResponseRoot ListAccountsResponseRoot { get; set; }
    }
}
