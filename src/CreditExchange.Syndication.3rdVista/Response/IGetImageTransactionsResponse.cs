﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public interface IGetImageTransactionsResponse
    {
        IGetImageTransactionsRoot Root { get; set; }
    }
}
