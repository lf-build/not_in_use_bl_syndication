﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class RegisterUserResponse : IRegisterUserResponse
    {
        public RegisterUserResponse()
        {

        }
        public RegisterUserResponse(Proxy.Response.IRegisterUserResponse registerUserResponse)
        {
            if (registerUserResponse != null)
                Root = new RegisterUserResponseRoot(registerUserResponse.Root);
        }
        public IRegisterUserResponseRoot Root { get; set; }
    }
}
