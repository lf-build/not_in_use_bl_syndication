﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public interface IListStatementFilesResponse
    {
        IListStatementFilesRoot Root { get; set; }
    }
}
