﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public interface IRegisterUserResponse
    {
        IRegisterUserResponseRoot Root { get; set; }
    }
}
