﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class ListStatementFilesResponse : IListStatementFilesResponse
    {
        public ListStatementFilesResponse() { }
        public ListStatementFilesResponse(Proxy.Response.IListStatementFilesResponse listStatementFilesResponse)
        {
            if (listStatementFilesResponse != null)
                Root = new ListStatementFilesRoot(listStatementFilesResponse.Root);
        }
        public IListStatementFilesRoot Root { get; set; }
    }
}
