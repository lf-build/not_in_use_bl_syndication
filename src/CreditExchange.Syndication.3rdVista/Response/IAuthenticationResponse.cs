﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public interface IAuthenticationResponse
    {
        IAuthenticationResponseRoot AuthenticationResponseRoot { get; set; }
    }
}
