﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public interface IGetTransactionsResponse
    {
        IGetTransactionsResponseRoot GetTransactionsResponseRoot { get; set; }
    }
}
