﻿using CreditExchange.Syndication.ThirdVista.Response.Models;

namespace CreditExchange.Syndication.ThirdVista.Response
{
    public class GetTransactionsResponse: IGetTransactionsResponse
    {
        public GetTransactionsResponse() { }
        public GetTransactionsResponse(Proxy.Response.IGetTransactionsResponse getTransactionsResponse)
        {
            if (getTransactionsResponse != null)
                GetTransactionsResponseRoot = new GetTransactionsResponseRoot(getTransactionsResponse.GetTransactionsResponseRoot);
        }
        public IGetTransactionsResponseRoot GetTransactionsResponseRoot { get; set; }
    }
}
