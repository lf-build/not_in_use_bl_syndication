﻿using CreditExchange.Syndication.ThirdVista.Response;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.ThirdVista
{
    public interface IThirdVistaService
    {
        Task<IRegisterUserResponse> RegisterUser(string entityType, string entityId,string customerSessionId, string email, string password);
        Task<IAuthenticationResponse> AuthenticateUser(string entityType, string entityId,string email, string password);
        Task<IListAccountsResponse> GetAccounts(string entityType, string entityId,string sessionId);
        Task<IGetTransactionsResponse> GetTransactions(string entityType, string entityId,string sessionId);
        Task<IGetImageTransactionsResponse> GetImageTransactions(string entityType, string entityId,string sessionId);
        Task<IListStatementFilesResponse> GetStatementFiles(string entityType, string entityId,string sessionId);
    }
}
