﻿using CreditExchange.Syndication.ThirdVista.Proxy;
using LendFoundry.EventHub.Client;
using System;
using CreditExchange.Syndication.ThirdVista.Response;
using System.Threading.Tasks;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using CreditExchange.Syndication.ThirdVista.Events;
using System.Linq;

namespace CreditExchange.Syndication.ThirdVista
{
    public class ThirdVistaService : IThirdVistaService
    {
        public ThirdVistaService(IThirdVistaProxy proxy, IEventHubClient eventHub, ILookupService lookup)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
        }
        private IThirdVistaProxy Proxy { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }

        public async Task<IRegisterUserResponse> RegisterUser(string entityType, string entityId,string customerSessionId, string email, string password)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(customerSessionId))
                throw new ArgumentNullException(nameof(customerSessionId));
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));
            var referencenumber = Guid.NewGuid().ToString("N");
            var response = await Task.Run(() => Proxy.RegisterUser(customerSessionId, email, password));
            var result = new RegisterUserResponse(response); 
            await EventHub.Publish(new ThirdVistaRegisterUserRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request =new { CustomerSessionId= customerSessionId , Email = email,Password=password },
                ReferenceNumber = referencenumber
            });
            return result;
        }

        public async Task<IAuthenticationResponse> AuthenticateUser(string entityType, string entityId,string email, string password)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));
            var response = await Task.Run(() => Proxy.AuthenticateUser(email, password));
            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new AuthenticationResponse(response);
            await EventHub.Publish(new ThirdVistaAuthenticateUserRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request = new { Email = email, Password = password },
                ReferenceNumber = referencenumber
            });
            return result;
        }

        public async Task<IListAccountsResponse> GetAccounts(string entityType, string entityId,string sessionId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));
            var response = await Task.Run(() => Proxy.GetAccounts(sessionId));
            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new ListAccountsResponse(response);
            await EventHub.Publish(new ThirdVistaGetAccountsRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request =new { SessionId= sessionId },
                ReferenceNumber = referencenumber
            });
            return result;
        }

        public async Task<IGetTransactionsResponse> GetTransactions(string entityType, string entityId,string sessionId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));
            var response = await Task.Run(() => Proxy.GetTransactions(entityId));
            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new GetTransactionsResponse(response);
            await EventHub.Publish(new ThirdVistaGetTransactionsRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request =new { SessionId = sessionId },
                ReferenceNumber = referencenumber
            });
            return result;
        }

        public async Task<IGetImageTransactionsResponse> GetImageTransactions(string entityType, string entityId,string sessionId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));
            var response = await Task.Run(() => Proxy.GetImageTransactions(sessionId));
            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new GetImageTransactionsResponse(response);
            await EventHub.Publish(new ThirdVistaGetImageTransactionsRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request = new { SessionId = sessionId },
                ReferenceNumber = referencenumber
            });
            return result;
        }

        public async Task<IListStatementFilesResponse> GetStatementFiles(string entityType, string entityId,string sessionId)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(sessionId))
                throw new ArgumentNullException(nameof(sessionId));
            var response = await Task.Run(() => Proxy.GetStatementFiles(sessionId));
            var referencenumber = Guid.NewGuid().ToString("N");
            var result = new ListStatementFilesResponse(response);
            await EventHub.Publish(new ThirdVistaGetStatementFilesRequested
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = result,
                Request = new { SessionId = sessionId },
                ReferenceNumber = referencenumber
            });
            return result;
        }
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}
