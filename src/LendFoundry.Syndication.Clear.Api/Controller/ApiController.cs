﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Clear.Request;
using Microsoft.AspNet.Mvc;
using System;

using System.Threading.Tasks;

namespace LendFoundry.Syndication.Clear.Api
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IClearService clearService)
        {
            ClearService = clearService;
        }

        private IClearService ClearService { get; }

        [HttpPost("{entitytype}/{entityid}/person/search")]
        public async Task<IActionResult> SearchPerson(string entityType, string entityId, [FromBody]PersonSearchRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {

                    return Ok(await Task.Run(() => ClearService.SearchPerson(entityType, entityId, request)));

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }


        [HttpPost("{entitytype}/{entityid}/person/report/{groupId}")]
        public async Task<IActionResult> GetPersonReport(string entityType, string entityId, string groupId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {

                    return Ok(await Task.Run(() => ClearService.GetPersonReport(entityType, entityId, groupId)));

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }
        [HttpPost("{entitytype}/{entityid}/business/search")]
        public async Task<IActionResult> SearchBusiness(string entityType, string entityId, [FromBody]BusinessSearchRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {

                    return Ok(await Task.Run(() => ClearService.SearchBusiness(entityType, entityId, request)));

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPost("{entitytype}/{entityid}/business/report/{groupId}")]
        public async Task<IActionResult> GetBusinessReport(string entityType, string entityId,string groupId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {

                    return Ok(await Task.Run(() => ClearService.GetBusinessReport(entityType, entityId, groupId)));

                }
                catch (Exception exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

    }
}