﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;
using LendFoundry.Syndication.LexisNexis.Client;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;

namespace LendFoundry.Paynet.Client.Test
{
    public class LexisNexisServiceClientTest
    {
        private Mock<IServiceClient> Client { get; set; }
        public LexisNexisClientService LexisNexisServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        public LexisNexisServiceClientTest()
        {
            Client = new Mock<IServiceClient>();
            LexisNexisServiceClient = new LexisNexisClientService(Client.Object);
        }

        [Fact]
        public async void client_BankruptcySearch()
        {
            FcraBankruptcySearch3Response response = new FcraBankruptcySearch3Response();
            response.Records = null;

            Client.Setup(s => s.ExecuteAsync<BankruptcySearchResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new BankruptcySearchResponse(response));

            var Bankruptcyresponse = await LexisNexisServiceClient.BankruptcySearch("application", "test", It.IsAny<ISearchBankruptcyRequest>());
            Assert.Equal("/bankruptcy/{entityType}/{entityId}/bankruptcy-search", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_BankruptcyReport()
        {
            FcraBankruptcyReport3Response response = new FcraBankruptcyReport3Response();
            response.BankruptcyReportRecords = null;

            Client.Setup(s => s.ExecuteAsync<BankruptcyReportResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new BankruptcyReportResponse(response));

            var Bankruptcyresponse = await LexisNexisServiceClient.BankruptcyReport("application", "test", It.IsAny<IBankruptcyReportRequest>());
            Assert.Equal("/bankruptcy/{entityType}/{entityId}/bankruptcy-report", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_CriminalSearch()
        {
            FcraCriminalSearchResponse response = new FcraCriminalSearchResponse();
            response.Records = null;

            Client.Setup(s => s.ExecuteAsync<CriminalRecordSearchResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new CriminalRecordSearchResponse(response));

            var Bankruptcyresponse = await LexisNexisServiceClient.CriminalSearch("application", "test", It.IsAny<ICriminalRecordSearchRequest>());
            Assert.Equal("/criminalrecord/{entityType}/{entityId}/criminalrecord-search", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_CriminalRecordReport()
        {
            FcraCriminalReportResponse response = new FcraCriminalReportResponse();
            response.CriminalRecords = null;

            Client.Setup(s => s.ExecuteAsync<CriminalReportResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new CriminalReportResponse(response));

            var Bankruptcyresponse = await LexisNexisServiceClient.CriminalRecordReport("application", "test", It.IsAny<ICriminalRecordReportRequest>());
            Assert.Equal("/criminalrecord/{entityType}/{entityId}/criminalrecord-report", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}