﻿using CreditExchange.Syndication.YodleeFastLink.Request;
using CreditExchange.Syndication.YodleeFastLink.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace CreditExchange.YodleeFastLink.Client.Test
{
    public class YodleeFastLinkClientTest
    {
        public YodleeFastLinkClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            yodleeFastLinkServiceClient = new YodleeFastLinkService(MockServiceClient.Object);
        }

        public YodleeFastLinkService yodleeFastLinkServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        [Fact]
        public async void client_GetCobrandToken()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<CobrandResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new CobrandResponse());

            var response = await yodleeFastLinkServiceClient.GetCobrandToken("123", "test");
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/get_cobrand_token", restRequest.Resource);
            Assert.Equal(Method.GET, restRequest.Method);
        }

        [Fact]
        public async void client_RegisterUser()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<UserAuthenticateResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new UserAuthenticateResponse());

            var response = await yodleeFastLinkServiceClient.RegisterUser("123", "test", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/register_user", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_AuthenticateUser()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<UserAuthenticateResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new UserAuthenticateResponse());

            var response = await yodleeFastLinkServiceClient.RegisterUser("123", "test", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/register_user", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_GetFastlinkAccessToken()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<FastlinkAccessTokenResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new FastlinkAccessTokenResponse());

            var response = await yodleeFastLinkServiceClient.GetFastlinkAccessToken("123", "test", It.IsAny<FastlinkAccessTokenRequest>());
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/get_fastlink_accesstoken", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_GetUserBankAccounts()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<GetBankAccountResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetBankAccountResponse());

            var response = await yodleeFastLinkServiceClient.GetUserBankAccounts("123", "test", It.IsAny<GetBankAccountsRequest>());
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/get_user_bank_accounts", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        [Fact]
        public async void client_GetAccountTransactions()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<TransactionResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new TransactionResponse());

            var response = await yodleeFastLinkServiceClient.GetAccountTransactions("123", "test", It.IsAny<GetTransactionRequest>());
            Assert.Equal("{entitytype}/{entityid}/yodleefastlink/get_account_transactions", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}