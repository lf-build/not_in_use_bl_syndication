﻿using CreditExchange.Syndication.YodleeFastLink;
using CreditExchange.Syndication.YodleeFastLink.Request;
using CreditExchange.Syndication.YodleeFastLink.Response;
using CreditExchange.YodleeFastLink.Api.Controllers;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.YodleeFastLink;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace CreditExchange.YodleeFastLink.Api.Test
{
    public class YodleeFastLinkController
    {
        private Mock<IYodleeFastLinkService> yelpService { get; }

        public ApiController GetController(IYodleeFastLinkService yodleeFastLinkService)
        {
            return new ApiController(yodleeFastLinkService);
        }

        private Mock<IYodleeFastLinkService> GetYodleeFastLinkService()
        {
            return new Mock<IYodleeFastLinkService>();
        }

        #region ReturnOnSuccess

        [Fact]
        public async void AuthenticateUserReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.AuthenticateUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>())).ReturnsAsync(It.IsAny<IUserAuthenticateResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).AuthenticateUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetAccountTransactionsReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetAccountTransactions("", "", It.IsAny<GetTransactionRequest>())).ReturnsAsync(It.IsAny<ITransactionResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).GetAccountTransactions("", "", It.IsAny<GetTransactionRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetCobrandTokenReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetCobrandToken("", "")).ReturnsAsync(It.IsAny<ICobrandResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).GetCobrandToken("", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetFalstLinkUrlReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetFalstLinkUrl("", "", It.IsAny<FastLinkUrlRequest>())).ReturnsAsync(It.IsAny<RestSharp.IRestResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).GetFalstLinkUrl("", "", It.IsAny<FastLinkUrlRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetFastlinkAccessTokenReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetFastlinkAccessToken("", "", It.IsAny<FastlinkAccessTokenRequest>())).ReturnsAsync(It.IsAny<IFastlinkAccessTokenResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).GetFastlinkAccessToken("", "", It.IsAny<FastlinkAccessTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void GetUserBankAccountsReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetUserBankAccounts("", "", It.IsAny<GetBankAccountsRequest>())).ReturnsAsync(It.IsAny<IGetBankAccountResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).GetUserBankAccounts("", "", It.IsAny<GetBankAccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void RegisterUserReturnOnSuccess()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.RegisterUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>())).ReturnsAsync(It.IsAny<IUserAuthenticateResponse>());

            var response = (HttpOkObjectResult)await GetController(yodleeFastLinkService.Object).RegisterUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        #endregion ReturnOnSuccess

        #region ReturnError

        [Fact]
        public async void GetExchangeTokenReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.AuthenticateUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).AuthenticateUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetAccountTransactionsReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetAccountTransactions("", "", It.IsAny<GetTransactionRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).GetAccountTransactions("", "", It.IsAny<GetTransactionRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetCobrandTokenReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetCobrandToken("", "")).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).GetCobrandToken("", "");
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetFalstLinkUrlReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetFalstLinkUrl("", "", It.IsAny<FastLinkUrlRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).GetFalstLinkUrl("", "", It.IsAny<FastLinkUrlRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetFastlinkAccessTokenReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetFastlinkAccessToken("", "", It.IsAny<FastlinkAccessTokenRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).GetFastlinkAccessToken("", "", It.IsAny<FastlinkAccessTokenRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void GetUserBankAccountsReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.GetUserBankAccounts("", "", It.IsAny<GetBankAccountsRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).GetUserBankAccounts("", "", It.IsAny<GetBankAccountsRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        [Fact]
        public async void RegisterUserReturnError()
        {
            var yodleeFastLinkService = GetYodleeFastLinkService();
            yodleeFastLinkService.Setup(x => x.RegisterUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>())).Throws(new YodleeFastLinkException());

            var response = (ErrorResult)await GetController(yodleeFastLinkService.Object).RegisterUser("", "", It.IsAny<UserRegisterNAuthenticateRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }

        #endregion ReturnError
    }
}