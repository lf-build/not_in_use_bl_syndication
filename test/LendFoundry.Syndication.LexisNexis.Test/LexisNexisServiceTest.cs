﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.LexisNexis;
using LendFoundry.Syndication.LexisNexis.Api;
using LendFoundry.Syndication.LexisNexis.Bankruptcy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcyReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.Bankruptcy.Proxy.BankruptcySearch.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalReport.ServiceReference;
using LendFoundry.Syndication.LexisNexis.CriminalRecord.Proxy.CriminalSearch.ServiceReference;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace CreditExchange.Syndication.LexisNexis.Test
{
    public class LexisNexisServiceTest
    {
        public LexisNexisServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Logger = new Mock<ILogger>();
            CriminalRecordProxy = new Mock<ICriminalRecordProxy>();
            BankruptcyProxy = new Mock<IBankruptcyProxy>();

            bankruptcyConfiguration = new BankruptcyConfiguration
            {
                BankruptcyUrl = "http://192.168.1.72:7022/bankruptcy",
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser=null,
                Options=null
                //EndUser = new LendFoundry.Syndication.LexisNexis.Bankruptcy.User
                //{
                //     ReferenceCode="",
                //     BillingCode="",
                //     QueryId="",
                //     GLBPurpose="3",
                //     DLPurpose="1",
                //     MaxWaitSeconds= 10000,
                //     AccountNumber="AccountNumber",
                //     EndUser= {
                //       City="",
                //       CompanyName="",
                //       State="",
                //       StreetAddress1="",
                //       Zip5=""
                //      }
                // },
                //Options = new SearchOption
                //{
                //     FCRAPurpose=1,
                //     ReturnCount=10,
                //     StartingRecord=1
                // }
            };

            criminalRecordConfiguration = new CriminalRecordConfiguration()
            {
                CriminalRecordUrl = "http =//192.168.1.72=7022/criminalrecord",
                UserName = "CAPLLCXML",
                Password = "Sigma@123",
                EndUser = null,
                Options = null
                //EndUser = new LendFoundry.Syndication.LexisNexis.CriminalRecord.User
                //{
                //                  ReferenceCode= "",
                //                  BillingCode="",
                //                  QueryId="",
                //                  GLBPurpose="3",
                //                  DLPurpose= "1",
                //                  MaxWaitSeconds= 10000,
                //                  AccountNumber= "AccountNumber",
                //                  EndUser= {
                //                    City="" ,
                //                    CompanyName="" ,
                //                    State= "",
                //                    StreetAddress1="" ,
                //                    Zip5=""
                //                    }
                //            },
                //Options = new SearchOption
                //{
                //          FCRAPurpose= 1,
                //          ReturnCount= 10,
                //          StartingRecord= 1
                //        }
            };

            lexisNexisConfiguration = new LexisNexisConfiguration();
            lexisNexisConfiguration.Bankruptcy = bankruptcyConfiguration;
            lexisNexisConfiguration.CriminalRecord = criminalRecordConfiguration;
            lexisNexisService = new LexisNexisService(CriminalRecordProxy.Object, BankruptcyProxy.Object,lexisNexisConfiguration, Lookup.Object,Logger.Object, EventHub.Object);
        }

        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private Mock<ILogger> Logger { get; }
        private LexisNexisService lexisNexisService { get; }
        private LexisNexisConfiguration lexisNexisConfiguration { get; }
        private BankruptcyConfiguration bankruptcyConfiguration { get; }

        private CriminalRecordConfiguration criminalRecordConfiguration { get; }
        private Mock<ICriminalRecordProxy> CriminalRecordProxy { get; }
        private Mock<IBankruptcyProxy> BankruptcyProxy { get; }

      


        [Fact]
        public void BankruptcySearch_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchBankruptcyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", It.IsAny<string>(), It.IsAny<ISearchBankruptcyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", It.IsAny<ISearchBankruptcyRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => lexisNexisService.BankruptcySearch(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchBankruptcyRequest>()));
            BankruptcyProxy.Setup(x => x.BankruptcySearch(null,null,null)).ReturnsAsync(It.IsAny<FcraBankruptcySearch3Response>());
            var SsnNullRequest = new SearchBankruptcyRequest
            {
                Ssn = "",
                FirstName="Abc",
                LastName="Abc"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", SsnNullRequest));

            var SsnInvalidRequest = new SearchBankruptcyRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = "A$$bc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", SsnInvalidRequest));

            var FirstNameNullRequest = new SearchBankruptcyRequest
            {
                Ssn = "123456789",
                FirstName = "",
                LastName = "Abc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", FirstNameNullRequest));

            var LastNameNullRequest = new SearchBankruptcyRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = ""
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", LastNameNullRequest));

            var FirstNameInvalidRequest = new SearchBankruptcyRequest
            {
                Ssn = "123456789",
                FirstName = "Ab#$c",
                LastName = "Abc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", FirstNameInvalidRequest));

            var LastNameInvalidRequest = new SearchBankruptcyRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = "A$$bc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcySearch("application", "12345", LastNameInvalidRequest));

        }


        [Fact]
        public void BankruptcyReport_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IBankruptcyReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", It.IsAny<string>(), It.IsAny<IBankruptcyReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", "12345", It.IsAny<IBankruptcyReportRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => lexisNexisService.BankruptcyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IBankruptcyReportRequest>()));
            BankruptcyProxy.Setup(x => x.BankruptcyReport(null, null, null)).ReturnsAsync(It.IsAny<FcraBankruptcyReport3Response>());
            var QueryIdNullRequest = new BankruptcyReportRequest
            {
                QueryId =null,
                UniqueId="123456789"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", "12345", QueryIdNullRequest));

            var QueryIdInvalidRequest = new BankruptcyReportRequest
            {
                QueryId ="123$%gh77",
                UniqueId = "123456789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", "12345", QueryIdInvalidRequest));

            var UniqueIdNullRequest = new BankruptcyReportRequest
            {
                QueryId = "123456789",
                UniqueId = "123456789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", "12345", UniqueIdNullRequest));

            var UniqueIdInvalidRequest = new BankruptcyReportRequest
            {
                QueryId = "123456789",
                UniqueId = "12345YU6789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.BankruptcyReport("application", "12345", UniqueIdInvalidRequest));


        }



        [Fact]
        public void CriminalSearch_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ICriminalRecordSearchRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", It.IsAny<string>(), It.IsAny<ICriminalRecordSearchRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", It.IsAny<ICriminalRecordSearchRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => lexisNexisService.CriminalSearch(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ICriminalRecordSearchRequest>()));
            CriminalRecordProxy.Setup(x => x.CriminalRecordSearch(null, null, null)).ReturnsAsync(It.IsAny<FcraCriminalSearchResponse>());
            var SsnNullRequest = new CriminalRecordSearchRequest
            {
                Ssn = "",
                FirstName = "Abc",
                LastName = "Abc"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", SsnNullRequest));

            var SsnInvalidRequest = new CriminalRecordSearchRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = "A$$bc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", SsnInvalidRequest));

            var FirstNameNullRequest = new CriminalRecordSearchRequest
            {
                Ssn = "123456789",
                FirstName = "",
                LastName = "Abc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", FirstNameNullRequest));

            var LastNameNullRequest = new CriminalRecordSearchRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = ""
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", LastNameNullRequest));

            var FirstNameInvalidRequest = new CriminalRecordSearchRequest
            {
                Ssn = "123456789",
                FirstName = "Ab#$c",
                LastName = "Abc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", FirstNameInvalidRequest));

            var LastNameInvalidRequest = new CriminalRecordSearchRequest
            {
                Ssn = "123456789",
                FirstName = "Abc",
                LastName = "A$$bc"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalSearch("application", "12345", LastNameInvalidRequest));

        }


        [Fact]
        public void CriminalRecordReport_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ICriminalRecordReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", It.IsAny<string>(), It.IsAny<ICriminalRecordReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", "12345", It.IsAny<ICriminalRecordReportRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => lexisNexisService.BankruptcyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IBankruptcyReportRequest>()));
            CriminalRecordProxy.Setup(x => x.CriminalRecordReport(null, null, null)).ReturnsAsync(It.IsAny<FcraCriminalReportResponse>());
            var QueryIdNullRequest = new CriminalRecordReportRequest
            {
                QueryId = null,
                UniqueId = "123456789"
            };
            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", "12345", QueryIdNullRequest));

            var QueryIdInvalidRequest = new CriminalRecordReportRequest
            {
                QueryId = "123$%gh77",
                UniqueId = "123456789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", "12345", QueryIdInvalidRequest));

            var UniqueIdNullRequest = new CriminalRecordReportRequest
            {
                QueryId = "123456789",
                UniqueId = "123456789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", "12345", UniqueIdNullRequest));

            var UniqueIdInvalidRequest = new CriminalRecordReportRequest
            {
                QueryId = "123456789",
                UniqueId = "12345YU6789"
            };

            Assert.ThrowsAsync<ArgumentNullException>(() => lexisNexisService.CriminalRecordReport("application", "12345", UniqueIdInvalidRequest));


        }



    }
}